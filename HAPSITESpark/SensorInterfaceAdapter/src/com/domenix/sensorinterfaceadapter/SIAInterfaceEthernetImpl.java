/*
 * SIAInterfaceEthernetImpl.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains an implementation of the SIAInterface for TCP/IP Ethernet connections.
 *
 * Version: V1.0  15/08/02
 */
package com.domenix.sensorinterfaceadapter;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.common.enums.NetworkProtocolTypeEnum;
import com.domenix.common.spark.CtlWrapper;
import com.domenix.common.spark.CtlWrapperQueue;
import com.domenix.common.spark.IPCQueueEvent;
import com.domenix.common.spark.IPCQueueEvent.IPCQueueEventType;
import com.domenix.common.spark.IPCQueueListenerInterface;
import com.domenix.common.spark.IPCWrapper;
import com.domenix.common.spark.IPCWrapperQueue;
import com.domenix.common.spark.data.CommandMsg;
import com.domenix.common.spark.data.MsgContent;
import com.domenix.common.utils.SparkThreadFactory;
import com.domenix.common.utils.SystemDateTime;
import com.domenix.common.utils.UtilTimer;
import com.domenix.commonsa.interfaces.GetIdentificationProcessor;
import com.domenix.commonsa.interfaces.HeaderMacroProcessor;
import com.domenix.commonsa.interfaces.SIAConnEventEnum;
import com.domenix.commonsa.interfaces.SIATransferEventEnum;
import com.domenix.commonsa.interfaces.SIAConnEvent;
import com.domenix.commonsa.interfaces.SIAConnection;
import com.domenix.commonsa.interfaces.SIAConnectionList;
import com.domenix.commonsa.interfaces.SIAConnectionTypeEnum;
import com.domenix.commonsa.interfaces.SIALink;
import com.domenix.commonsa.interfaces.SIALinkEvent;
import com.domenix.commonsa.interfaces.SIALinkList;
import com.domenix.commonsa.interfaces.SIATransferEvent;
import com.domenix.commonsa.interfaces.SaConnEventQueue;
import com.domenix.commonsa.interfaces.SIAXferEventQueue;
import com.domenix.commonsa.interfaces.SAQueues;
import com.domenix.commonsa.interfaces.SaAckNakMessageBuilder;
import com.domenix.commonsa.interfaces.SaCcsiAckNakEnum;
import com.domenix.commonsa.interfaces.SaCcsiChannelEnum;
import com.domenix.commonsa.interfaces.SaLinkEventQueue;
import com.domenix.commonsa.interfaces.SaMsgParserUtilities;
import com.domenix.commonsa.interfaces.SaNakDetailCodeEnum;
import com.domenix.commonsa.interfaces.SaNakReasonCodeEnum;
import com.domenix.commonsa.interfaces.SensorNetworkSettings;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import org.apache.log4j.Logger;
//import com.domenix.ccsi.protocol.PHContext;

//~--- classes ----------------------------------------------------------------
/**
 * This implements the SIAInterface for Ethernet TCP/IP based communications.
 *
 * @author kmiller
 */
public class SIAInterfaceEthernetImpl implements SIAInterface, IPCQueueListenerInterface, Runnable
{  
    
    /**
     * Pending send entry ID generator
     */
    private static final AtomicLong ITEM_ID_GENERATOR = new AtomicLong(0L);

    //~--- fields ---------------------------------------------------------------
    /**
     * Field description
     */
    private long myConnectId = 0L;

    
    /**
     * List of the available links
     */
    private final SIALinkList theLinks = new SIALinkList();

    /**
     * Started up flag
     */
    private boolean startedUp = false;

    /**
     * Shut down now
     */
    private boolean shutDown = false;

    /**
     * Field description
     */
    private boolean checkForStartMsg = true;

    /**
     * Field description
     */
    private boolean waitingForConnStart = false;

    /**
     * Waiting for a connection end message.
     */
    private final boolean waitingForConnEnd = false;

    /**
     * Enforce three strikes
     */
    private boolean enforceStrikes = true;

    /**
     * Connection event queue that we write
     */
    private SaConnEventQueue connEventQueue;

    /**
     * The control queue
     */
    private CtlWrapperQueue controlQueue;

    /**
     * The executor for service threads
     */
    private ExecutorService executor;

    /**
     * The message builder
     */
    private SaAckNakMessageBuilder saMsgBuilder;

    /**
     * Field description
     */
    private SaMsgParserUtilities saMsgParser;

    /**
     * Field description
     */
    private SaLinkEventQueue myLinkEventQ;

    /**
     * Field description
     */
    private LinkedBlockingDeque<SIALinkEvent> myLinkEvents;

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger;
    /** is debug log level enabled */
    private final boolean logLevelDebug;
    
    /**
     * Our pending send list
     */
    private PendingSendItemList pendingSends;

    /**
     * The Host Network configuration
     */
////    private HostNetworkSettings hostNetworkSettings;
    private SensorNetworkSettings sensorNetworkSettings;

    /**
     * Our transfer event queue - we only write to this
     */
    private SIAXferEventQueue siaTransferEventQueue;
    private SIAXferEventQueue spaTransferEventQueue;

    private static final SparkThreadFactory sparkPoolThreadFactory = new SparkThreadFactory ();
    
    private SIAConnectionList connectionList;
    
    private IPCWrapperQueue spAdapterToSi;
    
    private IPCWrapperQueue siAdapterToSp;
    
    
    //~--- constructors ---------------------------------------------------------

    /**
     * Constructs a class instance.
     *
     */
    public SIAInterfaceEthernetImpl()
    {
        myLogger = Logger.getLogger(SIAInterfaceEthernetImpl.class);
        myLogger.isDebugEnabled();
        logLevelDebug = myLogger.isDebugEnabled();
    }

    /**
     * Initializes the interface.
     *
     * @param queues - messaging queues used by the Host Adapter
     * @param config - Host Interface configuration information
     * @param theList - list of connections
     * @param theTimer - timer class instance
     */
    //TODO:   pass in theContext.   Used to setUuid.
    @Override
    public void initialize(SAQueues queues, SensorNetworkSettings config, SIAConnectionList theList, UtilTimer theTimer)
    {
        if ((queues != null) && (config != null) && (theList != null) && (theTimer != null))
        {
            connectionList = theList;
            sensorNetworkSettings = config;
            
            this.spAdapterToSi = queues.getSpAdapterToSi();
            this.siAdapterToSp = queues.getSiAdapterToSp();
            this.spAdapterToSi.addIPCQueueEventListener(this);
            this.connEventQueue = queues.getSiaConnEventQueue();
            this.spaTransferEventQueue = queues.getSpaXferEventQueue();
            this.siaTransferEventQueue = queues.getSiaXferEventQueue();
            this.siaTransferEventQueue.addIPCQueueEventListener(this);
            this.controlQueue = queues.getSiaCtlQueue();
            this.controlQueue.addIPCQueueEventListener(this);
            this.saMsgBuilder = new SaAckNakMessageBuilder(sensorNetworkSettings);
            this.executor = Executors.newFixedThreadPool(2, sparkPoolThreadFactory);
            this.checkForStartMsg = true;
            this.saMsgParser = new SaMsgParserUtilities(config.getClassification());
            this.myLinkEvents = new LinkedBlockingDeque<>();
            this.pendingSends = new PendingSendItemList();
            this.myLinkEventQ = queues.getSIALinkEventQueue();           
            this.myLinkEventQ.addIPCQueueEventListener(this);
//            this.enforceStrikes = hostNetworkSettings.isEnforceStrikes();

            //
            // Create the links
            //
            if (config.getNetworkProtocolType() == NetworkProtocolTypeEnum.Open)
            {
                //physical socket connection good.
                SIALinkEthernetImpl theLink = new SIALinkEthernetImpl(sensorNetworkSettings, 
                        connectionList, queues, theTimer);

                theLink.setBroadcastIpAddress(config.getBroadcastAddr());
                theLink.setLocalIpAddress(config.getPHIP());
//                theLink.setSecureComms(config.isEncryptComm());

                if (!config.isOiServer())
                {
                    if (config.getIP() != null)
                    {
                        theLink.setHostIpAddress(config.getIP());
                        theLink.setHostIpPort(config.getPort());
                        
                        
//                        //DO I NEED THIS NEXT LINE?!?!?!?!?!?!?!??!?!?!?!?!?!?!?
//                        theLink.setConnected(true);
//                        //?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!
                        
                    } else
                    {
                        throw (new IllegalStateException("CONFIGURATION IS MESSED UP!"));
                    }
                }

                theLink.getLinkEventQueue().addIPCQueueEventListener(this);
                this.theLinks.addLink(theLink);
                myLogger.debug("Connect link " + theLink.getLinkId());
            }
        }

        myLogger.info("SIAInterfaceEthernetImpl constructed");
    }

    
    
    
//KBM    START:    added from EthernetAdapterCcsi
//    /**
//     * Sets the callback for receiving data
//     *
//     * @param callback
//     */
//    public void setListener(SensorReceiveCallbackInterface callback) {
//        this.callback = callback;
//    }
      
//        /**
//     * Starts the receive data thread running
//     */
//    public void start() {
//        thread = new Thread(this);
//        if (server)
//        {
//            thread.setName("Server ReceiveDataThread");
//        } else {
//            thread.setName("Client ReceiveDataThread");
//        }
//        thread.start();
//    }
//KBM    END:    added from EthernetAdapterCcsi    
    
    
    
    
    
    //~--- methods --------------------------------------------------------------
    /**
     * Start up the interface
     *
     * @return flag indicating success/failure
     */
    @Override
    public boolean startInterface()
    {
        boolean retVal = false;

        if (!this.startedUp)
        {
            for (SIALink x : this.theLinks.getLinkList())
            {
                SIALinkEthernetImpl aLink = (SIALinkEthernetImpl) x;

                sparkPoolThreadFactory.setThreadName("SensorLinkEthernetThread");
                this.executor.execute(aLink);
            }

            retVal = true;
            this.startedUp = retVal;
        } else
        {
            myLogger.info("Interface already started up");
        }

        if (retVal)
        {
            myLogger.info("Started the interface");
        } else
        {
            myLogger.error("Error starting the interface");
        }

        return (retVal);
    }

    /**
     * Shut down the interface
     *
     * @return flag indicating success/failure
     */
    @Override
    public boolean shutDownInterface()
    {
        boolean retVal = false;

        if (this.startedUp)
        {
            myLogger.info("Shutting down the interface");

            // SEND END MESSAGE TO TERMINATE if we have a connection
            for (SIAConnection x : connectionList.getConnectionList())
            {
                x.setConnType(SIAConnectionTypeEnum.NO_CONNECTION);

                if (x.getConnType() != SIAConnectionTypeEnum.NO_CONNECTION)
                {
                    myLogger.debug("Sending open interface end on link " + x.getLinkId());

                    String endStr = this.createEndMsg();
                    long linkIndx = connectionList.getItem(this.myConnectId).getLinkId();
                    SIALinkEthernetImpl myLink = (SIALinkEthernetImpl) this.theLinks.getItem((int) linkIndx);
                    PendingSendItem psi = new PendingSendItem();

                    psi.isOIConnEndMsg = true;
                    this.pendingSends.addEntry(psi);
                    myLink.send(endStr, this.myConnectId, psi.itemId);
                }
            }

            for (SIALink x : this.theLinks.getLinkList())
            {
                SIALinkEthernetImpl aLink = (SIALinkEthernetImpl) x;

                aLink.shutDown();
            }

            boolean doMore = true;

            this.executor.shutdown();

            while (doMore)
            {
                try
                {
                    if (this.executor.awaitTermination(5, TimeUnit.SECONDS))
                    {
                        doMore = false;
                        retVal = true;
                        this.startedUp = false;
                    } else
                    {
                        this.executor.shutdownNow();
                        this.executor.awaitTermination(3, TimeUnit.SECONDS);
                        doMore = false;
                        retVal = true;
                        this.startedUp = false;
                    }
                } catch (InterruptedException ignore)
                {
                }
            }

            this.shutDown = true;
            myLogger.info("We are shut down");
        }

        return (retVal);
    }

    /**
     * Initiates SIA actions to register the sensor on the network. This is a
     * NOOPa at this time and will always return true.
     *
     * @param linkId the ID number of the link for network registration
     *
     * @return flag indicating success (true) or failure (false)
     *
     * @throws IllegalArgumentException
     */
    @Override
    public boolean register(int linkId) throws IllegalArgumentException
    {
        return (true);
    }

    /**
     * Initiates actions to deregister the sensor from the network. This is a
     * NOOPa at this time and will always return true.
     *
     * @param linkId the ID number of the link for network registration
     *
     * @return flag indicating success (true) or failure (false)
     *
     * @throws IllegalArgumentException
     */
    @Override
    public boolean deregister(int linkId) throws IllegalArgumentException
    {
        return (true);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Get the host IP address
     *
     * @param connectionId the ID of the connection
     *
     * @return the host address or null if not found
     */
    @Override
    public String getHostIp(long connectionId)
    {
        String host;
        int link ;

        link = connectionList.getItem(connectionId).getLinkId();

        SIALink myLink = this.theLinks.getItem(link);

        host = myLink.getConnectHostAddress(connectionId);

        return (host);
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Connect the sensor to a host system. This function initiates the
     * connection process on the connection through its assigned link. The
     * configuration information for the connection is specific to the type of
     * connection being implemented by the class that implements this interface
     * and must be provided prior to invoking this function.
     *
     * @param linkId the ID number of the link to be used
     *
     * @return an SIAConnection object or null if the connection initiation
     * fails
     *
     * @throws IllegalArgumentException if the index is out of range or does not
     * identify an available link
     */
    @Override
    public SIAConnection connect(int linkId) throws IllegalArgumentException
    {
        SIAConnection retVal = null;
        boolean gotLink = false;
        SIAConnection c;

        myLogger.info("Connected to linkID: " + linkId);
        if (connectionList.isEmpty())
        {
            c = new SIAConnection(linkId);
            c.setHostIpString(sensorNetworkSettings.getIP().getHostAddress());
            connectionList.addConnection(c);
            gotLink = theLinks.getItem((int) linkId).connect(c.getConnectionId());

            if (gotLink)
            {
                retVal = c;
            }
        } else
        {
            for (SIAConnection x : connectionList.getConnectionList())
            {
                if (x.getLinkId() == linkId)
                {
                    theLinks.getItem((int) linkId).connect(linkId);
                    retVal = x;
                }
            }
        }

        if (retVal != null)
        {
            this.myConnectId = retVal.getConnectionId();
        }

        return retVal;
    }

    /**
     * Disconnect the sensor from a host system. This function initiates the
     * disconnect process on an active connection through its assigned link.
     *
     * @param connectionId the ID number of the connection to be tested
     *
     * @return indication of success (true) or failure (false)
     *
     * @throws IllegalArgumentException if the index is out of range or does not
     * identify an active connection
     */
    @Override
    public boolean disconnect(long connectionId) throws IllegalArgumentException
    {
        boolean retVal = false;

        if (connectionId >= 0L)
        {
            long linkIndx = connectionList.getItem(connectionId).getLinkId();
            SIALinkEthernetImpl myLink = (SIALinkEthernetImpl) this.theLinks.getItem((int) linkIndx);

            if (myLink != null)
            {
                if (connectionList.getItem(connectionId).getConnType()
                        != SIAConnectionTypeEnum.NO_CONNECTION)
                {
                    // send End Msg.
                    String endMsg = this.createEndMsg();
                    PendingSendItem psi = new PendingSendItem();

                    psi.isOIConnEndMsg = true;
                    this.pendingSends.addEntry(psi);
                    myLink.send(endMsg, connectionId, psi.itemId);
                    myLogger.debug ("Sending END MSG as part of Discconnect.");
                }

                retVal = myLink.disconnect(connectionId);
            }
        }

        return retVal;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Return an indication of whether or not the connection is connected. This
     * indication must be based on the CCSI logical connection concept and
     * return the appropriate value.
     *
     * @param connectionId the ID number of the connection to be tested
     *
     * @return SIAConnectionTypeEnum indicating the type of connection
     *
     * @throws IllegalArgumentException if the index is out of range or does not
     * identify an active connection
     */
    @Override
    public SIAConnectionTypeEnum isConnected(long connectionId) throws IllegalArgumentException
    {
        SIAConnectionTypeEnum retVal = SIAConnectionTypeEnum.NO_CONNECTION;

        if (connectionId >= 1L)
        {
            for (SIAConnection x : connectionList.getConnectionList())
            {
                if (x.getConnectionId() == connectionId)
                {
                    retVal = x.getConnType();

                    return (retVal);
                }
            }
        }

        return (retVal);
    }

    /**
     * Returns a flag indicating whether the link underlying this connection is
     * 'open'. Open means that the underlying link is physically connected to
     * the communications media and is available for active communications. For
     * example, on a TCP/IP socket type link it means that there is currently an
     * open socket between the sensor and the host system.
     *
     * @param connectionId the ID number of the connection to be tested
     *
     * @return boolean indicating open (true) or not open (false)
     *
     * @throws IllegalArgumentException if the index is out of range or does not
     * identify an active connection
     */
    @Override
    public boolean isOpen(long connectionId) throws IllegalArgumentException
    {
        boolean retVal = false;

        if (connectionId >= 1L)
        {
            for (SIAConnection x : connectionList.getConnectionList())
            {
                if (x.getConnectionId() == connectionId)
                {
                    SIALinkEthernetImpl y = (SIALinkEthernetImpl) this.theLinks.getItem((int) x.getLinkId());

                    return (y.isConnected());
                }
            }
        }

        return (retVal);
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * Used to tell SIA to go to PERSISTENT_CONNECTION state.
     *
     * @param connectionId the ID number of the connection
     */
    @Override
    public void setPersistent(long connectionId)
    {
        connectionList.getItem(connectionId).setConnType(
                SIAConnectionTypeEnum.PERSISTENT_CONNECTION);
    }

    /**
     * Used to tell SIA to go to TRANSIENT_CONNECTION state.
     *
     * @param connectionId the ID number of the connection
     */
    @Override
    public void setTransient(long connectionId)
    {
        connectionList.getItem(connectionId).setConnType(SIAConnectionTypeEnum.TRANSIENT_CONNECTION);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Returns a list of the available links that can be used for communications
     * with a host.
     *
     * @return SIALinkList containing a list of the links available on the
     * sensor
     */
    @Override
    public SIALinkList getAvailableLinks()
    {
        return (this.theLinks);
    }

    /**
     * Returns a list of the available connections that can be used for
     * communications with a host.
     *
     * @return SIAConnectionList containing a list of the connections available
     * on the sensor
     */
    @Override
    public SIAConnectionList getActiveConnections()
    {
        return (connectionList);
    }

    /**
     * Returns an indication of whether or not the link is registered on the
     * network.
     *
     * @param linkId the ID number of the link to be tested
     *
     * @return boolean indicating if the link is registered (true) or not
     * (false)
     *
     * @throws IllegalArgumentException if the index is out of range or does not
     * identify an available link
     */
    @Override
    public boolean isRegistered(int linkId) throws IllegalArgumentException
    {
        return (true);
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Sends an acknowledgement for the MSN on the connection.
     *
     * @param connectionId the ID number of the connection
     * @param channel the channel for the ACK
     * @param msn the MSN of the message being acknowledged
     *
     * @return - the message was sent successfully
     */
    @Override
    public boolean sendAck(long connectionId, SaCcsiChannelEnum channel, long msn)
    {
        boolean retVal = false;
        
        System.out.println("SIA Interface.sendAck for channel:  " + channel + "  and MSN:  " + msn);
        System.out.println();
        
        if (connectionId >= 1L)
        {
            for (SIAConnection x : connectionList.getConnectionList())
            {
                if (x.getConnectionId() == connectionId)
                {
                    String msg = saMsgBuilder.makeAckMessage(x, channel, msn);
                    SIALinkEthernetImpl y = (SIALinkEthernetImpl) this.theLinks.getItem(x.getLinkId());
                    PendingSendItem psi = new PendingSendItem();

                    psi.sentAckId = msn;

                    SaCcsiChannelEnum[] channs = new SaCcsiChannelEnum[1];

                    channs[0] = channel;
                    psi.sentChannels = channs;
                    this.pendingSends.addEntry(psi);
                    retVal = y.send(msg, connectionId, psi.itemId);
                }
            }
        }

        return (retVal);
    }

    /**
     * Sends a specified negative acknowledgment for the MSN on the connection
     * with optional parameters to fill in a detailed status report if the host
     * is registered for the STATUS channel and
     *
     * @param connectionId the ID number of the connection
     * @param channel the channel for NAK
     * @param msn the MSN of the message being negatively acknowledged
     * @param nakCode the message header NAK code
     * @param nakDetailCode the detailed NAK code of the error if applicable,
     * else null
     * @param nakDetailMessage the detailed NAK message for the error if
     * applicable, else null
     *
     * @return flag indicating successful queuing (true) or not (false)
     */
    @Override
    public boolean sendNak(long connectionId, SaCcsiChannelEnum channel, long msn, SaNakReasonCodeEnum nakCode,
            SaNakDetailCodeEnum nakDetailCode, String nakDetailMessage)
    {
        boolean retVal = false;

        if (connectionId >= 1L)
        {
            for (SIAConnection x : connectionList.getConnectionList())
            {
                if (x.getConnectionId() == connectionId)
                {
                    String msg = saMsgBuilder.makeNakMessage(x, channel, msn, nakCode, nakDetailCode,
                            nakDetailMessage);
                    SIALinkEthernetImpl y = (SIALinkEthernetImpl) this.theLinks.getItem((int) x.getLinkId());
                    PendingSendItem psi = new PendingSendItem();

                    psi.sentAckId = msn;
                    this.pendingSends.addEntry(psi);
                    retVal = y.send(msg, connectionId, psi.itemId);

                    if (this.enforceStrikes)
                    {
                        if (connectionList.getItem(myConnectId).incrementThreeStrikes())
                        {
                            this.disconnect(connectionId);
                        }
                    }
                }
            }
        }

        return (retVal);
    }

    /**
     * Sends a heartbeat message to the host system.
     *
     * @param connectionId the ID number of the connection
     *
     * @return flag indicating successful queuing (true) or not (false)
     */
    @Override
    public boolean sendHrtbt(long connectionId)
    {
        boolean retVal = false;
System.out.println("SEND HEARTBEAT METHOD in SIA INTERFACE.");
        if (connectionId >= 1L)
        {
            for (SIAConnection x : connectionList.getConnectionList())
            {
                if (x.getConnectionId() == connectionId)
                {
                    String msg = saMsgBuilder.makeHrtbtMessage(x);
                    SIALinkEthernetImpl y = (SIALinkEthernetImpl) this.theLinks.getItem((int) x.getLinkId());
                    PendingSendItem psi = new PendingSendItem();
                    SaCcsiChannelEnum[] chans = new SaCcsiChannelEnum[1];

                    chans[0] = SaCcsiChannelEnum.HRTBT;
                    psi.sentChannels = chans;

                    this.pendingSends.addEntry(psi);
                    retVal = y.send(msg, connectionId, psi.itemId);
                }
            }
        }

        return (retVal);
    }

    /**
     * Sends a report message to the host system. The string provided must
     * contain a complete CCSI
     * message to be transmitted as
     * defined in the CCSI schemas.
     *
     * @param connectionId the ID number of the connection
     * @param channel the channels of the message
     * @param cacheKeys the cache key(s) for the report(s) being sent
     * @param msg the message to be transmitted
     * @param ack the type of ack/nak or null if not used
     * @param ackMsn the MSN to be acknowledged
     * @param nakCode the NAK code
     *
     * @return flag indicating successful queuing (true) or not (false)
     */
    @Override
    public boolean sendReport(long connectionId, SaCcsiChannelEnum[] channel, String[] cacheKeys, String msg,
            SaCcsiAckNakEnum ack, long ackMsn, SaNakReasonCodeEnum nakCode)
    {
        boolean retVal = false;

        if (connectionId >= 1L)
        {
            for (SIAConnection x : connectionList.getConnectionList())
            {
                if (x.getConnectionId() == connectionId)
                {
                    String fullMsg = saMsgBuilder.makeReportMessage(msg, channel, ack, ackMsn, nakCode);
                    SIALinkEthernetImpl y = (SIALinkEthernetImpl) this.theLinks.getItem((int) x.getLinkId());
                    PendingSendItem psi = new PendingSendItem();

                    psi.cacheKeys = cacheKeys;
                    psi.sentChannels = channel;

                    if (ack != null)
                    {
                        psi.sentAckId = ackMsn;
                    }

                    this.pendingSends.addEntry(psi);
                    retVal = y.send(fullMsg, connectionId, psi.itemId);
                }
            }
        }

        return (retVal);
    }

    /**
     * Handler for IPCQueue interface events.
     *
     * @param evt the IPCQueue event
     */
    @Override
    public void queueEventFired(IPCQueueEvent evt)
    {
        try { 
        
            if (evt.getEventSource() instanceof SaLinkEventQueue)
            {
                long theLinkId = 0L;
                SIALinkEvent linkEvt = this.myLinkEventQ.readFirst(0);

                if (linkEvt != null)
                {
                    System.out.println((char)27 + "[31m" + "SIAInterfaceEthernetImpl.queueEventFired:  Link Event:  " + (char)27 + "[31m" + linkEvt.getEventType());
//                    System.out.println();
                    
                    theLinkId = linkEvt.getLinkId();
                    this.myLinkEvents.addLast(linkEvt);
                }
            } else if (evt.getEventSource() instanceof CtlWrapperQueue)
            {
                CtlWrapper ctlEvt = this.controlQueue.readFirst(0);

                if (ctlEvt != null)
                {
                    System.out.println((char)27 + "[31m" + "SIAInterfaceEthernetImpl.queueEventFired:  Ctl Wrapper:  " + (char)27 + "[31m" + ctlEvt.getUuid());
                    System.out.println();
                    
                    if (ctlEvt.getCtlCommand() == CtlWrapper.STOP_THREAD)
                    {
                        this.shutDownInterface();
                        this.shutDown = true;
                    } else
                    {
                    }
                }
            //Added per Tom:
            } else if (evt.getEventSource() instanceof SIAXferEventQueue)
            {
                SIATransferEvent xfrEvt = this.siaTransferEventQueue.readFirst(0);

                if (xfrEvt != null)
                {
                    System.out.println((char)27 + "[31m" + "SIAInterfaceEthernetImpl.queueEventFired:  TRANSFER EVENT:  " + (char)27 + "[31m" + xfrEvt.getXferType());
                    System.out.println();    
                    
                    this.spaTransferEventQueue.insertLast(xfrEvt);

                }
            //Added per Tom:
            } else if (evt.getEventSource() instanceof SaLinkEventQueue)
            {
                
                    SIALinkEvent sale = this.myLinkEventQ.readFirst(0);
                    
                    switch (sale.getEventType()) {
                    
                        case LINK_RECEIVED: 
                            System.out.println((char)27 + "[31m" + "SIAInterfaceEthernetImpl.queueEventFired:    get Send MSN:  " + (char)27 + "[31m" + sale.getSentMsn());
                            System.out.println();
                            break;

                        default:
                            break;
                                   
                    }
                    
            } else if (evt.getEventType() == IPCQueueEventType.IPC_NEW)
            {
                  
//THIS IS NOT CORRECT!!!!   WE NEED TO POP IT OFF THE QUEUE........UNRESOLVED!!!!                
//THIS IS NOT CORRECT!!!!   WE NEED TO POP IT OFF THE QUEUE........UNRESOLVED!!!!                
//THIS IS NOT CORRECT!!!!   WE NEED TO POP IT OFF THE QUEUE........UNRESOLVED!!!!                
//THIS IS NOT CORRECT!!!!   WE NEED TO POP IT OFF THE QUEUE........UNRESOLVED!!!!                
                IPCWrapper wrapper = (IPCWrapper)evt.getPayload();
  

                //SEND REGISTRATION MESSAGE
                //SEND REGISTRATION MESSAGE
                //SEND REGISTRATION MESSAGE
                if ((wrapper != null) && (wrapper.getMsgBody() != null) &&
                        (wrapper.getMsgBody().getCmd() != null) && (wrapper.getMsgBody().getCmd().getCmd() != null) &&
                        (wrapper.getMsgBody().getCmd().getCmd().contains("Register")))
                {
                    System.out.println((char)27 + "[31m" + "SIAInterfaceEthernetImpl.queueEventFired -> GOT IPC WRAPPER -> " + (char)27 + "[31m" + wrapper.getMsgBody().getCmd().getCmd());   
                    System.out.println();
                    //SEND REGISTRATION TO SENSOR.
                    int registerLength = wrapper.getMsgBody().getCmd().getCmd().length();
                    System.out.println((char)27 + "[31m" + "SIAInterfaceEthernetImpl.queueEventFired -> SENDING REGISTRATION:   SIAInterface, about to put on SIALink.");
                    System.out.println();
                    HeaderMacroProcessor hmp = new HeaderMacroProcessor(sensorNetworkSettings.getUuid(), registerLength, "N", "c");
                    String middle = hmp.getTranslation(hmp.headerMiddle);
                    String finalRegistrationXML = hmp.headerStart + middle + wrapper.getMsgBody().getCmd().getCmd();

                    PendingSendItem psi = new PendingSendItem();                         
                    this.pendingSends.addEntry(psi);
                    SIALink siaLink = this.theLinks.getItem(connectionList.getItem(myConnectId).getLinkId());
                    siaLink.send(finalRegistrationXML, myConnectId, psi.itemId);  
                } else {

                    if ((wrapper != null) && (wrapper.getMsgBody() != null) &&
                        (wrapper.getMsgBody().getCmd() != null) && (wrapper.getMsgBody().getCmd().getCmd() != null))
                    {
                        System.out.println((char)27 + "[31m" + "SIAInterfaceEthernetImpl.queueEventFired:    MSG BODY:  " + (char)27 + "[31m" + wrapper.getMsgBody().getCmd().getCmd());
                        System.out.println();
                    } else
                    {
                        try {
                            System.out.println((char)27 + "[31m" + "SIAInterfaceEthernetImpl.queueEventFired:    RAW MSG:  " + (char)27 + "[31m" + wrapper.getRawMsg());
                            System.out.println();
                        } catch(Exception e) {
                            System.out.println((char)27 + "[31m" + "SIAInterfaceEthernetImpl.queueEventFired -> CAUGHT EXCEPTION WHEN PRINTING RAW MSG.");
                        }
                    }
//WHAT DO I DO HERE???!?!?!?!?!?                  
//WHAT DO I DO HERE???!?!?!?!?!?                  
//WHAT DO I DO HERE???!?!?!?!?!?                  
//WHAT DO I DO HERE???!?!?!?!?!?                  
//siAdapterToSp.insertLast(wrapper);



                }
            } else       
            {
                myLogger.error("Unexpected queue event received.");
                System.out.println((char)27 + "[31m" + "SIAInterfaceEthernetImpl.queueEventFired:  Unexpected queue event received.");
                System.out.println();                
            }
        } catch(Exception ex) {
          
            System.out.println((char)27 + "[31m" + "EXCEPTION in SIAInterfaceEthernetImpl.queueEventFired!!!!");
            System.out.println();
            ex.printStackTrace();
            System.out.println();
        }
    }

    
    
    
    
//     public void write(String msg) {          //byte[] message) {
////        System.out.println("ETHERNET_ADAPTER:  WRITE.");
////        System.out.println();
//
//System.out.println("size = " + theLinks.size());
//    SIALink siaLink = theLinks.getItem(0);
//    if (siaLink.isConnected())
//    {
//        siaLink.send(msg, 0,0);      //myConnectId, myConnectId)
//
//    }
//        try {
//            if (message!=null) {
////                System.out.println("EthernetAdapter.write:  ");  // + message.toString());
////                System.out.println();
//            }
//            if (outputStream != null)
//            {
////                System.out.println("EthernetAdapter.write -> About to WRITE to outputStream:  ");// + message.toString());
////                System.out.println();
//                outputStream.write(message);
//            } else {
////                System.out.println("EthernetAdapter.write:   Attempting to Re-Connect...");
////                System.out.println();
////                connect ();
//                myLogger.error("Attempting to reconnect");
//            }
////            System.out.println ("EthernetAdapter.write:  bytes written: " + message.length);
//        } catch (IOException ex) {
//            System.out.println("EXCEPTION in ETHERNET_ADAPTER.WRITE!!!");
//            System.out.println();
// 
//            myLogger.error("IO Exception writing to ethernet, attempting to reconnect");
//            try {
//                Thread.sleep(10000);
//            } catch (InterruptedException ex1) {
//
//            }
////            connect();
//        }
//    }
   
    
    
    
    
    
    
    
    
    /**
     * Thread execution point
     */
    @Override
    public void run()
    {
        boolean processing = true;

        myLogger.info("Initiated SIA IF Ethernet thread");

        while ((processing) && (!this.shutDown))
        {
            try
            {
                SIALinkEvent e = null;

                e = myLinkEvents.pollFirst(10, TimeUnit.SECONDS);

                if (e != null)
                {
                    System.out.println((char)27 + "[34m" + "SIA InterfaceEthImpl.run:    e = " + e.getEventType() );
                    System.out.println();
                    SIAConnectionTypeEnum connType = connectionList.getItem(myConnectId).getConnType();
                    PendingSendItem psi = null;

                    switch (e.getEventType())
                    {
                        case LINK_SEND_OK:
                            long item = e.getItemId();

                            if (item >= 0L)
                            {
                                psi = this.pendingSends.find(item);
                            }
                            if (psi != null)
                            {
//HERE HERE HERE HERE HERE                                
                                if (connType == SIAConnectionTypeEnum.NO_CONNECTION)
                                {
                                    if (psi.isOIConnStartMsg)
                                    {
                                        System.out.println((char)27 + "[34m" + "<<<<<<<<<< SIA InterfaceEthernetImpl.run RECEIPT of OICONNECT.");
                                        System.out.println();
//for DP, I don't think this should exist.                                       
//                                        if (sensorNetworkSettings.isOiServer())
//                                        {
//                                            connectionList.getItem(myConnectId).setConnType(
//                                                    SIAConnectionTypeEnum.TRANSIENT_CONNECTION);
//                                        }
                                    }
                                } else
                                {
                                    if (psi.sentAckId >= 0L)
                                    {
                                        SIATransferEvent sentAck = new SIATransferEvent(SIATransferEventEnum.XFER_SENT_ACK);

                                        sentAck.setEventStatus(true);
                                        sentAck.setAckMsn(psi.sentAckId);
                                        sentAck.setChannels(psi.sentChannels);
                                        sentAck.setConnectionId(myConnectId);
                                        sentAck.setSentMsn(e.getSentMsn());
                                        this.siaTransferEventQueue.insertLast(sentAck);

                                        // RESET THREE XXX's
                                        if (this.enforceStrikes)
                                        {
                                            connectionList.getItem(myConnectId).resetStrikes();
                                        }
                                    }

                                    if ((psi.sentChannels != null) && (psi.sentChannels.length == 1)
                                            && (psi.sentChannels[0] == SaCcsiChannelEnum.HRTBT))
                                    {
                                        // SEND HEARTBEAT EVENT.
                                        SIATransferEvent sentHrtbt = new SIATransferEvent(SIATransferEventEnum.XFER_SENT_HRTBT);
System.out.println("SEND HEARTBEAT...");
System.out.println();
                                        sentHrtbt.setEventStatus(true);
                                        sentHrtbt.setAckFlag(false);
                                        sentHrtbt.setSentMsn(e.getSentMsn());
                                        sentHrtbt.setConnectionId(myConnectId);
                                        this.siaTransferEventQueue.insertLast(sentHrtbt);
                                    } else if ((psi.sentChannels != null) && (psi.sentChannels.length > 1))
                                    {
                                        // SEND REPORT EVENT.
                                        SIATransferEvent sentRpt = new SIATransferEvent(SIATransferEventEnum.XFER_SENT_REPORT);

                                        sentRpt.setEventStatus(true);
                                        sentRpt.setAckFlag(false);
                                        sentRpt.setSentMsn(e.getSentMsn());
                                        sentRpt.setConnectionId(myConnectId);
                                        sentRpt.setCacheKeys(psi.cacheKeys);
                                        sentRpt.setChannels(psi.sentChannels);
                                        this.siaTransferEventQueue.insertLast(sentRpt);
                                    } else if ((psi.sentChannels != null) && (psi.sentChannels.length == 1))
                                    {
                                        if ((psi.sentChannels.length == 1)
                                                && ((psi.sentChannels[0] != SaCcsiChannelEnum.HRTBT)
                                                && (psi.sentChannels[0] != SaCcsiChannelEnum.CMD)))
                                        {
                                            // SEND REPORT EVENT.
                                            SIATransferEvent sentRpt = 
                                                    new SIATransferEvent(SIATransferEventEnum.XFER_SENT_REPORT);

                                            sentRpt.setEventStatus(true);
                                            sentRpt.setSentMsn(e.getSentMsn());
                                            sentRpt.setCacheKeys(psi.cacheKeys);
                                            sentRpt.setChannels(psi.sentChannels);
                                            sentRpt.setConnectionId(myConnectId);
                                            this.siaTransferEventQueue.insertLast(sentRpt);
                                        }
                                    }
                                }    // end if have a connection - next test we have no connection
                            }      // end if we found a pending send entry

                            break;

                        case LINK_CONNECTED:
                            myLogger.info("Got LINK_CONNECTED message");
                            if (connectionList.getItem(myConnectId).getConnType()
                                    == SIAConnectionTypeEnum.NO_CONNECTION)
                            {
                                if (!sensorNetworkSettings.isOiServer())
                                {
                                    this.waitingForConnStart = true;
                                } else
                                {
                                    this.waitingForConnStart = true;

                                    //THIS IS FOR THE OICONNECTION MESSAGE TO THE SENSOR.
                                    SIALink siaLink
                                            = this.theLinks.getItem(connectionList.getItem(myConnectId).getLinkId());
                                    String connStartMsg = this.createInitialStartMsg();
//                                    String connStartMsg = this.createReturnStartMsg();

                                    psi = new PendingSendItem();
                                    psi.isOIConnStartMsg = true;
                                    this.pendingSends.addEntry(psi);
                                    siaLink.send(connStartMsg, myConnectId, psi.itemId);
                                    System.out.println((char)27 + "[34m" + ">>>>>>>>>>  OICONNECT SENT.  (SIA InterfaceEthernetImpl.run)");
                                    System.out.println();
                                }
                            }

                            break;

                        case LINK_DISCONNECTED:
                            if (connectionList.getItem(myConnectId).getConnType()
                                    != SIAConnectionTypeEnum.PERSISTENT_CONNECTION)
                            {
                                connectionList.getItem(myConnectId).setConnType(
                                        SIAConnectionTypeEnum.NO_CONNECTION);

                                SIAConnEvent connLostEvt = new SIAConnEvent(SIAConnEventEnum.CONN_TERMINATED);

                                connLostEvt.setConnId(this.myConnectId);
                                this.connEventQueue.insertLast(connLostEvt);
                            }

                            break;
//WE RECEIED A MESSAGE HERE!!!
//WE RECEIED A MESSAGE HERE!!!
//WE RECEIED A MESSAGE HERE!!!
                        case LINK_RECEIVED:    // MSG REC'D....
                            String msg = e.getLinkBuffer();
                            myLogger.info("Got LINK_RECEIVED message: " + msg);
                            System.out.println((char)27 + "[34m" + "SIAInterfaceEthernetImpl.run -> Got LINK_RECEIVED message: " + msg);
                            System.out.println();

                            if (!msg.isEmpty())
                            {
                                System.out.println((char)27 + "[34m" + "SIAInterfaceEthernetImpl.run -> ABOUT TO VALIDATE AND ROUTE:  " + msg);
                                System.out.println();                                
                                this.validateAndRoute(msg);
                            }

                            break;

                        default:
                            break;
                    }
                }
            } catch (Exception ex)
            {
                myLogger.error("Thread exiting due to exception.", ex);
            }
        }

        myLogger.info("Thread exit");
    }

    /**
     * Validate a received SID
     *
     *
     * @param sid
     * @param conn flag indicating this is a connection message
     *
     * @return valid or not valid in this context
     */
    private boolean validateSid(String sid, boolean conn)
    {
        boolean retVal = false;

        if ((sensorNetworkSettings.getSensorID() != null)
                && (sensorNetworkSettings.getSensorID().trim().length() > 0)
                && (sid.equalsIgnoreCase(sensorNetworkSettings.getSensorID())))
        {
            if (logLevelDebug)
            {
                myLogger.debug ("SID " + sid + " IS GOOD!");
            }
            retVal = true;
        } else if (sid.equalsIgnoreCase(sensorNetworkSettings.getUuid()))
        {
            if (logLevelDebug)
            {
                myLogger.debug ("SID is set to UUID: " + sid);
            }
            retVal = true;
        } else if ((sid.equalsIgnoreCase(sensorNetworkSettings.getDiscoverySID())) && conn)
        {
            if (logLevelDebug)
            {
                myLogger.debug ("SID is set to DISCOVERY: " + sid);
            }
            retVal = true;
        } else
        {
            myLogger.error ("SID \"" + sid + "\" IS NOT VALID!!!" +
                    " Must be one of Host ID, Sensor UUID or Discovery UUID");
        }

        return retVal;
    }

    
    
    
    private String createInitialStartMsg()
    {
        String temp;

        temp = sensorNetworkSettings.getDiscoverySID();
        
        String retStr = saMsgParser.getMsgConnection() + "sid=\"" + temp + "\" type=\"start\"/>";

        myLogger.info("start msg = " + retStr);

        return retStr;
    }
        
        
    /**
     * Creates a start message to reply to the host
     *
     * @return the message
     */
    private String createReturnStartMsg()
    {
        String temp;

        temp = sensorNetworkSettings.getSensorIDorUUID();
        
        String retStr = saMsgParser.getMsgConnection() + "sid=\"" + temp + "\" type=\"start\"/>";

        myLogger.info("start msg = " + retStr);

        return retStr;
    }

    /**
     * Creates an open interface end message
     *
     * @return the message
     */
    private String createEndMsg()
    {
        String temp = sensorNetworkSettings.getSensorIDorUUID();
        String sendStr = saMsgParser.getMsgConnection() + "sid=\"" + temp + "\" type=\"end\"/>";

        return sendStr;
    }

    /**
     * Validates the message length field
     *
     * @param str the message
     *
     * @return valid or not valid
     */
    private boolean validMsgLen(String str)
    {
        boolean retVal = false;
        Matcher lenPattern = saMsgParser.getLenPattern().matcher(str);
        int indx = str.indexOf("len=");
        int slashBracketIndx = str.indexOf("/>", indx);
        int slashIndx = str.indexOf(">", indx);
        int i = 0;
        int lengthStr = str.length();

        if (lenPattern.find())
        {
            String len = lenPattern.group(1);
            int lenInt = Integer.parseInt(len);

            if ((lenInt == 0) && (slashBracketIndx > indx))
            {
                return true;
            }

            if (slashIndx > 0)
            {
                i = slashIndx + 1;

                int msgLen = lengthStr - i;

                if (msgLen == lenInt)
                {
                    return true;
                }
            }
        }

        return retVal;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Gets the message length field
     *
     * @param str the message
     *
     * @return the length value
     */
    private int getMsgLen(String str)
    {
        int lenInt = 0;
        Matcher lenPattern = saMsgParser.getLenPattern().matcher(str);

        if (lenPattern.find())
        {
            String len = lenPattern.group(1);

            lenInt = Integer.parseInt(len);
        }

        return lenInt;
    }

    /**
     * Get the channels in the header or return null if no valid channels were
     * found
     *
     * @param msg the message to be parsed
     *
     * @return channels or null
     */
    private String getValidChannel(String msg)
    {
        String chans = saMsgParser.getChannels(msg);

        if (chans != null)
        {
            return (chans);
        } else
        {
            myLogger.error("Missing or invalid channels.");
        }

        return null;
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Validates the received DTG
     *
     * @param str the message
     *
     * @return true = valid or false = not valid
     */
    private boolean validDTG(String str)
    {
        boolean retVal = false;
        Matcher dtgPattern = saMsgParser.getDtgPattern().matcher(str);
        long currentTime = System.currentTimeMillis() / 1000;

        if (sensorNetworkSettings.getEnforceTime() == false)
        {
            retVal = true;
        } else
        {
            if (logLevelDebug)
            {
                myLogger.debug ("current time = " + currentTime);
            }
            if ((str != null) && (str.trim().length() > 0))
            {
                if (dtgPattern.find())
                {
                    String dtgStr = dtgPattern.group(1);
                    long dtg = Long.parseLong(dtgStr);

                    if (logLevelDebug)
                    {
                        myLogger.debug ("parsed dtg = " + dtg);
                    }

                    long temp = currentTime - dtg;

                    if (Math.abs(temp) <= sensorNetworkSettings.getEnforceTimeValue())
                    {
                        retVal = true;
                    }
                }
            }
        }
        return retVal;
    }

    /**
     * Validates the ACK field
     *
     * @param str the message
     *
     * @return valid or not valid
     */
    private boolean validAck(String str)
    {
        boolean retVal = false;
        Matcher mAck = saMsgParser.getAckPattern().matcher(str);

        if (mAck.find())
        {
            String ack = mAck.group(1);

            if (ack.matches("ACK"))
            {
                retVal = true;
            }
        }

        return retVal;
    }

    /**
     * Validates the received MSN
     *
     * @param str the message
     *
     * @return valid or not valid
     */
    private boolean validMSN(String str)
    {
        boolean validMsn = false;
        boolean reTx = false;
//        long msnNum = -1L;

        long msnNum = saMsgParser.getMsn(str);
        if (logLevelDebug)
        {
            myLogger.debug ("MSN of msg: " + msnNum);
        }
        String sFlag = saMsgParser.getFlags(str);

        if (sFlag != null)
        {
            if (sFlag.contains("R"))
            {
                reTx = true;
            }
        }

        boolean resync = saMsgParser.isResetMsgSeq(str);
        if (logLevelDebug)
        {
            myLogger.debug ("Validating with: " + msnNum + " reTx: " + reTx + " resync: " + resync);
        }

        validMsn
                = connectionList.getItem(myConnectId).getConnectionMsns().validateReceivedMsn(msnNum, reTx,
                        resync);

        return validMsn;
    }

    /**
     * Returns an array of enumeration values for the channels listed in the
     * header
     *
     * @param x the channel string
     *
     * @return array of CcsiChannelEnum's
     */
    private SaCcsiChannelEnum[] makeEnumArray(String x)
    {
        SaCcsiChannelEnum[] retVal;

        if ((x != null) && (!x.isEmpty()))
        {
            retVal = new SaCcsiChannelEnum[x.length()];

            for (int i = 0; i < x.length(); i++)
            {
                retVal[i] = SaCcsiChannelEnum.value(x.charAt(i));
            }
        } else
        {
            retVal = new SaCcsiChannelEnum[1];
            retVal[0] = SaCcsiChannelEnum.CMD;
        }

        return (retVal);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Gets the SID from the header
     *
     * @return the SID
     */
    private String getSensorSid()
    {
        String retVal = sensorNetworkSettings.getSensorIDorUUID();
        return (retVal);
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Validates a received message and routes queue entries to the PH
     *
     * @param fullMsg the message
     */
    private void validateAndRoute(String fullMsg)
    {
        boolean isDiscovery = false;    // Is this a discovery start connect
        long msgMsn = -1L;      // The MSN
        SaCcsiChannelEnum channels[] = null;     // The parsed channel result
        SIAConnection myConnection = connectionList.getItem(this.myConnectId);
        String msgType = null;     // The type of message as conn or msg
        long ackMsn = -1L;      // The ack/nak MSN
        SaNakReasonCodeEnum hdrNakCode = null;     // The NAK reason
        String hdrSid = null;     // The received SID
        String connType = null;     // The connection type
        String msgDtg = "";
        String sChannels = "";
        String sFlags = "";
        String nakCode = "";
        boolean msnError = false;
        boolean dtgError = false;
        boolean lenError = false;

//        System.out.println("SIA.validateAndRoute:  start....");
        // What kind do we have?
        msgType = saMsgParser.getHdrConnType(fullMsg);

        if (msgType != null)
        {
            hdrSid = saMsgParser.getSid(fullMsg);

            if (hdrSid != null)
            {
                if (msgType.charAt(0) == 'H')
                {
                    // This is a CCSI message
                    if (this.validateSid(hdrSid, false)) // Valid SID?
                    {
                        // MESSAGE HDR VALIDATION:
                        // msglen
                        // channels
                        // flg
                        // dtg
                        // msn     //need flag reTx
                        // ack
                        // ackmsn
                        // nakcode
                        // and ROUTE    Gen. xfer  recv  Events for Cmd, Ack, Hrtbt
                        // 3 strikes:
                        // SID, MSN, DTG, LEN   ==  NOT VALID = STRIKE
                        // if strike (MSN, DTG, LEN) , NAK message based upon NakCodes (5 in all)
                        if (this.validMsgLen(fullMsg))
                        {                                                // get Msg Len.
                            int msgLen = this.getMsgLen(fullMsg);

                            if (logLevelDebug)
                            {
                                myLogger.debug ("The length is " + msgLen);
                            }
                        } else
                        {
                            lenError = true;
                        }

                        if (this.validDTG(fullMsg))
                        {
                            msgDtg = saMsgParser.getDtg(fullMsg);
                        } else
                        {
                            dtgError = true;
                        }

                        sChannels = saMsgParser.getChannels(fullMsg);    // get Channels
                        sFlags = saMsgParser.getFlags(fullMsg);       // get Flags.

                        if (this.validMSN(fullMsg))
                        {                                                // get MSN.
                            msgMsn = saMsgParser.getMsn(fullMsg);
                        } else
                        {
                            msnError = true;
                            // Maybe need to increment MSN
                        }

                        if ((lenError) || (dtgError) || (msnError) || (sChannels == null))
                        {
                            // if strike (MSN, DTG, LEN, CHAN) , NAK message based upon NakCodes (5 in all):
                            if (this.enforceStrikes)
                            {
                                connectionList.getItem(myConnectId).incrementThreeStrikes();
                            }

                            ackMsn = saMsgParser.getAckMsn(fullMsg);

                            if (lenError)
                            {
                                hdrNakCode = SaNakReasonCodeEnum.LENMSM;
                            } else if (dtgError)
                            {
                                hdrNakCode = SaNakReasonCodeEnum.TIMOUT;
                            } else if (msnError)
                            {
                                hdrNakCode = SaNakReasonCodeEnum.MSGMSN;
                            } else
                            {
                                hdrNakCode = SaNakReasonCodeEnum.FORMAT;
                            }

                            String baseChans = this.getValidChannel(fullMsg);

                            if ((baseChans == null) || (baseChans.isEmpty()))
                            {
                                baseChans = saMsgParser.getRawChannels(fullMsg);

                                if ((baseChans == null) || (baseChans.isEmpty()))
                                {
                                    baseChans = "c";
                                }
                            }

                            SaCcsiChannelEnum[] enumChans = new SaCcsiChannelEnum[baseChans.length()];

                            for (int i = 0; i < enumChans.length; i++)
                            {
                                SaCcsiChannelEnum val = SaCcsiChannelEnum.value(baseChans.charAt(i));

                                if (val != null)
                                {
                                    enumChans[i] = val;
                                }
                            }

                            if (enumChans[0] == null)
                            {
                                enumChans[0] = SaCcsiChannelEnum.CMD;
                            }

                            // SEND NAK:
                            this.sendNak(myConnectId, enumChans[0], msgMsn, hdrNakCode, null, null);
                        } else
                        {
                            // GOOD MESSAGE!!!  RESET THREE XXX's
                            if (this.enforceStrikes)
                            {
                                connectionList.getItem(myConnectId).resetStrikes();
                            }

                            sChannels = this.getValidChannel(fullMsg);

                            SaCcsiChannelEnum[] eChannels;

                            if ((sChannels == null) || (sChannels.isEmpty()))
                            {
                                sChannels = "c";
                            }

                            eChannels = this.makeEnumArray(sChannels);

                            // GOOD MESSAGE!!!    ROUTE TO CORRECT:  Gen. xfer  recv  Events for Cmd, Ack, Hrtbt
                            if (this.validAck(fullMsg))
                            {
                                //ACK
                                SIATransferEvent recvAckEvt = new SIATransferEvent(SIATransferEventEnum.XFER_RCV_ACK);
//THIS IS WHERE THE ACK COMES IN AND HANDLED.    ie, the ACK from the initial REGISTER CMD.
//SEEMS TO SEND AN ACK BACK TO THE TESTSENSOR.
                                recvAckEvt.setConnectionId(this.myConnectId);
                                recvAckEvt.setSentMsn(msgMsn);
                                recvAckEvt.setChannels(eChannels);

                                SaCcsiAckNakEnum which = this.saMsgParser.getAck(fullMsg);

                                recvAckEvt.setAckFlag(true);
                                recvAckEvt.setEventStatus(which == SaCcsiAckNakEnum.ACK);
                                ackMsn = this.saMsgParser.getAckMsn(fullMsg);
                                recvAckEvt.setAckMsn(ackMsn);
                                nakCode = this.saMsgParser.getNakCode(fullMsg);
                                recvAckEvt.setNakCode(nakCode);
                                System.out.println((char)27 + "[36m" + "SIAInterfaceEthernetImpl.validateAndRoute:   we RECEIVED an ACK for MSN:  " + recvAckEvt.getAckMsn());
                                System.out.println();
                                this.siaTransferEventQueue.insertLast(recvAckEvt);
                            }
                            //HEARTBEAT
                            if (sChannels.contains("h"))
                            {
                                SIATransferEvent recvHrtbtEvt = new SIATransferEvent(SIATransferEventEnum.XFER_RCV_HRTBT);
System.out.println((char)27 + "[36m" + "SIAInterfaceEthernetImpl.validateAndRoute:   HEARTBEAT RECEIVED!!!");
System.out.println();
                                //Tom said to use these settings.
                                recvHrtbtEvt.setConnectionId(this.myConnectId);
                                recvHrtbtEvt.setRecvdMsn(msgMsn);   
                                recvHrtbtEvt.setSentMsn(-1);
                                long dtg = Long.parseLong(msgDtg);
                                recvHrtbtEvt.setRcvdDtg(dtg);
                                SaCcsiChannelEnum[] hChan = new SaCcsiChannelEnum[1];
                                hChan[0] = SaCcsiChannelEnum.HRTBT;
                                recvHrtbtEvt.setChannels(hChan);
                                recvHrtbtEvt.setEventStatus(true);
                                this.siaTransferEventQueue.insertLast(recvHrtbtEvt);
                            //COMMAND    
                            } else if (sChannels.contains("c"))
                            {
                                SIATransferEvent recvCmdEvt = new SIATransferEvent(SIATransferEventEnum.XFER_RCV_CMD);
//TOM SAYS, WE ARE SENDING A COMMAND OR PARSING A RECEIVED CMD.
                                recvCmdEvt.setConnectionId(this.myConnectId);
                                recvCmdEvt.setRecvdMsn(msgMsn);
                                recvCmdEvt.setFlags(sFlags);
                                recvCmdEvt.setAckFlag(true);        //false);                  //SET TO WHAT EVER VALUE IS IN recvCmdEvt
                                recvCmdEvt.setChannels(eChannels);
                                recvCmdEvt.setEventStatus(true);

                                long dtg = Long.parseLong(msgDtg);

                                recvCmdEvt.setRcvdDtg(dtg / 1000);
                                recvCmdEvt.setRecvdBody(fullMsg);
//SHOULD THIS MESSAGE GO TO THE SPA??????????                                
//SHOULD THIS MESSAGE GO TO THE SPA??????????                                
//SHOULD THIS MESSAGE GO TO THE SPA??????????    
//THIS IS THE ACK from the INITIAL REGISTER CMD.    THIS IS THE XML MSG BODY as a Header.

//USED TO GO TO TRANSFER EVEN QUEUE, BUT TRYING TO FORWARD TO SPA.
                                IPCWrapper wrap = new IPCWrapper();
                                MsgContent msgContent = new MsgContent();
                                CommandMsg cm = new CommandMsg();
                                cm.setCmd(recvCmdEvt.getRecvdBody());
                                msgContent.setCmd(cm);
                                wrap.setMsgBody(msgContent);
                                this.siAdapterToSp.insertLast(wrap);
                                
//                                this.transferEventQueue.insertLast(recvCmdEvt);
//                                myLogger.debug ("Sent CMD Event to PH.");
                                
                                
                            //IDENT
                            } else if (sChannels.contains("i"))
                            {
//WE HAVE THE GET_IDENTIFICATION MESSAGE FROM THE SIA.   SENDING TO THE SPA.
                                System.out.println((char)27 + "[36m" + "<<<<<<<<<<  WE FOUND A GET_IDENTICATION MSG!!!   back from TestSensor!  (SIA InterfaceEthernetImpl.validateAndRoute) ");   // + fullMsg.trim());
                                System.out.println();
                                IPCWrapper ipcw = new IPCWrapper();
                                ipcw.setRoutingKey("ccsi.cmd.Get_Identification");
                                ipcw.setXMLCmd(fullMsg.trim());
                                this.siAdapterToSp.insertFirst(ipcw);

                                
                            } else
                            {    
                                if (logLevelDebug)
                                {
                                    myLogger.debug ("Received " + sChannels);
                                }
                            }
                        }
                    } else
                    {
                        if (this.enforceStrikes)
                        {
                            connectionList.getItem(myConnectId).incrementThreeStrikes();
                        }

                        myLogger.error("Error:  Drop message, SID not valid.");
                    }
                } else
                {
                    // get the date and time from the connect message and use it to set the system date and time
                    String connectTime = saMsgParser.getConnectTime(fullMsg);
                    if (connectTime != null)
                    {
                        SystemDateTime systemDateTime = new SystemDateTime();
                        try
                        {
                            systemDateTime.setSystemDateTime(connectTime);
                        } catch (IOException | ParseException ex)
                        {
                            myLogger.error("Exception setting date and time", ex);
                        }
                    }
                    // This is a connection message
                    connType = saMsgParser.getOIConnType(fullMsg);

                    if (connType != null)
                    {
                        if (connType.charAt(0) == 's')
                        {
                            boolean isSidValid = false;

                            if (this.waitingForConnStart)
                            {
                                isSidValid = this.validateSid(hdrSid, true);
                            } else
                            {
                                isSidValid = this.validateSid(hdrSid, false);
                            }

                            if (isSidValid)
                            {
                                isDiscovery = hdrSid.contentEquals(sensorNetworkSettings.getDiscoverySID());

                                if (isDiscovery)
                                {
                                    hdrSid = this.getSensorSid();
                                }

                                if (hdrSid != null)
                                {
                                    

//                                    if ((myConnection.getConnType() == SIAConnectionTypeEnum.TRANSIENT_CONNECTION) || 
                                      if     ((myConnection.getConnType() == SIAConnectionTypeEnum.NO_CONNECTION)
                                            && (this.waitingForConnStart))
                                    {
//                                        if (sensorNetworkSettings.isOiServer())
                                        if (!sensorNetworkSettings.isOiServer())
                                        {
                                            myLogger.debug ("Sending OI start message reply");
                                            String startStr = this.createReturnStartMsg();
                                            PendingSendItem psi = new PendingSendItem();

                                            psi.isOIConnStartMsg = true;
                                            this.pendingSends.addEntry(psi);

                                            SIALink aLink = this.theLinks.getItem(myConnection.getLinkId());
                                            boolean retVal = aLink.send(startStr, myConnectId, psi.itemId);
System.out.println("@");
                                            myLogger.debug ("SEND STATUS: " + retVal);

                                            if (retVal)
                                            {
                                                // Initialize MSNs.
                                                connectionList.getItem(myConnectId).getConnectionMsns().initializeMsns();
                                                connectionList.getItem(myConnectId).setConnType(
                                                        SIAConnectionTypeEnum.TRANSIENT_CONNECTION);
                                            }

                                            this.waitingForConnStart = false;
                                        } 
                                        else
                                        {                                              
                                            //Actually, Connection is ALREADY set to TRANSIENT
                                            // As Client:   go to Transient and Initialize MSNs.
                                            //send the SID to theContext.
                                            //initiate send of GetIdentification.
                                            connectionList.getItem(myConnectId).setConnType(
                                                    SIAConnectionTypeEnum.TRANSIENT_CONNECTION);
                                            connectionList.getItem(myConnectId).getConnectionMsns().initializeMsns();
                                            sensorNetworkSettings.setUuid(hdrSid);

//SEND INITIAL GET_INFORMATION MSG TO TESTSENSOR.                                            
                                            GetIdentificationProcessor gip = new GetIdentificationProcessor();
                                            HeaderMacroProcessor hmp = new HeaderMacroProcessor(hdrSid, gip.getLength(), "N", "c");
                                            
                                            String translation = hmp.getTranslation(hmp.headerMiddle);
                                            String cmdGetInformation = hmp.headerStart + translation + gip.getGetIdentificationMessage();
                                            //System.out.println("FULL MESSAGE = " + cmdGetInformation);
                                     
                                            SIALink siaLink = this.theLinks.getItem(connectionList.getItem(myConnectId).getLinkId());
                                            PendingSendItem psi = new PendingSendItem(); 
                                            this.pendingSends.addEntry(psi);
                                            
                                            System.out.println((char)27 + "[36m" + ">>>>>>>>>>  SEND GET_IDENTIFICATION MSG.   (SIA InterfaceEthernetImpl.validateAndRoute)");
                                            System.out.println();
                                            
                                            siaLink.send(cmdGetInformation, myConnectId, psi.itemId);
                                            
                                            
                                            
                                        }
                                    } else
                                    {
                                        myLogger.error("Error:  Received 2nd Connect Msg.");
                                        System.out.println((char)27 + "[36m" + "SIAInterfaceEthernetImpl.validateAndRoute -> Error:  Received 2nd Connect Msg.");
                                    }
                                }
                            } else
                            {
                                // If Start Message and BAD SID, disconnect.
                                if (this.enforceStrikes)
                                {
                                    connectionList.getItem(myConnectId).incrementThreeStrikes();
                                }

                                myLogger.error("Error:  Drop Start Connection message, SID not valid.");
                                this.disconnect(myConnectId);
                            }
                        } else
                        {
                            if (this.validateSid(hdrSid, false))
                            {
                                myLogger.debug ("Sending reply to OI end message");
                                String endStr = this.createEndMsg();
                                PendingSendItem psi = new PendingSendItem();
System.out.println("$");
                                psi.isOIConnEndMsg = true;
                                this.pendingSends.addEntry(psi);

                                SIALink aLink = this.theLinks.getItem(myConnection.getLinkId());
                                boolean retVal = aLink.send(endStr, myConnectId, psi.itemId);
                                connEventQueue.insertLast(
                                        new SIAConnEvent(SIAConnEventEnum.CONN_TERMINATED, true, myConnectId));
                                myLogger.debug ("SEND STATUS: " + retVal);
                                connectionList.getItem(myConnectId).setConnType(
                                        SIAConnectionTypeEnum.NO_CONNECTION);
                                this.disconnect(myConnectId);
                            }
                        }
                    } else
                    {
                        myLogger.error("Error:   Drop Msg, connection type is NULL.");
                    }
                }
            } else
            {
                myLogger.error("Error: SID is blank, must be one of Host ID, Sensor UUID or Discovery UUID");
            }
        } else
        {
            myLogger.error("Error:   Ignore Msg, msg type is NULL.");
        }
    }

    /**
     * Starts the execution of this thread
     *
     * @param x
     */
    @Override
    public void doRun(Executor x)
    {
        x.execute(this);
    }

    //~--- inner classes --------------------------------------------------------
    /**
     * Container for message waiting to be sent
     */
    class PendingSendItem
    {

        /**
         * The cache keys
         */
        String[] cacheKeys = null;

        /**
         * The ID of this pending send item
         */
        long itemId = -1L;

        /**
         * The ACK MSN sent
         */
        long sentAckId = -1L;    // TCS needs to be initialized correctly

        /**
         * The channels that were sent
         */
        SaCcsiChannelEnum[] sentChannels = null;

        /**
         * This is an Open Interface start message
         */
        boolean isOIConnStartMsg = false;

        /**
         * This is an Open Interface end message
         */
        boolean isOIConnEndMsg = false;

        //~--- constructors -------------------------------------------------------
        /**
         * Constructs an item instance
         */
        public PendingSendItem()
        {
            itemId = ITEM_ID_GENERATOR.getAndIncrement();

            if (itemId == Long.MAX_VALUE)
            {
                ITEM_ID_GENERATOR.set(0L);
            }
        }
    }

    /**
     * A list to hold pending send entries
     */
    class PendingSendItemList
    {

        /**
         * The entry map indexed by ID
         */
        private final HashMap<Long, PendingSendItem> pendingList = new HashMap<>();

        //~--- constructors -------------------------------------------------------
        /**
         * Constructs a list instance
         */
        public PendingSendItemList()
        {
            pendingList.clear();
        }

        //~--- methods ------------------------------------------------------------
        /**
         * Adds an entry to the list
         *
         * @param x the item to be added
         *
         * @return added (true) or not added (false)
         */
        public boolean addEntry(PendingSendItem x)
        {
            boolean retVal = false;
            PendingSendItem v = this.pendingList.put(Long.valueOf(x.itemId), x);

            if (v == null)
            {
                retVal = true;
            }

            return (retVal);
        }

        //~--- get methods --------------------------------------------------------
        /**
         * Get the length of the list
         *
         * @return list length
         */
        public int getSize()
        {
            return (this.pendingList.size());
        }

        //~--- methods ------------------------------------------------------------
        /**
         * Get the item by its ID
         *
         * @param itemId the item's ID
         *
         * @return the item or null if not in the list
         */
        public PendingSendItem find(long itemId)
        {
            Long testId = itemId;

            if (this.pendingList.containsKey(testId))
            {
                return (this.pendingList.get(testId));
            }

            return (null);
        }

        /**
         * Removes an entry from the list
         *
         * @param itemId the ID of the item to remove
         */
        public void remove(long itemId)
        {
            Long testId = itemId;

            this.pendingList.remove(testId);
        }
    }
}
