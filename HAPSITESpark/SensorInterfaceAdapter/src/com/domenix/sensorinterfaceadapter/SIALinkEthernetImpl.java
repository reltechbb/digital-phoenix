/*
 * SIALinkEthernetImpl.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains a specialized SIA link for Ethernet based communications.
 *
 * Version: V1.0  15/08/02
 */
package com.domenix.sensorinterfaceadapter;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.commonsa.interfaces.SaLinkEventQueue;
import com.domenix.commonsa.interfaces.SIAConnection;
import com.domenix.commonsa.interfaces.SIAConnectionTypeEnum;
import com.domenix.commonsa.interfaces.SIAInterfaceTypeEnum;
import com.domenix.commonsa.interfaces.SIALinkEvent;
import com.domenix.commonsa.interfaces.SIALinkEventTypeEnum;
import com.domenix.common.spark.IPCQueueEvent;
import com.domenix.common.spark.IPCQueueListenerInterface;
import com.domenix.common.utils.TimerChangeEvent;
import com.domenix.common.utils.TimerPurposeEnum;
import com.domenix.common.utils.UtilTimer;
import com.domenix.common.utils.UtilTimerListener;
import com.domenix.commonsa.interfaces.SIAConnectionList;
import com.domenix.commonsa.interfaces.SAQueues;
import com.domenix.commonsa.interfaces.SaCcsiMsnType;
import com.domenix.commonsa.interfaces.SaMsgParserUtilities;
import com.domenix.commonsa.interfaces.SensorNetworkSettings;

//import com.domenix.commonsa.interfaces.SIALinkStateCmdEnum;


import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------
import java.io.IOException;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.StandardSocketOptions;

import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

import javax.security.cert.X509Certificate;

//~--- classes ----------------------------------------------------------------
/**
 * This class extends the SIALink type for Ethernet TCP/IP interfaces.
 *
 * @author kmiller
 */
public class SIALinkEthernetImpl extends com.domenix.commonsa.interfaces.SIALink
        implements Runnable, IPCQueueListenerInterface, UtilTimerListener
{

    /**
     * The timeout wait in milli seconds
     */
    private static final int DEFAULT_CONN_RETRY_TIMEOUT = 300;

    /**
     * Field description
     */
    private static final int MAX_CONNECT_ATTEMPTS = 3;

    /**
     * UTF-8 character set
     */
    private static final Charset utf8 = Charset.forName("UTF-8");

    //~--- fields ---------------------------------------------------------------
    /**
     * The R/W buffer size
     */
    private final int BUFFER_SIZE = (32 * 1024);

    /**
     * Field description
     */
    private int connectAttempts = 0;

    /**
     * Field description
     */
    private String connectedHostAddress = "";

    /**
     * The active connection ID number
     */
    private long connectionId = 0L;

    /**
     * Current send entry
     */
    private SendListEntry currentSending = null;

    /**
     * Field description
     */
    private String myHostIpString = "";

    /**
     * Open Interface connect retry timeout (seconds).
     */
    private int oiConnRetryTimeout = SIALinkEthernetImpl.DEFAULT_CONN_RETRY_TIMEOUT;

    /**
     * Field description
     */
    private boolean needToWrite = false;

    /**
     * Field description
     */
    private boolean sensorConnected = false;

    /**
     * The network broadcast IP address
     */
    private InetAddress broadcastIpAddress;

    /**
     * Field description
     */
    private SocketChannel client;

    /**
     * The network default gateway address
     */
    private InetAddress gatewayAddress;

    /**
     * The host IP address
     */
    private InetAddress hostIpAddress;

    /**
     * The host port number
     */
    private int hostIpPort;

    /**
     * The link internal state queue
     */
    private final SaLinkStateQueue internalLinkState;

    /**
     * The link event queue
     */
    private SaLinkEventQueue linkEventQueue;

    /**
     * The sensor's local IP address for this Ethernet link.
     */
    private InetAddress localIpAddress;

    /**
     * Field description
     */
    private SaMsgParserUtilities msgParser;

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger;
    /* the log level < debug*/
    private boolean logLevelDebug;

    /**
     * The sensor PKI certificates associated
     */
    private X509Certificate pkiCert;

    /**
     * Secure communications enabled on this link
     */
    private boolean secureComms;

    /**
     * The selector for the server socket
     */
    private Selector selector;

    /**
     * A list of messages to be sent
     */
    private final LinkedBlockingQueue<SendListEntry> sendList;

    /**
     * Our socket channel to the host
     */
    private SocketChannel sockChan;

    /**
     * The selection key for the client socket
     */
    private SelectionKey sockKey;

    /**
     * The selection key for the server socket
     */
    private SelectionKey svrKey;

    /**
     * The server socket channel
     */
    private ServerSocketChannel svrSock;

    /**
     * The host's address
     */
//    private InetSocketAddress theHost;
    private InetSocketAddress theSensor;

    /**
     * The sensor configuration
     */
    private SensorNetworkSettings sensorNetworkSettings;    // Singleton

    SIAConnectionList connectionList;
    /**
     * The base timer utility
     */
    private UtilTimer ut;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructor providing an interface type that default to the TCP/IP Open
     * Interface. If the secure communications flag is set later the type will
     * be changed to TCP/IP secure.
     *
     * @param cfg
     * @param theList
     * @param queues
     * @param timerBase
     */
    public SIALinkEthernetImpl(SensorNetworkSettings cfg, SIAConnectionList theList,
            SAQueues queues, UtilTimer timerBase)
    {
        super();
        super.setInterfaceType(SIAInterfaceTypeEnum.INTFC_TCPIP_OI);
        connectionList = theList;
        this.linkEventQueue = queues.getSIALinkEventQueue();  
        this.sensorNetworkSettings = cfg;
        
        myLogger = Logger.getLogger(SIALinkEthernetImpl.class);
        logLevelDebug = myLogger.isDebugEnabled();
        
        this.sendList = new LinkedBlockingQueue<>(10);
        this.internalLinkState = new SaLinkStateQueue();

        this.theSensor = new InetSocketAddress(sensorNetworkSettings.getIP(), sensorNetworkSettings.getPort());
        msgParser = new SaMsgParserUtilities("UNCLASS");    //cfg.getClassification());
        ut = timerBase;

//        try
//        {
////KBM:   8/3/20 -- we need to get Socket Connection somewhere else.... 
////            if (!this.sensorNetworkSettings.isOiServer())
////            {
////                //KBM   changed from Server to Client   7/29/2020
////                //this next line does get a successful Socket Connection with TestSensor.
////                this.openClientSocket();                //createServerSocket();        
////            }
//        } catch (Exception ex)
//        {
//            myLogger.fatal("Could not create the client socket IP: " + 
//                    sensorNetworkSettings.getIP() + " Port: " + sensorNetworkSettings.getPort(), ex);
//            this.sensorConnected = false;
//        }

        int connRetryTime = this.sensorNetworkSettings.getConnRetryTime();    //from Host side

        if (connRetryTime <= 0)
        {
            connRetryTime = 30;
        }

        this.sensorConnected = false;
        this.myHostIpString = "";
        myLogger.info("Constructed...");
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Interrupt this thread
     */
    public void shutDown()
    {
        this.internalLinkState.insertFirst(new LinkStateQueueEntry(SIALinkStateCmdEnum.TERMINATE));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the localIpAddress
     */
    public InetAddress getLocalIpAddress()
    {
        return localIpAddress;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param localIpAddress the localIpAddress to set
     */
    public void setLocalIpAddress(InetAddress localIpAddress)
    {
        this.localIpAddress = localIpAddress;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the broadcastIpAddress
     */
    public InetAddress getBroadcastIpAddress()
    {
        return broadcastIpAddress;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param broadcastIpAddress the broadcastIpAddress to set
     */
    public void setBroadcastIpAddress(InetAddress broadcastIpAddress)
    {
        this.broadcastIpAddress = broadcastIpAddress;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the gatewayAddress
     */
    public InetAddress getGatewayAddress()
    {
        return gatewayAddress;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param gatewayAddress the gatewayAddress to set
     */
    public void setGatewayAddress(InetAddress gatewayAddress)
    {
        this.gatewayAddress = gatewayAddress;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the secureComms
     */
    public boolean isSecureComms()
    {
        return secureComms;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param secureComms the secureComms to set
     */
    public void setSecureComms(boolean secureComms)
    {
        this.secureComms = secureComms;

        if (secureComms)
        {
            super.setInterfaceType(SIAInterfaceTypeEnum.INTFC_TCPIP_SEC);
        } else
        {
            super.setInterfaceType(SIAInterfaceTypeEnum.INTFC_TCPIP_OI);
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pkiCert
     */
    public X509Certificate getPkiCert()
    {
        return pkiCert;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pkiCert the pkiCert to set
     */
    public void setPkiCert(X509Certificate pkiCert)
    {
        this.pkiCert = pkiCert;
    }

    //~--- methods --------------------------------------------------------------
    /**
     * This method initiates the connection on the link.
     *
     * @param connectId the connection ID for the connection
     *
     * @return an indication of success (true) or failure (false)
     */
    @Override
    public boolean connect(long connectId)
    {
        boolean retVal = false;

        if ((connectId >= 0L) && (getConnList().getItem(connectId) != null))
        {
            this.connectionId = connectId;
            this.internalLinkState.insertLast(new LinkStateQueueEntry(SIALinkStateCmdEnum.CONNECT_HOST, connectId));
            retVal = true;
        }

        return (retVal);
    }

    /**
     * This method terminates the connection on the link.
     *
     * @param connectId the connection ID for the disconnect
     *
     * @return an indication of success (true) or failure (false)
     */
    @Override
    public boolean disconnect(long connectId)
    {
        boolean retVal = false;

        if ((connectId >= 1L) && (getConnList().getItem(connectId) != null))
        {
            this.internalLinkState.insertLast(new LinkStateQueueEntry(SIALinkStateCmdEnum.DISCONNECT_HOST, connectId));
            retVal = true;
        }

        return (retVal);
    }

    
    
    
    /**
     * This method sends a message to the host system on this link.
     *
     * @param msg String containing the message to be sent
     * @param connectId the ID number of this connection
     * @param itemId
     *
     * @return flag indicating success (true) or failure (false)
     */
    @Override
    public synchronized boolean send(String msg, long connectId, long itemId)
    {
        if ((getConnList().getItem(connectId).getConnType()
                == SIAConnectionTypeEnum.PERSISTENT_CONNECTION) && (!this.sensorConnected))
        {
            myLogger.info("PERSISTENT conn and host is NOT Connected......CONNECTING FIRST.....");
            this.openClientSocket();
        }

        if ((msg != null) && (!msg.trim().isEmpty()) && (connectId >= 1L))
        {
            if (getConnList().getItem(connectId) != null)
            {
                SendListEntry sle = new SendListEntry(msg, connectId, itemId);
                boolean ret = this.sendList.add(sle);

                this.needToWrite = true;

                return (ret);
            } else
            {
                return (false);
            }
        } else
        {
            myLogger.info("Cannot send message.");
            return (false);
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the hostIpAddress
     */
    public InetAddress getHostIpAddress()
    {
        return hostIpAddress;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param hostIpAddress the hostIpAddress to set
     */
    public void setHostIpAddress(InetAddress hostIpAddress)
    {
        this.hostIpAddress = hostIpAddress;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the hostIpPort
     */
    public int getHostIpPort()
    {
        return hostIpPort;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param hostIpPort the hostIpPort to set
     */
    public void setHostIpPort(int hostIpPort)
    {
        this.hostIpPort = hostIpPort;
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Method description
     *
     */
    private void readyConnection()
    {
        if (!this.sensorNetworkSettings.isOiServer())      //ServerOIServer is FALSE.
        {
            myLogger.info("Initiating connection as a Server...");

            this.enableServerSocket();
        } else
        {
            myLogger.info("Initiating connection as a Client...");

            this.openClientSocket();
        }
    }

    /**
     * Thread execution point
     */
    @Override
    public void run()
    {
        Object syncObject = new Object ();
//        String emptyString = "";
        ByteBuffer theBuffer = ByteBuffer.allocate(this.BUFFER_SIZE);
        boolean doMore = true;
        int readByteCount = 0;
        long timeoutWait = this.oiConnRetryTimeout;

        myLogger.info("Running...");

        try
        {
            while ((doMore) && (!Thread.currentThread().isInterrupted()))
            {
                //
                //  Check for a received message
                //
                if ((this.selector != null) && (this.selector.select(timeoutWait) == 0))
                {
                    if (readByteCount > 0)
                    {
                        theBuffer.flip();

                        String theMessage = utf8.newDecoder().decode(theBuffer).toString();

//                        System.out.println((char)27 + "[33m" + "SIA LINK.run -> read.   theBuffer:  " + theMessage);
//                        System.out.println();
                        
                        theBuffer.clear();
                        this.sockKey.attach(null);
                        readByteCount = 0;

                        SIALinkEvent linkEvent = new SIALinkEvent(SIALinkEventTypeEnum.LINK_RECEIVED, true);

                        //SEND TO THE SIAInterfaceEthernetImpl (propogate upward)                        
                        linkEvent.setLinkBuffer(theMessage);    // Package the Message (string) into Link Event to send to Logical Layer.
                        linkEvent.setLinkId(this.getLinkId());    // our Link Id, so Logial Layer knows who it's from.
                        this.linkEventQueue.insertLast(linkEvent);    // Tell Logical Layer we rec'd a msg!
                    }
                }

                //
                //  Process internal link events
                //
                if (this.internalLinkState.size() > 0)
                {
                    LinkStateQueueEntry ent = this.internalLinkState.readFirst(1L);

                    if (ent != null)
                    {
                        myLogger.info("Received entry " + ent.getLinkCmdType().name());
//                        System.out.println((char)27 + "[33m" + "SIALink.run -> Received entry " + ent.getLinkCmdType().name());
//                        System.out.println((char)27 + "[33m" + "SIALink.run -> Received entry (internalLinkState) " + ent.getLinkCmdType());
//                        System.out.println();
                        
                        // Process OUR (Link Layer) Events.....
                        switch (ent.getLinkCmdType())
                        {
                            case TERMINATE:
                                doMore = false;
                                this.closeConnections();
                                getConnList().getItem(connectionId).setConnType(
                                        SIAConnectionTypeEnum.NO_CONNECTION);

                                break;

                            case CONNECT_HOST:    // we have an order to CONNECT TO HOST.
                                if (getConnList().getItem(connectionId).getConnType()
                                        == SIAConnectionTypeEnum.NO_CONNECTION)
                                {
                                    this.readyConnection();
                                } else if (getConnList().getItem(connectionId).getConnType()
                                        == SIAConnectionTypeEnum.TRANSIENT_CONNECTION)
                                {
                                    System.out.println((char)27 + "[33m" + "ERROR!  Can't re-connect to Host in TRANSIENT.");
                                    this.myLogger.error("ERROR!  Can't re-connect to Host in TRANSIENT.");
                                    this.internalLinkState.insertLast(new LinkStateQueueEntry(SIALinkStateCmdEnum.CONN_FAILED,
                                            this.connectionId));
                                }

                                break;

                            case CLOSE_SOCKET:
                                this.closeClientSocket();

                                if (getConnList().getItem(connectionId).getConnType()
                                        != SIAConnectionTypeEnum.PERSISTENT_CONNECTION)
                                {
                                    getConnList().getItem(connectionId).setConnType(
                                            SIAConnectionTypeEnum.NO_CONNECTION);

                                    if (this.sensorNetworkSettings.isOiServer())
                                    {
                                        this.enableServerSocket();
                                    }
                                } else
                                {
                                    this.enableServerSocket();
                                }

                                myLogger.info(" Close Socket.....Disconnect from Host.");

                                break;

                            case DISCONNECT_HOST:
                                this.closeClientSocket();

                                if (getConnList().getItem(connectionId).getConnType()
                                        == SIAConnectionTypeEnum.PERSISTENT_CONNECTION)
                                {
                                    this.enableServerSocket();
                                } else if (this.sensorNetworkSettings.isOiServer())
                                {
                                    this.enableServerSocket();
                                } else
                                {
                                    getConnList().getItem(connectionId).setConnType(
                                            SIAConnectionTypeEnum.NO_CONNECTION);
                                }

                                myLogger.info(" Disconnect from Host.");

                                break;

                            case OPEN_SOCKET:
                                myLogger.info(" Open Socket Event.");
                                this.openClientSocket();

                                break;

                            case CONN_FAILED:
                                this.closeConnections();
                                getConnList().getItem(connectionId).setConnType(
                                        SIAConnectionTypeEnum.NO_CONNECTION);
                                myLogger.info(" CONN FAILED Event: Close all Sockets!");

                                break;

                            case CONN_SUCCESS:
                                if (getConnList().getItem(connectionId).getConnType()
                                        == SIAConnectionTypeEnum.NO_CONNECTION)
                                {
                                    getConnList().getItem(connectionId).setConnType(
                                            SIAConnectionTypeEnum.TRANSIENT_CONNECTION);
                                }

                                break;

                            case SOCKET_CLOSED:
                                myLogger.info(" Socket Closed Event.");

                                break;

                            case SOCKET_OPENED:
                                myLogger.info(" Socket Opened Event.");

                                break;

                            case SENSOR_DISCONNECTED:
                                if (getConnList().getItem(this.connectionId) != null)
                                {
                                    if (getConnList().getItem(connectionId).getConnType()
                                            != SIAConnectionTypeEnum.PERSISTENT_CONNECTION)
                                    {
                                        SaCcsiMsnType msnGenerator = getConnList().getItem(connectionId).getConnectionMsns();
                                        msnGenerator.initializeMsns();
                                    }
                                }

                                myLogger.info(" Sensor Disconnected Event.");
                                this.internalLinkState.insertLast(new LinkStateQueueEntry(SIALinkStateCmdEnum.CONNECT_HOST,
                                        connectionId));

                                break;

                            case NONE:
                                System.out.println((char)27 + "[33m" + "SIA Link.run:  No Event:  " + ent.getLinkCmdType());
                                System.out.println();
                                myLogger.info(" No Event.");

                                break;

                            default:
                                break;
                        }
                    }
                } else if (selector != null)
                {
                    try
                    {
                    if (needToWrite)
                    {
                        needToWrite = false;
                        this.sockKey.interestOps(this.sockKey.interestOps() | SelectionKey.OP_WRITE);
                    }

                    // Something is ready
                    Iterator<SelectionKey> keyIter = selector.selectedKeys().iterator();

                    while (keyIter.hasNext())
                    {
                        SelectionKey key = keyIter.next();

                        if ((key.isAcceptable()) && (key.isValid()))
                        {
                            client = null;

                            try
                            {
                                client = this.handleAccept(key);
                            } catch (Exception accEx)
                            {
                                myLogger.error("Exception during accept operation.", accEx);
                            }

                            if (client != null)
                            {
                                SocketAddress xxx = client.getRemoteAddress();

                                this.connectedHostAddress = ((InetSocketAddress) xxx).getHostString();
                                this.sockChan = client;

                                if (this.sendList.isEmpty())
                                {
                                    this.sockKey = sockChan.register(this.selector, SelectionKey.OP_READ);
                                } else
                                {
                                    this.sockKey = sockChan.register(this.selector, SelectionKey.OP_WRITE);
                                }
                            } else
                            {
                                myLogger.info("Did not accept the connection.");
                            }
                        } else if ((key.isConnectable()) && (key.isValid()))
                        {
                            try
                            {
                                this.sockChan = this.handleConnect(key);
                            } catch (Exception connEx)
                            {
                                myLogger.error("Exception during connection operations.", connEx);

                                if (this.sockChan != null)
                                {
                                    this.sockChan.close();
                                    this.sockChan = null;
                                }
                            }

                            if (this.sockChan == null)
                            {
                                myLogger.error("Error connecting client socket to sensor.");
                            } else
                            {
                                if (this.sendList.isEmpty())
                                {
                                    this.sockKey = sockChan.register(this.selector, SelectionKey.OP_READ);
                                } else
                                {
                                    this.sockKey = sockChan.register(this.selector, SelectionKey.OP_WRITE);
                                }
                            }
                        } else if ((key.isReadable()) && (key.isValid()))
                        {
                            try
                            {
                                readByteCount = this.handleRead(key, theBuffer);
                            } catch (Exception readEx)
                            {
                                myLogger.error("Exception during read operation.", readEx);
                                readByteCount = -1;
                            }

                            if (readByteCount == -1)
                            {
                                myLogger.info("Socket closed during read.");

                                if (getConnList().getItem(connectionId).getConnType()
                                        == SIAConnectionTypeEnum.PERSISTENT_CONNECTION)
                                {
                                    this.internalLinkState.insertLast(
                                            new LinkStateQueueEntry(SIALinkStateCmdEnum.SENSOR_DISCONNECTED));
                                }
                                this.internalLinkState.insertLast(new LinkStateQueueEntry(SIALinkStateCmdEnum.CLOSE_SOCKET));
                                myLogger.info("Closing out the socket.");
                            }
                        } else if ((key.isWritable()) && (key.isValid()))
                        {
                            try
                            {
                                this.handleWrite(key);
                            } catch (Exception writEx)
                            {
                                myLogger.error("Exception during write operation.", writEx);

                                if ((key.isValid()) && (key.isWritable()))
                                {
                                    key.interestOps(0);
                                }
                            }
                        }

                        keyIter.remove();
                    }
                    }
                    catch (CancelledKeyException ex)
                    {
                        // happens when a connection has been closed and an attempt to access the selector occurs
                        myLogger.error("Cancelled Key Exception.", ex);
                    }
                } else
                {
                    try
                    {
                        synchronized (syncObject)
                        {
                            syncObject.wait(2000L);
                        }
                    } catch (InterruptedException intrEx)
                    {
                        myLogger.error("Interrupted Exception", intrEx);
                    }
                }
            }
        } catch (Exception ex)
        {
            myLogger.error("Exception in main loop", ex);
            System.out.println((char)27 + "[33m" + "SIALink.run:    Exception in main loop");
            ex.printStackTrace();
            System.out.println();
        } finally
        {
            // close out all sockets, selectors, and keys
            try
            {
                myLogger.error("Main loop shutting down.");

                if (selector != null)
                {
                    selector.close();
                }

                if (sockKey != null)
                {
                    sockKey.cancel();
                    sockKey = null;
                }

                if (svrKey != null)
                {
                    svrKey.cancel();
                    svrKey = null;
                }

                if (svrSock != null)
                {
                    svrSock.close();
                    svrSock = null;
                }

                if (sockChan != null)
                {
                    sockChan.close();
                    sockChan = null;
                }

            } catch (Exception e)
            {
                myLogger.error("Error closing sockets in this.closeConnections!", e);
            } finally
            {
                this.sensorConnected = false;
                this.myHostIpString = "";
                this.needToWrite = false;
                this.sendList.clear();                
            }
        }
    }

    /**
     * Handle an accept connection selector trigger indicating that a server
     * socket is ready to accept another connection or has an error pending.
     *
     * @param key selection key for the accept operation
     *
     * @return SocketChannel for the client connection that was accepted or null
     * if error
     *
     * @throws IOException
     */
    private synchronized SocketChannel handleAccept(SelectionKey key) throws IOException
    {
        // The client socket channel
        SocketChannel myclient = ((ServerSocketChannel) key.channel()).accept();

        if ((myclient != null) && (myclient.getRemoteAddress() != null))
        {
            SocketAddress xxx = myclient.getRemoteAddress();

            this.connectedHostAddress = ((InetSocketAddress) xxx).getHostString();
            myclient.configureBlocking(false);

            // The connecting client's network address
            InetAddress clientAddr = ((InetSocketAddress) myclient.getRemoteAddress()).getAddress();
            SIAConnection conn = getConnList().getItem(this.connectionId);

            myLogger.info("Connect from " + clientAddr.getHostAddress());

            switch (conn.getConnType())
            {
                case PERSISTENT_CONNECTION:

                    // See if this is the same sensor
                    if ((clientAddr.getHostAddress().equalsIgnoreCase(sensorNetworkSettings.getIP().getHostAddress()))
                            || (clientAddr.getHostAddress().equalsIgnoreCase(this.myHostIpString)))
                    {
                        // It is the same one, are we already connected?
                        if (this.sockChan != null)
                        {
                            // We are, close the connection
                            myclient.close();
                            myclient = null;
                            this.connectedHostAddress = "";
                            myLogger.error("Connection attempted from host with an existing connection");
                        } else
                        {
                            key.interestOps(SelectionKey.OP_READ);
                            this.sensorConnected = true;
                            myLogger.info("Connection re-established with " + clientAddr.getHostAddress());
                            this.linkEventQueue.insertLast(new SIALinkEvent(true, this.getLinkId(),
                                    SIALinkEventTypeEnum.LINK_CONNECTED));
                        }
                    } else if (this.sockChan != null)
                    {
                        myclient.close();
                        myclient = null;
                        this.connectedHostAddress = "";
                        myLogger.error("Too many hosts, closed socket.");
                    }

                    break;

                case TRANSIENT_CONNECTION:

                    // We're already connected to a sensor for a transient connection, close this one
                    myclient.close();
                    myclient = null;
                    this.connectedHostAddress = "";
                    myLogger.error("Connection attempt from a different sensor while transient connection active.");

                    break;

                default:
//                    System.out.println((char)27 + "[33m" + "NEW Connection from HOST!    SIALinkEthernetImpl.handleAccept");
                    this.disableServerSocket();                          // we need to disable any (additional) ACCEPTS so that
                    myLogger.info("New connection from host " + clientAddr.getHostAddress());
                    this.sensorConnected = true;
                    this.myHostIpString = clientAddr.getHostAddress();
                    this.linkEventQueue.insertLast(new SIALinkEvent(true, this.getLinkId(),
                            SIALinkEventTypeEnum.LINK_CONNECTED));    // tell Logical Layer we are LINK CONNECTED! Yay!

                    break;
            }    // End switch(conn type)
        } else if (myclient != null)
        {
            // The client address was null, close the connection socket
            myclient.close();
            myclient = null;
            this.connectedHostAddress = "";
            myLogger.error("Accepted socket had no client address.");
        }

        return (myclient);
    }

    /**
     * Handle a read ready selector trigger indicating that a channel is ready
     * for reading, has reached the end of stream, or has an error pending.
     *
     * @param key selection key for the read
     * @param buf a byte buffer to use to hold the read bytes
     *
     * @return the number of bytes read or -1 if an exception or socket closure
     * occurred
     * @throws IOException
     */
    private synchronized int handleRead(SelectionKey key, ByteBuffer buf) throws IOException
    {
        int bytesRead = 0;

        if ((key.isValid()) && (key.isReadable()))
        {
            if (key.attachment() == null)
            {
                buf.clear();
                key.attach(buf);
            }

            try
            {
                bytesRead = ((SocketChannel) key.channel()).read(buf);
            } catch (Exception readEx)
            {
                key.interestOps(0);
                key.channel().close();
                myLogger.error("Error during read.", readEx);
                bytesRead = -1;
            }

            if (bytesRead == -1)
            {
                key.attach(null);
                key.interestOps(0);
                key.channel().close();
                this.sensorConnected = false;
                this.myHostIpString = "";

                if (getConnList().getItem(this.connectionId).getConnType()
                        != SIAConnectionTypeEnum.PERSISTENT_CONNECTION)
                {
                    this.sendList.clear();
                    this.needToWrite = false;
                }

                this.linkEventQueue.insertLast(new SIALinkEvent(true, this.getLinkId(),
                        SIALinkEventTypeEnum.LINK_DISCONNECTED));
            }   
        }
        
        String s = new String(buf.array());
//            System.out.println((char)27 + "[33m" + "SIA LINK.handleRead ->  we just read this:  " + s.substring(0, bytesRead));
//            System.out.println();
        
        return (bytesRead);
    }

    /**
     * Handle a write ready selector trigger indicating that a channel is ready
     * for writing, has been closed for writing, or has an error pending.
     *
     * @param key selection key for the write
     *
     * @throws IOException
     */
    private void handleWrite(SelectionKey key) throws IOException
    {
        ByteBuffer msg = null;

        if ((key.isValid()) && (key.isWritable()))
        {
            if (key.attachment() == null)
            {
                if (this.sendList.peek() != null)
                {
                    this.currentSending = this.sendList.poll();    // Read and Remove Send Item.

                    String sendStr = this.currentSending.sendBuffer;

                    // fill in the MSN and DTG
                    if (getConnList().getItem(this.connectionId) != null)
                    {
                        SaCcsiMsnType msnGenerator = getConnList().getItem(connectionId).getConnectionMsns();

                        if (this.currentSending.sendBuffer.contains("{msgMsn}"))
                        {
                            long theMsn = msnGenerator.getNextTransmitMsn();

                            this.currentSending.sentMsn = theMsn;

                            String sMsn = String.valueOf(theMsn);

                            sendStr = sendStr.replace("{msgMsn}", sMsn);
                        }

                        if (this.currentSending.sendBuffer.contains("{msgDtg}"))
                        {
                            long sysTime = System.currentTimeMillis() / 1000;
                            String sTime = String.valueOf(sysTime);

                            sendStr = sendStr.replace("{msgDtg}", sTime);
                        }
                    }

                    myLogger.info("Writing msg: " + sendStr);
//                    System.out.println((char)27 + "[33m" + "Writing msg: " + sendStr);
                    msg = ByteBuffer.wrap(sendStr.getBytes(utf8));
                    key.attach(msg);
                } else
                {
                    key.interestOps(SelectionKey.OP_READ);

                    return;
                }
            } else
            {
                msg = (ByteBuffer) key.attachment();
            }

            ((SocketChannel) key.channel()).write(msg);

            if (!msg.hasRemaining())
            {
                if (logLevelDebug)
                {
                    myLogger.debug("Complete write of message.");
                }
                if (this.sendList.isEmpty())
                {
                    key.interestOps(SelectionKey.OP_READ);
                } else
                {
                    key.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                }

                key.attach(null);

                SIALinkEvent x = new SIALinkEvent(true, this.getLinkId(), SIALinkEventTypeEnum.LINK_SEND_OK);

                x.setItemId(this.currentSending.itemId);
                x.setSentMsn(this.currentSending.sentMsn);
                this.currentSending = null;
                key.attach(null);
                this.linkEventQueue.insertLast(x);
            } else
            {
                msg.compact();
            }
        }
    }

    /**
     * Handle a connect ready selector trigger indicating that a channel is
     * ready to complete its connection sequence or has an error pending.
     *
     * @param key selection key for the connect
     *
     * @return the socket channel for the connection or null if none
     */
    private synchronized SocketChannel handleConnect(SelectionKey key)
    {
        SocketChannel theChan = null;

        if (key.isConnectable())
        {
            theChan = (SocketChannel) key.channel();

            try
            {
                while (!theChan.finishConnect())
                {
                }

                this.sensorConnected = true;

                StringBuilder msg = new StringBuilder("Client socket setup for ");

                msg.append(((InetSocketAddress) theChan.getRemoteAddress()).getHostString());
                myLogger.info(msg.toString());
                this.linkEventQueue.insertLast(new SIALinkEvent(true, this.getLinkId(),
                        SIALinkEventTypeEnum.LINK_CONNECTED));
            } catch (IOException ioEx)
            {
                myLogger.error("Error making connection to host.", ioEx);
                theChan = null;
                this.sensorConnected = false;
                this.myHostIpString = "";

                if (getConnList().getItem(this.connectionId).getConnType()
                        != SIAConnectionTypeEnum.PERSISTENT_CONNECTION)
                {
                    this.sendList.clear();
                    this.needToWrite = false;
                }

                if (this.connectAttempts < this.MAX_CONNECT_ATTEMPTS)
                {
                    long myTimer = ut.setNewTimer(this.oiConnRetryTimeout, false, this, null, "MyLinkTimer",
                            TimerPurposeEnum.CONNRETRY_TIMER);

                    ut.addUtilTimerListener(this, myTimer, TimerChangeEvent.TEST_TIMER_EXPIRED);
                }

                this.connectAttempts++;
            }
        }

        return (theChan);
    }

    /**
     * Handle connect retry timer expiration
     *
     * @param e the event
     */
    @Override
    public void timerExpired(TimerChangeEvent e)
    {
        myLogger.info("Connect Timer Expired!    Retrying connection attempt # " + this.connectAttempts);
        this.openClientSocket();
    }

    /**
     * Open a client socket connection to the sensor.
     */
    private synchronized void openClientSocket()
    {
        int i = 0;

        try
        {
            if ((this.sockChan != null) && (this.sockKey != null))
            {
                this.sockChan.close();
                this.sockKey.cancel();
                this.sockChan = null;
                this.sockKey = null;
            }

            if (this.selector == null)
            {
                this.selector = Selector.open();
            }

            this.sockChan = SocketChannel.open();
            this.sockChan.configureBlocking(false);
            this.sockChan.setOption(StandardSocketOptions.SO_KEEPALIVE, true);
            this.sockChan.setOption(StandardSocketOptions.SO_SNDBUF, 32768);
            this.sockChan.setOption(StandardSocketOptions.SO_RCVBUF, 32768);
            this.sockKey = this.sockChan.register(this.selector, SelectionKey.OP_CONNECT);

            try
            {
                this.sockChan.connect(this.theSensor);
            } catch (Exception e)
            {
                myLogger.error("Exception connecting to sensor", e);
            }
        } catch (Exception ex)
        {
            myLogger.error("Exception opening client socket.", ex);
        }
    }

    /**
     * Create a server socket
     *
     * @throws IOException
     */
    private void createServerSocket() throws IOException
    {
        myLogger.info("Creating server socket.");
        this.svrSock = ServerSocketChannel.open();
        this.svrSock.configureBlocking(false);
        this.svrSock.setOption(StandardSocketOptions.SO_REUSEADDR, true);
        this.svrSock.bind(new InetSocketAddress(this.sensorNetworkSettings.getPHIP(), 
                this.sensorNetworkSettings.getPHPort()), 1);
    }

    /**
     * Disable the server socket
     */
    private void disableServerSocket()
    {
        myLogger.info("Disabling server socket.");

        try
        {
            if (this.svrSock != null)
            {
                if (this.svrKey != null)
                {
                    this.svrKey.interestOps(0);
                }
            }
        } catch (Exception ex)
        {
            myLogger.error("Exception disabling server socket.", ex);
        }
    }

    /**
     * Open a server socket to listen for incoming connections.
     */
    private void enableServerSocket()
    {
        myLogger.info("Enabling Server Socket.");

        try
        {
            if (this.svrSock != null)
            {
                if (this.selector == null)
                {
                    this.selector = Selector.open();
                }

                if (this.svrKey == null)
                {
                    this.svrKey = this.svrSock.register(selector, SelectionKey.OP_ACCEPT);
                } else
                {
                    this.svrKey.interestOps(SelectionKey.OP_ACCEPT);
                }
            } else
            {
                if (this.selector == null)
                {
                    this.selector = Selector.open();
                }

                this.createServerSocket();
                this.svrKey = this.svrSock.register(selector, SelectionKey.OP_ACCEPT);
            }

            myLogger.info("Our Server Sock is: " + svrSock.getLocalAddress().toString());
            myLogger.info("Our Server Key is Acceptable?: " + svrKey.isAcceptable());
            myLogger.info("Our Selector is Open?: " + selector.isOpen());
            myLogger.info("Connectable: " + svrKey.isConnectable());
            myLogger.info("Readable: " + svrKey.isReadable());
            myLogger.info("Writable: " + svrKey.isWritable());
            myLogger.info("Key: " + svrKey.toString());
        } catch (Exception ex)
        {
            myLogger.error("Exception enabling server socket.", ex);
        }
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Handler for a new internal link state queue event
     *
     * @param evt the queue event
     */
    @Override
    public void queueEventFired(IPCQueueEvent evt)
    {
        if (evt.getEventType() == IPCQueueEvent.IPCQueueEventType.IPC_NEW)
        {
            if (logLevelDebug)
            {
                myLogger.debug("Got new link state queue entry.");
            }
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the theSensorConfig
     */
    public SensorNetworkSettings getTheSensorConfig()
    {
        return sensorNetworkSettings;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param sensorNetworkSettings
     */
    public void setTheSensorConfig(SensorNetworkSettings sensorNetworkSettings)
    {
        this.sensorNetworkSettings = sensorNetworkSettings;
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Close all socket related connection information
     */
    public synchronized void closeConnections()
    {
        try
        {
            myLogger.info("Closing all connections.");

            if (selector != null)
            {
                selector.close();
            }

            if (sockKey != null)
            {
                sockKey.cancel();
                sockKey = null;
            }

            if (svrKey != null)
            {
                svrKey.cancel();
                svrKey = null;
            }

            if (svrSock != null)
            {
                svrSock.close();
            }

            svrSock = null;

            if (sockChan != null)
            {
                sockChan.close();
                sockChan = null;
            }

            this.sensorConnected = false;
            this.myHostIpString = "";
            this.sendList.clear();
            this.needToWrite = false;
        } catch (Exception e)
        {
            this.sensorConnected = false;
            this.myHostIpString = "";
            this.sendList.clear();
            this.needToWrite = false;
            myLogger.error("Error closing sockets in this.closeConnections!", e);
        }
    }

    /**
     * Close the client socket
     */
    private synchronized void closeClientSocket()
    {
        try
        {
            sockChan.close();
            this.sensorConnected = false;
            this.myHostIpString = "";

            if (getConnList().getItem(this.connectionId).getConnType()
                    != SIAConnectionTypeEnum.PERSISTENT_CONNECTION)
            {
                this.sendList.clear();
                this.needToWrite = false;
            }

            this.linkEventQueue.insertLast(new SIALinkEvent(true, this.getLinkId(),
                    SIALinkEventTypeEnum.LINK_DISCONNECTED));
        } catch (Exception e)
        {
            this.sensorConnected = false;
            this.myHostIpString = "";

            if (getConnList().getItem(this.connectionId).getConnType()
                    != SIAConnectionTypeEnum.PERSISTENT_CONNECTION)
            {
                this.sendList.clear();
                this.needToWrite = false;
            }

            myLogger.error("Error closing sockets in this.closeSockets!", e);
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the linkEventQueue
     */
    public SaLinkEventQueue getLinkEventQueue()
    {
        return linkEventQueue;
    }

    /**
     * Get the connected host's address
     *
     * @param connectId which connection
     *
     * @return the address
     */
    @Override
    public String getConnectHostAddress(long connectId)
    {
        return (this.connectedHostAddress);
    }

    private SIAConnectionList getConnList ()
    {
        return connectionList;
    }
    
    //~--- inner classes --------------------------------------------------------
    /**
     * Container for a send list buffer entry
     */
    class SendListEntry
    {

        /**
         * The connection to send on
         */
        long connId = 0L;

        /**
         * The logical connection item ID, if it is < 0 then it's internally
         * generated
         */
        long itemId = -1L;

        /**
         * The message to be sent
         */
        String sendBuffer = null;

        /**
         * The MSN that was sent
         */
        long sentMsn = -1L;

        //~--- constructors -------------------------------------------------------
        /**
         * Constructs an entry
         *
         * @param buf the message to send
         * @param id the connection ID
         * @param item
         */
        public SendListEntry(String buf, long id, long item)
        {
            this.connId = id;
            this.sendBuffer = buf;
            this.itemId = item;
        }
    }
}
