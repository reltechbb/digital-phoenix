/*
 * SensorReceiveCallbackInterface.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Callback used by the SerialAdapter to return data to the Sensor Interface Adapter Reader
 *
 */
package com.domenix.sensorinterfaceadapter;

/**
 * Callback used by the SerialAdapter to return data to the Sensor Interface Adapter Reader
 * 
 * @author jmerritt
 */
public interface SensorReceiveCallbackInterface
{
    // for ascii interfaces terminated with a CR LF
    void dataReceived (String message);
    // for binary interfaces, returns a blob of data
    void dataReceived (byte [] message);
}
