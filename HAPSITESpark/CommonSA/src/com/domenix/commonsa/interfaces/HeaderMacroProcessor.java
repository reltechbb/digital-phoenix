/*
 * HeaderMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/11/03
 */
package com.domenix.commonsa.interfaces;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.common.ccsi.MSN;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------
/**
 * This class generates alert report information for readings.
 *
 * @author kmiller
 */
public class HeaderMacroProcessor extends MacroProcessor {

    /**
     * Field description
     */
    
    public final String headerStart = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ccsi:Hdr xmlns:ccsi=\"file:/C:/ccsi/1.1/CCSI\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ";
    
    public final String headerMiddle = "sid=\"{sid}\" msn=\"{msn}\" dtg=\"{dtg}\" chn=\"{chn}\" mod=\"{mod}\" len=\"{len}\">";
    
    public final String headerEnd = "</ccsi:Hdr>";         
 
    /**
     * The sensor configuration
     */
//    private CcsiConfigSensor theConfig;

    /**
     * The saved state
     */
//    private CcsiSavedState theState;
    
    private String channel;
    private String mode;
    private String length;
    private String uuid;
    
    
    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs a class instance
     *
     * @param cfg the sensor configuration
     * @param state the saved state
     */
    public HeaderMacroProcessor(String uuid, int length, String mode, String channel) {      //, String length) {
        super();
//        if (cfg != null) {      // && (state != null)) {
//            this.theConfig = cfg;
//            this.theState = state;
        this.uuid = uuid;
        this.channel = channel; 
        this.mode = mode;
        this.length = String.valueOf(length);
        initialize();    
//        } else {
//            throw new IllegalArgumentException("Missing or null argument.");
//        }
    }

    public void setLength(String length) {
        this.length = length;
    }

    //~--- methods --------------------------------------------------------------
    /**
     * This initializes
     *
     */
    private void initialize() {
        super.textMacros = "\\{(sid|msn|dtg|chn|mod|len)\\}";
        super.macroMap.put("sid", this);
        super.macroMap.put("msn", this);
        super.macroMap.put("dtg", this);
        super.macroMap.put("chn", this);
        super.macroMap.put("mod", this);
        super.macroMap.put("len", this);
    }

    /**
     * This method translates a particular macro into its string value or null if there is no
     * translation.
     *
     * @param macro the macro to be translated
     *
     * @return String containing the replacement value or null if no replacement
     */
    @Override
    public String translateMacro(String macro) {
        String retVal = "";

        switch (macro) {
            case "sid":
                retVal = this.uuid;
                break;

            case "msn":
                retVal = String.valueOf(MSN.getMSN().getNextMSN());
                break;

            case "dtg":
                long time = System.currentTimeMillis();
                retVal = String.valueOf(time/1000);
                break;

            case "chn":
                retVal = this.channel;
                break;

            case "mod":
                retVal = this.mode;
                break;
                
            case "len":
                retVal = this.length;
                break;
                
            default:
                break;
        }

        return (retVal);
    }

//    
//    public String returnVal() {
//    
//    }
    
    
    
    
    
    //~--- get methods ----------------------------------------------------------
    /**
     * This method runs macro processing on the input string and returns the result.
     *
     * @param inString the string to be processed
     *
     * @return String containing the result of macro processing
     */
    @Override
    public String getTranslation(String inString) {
        return (super.getTranslation(inString));
    }
}
