/*
 * NSDSConnection.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the base definition of a CCSI connection which may be extended to support
 * the special needs of specific instances.
 *
 * Version: V1.0  15/08/01
 */


package com.domenix.commonsa.interfaces;

//~--- JDK imports ------------------------------------------------------------

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

//~--- classes ----------------------------------------------------------------

/**
 * This is the base class for NSDS Connections which must be extended by classes implementing
 * specific types of connections.
 *
 * @author kmiller
 */
public class SIAConnection
{
  /** The connection identification ID number generator. */
  private static final AtomicLong	idGenerator;

  //~--- static initializers --------------------------------------------------

  static
  {
    idGenerator	= new AtomicLong( 0L );
  }

  //~--- fields ---------------------------------------------------------------

  /** The ID number of this link for this connections. */
  private int	linkId	= -1;

  /** The MSN pair for this connection. */
  private final SaCcsiMsnType	connectionMsns	= new SaCcsiMsnType();

  /** Is this connection unidirectional? */
  private boolean	unidirectional	= false;
  
  /** The three strikes counter for a connection */
  private final AtomicInteger strikeCounter;

  /** List of connection event listeners. */
//  private final ArrayList<NSDSConnEvent>	connEventListeners	= new ArrayList<>();
  
  /** The channels that are registered on this connection */
  private SaChannelRegistrationList registeredChannels;
  
  /** Is the link for this open or closed */
  private boolean linkOpen = false;

  /** The ID number assigned to this connection. */
  private final long	connectionId;

  /** The connection type */
  private SIAConnectionTypeEnum connType = SIAConnectionTypeEnum.NO_CONNECTION;
  
  /** The host IP address that is connected by OI start */
  private String hostIpString = null;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a connection on the specified link.
   *
   * @param linkId the ID number of the link for the connection.
   */
  public SIAConnection( int linkId )
  {
    this.connectionId	= SIAConnection.idGenerator.incrementAndGet();
    this.linkId				= linkId;
    this.strikeCounter = new AtomicInteger( 0 );
    this.connType     = SIAConnectionTypeEnum.NO_CONNECTION;
    this.registeredChannels = new SaChannelRegistrationList();
  }
  
  /**
   * @return the linkId
   */
  public int getLinkId()
  {
    return linkId;
  }

  /**
   * @return the connectionMsns
   */
  public SaCcsiMsnType getConnectionMsns()
  {
    return connectionMsns;
  }

  /**
   * @return the unidirectional
   */
  public boolean isUnidirectional()
  {
    return unidirectional;
  }

  /**
   * @param unidirectional the unidirectional to set
   */
  public void setUnidirectional(boolean unidirectional)
  {
    this.unidirectional = unidirectional;
  }

  /**
   * @return the connectionId
   */
  public long getConnectionId()
  {
    return connectionId;
  }
  
  public boolean incrementThreeStrikes()
  {
    int temp = this.strikeCounter.getAndIncrement();
    if ( temp >= 3 )
    {
      return( true );
    }
    else
    {
      return( false );
    }
  }
  
  public void resetStrikes()
  {
    this.strikeCounter.set( 0 );
  }

  /**
   * @return the connType
   */
  public SIAConnectionTypeEnum getConnType()
  {
    return connType;
  }

  /**
   * @param connType the connType to set
   */
  public void setConnType(SIAConnectionTypeEnum connType)
  {
    this.connType = connType;
  }

  /**
   * @return the linkOpen
   */
  public boolean isLinkOpen()
  {
    return linkOpen;
  }

  /**
   * @param linkOpen the linkOpen to set
   */
  public void setLinkOpen(boolean linkOpen)
  {
    this.linkOpen = linkOpen;
  }

  /**
   * @return the registeredChannels
   */
  public SaChannelRegistrationList getRegisteredChannels()
  {
    return registeredChannels;
  }

  /**
   * @param registeredChannels the registeredChannels to set
   */
  public void setRegisteredChannels(SaChannelRegistrationList registeredChannels)
  {
    this.registeredChannels = registeredChannels;
  }

  /**
   * @return the hostIpString
   */
  public String getHostIpString()
  {
    return hostIpString;
  }

  /**
   * @param hostIpString the hostIpString to set
   */
  public void setHostIpString(String hostIpString)
  {
    this.hostIpString = hostIpString;
  }
}
