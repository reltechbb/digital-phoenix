/*
 * CcsiMsnType.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains a pair of MSNs for a connection.
 *
 * Version: V1.0  15/08/01
 */


package com.domenix.commonsa.interfaces;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.concurrent.atomic.AtomicLong;

//~--- classes ----------------------------------------------------------------

/**
 * This defines a CCSI message sequence number pair for a connection and provides the means for bound checking,
 * incrementing, and resetting the value.
 *
 * @author kmiller
 */
public class SaCcsiMsnType
{
  /** The maximum valid CCSI MSN value */
  public final long	MAXIMUM_MSN_VALUE	= 4294967295L;

  /** Error/Debug logger */
  private final Logger	myLogger;

  /** Next expected MSN from the sensor. */
  private final AtomicLong	nextReceiveMsn;

  /** Next MSN to the sensor. */
  private final AtomicLong	nextTransmitMsn;

  //~--- constructors ---------------------------------------------------------

  /**
   * Default constructor that initializes the atomic values.
   */
  public SaCcsiMsnType()
  {
    myLogger							= Logger.getLogger(SaCcsiMsnType.class );
    this.nextReceiveMsn		= new AtomicLong( -1L );
    this.nextTransmitMsn	= new AtomicLong( -1L );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This method resets the MSNs to 1
   */
  public synchronized void initializeMsns()
  {
    this.nextReceiveMsn.set( 1L );
    this.nextTransmitMsn.set( 1L );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the next receive MSN and increments/wraps it afterward.  This is not
   * reversible and is only used when the number must be incremented.
   *
   * @return long containing the next expected MSN from the sensor
   */
  public synchronized long getNextReceiveMsn()
  {
    long	retValue	= nextReceiveMsn.get();

    incrementNextReceiveMsn();

    return ( retValue );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Increment the next receive MSN
   */
  public synchronized void incrementNextReceiveMsn()
  {
    long	testMsn	= nextReceiveMsn.incrementAndGet();

    if ( testMsn > MAXIMUM_MSN_VALUE )
    {
      nextReceiveMsn.set( 1L );
    }

    myLogger.debug( "Next RCV set to " + nextTransmitMsn.get() );
  }

  /**
   * Increment the next transmit MSN
   */
  public synchronized void incrementNextTransmitMsn()
  {
    long	testMsn	= nextTransmitMsn.incrementAndGet();

    if ( testMsn > MAXIMUM_MSN_VALUE )
    {
      nextReceiveMsn.set( 1L );
    }

    myLogger.debug( "Next XMT set to " + nextTransmitMsn.get() );
  }

  /**
   * Decrement the next transmit MSN
   */
  public synchronized void decrementNextTransmitMsn()
  {
    long	testMsn	= nextTransmitMsn.decrementAndGet();

    if ( testMsn == 0L )
    {
      nextReceiveMsn.set( this.MAXIMUM_MSN_VALUE );
    }

    myLogger.debug( "Next XMT set to " + nextTransmitMsn.get() );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the next transmit MSN and increments/wraps it afterward.  This is not
   * reversible and must only be used when the number must be incremented.
   *
   * @return long containing the next MSN to be sent to the sensor
   */
  public synchronized long getNextTransmitMsn()
  {
    long	retVal	= nextTransmitMsn.get();

    incrementNextTransmitMsn();

    return ( retVal );
  }

  /**
   * Returns the next receive MSN without incrementing its value.
   *
   * @return long containing the next expected MSN from the sensor
   */
  public synchronized long getReceiveMsn()
  {
    return ( nextReceiveMsn.get() );
  }

  /**
   * Returns the next transmit MSN without incrementing its value.
   *
   * @return long containing the next MSN that will be sent to the sensor
   */
  public synchronized long getTransmitMsn()
  {
    return ( nextTransmitMsn.get() );
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * Sets the next expected receive MSN to the supplied value after validating it.
   *
   * @param nxt long containing the new nextReceiveMsn value
   */
  public synchronized void setNextReceiveMsn( long nxt )
  {
    if ( ( nxt >= 1L ) && ( nxt <= MAXIMUM_MSN_VALUE ) )
    {
      nextReceiveMsn.set( nxt );
    }

    myLogger.debug( "Next RCV set to " + nextTransmitMsn.get() );
  }

  /**
   * Sets the next expected transmit MSN to the supplied value after validating it.
   *
   * @param nxt long containing the new nextTransmitMsn value
   */
  public synchronized void setNextTransmitMsn( long nxt )
  {
    if ( ( nxt >= 0L ) && ( nxt <= MAXIMUM_MSN_VALUE ) )
    {
      nextTransmitMsn.set( nxt );
    }

    myLogger.debug( "Next XMT set to " + nextTransmitMsn.get() );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Validates a received MSN taking into account retransmit, the Reset_Msg_Seq command, and
   * next expected MSN.
   *
   * @param msn the received MSN
   * @param retransmit flag indicating that this is a retransmission
   * @param resync flag indicating that this is a Reset_Msg_Seq command
   *
   * @return valid (true) or not valid (false)
   */
  public boolean validateReceivedMsn( long msn , boolean retransmit , boolean resync )
  {
    boolean	retVal	= false;
    myLogger.debug( "Validating MSN received: " + msn + " against next RCV " + this.getReceiveMsn() );

    if ( ( msn >= 0L ) && ( msn <= MAXIMUM_MSN_VALUE ) )
    {
      if ( resync && ( msn == 0L ) )
      {
        this.setNextReceiveMsn( 1L );
        this.setNextTransmitMsn( 1L );

        return ( true );
      }

      long	nextExpected	= this.getReceiveMsn();
      myLogger.debug( "Next expected MSN = " + nextExpected );

      if ( msn == nextExpected )
      {
        this.incrementNextReceiveMsn();
        retVal	= true;
      }
      else if ( !retransmit )
      {
        this.incrementNextReceiveMsn();
        retVal	= false;
      }
      else if ( retransmit )
      {
        long	temp	= nextExpected;

        if ( nextExpected <= 10L )
        {
          temp	= MAXIMUM_MSN_VALUE - ( 10L - nextExpected );

          if ( ( ( msn > 0L ) && ( msn < nextExpected ) ) || ( ( msn >= temp ) && ( msn <= MAXIMUM_MSN_VALUE ) ) )
          {
            retVal	= true;
          }
          else
          {
            retVal	= false;
          }
        }
        else
        {
          retVal	= ( ( msn >= temp - 10L ) && ( msn < temp ) );
        }
      }
    }

    return ( retVal );
  }
}
