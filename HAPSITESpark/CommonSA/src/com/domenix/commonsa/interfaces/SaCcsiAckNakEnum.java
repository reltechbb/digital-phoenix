/*
 * CcsiAckNakEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <put description here>
 *
 */
package com.domenix.commonsa.interfaces;

/**
 * Ack Nack Enums
 * 
 * @author kmiller
 */
public enum SaCcsiAckNakEnum
{
  NONE , ACK , NACK;
}
