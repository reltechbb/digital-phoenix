/*
 * NSDSXferEventQueue.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.commonsa.interfaces;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.common.spark.IPCQueue;

import org.apache.log4j.Logger;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a queue NSDS transfer events.
 *
 * @author kmiller
 */
public class SIAXferEventQueue extends IPCQueue<SIATransferEvent>
{
  /** Error/Debug logger */
  private final Logger	myLogger	= Logger.getLogger( SaLinkEventQueue.class );

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs the queue
   */
  public SIAXferEventQueue()
  {
    super( "NSDSTransferQueue" );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This method flushes the contents of the queue by deleting all
   * entries.  This is used when cleaning up after a test run is
   * completed or ended.
   */
  @Override
  public void flushQueue()
  {
    super.flushQueue();
    myLogger.debug( this.getQueueName() + " queue flushed." );
  }

  /**
   * This method inserts an entry at the front of the queue for
   * cases where the test is to be shut down.
   *
   * @param entry transfer event to be inserted.
   */
  @Override
  public void insertFirst( SIATransferEvent entry )
  {
    try
    {
      super.insertFirst( entry );
      myLogger.debug( "Inserted: " + entry.toString() );
    }
    catch ( Exception ex )
    {
      myLogger.error( "Insert failed: " , ex );
    }
  }

  /**
   * This method adds a command entry to the end of the queue.
   *
   * @param entry NSDSTransferEvent entry to be inserted.
   */
  @Override
  public void insertLast( SIATransferEvent entry )
  {
    try
    {
      super.insertLast( entry );
      myLogger.debug( "Inserted: " + entry.toString() );
    }
    catch ( Exception ex )
    {
      myLogger.error( "Insert failed: " , ex );
    }
  }

  /**
   * This method reads the first entry in the queue or waits
   * for a caller specified length of time until an entry is
   * placed in the queue.  If the timeout expires with no queue
   * entries a null result will be returned.  The queue read
   * removes the command entry from the queue.
   *
   * @param waitTime long containing the number of seconds to wait.
   *
   * @return NSDSTransferEvent entry from the queue or null if a timeout or error occurs.
   */
  @Override
  public SIATransferEvent readFirst( long waitTime )
  {
    SIATransferEvent	cmd	= null;

    try
    {
      myLogger.debug( "Reading with " + Long.toString( waitTime ) + " seconds." );
      cmd	= super.readFirst( waitTime );

      if ( cmd != null )
      {
        myLogger.debug( "Read: " + cmd.toString() );
      }
      else
      {
        myLogger.debug( "Read timed out." );
      }
    }
    catch ( Exception ex )
    {
      myLogger.error( ex );
    }

    return ( cmd );
  }

  /**
   * This method returns the current number of entries in the
   * queue to the caller.
   *
   * @return int containing the number of entries in the queue.
   */
  @Override
  public int size()
  {
    return ( super.size() );
  }

  /**
   * This method removes the first entry from the queue.
   */
  @Override
  public void removeFirst()
  {
    try
    {
      super.removeFirst();
    }
    catch ( Exception ex )
    {
      myLogger.error( "Error removing the first entry" , ex );
    }
  }
}
