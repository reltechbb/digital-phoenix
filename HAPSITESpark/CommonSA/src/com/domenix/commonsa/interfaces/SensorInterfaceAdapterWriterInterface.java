/*
 * SparkSIAdapterWriterInterface.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of the interface that must be implemented by all sensor
 * adapter writers.
 *
 */
package com.domenix.commonsa.interfaces;

import com.domenix.common.config.InterfaceSettingsBase;
import com.domenix.common.spark.IPCWrapperQueue;
import java.util.concurrent.Executor;

/**
 * Interface definition for a class that writes to the sensor
 * 
 * @author jmerritt
 */
public interface SensorInterfaceAdapterWriterInterface
{

    /**
     * Initializes the Sensor Interface Adapter Writer
     * 
     * @param fromSensorProtocolAdapter
     * @param configFileBase
     * @param ccsiConfigSensor - Interface configuration. Cast this to the 
     * appropriate concrete class: SerialSettings, NetworkSettings or USBSettings
     * @param configFileName - vendor specific configuration
     * @return 
     */
    boolean initialize(IPCWrapperQueue fromSensorProtocolAdapter, String configFileBase, 
            InterfaceSettingsBase ccsiConfigSensor, String configFileName);

    /**
     * Start the main thread
     * 
     * @param x - instance of implementing class
     */
    void doRun(Executor x);

    /**
     * Shuts down the writer
     * @return 
     */
    public boolean shutdown();
    
    /**
     * Gets the physical interface adapter instance
     * @return 
     */
    public Object getInterfaceAdapter ();
    
    /**
     * Sets the physical Interface Adapter instance
     * @param adapter 
     */
    public void setInterfaceAdapter (Object adapter);

}
