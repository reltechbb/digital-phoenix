/*
 * NSDSTransferEvent.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/11/10
 */


package com.domenix.commonsa.interfaces;

//~--- non-JDK imports --------------------------------------------------------


//~--- classes ----------------------------------------------------------------

/**
 * This class defines an NSDS Transfer Event between the NSDS interface and the Protocol Handler.
 * 
 * @author kmiller
 */
public class SIATransferEvent
{
  /** The MSN that was sent */
  private long	ackMsn	= -1L;

  /** The channels of the message */
  private SaCcsiChannelEnum[]	channels	= null;

  /** The ID number of the connection for the event. */
  private long	connectionId	= -1L;

  /** The flags that were received */
  private boolean	ackFlag	= true;

  /** The timestamp (milliseconds) of the event. */
  private long	eventTime	= System.currentTimeMillis();

  /** The received flags */
  private String	flags	= null;

  /** The NAK code if one is present */
  private String	nakCode	= null;

  /** Field description */
  private long	rcvdDtg	= -1L;

  /** The body of the message */
  private String	recvdBody	= null;

  /** The MSN that was sent */
  private long	recvdMsn	= -1L;

  /** The status of the event indicating successful (true) or failed (false) with a default value of failed. */
  private boolean	eventStatus	= false;

  /** The MSN that was sent */
  private long	sentMsn	= -1L;

  /** The cache keys */
  private String	cacheKeys[];

  /** The type of transfer event */
  private SIATransferEventEnum	xferType;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance by type
   *
   * @param type the type of transfer event
   */
  public SIATransferEvent( SIATransferEventEnum type )
  {
    this.xferType	= type;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the ackChannels
   */
  public SaCcsiChannelEnum[] getChannels()
  {
    SaCcsiChannelEnum[] retVal = null;
    if (channels != null)
    {
        retVal = (SaCcsiChannelEnum[]) channels.clone();
    }
    return retVal;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param channels the ackChannels to set
   */
  public void setChannels( SaCcsiChannelEnum[] channels )
  {
    if (channels != null)
    {
        this.channels = (SaCcsiChannelEnum[]) channels.clone();
    } else
    {
        this.channels = null;
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the ackMsn
   */
  public long getAckMsn()
  {
    return ackMsn;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param ackMsn the ackMsn to set
   */
  public void setAckMsn( long ackMsn )
  {
    this.ackMsn	= ackMsn;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the connectionId
   */
  public long getConnectionId()
  {
    return connectionId;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param connectionId the connectionId to set
   */
  public void setConnectionId( long connectionId )
  {
    this.connectionId	= connectionId;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the ackFlag
   */
  public boolean isAckFlag()
  {
    return ackFlag;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param ackFlag the ackFlag to set
   */
  public void setAckFlag( boolean ackFlag )
  {
    this.ackFlag	= ackFlag;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the eventTime
   */
  public long getEventTime()
  {
    return eventTime;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param eventTime the eventTime to set
   */
  public void setEventTime( long eventTime )
  {
    this.eventTime	= eventTime;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the nakCode
   */
  public String getNakCode()
  {
    return nakCode;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param nakCode the nakCode to set
   */
  public void setNakCode( String nakCode )
  {
    this.nakCode	= nakCode;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the recvdMsn
   */
  public long getRecvdMsn()
  {
    return recvdMsn;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param recvdMsn the recvdMsn to set
   */
  public void setRecvdMsn( long recvdMsn )
  {
    this.recvdMsn	= recvdMsn;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the eventStatus
   */
  public boolean isEventStatus()
  {
    return eventStatus;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param eventStatus the eventStatus to set
   */
  public void setEventStatus( boolean eventStatus )
  {
    this.eventStatus	= eventStatus;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the sentMsn
   */
  public long getSentMsn()
  {
    return sentMsn;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param sentMsn the sentMsn to set
   */
  public void setSentMsn( long sentMsn )
  {
    this.sentMsn	= sentMsn;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the recvdBody
   */
  public String getRecvdBody()
  {
    return recvdBody;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param recvdBody the recvdBody to set
   */
  public void setRecvdBody( String recvdBody )
  {
    this.recvdBody	= recvdBody;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the xferType
   */
  public SIATransferEventEnum getXferType()
  {
    return xferType;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param xferType the xferType to set
   */
  public void setXferType( SIATransferEventEnum xferType )
  {
    this.xferType	= xferType;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the cacheKeys
   */
  public String[] getCacheKeys()
  {
    String [] retVal = null;
    if (cacheKeys != null)
    {
      retVal =  (String[]) cacheKeys.clone();
    }
    return retVal;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param cacheKeys the cacheKeys to set
   */
  public void setCacheKeys( String[] cacheKeys )
  {
    if (cacheKeys != null)
    {
        this.cacheKeys = (String[]) cacheKeys.clone();
    } else
    {
        this.cacheKeys = null;
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the flags
   */
  public String getFlags()
  {
    return flags;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param flags the flags to set
   */
  public void setFlags( String flags )
  {
    this.flags	= flags;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the rcvdDtg
   */
  public long getRcvdDtg()
  {
    return rcvdDtg;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param rcvdDtg the rcvdDtg to set
   */
  public void setRcvdDtg( long rcvdDtg )
  {
    this.rcvdDtg	= rcvdDtg;
  }
}
