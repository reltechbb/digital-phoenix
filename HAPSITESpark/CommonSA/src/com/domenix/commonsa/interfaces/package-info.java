/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * Interfaces to the Sensor Protocol Adapter (SPA), Sensor Interface Adapter (SIA)
 * used by the Protocol Handler (PH)
 *
 */
package com.domenix.commonsa.interfaces;