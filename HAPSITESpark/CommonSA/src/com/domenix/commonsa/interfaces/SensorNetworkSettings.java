/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domenix.commonsa.interfaces;

import com.domenix.common.config.NetworkSettings;

/**
 *
 * @author jmerritt
 */
public class SensorNetworkSettings  extends NetworkSettings {
        
    private String uuid = null;    //10000000-1000-1000-1000-100000000000";
////b4d81dec-616b-4f2c-a8b2-f787ad24826e";            //12345678-1234-1234-1234-1234567890ab";

    /** ID of the sensor as assigned by the host */
    private String sensorID = null;
    /** The mode of the PH */

    /** classification of communications */
    private String classification = "UNCLASS";
    
    /** Sensor is an Open Interface server */
    private boolean oiServer = true;
    
    
    /** Time window should be enforced */
    private boolean enforceTime = true;
    /** time window is + or - this value in seconds */
    private long enforceTimeValue = 15L;
    
    /** 3 strikes should be enforced */
    private boolean enforceStrikes = false;
   
    
    /** sensor manufacturer */
    private String sensorManufacturer = "Unspecified";
    /** sensor serial number */
    private String sensorSerialNumber = "Unspecified";
    /** sensor model */
    private String sensorModel = "Unspecified";
    /** sensor name */
    private String sensorName = "Unspecified";

    /** The mode of the PH */
    private SaCcsiModeEnum mode = SaCcsiModeEnum.I;
    /** time delay between retires in seconds */
    
    
    private int connRetryTime = 30;
    /** number of times to retry a connection before giving up */
    private int connRetryCount = 3;
    
    
     /**
     * The discovery SID
     */
    private static final String DISCOVERY_SID = "b4d81dec-616b-4f2c-a8b2-f787ad24826e";
    
    
    
    public SensorNetworkSettings (String type)
    {
        super (type);
    }

    public int getConnRetryTime() {
        return connRetryTime;
    }

    public void setConnRetryTime(int connRetryTime) {
        this.connRetryTime = connRetryTime;
    }

    public int getConnRetryCount() {
        return connRetryCount;
    }

    public void setConnRetryCount(int connRetryCount) {
        this.connRetryCount = connRetryCount;
    }

    public boolean isEnforceStrikes() {
        return enforceStrikes;
    }

    public void setEnforceStrikes(boolean enforceStrikes) {
        this.enforceStrikes = enforceStrikes;
    }

    
    
    
    
    public boolean isOiServer() {
        return oiServer;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getSensorID() {
        return sensorID;
    }

    public void setSensorID(String sensorID) {
        this.sensorID = sensorID;
    }


    public String getClassification ()
    {
        return classification;
    }
    
    public void setClassification (String classification)
    {
        this.classification = classification;
    }    

    public SaCcsiModeEnum getMode() {
        return mode;
    }

    public void setMode(SaCcsiModeEnum mode) {
        this.mode = mode;
    }
 
    
    
    
    
    /**
     * Returns the Host Sensor ID that was assigned by the host.  If the ID is null
     * or empty the UUID of the sensor is returned.
     * If neither are valid the discovery SID is returned
     * @return sensor ID
     */
    public String getSensorIDorUUID ()
    {
        String retVal;
 
        if ((getSensorID() != null) && (!sensorID.trim().isEmpty()))
        {
            retVal = getSensorID();
        }
        else if((getUuid() != null) && (!uuid.isEmpty()))
        {
            retVal = getUuid();
        }
        else
        {
            retVal =  getDISCOVERY_SID();
        }
        return retVal;
    }
    
    public String getDiscoverySID()
    {
        return getDISCOVERY_SID();
    }
    
    
    public String getSensorManufacturer ()
    {
        return sensorManufacturer;
    }
    
    public void setSensorManufacturer (String manufacturer)
    {
        sensorManufacturer = manufacturer;
    }
    
    public String getSensorSerialNumber ()
    {
        return sensorSerialNumber;
    }
    
    public void setSensorSerialNumber (String serial)
    {
        sensorSerialNumber = serial;
    }
    
    
    public String getSensorModel ()
    {
        return sensorModel;
    }
    
    public void setSensorModel (String model)
    {
        sensorModel = model;
    }
    
    public String getSensorName ()
    {
        return sensorName;
    }
    
    public void setSensorName (String name)
    {
        sensorName = name;
    }

    
    
     /**
     * time window should be enforced
     * @param enforce 
     */
    public void setEnforceTime (boolean enforce)
    {
        enforceTime = enforce;
    }
    
    public boolean getEnforceTime ()
    {
        return isEnforceTime();
    }
    
    public void setEnforceTimeValue (long time)
    {
        enforceTimeValue = time;
    }
    
    public long getEnforceTimeValue ()
    {
        return enforceTimeValue;
    }

    /**
     * @param oiServer the oiServer to set
     */
    public void setOiServer(boolean oiServer) {
        this.oiServer = oiServer;
    }

    /**
     * @return the enforceTime
     */
    public boolean isEnforceTime() {
        return enforceTime;
    }

    /**
     * @return the DISCOVERY_SID
     */
    public static String getDISCOVERY_SID() {
        return DISCOVERY_SID;
    }

//    /**
//     * @param aDISCOVERY_SID the DISCOVERY_SID to set
//     */
//    public static void setDISCOVERY_SID(String aDISCOVERY_SID) {
//        DISCOVERY_SID = aDISCOVERY_SID;
//    }
    
    
}
