/*
 * NSDSConnectionTypeEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of the types of connections for NSDS.
 *
 * Version: V1.0  15/08/01
 */


package com.domenix.commonsa.interfaces;

/**
 * This defines the enumeration of the types of connections supported by NSDS.
 * 
 * @author kmiller
 */
public enum SIAConnectionTypeEnum
{
  /** No connection */
  NO_CONNECTION , 
  /** Connection is a CCSI transient logical connection */
  TRANSIENT_CONNECTION , 
  /** Connection is a CCSI persistent logical connection */
  PERSISTENT_CONNECTION;

  /**
   * Returns the String form of the enumeration values name.
   *
   * @return String containing the enumeration value name
   */
  public String value()
  {
    return name();
  }

  /**
   * Returns the enumeration value for the supplied name String.
   *
   * @param v String containing the name of the enumeration value
   *
   * @return the enumeration value or null.
   */
  public static SIAConnectionTypeEnum fromValue( String v )
  {
    return valueOf( v );
  }
}
