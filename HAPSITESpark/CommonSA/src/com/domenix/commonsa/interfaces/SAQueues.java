/*
 * HAQueues.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file is a container for Queue references
 *
 */
package com.domenix.commonsa.interfaces;

import com.domenix.common.spark.CtlWrapperQueue;
import com.domenix.common.spark.IPCWrapperQueue;

/**
 * This file is a container for Queue references
 * 
 * @author jmerritt
 */
public class SAQueues
{
    /**
     * Connection events from the Host Interface Adapter
     */
    private SaConnEventQueue siaConnEventQueue;

    /**
     * Transfer messages from the Host Interface Adapter
     */
    private SIAXferEventQueue siaXferEventQueue;
    
    private SIAXferEventQueue spaXferEventQueue;
    
    private SIAXferEventQueue phXferEventQueue;
    
    /**
     * Control messages to the Host Interface Adapter
     */
    private CtlWrapperQueue siaCtlQueue;    
    
    /**
     * Link events
     */
    private SaLinkEventQueue siaLinkEventQueue;

 
    
    
    
    public SIAXferEventQueue getSpaXferEventQueue() {
        return spaXferEventQueue;
    }

    public void setSpaXferEventQueue(SIAXferEventQueue spaXferEventQueue) {
        this.spaXferEventQueue = spaXferEventQueue;
    }

    public SIAXferEventQueue getPhXferEventQueue() {
        return phXferEventQueue;
    }

    public void setPhXferEventQueue(SIAXferEventQueue phXferEventQueue) {
        this.phXferEventQueue = phXferEventQueue;
    }

    
    
    
    
    
    
    public SaConnEventQueue getSiaConnEventQueue() {
        return siaConnEventQueue;
    }

    public void setSiaConnEventQueue(SaConnEventQueue siaConnEventQueue) {
        this.siaConnEventQueue = siaConnEventQueue;
    }

    public SIAXferEventQueue getSiaXferEventQueue() {
        return siaXferEventQueue;
    }

    public void setSiaXferEventQueue(SIAXferEventQueue siaXferEventQueue) {
        this.siaXferEventQueue = siaXferEventQueue;
    }

    public CtlWrapperQueue getSiaCtlQueue() {
        return siaCtlQueue;
    }

    public void setSiaCtlQueue(CtlWrapperQueue siaCtlQueue) {
        this.siaCtlQueue = siaCtlQueue;
    }

    public SaLinkEventQueue getSiaLinkEventQueue() {
        return siaLinkEventQueue;
    }

    public void setSiaLinkEventQueue(SaLinkEventQueue siaLinkEventQueue) {
        this.siaLinkEventQueue = siaLinkEventQueue;
    }

    
    
    
    
    /**
     * The Sensor Adapter control IPC Queue
     */
    private CtlWrapperQueue sensorAdapterQueue;

    /**
     * Sensor Protocol Adapter to CCSI Protocol Handler IPC
     */
    private IPCWrapperQueue spAdapterToPh;

    /**
     * Sensor Interface Adapter to CCSI Protocol Handler IPC
     */
    private IPCWrapperQueue siAdapterToSp;

    /**
     * Sensor Interface Adapter to CCSI Protocol Handler IPC
     */
    //private IPCWrapperQueue spAdapterToSi;
    private IPCWrapperQueue spAdapterToSi;    
    
    /**
     * The CCSI Protocol Handler to Sensor Adapter IPC
     */
    //private IPCWrapperQueue spWriterDataQueue;
    private IPCWrapperQueue spWriterDataQueue;    
    
    
//    //WRITER
//    private IPCWrapperQueue fromSensorProtocolAdapter = null;
//    private LinkedBlockingQueue<IPCWrapper> pendingSendQueue = null;
//    //END WRITER
//    
//    //READER
//    private IPCWrapperQueue toSensorProtocolAdapter = null;
//    private IPCWrapperQueue toInterfaceAdapterWriter = null;    
//    //END READER

    
    public SAQueues() {
    }
    
    /**
     * Connection events from the Host Interface Adapter
     * @return 
     */
    public SaLinkEventQueue getSIALinkEventQueue ()
    {
        return siaLinkEventQueue;
    }
    
    
     public CtlWrapperQueue getSensorAdapterQueue() {
        return sensorAdapterQueue;
    }

    public void setSensorAdapterQueue(CtlWrapperQueue sensorAdapterQueue) {
        this.sensorAdapterQueue = sensorAdapterQueue;
    }

    public IPCWrapperQueue getSpAdapterToPh() {
        return spAdapterToPh;
    }

    public void setSpAdapterToPh(IPCWrapperQueue spAdapterToPh) {
        this.spAdapterToPh = spAdapterToPh;
    }

    public IPCWrapperQueue getSiAdapterToSp() {
        return siAdapterToSp;
    }

    public void setSiAdapterToSp(IPCWrapperQueue siAdapterToSp) {
        this.siAdapterToSp = siAdapterToSp;
    }

    public IPCWrapperQueue getSpAdapterToSi() {
        return spAdapterToSi;
    }

    public void setSpAdapterToSi(IPCWrapperQueue spAdapterToSi) {
        this.spAdapterToSi = spAdapterToSi;
    }

    public IPCWrapperQueue getSpWriterDataQueue() {
        return spWriterDataQueue;
    }

    public void setSpWriterDataQueue(IPCWrapperQueue spWriterDataQueue) {
        this.spWriterDataQueue = spWriterDataQueue;
    }
    
    
    
    
    
    
    
//    public IPCWrapperQueue getFromSensorProtocolAdapter() {
//        return fromSensorProtocolAdapter;
//    }
//
//    public void setFromSensorProtocolAdapter(IPCWrapperQueue fromSensorProtocolAdapter) {
//        this.fromSensorProtocolAdapter = fromSensorProtocolAdapter;
//    }
//
//    public LinkedBlockingQueue<IPCWrapper> getPendingSendQueue() {
//        return pendingSendQueue;
//    }
//
//    public void setPendingSendQueue(LinkedBlockingQueue<IPCWrapper> pendingSendQueue) {
//        this.pendingSendQueue = pendingSendQueue;
//    }
//
//    public IPCWrapperQueue getToSensorProtocolAdapter() {
//        return toSensorProtocolAdapter;
//    }
//
//    public void setToSensorProtocolAdapter(IPCWrapperQueue toSensorAdapter) {
//        this.toSensorProtocolAdapter = toSensorAdapter;
//    }
//
//    public IPCWrapperQueue getToInterfaceAdapterWriter() {
//        return toInterfaceAdapterWriter;
//    }
//
//    public void setToInterfaceAdapterWriter(IPCWrapperQueue toInterfaceAdapterWriter) {
//        this.toInterfaceAdapterWriter = toInterfaceAdapterWriter;
//    }
//    
    
    
//    /**
//     * Control messages to the Host Protocol Adapter
//     */
//    private CtlWrapperQueue hpaCtlQueue;
//
//    /**
//     * Control messages to the Host Interface Adapter
//     */
//    private CtlWrapperQueue nsdsCtlQueue;
//    
//    /**
//     * Transfer messages to the Protocol Handler
//     */
//    private NSDSXferEventQueue hpaXferEventQueue;
//
//    /**
//     * Transfer messages from the Host Interface Adapter
//     */
//    private NSDSXferEventQueue hiaXferEventQueue;
//
//    /**
//     * Connection events to the Protocol Handler
//     */
//    private ConnEventQueue hpaConnEventQueue;
//
//    /**
//     * Connection events from the Host Interface Adapter
//     */
//    private ConnEventQueue hiaConnEventQueue;
//    
//    /**
//     * Link events
//     */
//    private LinkEventQueue nsdsLinkEventQueue;
//    
//    /**
//     * Control messages to the Host Protocol Adapter
//     * @return 
//     */
//    public CtlWrapperQueue getHpaCtlQueue ()
//    {
//        return hpaCtlQueue;
//    }
//
//    /**
//     * Sets the HostProtocolAdapter control queue
//     * @param queue 
//     */
//    public void setHpaCtlQueue (CtlWrapperQueue queue)
//    {
//        hpaCtlQueue = queue;
//    }
//    
//    /**
//     * Control messages to the Host Interface Adapter
//     * @return 
//     */
//    public CtlWrapperQueue getNsdsCtlQueue ()
//    {
//        return nsdsCtlQueue;
//    }
//    
//    /**
//     * Sets the NSDS control queue
//     * @param queue 
//     */
//    public void setNsdsCtlQueue (CtlWrapperQueue queue)
//    {
//        nsdsCtlQueue = queue;
//    }
//    
//    /**
//     * Transfer messages to the Protocol Handler
//     * @return 
//     */
//    public NSDSXferEventQueue getHpaXferEventQueue ()
//    {
//        return hpaXferEventQueue;
//    }
//
//    /**
//     * Sets the HostProtocolAdapter transfer event queue
//     * @param queue 
//     */
//    public void setHpaXferEventQueue (NSDSXferEventQueue queue)
//    {
//        hpaXferEventQueue = queue;
//    }
//    
//    /**
//     * Transfer messages from the Host Interface Adapter
//     * @return 
//     */
//    public NSDSXferEventQueue getHiaXferEventQueue ()
//    {
//        return hiaXferEventQueue;
//    }
//
//    /**
//     * Sets the HostInterfaceAdapter transfer event queue
//     * @param queue 
//     */
//    public void setHiaXferEventQueue (NSDSXferEventQueue queue)
//    {
//        hiaXferEventQueue = queue;
//    }
//    
//    /**
//     * Connection events to the Protocol Handler
//     * @return 
//     */
//    public ConnEventQueue getHpaConnEventQueue ()
//    {
//        return hpaConnEventQueue;
//    }
//
//    /**
//     * Sets the HostProtocolAdapter connection event queue
//     * @param queue 
//     */
//    public void setHpaConnEventQueue (ConnEventQueue queue)
//    {
//        hpaConnEventQueue = queue;
//    }
//    
//    /**
//     * Connection events from the Host Interface Adapter
//     * @return 
//     */
//    public ConnEventQueue getHiaConnEventQueue ()
//    {
//        return hiaConnEventQueue;
//    }
//    
//    /**
//     * Sets the HostInterfaceAdapter connection event queue
//     * @param queue 
//     */
//    public void setHiaConnEventQueue (ConnEventQueue queue)
//    {
//        hiaConnEventQueue = queue;
//    }
//    
//    /**
//     * Connection events from the Host Interface Adapter
//     * @return 
//     */
//    public LinkEventQueue getNSDSLinkEventQueue ()
//    {
//        return nsdsLinkEventQueue;
//    }
//    
//    /**
//     * Sets the NSDS Link Event queue
//     * @param queue 
//     */
//    public void setNSDSLinkEventQueue (LinkEventQueue queue)
//    {
//        nsdsLinkEventQueue = queue;
//    }





}
