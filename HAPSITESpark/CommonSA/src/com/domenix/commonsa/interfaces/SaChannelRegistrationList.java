/*
 * ChannelRegistrationList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains a list of registered channels for the sensor.
 *
 * Version: V1.0  15/09/10
 */


package com.domenix.commonsa.interfaces;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.common.utils.UtilTimer;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

//~--- classes ----------------------------------------------------------------

/**
 * This class encapsulates a list of active channel registrations from a host.
 *
 * @author kmiller
 */
public class SaChannelRegistrationList
{
  /** The channel list */
  private final ArrayList<SaChannelRegistrationType>	theList	= new ArrayList<>();

  /** The error/debug logger */
  private final Logger	myLogger	= Logger.getLogger(SaChannelRegistrationList.class );

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   */
  public SaChannelRegistrationList()
  {
  }
  
  /**
   * tests if a list of ChannelRegistrationType is empty
   * @return 
   */
  public boolean isEmpty()
  {
    return( theList.isEmpty() );
  }
  
  /**
   * Gets a list of ChannelRegistrationType
   * @return 
   */
  public ArrayList<SaChannelRegistrationType> getList()
  {
    return( this.theList );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Remove all entries from the list
   *
   * @param timer the timer base to allow cancellation of timers
   */
  public void clearList( UtilTimer timer )
  {
    for ( SaChannelRegistrationType x : this.theList )
    {
      if ( x.getPeriodTimer() >= 0L )
      {
        if ( timer != null )
        {
          timer.cancelTimer( x.getPeriodTimer() );
          myLogger.info( "Cancelled timer for channel: " + x.getMsgChannel().name() );
        }
      }
    }

    this.theList.clear();
  }

  /**
   * Adds a new channel registration to the list
   *
   * @param creg the channel registration to be added
   *
   * @return flag indicating success or failure
   */
  public boolean addChannelRegistration( SaChannelRegistrationType creg )
  {
    boolean	retValue;

    for ( SaChannelRegistrationType x : this.theList )
    {
      if ( x.getMsgChannel() == creg.getMsgChannel() )
      {
        retValue	= false;

        return ( retValue );
      }
    }

    retValue	= this.theList.add( creg );

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Return a flag indicating whether or not the specified channel is in the
   * registered list.
   *
   * @param chn the channel to be searched
   *
   * @return flag indicating if the channel is registered or not
   */
  public boolean hasChannel( SaCcsiChannelEnum chn )
  {
    boolean	retValue	= false;

    for ( SaChannelRegistrationType x : this.theList )
    {
      if ( x.getMsgChannel() == chn )
      {
        retValue	= true;

        break;
      }
    }

    return ( retValue );
  }
  
  /**
   * Returns a channel registration information type for the specified channel.
   * 
   * @param chan the channel to be returned
   * 
   * @return the channel registration record or null if not found
   */
  public SaChannelRegistrationType getRegistration( SaCcsiChannelEnum chan )
  {
    SaChannelRegistrationType reg = null;
    
    for ( SaChannelRegistrationType x : this.theList )
    {
      if ( x.getMsgChannel() == chan )
      {
        reg	= x;

        break;
      }
    }

    return( reg );
    
  }
  //~--- methods --------------------------------------------------------------

  /**
   * Removes a registered channel from the list.
   *
   * @param chn the channel to be removed
   * @param timer the base timer to allow cancellation of a pending timer
   *
   * @return flag indicating success or failure
   */
  public boolean removeChannel( SaCcsiChannelEnum chn , UtilTimer timer )
  {
    boolean	retValue	= false;

    for ( SaChannelRegistrationType x : this.theList )
    {
      if ( x.getMsgChannel() == chn )
      {
        long	t	= x.getPeriodTimer();

        this.theList.remove( x );

        if ( t >= 0L )
        {
          timer.cancelTimer( t );
        }

        retValue	= true;

        break;
      }
    }

    return ( retValue );
  }
}
