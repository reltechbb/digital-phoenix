/*
 * SensorProtocolAdapterWriterInterface.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Interface definition for a class to translate messages from the format 
 * utilized by the sensor to CCSI format
 *
 */
package com.domenix.commonsa.interfaces;

import com.domenix.common.config.InterfaceSettingsBase;
import com.domenix.common.config.SensorUniqueConfigItem;
import com.domenix.common.spark.IPCWrapperQueue;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * Interface definition for a class to translate messages from the Protocol Handler format 
 * to the format utilized by the sensor
 *
 * @author jmerritt
 */
public interface SensorProtocolAdapterWriterInterface 
{
    /**
     * 
     * @param toSensorInterface
     * @param fromProtocolHandler
     * @param toSPAReader - to Sensor Protocol Adapter Reader (optional in case the message needs to 
     * be forwarded to the SPA Reader
     * @param configFileBase - path to the configuration file
     * @param configFileName - name of the configuration file
     * @param uniqueConfig - List of default values for sensor unique configuration items
     * @param configSensor - Interface configuration. Cast this to the 
     * appropriate concrete class: SerialSettings, NetworkSettings or USBSettings
     * @return - true = OK or false = an error occurred
     */
    public boolean initialize (IPCWrapperQueue toSensorInterface, IPCWrapperQueue fromProtocolHandler, 
            IPCWrapperQueue toSPAReader, List <SensorUniqueConfigItem> uniqueConfig,
            String configFileBase, String configFileName,
            InterfaceSettingsBase configSensor);
    
    /**
     * Starts the thread running
     * @param x - an instance of the implementing class
     */
    public void doRun (Executor x);
    
    /**
     * Shuts the Protocol Adapter down
     * @return - true = OK or false = an error occurred
     */
    public boolean shutdown ();
}
