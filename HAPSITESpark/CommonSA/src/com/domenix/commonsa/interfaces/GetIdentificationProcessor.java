/*
 * <put file name here>
 *
 * Copyright (c) 2001-2020 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * <put description here>
 *
 */
package com.domenix.commonsa.interfaces;

/**
 *
 * @author Administrator
 */
public class GetIdentificationProcessor {
    
//    public final String GET_IDENTIFICATION = "<CCSIDoc>\n" +
//        "<Msg>\n" +
//        "<CmdChn Cmd=\"Get_Identification\"/>\n" +
//        "</Msg>\n" +
//        "</CCSIDoc></ccsi:Hdr>";
    public final String GET_IDENTIFICATION = "<CCSIDoc><Msg><CmdChn Cmd=\"Get_Identification\"/></Msg></CCSIDoc></ccsi:Hdr>";
   
    public GetIdentificationProcessor() {
    }
    
    public int getLength() {
//        System.out.println("getLength=" + GET_IDENTIFICATION.length());
        return GET_IDENTIFICATION.length();
        
    }

    
    public String getGetIdentificationMessage() {
        return this.GET_IDENTIFICATION;
    }
    
}
