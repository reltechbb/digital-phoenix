/*
 * MsgParserUtilities.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains a series of patterns and variables to support parsing CCSI messages and
 * open interface messages.
 *
 * Version: V1.0  15/08/22
 */
package com.domenix.commonsa.interfaces;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.commonsa.interfaces.SaCcsiAckNakEnum;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//~--- classes ----------------------------------------------------------------
/**
 * This class contains the patterns for finding open interface and CCSI messages
 *
 * @author kmiller
 */
public class SaMsgParserUtilities {

    /**
     * The discovery SID
     */
//    private final String discoverySid = "b4d81dec-616b-4f2c-a8b2-f787ad24826e";

    /**
     * Field description
     */
    private final String endHdr = "</ccsi:Hdr>";

    /**
     * Field description
     */
    private final Pattern endHdrPattern = Pattern.compile("</([a-zA-Z0-9\\.\\:\\_]{1,30}:)?Hdr");

    /**
     * Field description
     */
    private final String msgConnection
            = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<ccsi:CCSIConnection xmlns:ccsi=\"file:/C:/ccsi/1.1/CCSI\" " + 
            "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
            "xmlns:ism=\"urn:us:gov:ic:ism\" ";

    /**
     * Field description
     */
    private final String msgStart
            = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<ccsi:Hdr xmlns:ccsi=\"file:/C:/ccsi/1.1/CCSI\" " + 
            "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
            "xmlns:ism=\"urn:us:gov:ic:ism\" ";

    /**
     * Pattern for finding if this is a Reset_Msg_Seq command
     */
    private final Pattern resetMsgSeqPattern = Pattern.compile("<CmdChn\\s*Cmd\\s*=\\s*\"Reset_Msg_Seq\"");

    /**
     * Start of message pattern - Group 0 contains the Hdr or CCSIConnecction
     */
    private final Pattern somPattern
            = Pattern.compile("(<\\?xml[\\s\\w\\\"\\-\\=\\.]*\\?>)?<([a-zA-Z0-9\\.\\:\\_]{1,30}:)?(Hdr|CCSIConnection)");

    /**
     * A start message tag with flags - Group 2 has the flags
     */
    private final Pattern flaggedMsgPattern
            = Pattern.compile("<([a-zA-Z0-9\\.\\:\\_]{1,30}:)?Msg\\s*flg=\"([C|E]{1,2})\"\\s*>");

    /**
     * End of message tag
     */
    private final Pattern endMsgPattern = Pattern.compile("</([a-zA-Z0-9\\.\\:\\_]{1,30}:)?Msg\\s*>");

    /**
     * Text macros
     */
    private final String textMacros
            = "\\{((new)?([Aa]lertId|[Rr]eadingId|[Dd]tgId|[Ii]dentHostSensorId|" +
            "[Hh]ostSensorId|[Hh]ostIp|[Ss]ensorIp|[Pp]ortNumber|[Nn]etMask|[Ll]inkPort|" +
            "[Bb]roadcastIp|[Gg]atewayIp|[Ss]ensorUuid|[cC]reateDate))\\}";

    /**
     * Start of header
     */
    private final Pattern startHdrPat = Pattern.compile("<([a-zA-Z0-9\\.\\:\\_]{1,30}:)?Hdr");

    /**
     * Start of message, either Msg or connection
     */
    private final Pattern somTypePattern = Pattern.compile("<([a-zA-Z0-9\\.\\:\\_]{1,30}:)?(Hdr|CCSIConnection)");

    /**
     * sid tag pattern - Group 1 is the sid
     */
    private final Pattern sidPattern = Pattern.compile("\\s*sid\\s*=\\s*\"(([a-zA-Z0-9\\-\\_\\.]){1,63})\"");

    /**
     * Connection type pattern - Group 1 is the contains the match
     */
    private final Pattern typePattern = Pattern.compile("\\s*type\\s*=\\s*\"((start|end){1,1})\"");

    /**
     * Message channel pattern - Group 1 is the channels
     */
    private final Pattern rawChanPattern = Pattern.compile("\\s*chn\\s*=\\s*\"(([a-z0-9]){1,11})\"");

    /**
     * Valid NACK codes
     */
    private final Pattern nakCodePattern
            = Pattern.compile("\\s*nakcod\\s*=\\s*\"((FORMAT|LENMSM|MSGCON|MSGMSN|TIMOUT){1,1})\"");

    /**
     * MSN match pattern Group 1 is the MSN
     */
    private final Pattern msnPattern = Pattern.compile("\\s*msn\\s*=\\s*\"(([0-9]){1,10})\"");

    /**
     * Msg element flags
     */
    private final Pattern msgFlgPattern = Pattern.compile("flg\\s*=\\s*\"(([C|E]){1,2})\"");    // Group 1 returns the char(s)

    /**
     * Mode match pattern Group 1 is the mode
     */
    private final Pattern modePattern = Pattern.compile("\\s*mod\\s*=\\s*\"(([N|D|X|T|U|S|I]){1,1})\"");

    /**
     * Long pattern
     */
    private final Pattern longPattern = Pattern.compile("([0-9]){1,20}");

    /**
     * Message length pattern - Group 1 is the length
     */
    private final Pattern lenPattern = Pattern.compile("len=\"(([0-9]){1,10})\"");

    /**
     * Message header flags
     */
    private final Pattern hdrFlgPattern = Pattern.compile("flg\\s*=\\s*\"(((C|E|R)){1,3})\"");    // Group 1 returns the char(s)

    /**
     * DTG match pattern Group 1 is the DTG
     */
    private final Pattern dtgPattern = Pattern.compile("\\s*dtg\\s*=\\s*\"(([0-9]){1,25})\"");

    /**
     * connect time pattern is the time of the host
     */
    private final Pattern connectTimePattern = Pattern.compile("\\s*time\\s*=\\s*\"(([0-9]){1,25})\"");

    /**
     * Conn start
     */
    private final Pattern connStartPattern = Pattern.compile("\\s*type\\s*=\\s*\"start\"");

    /**
     * Connect start string.
     */
    private final Pattern connPattern = Pattern.compile("<([a-zA-Z0-9\\.\\:\\_]{1,30}:)?CCSIConnection ");

    /**
     * Conn end pattern
     */
    private final Pattern connEndPattern = Pattern.compile("\\s*type\\s*=\\s*\"end\"");

    /**
     * Message channel pattern - Group 1 is the channels
     */
    private final Pattern chanPattern = Pattern.compile("\\s*chn\\s*=\\s*\"(([a|f|h|i|l|m|r|s|t|c|z]){1,11})\"");

    /**
     * ACK Pattern
     */
    private final Pattern ackPattern = Pattern.compile("\\s*ack\\s*=\\s*\"((ACK|NACK)){1,1}\"");

    /**
     * ACK MSN Pattern - Group 1 is the MSN
     */
    private final Pattern ackMsnPattern = Pattern.compile("\\s*ackmsn\\s*=\\s*\"(([0-9]){1,10})\"");

    /**
     * Error/Debug logger
     */
    private final Logger myLogger;

    /**
     * classification string
     */
    private String classification = "";

    //~--- methods --------------------------------------------------------------
    /**
     * Constructs an instance
     *
     * @param classification - IC-ISM format classification string to add to all message headers
     */
    public SaMsgParserUtilities(String classification) {
        this.myLogger = Logger.getLogger(SaMsgParserUtilities.class);
        this.classification = classification;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the endHdr
     */
    public String getEndHdr() {
        return endHdr;
    }

    /**
     * Extracts the Ack or Nak identifier from the message
     *
     * @param msg the message
     *
     * @return indication of if its an ack or nak
     */
    public SaCcsiAckNakEnum getAck(String msg) {
        SaCcsiAckNakEnum retVal = SaCcsiAckNakEnum.NONE;
        Matcher ackMatcher = this.ackPattern.matcher(msg);

        if (ackMatcher.find()) {
            String temp = ackMatcher.group(1);

            if ((temp != null) && (!temp.isEmpty())) {
                if (temp.startsWith("A")) {
                    retVal = SaCcsiAckNakEnum.ACK;
                } else if (temp.startsWith("N")) {
                    retVal = SaCcsiAckNakEnum.NACK;
                }
            }
        }

        return (retVal);
    }

    /**
     * @return the endHdrPattern
     */
    public Pattern getEndHdrPattern() {
        return endHdrPattern;
    }

    /**
     * @return the msgConnection
     */
    public String getMsgConnection() {
        return msgConnection;
    }

    /**
     * @return the msgStart
     */
    public String getMsgStart() {
        return msgStart + classification;
    }

    /**
     * @return the somPattern
     */
    public Pattern getSomPattern() {
        return somPattern;
    }

    /**
     * @return the flaggedMsgPattern
     */
    public Pattern getFlaggedMsgPattern() {
        return flaggedMsgPattern;
    }

    /**
     * @return the endMsgPattern
     */
    public Pattern getEndMsgPattern() {
        return endMsgPattern;
    }

    /**
     * @return the textMacros
     */
    public String getTextMacros() {
        return textMacros;
    }

    /**
     * @return the startHdrPat
     */
    public Pattern getStartHdrPat() {
        return startHdrPat;
    }

    /**
     * @return the somTypePattern
     */
    public Pattern getSomTypePattern() {
        return somTypePattern;
    }

    /**
     * @return the sidPattern
     */
    public Pattern getSidPattern() {
        return sidPattern;
    }

    /**
     * @return the nakCodePattern
     */
    public Pattern getNakCodePattern() {
        return nakCodePattern;
    }

    /**
     * @return the msnPattern
     */
    public Pattern getMsnPattern() {
        return msnPattern;
    }

    /**
     * @return the longPattern
     */
    public Pattern getLongPattern() {
        return longPattern;
    }

    /**
     * @return the lenPattern
     */
    public Pattern getLenPattern() {
        return lenPattern;
    }

    /**
     * @return the lastConnStart
     */
//public boolean isLastConnStart()
//{
//  return lastConnStart;
//}
//
///**
// * @return the lastConnEnd
// */
//public boolean isLastConnEnd()
//{
//  return lastConnEnd;
//}
    /**
     * @return the dtgPattern
     */
    public Pattern getDtgPattern() {
        return dtgPattern;
    }

    /**
     * @return the chanPattern
     */
    public Pattern getChanPattern() {
        return chanPattern;
    }

    /**
     * @return the ackPattern
     */
    public Pattern getAckPattern() {
        return ackPattern;
    }

    /**
     * @return the ackMsnPattern
     */
    public Pattern getAckMsnPattern() {
        return ackMsnPattern;
    }

    /**
     * @return the msgFlgPattern
     */
    public Pattern getMsgFlgPattern() {
        return msgFlgPattern;
    }

    /**
     * @return the hdrFlgPattern
     */
    public Pattern getHdrFlgPattern() {
        return hdrFlgPattern;
    }

    /**
     * @return the connStartPattern
     */
    public Pattern getConnStartPattern() {
        return connStartPattern;
    }

    /**
     * @return the connPattern
     */
    public Pattern getConnPattern() {
        return connPattern;
    }

    /**
     * @return the connEndPattern
     */
    public Pattern getConnEndPattern() {
        return connEndPattern;
    }

    /**
     * @return the resetMsgSeqPattern
     */
    public Pattern getResetMsgSeqPattern() {
        return resetMsgSeqPattern;
    }

    /**
     * Returns the type communications received or null if it is not recognized.
     *
     * @param msg the received message
     *
     * @return "Hdr", "CCSIConnection" , or null
     */
    public String getHdrConnType(String msg) {
        Matcher htMatcher = this.somTypePattern.matcher(msg);

        if ((msg != null) && (msg.trim().length() > 0)) {
            if (htMatcher.find()) {
                if (htMatcher.group().contains("Hdr")) {
                    return ("Hdr");
                } else {
                    return ("CCSIConnection");
                }
            }
        }

        return (null);
    }

    /**
     * Returns the SID of a message or null if not found
     *
     * @param msg the message to be parsed
     *
     * @return the sid or null if not found
     */
    public String getSid(String msg) {
        Matcher sidM = this.sidPattern.matcher(msg);

        if ((msg != null) && (msg.trim().length() > 0)) {
            if (sidM.find()) {
                return (sidM.group(1));
            }
        }

        return (null);
    }

    /**
     * Returns the open interface connection type or null if not found
     *
     * @param msg the message to be parsed
     *
     * @return the connection type or null if not found
     *
     */
    public String getOIConnType(String msg) {
        String retVal = null;
        Matcher typeM = this.typePattern.matcher(msg);

        if ((msg != null) && (msg.trim().length() > 0)) {
            if (typeM.find()) {
                retVal = typeM.group(1);
            }
        }

        return (retVal);    // TEMP TEMP TEMP
    }

    /**
     * Returns the MSN or -1 if not found
     *
     * @param msg the message to be parsed
     *
     * @return the MSN or -1 if not found
     */
    public long getMsn(String msg) {
        long retVal = -1L;

        if ((msg != null) && (msg.trim().length() > 0)) {
            Matcher msnM = this.msnPattern.matcher(msg);

            if (msnM.find()) {
                String msnText = msnM.group(1);

                if (msnText != null) {
                    try {
                        retVal = Long.parseLong(msnText);
                    } catch (NumberFormatException ex) {
                        myLogger.error("Invalid msn " + msnText, ex);
                    }
                } else {
                    myLogger.error("Missing msn ");
                }
            }
        }

        return (retVal);
    }

    /**
     * Returns the Ack MSN or -1 if not found
     *
     * @param msg the message to be parsed
     *
     * @return the ack MSN or -1 if not found
     */
    public long getAckMsn(String msg) {
        long retVal = -1L;

        if ((msg != null) && (msg.trim().length() > 0)) {
            Matcher ackMsnM = this.ackMsnPattern.matcher(msg);

            if (ackMsnM.find()) {
                String ackText = ackMsnM.group(1);

                if (ackText != null) {
                    try {
                        retVal = Long.parseLong(ackText);
                    } catch (NumberFormatException ex) {
                        myLogger.error("Invalid ack msn " + ackText, ex);
                    }
                } else {
                    myLogger.error("Missing ack msn ");
                }
            }
        }

        return (retVal);
    }

    /**
     * Returns the channel(s) or null if not found
     *
     * @param msg the message to be parsed
     *
     * @return the channels or null if not found
     */
    public String getChannels(String msg) {
        Matcher chnM = this.chanPattern.matcher(msg);

        if ((msg != null) && (msg.trim().length() > 0)) {
            if (chnM.find()) {
                return (chnM.group(1));
            } else {
                Matcher rawM = this.rawChanPattern.matcher(msg);

                if (rawM.find()) {
                    return (chnM.group(1));
                } else if (msg.indexOf("chn") > 0) {
                    return (null);
                } else {
                    return ("c");
                }
            }
        }

        return (null);
    }

    /**
     * Returns the channel(s) or null if not found
     *
     * @param msg the message to be parsed
     *
     * @return the channels or null if not found
     */
    public String getRawChannels(String msg) {
        Matcher chnM = this.rawChanPattern.matcher(msg);

        if ((msg != null) && (msg.trim().length() > 0)) {
            if (chnM.find()) {
                return (chnM.group(1));
            }
        }

        return (null);
    }

    /**
     * Returns the NAK code or null if not found
     *
     * @param msg the message to be parsed
     *
     * @return the NAK code or null if not found
     */
    public String getNakCode(String msg) {
        Matcher nakM = this.nakCodePattern.matcher(msg);

        if ((msg != null) && (msg.trim().length() > 0)) {
            if (nakM.find()) {
                return (nakM.group(1));
            }
        }

        return (null);
    }

    /**
     * Returns the DTG or null if not found
     *
     * @param msg the message to be parsed
     *
     * @return the DTG or null if not found
     */
    public String getDtg(String msg) {
        Matcher dtgM = this.dtgPattern.matcher(msg);

        if ((msg != null) && (msg.trim().length() > 0)) {
            if (dtgM.find()) {
                return (dtgM.group(1));
            }
        }

        return (null);
    }

    /**
     * Returns the time from the connect message or null if not found
     *
     * @param msg the message to be parsed
     *
     * @return the epoch time or null if not found
     */
    public String getConnectTime(String msg) {
        Matcher timeM = this.connectTimePattern.matcher(msg);

        if ((msg != null) && (msg.trim().length() > 0)) {
            if (timeM.find()) {
                return (timeM.group(1));
            }
        }

        return (null);
    }

    /**
     * Returns the mode or null if not found
     *
     * @param msg the message to be parsed
     *
     * @return the mode or null if not found
     */
    public String getMode(String msg) {
        Matcher modM = this.modePattern.matcher(msg);

        if ((msg != null) && (msg.trim().length() > 0)) {
            if (modM.find()) {
                return (modM.group(1));
            }
        }

        return (null);
    }

    /**
     * Returns the flags or null if not found
     *
     * @param msg the message to be parsed
     *
     * @return the flags or null if not found
     */
    public String getFlags(String msg) {
        Matcher flagM = this.msgFlgPattern.matcher(msg);

        if ((msg != null) && (msg.trim().length() > 0)) {
            if (flagM.find()) {
                return (flagM.group(1));
            }
        }

        return (null);
    }

    /**
     * Returns a flag indicating if this is a Reset_Msg_Seq command
     *
     * @param msg the message to be parsed
     *
     * @return flag indicating it is (true) or is not (false)
     */
    public boolean isResetMsgSeq(String msg) {
        Matcher rmsM = this.resetMsgSeqPattern.matcher(msg);

        if ((msg != null) && (msg.trim().length() > 0)) {
            if (rmsM.find()) {
                return (true);
            }
        }

        return (false);
    }
}
