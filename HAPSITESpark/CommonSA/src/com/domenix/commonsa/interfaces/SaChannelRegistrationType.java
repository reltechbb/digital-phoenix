/*
 * ChannelRegistrationType.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains registration information for a single channel.
 *
 * Version: V1.0  15/09/11
 */


package com.domenix.commonsa.interfaces;

/**
 * This class encapsulates a host's registration for a single channel.
 *
 * @author kmiller
 */
public class SaChannelRegistrationType
{
  /** The channel */
  private SaCcsiChannelEnum	msgChannel;

  /** The timer for periodic reports. */
  private long	periodTimer;
  
  /** The timer period for periodic registrations */
  private int period = 0;

  /** The type of channel registration. */
  private SaCcsiChannelRegistrationEnum	regType;
  
  /** Details when reporting */
  private boolean details = false;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an empty class instance
   */
  public SaChannelRegistrationType()
  {
    this.msgChannel		= null;
    this.regType			= SaCcsiChannelRegistrationEnum.NONE;
    this.periodTimer	= -1L;
  }
  
  /**
   * Constructs a class instance with all information.
   *
   * @param chn the registered channel
   * @param reg the type of registration
   */
  public SaChannelRegistrationType( SaCcsiChannelEnum chn , SaCcsiChannelRegistrationEnum reg )
  {
    this.msgChannel		= chn;
    this.regType			= reg;
    this.period     	= 0;
  }

  /**
   * Constructs a class instance with all information.
   *
   * @param chn the registered channel
   * @param reg the type of registration
   * @param period the time period for the registration
   */
  public SaChannelRegistrationType( SaCcsiChannelEnum chn , SaCcsiChannelRegistrationEnum reg , int period )
  {
    this.msgChannel		= chn;
    this.regType			= reg;
    this.period     	= period;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the msgChannel
   */
  public SaCcsiChannelEnum getMsgChannel()
  {
    return msgChannel;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param msgChannel the msgChannel to set
   */
  public void setMsgChannel( SaCcsiChannelEnum msgChannel )
  {
    this.msgChannel	= msgChannel;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the regType
   */
  public SaCcsiChannelRegistrationEnum getRegType()
  {
    return regType;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param regType the regType to set
   */
  public void setRegType( SaCcsiChannelRegistrationEnum regType )
  {
    this.regType	= regType;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the periodTimer
   */
  public long getPeriodTimer()
  {
    return periodTimer;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param periodTimer the periodTimer to set
   */
  public void setPeriodTimer( long periodTimer )
  {
    this.periodTimer	= periodTimer;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Returns the current state as a string.
   *
   * @return the current state
   */
  @Override
  public String toString()
  {
    StringBuilder	msg	= new StringBuilder( "Registered: " );

    msg.append( this.msgChannel.name() ).append( " as " ).append( this.regType.name() );

    if ( this.regType == SaCcsiChannelRegistrationEnum.PERIOD )
    {
      msg.append( " repeats timer " ).append( this.periodTimer );
    }

    return ( msg.toString() );
  }

  /**
   * @return the details
   */
  public boolean isDetails()
  {
    return details;
  }

  /**
   * @param details the details to set
   */
  public void setDetails(boolean details)
  {
    this.details = details;
  }

  /**
   * @return the period
   */
  public int getPeriod()
  {
    return period;
  }

  /**
   * @param period the period to set
   */
  public void setPeriod(int period)
  {
    this.period = period;
  }
}
