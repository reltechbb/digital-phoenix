/*
 * SensorProtocolAdapterReaderInterface
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Interface definition for a class to translate messages from the format 
 * utilizied by the sensor to CCSI format
 *
 */
package com.domenix.commonsa.interfaces;

import com.domenix.common.spark.CtlWrapperQueue;
import com.domenix.common.config.InterfaceSettingsBase;
import com.domenix.common.config.SensorUniqueConfigItem;
import com.domenix.common.spark.IPCWrapperQueue;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * Interface definition for a class to translate messages from the format 
 * utilized by the sensor to CCSI format
 * 
 * @author jmerritt
 */
public interface SensorProtocolAdapterReaderInterface
{

    /**
     * Initialize parameters
     * 
     * @param fromInterfaceAdapter - queue that messages from the Sensor 
     * Interface Adapter are received on
     * @param toProtocolHandler - queue that translated messages are posted to 
     * @param toInterfaceAdapterWriter - queue for ack/nack messages to be sent 
     * back to the sensor
     * @param ctlWrapperQueue - queue for sending sensor start messages
     * @param sensorConfig - Interface configuration. Cast this to the 
     * appropriate concrete class: SerialSettings, NetworkSettings or USBSettings
     * @param uniqueConfig - List of default values for sensor unique configuration items
     * @param configFileBase - location of the configuration file
     * @param configFileName - name of the configuration file
     * @return - true = OK or false = an error occurred
     */
    public boolean initialize(SAQueues saQueues,              //IPCWrapperQueue fromInterfaceAdapter,
//            IPCWrapperQueue toProtocolHandler, 
//            IPCWrapperQueue toInterfaceAdapterWriter,
            CtlWrapperQueue ctlWrapperQueue, 
            InterfaceSettingsBase sensorConfig,
            List <SensorUniqueConfigItem> uniqueConfig,
            String configFileBase,
            String configFileName);

    /**
     * Starts the Protocol Adapter
     * @param x - instance of the implementing class
     */
    public void doRun(Executor x);

    /**
     * Shuts down the Protocol Adapter
     * @return - true = OK or false = an error occurred
     */
    public boolean shutdown();
}
