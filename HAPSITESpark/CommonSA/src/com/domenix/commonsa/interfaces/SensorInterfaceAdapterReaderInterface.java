/*
 * SparkSIAdapterReaderInterface.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of the interface that must be implemented by all sensor
 * adapter writers.
 *
 */
package com.domenix.commonsa.interfaces;

import com.domenix.common.config.InterfaceSettingsBase;
import java.util.concurrent.Executor;

import com.domenix.common.spark.IPCWrapperQueue;

/**
 * Interface definition for an adapter which reads from a physical interface
 * and sends the data to the specified queue
 * 
 * @author jmerritt
 */
public interface SensorInterfaceAdapterReaderInterface
{
    /**
     * Initialize parameters for the interface
     * 
     * @param toSensorProtocolAdapter - IPCWrapperQueue to receive read messages
     * @param toInterfaceAdapterWriter - IPCWrapperQueue to send error responses
     * @param configFileBase - location of configuration files
     * @param ccsiConfigSensor - Interface configuration. Cast this to the 
     * appropriate concrete class: SerialSettings, NetworkSettings or USBSettings
     * @param configFileName - name of the vendor specific configuration file
     * @return true = OK or false = an error occurred
     */
    public boolean initialize (IPCWrapperQueue toSensorProtocolAdapter, 
            IPCWrapperQueue toInterfaceAdapterWriter, String configFileBase, 
            InterfaceSettingsBase ccsiConfigSensor, String configFileName);
    
    /**
     * Completes the initialization of the interface and starts the process for receiving messages
     * @param x - instance of implementing class
     */
    public void doRun ( Executor x );
    
    /**
     * shuts down the interface
     * @return - true = OK or false = an error occurred
     */
    public boolean shutdown ();
    
    /**
     * Starts the physical interface adapter
     */
    public void start ();
    
    /**
     * gets an instance of the physical interface adapter
     * @return 
     */
    public Object getInterfaceAdapter ();
    
    /**
     * sets an instance of the physical interface adapter
     * @param adapter 
     */
    public void setInterfaceAdapter (Object adapter);
}
