/*
 * SIAConnEvent.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of an SIA Connection Event
 *
 * Version: V1.0  15/08/01
 */


package com.domenix.commonsa.interfaces;


/**
 * This class defines an SIA connection event which is dispatched to registered listeners when
 * the connection state changes.
 *
 * @author kmiller
 */
public class SIAConnEvent
{
  /** The type of the event */
  private SIAConnEventEnum	connEventType	= SIAConnEventEnum.CONN_TERMINATED;

  /** The connection ID */
  private long	connId	= -1L;

  /** Event status */
  private boolean	status	= false;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a basic connection event.
   *
   * @param type the type of connection event (terminated, transient, persistent)
   */
  public SIAConnEvent( SIAConnEventEnum type )
  {
    this.connEventType	= type;
  }

  /**
   * Constructs an event with a source and a status.
   *
   * @param type the type of connection event (terminated, transient, persistent)
   * @param status the status of the event
   */
  public SIAConnEvent( SIAConnEventEnum type , boolean status )
  {
    this.status					= status;
    this.connEventType	= type;
  }

  /**
   * Constructs an event with a source, status, and connection ID number.
   *
   * @param type the type of connection event (terminated, transient, persistent)
   * @param status the status of the event
   * @param connId the identification number of the connection for the event
   */
  public SIAConnEvent( SIAConnEventEnum type , boolean status , long connId )
  {
    this.connEventType	= type;
    this.status					= status;
    this.connId					= connId;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the connEventType
   */
  public SIAConnEventEnum getConnEventType()
  {
    return connEventType;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param connEventType the connEventType to set
   */
  public void setConnEventType( SIAConnEventEnum connEventType )
  {
    this.connEventType	= connEventType;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the status
   */
  public boolean isStatus()
  {
    return status;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param status the status to set
   */
  public void setStatus( boolean status )
  {
    this.status	= status;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the connId
   */
  public long getConnId()
  {
    return connId;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param connId the connId to set
   */
  public void setConnId( long connId )
  {
    this.connId	= connId;
  }
}
