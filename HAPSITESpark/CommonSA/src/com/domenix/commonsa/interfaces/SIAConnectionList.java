/*
 * SIAConnectionList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of a list SIAConnection elements.
 *
 * Version: V1.0  15/08/02
 */


package com.domenix.commonsa.interfaces;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

//~--- classes ----------------------------------------------------------------

/**
 * This defines a list of SIAConnection elements.
 * 
 * @author kmiller
 */
public class SIAConnectionList
{
  /** The list of SIAConnection items. */
  private final ArrayList<SIAConnection>	connectionList	= new ArrayList<>();

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a instance of the class.
   */
  public SIAConnectionList()
  {
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the underlying list object
   *
   * @return List of SIA connection objects
   */
  protected List<SIAConnection> getList()
  {
    return ( this.connectionList );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Add an SIAConnection item to the list.
   *
   * @param item SIAConnection to be added
   *
   */
  public void addConnection( SIAConnection item )
  {
    if ( item != null )
    {
      this.connectionList.add( item );
    }
    else
    {
      throw new IllegalArgumentException( "Null item cannot be added to the list" );
    }
  }

  /**
   * Get the number of items in the list.
   *
   * @return int the number of items in the list
   */
  public int size()
  {
    return ( this.connectionList.size() );
  }

  /**
   * Clear all items from the list
   */
  public void clear()
  {
    this.connectionList.clear();
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the SIAConnection item located at the index.
   *
   * @param connId the list index of the item to be returned
   *
   * @return SIAConnection located in the index slot in the list or null if the index is invalid
   */
  public SIAConnection getItem( long connId )
  {
    if ( connId >= 0L )
    {
      for ( SIAConnection x : this.connectionList )
      {
        if ( x.getConnectionId() == connId )
        {
          return( x );
        }
      }
      return( null );
    }
    else
    {
      return ( null );
    }
  }
  
  /**
   * Remove the connection entry for the specified ID number.
   * 
   * @param connId the connection to be removed
   */
  public void removeItem( long connId )
  {
    if ( connId >= 0L )
    {
      for ( SIAConnection x : this.connectionList )
      {
        if ( x.getConnectionId() == connId )
        {
          this.connectionList.remove( x );
        }
      }
    }
  }

  /**
   * Returns a flag indicating if the list is empty.
   *
   * @return boolean indicating if the list is empty (true) or has at least on element (false)
   */
  public boolean isEmpty()
  {
    return ( this.connectionList.isEmpty() );
  }

  /**
   * @return the connectionList
   */
  public ArrayList<SIAConnection> getConnectionList()
  {
    return connectionList;
  }
}
