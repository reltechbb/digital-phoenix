#!/bin/sh
#
# Launches the Spark for the HAPSITE
#
# Location of data (configuration files, keys, etc)
BASEDIR=/opt/SparkCCSI
LIB=$BASEDIR/lib
DATA=$BASEDIR/data
# FQN of the Sensor Protocol Adapter Reader class
SPAREADER=com.domenix.sensorprotocoladapter.SPAReaderHAPSITEImpl
# FQN of the Sensor Protocol Adapter Writer class
SPAWRITER=com.domenix.sensorprotocoladapter.SPAWriterHAPSITEImpl
# FQN of the Sensor Interface Adapter Reader class
SIAREADER=com.domenix.sensorinterfaceadapter.SIAReaderEthernetImpl
# FQN of the Sensor Interface Adapter Writer class
SIAWRITER=com.domenix.sensorinterfaceadapter.SIAWriterEthernetImpl
# FQN of the Host Interface Adapter class
HOSTIAIMPL=com.domenix.nsds.impl.NSDSInterfaceStubImpl
# FQN of the Host Protocol Adapter class
HOSTPAIMPL=com.domenix.hpaisa.hapsite.HPAISAHAPSITEImpl
# Configuration file for the Sensor Protocol Adapter
SPACONFIG=HAPSITEInterfaceConfig.xml
# Configuration file for the Sensor Interface Adapter
SIACONFIG=HAPSITEInterfaceConfig.xml
# Configuration file for the Host Interface Adapter
HIACONFIG=HAPSITEInterfaceConfig.xml
# Configuration file for the Host Protocol Adapter
HPACONFIG=HAPSITEInterfaceConfig.xml
# Configuration file for the Spark CCSI
CCSICONFIG=isa-config-HAPSITERemote.xml
# Location of log files (on ram disk)
LOGDIR=/var/ramDisk/logs
# mode of the position
# choices: NO_POSITION_MODE, STATIC_MODE, SIMULATED_MODE, REAL_MODE
POSITION_MODE=REAL_MODE

# create the log directory and change the owner
mkdir -p $LOGDIR
chown -R pi:pi $LOGDIR

# start Spark CCSI
java -Dlog.file=$LOGDIR/SPARK_Testing.log \
    -Dposition.movement=$POSITION_MODE \
    -Dposition.device=AMA0 \
    -DreportBSO=false \
    -Dccsi.baseDir=$BASEDIR/data -Dccsi.spa.reader=$SPAREADER \
    -Dccsi.spa.writer=$SPAWRITER -Dccsi.sia.reader=$SIAREADER \
    -Dccsi.sia.writer=$SIAWRITER -Dccsi.sia.configFile=$SIACONFIG \
    -Dccsi.spa.configFile=$SPACONFIG -Dccsi.config.file=$CCSICONFIG \
    -Dccsi.hpa.configFile=$HPACONFIG -Dccsi.hpa.impl=$HOSTPAIMPL \
    -Dccsi.hia.configFile=$HIACONFIG -Dccsi.hia.impl=$HOSTIAIMPL \
    -jar $LIB/SparkCCSI.jar



