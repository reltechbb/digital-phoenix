#!/bin/sh
#
# Launches the SparkCCSI for the LCD-3
#
# Location of data (xsd, xml, configuration files, etc)
BASEDIR=/opt/SparkCCSI
LIB=$BASEDIR/lib
DATA=$BASEDIR/data
# FQN of the Sensor Protocol Adapter Reader class
SPAREADER=com.domenix.sensorprotocoladapter.SPAReaderLCDImpl
# FQN of the Sensor Protocol Adapter Writer class
SPAWRITER=com.domenix.sensorprotocoladapter.SPAWriterImplTemplate
# FQN of the Sensor Interface Adapter Reader class
SIAREADER=com.domenix.sensorinterfaceadapter.SIAReaderSerialImpl
# FQN of the Sensor Interface Adapter Writer class
SIAWRITER=com.domenix.sensorinterfaceadapter.SIAWriterSerialImpl
# FQN of the Host Interface Adapter class
HOSTIAIMPL=com.domenix.nsds.impl.NSDSInterfaceEthernetImpl
# FQN of the Host Protocol Adapter class
HOSTPAIMPL=com.domenix.hpa.impl.HostProtocolAdapterImpl
# Configuration file for the Sensor Protocol Adapter
SPACONFIG=LCDInterfaceConfig.xml
# Configuration file for the Sensor Interface Adapter
SIACONFIG=LCDInterfaceConfig.xml
# Configuration file for the Host Interface Adapter
HIACONFIG=LCDInterfaceConfig.xml
# Configuration file for the Host Protocol Adapter
HPACONFIG=LCDInterfaceConfig.xml
# Configuration file for the Spark CCSI
CCSICONFIG=ccsi-config-LCD.xml
# Location of log files (on ram disk)
LOGDIR=/var/ramDisk/logs

# create the log directory and change the owner
mkdir -p $LOGDIR
chown -R pi:pi $LOGDIR

# start Spark CCSI
java -Dlog.file=$LOGDIR/SPARK_Testing.log \
    -Dccsi.baseDir=$BASEDIR/data -Dccsi.spa.reader=$SPAREADER \
    -Dccsi.spa.writer=$SPAWRITER -Dccsi.sia.reader=$SIAREADER \
    -Dccsi.sia.writer=$SIAWRITER -Dccsi.sia.configFile=$SIACONFIG \
    -Dccsi.spa.configFile=$SPACONFIG -Dccsi.config.file=$CCSICONFIG \
    -Dccsi.hpa.configFile=$HPACONFIG -Dccsi.hpa.impl=$HOSTPAIMPL \
    -Dccsi.hia.configFile=$HIACONFIG -Dccsi.hia.impl=$HOSTIAIMPL \
    -jar $LIB/SparkCCSI.jar

