#!/bin/sh
#
# Installs SparkCCSI jars and data
# Creates directories as required
# Installs SparkCCSI as a service
#
# Author: jmerritt
#
BASE=/opt/SparkCCSI/
LIB=$BASE/lib
DATA=$BASE/data
LOGS=/var/ramDisk/logs
OWNER=pi:pi
HOME=/home/pi

# makes directories
makeDirs ()
{
    echo " " Creating directories $LIB $DATA
    sudo mkdir -p $LIB
    sudo mkdir -p $DATA
    echo " " Changing ownership
    sudo chown -R $OWNER $BASE
    echo " " Creating $LOGS directory
    sudo mkdir -p $LOGS
    echo " " Changing ownership to $OWNER
    sudo chown -R $OWNER $LOGS
}

# installs the Spark CCSI jars
installJars ()
{
    echo " " Installing jars to $LIB
    unzip $HOME/SparkJars.zip -d $LIB
}

# installs the data
installData ()
{
    echo " " Installing data to $DATA
    unzip $HOME/SparkData.zip -d $DATA
    chmod +x $DATA/*.sh
}

# installs support libraries
installOtherJars ()
{
    echo " " Installing other jars
    unzip $HOME/OtherJars.zip -d $LIB
}

removeControllerService ()
{
    if [ -f "/lib/systemd/system/controller.service" ]
    then
        echo stopping the existing service
        sudo systemctl stop controller.service
        sudo systemctl disable controller.service
        sudo rm /lib/systemd/system/controller.service
    fi
}

printControllerChoices ()
{
    echo "Select an action:"
    echo "  1 Enable local LCD Controller"
    echo "  2 Disable local LCD Controller"
    echo "  3 Enable local Radiac Controller"
    echo "  4 Disable local Radiac Controller"
    echo "  Anything else - cancel"
}

# installs SparkCCSI as a service
installService ()
{
    printServiceChoices
    read -p "Select a service to install: " action
    case $action in
        0) return
            ;;
        1) servicename="/opt/SparkCCSI/data/SparkRADIAC.sh"
            ;;
        2) servicename="/opt/SparkCCSI/data/SparkLCD.sh"
            ;;
        3) servicename="/opt/SparkCCSI/data/SparkRADIACISA.sh"
            ;;
        4) servicename="/opt/SparkCCSI/data/SparkLCDISA.sh"
            ;;
        5) servicename="/opt/SparkCCSI/data/SparkJCADISA.sh"
            ;;
        6) servicename="/opt/SparkCCSI/data/SparkHAPSITEISA.sh"
            ;;
        9) servicename=""
            ;;
        *) echo "Invalid selection"
            return
            ;;
    esac
   
    # stop the existing service 
    if [ -f "/lib/systemd/system/sparkccsi.service" ]
    then
        echo stopping the existing service
        sudo systemctl stop sparkccsi.service
        sudo systemctl disable sparkccsi.service
        sudo rm /lib/systemd/system/sparkccsi.service
    fi

    echo "Installing: " $servicename

    file="/opt/SparkCCSI/data/SparkCCSI.sh"

    if [ -f "$file" ]
    then
        rm $file
    fi

    if [ -z $servicename ]
    then
        echo "Symbolic link not being set - do it manually"
    else
        ln -s $servicename $file
    fi

    file="/opt/SparkCCSI/data/saved-state.xml"
    echo Deleting saved state $file

    if [ -f "$file" ]
    then
        rm $file
    fi

    sudo cp $DATA/sparkccsi.service /lib/systemd/system
    sudo chmod 644 /lib/systemd/system/sparkccsi.service
    sudo systemctl daemon-reload
    sudo systemctl enable sparkccsi.service
    # sudo systemctl start sparkccsi.service
}

printServiceChoices ()
{
    echo "Services: "
    echo "  1 RADIAC"
    echo "  2 LCD"
    echo "  3 RADIAC ISA (Remote Controller)"
    echo "  4 LCD ISA (Local Controller)"
    echo "  5 JCAD ISA (Remote Controller)"
    echo "  6 HAPSITE ISA (Remote Controller)"
    echo "  9 Other"
    echo "  0 None"
}

printOptions ()
{
    echo "Action to perform: "
    echo "  1 Make directories"
    echo "  2 Install/update Spark jars"
    echo "  3 Install/update Data"
    echo "  4 Install/update Other jars"
    echo "  5 Install/update Schema"
    echo "  6 Install SparkCCSI as a service"
    echo "  7 Perform All Actions"
    echo "  8 Enable/disable local controllers"
    echo "  9 Exit"
}

localController ()
{
    printControllerChoices
    read -p "Select an action: " action
    case $action in
      1) removeControllerService
         sudo cp /opt/controller/isa-controller-6.0.10-lcd/controller.service /lib/systemd/system
         sudo chmod 644 /lib/systemd/system/controller.service
         sudo systemctl daemon-reload
         sudo systemctl enable controller.service
         if [ -f "/opt/SparkCCSI/data/SparkCCSI.sh" ]
         then
              rm /opt/SparkCCSI/data/SparkCCSI.sh
         fi 
         ln -s /opt/SparkCCSI/data/ccsi-config-LCDISALocal.xml /opt/SparkCCSI/data/ccsi-config-LCDISA.xml
      ;;
      2) removeControllerService
         if [ -f "/opt/SparkCCSI/data/SparkCCSI.sh" ]
         then
              rm /opt/SparkCCSI/data/SparkCCSI.sh
         fi 
         ln -s /opt/SparkCCSI/data/ccsi-config-LCDISARemote.xml /opt/SparkCCSI/data/ccsi-config-LCDISA.xml
      ;;
      3) removeControllerService
         sudo cp /opt/controller/isa-controller-6.0.10-radiac/controller.service /lib/systemd/system
         sudo chmod 644 /lib/systemd/system/controller.service
         sudo systemctl daemon-reload
         sudo systemctl enable controller.service
         if [ -f "/opt/SparkCCSI/data/SparkCCSI.sh" ]
         then
              rm /opt/SparkCCSI/data/SparkCCSI.sh
         fi 
         ln -s /opt/SparkCCSI/data/ccsi-config-RADIACISALocal.xml /opt/SparkCCSI/data/ccsi-config-RADIAC.xml
      ;;
      4) removeControllerService
         if [ -f "/opt/SparkCCSI/data/SparkCCSI.sh" ]
         then
              rm /opt/SparkCCSI/data/SparkCCSI.sh
         fi 
         ln -s /opt/SparkCCSI/data/ccsi-config-RADIACISARemote.xml /opt/SparkCCSI/data/ccsi-config-RADIACISA.xml
      ;;
      *) return
      ;;
    esac
}

while true
do
    printOptions
    read -p "Select an action: " action

    case $action in
        1) makeDirs
        ;;
        2) installJars
        ;;
        3) installData
        ;;
        4) installOtherJars
        ;;
        6) installService
        ;;
        7) makeDirs
            installJars
            installData
            installOtherJars
            installService
            ;;
        9) exit
        ;;
        *) echo "Unknown action: " $action
        ;;
    esac
done
