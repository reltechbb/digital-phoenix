<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:ism="urn:us:gov:ic:ism" id="NTK-ID-00048">
    <sch:p ism:classification="U" ism:ownerProducer="USA" class="ruleText">
        [NTK-ID-00048][Error] ntk:AccessPolicy, ntk:ProfileDes, and ntk:AccessProfileValue are required to have text content.
    </sch:p>
    
    <sch:p ism:classification="U" ism:ownerProducer="USA" class="codeDesc">
        ntk:AccessPolicy, ntk:ProfileDes, and ntk:AccessProfileValue are required to have text content.
    </sch:p>
    
    <sch:rule context="ntk:AccessPolicy | ntk:ProfileDes | ntk:AccessProfileValue">
        <sch:assert test="not(empty(text()))"
                  flag="error">
            [NTK-ID-00048][Error] ntk:AccessPolicy, ntk:ProfileDes, and ntk:AccessProfileValue are required to have text content.
        </sch:assert>
    </sch:rule>
</sch:pattern>
