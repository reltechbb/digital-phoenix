<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:ism="urn:us:gov:ic:ism" id="NTK-ID-00050">
   
   <sch:p ism:classification="U" ism:ownerProducer="USA" class="ruleText">[NTK-ID-00050][Error] 
      EXDIS profiles requires ntk:ProfileDes with type agencydissem (urn:us:gov:ic:ntk:profile:agencydissem).</sch:p>
   
   <sch:p ism:classification="U" ism:ownerProducer="USA" class="codeDesc">.</sch:p>
   
   <sch:rule context="ntk:AccessProfile[ntk:AccessPolicy = 'urn:us:gov:ic:aces:ntk:xd']">
      <sch:assert test="ntk:ProfileDes = 'urn:us:gov:ic:ntk:profile:agencydissem'">[NTK-ID-00050][Error] 
         EXDIS profiles requires ntk:ProfileDes with type agencydissem (urn:us:gov:ic:ntk:profile:agencydissem).
      </sch:assert>
   </sch:rule>
</sch:pattern>