<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:ism="urn:us:gov:ic:ism" id="NTK-ID-00045" is-a="ValueExistsInList">
   
   <sch:p ism:classification="U" ism:ownerProducer="USA" class="ruleText">[NTK-ID-00045][Error] If an ntk:AccessProfileValue with @ntk:vocabulary of [datasphere:license] 
      is specified, then the value must exist in the LIC.CES License CVE (CVEnumLicLicense.xml).</sch:p>
   
   <sch:p ism:classification="U" ism:ownerProducer="USA" class="codeDesc">For AccessProfileValue with vocabulary 'datasphere:license',
      invoke abstract rule ValueExistsInList to check if the value exists in the License CVE.</sch:p>
   
   <sch:param name="context" value="ntk:AccessProfileValue[@ntk:vocabulary='datasphere:license']"/>
   <sch:param name="list" value="$licenseList"/>
   <sch:param name="errMsg" value="'[NTK-ID-00045][Error] If an ntk:AccessProfileValue with @ntk:vocabulary of [datasphere:license] 
      is specified, then the value must exist in the LIC.CES License CVE (CVEnumLicLicense.xml)'"/>
</sch:pattern>
