<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:ism="urn:us:gov:ic:ism" id="NTK-ID-00043">

   <sch:p ism:classification="U" ism:ownerProducer="USA" class="ruleText">[NTK-ID-00043][Error] The source version (@ntk:sourceVersion) must match the version of the CVE being used
      to validate values of the NTK instance.</sch:p>

   <sch:p ism:classification="U" ism:ownerProducer="USA" class="codeDesc">For any given vocabularyType that is tied to a CES, check the claimed sourceVersion against
      the version of the CVE file being used for validation and ensure they are equal.</sch:p>

  <sch:rule abstract="true" id="abs_rule_00043">
     <sch:assert test="$cve/@specVersion = @ntk:sourceVersion" flag="error">[NTK-ID-00043][Error] 
        The source version (@ntk:sourceVersion) must match the version of the CVE being used
        to validate values of the NTK instance.
        The NTK claims that the vocabulary <sch:value-of select="@ntk:name"/> is compliant with 
        <sch:value-of select="@ntk:sourceVersion"/>, but the CVE used points at spec version 
        <sch:value-of select="$cve/@specVersion"/>.
     </sch:assert>
  </sch:rule>
   
   <sch:rule context="ntk:Access//ntk:VocabularyType[@ntk:name='datasphere:mn:region']">
      <sch:let name="cve" value="document('../../CVE/MN/CVEnumMNRegion.xml')//cve:CVE"/>
      <sch:extends rule="abs_rule_00043"/>
   </sch:rule>
   
   <sch:rule context="ntk:Access//ntk:VocabularyType[@ntk:name='datasphere:mn:issue']">
      <sch:let name="cve" value="document('../../CVE/MN/CVEnumMNIssue.xml')//cve:CVE"/>
      <sch:extends rule="abs_rule_00043"/>
   </sch:rule>
   
   <sch:rule context="ntk:Access//ntk:VocabularyType[@ntk:name='datasphere:license']">
      <sch:let name="cve" value="document('../../CVE/LIC/CVEnumLicLicense.xml')//cve:CVE"/>
      <sch:extends rule="abs_rule_00043"/>
   </sch:rule>
   
   <!-- USAgency 2015-FEB does not currently have the spot for the version. It will be added the next time
      USAgency revs. Uncomment this on next revision of NTK following a new version of USAgency-->
   <!--<sch:rule context="ntk:Access//ntk:VocabularyType[@ntk:name='organization:usa-agency']">
      <sch:let name="cve" value="document('../../CVE/USAgency/CVEnumUSAgencyAcronym.xml')//cve:CVE"/>
      <sch:extends rule="abs_rule_00043"/>-->
   <!--</sch:rule>-->
</sch:pattern>
