<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:ism="urn:us:gov:ic:ism" id="NTK-ID-00041">

   <sch:p ism:classification="U" ism:ownerProducer="USA" class="ruleText">[NTK-ID-00041][Error] Source versions (@ntk:sourceVersion) must be consistent for all NTK Profiles
   within a document that contribute to the actual overall access restrictions of the document.</sch:p>

   <sch:p ism:classification="U" ism:ownerProducer="USA" class="codeDesc">For any given vocabularyType that is tied to a CES, determine how many distinct versions of that
   CES are specified. Report an error if there is more than one version found.</sch:p>

  <sch:rule abstract="true" id="abs_rule_00041">
     <sch:let name="versions" 
        value="distinct-values(for $version in //ntk:Access//ntk:VocabularyType[@ntk:name=$vocab]/@ntk:sourceVersion return $version)"/>
     <sch:assert test="not(count($versions)>1)" flag="error">[NTK-ID-00041][Error] 
        Source versions (@ntk:sourceVersion) must be consistent for all NTK Profiles
        within a document that contribute to the actual overall access restrictions of the document.
        Found <sch:value-of select="$vocab"/> versions: <sch:value-of select="$versions"/> 
     </sch:assert>
  </sch:rule>
   
   <sch:rule context="ntk:Access//ntk:VocabularyType[@ntk:name='datasphere:mn:region']">
      <sch:let name="vocab" value="'datasphere:mn:region'"/>
      <sch:extends rule="abs_rule_00041"/>
   </sch:rule>
   
   <sch:rule context="ntk:Access//ntk:VocabularyType[@ntk:name='datasphere:mn:issue']">
      <sch:let name="vocab" value="'datasphere:mn:issue'"/>
      <sch:extends rule="abs_rule_00041"/>
   </sch:rule>
   
   <sch:rule context="ntk:Access//ntk:VocabularyType[@ntk:name='organization:usa-agency']">
      <sch:let name="vocab" value="'organization:usa-agency'"/>
      <sch:extends rule="abs_rule_00041"/>
   </sch:rule>
   
   <sch:rule context="ntk:Access//ntk:VocabularyType[@ntk:name='datasphere:license']">
      <sch:let name="vocab" value="'datasphere:license'"/>
      <sch:extends rule="abs_rule_00041"/>
   </sch:rule>
</sch:pattern>
