<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:ism="urn:us:gov:ic:ism" id="NTK-ID-00026">
    <sch:p ism:classification="U" ism:ownerProducer="USA" class="ruleText">
        [NTK-ID-00026][Error] AccessProfiles containing the AccessPolicy [urn:us:gov:ic:aces:ntk:ico] may not have
        ProfileDes, VocabularyType, or AccessProfileValue elements specified.
        
        Human Readable: When the ICO ACES is referenced, no data content may be specified in the AccessProfile.
    </sch:p>
    <sch:p ism:classification="U" ism:ownerProducer="USA" class="codeDesc">
        For every ntk:AccessProfile that has an ntk:AccessPolicy of [urn:us:gov:ic:aces:ntk:ico], 
        the profile does not specify any of the data elements of ntk:ProfileDes, ntk:VocabularyType, 
        or ntk:AccessProfileValue.
    </sch:p>
    <sch:rule context="ntk:AccessProfile[ntk:AccessPolicy='urn:us:gov:ic:aces:ntk:ico']">
        <sch:assert test="not(ntk:ProfileDes | ntk:VocabularyType | ntk:AccessProfileValue)">
            [NTK-ID-00026][Error] AccessProfiles containing the AccessPolicy [urn:us:gov:ic:aces:ntk:ico] may not have
            ProfileDes, VocabularyType, or AccessProfileValue elements specified.
            
            Human Readable: When the ICO ACES is referenced, no data content may be specified in the AccessProfile. 
        </sch:assert>
    </sch:rule>
</sch:pattern>
