<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:ism="urn:us:gov:ic:ism" id="NTK-ID-00025">
    <sch:p ism:classification="U" ism:ownerProducer="USA" class="ruleText">
        [NTK-ID-00025][Error] Sources cannot be overridden. If a
        built-in vocabulary type is specified and the source 
        attribute is present it must equal the built-in source.
    </sch:p>
    <sch:p ism:classification="U" ism:ownerProducer="USA" class="codeDesc">
        When a builtin vocabulary is specified in an ntk:VocabularyType element and the source
        attribute is present, then verify that the source specified matches the built in source 
        value.
    </sch:p>
    <sch:rule context="ntk:VocabularyType[index-of($builtinVocab, @ntk:name) > 0 and @ntk:source]">
        <sch:let name="index" value="index-of($builtinVocab, @ntk:name)"/>
        <sch:assert test="@ntk:source=$builtinVocabSource[$index]" flag="error">
            [NTK-ID-00025][Error] Sources cannot be overridden. If a
            built-in vocabulary type is specified and the source attribute is present it must equal
            the built-in source. The source [<sch:value-of select="@ntk:source"/>] is invalid with
            respect to the vocabulary type [<sch:value-of select="@ntk:name"/>]. A source of
            [<sch:value-of select="$builtinVocabSource[$index]"/>] is expected.
        </sch:assert>
    </sch:rule>
</sch:pattern>
