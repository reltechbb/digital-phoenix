<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:ism="urn:us:gov:ic:ism" id="NTK-ID-00024">
    <sch:p ism:classification="U" ism:ownerProducer="USA" class="ruleText">
        [NTK-ID-00024][Error] If there is a Profile DES specified, then there must be at least
        one ntk:AccessProfileValue.
    </sch:p>
    <sch:p ism:classification="U" ism:ownerProducer="USA" class="codeDesc">
        When ntk:ProfileDes exists, make sure there is a following sibling ntk:AccessProfileValue
        also.
    </sch:p>
    <sch:rule context="ntk:ProfileDes">
        <sch:assert test="following-sibling::ntk:AccessProfileValue" flag="error">
            [NTK-ID-00024][Error] If there is a Profile DES specified, then there must be at least
            one ntk:AccessProfileValue.
        </sch:assert>
    </sch:rule>
</sch:pattern>
