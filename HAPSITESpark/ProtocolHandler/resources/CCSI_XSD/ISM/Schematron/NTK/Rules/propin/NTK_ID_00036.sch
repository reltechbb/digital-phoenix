<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:ism="urn:us:gov:ic:ism" id="NTK-ID-00036">

   <sch:p ism:classification="U" ism:ownerProducer="USA" class="ruleText">[NTK-ID-00036][Error] PROPIN access policies must have characters after the predefined
      portion ‘urn:us:gov:ic:aces:ntk:propin:’.</sch:p>

   <sch:p ism:classification="U" ism:ownerProducer="USA" class="codeDesc">Given an ntk:AccessPolicy that starts with ‘urn:us:gov:ic:aces:ntk:propin:’, the string
      length must be greater than 30 (that is, there must be characters after the predefined portion).</sch:p>

   <sch:rule context="ntk:AccessPolicy[starts-with(., 'urn:us:gov:ic:aces:ntk:propin:')]">
      <sch:assert test="string-length(.) > 30" flag="error">[NTK-ID-00036][Error] PROPIN access policies
         must have characters after the predefined portion ‘urn:us:gov:ic:aces:ntk:propin:’.</sch:assert>
   </sch:rule>
</sch:pattern>
