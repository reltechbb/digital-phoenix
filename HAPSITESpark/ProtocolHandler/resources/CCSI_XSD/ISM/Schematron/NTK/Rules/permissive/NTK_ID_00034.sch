<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:ism="urn:us:gov:ic:ism" id="NTK-ID-00034">

   <sch:p ism:classification="U" ism:ownerProducer="USA" class="ruleText">[NTK-ID-00034][Error] Use of the permissive access policy requires the Group &amp; Individual
      Profile DES.</sch:p>

   <sch:p ism:classification="U" ism:ownerProducer="USA" class="codeDesc">If ntk:AccessProfile has an ntk:AccessPolicy = 'urn:us:gov:ic:aces:ntk:permissive',
      ntk:ProfileDes must be 'urn:us:gov:ic:ntk:profile:grp-ind'.</sch:p>

   <sch:rule context="ntk:AccessProfile[ntk:AccessPolicy='urn:us:gov:ic:aces:ntk:permissive']/ntk:ProfileDes">
      <sch:assert test=". = 'urn:us:gov:ic:ntk:profile:grp-ind'" flag="error">[NTK-ID-00034][Error] Use of the
         permissive access policy requires the Group and Individual Profile DES.</sch:assert>
   </sch:rule>
</sch:pattern>
