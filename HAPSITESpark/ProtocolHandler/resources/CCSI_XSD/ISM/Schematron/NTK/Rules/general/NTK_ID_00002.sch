<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:ism="urn:us:gov:ic:ism" id="NTK-ID-00002">
    <sch:p ism:classification="U" ism:ownerProducer="USA" class="ruleText">
        [NTK-ID-00002][Error]
        ntk:RequiresAnyOf and ntk:RequiresAllOf must contain ntk:AccessProfileList.
        
        Human Readable: ntk:RequiresAnyOf and ntk:RequiresAllOf must have the child element ntk:AccessProfileList.
    </sch:p>
    
    <sch:p ism:classification="U" ism:ownerProducer="USA" class="codeDesc">
        This rule ensures that ntk:AccessProfileList exist as a child element of ntk:RequiresAnyOf and 
        ntk:RequiresAllOf.
    </sch:p>
    
    <sch:rule context="ntk:RequiresAnyOf|ntk:RequiresAllOf">
        <sch:assert test="ntk:AccessProfileList"
                  flag="error">
            [NTK-ID-00002][Error]
            ntk:RequiresAnyOf and ntk:RequiresAllOf must contain ntk:AccessProfileList.
            
            Human Readable: ntk:RequiresAnyOf and ntk:RequiresAllOf must have the child element ntk:AccessProfileList.
        </sch:assert>
    </sch:rule>
</sch:pattern>
