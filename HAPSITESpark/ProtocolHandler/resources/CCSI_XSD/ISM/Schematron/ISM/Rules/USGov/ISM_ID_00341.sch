<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00341">

    <sch:p class="ruleText"> [ISM-ID-00341][Error] If ISM_USGOV_RESOURCE and SCIcontrols contains a token matching [SI-G]
        or [SI-G-XXXX], then ism:disseminationControls cannot contain [OC-USGOV] 
        
        Human Readable: OC-USGOV cannot be used if SI-G or an SI-G subs are present. </sch:p>
    <sch:p class="codeDesc"> If the document is an ISM_USGOV_RESOURCE and ism:SCIcontrols contains [SI-G] or [SI-G-XXXX], then
        ism:disseminationControls cannot contain [OC-USGOV] </sch:p>

    <sch:rule
        context="*[$ISM_USGOV_RESOURCE and (util:containsAnyTokenMatching(@ism:SCIcontrols, ('^SI-G-[A-Z]{4}$'))) or util:containsAnyOfTheTokens(@ism:SCIcontrols, ('SI-G'))]">

        <sch:assert
            test="not(util:containsAnyOfTheTokens(@ism:disseminationControls, ('OC-USGOV')))"
            flag="error"> [ISM-ID-00341][Error] If ISM_USGOV_RESOURCE and SCIcontrols contains a token matching [SI-G] or
            [SI-G-XXXX], then ism:disseminationControls cannot contain [OC-USGOV] Human Readable:
            OC-GOV cannot be used if SI-G or an SI-G subs are present. </sch:assert>

    </sch:rule>
</sch:pattern>
