<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron"
             id="ISM-ID-00033"
             is-a="MutuallyExclusiveAttributeValues">
    <sch:p class="ruleText">
    	[ISM-ID-00033][Error] If ISM_USGOV_RESOURCE, then tokens [REL], [EYES] 
        and [NF] are mutually exclusive for attribute disseminationControls.
    </sch:p>
	  <sch:p class="codeDesc">
		This rule uses an abstract pattern to consolidate logic.
		If the document is an ISM_USGOV_RESOURCE, for each element which 
		has attribute ism:disseminationControls specified with a value 
		containing the token [REL], [EYES] or [NF], this rule ensures that attribute
		ism:disseminationControls is specified with a value containing only 
		one of the tokens [REL], [EYES] or [NF].
	</sch:p>
	
	  <sch:param name="context"
              value="*[$ISM_USGOV_RESOURCE                                    and util:containsAnyOfTheTokens(@ism:disseminationControls, ('REL', 'EYES', 'NF'))]"/>
	  <sch:param name="attrValue" value="@ism:disseminationControls"/>
	  <sch:param name="mutuallyExclusiveTokenList" value="('REL', 'EYES', 'NF')"/>
	  <sch:param name="errMsg"
              value="'   [ISM-ID-00033][Error] If ISM_USGOV_RESOURCE, then tokens [REL], [EYES]    and [NF] are mutually exclusive for attribute disseminationControls.   '"/>
</sch:pattern>
