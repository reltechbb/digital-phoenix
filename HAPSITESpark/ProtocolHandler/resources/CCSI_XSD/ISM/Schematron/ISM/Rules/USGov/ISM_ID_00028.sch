<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00028">
    <sch:p class="ruleText">
      [ISM-ID-00028][Error] If ISM_USGOV_RESOURCE and attribute 
      disseminationControls contains the name token [OC] or [EYES],
      then attribute classification must have a value of [TS], [S], or [C].
      
      Human Readable: Portions marked for ORCON or EYES ONLY dissemination 
      in a USA document must be CONFIDENTIAL, SECRET, or TOP SECRET.
    </sch:p>
    <sch:p class="codeDesc">
        If the document is an ISM_USGOV_RESOURCE, for each element which has 
    	attribute ism:disseminationControls specified with a value containing
    	the token [OC] or [EYES] this rule ensures that attribute
    	ism:classification is specified with a value of [C], [S], or [TS].
    </sch:p>
	  <sch:rule context="*[$ISM_USGOV_RESOURCE                      and util:containsAnyOfTheTokens(@ism:disseminationControls, ('OC', 'EYES'))]">
        <sch:assert test="@ism:classification=('TS', 'S', 'C')" flag="error">
            [ISM-ID-00028][Error] If ISM_USGOV_RESOURCE and attribute 
            disseminationControls contains the name token [OC] or [EYES], 
            then attribute classification must have a value of [TS], [S], or [C].
            
            Human Readable: Portions marked for ORCON or EYES ONLY dissemination 
            in a USA document must be CONFIDENTIAL, SECRET, or TOP SECRET.
        </sch:assert>
    </sch:rule>
</sch:pattern>
