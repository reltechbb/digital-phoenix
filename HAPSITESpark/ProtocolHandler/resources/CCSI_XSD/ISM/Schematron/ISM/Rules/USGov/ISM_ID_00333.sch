<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:ism="urn:us:gov:ic:ism"
  id="ISM-ID-00333" ism:resourceElement="true" ism:createDate="2011-01-27" ism:classification="U"
  ism:ownerProducer="USA">
  <sch:p class="ruleText" ism:classification="U" ism:ownerProducer="USA"> [ISM-ID-00333][Error] If
    ISM_USGOV_RESOURCE and attribute SCIcontrols contains a token matching [HCS-X], where X is
    represented by the regular expression character class [A-Z], then it must also contain the name
    token [HCS]. 
    
    Human Readable: A USA document with HCS compartment data must also specify that it
    contains HCS data. </sch:p>
  <sch:p class="codeDesc" ism:classification="U" ism:ownerProducer="USA"> If the document is an
    ISM_USGOV_RESOURCE, for each element which specifies attribute ism:SCIcontrols with a value
    containing a token matching [HCS-X], where X is represented by the regular expression character
    class [A-Z], this rule ensures that attribute ism:SCIcontrols is specified with a value
    containing the token [HCS]. </sch:p>
  <sch:rule
    context="*[$ISM_USGOV_RESOURCE and util:containsAnyTokenMatching(@ism:SCIcontrols, ('^HCS-[A-Z]$'))]"
    ism:classification="U" ism:ownerProducer="USA">
    <sch:assert test="util:containsAnyOfTheTokens(@ism:SCIcontrols, ('HCS'))" flag="error">
      [ISM-ID-00333][Error] If ISM_USGOV_RESOURCE and attribute SCIcontrols contains a token
      matching [HCS-X], where X is represented by the regular expression character class [A-Z], then
      it must also contain the name token [HCS]. Human Readable: A USA document with HCS compartment
      data must also specify that it contains HCS data. </sch:assert>
  </sch:rule>
</sch:pattern>
