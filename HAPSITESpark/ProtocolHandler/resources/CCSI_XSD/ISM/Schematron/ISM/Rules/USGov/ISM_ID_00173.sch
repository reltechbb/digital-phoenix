<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00173">
    <sch:p class="ruleText"> [ISM-ID-00173][Error] If ISM_USGOV_RESOURCE and attribute
        atomicEnergyMarkings contains a name token starting with [RD-SG] or [FRD-SG], then attribute
        classification must have a value of [S] or [TS]. Human Readable: Portions in a USA document
        that contain RD or FRD SIGMA data must be marked SECRET or TOP SECRET. </sch:p>
	  <sch:p class="codeDesc"> If the document is an ISM_USGOV_RESOURCE, for each element which has
        attribute ism:atomicEnergyMarkings specified with a value containing a token starting with
        [RD-SG] or [FRD-SG], this rule ensures that the attribute ism:classification has a value of [S]
        or [TS]. </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE                          and util:containsAnyTokenMatching(@ism:atomicEnergyMarkings, ('^RD-SG', '^FRD-SG'))]">
		    <sch:assert test="@ism:classification = ('S','TS')" flag="error"> [ISM-ID-00173][Error] If
            ISM_USGOV_RESOURCE and attribute atomicEnergyMarkings contains a name token starting
            with [RD-SG] or [FRD-SG], then attribute classification must have a value of [S] or
            [TS]. Human Readable: Portions in a USA document that contain RD or FRD SIGMA data must
            be marked SECRET or TOP SECRET. </sch:assert>
	  </sch:rule>
</sch:pattern>
