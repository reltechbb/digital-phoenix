<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron"
             id="ISM-ID-00167"
             is-a="ValuesOrderedAccordingToCve">
  <sch:p class="ruleText">
    [ISM-ID-00167][Error] If ISM_USGOV_RESOURCE and attribute displayOnlyTo is specified, 
    then each of its values must be ordered in accordance with CVEnumISMCATRelTo.xml.
  </sch:p>
  <sch:p class="codeDesc">
    This rule uses an abstract pattern to consolidate logic. It verifies that
    the attribute ism:$attrLocalName has values in the same order as the list
    $cveTermList, which is defined in the main schematron file, ISM_XML.sch.
  </sch:p>
  <sch:param name="attrLocalName" value="displayOnlyTo"/>
  <sch:param name="attrValueTokens"
              value="tokenize(normalize-space(string(@ism:displayOnlyTo)), ' ')"/>
  <sch:param name="cveTermList" value="$displayOnlyToList"/>
  <sch:param name="errorMessage"
              value="'     [ISM-ID-00167][Error] If ISM_USGOV_RESOURCE and attribute displayOnlyTo is specified,      then each of its values must be ordered in accordance with CVEnumISMCATRelTo.xml.     '"/>
</sch:pattern>
