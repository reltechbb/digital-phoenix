<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00097">
    <sch:p class="ruleText">
        [ISM-ID-00097][Warning] If ISM_USGOV_RESOURCE and attribute FGIsourceProtected is 
        specified with a value other than [FGI] then the value(s) must not be discoverable in IC shared spaces.
        
        Human Readable: FGI Protected should rarely if ever be seen outside of an agency's internal systems.    
    </sch:p>
    <sch:p class="codeDesc">
    	If the document is an ISM_USGOV_RESOURCE, for each element which specifies
    	the attribute ism:FGIsourceProtected, this rule ensures that attribute
    	ism:FGIsourceProtected contains only the token [FGI].
    </sch:p>
	  <sch:rule context="*[$ISM_USGOV_RESOURCE and @ism:FGIsourceProtected]">
        <sch:assert test="normalize-space(string(./@ism:FGIsourceProtected))='FGI'"
                  flag="warning">
        	[ISM-ID-00097][Warning] If ISM_USGOV_RESOURCE and attribute FGIsourceProtected is 
        	specified with a value other than [FGI] then the value(s) must not be discoverable in IC shared spaces.
        	
        	Human Readable: FGI Protected should rarely if ever be seen outside of an agency's internal systems.  
        </sch:assert>
    </sch:rule>
</sch:pattern>
