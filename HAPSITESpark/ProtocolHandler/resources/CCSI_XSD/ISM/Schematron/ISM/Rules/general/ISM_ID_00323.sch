<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00323">
    <sch:p class="ruleText">
        [ISM-ID-00323][Error] The attribute 
        ISMCATCESVersion in the namespace urn:us:gov:ic:ism must be specified.
        
        Human Readable: The CVE encoding specification version for ISM CAT must
        be specified.
    </sch:p>
    <sch:p class="codeDesc">
        This rule ensures that the attribute ism:ISMCATCESVersion 
        is specified.
    </sch:p>
    <sch:rule context="/">
        <sch:assert test="some $element in descendant-or-self::node() satisfies $element/@ism:ISMCATCESVersion"
                  flag="error">
            [ISM-ID-00323][Error] The attribute 
            ISMCATCESVersion in the namespace urn:us:gov:ic:ism must be specified.
            
            Human Readable: The CVE encoding specification version for ISM CAT must
            be specified.
        </sch:assert>
    </sch:rule>
</sch:pattern>
