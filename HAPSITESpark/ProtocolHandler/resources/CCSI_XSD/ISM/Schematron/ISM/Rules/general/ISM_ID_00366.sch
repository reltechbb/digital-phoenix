<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00366">
    <sch:p class="ruleText">
        [ISM-ID-00366][Error] The @ntk:DESVersion is less than the minimum version 
        allowed: 201508. 
        
        Human Readable: The NTK version imported by ISM must be greater than or equal to 2015-AUG. 
    </sch:p>
    <sch:p class="codeDesc">
        For all elements that contain @ntk:DESVersion, this rule verifies that the version
        is greater than or equal to the minimum allowed version: 201508.  
    </sch:p>
    <sch:rule context="*[@ntk:DESVersion]">
        <sch:let name="version" value="number(if (contains(@ntk:DESVersion,'-')) then substring-before(@ntk:DESVersion,'-') else @ntk:DESVersion)"/>
        <sch:assert test="$version &gt;= 201508" flag="error">
            [ISM-ID-00366][Error] The @ntk:DESVersion is less than the minimum version 
            allowed: 201508. 
            
            Human Readable: The NTK version imported by ISM must be greater than or equal to 2015-AUG.
        </sch:assert>
    </sch:rule>
</sch:pattern>