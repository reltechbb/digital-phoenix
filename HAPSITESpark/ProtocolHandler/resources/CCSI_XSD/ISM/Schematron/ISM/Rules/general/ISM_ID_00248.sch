<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00248">
	  <sch:p class="ruleText">
		[ISM-ID-00248][Error] ISM_RESOURCE_ELEMENT cannot have externalNotice set to [true].
		
		Human Readable: ISM resource elements can not be external notices.
	</sch:p>
	  <sch:p class="codeDesc">
	  	If ISM_RESOURCE_ELEMENT, this rule ensures that the ISM_RESOURCE_ELEMENT does not contain
		externalNotice set to [true].
	</sch:p>
	  <sch:rule context="*[generate-id(.) = generate-id($ISM_RESOURCE_ELEMENT)][@ism:externalNotice]">
		    <sch:assert test="not(string(@ism:externalNotice)='true')" flag="error">
			[ISM-ID-00248][Error] ISM_RESOURCE_ELEMENT cannot have externalNotice set to [true].
			
			Human Readable: ISM resource elements cannot be external notices.
		</sch:assert>
	  </sch:rule>
</sch:pattern>
