<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00099">
    <sch:p class="ruleText">
        [ISM-ID-00099][Error] If ISM_USGOV_RESOURCE and attribute ownerProducer
        contains the token [FGI], then the token [FGI] must be the only value 
        in attribute ownerProducer.
    </sch:p>
    <sch:p class="codeDesc">
        If the document is an ISM_USGOV_RESOURCE, for each element which
        specifies attribtue ism:ownerProducer with a value containing the token
        [FGI] this rule ensures that attribute ism:ownerProducer only contains a 
        single token.
    </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE                         and util:containsAnyOfTheTokens(@ism:ownerProducer, ('FGI'))]">
        <sch:assert test="             count(                 tokenize(normalize-space(string(@ism:ownerProducer)), ' ')             ) = 1"
                  flag="error">
            [ISM-ID-00099][Error] If ISM_USGOV_RESOURCE and attribute ownerProducer
            contains the token [FGI], then the token [FGI] must be the only value 
            in attribute ownerProducer.
        </sch:assert>
    </sch:rule>
</sch:pattern>
