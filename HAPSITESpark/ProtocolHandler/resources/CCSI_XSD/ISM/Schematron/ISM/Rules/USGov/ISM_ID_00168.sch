<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00168">
    <sch:p class="ruleText">
        [ISM-ID-00168][Error] If ISM_USGOV_RESOURCE and attribute 
        disseminationControls is not specified or is specified and does not contain the name token 
        [DISPLAYONLY], then attribute displayOnlyTo must not be specified.
        
        Human Readable: If a portion in a USA document is not marked for DISPLAY ONLY dissemination, 
        it must not list countries to which it may be disclosed. 
    </sch:p>
    <sch:p class="codeDesc">
        If the document is an ISM_USGOV_RESOURCE and attribute ism:disseminationControls
        does not contain the token [DISPLAYONLY], this rule ensures that the attribute 
      	ism:displayOnlyTo is not specified.
    </sch:p>
	  <sch:rule context="*[$ISM_USGOV_RESOURCE                       and not(util:containsAnyOfTheTokens(@ism:disseminationControls, ('DISPLAYONLY')))]">
        <sch:assert test="not(@ism:displayOnlyTo)" flag="error">
            [ISM-ID-00168][Error] If ISM_USGOV_RESOURCE and attribute 
            disseminationControls is not specified or is specified and does not contain the name token 
            [DISPLAYONLY], then attribute displayOnlyTo must not be specified.
            
            Human Readable: If a portion in a USA document is not marked for DISPLAY ONLY dissemination, 
            it must not list countries to which it may be disclosed.
        </sch:assert>
    </sch:rule>
</sch:pattern>
