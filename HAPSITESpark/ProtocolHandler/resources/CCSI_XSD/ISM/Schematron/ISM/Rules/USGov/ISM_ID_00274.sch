<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00274">
	<sch:p class="ruleText">[ISM-ID-00274][Error] All ISM createDate attributes must be a Date
		without a timezone.</sch:p>

	<sch:p class="codeDesc">For all elements which contain a createDate attribute, this rule ensures that
		the createDate value matches the pattern defined for type Date without timezone information.
		The value must conform to the Regex ‘[0-9]{4}-[0-9]{2}-[0-9]{2}$’</sch:p>

	<sch:p class="codeNote">The first assert in this rule is not able to be failed in unit tests. If
		the createDate does not conform to type Date, schematron fails when defining global
		variables before any rules are fired. The first assert is included as a normative statement
		of the requirement that the attribute be a Date type. The rule can fail the second assert,
		which ensures there is no timezone info.</sch:p>

	<sch:rule context="*[@ism:createDate]">
		<sch:assert test="util:meetsType(@ism:createDate, $DatePattern)" flag="error">
			[ISM-ID-00274][Error] All createDate attribute values must be of type Date. </sch:assert>
		<sch:assert test="matches(@ism:createDate, '[0-9]{4}-[0-9]{2}-[0-9]{2}$')">
			[ISM-ID-00274][Error] All createDate attribute values must not have any timezone
			information specified. </sch:assert>
	</sch:rule>
</sch:pattern>
