<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00133">
    <sch:p class="ruleText">
        [ISM-ID-00133][Error] If ISM_NSI_EO_APPLIES and attribute 
        declassException is specified and contains the tokens [25X1-EO-12951],
        [50X1-HUM], or [50X2-WMD], then attribute declassDate or declassEvent must NOT be specified.
        
        Human Readable: Documents under E.O. 13526 must not specify declassDate or declassEvent if 
        a declassException of 25X1-EO-12951, 50X1-HUM, or 50X2-WMD is specified.
    </sch:p>
    <sch:p class="codeDesc">
    	If ISM_NSI_EO_APPLIES, for each element which specifies 
    	ism:declassException with a value containing token 
    	[50X1-HUM], or [50X2-WMD] this rule ensures that attributes ism:declassDate
    	and ism:declassEvent are NOT specified.
    </sch:p>
	  <sch:rule context="*[$ISM_NSI_EO_APPLIES      and util:containsAnyOfTheTokens(@ism:declassException, ('25X1-EO-12951', '50X1-HUM', '50X2-WMD'))]">
        <sch:assert test="not(@ism:declassDate or @ism:declassEvent)" flag="error">
        	[ISM-ID-00133][Error] If ISM_NSI_EO_APPLIES and attribute 
        	declassException is specified and contains the tokens [25X1-EO-12951],
        	[50X1-HUM], or [50X2-WMD], then attribute declassDate or declassEvent 
        	must NOT be specified.
        	
        	Human Readable: Documents under E.O. 13526 must not specify declassDate
        	or declassEvent if a declassException of 25X1-EO-12951, 50X1-HUM, or 
        	50X2-WMD is specified.
        </sch:assert>
    </sch:rule>
</sch:pattern>
