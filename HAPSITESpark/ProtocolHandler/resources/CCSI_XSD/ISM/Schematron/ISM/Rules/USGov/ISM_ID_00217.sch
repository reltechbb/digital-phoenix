<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00217">

    <sch:p class="ruleText">

        [ISM-ID-00217][Error] If ISM_USGOV_RESOURCE attribute FGIsourceProtected
        contains [FGI], it must be the only value.

    </sch:p>

    <sch:p class="codeDesc">

    	If the document is an ISM_USGOV_RESOURCE, for each element which specifies
    	the attribute ism:FGIsourceProtected, this rule ensures that attribute
    	ism:FGIsourceProtected contains only the token [FGI].

    </sch:p>

	  <sch:rule context="*[$ISM_USGOV_RESOURCE and @ism:FGIsourceProtected]">
		    <sch:assert test="normalize-space(string(@ism:FGIsourceProtected))='FGI'"
                  flag="error">

        	[ISM-ID-00217][Error] If ISM_USGOV_RESOURCE attribute FGIsourceProtected
        	contains [FGI], it must be the only value.

        </sch:assert>

    </sch:rule>

</sch:pattern>
