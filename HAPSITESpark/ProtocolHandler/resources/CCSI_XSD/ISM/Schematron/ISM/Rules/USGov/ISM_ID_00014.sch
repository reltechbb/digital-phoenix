<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00014">
    <sch:p class="ruleText">
        [ISM-ID-00014][Error] If ISM_NSI_EO_APPLIES then one or more of the following 
        attributes: declassDate, declassEvent, or declassException must be specified on the ISM_RESOURCE_ELEMENT.
        
        Human Readable: Documents under E.O. 13526 must have declassification instructions included in the 
        classification authority block information.
    </sch:p>
    <sch:p class="codeDesc">
        If ISM_NSI_EO_APPLIES, this rule ensures that the ISM_RESOURCE_ELEMENT specifies
      one of the following attributes: ism:declassDate, ism:declassEvent, ism:declassException.
    </sch:p>
    <sch:rule context="*[$ISM_NSI_EO_APPLIES                         and generate-id(.) = generate-id($ISM_RESOURCE_ELEMENT)]">
        <sch:assert test="@ism:declassDate or @ism:declassEvent or @ism:declassException"
                  flag="error">
            [ISM-ID-00014][Error] If ISM_NSI_EO_APPLIES then one or more of the following 
            attributes: declassDate, declassEvent, or declassException must be specified on the ISM_RESOURCE_ELEMENT.
            
            Human Readable: Documents under E.O. 13526 must have declassification instructions included in the 
            classification authority block information.
        </sch:assert>
    </sch:rule>
</sch:pattern>
