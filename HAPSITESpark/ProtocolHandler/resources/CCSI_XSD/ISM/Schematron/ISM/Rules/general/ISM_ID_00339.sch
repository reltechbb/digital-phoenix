<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00339">
    <sch:p class="ruleText">
        
        [ISM-ID-00339][Error] 
        1. ism:ownerProducer of resource element contains USA
        2. ism:compliesWith does not contain USGov
        
        Human Readable: All documents that contain USA in @ism:ownerProducer of
        the first resource node (in document order) must claim USGov in @ism:compliesWith
    </sch:p>
    <sch:p class="codeDesc">
        If a document contains USA in @ism:ownerProducer (for the resource element), then
        @ism:compliesWith must contain USGov.
    </sch:p>
    <sch:rule context="*[ generate-id(.) = generate-id($ISM_RESOURCE_ELEMENT)         and util:containsAnyOfTheTokens(@ism:ownerProducer, ('USA'))]">
        <sch:assert test="             util:containsAnyOfTheTokens(@ism:compliesWith, ('USGov'))"
                  flag="error"> [ISM-ID-00339][Error] 
            1. ism:ownerProducer of resource element contains USA
            2. ism:compliesWith does not contain USGov
            
            Human Readable: All documents that contain USA in @ism:ownerProducer of
            the first resource node (in document order) must claim USGov in @ism:compliesWith
        </sch:assert>
    </sch:rule>
</sch:pattern>
