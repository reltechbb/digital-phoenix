<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00183">
    <sch:p class="ruleText">
        [ISM-ID-00183][Error] If ISM_USGOV_RESOURCE and attribute 
        atomicEnergyMarkings contains a name token starting with [RD-SG],
        then it must also contain the name token [RD].
    </sch:p>
	  <sch:p class="codeDesc">
		If the document is an ISM_USGOV_RESOURCE, for each element which has 
		attribute ism:atomicEnergyMarkings specified with a value containing a 
		token starting with [RD-SG], this rule ensures that attribute 
		ism:atomicEnergyMarkings also contains the token [RD].
	</sch:p>
	  <sch:rule context="*[$ISM_USGOV_RESOURCE                      and util:containsAnyTokenMatching(@ism:atomicEnergyMarkings, ('^RD-SG'))]">
		    <sch:assert test="util:containsAnyOfTheTokens(@ism:atomicEnergyMarkings, ('RD'))"
                  flag="error">
			[ISM-ID-00183][Error] If ISM_USGOV_RESOURCE and attribute 
      atomicEnergyMarkings contains a name token starting with [RD-SG],
      then it must also contain the name token [RD].
		</sch:assert>
	  </sch:rule>
</sch:pattern>
