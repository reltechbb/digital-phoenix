<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00142">
    <sch:p class="ruleText">[ISM-ID-00142][Error] If the Classified National Security Information
        Executive Order applies to the document, then a classification authority must be
        specified.</sch:p>

    <sch:p class="codeDesc">If ISM_NSI_EO_APPLIES is true (defined in ISM_XML.sch), then the
        resource element (has the attribute @ism:resourceElement="true") must have either
        @ism:classifiedBy or @ism:derivativelyClassifiedBy</sch:p>

    <sch:rule
        context="*[$ISM_NSI_EO_APPLIES and generate-id(.) = generate-id($ISM_RESOURCE_ELEMENT)]">
        <sch:assert test="@ism:classifiedBy or @ism:derivativelyClassifiedBy" flag="error"
            >[ISM-ID-00142][Error] If the Classified National Security Information Executive Order
            applies to the document, then a classification authority must be specified.</sch:assert>
    </sch:rule>
</sch:pattern>
