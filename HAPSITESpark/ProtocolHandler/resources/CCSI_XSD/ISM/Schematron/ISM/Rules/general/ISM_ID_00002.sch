<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00002">
    <sch:p class="ruleText">
        [ISM-ID-00002][Error] For every attribute in the ISM namespace that is
        used in a document a non-null value must be present.
    </sch:p>
    <sch:p class="codeDesc">
        For each element which defines an attribute in the ISM namespace, This rule ensures that each attribute in the ISM namespace is specified with 
        a non-whitespace value.
    </sch:p>
    <sch:rule context="*[@ism:*]">
        <sch:assert test="every $attribute in @ism:* satisfies normalize-space(string($attribute))"
                  flag="error">
        	[ISM-ID-00002][Error] For every attribute that is used in a 
        	document a non-null value must be present.
        </sch:assert>
    </sch:rule>
</sch:pattern>
