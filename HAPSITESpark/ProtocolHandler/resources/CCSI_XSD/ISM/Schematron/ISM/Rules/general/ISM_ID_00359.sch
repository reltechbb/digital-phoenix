<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00359">
    <sch:p class="ruleText">
        [ISM-ID-00359][Error] The classification of a tetragraph may not be greater 
        than the classification of the document.
    </sch:p>
    <sch:p class="codeDesc">
        For documents that use tetragraphs, this rule verifies that the classification of the tetragraph isn't greater
        than the classification of the document.
    </sch:p>
    <sch:rule context="*[@ism:resourceElement='true'][1]">
        <sch:let name="documentClassification" value="@ism:classification" />
        
        <sch:let name="tetrasWithOwnerProducer" 
            value="distinct-values(for $value in tokenize(@ism:ownerProducer,' ') return 
            if($catt//catt:Tetragraph[catt:TetraToken=$value]/@ism:ownerProducer) 
            then $value
            else null)"/>
 
        
        <sch:let name="moreRestrictiveTetras" value="for $tetra in $tetrasWithOwnerProducer return 
            if ($catt//catt:Tetragraph[catt:TetraToken=$tetra]/@ism:classification != $documentClassification) 
            then
                (if ($documentClassification = 'TS')
                then null
                else if ($catt//catt:Tetragraph[catt:TetraToken=$tetra]/@ism:classification = 'TS')
                then $tetra
                else if ($documentClassification = 'U')
                then $tetra
                else if ($documentClassification = 'C' and $catt//catt:Tetragraph[catt:TetraToken=$tetra]/@ism:classification = 'S')
                then $tetra
                else if ($documentClassification = 'R' and ($catt//catt:Tetragraph[catt:TetraToken=$tetra]/@ism:classification = 'C' or $catt//catt:Tetragraph[catt:TetraToken=$tetra]/@ism:classification = 'S'))
                then $tetra
                else
                null
                )
            else null"/>
        
        <sch:assert test="empty($moreRestrictiveTetras)" flag="error">
            [ISM-ID-00359][Error] A document using tetragraphs may not have a classification that is greater
            than the classification of the document. The following tetragraphs
            have a more restrictive classification than the document: 
            <sch:value-of select="string-join($moreRestrictiveTetras,', ')"/>.
        </sch:assert>

        <sch:assert test="exists($catt//catt:Tetragraphs)">CATT does not exist!</sch:assert>
    </sch:rule>
</sch:pattern>