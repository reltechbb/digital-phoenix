<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00300">
    <sch:p class="ruleText">
        [ISM-ID-00300][Error] DESVersion attributes must be specified as version 201609.
    </sch:p>
    <sch:p class="codeDesc">201
        DESVersion attributes must be specified as version 201609.
    </sch:p>
    <sch:rule context="*[@ism:DESVersion]">
        <sch:assert test="matches(@ism:DESVersion,'^201609(-.{1,23})?$')" flag="error">
            [ISM-ID-00300][Error] DESVersion attributes must be specified as version 201609.
        </sch:assert>
    </sch:rule>
</sch:pattern>
