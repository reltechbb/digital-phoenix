<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00103">
    <sch:p class="ruleText">
        [ISM-ID-00103][Error] At least one element must have attribute 
        resourceElement specified with a value of [true].
    </sch:p>
    <sch:p class="codeDesc">
        For the document, this rule ensures that at least one element specifies 
        attribute ism:resourceElement with a value of [true].
    </sch:p>
    <sch:rule context="/*[descendant-or-self::*[@ism:* except (@ism:ISMCATCESVersion)]]">
        <sch:assert test="some $token in //*[(@ism:*)] satisfies               $token/@ism:resourceElement=true()"
                  flag="error">
        	[ISM-ID-00103][Error] At least one element must have attribute 
        	resourceElement specified with a value of [true].
        </sch:assert>
    </sch:rule>
</sch:pattern>
