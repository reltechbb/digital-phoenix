<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern id="ISM-ID-00324" xmlns:sch="http://purl.oclc.org/dsdl/schematron">
    <sch:p class="ruleText">
        [ISM-ID-00324][Error] If a document is ISM_USGOV_RESOURCE, it must
        contain portion markings. 
        
        Human Readable: All valid ISM_USGOV_RESOURCE documents must
        also contain portion markings. 
    </sch:p>
    <sch:p class="codeDesc">
        Make sure that all ISM_USGOV_RESOURCE documents contain at least
        one portion mark if they are not uncaveated UNCLASSIFIED. 
        Allow compilation reason to suffice as an exemption from this rule.
    </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE and generate-id(.) = generate-id($ISM_RESOURCE_ELEMENT)
        and not(@ism:classification='U' and util:isUncaveatedAndNoFDR(.))
        and not(@ism:compilationReason)]">
        <sch:assert  
            test="count($partTags) &gt; 0"
            flag="error">[ISM-ID-00324][Error] If a document is ISM_USGOV_RESOURCE, it must
            contain portion markings. 
            
            Human Readable: All valid ISM_USGOV_RESOURCE documents must
            also contain portion markings. 
        </sch:assert>
    </sch:rule>  
    
</sch:pattern>
