<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00087">
    <sch:p class="ruleText"> [ISM-ID-00087][Error] Classified USA documents having SBU-NF Data must
        have NF at the resource level. </sch:p>
    <sch:p class="codeDesc"> If IC Markings System Register and Manual rules do not apply to the
        document then the rule does not apply and the rule returns true. If any element has
        attribute nonICmarkings specified with a value containing [SBU-NF], does not have attribute
        excludeFromRollup set to true, and the resourceElement has attribute classification
        specified with a value other than [U], this rule ensures that the resourceElement has
        attribute disseminationControls specified with a value containing [NF]. </sch:p>
    <sch:rule context="*[generate-id(.) = generate-id($ISM_RESOURCE_ELEMENT)]">
        <sch:assert
            test="
                if (not($ISM_USGOV_RESOURCE)) then
                    true()
                else
                    if (index-of($partNonICmarkings_tok, 'SBU-NF') &gt; 0 and not($bannerClassification = 'U')) then
                        (index-of($bannerDisseminationControls_tok, 'NF') &gt; 0)
                    else
                        true()"
            flag="error"> [ISM-ID-00087][Error] Classified USA documents having SBU-NF Data must
            have NF at the resource level. </sch:assert>
    </sch:rule>
</sch:pattern>
