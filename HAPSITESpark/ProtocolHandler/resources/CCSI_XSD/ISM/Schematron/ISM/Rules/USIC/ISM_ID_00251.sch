<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00251">
    <sch:p class="ruleText">
        [ISM-ID-00251][Error] If US IC resource, then attribute 
        @ism:noticeType must not be specified with a value of [COMSEC]. 
        
        Human Readable: COMSEC notices are not valid for US IC documents.
    </sch:p>
    <sch:p class="codeDesc">
    	If ISM_USIC_RESOURCE, for each element which has attribute 
    	@ism:noticeType specified, this rule ensures that attribute
    	@ism:noticeType is not specified with a value containing token
    	[COMSEC].
    </sch:p>
	  <sch:rule context="*[$ISM_USIC_RESOURCE and @ism:noticeType]">
        <sch:assert test="not(util:containsAnyTokenMatching(@ism:noticeType, 'COMSEC'))"
                  flag="error">
            [ISM-ID-00251][Error] If ISM_USIC_RESOURCE, then attribute 
            @ism:noticeType must not be specified with a value of [COMSEC]. 
            
            Human Readable: COMSEC notices are not valid for US IC documents.
        </sch:assert>
    </sch:rule>
</sch:pattern>
