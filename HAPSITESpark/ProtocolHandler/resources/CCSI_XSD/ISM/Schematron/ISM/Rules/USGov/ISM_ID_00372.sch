<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00372">
    <sch:p class="ruleText">
        [ISM-ID-00372][Error] If ISM_USGOV_RESOURCE and attribute nonICmarkings
        contains the name token [LES-NF] or [SBU-NF], then attribute disseminationControls
        must not contain the name token [NF], [REL], [EYES], [RELIDO], or [DISPLAYONLY].
        
        Human Readable: LES-NF and SBU-NF are incompatible with other Foreign Disclosure 
        and Release markings.
    </sch:p>
    <sch:p class="codeDesc">
        If the document is an ISM_USGOV_RESOURCE, for each element which
        specifies attribute ism:nonICmarkings with a value containing the token
        [LES-NF] or [SBU-NF] this rule ensures that attribute ism:disseminationControls is 
        not specified with a value containing the token [NF], [REL], [EYES], [RELIDO], or 
        [DISPLAYONLY].
    </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE                         
        and util:containsAnyOfTheTokens(@ism:nonICmarkings, ('LES-NF','SBU-NF'))]">  
        <sch:assert test="not(util:containsAnyOfTheTokens(@ism:disseminationControls, ('NF','REL','EYES','RELIDO','DISPLAYONLY')))"
            flag="error">
            [ISM-ID-00372][Error] LES-NF and SBU-NF are incompatible with other Foreign Disclosure 
            and Release markings.
        </sch:assert>
    </sch:rule>
</sch:pattern>
