<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00250">
	  <sch:p class="ruleText">
		[ISM-ID-00250][Error] If ISM_USGOV_RESOURCE, element Notice must specify
		attribute ism:noticeType or ism:unregisteredNoticeType.
		
		Human Readable: Notices must specify their type.
	</sch:p>
	  <sch:p class="codeDesc">
		This rule ensures for element ism:Notices must specify their type.
	</sch:p>
	  <sch:rule context="ism:Notice[$ISM_USGOV_RESOURCE]">
		    <sch:assert test="@ism:noticeType or @ism:unregisteredNoticeType" flag="error">
			[ISM-ID-00250][Error] If ISM_USGOV_RESOURCE, element Notice must specify
			attribute ism:noticeType or ism:unregisteredNoticeType.
			
			Human Readable: Notices must specify their type.
		</sch:assert>
	  </sch:rule>
</sch:pattern>
