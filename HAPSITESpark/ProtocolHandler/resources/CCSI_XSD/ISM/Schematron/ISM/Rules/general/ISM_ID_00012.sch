<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00012">
    <sch:p class="ruleText">
        [ISM-ID-00012][Error] If any of the attributes defined in 
        this DES other than DESVersion, ISMCATCESVersion, unregisteredNoticeType, or pocType 
        are specified for an element, then attributes classification and 
        ownerProducer must be specified for the element.
    </sch:p>
    <sch:p class="codeDesc">
    	For each element which defines an attribute in the ISM namespace other
    	than ism:pocType, ism:DESVersion, ism:ISMCATCESVersion, or ism:unregisteredNoticeType, this rule ensures that attributes ism:classification and ism:ownerProducer are
    	specified.
    </sch:p>
    <sch:rule context="*[@ism:* except (@ism:pocType | @ism:DESVersion | @ism:unregisteredNoticeType | @ism:ISMCATCESVersion)]">
        <sch:assert test="@ism:ownerProducer and @ism:classification" flag="error">
        	[ISM-ID-00012][Error] If any of the attributes defined in 
        	this DES other than ISMCATCESVersion, DESVersion, unregisteredNoticeType, or pocType 
        	are specified for an element, then attributes classification and 
        	ownerProducer must be specified for the element.
        </sch:assert>
    </sch:rule>
</sch:pattern>
