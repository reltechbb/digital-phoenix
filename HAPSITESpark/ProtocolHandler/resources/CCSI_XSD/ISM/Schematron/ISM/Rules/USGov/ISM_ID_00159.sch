<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00159">
    <sch:p class="ruleText">
        [ISM-ID-00159][Error] If ISM_USGOV_RESOURCE and:
        1. attribute classification of ISM_RESOURCE_ELEMENT is not [U]
        AND
        2. The attribute notice does contain [DoD-Dist-A] 
        or has attribute externalNotice with a value of [true].
        
        Human Readable: Distribution statement A (Public Release) is forbidden on classified documents.
    </sch:p>
    <sch:p class="codeDesc">
        If the document is an ISM_USGOV_RESOURCE and the attribute
        classification of ISM_RESOURCE_ELEMENT is not [U], for each element
        which specifies attribute ism:noticeType this rule ensures that attribute
        ism:noticeType is not specified with a value containing the token
        [DoD-Dist-A] unless it is an external notice with attribute ism:externalNotice is [true].
    </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE                          and not($ISM_RESOURCE_ELEMENT/@ism:classification = 'U')]">
        <sch:assert test="not(util:containsAnyOfTheTokens(@ism:noticeType, ('DoD-Dist-A')))                 or (@ism:externalNotice=true())"
                  flag="error"> 
            [ISM-ID-00159][Error] If ISM_USGOV_RESOURCE and:
        1. attribute classification of ISM_RESOURCE_ELEMENT is not [U]
        AND
        2. The attribute notice does contains [DoD-Dist-A]
        or has attribute externalNotice with a value of [true].
        Human Readable: Distribution statement A (Public Release) is forbidden on classified documents.
        </sch:assert>
    </sch:rule>
</sch:pattern>
