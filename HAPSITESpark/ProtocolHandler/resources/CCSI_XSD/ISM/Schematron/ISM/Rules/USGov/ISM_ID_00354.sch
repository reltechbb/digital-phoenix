<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00354" is-a="NtkHasCorrespondingData">

   <sch:p class="ruleText">[ISM-ID-00354][Error] The banner or a portion that contributes to roll-up must contain [XD]
      if EXDIS NTK metadata exists.</sch:p>

   <sch:p class="codeDesc">Invokes abstract rule NtkHasCorrespondingData.</sch:p>

   <sch:param name="ruleId" value="'ISM-ID-00354'"/>
   <sch:param name="policyName" value="'EXDIS'"/>
   <sch:param name="uriPrefix" value="'urn:us:gov:ic:aces:ntk:xd'"/>
   <sch:param name="attr" value="'nonICmarkings'"/>
   <sch:param name="dataType" value="'XD'"/>
   <sch:param name="dataTokenList" value="$partNonICmarkings_tok"/>
   <sch:param name="bannerTokenList" value="$bannerNonICmarkings_tok"/>
</sch:pattern>
