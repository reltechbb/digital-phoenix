<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00299">
    <sch:p class="ruleText">
        [ISM-ID-00299][Error] If an element contains the attribute declassException with a value of [AEA], 
        it must also contain the attribute atomicEnergyMarkings.
    </sch:p>
	  <sch:p class="codeDesc">
		If an element contains an ism:declassException attribute with a value containing
		AEA, this rule checks to make sure that element also has an ism:atomicEnergyMarkings
		attribute.
	</sch:p>
	  <sch:rule context="*[util:containsAnyTokenMatching(@ism:declassException, ('AEA'))]">
		    <sch:assert test="@ism:atomicEnergyMarkings" flag="error">
			[ISM-ID-00299][Error] If an element contains the attribute declassException with a value of [AEA], 
			it must also contain the attribute atomicEnergyMarkings.
		</sch:assert>
	  </sch:rule>
</sch:pattern>
