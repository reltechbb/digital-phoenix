<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00301">
  <sch:p class="ruleText">
    [ISM-ID-00301][Error] If ISM_USGOV_RESOURCE and attribute SCIcontrols contains any of the name tokens [EL],
    [EL-EU], or [EL-NK], then it must also contain the name token [SI].
    
    Human Readable: A USA document that contains ENDSEAL (EL), -ECRU (EK), or -NONBOOK (NK) compartment data must also specify that 
    it contains SI data. 
  </sch:p>
  <sch:p class="codeDesc">
    If the document is an ISM_USGOV_RESOURCE, for each element which specifies
    attribute ism:SCIcontrols with a value containing any of the tokens [EL], 
    [EL-EU], or [EL-NK], this rule ensures that attribute ism:SCIcontrols is 
    specified with a value containing the token [SI].
    
    If IC Markings System Register and Manual rules do not apply to the document then the rule does not apply
    and the rule returns true. 
  </sch:p>
  <sch:rule context="*[$ISM_USGOV_RESOURCE and util:containsAnyOfTheTokens(@ism:SCIcontrols, ('EL', 'EL-EU', 'EL-NK'))]">
      <sch:assert test="util:containsAnyTokenMatching(@ism:SCIcontrols, ('SI'))"
                  flag="error">
      [ISM-ID-00301][Error] If ISM_USGOV_RESOURCE and attribute SCIcontrols contains any of the name tokens [EL],
      [EL-EU], or [EL-NK], then it must also contain the name token [SI].
      
      Human Readable: A USA document that contains ENDSEAL (EL), -ECRU (EK), or -NONBOOK (NK) compartment data must also specify that 
      it contains SI data. 
    </sch:assert>
  </sch:rule>
</sch:pattern>
