<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00085" is-a="AttributeContributesToRollupWithException">
    <sch:p class="ruleText">
        [ISM-ID-00085][Error] If ISM_USGOV_RESOURCE and any element meeting ISM_CONTRIBUTES in the document 
        has the attribute nonICmarkings containing [XD] and does not have any element meeting ISM_CONTRIBUTES in the document having the 
        attribute nonICmarkings containing [ND] then the ISM_RESOURCE_ELEMENT must have nonICmarkings containing [XD].
        
        Human Readable: USA documents having XD Data and not having ND must have XD at the resource level.
    </sch:p>
    <sch:p class="codeDesc">
        This rule uses an abstract pattern to consolidate logic. For details on the code
        description, see the abstract pattern.
    </sch:p>
    <sch:param name="attrLocalName" value="nonICmarkings"/>
    <sch:param name="exceptAttrLocalName" value="nonICmarkings"/>
    <sch:param name="value" value="XD"/>
    <sch:param name="exceptValueList" value="('ND')"/>
    <sch:param name="errorMessage"
        value="'[ISM-ID-00085][Error] USA documents having XD Data and not having ND must have XD at the resource level.'"/>
</sch:pattern>
