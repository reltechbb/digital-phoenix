<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00272">
	  <sch:p class="ruleText">
		[ISM-ID-00272][Error] All compilationReason attributes must be a string with less than 1024 characters. 
	</sch:p>
	  <sch:p class="codeDesc">
	  	For all elements which contain an compilationReason attribute, this rule ensures that the compilationReason value is a string with less
		than 1024 characters.   
	</sch:p>
	  <sch:rule context="*[@ism:compilationReason]">
		    <sch:assert test="string-length(@ism:compilationReason) &lt;= 1024" flag="error">
			[ISM-ID-00272][Error] All compilationReason attributes must be a string with less than 1024 characters. 
		</sch:assert>
	  </sch:rule>
</sch:pattern>
