<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00242">
    <sch:p class="ruleText">
        [ISM-ID-00242][Error] If ISM_USGOV_RESOURCE and attribute SCIcontrols contains the name token [RSV],
        then it must also have attribute classification with a value of [S] or [TS].
        
        Human Readable: A USA document that contains RESERVE data must be classified SECRET or TOP SECRET.
    </sch:p>
    <sch:p class="codeDesc">
      If the document is an ISM_USGOV_RESOURCE, for each element which specifies
      attribute ism:SCIcontrols with a value containing the token [RSV], this rule ensures that attribute ism:classification is specified with a value containing
      the token [TS] or [S].
    </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE and util:containsAnyOfTheTokens(@ism:SCIcontrols, ('RSV'))]">
        <sch:assert test="util:containsAnyOfTheTokens(@ism:classification, ('TS', 'S'))"
                  flag="error">
            [ISM-ID-00242][Error] If ISM_USGOV_RESOURCE and attribute SCIcontrols contains the name token [RSV],
            then it must also have attribute classification with a value of [S] or [TS].
            
            Human Readable: A USA document that contains RESERVE data must be classified SECRET or TOP SECRET. 
        </sch:assert>
    </sch:rule>
</sch:pattern>
