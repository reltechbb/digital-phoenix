<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00362">
    <sch:p class="ruleText">
        [ISM-ID-00362][Error] HCS-P-subs cannot be used with OC-USGOV 
    </sch:p>
    <sch:p class="codeDesc">
        When OC-USGOV disseminationControls is used, tokens matching the regular expression 
        HCS-P-[A-Z0-9]{1,6} cannot be in SCIcontrols.
    </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE and util:containsAnyOfTheTokens(@ism:disseminationControls, ('OC-USGOV')) and @ism:SCIcontrols]">
        <sch:assert test="not(util:getStringFromSequenceWithOnlyRegexValues(@ism:SCIcontrols, 'HCS-P-[A-Z0-9]{1,6}'))"
            flag="error">
            [ISM-ID-00362][Error] HCS-P-subs cannot be used with OC-USGOV.
        </sch:assert>
    </sch:rule>
</sch:pattern>