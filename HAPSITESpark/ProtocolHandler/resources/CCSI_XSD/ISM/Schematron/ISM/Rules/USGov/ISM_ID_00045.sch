<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00045">
    <sch:p class="ruleText">
        [ISM-ID-00045][Error] If ISM_USGOV_RESOURCE and attribute SCIcontrols
        contains a name token starting with [SI-G], then attribute
        disseminationControls must contain the name token [OC].
        
        Human Readable: A USA document containing Special Intelligence (SI)
        GAMMA compartment data must be marked for ORIGINATOR CONTROLLED 
        dissemination.
    </sch:p>
    <sch:p class="codeDesc">
        If the document is an ISM_USGOV_RESOURCE, for each element which
        specifies attribute ism:SCIcontrols with a value containing a token
        starting with [SI-G] this rule ensures that attribute
        ism:disseminationControls is specified with a value containing the
        token [OC].
    </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE                         and util:containsAnyTokenMatching(@ism:SCIcontrols, ('^SI-G$'))]">
      <sch:assert test="util:containsAnyOfTheTokens(@ism:disseminationControls, ('OC'))"
                  flag="error">
            [ISM-ID-00045][Error] If ISM_USGOV_RESOURCE and attribute SCIcontrols
            contains a name token starting with [SI-G], then attribute
            disseminationControls must contain the name token [OC].
            
            Human Readable: A USA document containing Special Intelligence (SI)
            GAMMA compartment data must be marked for ORIGINATOR CONTROLLED 
            dissemination.
        </sch:assert>
    </sch:rule>
</sch:pattern>
