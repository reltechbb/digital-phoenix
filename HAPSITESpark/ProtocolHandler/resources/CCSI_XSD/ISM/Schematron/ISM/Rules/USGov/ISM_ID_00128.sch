<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00128"
    is-a="DataHasCorrespondingNoticeWithException">
    <sch:p class="ruleText">[ISM-ID-00128][Error] USA documents containing FRD data must have a
        non-external FRD notice unless the document contains RD in the banner.</sch:p>

    <sch:p class="codeDesc">This rule depends on the DataHasCorrespondingNoticeWithException
        abstract pattern to enforce that FRD documents have an FRD notice unless the banner has RD.
        See DataHasCorrespondingNoticeWithException for details.</sch:p>
    <sch:param name="ruleId" value="'ISM-ID-00128'"/>
    <sch:param name="attrName" value="'atomicEnergyMarkings'"/>
    <sch:param name="attrValue" value="$ISM_RESOURCE_ELEMENT/@ism:atomicEnergyMarkings"/>
    <sch:param name="noticeType" value="'FRD'"/>
    <sch:param name="exceptAttrValue" value="$ISM_RESOURCE_ELEMENT/@ism:atomicEnergyMarkings"/>
    <sch:param name="exceptNoticeType" value="'RD'"/>
</sch:pattern>
