<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron"
             xmlns:ism="urn:us:gov:ic:ism"
             id="ISM-ID-00334"
             ism:resourceElement="true"
             ism:createDate="2011-01-27"
             ism:classification="U"
             ism:ownerProducer="USA">
  <sch:p class="ruleText" ism:classification="U" ism:ownerProducer="USA">
    [ISM-ID-00334][Error] Rule removed in 2016-SEP.
  </sch:p>
  <sch:p class="codeDesc" ism:classification="U" ism:ownerProducer="USA"/>
</sch:pattern>
