<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00037">
    <sch:p class="ruleText">[ISM-ID-00037][Error] When ISM_USGOV_RESOURCE and @ism:nonICmarkings
        contains [SBU] or [SBU-NF] then @ism:classification must equal [U]. </sch:p>

    <sch:p class="ruleText">Human Readable: SBU and SBU-NF data must be marked
        UNCLASSIFIED on the banner in USA documents.</sch:p>

    <sch:p class="codeDesc">For a resource element (@ism:resourceElement="true"), if
        @ism:compliesWith contains ‘USGov’ and @ism:nonICmarkings contains [SBU] or [SBU-NF] then
        @ism:classification must equal [U].</sch:p>

    <sch:rule
        context="*[$ISM_USGOV_RESOURCE and @ism:resourceElement=true() and
                   util:containsAnyOfTheTokens(@ism:nonICmarkings, ('SBU', 'SBU-NF'))]">
        <sch:assert test="@ism:classification='U'" flag="error">[ISM-ID-00037][Error] When
            ISM_USGOV_RESOURCE and @ism:nonICmarkings contains [SBU] or [SBU-NF] then
            @ism:classification must equal [U].</sch:assert>
    </sch:rule>
</sch:pattern>
