<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron"
             id="ISM-ID-00257"
             is-a="ValidateTokenValuesExistenceInList">
    <sch:p class="ruleText">
    	[ISM-ID-00257][Error] All @ism:displayOnlyTo values must
    	be defined in CVEnumISMRelTo.xml.
    </sch:p>

	  <sch:p class="codeDesc">
		This rule uses an abstract pattern to consolidate logic. It checks that the
		value in parameter $searchTerm is contained in the parameter $list. The parameter
		$searchTerm is relative in scope to the parameter $context. The value for the parameter 
		$list is a variable defined in the main document (ISM_XML.sch), which reads 
		values from a specific CVE file.
	</sch:p>
	
	  <sch:param name="context" value="*[@ism:displayOnlyTo]"/>
	  <sch:param name="searchTermList" value="@ism:displayOnlyTo"/>
	  <sch:param name="list" value="$displayOnlyToList"/>
	  <sch:param name="errMsg"
              value="'   [ISM-ID-00257][Error] All @ism:displayOnlyTo values must   be defined in CVEnumISMCATRelTo.xml.   '"/>
</sch:pattern>
