<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00364">
    <sch:p class="ruleText">
        [ISM-ID-00364][Error] If an ISM_USGOV_RESOURCE has a value in @compilationReason and @noAggregation is present,
        @noAggregation must be false.
    </sch:p>
    <sch:p class="codeDesc">
        If an ISM_USGOV_RESOURCE has a value in @compilationReason and @noAggregation is present,
        @noAggregation must be false.
    </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE and string-length(normalize-space(@ism:compilationReason)) > 0 and string-length(normalize-space(@ism:noAggregation)) > 0]">
        <sch:assert test="@ism:noAggregation = 'false' "
            flag="error">
            [ISM-ID-00364][Error] If an ISM_USGOV_RESOURCE has a value in @compilationReason and @noAggregation is present,
            @noAggregation must be false.
        </sch:assert>
    </sch:rule>
</sch:pattern>