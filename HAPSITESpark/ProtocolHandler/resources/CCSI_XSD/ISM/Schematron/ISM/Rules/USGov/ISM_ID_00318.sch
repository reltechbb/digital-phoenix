<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?> 
<sch:pattern id="ISM-ID-00318" xmlns:sch="http://purl.oclc.org/dsdl/schematron"  is-a="CheckCommonCountries">
   <sch:p class="ruleText">
      [ISM-ID-00318][Error] Rollup compilation does not meet CAPCO guidance.
   </sch:p>
   <sch:p class="codeDesc">
      Where an element is the resource element and contains the @ism:releasableTo attribute
      check that the values specified meet minimum rollup conditions. Check all contributing
      portions against the banner for the existence of common countries ensuring that the 
      countries in the banner are the intersection of all contributing portions. The tetragraphs
      for TEYE, ACGU, and FVEY will be decomposed into their representative countries. All other
      tetragraphs will be treated as a wildcard of all countries since the membership is not 
      known.
      
      Once the minimum possibility of intersecting countries is determined the rule checks that
      there is not a portion for which the banner is not a subset. Then it checks for the case
      of no common countries that can be rollup up to the resource element. There is a check that
      if the banner countries are a subset of the common countries that a compilationReason is 
      specified. If compilationReason is not specified then the banner releasableTo countries
      must be the set of common countries from all contributing portions.
   </sch:p>
   
   <sch:param name="ruleId" value="'ISM-ID-00318'"/>
   <sch:param name="attrLocalName" value="'releasableTo'"/>
   <sch:param name="actualBannerTokens" value="$relToActualBannerTokens"/> 
   <sch:param name="calculatedBannerTokens" value="$relToCalculatedBannerTokens"/>
   
</sch:pattern>
