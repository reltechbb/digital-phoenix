<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00157">
    <sch:p class="ruleText"> [ISM-ID-00157][Error] If ISM_USDOD_RESOURCE and: 
        1. The attribute notice contains one of the [DoD-Dist-B], [DoD-Dist-C], [DoD-Dist-D], or [DoD-Dist-E] 
          AND
        2. The attribute noticeReason is not specified. 
        
        Human Readable: DoD distribution statements B, C, D , or E all require a reason. </sch:p>
    <sch:p class="codeDesc"> If the document is an ISM_USDOD_RESOURCE, for each element which
        specifies attribute ism:noticeType with a value containing the token [DoD-Dist-B],
        [DoD-Dist-C], [DoD-Dist-D], or [DoD-Dist-E], this rule ensures that attribute
        ism:noticeReason is specified. </sch:p>
    <sch:rule
        context="*[$ISM_USDOD_RESOURCE and util:containsAnyOfTheTokens(@ism:noticeType, ('DoD-Dist-B', 'DoD-Dist-C', 'DoD-Dist-D', 'DoD-Dist-E'))]">
        <sch:assert test="@ism:noticeReason" flag="error"> [ISM-ID-00157][Error] If
            ISM_USDOD_RESOURCE and: 1. The attribute notice contains one of the [DoD-Dist-B],
            [DoD-Dist-C], [DoD-Dist-D], or [DoD-Dist-E] AND 2. The attribute noticeReason is not
            specified. Human Readable: DoD distribution statements B, C, D , or E all require a
            reason. </sch:assert>
    </sch:rule>
</sch:pattern>
