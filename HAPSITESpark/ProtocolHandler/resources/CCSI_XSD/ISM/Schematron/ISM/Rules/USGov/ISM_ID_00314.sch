<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00314">
    <sch:p class="ruleText">
        [ISM-ID-00314][Error] If nonICmarkings contains the token [XD] then the 
        attribute disseminationControls must contain [NF].
        
        Human Readable: EXDIS data must be marked NOFORN.
    </sch:p>
    <sch:p class="codeDesc">
        If the nonICmarkings contains the ND token, then check that the disseminationControls
        attribute must have NF specified.
    </sch:p>
    <sch:rule context="*[util:containsAnyOfTheTokens(@ism:nonICmarkings, ('XD'))]">
        <sch:assert test="util:containsAnyOfTheTokens(@ism:disseminationControls, ('NF'))"
                  flag="error">
            [ISM-ID-00314][Error] If nonICmarkings contains the token [XD] then the 
            attribute disseminationControls must contain [NF].
            
            Human Readable: EXDIS data must be marked NOFORN.
        </sch:assert>
    </sch:rule>
</sch:pattern>
