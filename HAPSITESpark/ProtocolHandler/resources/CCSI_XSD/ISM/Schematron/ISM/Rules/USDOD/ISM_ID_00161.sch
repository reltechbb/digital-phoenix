<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00161">
    
    <sch:p class="ruleText">
        [ISM-ID-00161][Error] If the document is an
        1. ISM_USDOD_RESOURCE AND
        2. the attribute notice of ISM_RESOURCE_ELEMENT contains [DoD-Dist-A] AND
        3. no portions in the document have their attribute excludeFromRollup set to [true]
        THEN there must not be any attribute nonICmarkings present.
        
        Human Readable: Distribution statement A (Public Release) is 
        incompatible with any nonICMarkings if excludeFromRollup is not TRUE.
    </sch:p>
    
    <sch:p class="codeDesc">
        If the document is an ISM_USDOD_RESOURCE and @ism:noticeType contains 'DoD-Dist-A' 
        and no portions in the document have their @ism:excludeFromRollup set to true, 
        then there must not be any @ism:nonICMarkings present.
    </sch:p>
    
	  <sch:rule context="*[$ISM_USDOD_RESOURCE and (util:containsAnyOfTheTokens($ISM_RESOURCE_ELEMENT/@ism:noticeType, ('DoD-Dist-A')))
	      and not (@ism:excludeFromRollup=true())]">
		    <sch:assert test="not(@ism:nonICmarkings)"
                  flag="error"> 
		        [ISM-ID-00161][Error] Distribution statement A (Public Release) is incompatible with any nonICMarkings 
		        if excludeFromRollup is not TRUE.
        </sch:assert>
    </sch:rule>
    
</sch:pattern>
