<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00030">
    <sch:p class="ruleText">
        [ISM-ID-00030][Error] If ISM_USGOV_RESOURCE and attribute 
        disseminationControls contains the name token [FOUO], then attribute
        classification must have a value of [U].
        
        Human Readable: Portions marked for FOUO dissemination in a USA document
        must be classified UNCLASSIFIED.
    </sch:p>
    <sch:p class="codeDesc">
        If the document is an ISM_USGOV_RESOURCE, for each element which has 
    	attribute ism:disseminationControls specified with a value containing
    	the token [FOUO] this rule ensures that attribute ism:classification is 
    	specified with a value of [U].
    </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE                      and util:containsAnyOfTheTokens(@ism:disseminationControls, ('FOUO'))]">
        <sch:assert test="@ism:classification='U'" flag="error">
            [ISM-ID-00030][Error] If ISM_USGOV_RESOURCE and attribute 
        	disseminationControls contains the name token [FOUO], then attribute
        	classification must have a value of [U].
        	
        	Human Readable: Portions marked for FOUO dissemination in a USA document
        	must be classified UNCLASSIFIED.
        </sch:assert>
    </sch:rule>
</sch:pattern>
