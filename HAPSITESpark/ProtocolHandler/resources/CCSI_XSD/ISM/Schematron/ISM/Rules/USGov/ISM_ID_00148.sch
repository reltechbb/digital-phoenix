<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron"
             id="ISM-ID-00148"
             is-a="MutuallyExclusiveAttributeValues">
    <sch:p class="ruleText">
        [ISM-ID-00148][Error] If ISM_USGOV_RESOURCE, then Name tokens [LES] and [LES-NF] are mutually
        exclusive for attribute nonICmarkings.
        
        Human Readable: USA documents must not specify both LES and LES-NF on a single element.
    </sch:p>
	  <sch:p class="codeDesc">
		This rule uses an abstract pattern to consolidate logic.
		If the document is an ISM_USGOV_RESOURCE, for each element which 
		has attribute ism:nonICmarkings specified with a value containing
		the token [LES] or [LES-NF], this rule ensures that attribute
		ism:nonICmarkings is not specified with a value containing both tokens
		[LES] and [LES-NF].
	</sch:p>
	
	  <sch:param name="context"
              value="*[$ISM_USGOV_RESOURCE          and util:containsAnyOfTheTokens(@ism:nonICmarkings, ('LES', 'LES-NF'))]"/>
	  <sch:param name="attrValue" value="@ism:nonICmarkings"/>
	  <sch:param name="mutuallyExclusiveTokenList" value="('LES', 'LES-NF')"/>
	  <sch:param name="errMsg"
              value="'   [ISM-ID-00148][Error] If ISM_USGOV_RESOURCE, then Name tokens    [LES] and [LES-NF] are mutually exclusive for attribute nonICmarkings.      Human Readable: USA documents must not specify both LES and LES-NF    on a single element.   '"/>
</sch:pattern>
