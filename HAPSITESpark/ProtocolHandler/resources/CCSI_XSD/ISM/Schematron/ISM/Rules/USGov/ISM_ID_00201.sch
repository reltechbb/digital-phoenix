<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron"
             id="ISM-ID-00201"
             is-a="AttributeValueDeprecatedError">
    <sch:p class="ruleText">
    	[ISM-ID-00201][Error] Attribute displayOnlyTo must not contain values 
    	which have passed their deprecation date.
    </sch:p>
	  <sch:p class="codeDesc">
	  	For each element which specifies attribute ism:displayOnlyTo, this rule ensures that the value of ism:displayOnlyTo has not been deprecated. 
		This is indicated in the CVE file by an attribute (@deprecated) 
		on the term element for that displayOnlyTo value. 
		If the date value in @deprecated is in the future, then a 
		deprecation warning will be given. If the date value has 
		already occurred, then a deprecation error will be given. 
	</sch:p>
	
	  <sch:param name="ruleId" value="'ISM-ID-00201'"/>
	  <sch:param name="context" value="*[@ism:displayOnlyTo]"/>
	  <sch:param name="attrName" value="displayOnlyTo"/>
	  <sch:param name="cveName" value="CVEnumISMCATRelTo"/>
	  <sch:param name="cveSpec" value="ISMCAT"/>
</sch:pattern>
