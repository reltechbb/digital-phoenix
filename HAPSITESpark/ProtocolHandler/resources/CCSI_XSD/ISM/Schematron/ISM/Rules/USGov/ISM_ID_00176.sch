<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00176">

    <sch:p class="ruleText">

        [ISM-ID-00176][Error] If ISM_USGOV_RESOURCE and attribute 

        atomicEnergyMarkings has a name token containing [RD] or [FRD], 

        then attributes declassDate and declassEvent cannot be specified

        on the resourceElement.

        

        Human Readable: Automatic declassification of documents containing 

        RD or FRD information is prohibited. Attributes declassDate and 

        declassEvent cannot be used in the classification authority block when 

        RD or FRD is present.

    </sch:p>

    <sch:p class="codeDesc">

    	If the document is an ISM_USGOV_RESOURCE, for each element which 

    	has attribute ism:atomicEnergyMarkings specified with a value containing

the token [RD] or [FRD], this rule ensures that the resourceElement does not

    	have attributes ism:declassDate or ism:declassEvent specified.

    </sch:p>

	
  <sch:rule context="*[$ISM_USGOV_RESOURCE                        and util:containsAnyOfTheTokens(@ism:atomicEnergyMarkings, ('RD', 'FRD'))]">

        <sch:assert test="not($ISM_RESOURCE_ELEMENT/@ism:declassDate or $ISM_RESOURCE_ELEMENT/@ism:declassEvent)"
                  flag="error">

        	[ISM-ID-00176][Error] Automatic declassification of documents containing 

        	RD or FRD information is prohibited. Attributes declassDate and 

        	declassEvent cannot be used in the classification authority block when 

        	RD or FRD is present.

        </sch:assert>

    </sch:rule>

</sch:pattern>
