<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00118">
    <sch:p class="ruleText">
        [ISM-ID-00118][Error] The first element in document order having 
        resourceElement true must have createDate specified.
    </sch:p>
    <sch:p class="codeDesc">
        This rule ensures that the resourceElement has attribute createDate specified.
    </sch:p>
    <sch:rule context="*[generate-id(.) = generate-id($ISM_RESOURCE_ELEMENT)][1]">
        <sch:assert test="@ism:createDate" flag="error">
            [ISM-ID-00118][Error] The first element in document order having 
            resourceElement true must have createDate specified.
        </sch:assert>
    </sch:rule>
</sch:pattern>
