<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00214">

    <sch:p class="ruleText">

        [ISM-ID-00214][Error] If ISM_USGOV_RESOURCE then attribute 

        releasableTo must start with [USA].

    </sch:p>

    <sch:p class="codeDesc">

        If the document is an ISM_USGOV_RESOURCE, for each element which

        specifies attribute releasableTo this rule ensures that attribute

        releasableTo is specified with a value that starts with the token [USA].

    </sch:p>

    <sch:rule context="*[$ISM_USGOV_RESOURCE and @ism:releasableTo]">

        <sch:assert test="index-of(tokenize(normalize-space(string(@ism:releasableTo)),' '),'USA')=1"
                  flag="error">

            [ISM-ID-00214][Error] If ISM_USGOV_RESOURCE then attribute 

            releasableTo must start with [USA].

        </sch:assert>

    </sch:rule>

</sch:pattern>
