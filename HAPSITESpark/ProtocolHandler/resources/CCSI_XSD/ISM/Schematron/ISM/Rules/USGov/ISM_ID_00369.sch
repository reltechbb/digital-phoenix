<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00369">
    <sch:p class="ruleText">
        [ISM-ID-00369][Error] If ISM_USGOV_RESOURCE and attribute SCIcontrols
        contains the name token [TK-BLFH], then attribute disseminationControls
        must contain the name token [NF].
        
        Human Readable: A USA document containing TALENT KEYHOLE (TK) -BLUEFISH compartment data must also be
        marked for NO FOREIGN dissemination.
    </sch:p>
    <sch:p class="codeDesc">
        If the document is an ISM_USGOV_RESOURCE, for each element which
        specifies attribute ism:SCIcontrols with a value containing the token
        [TK-BLFH] this rule ensures that attribute ism:disseminationControls is 
        specified with a value containing the token [NF].
    </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE                         and util:containsAnyOfTheTokens(@ism:SCIcontrols, ('TK-BLFH'))]">  
        <sch:assert test="util:containsAnyOfTheTokens(@ism:disseminationControls, ('NF'))"
            flag="error">
            [ISM-ID-00369][Error] If ISM_USGOV_RESOURCE and attribute SCIcontrols
            contains the name token [TK-BLFH], then attribute disseminationControls
            must contain the name token [NF].
            
            Human Readable: A USA document containing TALENT KEYHOLE (TK) -BLUEFISH compartment data must also be
            marked for NO FOREIGN dissemination.
        </sch:assert>
    </sch:rule>
</sch:pattern>
