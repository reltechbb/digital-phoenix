<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00321"
	is-a="MutuallyExclusiveAttributeValues">
	<sch:p class="ruleText"> [ISM-ID-00321][Error] If ISM_USGOV_RESOURCE, then tokens [RD], [FRD] and
		[TFNI] are mutually exclusive for attribute atomicEnergyMarkings. Human Readable: RD, FRD, and
		TFNI are mutually exclusive and cannot be commingled in a portion mark or in the banner line. </sch:p>
	<sch:p class="codeDesc"> This rule uses an abstract pattern to consolidate logic. If the document
		is an ISM_USGOV_RESOURCE, for each element which has attribute ism:atomicEnergyMarkings
		specified with a value containing the token [RD], [FRD] or [TFNI], this rule ensures that attribute
		ism:disseminationControls is specified with a value containing only one of the tokens [RD],
		[FRD] or [TFNI]. </sch:p>

	<sch:param name="context"
		value="*[$ISM_USGOV_RESOURCE         and util:containsAnyOfTheTokens(@ism:atomicEnergyMarkings, ('RD', 'FRD', 'TFNI'))]"/>
	<sch:param name="attrValue" value="@ism:atomicEnergyMarkings"/>
	<sch:param name="mutuallyExclusiveTokenList" value="('RD', 'FRD', 'TFNI')"/>
	<sch:param name="errMsg"
		value="'         [ISM-ID-00321][Error] If ISM_USGOV_RESOURCE, then tokens [RD],      
         [FRD] and [TFNI] are mutually exclusive for attribute atomicEnergyMarkings.
        Human Readable: RD, FRD and TFNI are mutually exclusive and cannot be commingled
        in a portion mark or in the banner line.         '"
	/>
</sch:pattern>
