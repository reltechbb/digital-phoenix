<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00294">
	  <sch:p class="ruleText">
	  	[ISM-ID-00294][Error] All resourceElement attributes must be of type Boolean. 
	</sch:p>
	  <sch:p class="codeDesc">
	  	For all elements which contain an resourceElement attribute, this rule ensures that the resourceElement value matches the pattern
		defined for type Boolean. 
		
		Note: this rule is not able to be failed. If the resourceElement does
		not confirm to type Boolean, schematron fails when defining global
		variables before any rules are fired. 
	</sch:p>
	  <sch:rule context="*[@ism:resourceElement]">
		    <sch:assert test="util:meetsType(@ism:resourceElement, $BooleanPattern)"
                  flag="error">
		    	[ISM-ID-00294][Error] All resourceElement attributes values must be of type Boolean. 
		</sch:assert>
	  </sch:rule>
</sch:pattern>
