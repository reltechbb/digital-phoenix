<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00322">
    <sch:p class="ruleText">
        [ISM-ID-00322][Error] The @ism:ISMCATCESVersion imported by ISM must be greater than or equal to 201609.
        
        Human Readable: The ISMCAT version imported by ISM must be greater than or equal to 2016-SEP. 
    </sch:p>
    <sch:p class="codeDesc">
        For all elements that contain @ism:ISMCATCESVersion, this rule ensures that the version
        is greater than or equal to the minimum allowed version: 201609. 
    </sch:p>
    <sch:rule context="*[@ism:ISMCATCESVersion]">
        <sch:let name="version" value="number(if (contains(@ism:ISMCATCESVersion,'-')) then substring-before(@ism:ISMCATCESVersion,'-') else @ism:ISMCATCESVersion)"/>
        <sch:assert test="$version &gt;= 201609" flag="error">
            [ISM-ID-00322][Error] The @ism:ISMCATCESVersion imported by ISM must be greater than or equal to 201609.
            
            Human Readable: The ISMCAT version imported by ISM must be greater than or equal to 2016-SEP. 
        </sch:assert>
    </sch:rule>
</sch:pattern>
