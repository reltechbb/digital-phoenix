<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00225">
    <sch:p class="ruleText">
        [ISM-ID-00225][Error] If subject to IC rules, then attribute 
        nonICmarkings must not be specified with a value containing any name 
        token starting with [ACCM] or [NNPI]. 
        
        Human Readable: ACCM and NNPI tokens are not valid for documents that are subject
        to IC rules.
    </sch:p>
    <sch:p class="codeDesc">
        If ISM_USIC_RESOURCE, for each element which has attribute 
    	ism:nonICmarkings specified, this rule ensures that attribute
    	ism:nonICmarkings is not specified with a value containing a token
    	which starts with [ACCM] or [NNPI].
    </sch:p>
    <sch:rule context="*[$ISM_USIC_RESOURCE and @ism:nonICmarkings and util:contributesToRollup(.)]">
        <sch:assert test="not(util:containsAnyTokenMatching(@ism:nonICmarkings, ('ACCM', 'NNPI')))"
                  flag="error">
            [ISM-ID-00225][Error] If not exempt from IC rules, then attribute 
            nonICmarkings must not be specified with a value containing any name 
            token starting with [ACCM] or [NNPI]. 
            
            Human Readable: ACCM and NNPI tokens are not valid for documents that are
            subject to IC rules.
        </sch:assert>
    </sch:rule>
</sch:pattern>
