<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00353" is-a="NtkHasCorrespondingData">
   
   <sch:p class="ruleText">[ISM-ID-00353][Error] Banner or portion that contributes to roll-up must contain [OC] if
      ORCON NTK metadata exists.</sch:p>
   
   <sch:p class="codeDesc">Invokes abstract rule NtkHasCorrespondingData.</sch:p>
   
   <sch:param name="ruleId" value="'ISM-ID-00353'"/>
   <sch:param name="policyName" value="'ORCON'"/>
   <sch:param name="uriPrefix" value="'urn:us:gov:ic:aces:ntk:oc'"/>
   <sch:param name="attr" value="'disseminationControls'"/>
   <sch:param name="dataType" value="'OC'"/>
   <sch:param name="dataTokenList" value="$partDisseminationControls_tok"/>
   <sch:param name="bannerTokenList" value="$bannerDisseminationControls_tok"/>
</sch:pattern>
