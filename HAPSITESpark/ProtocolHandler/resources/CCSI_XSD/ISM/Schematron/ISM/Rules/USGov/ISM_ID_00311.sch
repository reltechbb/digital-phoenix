<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00311">
    <sch:p class="ruleText">
        [ISM-ID-00311][Error] If ISM_USGOV_RESOURCE and attribute SCIcontrols contains the name token [EL-NK],
        then it must also contain the name token [EL].
        
        Human Readable: A USA document that contains ENDSEAL (EL) -NONBOOK compartment data must also specify that 
        it contains EL data. 
    </sch:p>
    <sch:p class="codeDesc">
        If the document is an ISM_USGOV_RESOURCE, for each element which
        specifies attribute ism:SCIcontrols with a value containing the token
        [EL-NK] this rule ensures that attribute ism:SCIcontrols is 
        specified with a value containing the token [EL].
    </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE         and util:containsAnyOfTheTokens(@ism:SCIcontrols, ('EL-NK'))]">
        <sch:assert test="util:containsAnyOfTheTokens(@ism:SCIcontrols, ('EL'))"
                  flag="error">
            [ISM-ID-00311][Error] If ISM_USGOV_RESOURCE and attribute SCIcontrols contains the name token [EL-NK],
            then it must also contain the name token [EL].
            
            Human Readable: A USA document that contains ENDSEAL (EL) -NONBOOK compartment data must also specify that 
            it contains EL data. 
        </sch:assert>
    </sch:rule>
</sch:pattern>
