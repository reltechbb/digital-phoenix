<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron"
             id="ISM-ID-00075"
             is-a="AttributeContributesToRollupWithException">
  <sch:p class="ruleText">
    [ISM-ID-00075][Error] If ISM_USGOV_RESOURCE and any element meeting ISM_CONTRIBUTES in the 
    document having the attribute atomicEnergyMarkings containing [FRD] and no elements
    meeting ISM_CONTRIBUTES having the attribute atomicEnergyMarkings containing [RD], then the 
    ISM_RESOURCE_ELEMENT must have atomicEnergyMarkings containing [FRD].
    
    Human Readable: USA documents having Formerly Restricted Data (FRD) and not having Restricted Data (RD) must have FRD at the resource level.
  </sch:p>
  <sch:p class="codeDesc">
    This rule uses an abstract pattern to consolidate logic. For details on the code
    description, see the abstract pattern.
  </sch:p>
  <sch:param name="attrLocalName" value="atomicEnergyMarkings"/>
  <sch:param name="exceptAttrLocalName" value="atomicEnergyMarkings"/>
  <sch:param name="value" value="FRD"/>
  <sch:param name="exceptValueList" value="('RD')"/>
  <sch:param name="errorMessage"
              value="'[ISM-ID-00075][Error] USA documents having Formerly Restricted Data (FRD) and not having Restricted Data (RD) must have FRD at the resource level.'"/>
</sch:pattern>
