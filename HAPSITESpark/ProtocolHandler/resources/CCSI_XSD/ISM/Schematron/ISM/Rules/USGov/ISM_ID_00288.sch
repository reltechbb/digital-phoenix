<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00288">
	  <sch:p class="ruleText">
		[ISM-ID-00288][Error] All noticeReason attributes must be a string with less than 2048 characters. 
	</sch:p>
	  <sch:p class="codeDesc">
	  	For all elements which contain an noticeReason attribute, this rule ensures that the noticeReason value is a string with less
		than 2048 characters.   
	</sch:p>
	  <sch:rule context="*[@ism:noticeReason]">
		    <sch:assert test="string-length(@ism:noticeReason) &lt;= 2048" flag="error">
			[ISM-ID-00288][Error] All noticeReason attributes must be a string with less than 2048 characters. 
		</sch:assert>
	  </sch:rule>
</sch:pattern>
