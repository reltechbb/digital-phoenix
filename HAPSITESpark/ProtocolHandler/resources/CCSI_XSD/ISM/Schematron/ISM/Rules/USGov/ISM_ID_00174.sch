<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00174">
    <sch:p class="ruleText">
        [ISM-ID-00174][Error] If ISM_USGOV_RESOURCE and attribute 
        atomicEnergyMarkings contains the name token [RD], [FRD], or [TFNI], 
        then attribute classification must have a value of [TS], [S], or [C].
        
        Human Readable: USA documents with RD, FRD, or TFNI data must be marked CONFIDENTIAL,
        SECRET, or TOP SECRET.
    </sch:p>
	  <sch:p class="codeDesc">
		If the document is an ISM_USGOV_RESOURCE, for each element which has 
		attribute ism:atomicEnergyMarkings specified with a value containing 
		the token [RD], [FRD], or [TFNI], this rule ensures that the attribute 
		ism:classification has a value of [TS], [S], or [C].
	</sch:p>
	  <sch:rule context="*[$ISM_USGOV_RESOURCE                      and util:containsAnyOfTheTokens(@ism:atomicEnergyMarkings, ('RD', 'FRD', 'TFNI'))]">
		    <sch:assert test="@ism:classification = ('TS','S','C')" flag="error">
			[ISM-ID-00174][Error] If ISM_USGOV_RESOURCE and attribute 
        atomicEnergyMarkings contains the name token [RD], [FRD], or [TFNI], 
        then attribute classification must have a value of [TS], [S], or [C].
        
        Human Readable: USA documents with RD, FRD, or TFNI data must be marked CONFIDENTIAL,
        SECRET, or TOP SECRET.
		</sch:assert>
	  </sch:rule>
</sch:pattern>
