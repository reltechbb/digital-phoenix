<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00032">
    <sch:p class="ruleText">
        [ISM-ID-00032][Error] If ISM_USGOV_RESOURCE and attribute 
        disseminationControls is not specified, or is specified and does not 
        contain the name token [REL] or [EYES], then attribute releasableTo 
        must not be specified.
        
        Human Readable: USA documents must only specify to which countries it is 
        authorized for release if dissemination information contains 
        REL TO or EYES ONLY data. 
    </sch:p>
    <sch:p class="codeDesc">
        If the document is an ISM_USGOV_RESOURCE, for each element which
        does not specify attribute disseminationControls or specifies attribute
        ism:disseminationControls with a value containing the token 
        [REL] or [EYES] this rule ensures that attribute ism:releasableTo is not 
        specified.
    </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE                         and not(util:containsAnyOfTheTokens(@ism:disseminationControls, ('REL', 'EYES')))]">
        <sch:assert test="not(@ism:releasableTo)" flag="error">
            [ISM-ID-00032][Error] If ISM_USGOV_RESOURCE and attribute 
            disseminationControls is not specified, or is specified and does not 
            contain the name token [REL] or [EYES], then attribute releasableTo 
            must not be specified.
            
            Human Readable: USA documents must only specify to which countries it is 
            authorized for release if dissemination information contains 
            REL TO or EYES ONLY data. 
        </sch:assert>
    </sch:rule>
</sch:pattern>
