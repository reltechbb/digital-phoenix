<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00327">
    <sch:p class="ruleText">
        [ISM-ID-00327][Error] If ISM_USGOV_RESOURCE and: 
        1. Any element in the document that has the attribute disseminationControls containing [FOUO]
        AND
        2. Has the attribute classification [U]
        
        Then the element can only have the disseminationControls containing [REL], [RELIDO], [NF], [DISPLAYONLY], and [EYES].
        
        Human Readable: Dissemination control markings, excluding Foreign Disclosure and Release markings 
        (REL, RELIDO, NF, DISPLAYONLY, or EYES), in elements of USA Unclassified documents supersede and take precedence 
        over FOUO.
    </sch:p>
    <sch:p class="codeDesc">
        If the document is an ISM_USGOV_RESOURCE, for any element that contains @ism:disseminationControls
        with a value containing [FOUO] and has @ism:classification with a value of [U], 
        then this rule ensures that @ism:disseminationControls only contains the
        tokens [REL], [RELIDO], [NF], [EYES], [DISPLAYONLY], or [FOUO].
    </sch:p>
    <sch:rule context="*[$ISM_USGOV_RESOURCE
        and util:containsAnyOfTheTokens(@ism:disseminationControls, ('FOUO'))
        and util:containsAnyOfTheTokens(@ism:classification, ('U'))]">
        <sch:assert test="util:containsOnlyTheTokens(@ism:disseminationControls, ('REL', 'RELIDO', 'NF', 'EYES', 'DISPLAYONLY', 'FOUO'))"
                  flag="error">
            [ISM-ID-00327][Error] Dissemination control markings, excluding Foreign Disclosure and Release markings 
            (REL, RELIDO, NF, DISPLAYONLY, or EYES), in elements of USA Unclassified documents supersede and take precedence 
            over FOUO.
        </sch:assert>
    </sch:rule>
</sch:pattern>
