<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00358">
	  <sch:p class="ruleText">
	  	[ISM-ID-00358][Error] A document using tetragraphs may not have a releasableTo or 
	  	that is less restrictive than that of any tetragraph or organization 
	  	tokens used in the releasableTo fields.
	</sch:p>
	  <sch:p class="codeDesc">
	  	Determine the set of releasable countries by determining, for each token, if it is a country code or tetragraph.
	  	If it is a tetragraph get the membership from CATT, otherwise add the token to the list. Then determine if any
	  	of the tetragraph tokens have releasability restrictions themselves. If so, add that token to a list. Finally,
	  	determine if the releasability of the tetragraph tokens are more restrictive then the releasability of the document.
	  	If there are, trigger the error message.
	</sch:p>
	  <sch:rule context="*[@ism:resourceElement='true'][1]">
	  	<sch:let name="releasableToCountries" 
	  		value="distinct-values(tokenize(string-join(for $value in tokenize(@ism:releasableTo,' ') return 
	  			if(index-of($catt//catt:TetraToken,$value)>0) 
	  				then util:getTetragraphMembership($value) 
	  				else $value,' '),' '))"/>
	  	
	  	<sch:let name="tetrasWithReleasableTo" 
	  		value="distinct-values(for $value in tokenize(@ism:releasableTo,' ') return 
	  			if($catt//catt:Tetragraph[catt:TetraToken=$value]/@ism:releasableTo) 
			  		then $value
			  		else null)"/>
	  	
	  	<sch:let name="moreRestrictiveTetras" value="for $tetra in $tetrasWithReleasableTo return 
	  		if (every $value in $releasableToCountries satisfies index-of(util:getTetragraphReleasability($tetra),$value)) 
	  			then null else $tetra"/>
	  	
		    <sch:assert test="empty($moreRestrictiveTetras)" flag="error">
		    	[ISM-ID-00358][Error] A document using tetragraphs may not have a releasableTo or 
		    	that is less restrictive than that of any tetragraph or organization 
		    	tokens used in the releasableTo fields. The following tetragraphs
		    	have a more restrictive releasability than the document: 
		    	<sch:value-of select="string-join($moreRestrictiveTetras,', ')"/>
		</sch:assert>
	  	<sch:assert test="exists($catt//catt:Tetragraphs)">CATT does not exist!</sch:assert>
	  </sch:rule>
</sch:pattern>
