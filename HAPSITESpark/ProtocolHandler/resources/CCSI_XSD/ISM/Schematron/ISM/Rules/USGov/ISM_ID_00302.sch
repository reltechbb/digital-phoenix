<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00302">
    <sch:p class="ruleText">
        [ISM-ID-00302][Error] If ISM_USGOV_RESOURCE and attribute 
        disseminationControls contains the name token [OC-USGOV], then 
        name token [OC] must be specified.
        
        Human Readable: A USA document with OC-USGOV dissemination must 
        also contain an OC dissemination.
    </sch:p>
    <sch:p class="codeDesc">
    	If the document is an ISM_USGOV_RESOURCE, for each element which has 
    	attribute ism:disseminationControls specified with a value containing
    	the token [OC-USGOV], this rule ensures that token [OC] is also specified.
    </sch:p>
  <sch:rule context="*[$ISM_USGOV_RESOURCE                       and util:containsAnyOfTheTokens(@ism:disseminationControls, ('OC-USGOV'))]">
        <sch:assert test="util:containsAnyOfTheTokens(@ism:disseminationControls, ('OC'))"
                  flag="error">
            [ISM-ID-00302][Error] If ISM_USGOV_RESOURCE and attribute 
            disseminationControls contains the name token [OC-USGOV], then 
            name token [OC] must be specified.
            
            Human Readable: A USA document with OC-USGOV dissemination must 
            also contain an OC dissemination.
        </sch:assert>
    </sch:rule>
</sch:pattern>
