<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron"
             id="ISM-ID-00267"
             is-a="ValidateTokenValuesExistenceInListWhenContributesToRollup">
    <sch:p class="ruleText">
    	[ISM-ID-00267][Error] All @ism:SCIcontrols values that contributes to rollup must
    	be defined in CVEnumISMSCIControls.xml
    </sch:p>

	  <sch:p class="codeDesc">
		This rule uses an abstract pattern to consolidate logic. It checks that the
		value in parameter $searchTerm is contained in the parameter $list. The parameter
		$searchTerm is relative in scope to the parameter $context. The value for the parameter 
		$list is a variable defined in the main document (ISM_XML.sch), which reads 
		values from a specific CVE file. The $contributesToRollup is a boolean that indicates
		whether the attribute values in the context contribute to rollup.
	</sch:p>
	
	  <sch:param name="context" value="*[@ism:SCIcontrols]"/>
	  <sch:param name="searchTermList" value="@ism:SCIcontrols"/>
	  <sch:param name="list" value="$SCIcontrolsList"/>
	  <sch:param name="contributesToRollup" value="util:contributesToRollup(.)"/>
	  <sch:param name="errMsg"
              value="'[ISM-ID-00267][Error] All @ism:SCIcontrols values must be defined in CVEnumISMSCIControls.xml.'"/> 
</sch:pattern>
