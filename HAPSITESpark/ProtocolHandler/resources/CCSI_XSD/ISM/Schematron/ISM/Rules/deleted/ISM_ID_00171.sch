<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00171">
    <sch:p class="ruleText">
        [ISM-ID-00171][Warning] Rule removed in V10 because it did not correctly
        treat portions with @ism:releasableTo as also contributing to displayability.
        Rule 320 was introduced in V10 to enforce rollup of displayability on the banner.
    </sch:p>
    <sch:p class="codeDesc"/>
</sch:pattern>
