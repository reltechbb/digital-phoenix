<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00329">
    <sch:p class="ruleText">
        [ISM-ID-00329][Error] Attributes declassEvent and declassDate 
        are mutually exclusive.
    </sch:p>
	  <sch:p class="codeDesc">
		An element cannot have both attributes ism:declassEvent and 
		ism:declassDate.
	</sch:p>
	
    <sch:rule context="*[@ism:declassDate and @ism:declassEvent]">
	     <sch:assert test="false()" flag="error">
	       [ISM-ID-00329][Error] Attributes declassEvent and declassDate 
	       are mutually exclusive.
		</sch:assert>
	  </sch:rule>
</sch:pattern>
