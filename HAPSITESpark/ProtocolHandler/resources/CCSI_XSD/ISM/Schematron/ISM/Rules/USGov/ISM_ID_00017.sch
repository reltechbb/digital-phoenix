<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00017">
    <sch:p class="ruleText">
        [ISM-ID-00017][Error] If ISM_NSI_EO_APPLIES and attribute 
        classifiedBy is specified, then attribute classificationReason must 
        be specified. 
        
        Human Readable: Documents under E.O. 13526 containing 
        Originally Classified data require a classification reason to be
        identified.
    </sch:p>
    <sch:p class="codeDesc">
    	If ISM_NSI_EO_APPLIES, for each element which specifies attribute
    	ism:classifiedBy, this rule ensures that attribute ism:classificationReason
    	is specified.
    </sch:p>
	  <sch:rule context="*[$ISM_NSI_EO_APPLIES and @ism:classifiedBy]">
        <sch:assert test="@ism:classificationReason" flag="error">
        	[ISM-ID-00017][Error] If ISM_NSI_EO_APPLIES and attribute 
        	classifiedBy is specified, then attribute classificationReason must 
        	be specified. 
        	
        	Human Readable: Documents under E.O. 13526 containing 
        	Originally Classified data require a classification reason to be
        	identified.
        </sch:assert>
    </sch:rule>
</sch:pattern>
