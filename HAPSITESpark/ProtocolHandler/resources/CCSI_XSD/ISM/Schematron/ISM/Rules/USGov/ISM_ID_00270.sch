<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00270">
	
	<sch:p class="ruleText"> [ISM-ID-00270][Error] All classificationReason attributes must be a
		string with 4096 characters or less. </sch:p>
	<sch:p class="codeDesc"> For all elements which contain an classificationReason attribute, this
		rule ensures that the classificationReason value is a string with 4096 characters or less. </sch:p>
	<sch:rule context="*[@ism:classificationReason]">
		<sch:assert test="string-length(@ism:classificationReason) &lt;= 4096" flag="error">
			[ISM-ID-00270][Error] All classificationReason attributes must be a string with 4096
			characters or less. </sch:assert>
	</sch:rule>
</sch:pattern>
