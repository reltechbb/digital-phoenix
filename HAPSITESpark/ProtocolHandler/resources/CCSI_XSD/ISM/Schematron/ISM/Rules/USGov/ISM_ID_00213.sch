<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISM-ID-00213">
    <sch:p class="ruleText">
        [ISM-ID-00213][Error] If ISM_USGOV_RESOURCE and attribute 
        disseminationControls contains the name token [DISPLAYONLY], then 
        attribute displayOnlyTo must be specified.
        
        Human Readable: A USA document with DISPLAY ONLY dissemination must 
        indicate the countries to which it may be disclosed.
    </sch:p>
    <sch:p class="codeDesc">
    	If the document is an ISM_USGOV_RESOURCE, for each element which has 
    	attribute ism:disseminationControls specified with a value containing
    	the token [DISPLAYONLY] this rule ensures that attribute ism:displayOnlyTo
    	is specified.
    </sch:p>
  <sch:rule context="*[$ISM_USGOV_RESOURCE                       and util:containsAnyOfTheTokens(@ism:disseminationControls, ('DISPLAYONLY'))]">
        <sch:assert test="@ism:displayOnlyTo" flag="error">
        	[ISM-ID-00213][Error] If ISM_USGOV_RESOURCE and attribute 
        	disseminationControls contains the name token [DISPLAYONLY], then 
        	attribute displayOnlyTo must be specified.
        	
        	Human Readable: A USA document with DISPLAY ONLY dissemination must 
        	indicate the countries to which it may be disclosed.
        </sch:assert>
    </sch:rule>
</sch:pattern>
