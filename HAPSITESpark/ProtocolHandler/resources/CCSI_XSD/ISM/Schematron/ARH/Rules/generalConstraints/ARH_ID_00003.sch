<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ARH-ID-00003">
    <sch:p class="ruleText">
        [ARH-ID-00003][Error] Every ARH attribute in the document must be
        specified with a non-whitespace value.
        
        Human Readable: All attributes in the ARH namespace must specify a value.
    </sch:p>
    
    <sch:p class="codeDesc">
        For each element which specifies an attribute in the ARH namespace, we
        make sure that all attributes in the ARH namespace contain a non-whitespace
        value.
    </sch:p>
    
    <sch:rule context="*[@arh:*]">
        <sch:assert test="every $attribute in @arh:*  satisfies             string-length(normalize-space(string($attribute))) &gt; 0"
                  flag="error">
            [ARH-ID-00003][Error] Every ARH attribute in the document must be specified with a non-whitespace value.
            
            Human Readable: All attributes in the ARH namespace must specify a value.
        </sch:assert>
    </sch:rule>
</sch:pattern>
