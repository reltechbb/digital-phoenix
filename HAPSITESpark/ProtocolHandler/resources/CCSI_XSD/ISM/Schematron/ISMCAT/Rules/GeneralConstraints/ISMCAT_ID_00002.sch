<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISMCAT-ID-00002">
    <sch:p class="ruleText">
        [ISMCAT-ID-00002][Error] The @arh:DESVersion is less than the minimum version 
        allowed: 3. 
        
        Human Readable: The ARH version imported by ISMCAT must be greater than or equal to 3. 
    </sch:p>
    <sch:p class="codeDesc">
        For all elements that contain @arh:DESVersion, this rule verifies that the version
        is greater than or equal to the minimum allowed version: 3.  
    </sch:p>
    <sch:rule context="*[@arh:DESVersion]">
        <sch:let name="version" value="number(if (contains(@arh:DESVersion,'-')) then substring-before(@arh:DESVersion,'-') else @arh:DESVersion)"/>
        <sch:assert test="$version &gt;= 3" flag="error">
            [ISMCAT-ID-00002][Error] The @arh:DESVersion is less than the minimum version 
            allowed: 3. 
            
            Human Readable: The ARH version imported by ISMCAT must be greater than or equal to 3. 
        </sch:assert>
    </sch:rule>
</sch:pattern>