<?xml version="1.0" encoding="UTF-8"?>
<?ICEA pattern?>
<!-- Notices - Distribution Notice:
            This document is being made available by the Intelligence Community Chief Information Officer
            to Federal, State, Local, Tribal, and Foreign Partners and associated contractors. Approval for
            any further distribution must be coordinated via the Intelligence Community Chief Information 
            Officer, at ic-standards-support@iarpa.gov.-->
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="ISMCAT-ID-00001">
    <sch:p class="ruleText">[ISMCAT-ID-00001][Error] ISM CAT CESVersion attributes must be specified
        as version ‘201609’ (2016-SEP).</sch:p>

    <sch:p class="codeDesc">Ensure that all instances of the @ism:ISMCATCESVersion have a value of
        201609. This rule supports extending the version identifier with an optional trailing hypen
        and up to 23 additional characters. The version must match the regular expression
        “^201609(-.{1,23})?$”</sch:p>

    <sch:rule context="*[@ism:ISMCATCESVersion]">
        <sch:assert test="matches(@ism:ISMCATCESVersion,'^201609(-.{1,23})?$')" flag="error"
            >[ISMCAT-ID-00001][Error] ISMCAT CESVersion attributes must be specified as version
            ‘201609’ (for 2016-SEP).</sch:assert>
    </sch:rule>
</sch:pattern>
