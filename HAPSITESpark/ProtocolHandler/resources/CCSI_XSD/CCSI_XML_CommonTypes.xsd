<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns="file:/C:/ccsi/1.1/CCSI" xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:ccsi="file:/C:/ccsi/1.1/CCSI" targetNamespace="file:/C:/ccsi/1.1/CCSI" version="1.1.2" 
  xmlns:ism="urn:us:gov:ic:ism">
  <!--
		Change Log:
		TCS - 1/31/08 - Initial release.
		TCS - 3/14/08 - Updated IPV4 Address patterns to increase restrictions.
		TCS - 3/25/09 - Use updated base types
		
		Repository Path:           $HeadURL: https://svn-project.forge.mil/svn/repos/ccsischema11/trunk/CCSI_XML_CommonTypes.xsd $
    Last Committed:            $Revision: 76 $
    Last Change By:            $Author: swansonthomas $
    Last Changed Date:         $Date: 2015-12-24 09:38:11 -0500 (Thu, 24 Dec 2015) $
    ID:                        $Id: CCSI_XML_CommonTypes.xsd 76 2015-12-24 14:38:11Z swansonthomas $
    Revision				           $Rev: 76 $
		
		
  -->
  <!--
    Include the common CCSI type definitions.
  -->
  <xs:import namespace="urn:us:gov:ic:ism" schemaLocation="ISM/Schema/ISM/CCSI-IC-ISM.xsd"/>
  <xs:include schemaLocation="CCSI_XML_BaseTypes.xsd"/>
  
  <!-- 
	  Define an enumeration for whether a sensor support bidirectional or
	  unidirectional communications. 
  -->
  <xs:simpleType name="SensorCommDirectionType">
    <xs:annotation>
      <xs:documentation>
        This enumeration defines two values indicating if a sensor version supports bi-directional
        or uni-directional communications.
      </xs:documentation>
    </xs:annotation>
    <xs:restriction base="code-6">
      <xs:enumeration value="BIDIR"/>   <!-- Bi-directional communications (DEFAULT) -->
      <xs:enumeration value="UNIDIR"/>  <!-- Uni-directional communications -->
    </xs:restriction>
  </xs:simpleType>
  
  <xs:attributeGroup name="waivedImplementationGroup">
    <xs:annotation>
      <xs:documentation>
        This group of three attributes identifies a CCSI mandatory command or channel whose implementation has been waived
        by both the acquisition program and JPM IS.  It allows those to be marked specifically as waived as not count the
        command or channel as an error when validating the sensor definition file.
      </xs:documentation>
    </xs:annotation>
    <xs:attribute name="waived" type="xs:boolean" use="optional" default="false"/>
    <xs:attribute name="waiverText" type="xs:string" use="optional" default="Not Waived"/>
    <xs:attribute name="waiverDate" type="xs:dateTime" use="optional"/>
  </xs:attributeGroup>
  
  <xs:complexType name="TaggedDescriptionType">
    <xs:annotation>
      <xs:documentation>
        This type extends the DescriptionText type with security marking attributes.
      </xs:documentation>
    </xs:annotation>
    <xs:simpleContent>
      <xs:extension base="DescriptionText">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  <xs:complexType name="TaggedCcsiNameType">
    <xs:simpleContent>
      <xs:extension base="CcsiNameType">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  <xs:complexType name="TaggedCcsiNameListType">
    <xs:simpleContent>
      <xs:extension base="CcsiNameListType">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  <xs:complexType name="TaggedMaterielTypeModelNumberText">
    <xs:simpleContent>
      <xs:extension base="MaterielTypeModelNumberText">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  <xs:complexType name="TaggedMaterielSerialNumberIdentificationText">
    <xs:simpleContent>
      <xs:extension base="MaterielSerialNumberIdentificationText">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  <xs:complexType name="TaggedFieldLabelType">
    <xs:simpleContent>
      <xs:extension base="FieldLabelType">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  <xs:complexType name="TaggedHelpTextType">
    <xs:simpleContent>
      <xs:extension base="HelpTextType">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  <xs:complexType name="TaggedLoginNameText">
    <xs:simpleContent>
      <xs:extension base="LoginNameText">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  <xs:complexType name="TaggedLoginPasswordText">
    <xs:simpleContent>
      <xs:extension base="LoginPasswordText">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  <!--
    Defines the location of a physical object. May include sensors, aggregators, 
    vehicles, and command centers.
  -->
  <xs:complexType name="LocationType">
    <xs:annotation>
      <xs:documentation> Defines the location of a physical object. which may include sensors, aggregators, vehicles,
        and command centers. The latitude and longitude are mandatory values with the altitude defaulting to 0 meters.
      </xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="LocationTypeLatitude" type="AbsolutePointLatitudeCoordinate"/>
      <xs:element name="LocationTypeLongitude" type="AbsolutePointLongitudeCoordinate"/>
      <xs:element name="LocationTypeAltitude" type="VerticalDistanceDimension" default="0" minOccurs="0"/>
    </xs:sequence>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>

  <!-- Array definition components -->
  <!-- Array definition type setting the size limits and counts for array dimensions. -->
  <xs:simpleType name="ArrayDimensionType">
    <xs:annotation>
      <xs:documentation> Array definition type used for setting the size limits and counts for one dimension of an
        array. The dimension must be greater than or equal to 1. </xs:documentation>
    </xs:annotation>
    <xs:restriction base="ArrayTotalDimensionCount"/>
  </xs:simpleType>
  
  <xs:complexType name="PointOfContact">
    <xs:all>
      <xs:element name="PointOfContactNameText" type="text-80-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactSurnameText" type="text-50" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactOtherNameUsedText" type="text-2000-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactJobTitleNameText" type="text-50-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactPositionNameText" type="text-50-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactMilitaryRankCode" type="PointOfContactMilitaryRankCode" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactHonorificText" type="text-32-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactUserIdText" type="text-32-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactTypeCode" type="PointOfContactTypeCode" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactOfficeNameText" type="text-50" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactPhoneNumberText" type="text-32-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactEmailAddressText" type="text-50-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactOrganisationNameText" type="text-80-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactAgencyAcronymText" type="text-25-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactAffiliationText" type="text-50-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactAddressLineText" type="text-100" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactCityNameText" type="text-50-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactStateNameText" type="text-25-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactProvinceNameText" type="text-25-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactCountryCode" type="code-6" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactPostalCodeText" type="text-10-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactFaxNumberText" type="text-32-optional" nillable="true" minOccurs="0"/>
      <xs:element name="PointOfContactWebPageAddressText" type="text-50-optional" nillable="true" minOccurs="0"/>
    </xs:all>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>
  
  <!-- 
    Restricted string definition with whitespace preservation for spectral data.  This ensure that the
    XML processor preserves the blockSeparator and valueSeparator strings as the information is being
    generated/parsed.
  -->
   
  <!-- Define how data is transmitted from the sensor when the data type is an array. -->
  <xs:complexType name="ArrayTransmitData">
    <xs:annotation>
      <xs:documentation>
        This defines how an array of data is transmitted.  The attribute 'name' is the name of the sensor defined
        data element type being transmitted.  For each dimension of the array a set of four elements is provided
        that contain the row or column information.  These are:
        Data = The row or column data expressed as a string of Count values separated by the defined Separator
      </xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="ArrayData" type="ArrayDataStringBase" minOccurs="1" maxOccurs="1"/>
    </xs:sequence>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>

  <xs:complexType name="ArrayDimensionDefType">
    <xs:annotation>
      <xs:documentation> Array dimension definition type that defines, in detail, one dimension of an array. The
        definition includes the order, dimension name, the maximum number of elements that the dimension can have, and a
        label for use when displaying the dimension. </xs:documentation>
    </xs:annotation>
    <xs:attribute name="order" type="ArrayDimensionPrecedenceOrdinal" use="required"/>
    <xs:attribute name="name" type="ArrayDimensionDescriptionText" use="required"/>
    <xs:attribute name="max_elements" type="ArrayDimensionInstancePerDimensionCount" use="required"/>
    <xs:attribute name="label" type="UomLabelType" use="optional"/>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>

  <xs:complexType name="ArrayDefinitionType">
    <xs:annotation>
      <xs:documentation>
        This defines an array for CCSI. This includes a description of the array, the prime element
        type contained in the array, the number of dimensions, whether the data is transmitted in row first or column
        first order, the strings used to separate values and blocks, and a definition of each of the array dimensions. By default a comma is used as a value separator
        and line feed as a block (a row or a column) separator.
      </xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="ArrayDefinitionTypeDescription" type="TaggedDescriptionType"/>
      <xs:element name="ArrayDefinitionTypeElementType" type="TaggedCcsiDataType" minOccurs="0"/>
      <xs:element name="ArrayDefinitionTypeDimensions" type="ArrayDimensionDefType" minOccurs="0" maxOccurs="10"/>
    </xs:sequence>
    <xs:attribute name="nbr_dims" type="ArrayTotalDimensionCount" use="required"/>
    <xs:attribute name="row_first" type="xs:boolean" use="optional" default="true"/>
    <xs:attribute name="blockSeparator" type="xs:string" use="optional" default="&#10;"/>
    <xs:attribute name="valueSeparator" type="xs:string" use="optional" default=","/>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>

  <!-- Defines the material that a sensor can detect and whether it's a point 
    or standoff type. -->
  <xs:complexType name="CcsiSensorType">
    <xs:annotation>
      <xs:documentation> Defines the material that a sensor can detect and whether it's a point or standoff type.
      </xs:documentation>
    </xs:annotation>
    <xs:attribute name="class" type="SensorClass" use="required"/>
    <xs:attribute name="variant" type="SensorVariant" use="required"/>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>

  <!-- Role list type -->
  <xs:simpleType name="RoleListType">
    <xs:annotation>
      <xs:documentation> This defines a whitespace separated list of CCSI roles. </xs:documentation>
    </xs:annotation>
    <xs:list itemType="RoleType"/>
  </xs:simpleType>
  <xs:complexType name="TaggedRoleListType">
    <xs:simpleContent>
      <xs:extension base="RoleListType">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>

  <!-- IPv4 and IPv6 IP choice structure -->
  <!-- IPv4 type and definition -->
  <xs:simpleType name="Ipv4AddressType">
    <xs:annotation>
      <xs:documentation> Defines an IP address in terms of the IPv4 standard dotted quad notation. </xs:documentation>
    </xs:annotation>
    <xs:restriction base="ElectronicAddressInternetProtocolAddressText">
      <xs:maxLength value="15"/>
      <xs:pattern value="(([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])[.]){3}([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])"/>
    </xs:restriction>
  </xs:simpleType>

  <!-- IPv6 type and definition -->
  <xs:simpleType name="Ipv6AddressType">
    <xs:annotation>
      <xs:documentation> Defines an IP address in terms of the IPv6 standard hex word representation.
      </xs:documentation>
    </xs:annotation>
    <xs:restriction base="ElectronicAddressInternetProtocolAddressText">
      <xs:maxLength value="40"/>
      <xs:pattern value="(:{0,2})([0-9a-fA-F]{4}:){0,7}([0-9a-fA-F]){4}"/>
    </xs:restriction>
  </xs:simpleType>

  <!-- choice of IP types definition -->
  <xs:complexType name="IpAddressType">
    <xs:annotation>
      <xs:documentation> Defines an IP address using either IPv4 or IPv6 notation. </xs:documentation>
    </xs:annotation>
    <xs:choice>
      <xs:element name="IpAddressTypeIpv4" type="Ipv4AddressType"/>
      <xs:element name="IpAddressTypeIpv6" type="Ipv6AddressType"/>
    </xs:choice>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>

  <!-- IPv4 and IPv6 Net Mask choice structure -->
  <!-- IPv4 Net Mask type and definition -->
  <xs:simpleType name="Ipv4NetMaskType">
    <xs:annotation>
      <xs:documentation> Defines the Net Mask in terms of the IPv4 standard dotted quad notation. </xs:documentation>
    </xs:annotation>
    <xs:restriction base="ElectronicAddressNetmaskText">
      <xs:maxLength value="15"/>
      <xs:pattern value="(([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])[.]){3}([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])"/>
    </xs:restriction>
  </xs:simpleType>

  <!-- IPv6 Net Mask type and definition -->
  <xs:simpleType name="Ipv6NetMaskType">
    <xs:annotation>
      <xs:documentation> Defines the Net Mask in terms of the IPv6 standard hex word notation. </xs:documentation>
    </xs:annotation>
    <xs:restriction base="ElectronicAddressNetmaskText">
      <xs:maxLength value="40"/>
      <xs:pattern value="(:{0,2})([0-9a-fA-F]{4}:){0,7}([0-9a-fA-F]){4}"/>
    </xs:restriction>
  </xs:simpleType>

  <!-- choice of Net Mask types definition -->
  <xs:complexType name="IpNetMaskType">
    <xs:annotation>
      <xs:documentation> Defines an IP net mask type using either IPv4 or IPv6 notation. </xs:documentation>
    </xs:annotation>
    <xs:choice>
      <xs:element name="IpNetMaskTypeIpv4" type="Ipv4NetMaskType"/>
      <xs:element name="IpNetMaskTypeIpv6" type="Ipv6NetMaskType"/>
    </xs:choice>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>

  <!-- Begin dataValidityType block  -->
  <!-- Begin validityDateType block  -->
  <!-- Time Type  -->
  <xs:simpleType name="TimeType">
    <xs:annotation>
      <xs:documentation> Definition of time as hhmmss using a twenty four hour clock UTC with no offset.
      </xs:documentation>
    </xs:annotation>
    <xs:restriction base="time">
      <xs:length value="6"/>
      <xs:pattern value="[0-2][0-9][0-5][0-9][0-5][0-9]"/>
    </xs:restriction>
  </xs:simpleType>

  <!-- Date Type  -->
  <xs:simpleType name="DateType">
    <xs:annotation>
      <xs:documentation> Definition of date as yyyymmdd. </xs:documentation>
    </xs:annotation>
    <xs:restriction base="date">
      <xs:length value="8"/>
      <xs:pattern value="([1-2][0-9]{3})([0-1][0-9])([0-3][0-9])"/>
    </xs:restriction>
  </xs:simpleType>
  <!-- Date Time Group Type  -->

  <xs:simpleType name="DtgType">
    <xs:annotation>
      <xs:documentation> Definition of date time group as yyyymmddhhmmss. </xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:length value="14"/>
      <xs:pattern value="([1-2][0-9]{3})([0-1][0-9])([0-3][0-9])[0-2][0-9][0-5][0-9][0-5][0-9]"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="TaggedDtgType">
    <xs:simpleContent>
      <xs:extension base="DtgType">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  <xs:complexType name="TaggedLoginFailureCount">
    <xs:simpleContent>
      <xs:extension base="LoginFailureCount">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  <xs:complexType name="TaggedBinaryObjectItemBinaryObject">
    <xs:simpleContent>
      <xs:extension base="BinaryObjectItemBinaryObject">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>

  <!-- Validity Date Type  -->
  <xs:simpleType name="ValidityDateType">
    <xs:annotation>
      <xs:documentation> Defines a data element validity check type for dates and times in CCSI formats. This controls
        the displayed or user input date time type to constrain the method of data entry. </xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:enumeration value="Date"/>
      <xs:enumeration value="Time"/>
      <xs:enumeration value="DateTime"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="ValidityNumRangeType">
    <xs:annotation>
      <xs:documentation> Defines a type for specifying numeric ranges of valid values. It requires two numbers separated
        by a dash. Both numbers must be of the same type, either integer or float, with the first number less than or
        equal to the second. The dash separator may be preceeded and/or followed by a space character for legibility.
        Example: To define a value range for integers of 1 to 10 1 - 10 or 1-10 </xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="3"/>
      <xs:maxLength value="38"/>
      <xs:pattern value="[\-+]?[0-9]+(\.[0-9]+)?(\s+)?-(\s+)?[\-+]?[0-9]+(\.[0-9]+)?"/>
    </xs:restriction>
  </xs:simpleType>

  <!--  Defines the Valid Enumeration Type -->
  <xs:complexType name="ValidityRangeListType">
    <xs:annotation>
      <xs:documentation> Defines a list of numeric validity ranges to allow specification of discontiguous ranges.
      </xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="Range" type="ValidityNumRangeType" maxOccurs="20"/>
    </xs:sequence>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>
  <!-- End validityRangeListType block  -->

  <!-- Begin validityEnumType block  -->
  <!-- Enumeration List Item  -->
  <xs:simpleType name="EnumerationListItem">
    <xs:annotation>
      <xs:documentation> Definition of one item in a list of enumerated type. </xs:documentation>
    </xs:annotation>
    <xs:restriction base="ArrayDimensionDescriptionText">
      <xs:minLength value="1"/>
      <xs:maxLength value="32"/>
    </xs:restriction>
  </xs:simpleType>

  <!--  Defines the Valid Enumeration Type -->
  <xs:complexType name="ValidityEnumType">
    <xs:annotation>
      <xs:documentation> Defines an enumeration validity check type as a list of enumeration items. </xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="Item" type="EnumerationListItem" maxOccurs="50"/>
    </xs:sequence>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>
  <!-- End validityEnumType block  -->
  
  <!--  Defines the Valid Pattern Type  -->
  <xs:simpleType name="ValidityPatternType">  <!-- TCS: 4/11/2014 - Definition of pattern validity type -->
    <xs:annotation>
      <xs:documentation>
        A string representing a regular expression.  Note: backslash character must be escaped to be read by
        a validation engine (e.g., \n would be represented as \\n).  The pattern must be a valid regular
        expression at least 4 and no more than 256 characters in length including doubled escape characters. 
      </xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="4"/>
      <xs:maxLength value="256"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="TaggedValidityPatternType">
    <xs:simpleContent>
      <xs:extension base="ValidityPatternType">
        <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  
  <xs:complexType name="SensorUniqueData">
    <xs:annotation>
      <xs:documentation>
        This type is provided to support sensor unique data transmitted as a part of the sensor
        readings. The element name and its value are mandatory. The name must be unique within the sensor definition.
        The type may be used to identify a sensor definition file data element name that is the type of the value. The
        units may be used to provide the units of measure for the value.
      </xs:documentation>
    </xs:annotation>
    <xs:attribute name="Name" type="CcsiNameType" use="required"/>
    <xs:attribute name="Value" type="xs:string" use="required"/>
    <xs:attribute name="Type" type="CcsiNameType" use="optional"/>
    <xs:attribute name="Units" type="UomLabelType" use="optional"/>
    <xs:attribute name="mime" type="xs:string" use="optional" default=""/>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>

  <!--  Defines the Data Validity Type -->
  <xs:complexType name="DataValidityType">
    <xs:annotation>
      <xs:documentation> Defines the valid data format for data types. The validity specification may be one of the
        define validity types. In most cases, the higher level definition will allow multiple validity types for a data
        element defintion or command argument. </xs:documentation>
    </xs:annotation>
    <xs:choice>
      <xs:element name="Enumeration" type="ValidityEnumType"/>
      <xs:element name="Range" type="ValidityNumRangeType"/>
      <xs:element name="RangeList" type="ValidityRangeListType"/>
      <xs:element name="DateTime" type="ValidityDateType"/>
      <xs:element name="Pattern" type="ValidityPatternType"/>    <!-- TCS 5/11/2014 RegEx pattern validation -->
      <xs:element name="MinStringLength" type="xs:short"/>
      <xs:element name="MaxStringLength" type="xs:short"/>
      <xs:element name="Location" type="xs:string"/>
    </xs:choice>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>
  <!-- End dataValidityType block  -->

  <!--  Defines the Weather Item Type -->
  <xs:complexType name="WeatherItemType">
    <xs:annotation>
      <xs:documentation> Defines the set of data for reporting environmental information for a single point.
        WeatherItemTypePoint = 3-space definition of the location of the weather report WeatherItemTypeSpeed = wind
        speed at the location of the weather report WeatherItemTypeDirection = wind direction at the location of the
        weather report WeatherItemTypeTemperature = air or ground temperature at the location of the weather report
        WeatherItemTypeHumidity = relative humidity at the location of the weather report
        WeatherItemTypePrecipitationType = precipitation enumerated type at the location of the weather report
        WeatherItemTypePrecipitationRate = precipitation rate at the location of the weather report
        WeatherItemTypePressure = barometric pressure in the reported units of measure
      </xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="WeatherItemTypePoint" type="LocationType"/>
      <xs:element name="WeatherItemTypeSpeed" type="WindSpeedType"/>
      <xs:element name="WeatherItemTypeDirection" type="WindDirectionType"/>
      <xs:element name="WeatherItemTypeTemperature" type="TemperatureType" default="0" minOccurs="0"/>
      <xs:element name="WeatherItemTypeHumidity" type="RelativeHumidityType" default="0" minOccurs="0"/>
      <xs:element name="WeatherItemTypePrecipitationType" type="PrecipitationEnum" default="NKN" minOccurs="0"/>
      <xs:element name="WeatherItemTypePrecipitationRate" type="PrecipitationRateType" default="0.0" minOccurs="0"/>
      <xs:element name="WeatherItemTypePressure" type="AtmosphericPressureType" default="0.0" minOccurs="0"/>
      <xs:element name="WeatherItemTypePressureUom" type="AtmosphericPressureEnum" default="pa" minOccurs="0"/>
    </xs:sequence>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>

  <!--  Defines the Weather Data Type -->
  <xs:complexType name="WeatherDataType">
    <xs:annotation>
      <xs:documentation> Defines a collection of information for reporting environmental information from a single
        source. The time value represents the time of the validity of the data and if not present the current time will
        be assumed. </xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="WeatherDataTypeSource" type="WeatherSourceEnum"/>
      <xs:element name="WeatherDataTypeTime" type="DtgType" minOccurs="0"/>
      <xs:element name="WeatherDataTypeItemList" type="WeatherItemType" maxOccurs="20"/>
    </xs:sequence>
    <xs:attributeGroup ref="ism:SecurityAttributesOptionGroup"/>
  </xs:complexType>
</xs:schema>
