java -cp C:\Spark\Code\TRUNK\ProtocolHandler\dist\SparkCCSI.jar;C:\Spark\Code\TRUNK\SensorInterfaceAdapter\dist\SparkSIAdapter.jar;C:\Spark\Code\TRUNK\SensorProtocolAdapter\dist\SparkSPAdapter.jar ^
-Dccsi.baseDir=C:\Spark\Code\TRUNK\ProtocolHandler\src\resources ^
-Dccsi.spa.reader=com.domenix.sensorprotocoladapter.SensorProtocolAdapterReaderImpl ^
-Dccsi.spa.writer=com.domenix.sensorprotocoladapter.SensorProtocolAdapterWriterImpl ^
-Dccsi.sia.reader=com.domenix.sensorinterfaceadapter.SensorInterfaceAdapterReaderImpl ^
-Dccsi.sia.writer=com.domenix.sensorinterfaceadapter.SensorInterfaceAdapterWriterImpl ^
-Dccsi.hia.impl=com.domenix.nsds.impl.SensorInterfaceAdapterWriterImpl ^
-Dccsi.hpa.impl=com.domenix.nsds.impl.NSDSProtocolImpl ^
-Dccsi.sia.configFile=test.cfg ^
-Dccsi.spa.configFile=test.cfg ^
-Dccsi.hpa.configFile=test.cfg ^
-Dccsi.hia.configFile=test.cfg ^
com.domenix.ccsi.main.SparkMain
