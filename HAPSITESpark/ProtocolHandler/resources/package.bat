rem
rem creates zip files for installation
rem
zip -j c:/TEMP/SparkJars.zip ../dist/SparkCCSI.jar 
zip -j c:/TEMP/SparkJars.zip ../../HostInterfaceAdapter/dist/Host*.jar ../../HostProtocolAdapter/dist/Host*.jar
zip -j c:/TEMP/SparkJars.zip ../../SensorInterfaceAdapter/dist/Spark*.jar ../../SensorProtocolAdapter/dist/Spark*.jar
zip -j c:/TEMP/SparkJars.zip ../../Common/dist/Common.jar ../../CommonSA/dist/CommonSA.jar ../../CommonHA/dist/CommonHA.jar
zip -j c:/TEMP/SparkJars.zip ../../HostProtocolAdapterISA/dist/HostProtocolAdapterISA.jar
zip -j c:/TEMP/SparkJars.zip ../../JCAD-Replay/dist/JHRM-Replay.jar.jar ../../HAPSITE/dist/HAPSITE.jar
zip -j c:/TEMP/OtherJars.zip ../lib/jaxb* ../lib/jbzip* ../lib/jsr*.jar ../lib/log4j* ../lib/pi4j* ../lib/jSerialComm*
zip -j c:/TEMP/OtherJars.zip ../lib/absolutelayout/AbsoluteLayout.jar
zip -j c:/TEMP/OtherJars.zip ../../HostProtocolAdapterISA/lib/gs-*.jar  ../../HostProtocolAdapterISA/lib/guava-*.jar
zip -j c:/TEMP/OtherJars.zip ../../HostProtocolAdapterISA/lib/isa-*.jar  ../../HostProtocolAdapterISA/lib/jetm-*.jar
zip -j c:/TEMP/OtherJars.zip ../../HostProtocolAdapterISA/lib/nvl-*.jar  ../../HostProtocolAdapterISA/lib/owner-*.jar
zip -j c:/TEMP/OtherJars.zip ../../HostProtocolAdapterISA/lib/pherd-*.jar  ../../HostProtocolAdapterISA/lib/protobuf-*.jar
zip -j c:/TEMP/OtherJars.zip ../../HostProtocolAdapterISA/lib/slf4j-*.jar  ../../HostProtocolAdapterISA/lib/api-extended-*.jar
zip -r c:/TEMP/SparkData.zip * 
