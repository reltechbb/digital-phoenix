/*
 * CcsiLinkTypeEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This enumerates the type of networks on which a CCSI sensor can communicate.
 *
 * Version: V1.0  15/09/10
 */
package com.domenix.ccsi;

/**
 * This enumerates the type of networks on which a CCSI sensor can communicate.
 * 
 * @author kmiller
 */
public enum CcsiLinkTypeEnum
{
  /** Arcnet link */
  ARCNET , 
  /** 802.3 or similar wired Ethernet */
  ETHRNT , 
  /** A mixed network, e.g. Ethernet to a radio */
  MIXED , 
  /** Token ring network */
  TKNRNG , 
  /** A wireless network e.g., JTRS, 802.11, etc. */
  WRLESS;
}
