/*
 * <put file name here>
 *
 * Copyright (c) 2001-2020 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * <put description here>
 *
 */
package com.domenix.ccsi.jaxb;

/**
 *
 * @author Administrator
 */
public enum CommandTypeEnum {
    
    IDENT,
    REGISTER,
    
}
