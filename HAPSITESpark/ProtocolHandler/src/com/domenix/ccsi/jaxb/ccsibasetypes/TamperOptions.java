//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.25 at 11:21:34 AM EDT 
//


package com.domenix.ccsi.jaxb.ccsibasetypes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import com.sun.xml.bind.Locatable;
import com.sun.xml.bind.annotation.XmlLocation;
import org.xml.sax.Locator;


/**
 *  This type defines the options associated with tamper detection. 
 * 
 * <p>Java class for TamperOptions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TamperOptions"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Count" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="Window" type="{file:/C:/ccsi/1.1/CCSI}TimePeriodType"/&gt;
 *         &lt;element name="Action" type="{file:/C:/ccsi/1.1/CCSI}TamperActionType"/&gt;
 *         &lt;element name="Enable" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TamperOptions", propOrder = {
    "count",
    "window",
    "action",
    "enable"
})
public class TamperOptions
    implements Locatable
{

    @XmlElement(name = "Count")
    protected long count;
    @XmlElement(name = "Window")
    @XmlSchemaType(name = "long")
    protected int window;
    @XmlElement(name = "Action", required = true)
    @XmlSchemaType(name = "string")
    protected CBRNSensorStatusTamperActionCode action;
    @XmlElement(name = "Enable")
    protected boolean enable;
    @XmlLocation
    @XmlTransient
    protected Locator locator;

    /**
     * Gets the value of the count property.
     * 
     */
    public long getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     */
    public void setCount(long value) {
        this.count = value;
    }

    /**
     * Gets the value of the window property.
     * 
     */
    public int getWindow() {
        return window;
    }

    /**
     * Sets the value of the window property.
     * 
     */
    public void setWindow(int value) {
        this.window = value;
    }

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link CBRNSensorStatusTamperActionCode }
     *     
     */
    public CBRNSensorStatusTamperActionCode getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link CBRNSensorStatusTamperActionCode }
     *     
     */
    public void setAction(CBRNSensorStatusTamperActionCode value) {
        this.action = value;
    }

    /**
     * Gets the value of the enable property.
     * 
     */
    public boolean isEnable() {
        return enable;
    }

    /**
     * Sets the value of the enable property.
     * 
     */
    public void setEnable(boolean value) {
        this.enable = value;
    }

    public Locator sourceLocation() {
        return locator;
    }

    public void setSourceLocation(Locator newLocator) {
        locator = newLocator;
    }

}
