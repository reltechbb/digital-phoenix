//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.25 at 11:21:34 AM EDT 
//


package com.domenix.ccsi.jaxb.ccsibasetypes;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConfigBlockEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ConfigBlockEnum"&gt;
 *   &lt;restriction base="{file:/C:/ccsi/1.1/CCSI}code-10"&gt;
 *     &lt;enumeration value="BASE"/&gt;
 *     &lt;enumeration value="ANNUN"/&gt;
 *     &lt;enumeration value="COMM"/&gt;
 *     &lt;enumeration value="GENERAL"/&gt;
 *     &lt;enumeration value="LOCAL"/&gt;
 *     &lt;enumeration value="SECURITY"/&gt;
 *     &lt;enumeration value="TAMPER"/&gt;
 *     &lt;enumeration value="LINKS"/&gt;
 *     &lt;enumeration value="OPERATORS"/&gt;
 *     &lt;enumeration value="UNIQUE"/&gt;
 *     &lt;enumeration value="SENSDESC"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ConfigBlockEnum")
@XmlEnum
public enum ConfigBlockEnum {

    BASE,
    ANNUN,
    COMM,
    GENERAL,
    LOCAL,
    SECURITY,
    TAMPER,
    LINKS,
    OPERATORS,
    UNIQUE,
    SENSDESC;

    public String value() {
        return name();
    }

    public static ConfigBlockEnum fromValue(String v) {
        return valueOf(v);
    }

}
