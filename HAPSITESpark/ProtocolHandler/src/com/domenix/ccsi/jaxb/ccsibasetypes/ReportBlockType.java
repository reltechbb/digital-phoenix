//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.25 at 11:21:34 AM EDT 
//


package com.domenix.ccsi.jaxb.ccsibasetypes;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import com.sun.xml.bind.Locatable;
import com.sun.xml.bind.annotation.XmlLocation;
import org.xml.sax.Locator;


/**
 * 
 * 				This type defines a configuration report block.  It's use is intended to be that each block matches one of the
 * 				CCSI configuration blocks.  All items that are being reported from within a specific block (or block and key value)
 * 				are contained in a single instance of this type.
 * 			
 * 
 * <p>Java class for ReportBlockType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReportBlockType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Item" type="{file:/C:/ccsi/1.1/CCSI}NameValuePairType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Name" use="required" type="{file:/C:/ccsi/1.1/CCSI}ConfigBlockEnum" /&gt;
 *       &lt;attribute name="Key" type="{file:/C:/ccsi/1.1/CCSI}name-255" default=" " /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportBlockType", propOrder = {
    "item"
})
public class ReportBlockType
    implements Locatable
{

    @XmlElement(name = "Item", required = true)
    protected List<NameValuePairType> item;
    @XmlAttribute(name = "Name", required = true)
    protected ConfigBlockEnum name;
    @XmlAttribute(name = "Key")
    protected String key;
    @XmlLocation
    @XmlTransient
    protected Locator locator;

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NameValuePairType }
     * 
     * 
     */
    public List<NameValuePairType> getItem() {
        if (item == null) {
            item = new ArrayList<NameValuePairType>();
        }
        return this.item;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link ConfigBlockEnum }
     *     
     */
    public ConfigBlockEnum getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigBlockEnum }
     *     
     */
    public void setName(ConfigBlockEnum value) {
        this.name = value;
    }

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        if (key == null) {
            return " ";
        } else {
            return key;
        }
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    public Locator sourceLocation() {
        return locator;
    }

    public void setSourceLocation(Locator newLocator) {
        locator = newLocator;
    }

}
