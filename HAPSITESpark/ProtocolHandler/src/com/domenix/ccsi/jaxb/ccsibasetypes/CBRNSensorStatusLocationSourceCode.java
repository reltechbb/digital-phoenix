//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.25 at 11:21:34 AM EDT 
//


package com.domenix.ccsi.jaxb.ccsibasetypes;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CBRNSensorStatusLocationSourceCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CBRNSensorStatusLocationSourceCode"&gt;
 *   &lt;restriction base="{file:/C:/ccsi/1.1/CCSI}code-6"&gt;
 *     &lt;maxLength value="6"/&gt;
 *     &lt;enumeration value="HSTGPS"/&gt;
 *     &lt;enumeration value="INTGPS"/&gt;
 *     &lt;enumeration value="MAN"/&gt;
 *     &lt;enumeration value="NONE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CBRNSensorStatusLocationSourceCode")
@XmlEnum
public enum CBRNSensorStatusLocationSourceCode {

    HSTGPS,
    INTGPS,
    MAN,
    NONE;

    public String value() {
        return name();
    }

    public static CBRNSensorStatusLocationSourceCode fromValue(String v) {
        return valueOf(v);
    }

}
