//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.25 at 11:21:34 AM EDT 
//


package com.domenix.ccsi.jaxb.ccsibasetypes;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import com.sun.xml.bind.Locatable;
import com.sun.xml.bind.annotation.XmlLocation;
import org.xml.sax.Locator;


/**
 *  This defines the type defining the set of items that a sensor can
 * 				detect or report on. A sensor may report one or more of the basic sensing class sets
 * 				including environmental data, chemical, etc. Each sensing component on the sensor
 * 				must be defined through the list of items that it can detect/provide.
 * 			
 * 
 * <p>Java class for ItemListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence maxOccurs="unbounded"&gt;
 *         &lt;sequence maxOccurs="unbounded"&gt;
 *           &lt;element name="Capability" type="{file:/C:/ccsi/1.1/CCSI}SensingCapabilityItemType"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="SensingUnit" use="required" type="{file:/C:/ccsi/1.1/CCSI}SensingUnitNameType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemListType", propOrder = {
    "capability"
})
public class ItemListType
    implements Locatable
{

    @XmlElement(name = "Capability", required = true)
    protected List<SensingCapabilityItemType> capability;
    @XmlAttribute(name = "SensingUnit", required = true)
    protected String sensingUnit;
    @XmlLocation
    @XmlTransient
    protected Locator locator;

    /**
     * Gets the value of the capability property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the capability property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCapability().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SensingCapabilityItemType }
     * 
     * 
     */
    public List<SensingCapabilityItemType> getCapability() {
        if (capability == null) {
            capability = new ArrayList<SensingCapabilityItemType>();
        }
        return this.capability;
    }

    /**
     * Gets the value of the sensingUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSensingUnit() {
        return sensingUnit;
    }

    /**
     * Sets the value of the sensingUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSensingUnit(String value) {
        this.sensingUnit = value;
    }

    public Locator sourceLocation() {
        return locator;
    }

    public void setSourceLocation(Locator newLocator) {
        locator = newLocator;
    }

}
