//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.25 at 11:21:34 AM EDT 
//


package com.domenix.ccsi.jaxb.ccsibasetypes;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import com.sun.xml.bind.Locatable;
import com.sun.xml.bind.annotation.XmlLocation;
import org.xml.sax.Locator;


/**
 *  Defines the location of a physical object. which may include sensors, aggregators, vehicles,
 *         and command centers. The latitude and longitude are mandatory values with the altitude defaulting to 0 meters.
 *       
 * 
 * <p>Java class for LocationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LocationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LocationTypeLatitude" type="{file:/C:/ccsi/1.1/CCSI}AbsolutePointLatitudeCoordinate"/&gt;
 *         &lt;element name="LocationTypeLongitude" type="{file:/C:/ccsi/1.1/CCSI}AbsolutePointLongitudeCoordinate"/&gt;
 *         &lt;element name="LocationTypeAltitude" type="{file:/C:/ccsi/1.1/CCSI}VerticalDistanceDimension" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationType", propOrder = {
    "locationTypeLatitude",
    "locationTypeLongitude",
    "locationTypeAltitude"
})
public class LocationType
    implements Locatable
{

    @XmlElement(name = "LocationTypeLatitude", required = true)
    protected BigDecimal locationTypeLatitude;
    @XmlElement(name = "LocationTypeLongitude", required = true)
    protected BigDecimal locationTypeLongitude;
    @XmlElement(name = "LocationTypeAltitude", defaultValue = "0")
    protected BigDecimal locationTypeAltitude;
    @XmlLocation
    @XmlTransient
    protected Locator locator;

    /**
     * Gets the value of the locationTypeLatitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLocationTypeLatitude() {
        return locationTypeLatitude;
    }

    /**
     * Sets the value of the locationTypeLatitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLocationTypeLatitude(BigDecimal value) {
        this.locationTypeLatitude = value;
    }

    /**
     * Gets the value of the locationTypeLongitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLocationTypeLongitude() {
        return locationTypeLongitude;
    }

    /**
     * Sets the value of the locationTypeLongitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLocationTypeLongitude(BigDecimal value) {
        this.locationTypeLongitude = value;
    }

    /**
     * Gets the value of the locationTypeAltitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLocationTypeAltitude() {
        return locationTypeAltitude;
    }

    /**
     * Sets the value of the locationTypeAltitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLocationTypeAltitude(BigDecimal value) {
        this.locationTypeAltitude = value;
    }

    public Locator sourceLocation() {
        return locator;
    }

    public void setSourceLocation(Locator newLocator) {
        locator = newLocator;
    }

}
