//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.25 at 11:21:34 AM EDT 
//


package com.domenix.ccsi.jaxb.ccsibasetypes;

import com.sun.xml.bind.Locatable;
import com.sun.xml.bind.annotation.XmlLocation;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;
import org.xml.sax.Locator;


/**
 *  This defines the structure of a CCSI message header. It is designed to be as small as possible
 * 				to conserve bandwidth on low speed channels when performing normal housekeeping reporting. It includes an
 * 				optional CCSI document to handle channel data reporting and commands. 
 * 
 * <p>Java class for CCSIMsgType complex type.
 
 <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CCSIMsgType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence minOccurs="0"&gt;
 *         &lt;element name="CCSIDoc" type="{file:/C:/ccsi/1.1/CCSI}CCSIDocType"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="sid" use="required" type="{file:/C:/ccsi/1.1/CCSI}SensorIdType" /&gt;
 *       &lt;attribute name="msn" use="required" type="{file:/C:/ccsi/1.1/CCSI}MessageSequenceNumberType" /&gt;
 *       &lt;attribute name="chn" type="{file:/C:/ccsi/1.1/CCSI}ChannelListType" /&gt;
 *       &lt;attribute name="dtg" use="required" type="{file:/C:/ccsi/1.1/CCSI}MsgHdrDtgType" /&gt;
 *       &lt;attribute name="mod" type="{file:/C:/ccsi/1.1/CCSI}MsgSensorModeEnum" /&gt;
 *       &lt;attribute name="len" use="required" type="{file:/C:/ccsi/1.1/CCSI}MsgLengthType" /&gt;
 *       &lt;attribute name="flg" type="{file:/C:/ccsi/1.1/CCSI}MsgContentEnum" /&gt;
 *       &lt;attribute name="ack" type="{file:/C:/ccsi/1.1/CCSI}MessageAckNakEnum" /&gt;
 *       &lt;attribute name="ackmsn" type="{file:/C:/ccsi/1.1/CCSI}MessageSequenceNumberType" /&gt;
 *       &lt;attribute name="nakcod" type="{file:/C:/ccsi/1.1/CCSI}NakReasonEnum" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlRootElement (name="MessageHeaderType")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageHeaderType", propOrder = {
    "ccsiDoc"
})
public class CCSIMsgType
    implements Locatable
{

    @XmlElement(name = "CCSIDoc")
    protected CCSIDocType ccsiDoc;
    @XmlAttribute(name = "sid", required = true)
    protected String sid;
    @XmlAttribute(name = "msn", required = true)
    protected BigDecimal msn;
    @XmlAttribute(name = "chn")
    protected String chn;
    @XmlAttribute(name = "dtg", required = true)
    protected BigDecimal dtg;
    @XmlAttribute(name = "mod")
    protected MsgSensorModeEnum mod;
    @XmlAttribute(name = "len", required = true)
    protected long len;
    @XmlAttribute(name = "flg")
    protected String flg;
    @XmlAttribute(name = "ack")
    protected SensorMessageReceiptAcknowledgementCode ack;
    @XmlAttribute(name = "ackmsn")
    protected BigDecimal ackmsn;
    @XmlAttribute(name = "nakcod")
    protected SensorMessageNegativeAcknowledgementReasonCode nakcod;
    @XmlLocation
    @XmlTransient
    protected Locator locator;

    /**
     * Gets the value of the ccsiDoc property.
     * 
     * @return
     *     possible object is
     *     {@link CCSIDocType }
     *     
     */
    public CCSIDocType getCCSIDoc() {
        return ccsiDoc;
    }

    /**
     * Sets the value of the ccsiDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link CCSIDocType }
     *     
     */
    public void setCCSIDoc(CCSIDocType value) {
        this.ccsiDoc = value;
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public CCSIMsgType parseMsg(String header)
    {
        CCSIMsgType myMsg = null;

        try {
            JAXBContext msgContext = JAXBContext.newInstance(CCSIMsgType.class);
            Unmarshaller unConn = msgContext.createUnmarshaller();
            InputStream targetStream = new ByteArrayInputStream(header.getBytes());
            JAXBElement jaxbMsg = (JAXBElement) unConn.unmarshal(targetStream);
            myMsg = (CCSIMsgType)jaxbMsg.getValue();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return myMsg;
    }
       
       
       
       
    /**
     * Gets the value of the sid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSid() {
        return sid;
    }

    /**
     * Sets the value of the sid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSid(String value) {
        this.sid = value;
    }

    /**
     * Gets the value of the msn property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMsn() {
        return msn;
    }

    /**
     * Sets the value of the msn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMsn(BigDecimal value) {
        this.msn = value;
    }

    /**
     * Gets the value of the chn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChn() {
        return chn;
    }

    /**
     * Sets the value of the chn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChn(String value) {
        this.chn = value;
    }

    /**
     * Gets the value of the dtg property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDtg() {
        return dtg;
    }

    /**
     * Sets the value of the dtg property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDtg(BigDecimal value) {
        this.dtg = value;
    }

    /**
     * Gets the value of the mod property.
     * 
     * @return
     *     possible object is
     *     {@link MsgSensorModeEnum }
     *     
     */
    public MsgSensorModeEnum getMod() {
        return mod;
    }

    /**
     * Sets the value of the mod property.
     * 
     * @param value
     *     allowed object is
     *     {@link MsgSensorModeEnum }
     *     
     */
    public void setMod(MsgSensorModeEnum value) {
        this.mod = value;
    }

    /**
     * Gets the value of the len property.
     * 
     */
    public long getLen() {
        return len;
    }

    /**
     * Sets the value of the len property.
     * 
     */
    public void setLen(long value) {
        this.len = value;
    }

    /**
     * Gets the value of the flg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlg() {
        return flg;
    }

    /**
     * Sets the value of the flg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlg(String value) {
        this.flg = value;
    }

    /**
     * Gets the value of the ack property.
     * 
     * @return
     *     possible object is
     *     {@link SensorMessageReceiptAcknowledgementCode }
     *     
     */
    public SensorMessageReceiptAcknowledgementCode getAck() {
        return ack;
    }

    /**
     * Sets the value of the ack property.
     * 
     * @param value
     *     allowed object is
     *     {@link SensorMessageReceiptAcknowledgementCode }
     *     
     */
    public void setAck(SensorMessageReceiptAcknowledgementCode value) {
        this.ack = value;
    }

    /**
     * Gets the value of the ackmsn property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAckmsn() {
        return ackmsn;
    }

    /**
     * Sets the value of the ackmsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAckmsn(BigDecimal value) {
        this.ackmsn = value;
    }

    /**
     * Gets the value of the nakcod property.
     * 
     * @return
     *     possible object is
     *     {@link SensorMessageNegativeAcknowledgementReasonCode }
     *     
     */
    public SensorMessageNegativeAcknowledgementReasonCode getNakcod() {
        return nakcod;
    }

    /**
     * Sets the value of the nakcod property.
     * 
     * @param value
     *     allowed object is
     *     {@link SensorMessageNegativeAcknowledgementReasonCode }
     *     
     */
    public void setNakcod(SensorMessageNegativeAcknowledgementReasonCode value) {
        this.nakcod = value;
    }

    public Locator sourceLocation() {
        return locator;
    }

    public void setSourceLocation(Locator newLocator) {
        locator = newLocator;
    }

}
