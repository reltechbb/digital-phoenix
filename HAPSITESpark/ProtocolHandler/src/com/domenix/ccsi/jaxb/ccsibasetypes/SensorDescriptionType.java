//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.25 at 11:21:34 AM EDT 
//


package com.domenix.ccsi.jaxb.ccsibasetypes;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import com.sun.xml.bind.Locatable;
import com.sun.xml.bind.annotation.XmlLocation;
import org.xml.sax.Locator;


/**
 *  Container for sensor component information. Each component has an instance of this information
 * 				to define their presence, status, version, and identification information. 
 * 
 * <p>Java class for SensorDescriptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SensorDescriptionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ComponentType" type="{file:/C:/ccsi/1.1/CCSI}CcsiComponentEnum"/&gt;
 *         &lt;element name="Present" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="HardwareVersion" type="{file:/C:/ccsi/1.1/CCSI}VersionType"/&gt;
 *         &lt;element name="SoftwareVersion" type="{file:/C:/ccsi/1.1/CCSI}VersionType"/&gt;
 *         &lt;element name="BitPass" type="{file:/C:/ccsi/1.1/CCSI}PassFailEnum"/&gt;
 *         &lt;element name="SerialNumber" type="{file:/C:/ccsi/1.1/CCSI}MaterielSerialNumberIdentificationText"/&gt;
 *         &lt;element name="AccessRole" type="{file:/C:/ccsi/1.1/CCSI}RoleListType"/&gt;
 *         &lt;element name="AnnunciationType" type="{file:/C:/ccsi/1.1/CCSI}SensorTypeAnnunciatorTypeCode"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SensorDescriptionType", propOrder = {
    "componentType",
    "present",
    "hardwareVersion",
    "softwareVersion",
    "bitPass",
    "serialNumber",
    "accessRole",
    "annunciationType"
})
public class SensorDescriptionType
    implements Locatable
{

    @XmlElement(name = "ComponentType", required = true)
    @XmlSchemaType(name = "string")
    protected SensorComponentCategoryCode componentType;
    @XmlElement(name = "Present")
    protected boolean present;
    @XmlElement(name = "HardwareVersion", required = true)
    protected VersionType hardwareVersion;
    @XmlElement(name = "SoftwareVersion", required = true)
    protected VersionType softwareVersion;
    @XmlElement(name = "BitPass", required = true)
    @XmlSchemaType(name = "string")
    protected ElectronicEquipmentStatusSelfTestResultCode bitPass;
    @XmlElement(name = "SerialNumber", required = true)
    protected String serialNumber;
    @XmlList
    @XmlElement(name = "AccessRole", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected List<AccessRoleCategoryCode> accessRole;
    @XmlElement(name = "AnnunciationType", required = true)
    @XmlSchemaType(name = "string")
    protected SensorTypeAnnunciatorTypeCode annunciationType;
    @XmlLocation
    @XmlTransient
    protected Locator locator;

    /**
     * Gets the value of the componentType property.
     * 
     * @return
     *     possible object is
     *     {@link SensorComponentCategoryCode }
     *     
     */
    public SensorComponentCategoryCode getComponentType() {
        return componentType;
    }

    /**
     * Sets the value of the componentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SensorComponentCategoryCode }
     *     
     */
    public void setComponentType(SensorComponentCategoryCode value) {
        this.componentType = value;
    }

    /**
     * Gets the value of the present property.
     * 
     */
    public boolean isPresent() {
        return present;
    }

    /**
     * Sets the value of the present property.
     * 
     */
    public void setPresent(boolean value) {
        this.present = value;
    }

    /**
     * Gets the value of the hardwareVersion property.
     * 
     * @return
     *     possible object is
     *     {@link VersionType }
     *     
     */
    public VersionType getHardwareVersion() {
        return hardwareVersion;
    }

    /**
     * Sets the value of the hardwareVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link VersionType }
     *     
     */
    public void setHardwareVersion(VersionType value) {
        this.hardwareVersion = value;
    }

    /**
     * Gets the value of the softwareVersion property.
     * 
     * @return
     *     possible object is
     *     {@link VersionType }
     *     
     */
    public VersionType getSoftwareVersion() {
        return softwareVersion;
    }

    /**
     * Sets the value of the softwareVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link VersionType }
     *     
     */
    public void setSoftwareVersion(VersionType value) {
        this.softwareVersion = value;
    }

    /**
     * Gets the value of the bitPass property.
     * 
     * @return
     *     possible object is
     *     {@link ElectronicEquipmentStatusSelfTestResultCode }
     *     
     */
    public ElectronicEquipmentStatusSelfTestResultCode getBitPass() {
        return bitPass;
    }

    /**
     * Sets the value of the bitPass property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElectronicEquipmentStatusSelfTestResultCode }
     *     
     */
    public void setBitPass(ElectronicEquipmentStatusSelfTestResultCode value) {
        this.bitPass = value;
    }

    /**
     * Gets the value of the serialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets the value of the serialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumber(String value) {
        this.serialNumber = value;
    }

    /**
     * Gets the value of the accessRole property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accessRole property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccessRoleCategoryCode }
     * 
     * 
     */
    public List<AccessRoleCategoryCode> getAccessRole() {
        if (accessRole == null) {
            accessRole = new ArrayList<AccessRoleCategoryCode>();
        }
        return this.accessRole;
    }

    /**
     * Gets the value of the annunciationType property.
     * 
     * @return
     *     possible object is
     *     {@link SensorTypeAnnunciatorTypeCode }
     *     
     */
    public SensorTypeAnnunciatorTypeCode getAnnunciationType() {
        return annunciationType;
    }

    /**
     * Sets the value of the annunciationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SensorTypeAnnunciatorTypeCode }
     *     
     */
    public void setAnnunciationType(SensorTypeAnnunciatorTypeCode value) {
        this.annunciationType = value;
    }

    public Locator sourceLocation() {
        return locator;
    }

    public void setSourceLocation(Locator newLocator) {
        locator = newLocator;
    }

}
