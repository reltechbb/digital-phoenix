//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.25 at 11:21:34 AM EDT 
//


package com.domenix.ccsi.jaxb.ccsibasetypes;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.sun.xml.bind.Locatable;
import com.sun.xml.bind.annotation.XmlLocation;
import org.xml.sax.Locator;


/**
 *  Container for sensor unique configuration information. This is information that is added to the
 * 				non-volatile storage and available for access and update through the CCSI standard commands. Each item must have
 * 				a unique name, data type, value, access role list, and update role list. If the item's data type is an array
 * 				then the array definition variable must be provided. 
 * 
 * <p>Java class for SensorConfigItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SensorConfigItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ItemName" type="{file:/C:/ccsi/1.1/CCSI}UniqueNameType"/&gt;
 *         &lt;element name="ItemType" type="{file:/C:/ccsi/1.1/CCSI}CcsiDataType"/&gt;
 *         &lt;element name="ItemValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ItemAccessRole" type="{file:/C:/ccsi/1.1/CCSI}RoleListType"/&gt;
 *         &lt;element name="ItemModifyRole" type="{file:/C:/ccsi/1.1/CCSI}RoleListType"/&gt;
 *         &lt;element name="ItemArrayDefName" type="{file:/C:/ccsi/1.1/CCSI}UniqueNameRefType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SensorConfigItem", propOrder = {
    "itemName",
    "itemType",
    "itemValue",
    "itemAccessRole",
    "itemModifyRole",
    "itemArrayDefName"
})
public class SensorConfigItem
    implements Locatable
{

    @XmlElement(name = "ItemName", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String itemName;
    @XmlElement(name = "ItemType", required = true)
    @XmlSchemaType(name = "string")
    protected CcsiDataType itemType;
    @XmlElement(name = "ItemValue", required = true)
    protected String itemValue;
    @XmlList
    @XmlElement(name = "ItemAccessRole", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected List<AccessRoleCategoryCode> itemAccessRole;
    @XmlList
    @XmlElement(name = "ItemModifyRole", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected List<AccessRoleCategoryCode> itemModifyRole;
    @XmlElement(name = "ItemArrayDefName")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object itemArrayDefName;
    @XmlLocation
    @XmlTransient
    protected Locator locator;

    /**
     * Gets the value of the itemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * Sets the value of the itemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemName(String value) {
        this.itemName = value;
    }

    /**
     * Gets the value of the itemType property.
     * 
     * @return
     *     possible object is
     *     {@link CcsiDataType }
     *     
     */
    public CcsiDataType getItemType() {
        return itemType;
    }

    /**
     * Sets the value of the itemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CcsiDataType }
     *     
     */
    public void setItemType(CcsiDataType value) {
        this.itemType = value;
    }

    /**
     * Gets the value of the itemValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemValue() {
        return itemValue;
    }

    /**
     * Sets the value of the itemValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemValue(String value) {
        this.itemValue = value;
    }

    /**
     * Gets the value of the itemAccessRole property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemAccessRole property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemAccessRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccessRoleCategoryCode }
     * 
     * 
     */
    public List<AccessRoleCategoryCode> getItemAccessRole() {
        if (itemAccessRole == null) {
            itemAccessRole = new ArrayList<AccessRoleCategoryCode>();
        }
        return this.itemAccessRole;
    }

    /**
     * Gets the value of the itemModifyRole property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemModifyRole property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemModifyRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccessRoleCategoryCode }
     * 
     * 
     */
    public List<AccessRoleCategoryCode> getItemModifyRole() {
        if (itemModifyRole == null) {
            itemModifyRole = new ArrayList<AccessRoleCategoryCode>();
        }
        return this.itemModifyRole;
    }

    /**
     * Gets the value of the itemArrayDefName property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getItemArrayDefName() {
        return itemArrayDefName;
    }

    /**
     * Sets the value of the itemArrayDefName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setItemArrayDefName(Object value) {
        this.itemArrayDefName = value;
    }

    public Locator sourceLocation() {
        return locator;
    }

    public void setSourceLocation(Locator newLocator) {
        locator = newLocator;
    }

}
