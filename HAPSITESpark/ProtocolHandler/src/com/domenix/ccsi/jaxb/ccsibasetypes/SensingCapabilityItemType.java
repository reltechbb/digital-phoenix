//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.25 at 11:21:34 AM EDT 
//


package com.domenix.ccsi.jaxb.ccsibasetypes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import com.sun.xml.bind.Locatable;
import com.sun.xml.bind.annotation.XmlLocation;
import org.xml.sax.Locator;


/**
 *  This defines a type for listing an item that can be detected by the
 * 				sensing unit. The full definition may be classified and care should be taken when
 * 				developing the definitions. To support unclassified sensor definitions only the
 * 				basic item name is required. 
 * 
 * <p>Java class for SensingCapabilityItemType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SensingCapabilityItemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ItemType" type="{file:/C:/ccsi/1.1/CCSI}DetectedType"/&gt;
 *         &lt;element name="ItemSpecific" type="{file:/C:/ccsi/1.1/CCSI}IdentityType" minOccurs="0"/&gt;
 *         &lt;element name="MinimumLevel" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="LevelUnits" type="{file:/C:/ccsi/1.1/CCSI}UomLabelType" minOccurs="0"/&gt;
 *         &lt;element name="SampleTime" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/&gt;
 *         &lt;element name="ROCAreaUnderCurve" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/&gt;
 *         &lt;element name="Restrictions" type="{file:/C:/ccsi/1.1/CCSI}SensorRestrictionsList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SensingCapabilityItemType", propOrder = {
    "itemType",
    "itemSpecific",
    "minimumLevel",
    "levelUnits",
    "sampleTime",
    "rocAreaUnderCurve",
    "restrictions"
})
public class SensingCapabilityItemType
    implements Locatable
{

    @XmlElement(name = "ItemType", required = true)
    protected String itemType;
    @XmlElement(name = "ItemSpecific")
    protected String itemSpecific;
    @XmlElement(name = "MinimumLevel", defaultValue = "0")
    protected Double minimumLevel;
    @XmlElement(name = "LevelUnits")
    protected String levelUnits;
    @XmlElement(name = "SampleTime", defaultValue = "0")
    protected Short sampleTime;
    @XmlElement(name = "ROCAreaUnderCurve")
    protected Float rocAreaUnderCurve;
    @XmlElement(name = "Restrictions")
    protected SensorRestrictionsList restrictions;
    @XmlLocation
    @XmlTransient
    protected Locator locator;

    /**
     * Gets the value of the itemType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * Sets the value of the itemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemType(String value) {
        this.itemType = value;
    }

    /**
     * Gets the value of the itemSpecific property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemSpecific() {
        return itemSpecific;
    }

    /**
     * Sets the value of the itemSpecific property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemSpecific(String value) {
        this.itemSpecific = value;
    }

    /**
     * Gets the value of the minimumLevel property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMinimumLevel() {
        return minimumLevel;
    }

    /**
     * Sets the value of the minimumLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMinimumLevel(Double value) {
        this.minimumLevel = value;
    }

    /**
     * Gets the value of the levelUnits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLevelUnits() {
        return levelUnits;
    }

    /**
     * Sets the value of the levelUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLevelUnits(String value) {
        this.levelUnits = value;
    }

    /**
     * Gets the value of the sampleTime property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getSampleTime() {
        return sampleTime;
    }

    /**
     * Sets the value of the sampleTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setSampleTime(Short value) {
        this.sampleTime = value;
    }

    /**
     * Gets the value of the rocAreaUnderCurve property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getROCAreaUnderCurve() {
        return rocAreaUnderCurve;
    }

    /**
     * Sets the value of the rocAreaUnderCurve property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setROCAreaUnderCurve(Float value) {
        this.rocAreaUnderCurve = value;
    }

    /**
     * Gets the value of the restrictions property.
     * 
     * @return
     *     possible object is
     *     {@link SensorRestrictionsList }
     *     
     */
    public SensorRestrictionsList getRestrictions() {
        return restrictions;
    }

    /**
     * Sets the value of the restrictions property.
     * 
     * @param value
     *     allowed object is
     *     {@link SensorRestrictionsList }
     *     
     */
    public void setRestrictions(SensorRestrictionsList value) {
        this.restrictions = value;
    }

    public Locator sourceLocation() {
        return locator;
    }

    public void setSourceLocation(Locator newLocator) {
        locator = newLocator;
    }

}
