//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.25 at 11:21:34 AM EDT 
//


package com.domenix.ccsi.jaxb.ccsibasetypes;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConnectStateEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ConnectStateEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;maxLength value="9"/&gt;
 *     &lt;enumeration value="none"/&gt;
 *     &lt;enumeration value="connected"/&gt;
 *     &lt;enumeration value="locked"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ConnectStateEnum")
@XmlEnum
public enum ConnectStateEnum {

    @XmlEnumValue("none")
    NONE("none"),
    @XmlEnumValue("connected")
    CONNECTED("connected"),
    @XmlEnumValue("locked")
    LOCKED("locked");
    private final String value;

    ConnectStateEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ConnectStateEnum fromValue(String v) {
        for (ConnectStateEnum c: ConnectStateEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
