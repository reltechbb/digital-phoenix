//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.25 at 11:21:34 AM EDT 
//


package com.domenix.ccsi.jaxb.ccsibasetypes;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SensorRangeRestrictorEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SensorRangeRestrictorEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;maxLength value="32"/&gt;
 *     &lt;enumeration value="Temperature"/&gt;
 *     &lt;enumeration value="Humidity"/&gt;
 *     &lt;enumeration value="Light level"/&gt;
 *     &lt;enumeration value="Radiation level"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SensorRangeRestrictorEnum")
@XmlEnum
public enum SensorRangeRestrictorEnum {

    @XmlEnumValue("Temperature")
    TEMPERATURE("Temperature"),
    @XmlEnumValue("Humidity")
    HUMIDITY("Humidity"),
    @XmlEnumValue("Light level")
    LIGHT_LEVEL("Light level"),
    @XmlEnumValue("Radiation level")
    RADIATION_LEVEL("Radiation level");
    private final String value;

    SensorRangeRestrictorEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SensorRangeRestrictorEnum fromValue(String v) {
        for (SensorRangeRestrictorEnum c: SensorRangeRestrictorEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
