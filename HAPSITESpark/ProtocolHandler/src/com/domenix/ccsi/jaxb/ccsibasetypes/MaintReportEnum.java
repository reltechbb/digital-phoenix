//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.25 at 11:21:34 AM EDT 
//


package com.domenix.ccsi.jaxb.ccsibasetypes;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MaintReportEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MaintReportEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;maxLength value="12"/&gt;
 *     &lt;enumeration value="consumable"/&gt;
 *     &lt;enumeration value="failure"/&gt;
 *     &lt;enumeration value="periodic"/&gt;
 *     &lt;enumeration value="bit"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "MaintReportEnum")
@XmlEnum
public enum MaintReportEnum {

    @XmlEnumValue("consumable")
    CONSUMABLE("consumable"),
    @XmlEnumValue("failure")
    FAILURE("failure"),
    @XmlEnumValue("periodic")
    PERIODIC("periodic"),
    @XmlEnumValue("bit")
    BIT("bit");
    private final String value;

    MaintReportEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MaintReportEnum fromValue(String v) {
        for (MaintReportEnum c: MaintReportEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
