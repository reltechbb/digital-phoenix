/*
 * <put file name here>
 *
 * Copyright (c) 2001-2020 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * <put description here>
 *
 */
package com.domenix.ccsi.jaxb;

import com.domenix.ccsi.jaxb.ccsibasetypes.CCSIMsgType;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Administrator
 */
public class BaseCCSIMsgType {
    
//    String msg;
//    CommandTypeEnum commandType;
    

    public BaseCCSIMsgType() {    //String msg, CommandTypeEnum commandType) {
//        this.msg = msg;
//        this.commandType = commandType;
//        CCSIMsgType mht = new CCSIMsgType();
//        mht = this.parseMsg(this.msg); 
    }

    public CCSIMsgType parseMsg(String header) //File msgText )
    {
        CCSIMsgType myHdr = null;

        try {
//      System.out.println( "Testing: " + msgText.getAbsolutePath() );
            JAXBContext msgContext = JAXBContext.newInstance(CCSIMsgType.class);
            Unmarshaller unConn = msgContext.createUnmarshaller();
            InputStream targetStream = new ByteArrayInputStream(header.getBytes());
            JAXBElement theMsg = (JAXBElement) unConn.unmarshal(targetStream);
            myHdr = (CCSIMsgType) theMsg.getValue();

//      StringBuilder outMsg = new StringBuilder( "Message: SID: " ).append( myHdr.getSid() ).append( " msn: " )
//        .append( myHdr.getMsn().longValue() ).append( " Len: " ).append( myHdr.getLen() ).append( '\n' );
//      outMsg.append( "Chn: " ).append( myHdr.getChn() ).append( " dtg:" ).append( myHdr.getDtg().longValue() );
//      if ( myHdr.getMod() != null )
//      {
//        outMsg.append( " mod: " ).append( myHdr.getMod().name() ).append( '\n' );
//      }
//      else
//      {
//        outMsg.append( '\n' );
//      }
//      if ( myHdr.getAck() != null )
//      {
//        outMsg.append( " ack: " ).append( myHdr.getAck().name() );
//      }
//      if ( myHdr.getAckmsn() != null )
//      {
//        outMsg.append( " ackmsn: " ).append( myHdr.getAckmsn().longValue() );
//      }
//      if ( myHdr.getNakcod() != null )
//      {
//        outMsg.append( " nakcod: " ).append( myHdr.getNakcod().name() );
//      }
//      if ( myHdr.getFlg() != null )
//      {
//        outMsg.append( " flg: " ).append( myHdr.getFlg() );
//      }
//      System.out.println( outMsg.toString() );
//      outMsg.setLength( 0 );
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return myHdr;
    }
}
