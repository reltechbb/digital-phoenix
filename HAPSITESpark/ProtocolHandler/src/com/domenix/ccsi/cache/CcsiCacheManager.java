/*
 * CcsiCacheManager.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This class caches IPCWrappers for later retrieval
 *
 * Version: V1.0  10/10/2017
 */
package com.domenix.ccsi.cache;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.common.spark.IPCWrapper;
import java.io.IOException;

//~--- JDK imports ------------------------------------------------------------
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//~--- classes ----------------------------------------------------------------
/**
 * This class caches IPCWrappers for later retrieval
 *
 * @author jmerritt
 *
 * Cache key is a combination of the routing key for an event message plus the
 * system time of receipt in milli-seconds. The IPCWrapper class constructor and
 * setRoutingKey functions both use String cacheKey = type +
 * System.currentTimeMillis() to make the key. The type may be: alert, reading,
 * consumable, status, bit Entries for alerts are kept in the cache until a
 * clear is received. Entries are cached based on type logic such as:
 * --------------------- EXAMPLE --------------------------- switch( routingKey
 * ) { case ccsi.event.reading: { if ( entry.getReadingEvent().isAlert() ) {
 * cacheKey = "alert-"+NumberFormatter.getInteger(System.currentTimeMillis() ,
 * 10 , true , 10 , '0' ); } else { cacheKey = routingKey + "-" +
 * NumberFormatter.getInteger(...); } break; } etc. } ---------------------
 * EXAMPLE ---------------------------
 *
 */
public class CcsiCacheManager
{

    /**
     * Map of the key to IPCWrapper
     */
    private final Map<String, IPCWrapper> cacheMap;

    //~--- constructors --------------------------------------------------------
    /**
     * The constructor
     *
     * @param cfg the configuration information
     * @throws java.io.IOException - an IO Exception occurred
     */
    public CcsiCacheManager(CcsiConfigSensor cfg) throws IOException
    {
        // TODO: remove parameter
        // TODO update CcsiSensorConfig to remove cache information
        cacheMap = new HashMap<>();
    }

    /**
     * Shut down the cache
     *
     * @return flag indicating success (true) or failure (false)
     */
    public boolean shutDownCache()
    {
        cacheMap.clear();

        return true;
    }

    //~--- get methods ---------------------------------------------------------
    /**
     * Retrieve a cached message by it's ID
     *
     * @param key String containing the cache key
     *
     * @return the cached message or null if not found
     */
    public IPCWrapper getCachedMessage(String key)
    {
        IPCWrapper theEntry = cacheMap.get(key);

        return (theEntry);
    }

    /**
     * Returns a list of all entries in the cache.
     *
     * @return an ArrayList of all of the cached entries
     */
    public ArrayList<IPCWrapper> getAll()
    {
        ArrayList<IPCWrapper> results = new ArrayList<>();

        cacheMap.keySet().forEach((key) ->
        {
            results.add(cacheMap.get(key));
        });

        return (results);
    }

    /**
     * Returns a list of all cache entries of a specific routing key.
     *
     * @param keyMatch the key to be returned (alert-, bit-, ...)
     *
     * @return an ArrayList of all cached messages for the specified key
     */
    public ArrayList<IPCWrapper> getAllForRouting(String keyMatch)
    {
        ArrayList<IPCWrapper> results = new ArrayList<>();

        cacheMap.keySet().stream().filter((key)
                -> (key.contains(key))).forEachOrdered((key) ->
        {
            results.add(cacheMap.get(key));
        });

        if (!results.isEmpty())
        {
            Object[] temp = results.toArray();

            Arrays.sort(temp);
            results.clear();

            for (Object x : temp)
            {
                results.add((IPCWrapper) x);
            }
        }

        return (results);
    }

    //~--- methods -------------------------------------------------------------
    /**
     * Stores a received message in the cache.
     *
     * @param msg the received message
     *
     * @return flag indicating success (true) or failure (false) for caching the
     * message
     */
    public boolean storeCachedMessage(IPCWrapper msg)
    {
        boolean retVal = false;

        String cacheKey = msg.getCacheKey();
        if (cacheKey != null)
        {
            cacheMap.put(cacheKey, msg);
            retVal = true;
        }
        return (retVal);
    }

    /**
     * Updates an entry in the cache
     *
     * @param msg the message to update
     *
     * @return boolean indicating success (true) or failure (false)
     */
    public boolean updateCachedMessage(IPCWrapper msg)
    {
        boolean retVal = false;

        String cacheKey = msg.getCacheKey();
        if (cacheKey != null)
        {
            cacheMap.remove(cacheKey);
            cacheMap.put(cacheKey, msg);
            retVal = true;
        }

        return (retVal);
    }

    /**
     * Remove a cached message given its key.
     *
     * @param key String containing the cached element key
     *
     * @return flag indicating success (true) or failure (false)
     */
    public boolean removeCachedMessage(String key)
    {
        boolean retVal = true;
        cacheMap.remove(key);

        return (retVal);
    }
}
