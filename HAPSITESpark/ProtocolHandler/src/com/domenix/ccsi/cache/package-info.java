/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * This package contains the classes and functionality providing a caching mechanism for CCSI
 * sensor reports.
 *
 */
package com.domenix.ccsi.cache;