/*
 * CcsiAlertTypeEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains definition of the three CCSI alert subtypes.
 *
 * Version: V1.0  15/09/10
 */


package com.domenix.ccsi;

/**
 * This enumerates the three sub-levels of CCSI alerts.
 * 
 * @author kmiller
 */
public enum CcsiAlertTypeEnum
{ 
  /** Alert level indicates an active event */
  ALERT , 
  /** Warning level indicates danger but not active */
  WARNING , 
  /** No alert */
  NONE; 
}
