/*
 * SupportedChannelType.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file encapsulates a list of data items on a channel by channel basis
 *
 */
package com.domenix.ccsi;

//~--- JDK imports ------------------------------------------------------------
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.ccsi.protocol.ChannelSupport;
import java.util.ArrayList;
import java.util.List;

//~--- classes ----------------------------------------------------------------
/**
 * Encapsulates a list of channel specific data
 *
 * @author jmerritt
 */
public class SupportedChannelType
{

    /**
     * Field description
     */
    private final ArrayList<ChannelSupport> theChannels = new ArrayList<>();

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructor Creates a list entry for every channel with default values
     */
    public SupportedChannelType()
    {
    }

    //~--- methods --------------------------------------------------------------
    public void setSUD(CcsiChannelEnum chan, String sud)
    {
        ChannelSupport item = getChannel(chan);
        if (item != null)
        {
            item.setSUD(sud);
        }
    }

    public List<String> getSUD(CcsiChannelEnum chan)
    {
        ChannelSupport item = getChannel(chan);
        if (item != null)
        {
            return item.getSUD();
        } else
        {
            return new ArrayList<>();
        }
    }

    public void setMetadata(CcsiChannelEnum chan, String metadata)
    {
        ChannelSupport item = getChannel(chan);
        if (item != null)
        {
            item.setMetadata(metadata);
        }
    }

    public List<String> getMetadata(CcsiChannelEnum chan)
    {
        ChannelSupport item = getChannel(chan);
        if (item != null)
        {
            return item.getMetadata();
        } else
        {
            return new ArrayList<>();
        }
    }

    public void setCompressionThreshold(CcsiChannelEnum chan, int threshold)
    {
        ChannelSupport item = getChannel(chan);
        if (item != null)
        {
            item.setCompressionThreshold(threshold);
        }
    }

    public int getCompressionThreshold(CcsiChannelEnum chan)
    {
        ChannelSupport item = getChannel(chan);
        if (item != null)
        {
            return item.getCompressionThreshold();
        } else
        {
            return -1;
        }
    }

    public boolean isCompressionEnabled(CcsiChannelEnum chan)
    {
        ChannelSupport item = getChannel(chan);
        if (item != null)
        {
            return item.isCompressionEnabled();
        } else
        {
            return false;
        }
    }

    public boolean isChannelSupported(CcsiChannelEnum chan)
    {
        ChannelSupport item = getChannel(chan);
        if (item != null)
        {
            return item.isSupported();
        } else
        {
            return false;
        }
    }

    public void setChannelSupported(CcsiChannelEnum chan, boolean supported)
    {
        ChannelSupport item = getChannel(chan);
        if (item != null)
        {
            item.setSupported(supported);
        }
    }

    /**
     * Gets the ChannelSupport by key number.  Creates one if it doesn't exist.
     * @param keyNumber - format "03"
     * @return 
     */
    public ChannelSupport getChannelByKeyNumber(String keyNumber)
    {
        ChannelSupport channelSupport = null;
        for (ChannelSupport listItem : theChannels)
        {
            if (listItem.getKeyNumber().equals(keyNumber))
            {
                channelSupport = listItem;
                break;
            }
        }

        if (channelSupport == null)
        {
            channelSupport = new ChannelSupport(keyNumber);
            theChannels.add(channelSupport);
        }
        return channelSupport;
    }

    public ArrayList<ChannelSupport> getTheChannels ()
    {
        return theChannels;
    }
    
    private ChannelSupport getChannel(CcsiChannelEnum chan)
    {
        ChannelSupport channelSupport = null;
        for (ChannelSupport listItem : theChannels)
        {
            if (listItem.getChannel() == chan)
            {
                channelSupport = listItem;
                break;
            }
        }
        return channelSupport;
    }
}
