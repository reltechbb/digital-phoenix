/*
 * CcsiLocationSourceEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the types of valid location sources for a CCSI sensor.
 *
 * Version: V1.0  15/08/30
 */


package com.domenix.ccsi;

/**
 * This enumerates the valid CCSI location sources.
 *
 * @author kmiller
 */
public enum CcsiLocationSourceEnum
{
  /** No location source */
  none ,

  /** Location set manually */
  manual ,

  /** Internal GPS location source */
  internal ,

  /** Host set the location */
  host;
}
