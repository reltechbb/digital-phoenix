/*
 * CcsiSensorVariantEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the enumeration of a CCSI type variant for a sensor.
 *
 * Version: V1.0  15/08/29
 */


package com.domenix.ccsi;

/**
 * This enumerates the variants of a CBRN sensor as a point or standoff detector.
 *
 * @author kmiller
 */
public enum CcsiSensorVariantEnum
{
  /** A point detection sensor */
  PNT ,

  /** A standoff detection sensor */
  STNDOF;
}
