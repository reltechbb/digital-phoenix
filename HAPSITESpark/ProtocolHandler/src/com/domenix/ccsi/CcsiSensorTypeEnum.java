/*
 * CcsiSensorTypeEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of the CCSI type of a sensor.
 *
 * Version: V1.0  15/08/29
 */


package com.domenix.ccsi;

/**
 * This enumerates the CCSI sensor general type as a bio, chemical, radiation, nuclear, explosive, or
 * meteorological sensor.
 *
 * @author kmiller
 */
public enum CcsiSensorTypeEnum
{
  /** Biological sensor */
  BIO ,

  /** Chemical sensor */
  CHM ,

  /** Explosive sensor */
  EXP ,

  /** Radiological sensor */
  RAD ,

  /** Nuclear sensor */
  NUC ,

  /** Meteorological sensor */
  MET;
}
