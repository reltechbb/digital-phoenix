/*
 * ActiveAlertsType.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of a single active sensor alert.
 *
 * Version: V1.0  15/09/10
 */


package com.domenix.ccsi;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.common.utils.NumberFormatter;

//~--- JDK imports ------------------------------------------------------------

import java.util.Date;

//~--- classes ----------------------------------------------------------------

/**
 * This class encapsulates the information for a single active sensor alert.
 *
 * @author kmiller
 */
public class ActiveAlertsType
{
  /** Field description */
  private long	alertId;

  /** Field description */
  private String	cacheKey;

  /** Field description */
  private CcsiComponentEnum	component;

  /** Field description */
  private CcsiAlertLevelEnum	level;

  /** Field description */
  private Date	timeStamp;

  /** Field description */
  private CcsiAlertTypeEnum	type;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an empty class instance
   *
   */
  public ActiveAlertsType()
  {
    this.cacheKey		= null;
    this.alertId		= 0L;
    this.type				= CcsiAlertTypeEnum.NONE;
    this.level			= CcsiAlertLevelEnum.NONE;
    this.component	= CcsiComponentEnum.CC;
  }

  /**
   * Constructs a class instance with all parameters
   *
   * @param key the cache key of the alert message
   * @param id the ID number of the alert
   * @param type the alert type (alert, warning, none)
   * @param lvl the alert level (trigger,untrigger, none)
   * @param comp the component generating the alert
   * @param stamp the date time of the alert event
   */
  public ActiveAlertsType( String key , long id , CcsiAlertTypeEnum type , CcsiAlertLevelEnum lvl ,
                           CcsiComponentEnum comp , Date stamp )
  {
    this.cacheKey		= key;
    this.alertId		= id;
    this.type				= type;
    this.level			= lvl;
    this.component	= comp;
    if (stamp != null)
    {
        this.timeStamp	= (Date) stamp.clone();
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the CacheKey
   */
  public String getCacheKey()
  {
    return cacheKey;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param CacheKey the CacheKey to set
   */
  public void setCacheKey( String CacheKey )
  {
    this.cacheKey	= CacheKey;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the type
   */
  public CcsiAlertTypeEnum getType()
  {
    return type;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param type the type to set
   */
  public void setType( CcsiAlertTypeEnum type )
  {
    this.type	= type;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the alertId
   */
  public long getAlertId()
  {
    return alertId;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param alertId the alertId to set
   */
  public void setAlertId( long alertId )
  {
    this.alertId	= alertId;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the level
   */
  public CcsiAlertLevelEnum getLevel()
  {
    return level;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param level the level to set
   */
  public void setLevel( CcsiAlertLevelEnum level )
  {
    this.level	= level;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Returns a string containing the current object state.
   *
   * @return String containing the current state
   */
  @Override
  public String toString()
  {
    return ( this.toStateString( 0 ) );
  }

  /**
   * Returns a string formatted for use in saving and restoring active alert states which is:
   * the cache key, alert ID, the type, the level, the component, and the timestamp separated by commas.
   *
   * @param index the index number to be used for the
   *
   * @return - string representation of alerts
   */
  public String toStateString( int index )
  {
    StringBuilder	state	= new StringBuilder();

    state.append( NumberFormatter.getInt( index , 10 , true , 2 , '0' ) );
    state.append( '|' ).append( this.cacheKey ).append( ',' ).append( this.alertId ).append( ',' ).append(
        this.type.name() ).append( ',' ).append( this.level.name() ).append( this.component.name() ).append(
        NumberFormatter.getDate( timeStamp , null ) );

    return ( state.toString() );
  }

  /**
   * Returns the CCSI alert ID string for this alert.
   *
   * @return String containing the CCSI alert ID string
   */
  public String toAlertIdString()
  {
    StringBuilder	msg	= new StringBuilder();

    switch ( this.type )
    {
      case ALERT :
        msg.append( 'A' );

        break;

      case WARNING :
        msg.append( 'W' );

        break;

      case NONE :
        msg.append( 'N' );

        break;
    }

    switch ( this.level )
    {
      case TRIGGER :
        msg.append( 'T' );

        break;

      case UNTRIGGER :
        msg.append( 'U' );

        break;

      case NONE :
        msg.append( 'N' );

        break;
    }

    msg.append( NumberFormatter.getLong( alertId , 10 , true , 9 , '0' ) );

    return ( msg.toString() );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the component
   */
  public CcsiComponentEnum getComponent()
  {
    return component;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param component the component to set
   */
  public void setComponent( CcsiComponentEnum component )
  {
    this.component	= component;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the timeStamp
   */
  public Date getTimeStamp()
  {
    Date retVal = null;
    if (timeStamp != null)
    {
        retVal = (Date) timeStamp.clone();
    }
    return retVal;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param timeStamp the timeStamp to set
   */
  public void setTimeStamp( Date timeStamp )
  {
    if (timeStamp != null)
    {
        this.timeStamp	= (Date) timeStamp.clone ();
    } else
    {
        this.timeStamp = null;
    }
  }
}
