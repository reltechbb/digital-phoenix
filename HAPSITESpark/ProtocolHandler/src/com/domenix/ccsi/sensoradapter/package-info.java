/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * This package provides the interface and control logic for the Sensor Adapter component of a sensor CCSI
 * implementation that interfaces with the sensing component and adapts data for use by the other
 * components.
 *
 */
package com.domenix.ccsi.sensoradapter;