/*
 * SensorAdapter.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the controlling logic for the Sensor Interface and 
 * Sensor Protocol Adapters.
 *
 */
package com.domenix.ccsi.sensoradapter;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.common.spark.CtlWrapper;
import com.domenix.common.spark.CtlWrapperQueue;
import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.commonsa.interfaces.SensorProtocolAdapterReaderInterface;
import com.domenix.commonsa.interfaces.SensorProtocolAdapterWriterInterface;
import com.domenix.common.spark.IPCWrapperQueue;
import com.domenix.common.spark.IPCQueueEvent;
import com.domenix.common.spark.IPCQueueListenerInterface;
import com.domenix.common.utils.SparkThreadFactory;
import com.domenix.sensorinterfaceadapter.SIAInterface;
import com.domenix.sensorinterfaceadapter.SIAInterfaceEthernetImpl;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

//~--- classes ----------------------------------------------------------------
/**
 * The controlling logic for starting and stopping the Sensor Protocol and Interface Adapters.
 * @author kmiller
 */
public class SensorAdapter implements Runnable, IPCQueueListenerInterface
{

    /**
     * Error/Debug logger
     */
    private final Logger myLogger = Logger.getLogger(SensorAdapter.class);

    /**
     * Field description
     */
    private boolean shutdownNow = false;

    /**
     * Field description
     */
    private IPCWrapperQueue adapterDataQueue;

    /**
     * Sensor Adapter control queue
     */
    private CtlWrapperQueue ctlQueue;

    /**
     * The executor for service threads
     */
    private final ExecutorService executor;

    /**
     * The sensor adapters
     */
    private final SIAInterface theSIA;
    //private final SIAInterfaceEthernetImpl theSIA;
//    private final SensorInterfaceAdapterWriterInterface siAdapterWriter;
//    private final SensorInterfaceAdapterReaderInterface siAdapterReader;
    private final SensorProtocolAdapterWriterInterface spAdapterWriter;
    private final SensorProtocolAdapterReaderInterface spAdapterReader;

    /**
     * Field description
     */
    private CtlWrapperQueue rcvrCtlQueue;

    /**
     * Saved sensor state
     */
    private CcsiSavedState savedState;

    /**
     * Sensor configuration
     */
    private CcsiConfigSensor sensorCfg;

    private final SparkThreadFactory sparkThreadFactory;
    
    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs the sensor adapter class. Operations will not commence until
     * the run method is invoked by creating and executing a thread.
     *
     * @param ctlQueue the control queue for the sensor adapter
     * @param siAdapterWriter
     * @param siAdapterReader
     * @param spAdapterWriter
     * @param spAdapterReader
     * @param config the sensor configuration information
     * @param state the sensor saved state information
     */
    public SensorAdapter(CtlWrapperQueue ctlQueue,
            SIAInterface theSIA,
            //SIAInterfaceEthernetImpl theSIA,
//            SensorInterfaceAdapterWriterInterface siAdapterWriter,
//            SensorInterfaceAdapterReaderInterface siAdapterReader,
            SensorProtocolAdapterWriterInterface spAdapterWriter,
            SensorProtocolAdapterReaderInterface spAdapterReader,
            CcsiConfigSensor config, CcsiSavedState state)
    {
        this.ctlQueue = ctlQueue;
        this.sensorCfg = config;
        this.savedState = state;
        this.theSIA = theSIA;
//        this.siAdapterWriter = siAdapterWriter;
//        this.siAdapterReader = siAdapterReader;
        this.spAdapterWriter = spAdapterWriter;
        this.spAdapterReader = spAdapterReader;
        sparkThreadFactory = new SparkThreadFactory ();
        this.executor = Executors.newFixedThreadPool(4, sparkThreadFactory);
        this.ctlQueue.addIPCQueueEventListener(this);
    }

    
//    public SensorProtocolAdapterWriterInterface getSPAWI()
//    {
//            return spAdapterWriter;
//    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the ctlQueue
     */
    public CtlWrapperQueue getCtlQueue()
    {
        return ctlQueue;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param ctlQueue the ctlQueue to set
     */
    public void setCtlQueue(CtlWrapperQueue ctlQueue)
    {
        this.ctlQueue = ctlQueue;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the sensorCfg
     */
    public CcsiConfigSensor getSensorCfg()
    {
        return sensorCfg;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param sensorCfg the sensorCfg to set
     */
    public void setSensorCfg(CcsiConfigSensor sensorCfg)
    {
        this.sensorCfg = sensorCfg;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the savedState
     */
    public CcsiSavedState getSavedState()
    {
        return savedState;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param savedState the savedState to set
     */
    public void setSavedState(CcsiSavedState savedState)
    {
        this.savedState = savedState;
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Runs the instance once a thread is created and executed.
     */
    @Override
    public void run()
    {
        try
        {
            myLogger.info ("Starting SensorAdapter Thread");
            sparkThreadFactory.setThreadName("SPWriterThread");
            spAdapterWriter.doRun(executor);
            sparkThreadFactory.setThreadName("SPReaderThread");
            spAdapterReader.doRun(executor);
//            sparkThreadFactory.setThreadName("SIWriterThread");
//            siAdapterWriter.doRun(executor);
//            sparkThreadFactory.setThreadName("SIReaderThread");
//            siAdapterReader.doRun(executor);
            sparkThreadFactory.setThreadName("the Sensor Interface Adapter");
            theSIA.doRun(executor);

            
            while (!shutdownNow)
            {
                try
                {
                    while (!executor.awaitTermination(5, TimeUnit.SECONDS))
                    {
                    }
                } catch (InterruptedException ignored)
                {
                }
            }
            // shutdown the Sensor Adapters
            spAdapterWriter.shutdown();
            spAdapterReader.shutdown();
//            siAdapterWriter.shutdown();
//            siAdapterReader.shutdown();
            theSIA.shutDownInterface();
       } catch (Exception ex)
        {
            myLogger.fatal("Sensor adapter run exception", ex);
        }
    }

    /**
     * Read queue events to determine if the sensor adapter is being shut down.
     *
     * @param evt
     */
    @Override
    public void queueEventFired(IPCQueueEvent evt)
    {
//        System.out.println("SENSOR ADAPTOR:  queueEventFired:  " + evt.toString());
//        System.out.println();
        if (evt.getEventType() == IPCQueueEvent.IPCQueueEventType.IPC_NEW)
        {
            if (evt.getEventSource() instanceof CtlWrapperQueue)
            {
                CtlWrapper entry = (CtlWrapper) evt.getPayload();

//        this.ctlQueue.removeFirst();    // We don't need the contents of these
                if (entry.getCtlCommand() == CtlWrapper.STOP_THREAD)
                {
                    this.shutdownNow = true;
                } else
                {

                }
                this.ctlQueue.removeFirst();
            } else if (evt.getEventSource() instanceof IPCWrapperQueue)
            {
                this.adapterDataQueue.removeFirst();
            } else
            {
            }
        }
    }
}
