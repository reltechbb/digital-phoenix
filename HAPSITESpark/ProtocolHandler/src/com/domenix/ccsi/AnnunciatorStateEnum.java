/*
 * AnnunciatorStateEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the states that an annunciator may have
 *
 */
package com.domenix.ccsi;

/**
 * Annunciator states
 * @author jmerritt
 */
public enum AnnunciatorStateEnum
{
    /**
     * No annunciator
     */
    ABSENT,
    /**
     * Annunciator is normal
     */
    NORMAL,
    /** 
     * Annunciator is off
     */
    OFF,
    /**
     * Annunciator is silenced
     */
    SILNCD
}
