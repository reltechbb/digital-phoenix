/*
 * CcsiSreEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/28
 */


package com.domenix.ccsi;

/**
 *
 * @author kmiller
 */
public enum CcsiSreEnum
{
  LOGIN_SUCCESS("login_success" , "User {username} successfully logged in at {dtg}") ,
  LOGIN_FAIL("login_fail" , "User login failed at {dtg}") ,
  LOGIN_TIMEOUT("login_timeout" , "User login attempt timed out waiting for input at {dtg}") ,
  LOGOFF("logoff" , "User {username} logged out at {dtg}") ,
  INSUFFICIENT_PRIVILEGE(
    "insufficient_privilege" ,
    "Command {cmdname} received from {hostname} with an invalid role for execcution") , INVALID_INPUT(
      "invalid_input" , "Received input that is invalid.") , UNKNOWN_USER(
      "unknown_user" , "User {username} is unknown and attempted login at {dtg}") , UNKNOWN_SESSION(
      "unknown_session" , "Unrecognized or invalid process ID or certificate received at {dtg}") , ACCESS_VIOLATION(
      "access_violation" , "Unauthorized access attempt at {dtg}") , DEVICE_DISABLED(
      "device_disabled" , "Device {devname} has failed or been disabled at {dtg}") , SECURITY_BIT_FAIL(
      "security_bit_fail" , "A test of sensor security features has failed at {dtg}") , TAMPER_EVENT(
      "tamper_event" , " ") , INVALID_MESSAGE(
      "invalid_message" , "Message {msnreceived} is invalid {reason} at {dtg}") , UNAUTHORIZED_MESSAGE(
      "unauthorized_message" ,
      "An unauthorized message {msgtype} was received from {msgsource] on {linkname} at {dtg}") , SECURITY_LOG_ERASED(
        "security_log_erased" , "The SRE log was erased by {msgsource} at {dtg}") , ATTACK_DETECTED(
        "attack_detected" , "Detected a network attach on {linkname} at {dtg}") , INVALID_MARKING(
        "invalid_marking" , "Message {msnreceived} contained invalid classification/security markings") , CONNECT(
        "connect" , "Connected to host {hostip} on {linkname} at {dtg}") , CONNECT_FAIL(
        "connect_fail" , "Connection to host {hostip} failed on {linkname] at {dtg}") , CONNECT_END(
        "connect_end" , "Connection to host {hostip} on {linkname] ended at {dtg}") , NONE(
        "none" , "No current SRE to be reported") , OTHER("other" , "Any other security relevant event");

  /** The default text for an error message with embedded macros */
  private final String	defText;

  /** The XML enumeration string for this Java enumeration */
  private final String	xmlCode;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an enumeration instance
   *
   * @param xmlCode the associated XML enumeration name
   * @param defaultText the default text for a detail report
   */
  private CcsiSreEnum( String xmlCode , String defaultText )
  {
    this.xmlCode	= xmlCode;
    this.defText	= defaultText;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the xmlCode
   */
  public String getXmlCode()
  {
    return xmlCode;
  }

  /**
   * @return the defText
   */
  public String getDefText()
  {
    return defText;
  }
}
