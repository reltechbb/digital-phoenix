/*
 * CcsiBitStatusEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the enumeration for the result of a BIT.
 *
 * Version: V1.0  15/08/30
 */


package com.domenix.ccsi;

/**
 * This enumerates the results of a built in test (BIT).
 *
 * @author kmiller
 */
public enum CcsiBitStatusEnum
{
  /** Bit passed */
  PASS ,

  /** Bit failed */
  FAIL;
}
