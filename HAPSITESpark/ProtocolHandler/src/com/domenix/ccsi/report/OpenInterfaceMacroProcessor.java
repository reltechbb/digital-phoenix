/*
 * HeaderMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/11/03
 */
package com.domenix.ccsi.report;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------
/**
 * This class generates alert report information for readings.
 *
 * @author kmiller
 */
public class OpenInterfaceMacroProcessor extends MacroProcessor {

    /**
     * Field description
     */
    
    public final String headerStart = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <ccsi:Hdr xmlns:ccsi=\"file:/C:/ccsi/1.1/CCSI\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ";
    
    public final String openIfcInfo = "type=\"start\" sid=\"{sid}\"/>";
 
    /**
     * The sensor configuration
     */
    private CcsiConfigSensor theConfig;

    /**
     * The saved state
     */
//    private CcsiSavedState theState;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs a class instance
     *
     * @param cfg the sensor configuration
     * @param state the saved state
     */
    public OpenInterfaceMacroProcessor(CcsiConfigSensor cfg) {     //CcsiConfigSensor cfg, CcsiSavedState state) {
        super();
        if (cfg != null) {     // && (state != null)) {
            this.theConfig = cfg;
//            this.theState = state;
            initialize();    
        } else {
            throw new IllegalArgumentException("Missing or null argument.");
        }
    }


    //~--- methods --------------------------------------------------------------
    /**
     * This initializes
     *
     */
    private void initialize() {
        super.textMacros = "\\{(sid)\\}";
        super.macroMap.put("sid", this);
    }

    /**
     * This method translates a particular macro into its string value or null if there is no
     * translation.
     *
     * @param macro the macro to be translated
     *
     * @return String containing the replacement value or null if no replacement
     */
    @Override
    public String translateMacro(String macro) {
        String retVal = "";

        switch (macro) {
            case "sid":
                retVal = "b4d81dec-616b-4f2c-a8b2-f787ad24826e";
//                retVal = this.theConfig.getUuid();
                break;
                
            default:
                break;
        }

        return (retVal);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * This method runs macro processing on the input string and returns the result.
     *
     * @param inString the string to be processed
     *
     * @return String containing the result of macro processing
     */
    @Override
    public String getTranslation(String inString) {
        return (super.getTranslation(inString));
    }
}
