/*
 * AlertBitMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/11/03
 */


package com.domenix.ccsi.report;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.ccsi.protocol.PendingAlertEntry;
import com.domenix.common.spark.data.BITEventMsg;
import com.domenix.common.spark.data.BitResultEnum;
import com.domenix.utils.MacroProcessor;
import com.domenix.common.utils.NumberFormatter;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Date;

//~--- classes ----------------------------------------------------------------

/**
 * This class generates alert report information for BIT results.
 *
 * @author kmiller
 */
public class AlertBitMacroProcessorEXAMPLE extends MacroProcessor
{
  /** Field description */
  public final static String BIT_ALERT_REPORT = "<Msg><AlertsChn Event=\"{eventAlert}\" Source=\"{sourceAlert}\""
                                        + " Time=\"{alertDtg}\" AlertId=\"{alertId}\">{bit}</AlertsChn></Msg>";

  /** Field description */
  private final StringBuilder	bldr	= new StringBuilder();

  /** Field description */
  private BITEventMsg	bit;

  /** Field description */
  private PendingAlertEntry	msg;

  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs ...
   *
   *
   * @param cfg
   * @param state
   * @param bit
   */
  public AlertBitMacroProcessorEXAMPLE( CcsiConfigSensor cfg , CcsiSavedState state , BITEventMsg bit )
  {
    super();
    this.theConfig	= cfg;
    this.theState		= state;
    this.bit				= bit;
    initialize();
  }

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   * @param msg the content containing the BITEventMsg
   */
  public AlertBitMacroProcessorEXAMPLE( CcsiConfigSensor cfg , CcsiSavedState state , PendingAlertEntry msg )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      if ( ( msg != null ) )
      {
        this.msg	= msg;

        if ( this.msg.getBaseMsg().isSetBIT() )
        {
          this.bit	= ( (ArrayList<BITEventMsg>) msg.getBaseMsg().getBIT() ).get( 0 );
        }
      }

      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes
   *
   */
  private void initialize()
  {
    super.textMacros	= "\\{(eventAlert|sourceAlert|alertDtg|alertId|bit)\\}";
    super.macroMap.put( "eventalert" , this );
    super.macroMap.put( "sourcealert" , this );
    super.macroMap.put( "alertdtg" , this );
    super.macroMap.put( "alertid" , this );
    super.macroMap.put( "bit" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {
    String	retVal	= "";
    long		pVal		= 0L;

    if ( this.msg != null )
    {
      switch ( macro )
      {
        case "eventalert" :
          if ( this.msg.isTrigger() )
          {
            retVal	= ( ( this.bit.getResult() == BitResultEnum.FAIL )
                        ? "ALERT"
                        : "DEALERT" );
          }
          else
          {
            retVal	= ( ( this.bit.getResult() == BitResultEnum.FAIL )
                        ? "ALERT"
                        : "DEALERT" );
          }

          break;

        case "sourcealert" :
          retVal	= "bit";

          break;

        case "alertdtg" :
          Date	temp;

          if ( this.bit.isSetBitTime() )
          {
            temp	= this.bit.getBitTime().toGregorianCalendar().getTime();
          }
          else
          {
            temp	= null;
          }

          retVal	= NumberFormatter.getDate( temp , null );

          break;

        case "alertid" :
          retVal	= msg.getAlertId();

          break;

        case "bit" :
          bldr.setLength( 0 );

          switch ( this.bit.getComponentType() )
          {
            case CC :
              bldr.append( "<BIT Component=\"CC\" Executed=\"" );

              if ( this.bit.isSetBitTime() )
              {
                temp	= this.bit.getBitTime().toGregorianCalendar().getTime();
              }
              else
              {
                temp	= null;
              }

              bldr.append( NumberFormatter.getDate( temp , null ) ).append( "\" Result=\"" );

              break;

            case PDIL :
              bldr.append( "<BIT Component=\"PDIL\" Executed=\"" );

              if ( this.bit.isSetBitTime() )
              {
                temp	= this.bit.getBitTime().toGregorianCalendar().getTime();
              }
              else
              {
                temp	= null;
              }

              bldr.append( NumberFormatter.getDate( temp , null ) ).append( "\" Result=\"" );

              break;

            case SC :
              bldr.append( "<BIT Component=\"SC\" ComponentId=\"" );
              bldr.append( this.theState.getComponentSCName().getScName() ).append( "\" Executed=\"" );

              if ( this.bit.isSetBitTime() )
              {
                temp	= this.bit.getBitTime().toGregorianCalendar().getTime();
              }
              else
              {
                temp	= null;
              }

              bldr.append( NumberFormatter.getDate( temp , null ) ).append( "\" Result=\"" );

              break;

            default :
              retVal	= " ";
          }

          bldr.append( this.bit.getResult().name() ).append( "\"/>" );
          retVal	= bldr.toString();

          break;

        default :
          break;
      }
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return ( super.getTranslation( inString ) );
  }
}
