/*
 * ConfigGeneralMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 * This class provides for the generation of GENERAL blocks of configuration channel reports.
 * 
 * @author kmiller
 */
public class ConfigGeneralMacroProcessor extends MacroProcessor
{
  public final String generalConfigReport = "<Block Name=\"GENERAL\"><Item Name=\"TimeSource\""
  + " Value=\"{timeSource}\"/><Item Name=\"SelectedTimeSource\" Value=\"{selectTimeSource}\"/>"
  + "<Item Name=\"LocationSource\" Value=\"{locSource}\"/><Item Name=\"SelectedLocationSource\" Value=\"{selectLocSource}\"/>"
  + "<Item Name=\"UnitMode\" Value=\"{unitMode}\"/><Item Name=\"LocationReportOption\""
  + " Value=\"{locReportOpt}\"/></Block>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   */
  public ConfigGeneralMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  { //timeSource|locSource|selectTimeSource|selectLocSource|unitMode|locReportOpt|dismountPwrWrnLvl|dismountPwrShutLvl
    super.textMacros	=
      "\\{(timeSource|locSource|selectTimeSource|selectLocSource|unitMode|locReportOpt|dismountPwrWrnLvl|dismountPwrShutLvl)\\}";
    super.macroMap.put( "timesource" , this );
    super.macroMap.put( "locsource" , this );
    super.macroMap.put( "selecttimesource" , this );
    super.macroMap.put( "selectlocsource" , this );
    super.macroMap.put( "unitmode" , this );
    super.macroMap.put( "locreportopt" , this );
    super.macroMap.put( "dismountpwrwrnlvl" , this );
    super.macroMap.put( "dismountpwrshutlvl" , this );
 }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "timesource" :
        retVal = this.theState.getTimesource().name().toLowerCase();
        if ( retVal.equalsIgnoreCase( "none" ) )
        {
          retVal = "internal";
        }
        
        break;

      case "locsource" :
        retVal = this.theState.getLocationsource().name().toLowerCase();
        
        break;

      case "selecttimesource" :
        retVal = this.theState.getSelTimesource().name().toLowerCase();
        if ( retVal.equalsIgnoreCase( "none" ) )
        {
          retVal = "internal";
        }
        break;

      case "selectlocsource" :
        retVal = this.theState.getSelLocationsource().name().toLowerCase();
        
        break;
        
      case "unitmode" :
        retVal = this.theState.getMode().getLongName().trim();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "locreportopt" :
        retVal = this.theState.getLocationreportoption().name();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "dismountpwrwrnlvl" :
        //retVal = Integer.toString(this.theConfig.getConnRetryTime());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "dismountpwrshutlvl" :
        //retVal = Integer.toString(this.theConfig.getConnRetryTime());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
