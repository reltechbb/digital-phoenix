/*
 * ConfigCcSensdescMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 * This class provides generation of SENSDESC elements for Config channel reports.
 * 
 * @author kmiller
 */
public class ConfigCcSensdecMacroProcessor extends MacroProcessor
{
  /** The SENSDESC element macro formatting */
  public final String CcSensdecConfigReport = "<Block Name=\"SENSDESC\" Key=\"{keyVal}\"><Item Name=\"ComponentType\""
  + " Value=\"{componentValue}\"/><Item Name=\"Present\" Value=\"{itemValue}\"/><Item Name=\"HardwareVersion\" Value=\"{hwVersion}\"/>"
  + "<Item Name=\"SoftwareVersion\" Value=\"{swVersion}\"/><Item Name=\"BitPass\" Value=\"{passFail}\"/><Item Name=\"SerialNumber\""
  + " Value=\"{serialNum}\"/><Item Name=\"AccessRole\" Value=\"{accessRole}\"/><Item Name=\"AnnunciationType\" Value=\"{annuncType}\"/>"
  + "</Block>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   */
  public ConfigCcSensdecMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  { 
    super.textMacros	=
      "\\{(keyVal|componentValue|itemValue|hwVersion|swVersion|passFail|serialNum|accessRole|annuncType)\\}";
    super.macroMap.put( "keyval" , this );
    super.macroMap.put( "componentvalue" , this );
    super.macroMap.put( "itemvalue" , this );
    super.macroMap.put( "hwversion" , this );
    super.macroMap.put( "swversion" , this );
    super.macroMap.put( "passfail" , this );
    super.macroMap.put( "serialnum" , this );
    super.macroMap.put( "accessrole" , this );
    super.macroMap.put( "annunctype" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {//keyVal componentValue itemValue hwVersion swVersion passFail serialNum accessRole annuncType
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "keyval" :
        retVal = this.theState.getComponentCC().name();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "componentvalue" :
        retVal = this.theState.getComponentCC().toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "itemvalue" :
        retVal = Boolean.TRUE.toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "hwversion" :
        retVal = this.theConfig.getHwVersion().toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
        
      case "swversion" :
        retVal = this.theConfig.getSwVersion().toString();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "passfail" :
        retVal = this.theState.getComponentCCBitStatus().toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "serialnum" :
        retVal = this.theConfig.getSerial();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "accessrole" :
        retVal = "ANY";
//        if ( retVal == null )
//        {
//          retVal = "";
//        }
        break;
        
      case "annunctype" :
        retVal = "UIC";
//        if ( retVal == null )
//        {
//          retVal = "";
//        }
        break;

      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
