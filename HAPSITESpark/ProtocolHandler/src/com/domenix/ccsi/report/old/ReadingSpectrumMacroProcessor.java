/*
 * IdentMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.common.spark.data.ReadingSpectrumRow;
import com.domenix.utils.MacroProcessor;
import java.math.BigInteger;

//~--- classes ----------------------------------------------------------------

/**
 * This class produces spectrum information for the NGCD sensor report.
 * 
 * @author kmiller
 */
public class ReadingSpectrumMacroProcessor extends MacroProcessor
{
  /** The macro definitions for a reading spectrum */
  public final String readingSpectrumReport = "<Spectrum SpectrumName=\"{spname}\">"
  + "<Dimension order=\"{order}\" size=\"{size}\"/><SpectralData>{data}</SpectralData>"
  + "</Spectrum>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  /** The reading spectrum row */
  private ReadingSpectrumRow msg;
  
  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   * @param msg the reading spectrum rows
   */
  public ReadingSpectrumMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state , ReadingSpectrumRow msg)
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      if (msg != null) {
          this.msg = msg;
      }
      
      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  { //spname|order|size|data
    super.textMacros	=
      "\\{(spname|order|size|data)\\}";
    super.macroMap.put( "spname" , this );
    super.macroMap.put( "order" , this );
    super.macroMap.put( "size" , this );
    super.macroMap.put( "data" , this );


  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  { //spname|order|size|data
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "spname" :
        retVal = this.msg.getPolarity();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
 
      case "order" :
        double d1 = this.msg.getM1();
        retVal = Double.toString(d1);     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "size" :  
        double d2 = this.msg.getM2();
        retVal = Double.toString(d2);
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
       
      case "data" :
        BigInteger bi = this.msg.getCount();
        retVal = bi.toString();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
