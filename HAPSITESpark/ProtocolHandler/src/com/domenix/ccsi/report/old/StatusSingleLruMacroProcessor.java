/*
 * IdentMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.CcsiComponentEnum;
import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 * This class generates a single LRU element for a status report
 * 
 * @author kmiller
 */
public class StatusSingleLruMacroProcessor extends MacroProcessor
{
  /** The macro text for an LRU */
  public final String statusSingleLruReport = "<LruStatus Component=\"{component}\" Executed=\"{extime}\" Result=\"{result}\"/>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  private CcsiComponentEnum component;
  
  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   * @param component the type of component
   */
  public StatusSingleLruMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state, CcsiComponentEnum component )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      this.component = component;
      
      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  { //component|extime|result
    super.textMacros	=
      "\\{(component|extime|result)\\}";
    super.macroMap.put( "component" , this );
    super.macroMap.put( "extime" , this );
    super.macroMap.put( "result" , this );

  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  { //component|extime|result
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "component" :
        retVal = this.component.toString();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
 
      case "extime" :
        if (this.component == CcsiComponentEnum.CC) {
            retVal = this.theState.getComponentCCBitTime().toString();
        } else if (this.component == CcsiComponentEnum.PDIL) {
            retVal = this.theState.getComponentPDILBitTime().toString();
        } else {
            retVal = this.theState.getComponentSCBitTime(component.name()).toString(); 
        }         
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
  
      case "result" :
        if (this.component == CcsiComponentEnum.CC) {
            retVal = this.theState.getComponentCCStatus().toString();
        } else if (this.component == CcsiComponentEnum.PDIL) {
            retVal = this.theState.getComponentPDILBitStatus().toString();
        } else {
            retVal = this.theState.getComponentSCBitStatus(component.name()).toString(); 
        }         
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
        
      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
