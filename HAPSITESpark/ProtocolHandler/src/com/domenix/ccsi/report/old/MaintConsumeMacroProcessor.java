/*
 * MaintConsumeMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/11/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.common.spark.data.ConsumableEventMsg;
import com.domenix.utils.MacroProcessor;
import java.math.BigInteger;

//~--- classes ----------------------------------------------------------------

/**
 * This class generates MAINT channel reports for consumable events.
 * 
 * @author kmiller
 */
public class MaintConsumeMacroProcessor extends MacroProcessor
{
  //item|yesno|level|units
  public final String maintConsumeReport = "<MaintChn Type=\"consumable\"><Consumables Item=\"{item}\""
  + " NeedsAction=\"{yesno}\" SupplyLevel=\"{level}\" SupplyUnits=\"{units}\"/></MaintChn>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  private ConsumableEventMsg msg;
  
  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   * @param msg the message
   */
  public MaintConsumeMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state , ConsumableEventMsg msg)
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      if (msg != null) {
          this.msg = msg;
      }
      
      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  { //item|yesno|level|units
    super.textMacros	=
      "\\{(item|yesno|level|units)\\}";
    super.macroMap.put( "item" , this );
    super.macroMap.put( "yesno" , this );
    super.macroMap.put( "level" , this );
    super.macroMap.put( "units" , this );

  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  { //item|yesno|level|units
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "item" :
        retVal = this.msg.getItem();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
 
      case "yesno" :
        retVal = Boolean.toString(this.msg.isSetNeedsAction());     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "level" :
        BigInteger bVal = this.msg.getLevel();  
        retVal = bVal.toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
       
      case "units" :
        retVal = this.msg.getSupplyUom();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
