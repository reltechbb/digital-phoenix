/*
 * IdentMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;
import com.domenix.common.utils.NumberFormatter;

//~--- classes ----------------------------------------------------------------

/**
 * This class generates the information for all LRUs as a part of a status report.
 * 
 * @author kmiller
 */
public class StatusAllLruMacroProcessor extends MacroProcessor
{
  /** The macro processing text */
  public final String	statusLruReport	=
    "<LruStatus Component=\"{cccomponent}\" Executed=\"{ccextime}\" Result=\"{ccresult}\"/>"
    + "<LruStatus Component=\"{pdilcomponent}\" Executed=\"{pdilextime}\" Result=\"{pdilresult}\"/>"
    + "<LruStatus Component=\"{sccomponent}\" ComponentId=\"{sccompid}\" Executed=\"{scextime}\" Result=\"{scresult}\"/>";

  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   */
  public StatusAllLruMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;
      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes
   *
   */
  private void initialize()
  {
    super.textMacros	=
      "\\{(cccomponent|ccextime|ccresult|pdilcomponent|pdilextime|pdilresult|sccomponent|sccompid|scextime|scresult)\\}";
    super.macroMap.put( "cccomponent" , this );
    super.macroMap.put( "ccextime" , this );
    super.macroMap.put( "ccresult" , this );
    super.macroMap.put( "pdilcomponent" , this );
    super.macroMap.put( "pdilextime" , this );
    super.macroMap.put( "pdilresult" , this );
    super.macroMap.put( "sccomponent" , this );
    super.macroMap.put( "sccompid" , this );
    super.macroMap.put( "scextime" , this );
    super.macroMap.put( "scresult" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {    // cccomponent|ccextime|ccresult|pdilcomponent|pdilextime|pdilresult|sccomponent|scccompid|scextime|scresult
    String	retVal	= "";
    int			pVal		= 0;

    switch ( macro )
    {
      case "cccomponent" :
        retVal	= this.theState.getComponentCC().name();

        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "ccextime" :
        retVal	= NumberFormatter.getDate( this.theState.getComponentCCBitTime() , null );

        break;

      case "ccresult" :
        retVal	= this.theState.getComponentCCBitStatus().name();

        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "pdilcomponent" :
        retVal	= this.theState.getComponentPDIL().name();

        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "pdilextime" :
        retVal	= NumberFormatter.getDate( this.theState.getComponentPDILBitTime() , null );

        break;

      case "pdilresult" :
        retVal	= this.theState.getComponentPDILBitStatus().name();

        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "sccomponent" :
        retVal	= "SC";

        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "sccompid" :
        retVal = this.theState.getComponentSCName().getScName();
        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "scextime" :
        retVal	= NumberFormatter.getDate( this.theState.
                getComponentSCBitTime(theState.getComponentSCName().getScName()) , null );

        break;

      case "scresult" :
        retVal	= this.theState.getComponentSCBitStatus(theState.getComponentSCName().getScName()).toString();

        break;

      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return ( super.getTranslation( inString ) );
  }
}
