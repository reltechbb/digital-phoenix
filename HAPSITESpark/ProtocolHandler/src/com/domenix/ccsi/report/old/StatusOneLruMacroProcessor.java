/*
 * StatusOneLruMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This class generates a single LRU status report element.
 *
 */
package com.domenix.ccsi.report.old;

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.common.spark.data.StartSelfTestComponentEnum;
import com.domenix.utils.MacroProcessor;
import com.domenix.common.utils.NumberFormatter;

/**
 * This class generates a single LRU status report element.
 * 
 * @author kmiller
 */
public class StatusOneLruMacroProcessor extends MacroProcessor
{
  /** The text to use for a single LRU report */
  public String oneStatusReport = null;

  /** The macro processing text */
  public final String	statusCCReport	=
    "<LruStatus Component=\"{cccomponent}\" Executed=\"{ccextime}\" Result=\"{ccresult}\"/>";
  
  public final String statusPDILReport =
    "<LruStatus Component=\"{pdilcomponent}\" Executed=\"{pdilextime}\" Result=\"{pdilresult}\"/>";
  
  public final String statusSCReport =
    "<LruStatus Component=\"{sccomponent}\" ComponentId=\"{sccompid}\" Executed=\"{scextime}\" Result=\"{scresult}\"/>";

  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param comp which component
   * @param cfg the sensor configuration
   * @param state the saved state
   */
  public StatusOneLruMacroProcessor( StartSelfTestComponentEnum comp , CcsiConfigSensor cfg , CcsiSavedState state )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;
      switch( comp )
      {
        case CC :
          this.oneStatusReport = this.statusCCReport;
          break;
          
        case PDIL :
          this.oneStatusReport = this.statusPDILReport;
          break;
          
        case SC :
          this.oneStatusReport = this.statusSCReport;
          break;
          
        case UIC :
        case DPC :
        case WCC :
        default:
          this.oneStatusReport = "<LruStatus Component=\"" + comp.name() + "\" Executed=\"" +
              NumberFormatter.getDate(null, null) + "\" Result=\"PASS\"/>";
          break;
      }
      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes
   *
   */
  private void initialize()
  {
    super.textMacros	=
      "\\{(cccomponent|ccextime|ccresult|pdilcomponent|pdilextime|pdilresult|sccomponent|sccompid|scextime|scresult)\\}";
    super.macroMap.put( "cccomponent" , this );
    super.macroMap.put( "ccextime" , this );
    super.macroMap.put( "ccresult" , this );
    super.macroMap.put( "pdilcomponent" , this );
    super.macroMap.put( "pdilextime" , this );
    super.macroMap.put( "pdilresult" , this );
    super.macroMap.put( "sccomponent" , this );
    super.macroMap.put( "sccompid" , this );
    super.macroMap.put( "scextime" , this );
    super.macroMap.put( "scresult" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {    // cccomponent|ccextime|ccresult|pdilcomponent|pdilextime|pdilresult|sccomponent|scccompid|scextime|scresult
    String	retVal	= "";
    int			pVal		= 0;

    switch ( macro )
    {
      case "cccomponent" :
        retVal	= this.theState.getComponentCC().name();

        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "ccextime" :
        retVal	= NumberFormatter.getDate( this.theState.getComponentCCBitTime() , null );

        break;

      case "ccresult" :
        retVal	= this.theState.getComponentCCBitStatus().name();

        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "pdilcomponent" :
        retVal	= this.theState.getComponentPDIL().name();

        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "pdilextime" :
        retVal	= NumberFormatter.getDate( this.theState.getComponentPDILBitTime() , null );

        break;

      case "pdilresult" :
        retVal	= this.theState.getComponentPDILBitStatus().name();

        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "sccomponent" :
        retVal	= "SC";

        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "sccompid" :
        retVal = this.theState.getComponentSCName().getScName();
        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "scextime" :
        retVal	= NumberFormatter.getDate( this.theState.
                getComponentSCBitTime(theState.getComponentSCName().getScName()) , null );

        break;

      case "scresult" :
        retVal	= this.theState.getComponentSCBitStatus(theState.getComponentSCName().getScName()).toString();

        break;

      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return ( super.getTranslation( inString ) );
  }
}
