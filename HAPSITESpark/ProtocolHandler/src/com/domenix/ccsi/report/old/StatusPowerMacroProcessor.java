/*
 * StatusPowerMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/09/20
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 * This class generates a power status element for a status report.
 * 
 * @author michaelnguyen27
 */
public class StatusPowerMacroProcessor extends MacroProcessor
{    // battCharge|dpcLvl|pwrMode
  /** Field description */
  public final String	statusPowerReport	= "<PowerStatus PowerMode=\"NORM\"/>";

  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   */
  public StatusPowerMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;
      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes
   *
   */
  private void initialize()
  {    // battCharge|dpcLvl|pwrMode
    super.textMacros	= "\\{(pwrMode)\\}";
    super.macroMap.put( "pwrmode" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {    // battCharge|dpcLvl|pwrMode
    String	retVal	= "";
    int			pVal		= 0;

    switch ( macro )
    {
//    case "battcharge" :
//      retVal = this.msg.getStatus().getBattCharge().value();
//      if ( retVal == null )
//      {
//        retVal = "";
//      }
//      break;
//
//    case "dpclvl" :
//      //retVal = this.msg.getStatus();
//      if ( retVal == null )
//      {
//        retVal = "";
//      }
//      break;
      case "pwrmode" :
        retVal	= "NORM";

        break;

      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return ( super.getTranslation( inString ) );
  }
}
