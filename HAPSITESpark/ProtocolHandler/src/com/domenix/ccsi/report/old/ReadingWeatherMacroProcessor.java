/*
 * IdentMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.common.spark.data.ReadingEventMsg;
import com.domenix.utils.MacroProcessor;
import java.math.BigDecimal;

//~--- classes ----------------------------------------------------------------

/**
 * This class produces the weather details for a reading channel report.
 * 
 * @author kmiller
 */
public class ReadingWeatherMacroProcessor extends MacroProcessor
{
  /** The macro definition for weather information details */
  public final String readingBaseReport = "<WxData><WeatherDataTypeSource>{source}</WeatherDataTypeSource>"
  + "<WeatherDataTypeItemList><WeatherItemTypePoint><LocationTypeLatitude>{latitude}</LocationTypeLatitude>"
  + "<LocationTypeLongitude>{longitude}</LocationTypeLongitude></WeatherItemTypePoint>"
  + "<WeatherItemTypeSpeed>{wspeed}</WeatherItemTypeSpeed>"
  + "<WeatherItemTypeDirection>{wdir}</WeatherItemTypeDirection></WeatherDataTypeItemList></WxData>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  /** The reading */
  private ReadingEventMsg msg;
  
  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   * @param msg the reading
   */
  public ReadingWeatherMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state , ReadingEventMsg msg)
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      if (msg != null) {
          this.msg = msg;
      }
      
      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  { //source|latitude|longitude|wspeed|wdir
    super.textMacros	=
      "\\{(source|latitude|longitude|wspeed|wdir)\\}";
    super.macroMap.put( "source" , this );
    super.macroMap.put( "latitude" , this );
    super.macroMap.put( "longitude" , this );
    super.macroMap.put( "wspeed" , this );
    super.macroMap.put( "wdir" , this );

  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  { //source|latitude|longitude|wspeed|wdir
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "source" :
        retVal = this.msg.getItemId();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
 
      case "latitude" :
        retVal = Double.toString(this.msg.getLat());     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "longitude" :  
        retVal = Double.toString(this.msg.getLon());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
       
      case "wspeed" :
        BigDecimal bd1 = this.msg.getWindSpeed();
        retVal = bd1.toString();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "wdir" :
        BigDecimal bd2 = this.msg.getWindDirection();
        retVal = bd2.toString();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
        
      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
