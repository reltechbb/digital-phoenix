/*
 * ConfigLocalMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 *  This class provides for the generation of LOCAL blocks of configuration channel reports.
 * 
 * @author kmiller
 */
public class ConfigLocalMacroProcessor extends MacroProcessor
{
  
  public final String localConfigReport = "<Block Name=\"LOCAL\"><Item Name=\"HostSensorId\""
  + " Value=\"{hostSenId}\"/><Item Name=\"HostSensorName\" Value=\"{hostSenNm}\"/><Item Name=\"HostSensorMount\""
  + " Value=\"{hostSenMt}\"/><Item Name=\"HostSensorDescription\" Value=\"{hostSenDesc}\"/></Block>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   */
  public ConfigLocalMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  {
    super.textMacros	=
      "\\{(hostSenId|hostSenNm|hostSenMt|hostSenDesc)\\}";
    super.macroMap.put( "hostsenid" , this );
    super.macroMap.put( "hostsennm" , this );
    super.macroMap.put( "hostsenmt" , this );
    super.macroMap.put( "hostsendesc" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {
    String	retVal	= "";

    switch ( macro )
    {
      case "hostsenid" :
        retVal = this.theState.getHostNetworkSettings().getHostSensorIDOrUUID();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "hostsennm" :
        retVal = this.theState.getHostSensorName();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "hostsenmt" :
        retVal = this.theState.getHostSensorMounting().toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "hostsendesc" :
        retVal = this.theState.getHostSensorDescription();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
        
      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
