/*
 * IdentMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.common.spark.data.ConsumableEventMsg;
import com.domenix.utils.MacroProcessor;
import java.math.BigInteger;

//~--- classes ----------------------------------------------------------------

/**
 * This class generates MAINT channel reports for periodic consumable events.
 * 
 * @author kmiller
 */
public class MaintPeriodicConsumeMacroProcessor extends MacroProcessor
{
  //item|units|slvl|action
  public final String maintPeriodicConsumeReport = "<Consumable Item=\"{item}\"{units}{slvl} NeedsAction=\"{action}\"/>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;
  
  private ConsumableEventMsg msg;
  
  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   * @param msg the consumables message
   */
  public MaintPeriodicConsumeMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state , ConsumableEventMsg msg)
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      if (msg != null)
      {
          this.msg = msg;
      }
      
      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  { //item|units|slvl|action
    super.textMacros	=
      "\\{(item|units|slvl|action)\\}";
    super.macroMap.put( "item" , this );
    super.macroMap.put( "units" , this );
    super.macroMap.put( "slvl" , this );
    super.macroMap.put( "action" , this );

  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  { //item|units|slvl|action
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "item" :
        retVal = this.msg.getItem();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
 
      case "units" :
        if ( this.msg.isSetSupplyUom() )
        {
          retVal = " SupplyUnits=\"" + this.msg.getSupplyUom() + "\"";
        }
        else
        {
          retVal = " ";
        }
        
        break;
       
      case "slvl" :
        if ( this.msg.isSetLevel() )
        {
          BigInteger bi = this.msg.getLevel();
          retVal = " SupplyLevel=\"" + bi.toString() + "\"";
        }
        else
        {
          retVal = " ";
        }
        break;
 
      case "action" :
        if ( this.msg.isSetNeedsAction() )
        {
          retVal = Boolean.toString(this.msg.isNeedsAction());
        }     
        else
        {
          retVal = "false";
        }
        break;

      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
