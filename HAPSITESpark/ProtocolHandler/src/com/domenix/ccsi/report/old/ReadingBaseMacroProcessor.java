/*
 * ReadingBaseMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.common.spark.data.ReadingEventMsg;
import com.domenix.utils.MacroProcessor;
import com.domenix.common.utils.NumberFormatter;

//~--- JDK imports ------------------------------------------------------------

import java.math.BigDecimal;

import java.util.Date;

//~--- classes ----------------------------------------------------------------

/**
 * This class generates the base information for a reading channel report.
 * 
 * @author kmiller
 */
public class ReadingBaseMacroProcessor extends MacroProcessor
{
  /** Macro definitions for a base reading channel report. */
  public final String readingBaseReport	=
    "<ReadingReport Sensor=\"{sensor}\" Time=\"{time}\""
    + " {readingid}><Data Detect=\"{detect}\" Level=\"{lvl}\" DetailsAvailable=\"{details}\""
    + " {detectid} {units}>{sud}</Data></ReadingReport>";
  
  /** Macro definitions for a reading report as a part of an alert. */
  public final String readingAlertReport =
      "<Reading Sensor=\"{sensor}\" Time=\"{time}\""
    + " {readingid}><Data Detect=\"{detect}\" Level=\"{lvl}\" DetailsAvailable=\"{details}\""
    + " {detectid} {units}>{sud}</Data></Reading>";

  /** The reading */
  private ReadingEventMsg	msg;

  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   * @param msg
   */
  public ReadingBaseMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state , ReadingEventMsg msg )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      if ( msg != null )
      {
        this.msg	= msg;
      }

      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes
   *
   */
  private void initialize()
  {    // sensor|time|readingid|detect|lvl|details|confid|units
    super.textMacros	= "\\{(sensor|time|readingid|detect|lvl|details|detectid|units|sud)\\}";
    super.macroMap.put( "sensor" , this );
    super.macroMap.put( "time" , this );
    super.macroMap.put( "readingid" , this );
    super.macroMap.put( "detect" , this );
    super.macroMap.put( "lvl" , this );
    super.macroMap.put( "details" , this );
    super.macroMap.put( "detectid" , this );
    super.macroMap.put( "units" , this );
    super.macroMap.put( "sud" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {
    String	retVal	= "";
    int			pVal		= 0;

    switch ( macro )
    {
      case "sensor" :
//        retVal	= this.theState.getComponentSCName().getScName();
        retVal	= msg.getComponent();

        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "time" :
        Date	temp;
        if ( this.msg.isSetTime() )
        {
          temp = this.msg.getTime().toGregorianCalendar().getTime();
        }
        else
        {
          temp = null;
        }

        retVal	= NumberFormatter.getDate( temp , null );

        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "readingid" :
        if ( this.msg.isSetId() )
        {
          pVal	= this.msg.getId();

          String	rdgId	= "R" + NumberFormatter.getInt( pVal , 10 , true , 9 , '0' );

          retVal	= "ReadingID=\"" + rdgId + "\" ";
        }
        else
        {
          retVal	= " ";
        }

        break;

      case "detect" :
        if ( this.msg.isSetItem() )
        {
          retVal	= this.msg.getItem();
        }
        else
        {
          retVal	= "NONE";
        }

        break;

      case "lvl" :
        BigDecimal	bd	= this.msg.getLevel();

        retVal	= bd.toString();

        if ( retVal == null )
        {
          retVal	= "";
        }

        break;

      case "details" :
        boolean	dtl	= false;

        if ( ( this.msg.isSetReadingSpectrum() ) || ( this.msg.isSetTemperature() ) )
        {
          dtl	= true;
        }

        retVal	= Boolean.toString( dtl );

        break;

      case "detectid" :
        if ( this.msg.isSetItemId() )
        {
          retVal	= "Id=\"" + this.msg.getItemId() + "\"";
        }
        else
        {
          retVal	= " ";
        }

        break;

      case "units" :
        if ( this.msg.isSetUom() )
        {
          retVal	= "Units=\"" + this.msg.getUom() + "\"";
        }
        else
        {
          retVal	= " ";
        }

        break;

      case "sud":
          SUDProcessor sudProcessor = new SUDProcessor ();
          retVal = sudProcessor.processSUD(msg);
          break;
          
      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return ( super.getTranslation( inString ) );
  }
}
