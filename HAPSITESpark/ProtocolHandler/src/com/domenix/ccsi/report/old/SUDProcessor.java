/*
 * SUDProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Creates the Sensor Unique Data (SUD) portion of reports
 *
 */
package com.domenix.ccsi.report.old;

import com.domenix.common.spark.data.ReadingEventMsg;
import com.domenix.common.spark.data.SensorUniqueData;
import com.domenix.common.spark.data.SensorUniqueDataList;
import java.util.List;

/**
 * Creates the Sensor Unique Data (SUD) portion of report
 *
 * @author jmerritt
 */
public class SUDProcessor {

    public String processSUD(ReadingEventMsg msg) {
        SensorUniqueDataList sudList = msg.getSudList();
        return processSUD(sudList);
    }

//    public String processSUD(ConsumableEventMsg msg) {
//        SensorUniqueDataList sudList = msg.getSudList();
//        return processSUD(sudList);
//    }
    private String processSUD(SensorUniqueDataList list) {
        StringBuilder retValue = new StringBuilder("");
        if ((list != null) && (list.isSetSUD())) {

            List<SensorUniqueData> sudList = list.getSUD();
            if ((sudList != null) && !(sudList.isEmpty())) {
                for (SensorUniqueData sud : sudList) {
                    retValue.append("<SUD Name=\"");
                    retValue.append(sud.getItem());
                    retValue.append("\" Value=\"");
                    retValue.append(sud.getValue()).append("\" ");
                    if (sud.isSetType()) {
                        retValue.append("Type=\"").append(sud.getType()).append("\" ");
                    }
                    if (sud.isSetUnits()) {
                        retValue.append("Units=\"").append(sud.getUnits()).append("\" ");
                    }
                    if (sud.isSetMime()) {
                        retValue.append("mime=\"").append(sud.getMime()).append("\" ");
                    }
                    retValue.append("/>");
                }
            }
        }
        return retValue.toString();
    }
}
