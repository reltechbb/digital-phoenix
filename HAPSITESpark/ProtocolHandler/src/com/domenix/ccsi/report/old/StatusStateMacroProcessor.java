/*
 * StatusStateMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 * This class generates the STATE information for a status report
 *
 * @author michaelnguyen27
 */
public class StatusStateMacroProcessor extends MacroProcessor
{
  /** Field description */
  public final String	statusStateReport	=
    "<StateData{stateDegraded}{stateMaintReq}{statePwrStatus}{stateRdgAffected}{stateSensorInop}/>";

  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   */
  public StatusStateMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;
      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes
   *
   */
  private void initialize()
  {
    super.textMacros	= "\\{(stateDegraded|stateMaintReq|statePwrStatus|stateRdgAffected|stateSensorInop)\\}";
    super.macroMap.put( "statedegraded" , this );
    super.macroMap.put( "statemaintreq" , this );
    super.macroMap.put( "statepwrstatus" , this );
    super.macroMap.put( "statedegraded" , this );
    super.macroMap.put( "statesensorinop" , this );
    super.macroMap.put( "staterdgaffected" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {    // statedegraded|statemaintreq|statepwrstatus|staterdgaffected|statesensorinop
    String	retVal	= "";
    String	temp		= "";
    int			pVal		= 0;

    switch ( macro )
    {
      case "statedegraded" :
        temp		= Boolean.toString( this.theState.isStateDegraded() );
        retVal	= " DegradedMode=\"" + temp + "\"";

        break;

      case "statemaintreq" :
        temp		= Boolean.toString( this.theState.isStateMaintReq() );
        retVal	= " MaintReq=\"" + temp + "\"";

        break;

      case "staterdgaffected" :
        temp		= Boolean.toString( this.theState.isStateReadingAffected() );
        retVal	= " ReadingAffected=\"" + temp + "\"";

        break;

      case "statesensorinop" :
        temp		= Boolean.toString( this.theState.isStateSensorInop() );
        retVal	= " SensorInoperable=\"" + temp + "\"";

        break;

      case "statepwrstatus" :
        temp	= this.theState.getStatePwrStatus().name();

        switch ( temp )
        {
          case "AC" :
            temp	= "NORM";

            break;

          case "BATT" :
            temp	= "NORM";

            break;

          case "NA" :
            temp	= "UNK";

            break;

          default :
            temp	= "NORM";

            break;
        }

        retVal	= " PwrStatus=\"" + temp + "\"";

        break;

      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return ( super.getTranslation( inString ) );
  }
}
