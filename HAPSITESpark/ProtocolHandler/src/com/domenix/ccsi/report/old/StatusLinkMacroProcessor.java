/*
 * StatusLinkMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 * This class produces the LINK block section of a status report
 * 
 * @author kmiller
 */
public class StatusLinkMacroProcessor extends MacroProcessor
{
  /** Macro text for a LINK status channel report block */
  public final String statusLinkReport = "<LinkStatus LinkName=\"{linkname}\" LinkType=\"wired_802_3\""
  + " LinkPresent=\"true\" BwModeHigh=\"{bwmodehigh}\" EmconMode=\"{emconmode}\""
  + " LinkPortNbr=\"{linkportnbr}\" Compression=\"{linkcompression}\"><LinkIpAddress>{linkip}</LinkIpAddress>"
  + "<LinkNetMask>{linknetmask}</LinkNetMask><HostIpAddress>{hostip}</HostIpAddress></LinkStatus>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;
  
  /** The connection ID number */
  private long connId;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   * @param connId the connection ID
   */
  public StatusLinkMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state , long connId )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;
      this.connId     = connId;

      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  { //linkname|linktype|linkpresent|bwmodehigh|emconmode|linkportnbr|linkcompression|linkipv4|linknetmask|ipaddress
    super.textMacros	=
      "\\{(linkname|bwmodehigh|emconmode|linkportnbr|linkcompression|linkip|linknetmask|hostip)\\}";
    super.macroMap.put( "linkname" , this );
//    super.macroMap.put( "linktype" , this );
//    super.macroMap.put( "linkpresent" , this );
    super.macroMap.put( "bwmodehigh" , this );
    super.macroMap.put( "emconmode" , this );
    super.macroMap.put( "linkportnbr" , this );
    super.macroMap.put( "linkcompression" , this );
    super.macroMap.put( "linkip" , this );
    super.macroMap.put( "linknetmask" , this );
    super.macroMap.put( "hostip" , this );

  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {
    String	retVal	= "";
    int     pVal = 0;
    StringBuilder retValBldr = new StringBuilder();

    switch ( macro )
    {
      case "linkname" :
        retVal = this.theState.getStateLinkName().getLinkName();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "linktype" :
        retVal = this.theState.getStateLinkType().toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "linkpresent" :
        retVal = "true";
        
        break;

      case "bwmodehigh" :
        retVal = Boolean.toString(this.theState.isStateLinkBWHigh());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
        
      case "emconmode" :
        retVal = this.theState.getEmcon().name();     
        if ( retVal == null )
        {
          retVal = "NONE";
        }
        break;

      case "linkportnbr" :
        // PH port number
        retVal = Integer.toString(this.theConfig.getHostNetworkSettings().getPort());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "linkcompression" :
        retVal = Boolean.toString(this.theState.isStateLinkCompress());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "hostip" :
        retValBldr.setLength( 0 );
//        String hostIp = this.theState.getTheOpenInterface().getHostIp( this.connId );
        String hostIp = this.theConfig.getHostNetworkSettings().getIP().getHostAddress();
        if ( hostIp == null )
        {
          hostIp = "<IpAddressTypeIpv4>127.0.0.1</IpAddressTypeIpv4>";
        }
        else if ( hostIp.contains( ":" ) )
        {
          retValBldr.append( "<IpAddressTypeIpv6>").append( hostIp ).append( "</IpAddressTypeIpv6>" );
        }
        else
        {
          retValBldr.append( "<IpAddressTypeIpv4>").append(hostIp).append( "</IpAddressTypeIpv4>" );
        }
        retVal = retValBldr.toString();
        
        break;
        
       case "linknetmask" :
        retVal = this.theConfig.getHostNetworkSettings().getNetmask().getHostAddress();
        if ( ( retVal == null ) || ( retVal.isEmpty() ) )
        {
          retVal = "<IpNetMaskTypeIpv4>255.255.255.0</IpNetMaskTypeIpv4>";
        }
        else if ( retVal.contains( ":" ) )
        {
          retValBldr.append( "<IpNetMaskTypeIpv6>").append( retVal ).append( "</IpNetMaskTypeIpv6>" );
        }
        else
        {
          retValBldr.append( "<IpNetMaskTypeIpv4>").append( retVal ).append( "</IpNetMaskTypeIpv4>" );
        }
        retVal = retValBldr.toString();
        
        break;

      case "linkip" :
        retVal = this.theConfig.getHostNetworkSettings().getPHIP().getHostAddress();
        if ( retVal == null )
        {
          retVal = "<IpAddressTypeIpv4>127.0.0.1</IpAddressTypeIpv4>";
        }
        else if ( retVal.contains( ":" ) )
        {
          retValBldr.append( "<IpAddressTypeIpv6>").append( retVal ).append( "</IpAddressTypeIpv6>" );
        }
        else
        {
          retValBldr.append( "<IpAddressTypeIpv4>").append( retVal ).append( "</IpAddressTypeIpv4>" );
        }
        retVal = retValBldr.toString();
        break;
       
      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
