/*
 * ConfigUniqueProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file creates the Block section of  a Sensor Unique Report
 *
 */
package com.domenix.ccsi.report.old;

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.common.config.SensorUniqueConfigItem;
import java.util.List;

/**
 * Creates the Block section of a Sensor Unique Report
 * @author jmerritt
 */
public class ConfigUniqueProcessor
{
    private final CcsiConfigSensor cfg;
    private final CcsiSavedState state;
    
    /**
     * Constructor
     * @param cfg
     * @param state
     */
    public ConfigUniqueProcessor ( CcsiConfigSensor cfg , CcsiSavedState state )
    {
        this.cfg = cfg;
        this.state = state;
    }
    
    /**
     * Creates a UNIQUE block
     * @return
     */
    public String translate ()
    {
        StringBuilder messageString = new StringBuilder();
        List<SensorUniqueConfigItem> uniqueConfig = cfg.getSensorUniqueConfig();
        if (!uniqueConfig.isEmpty()) {
            messageString.append("<Block Name=\"UNIQUE\">");
            for (SensorUniqueConfigItem item : uniqueConfig) {
                messageString.append("<Item Name=\"");
                messageString.append(item.getItemName());
                messageString.append("\" Value=\"");
                messageString.append(item.getValue());
                messageString.append("\"/>");
            }
            messageString.append("</Block>");
        }
            return messageString.toString();
    }
}
