/*
 * ConfigOprMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of the interface that must be implemented by all sensor
 * adapter writers.
 *
 */
package com.domenix.ccsi.report.old;

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

/**
 * Macro for formatting the OPERATORS block
 * This block is unsupported
 * 
 * @author jmerritt
 */
public class ConfigOprMacroProcessor extends MacroProcessor
{

    /**
     * Macro for building the Annunciator Report
     */
    public static final String OPRCONFIGREPORT
            = "<Block Name=\"OPERATORS\" Key=\"{oprName}\">"
            + "</Block>";

    /**
     * The sensor configuration
     */
    private CcsiConfigSensor theConfig;

    /**
     * The saved state
     */
    private CcsiSavedState theState;

    /**
     * Constructs a class instance
     *
     * @param cfg the sensor configuration
     * @param state the saved state
     */
    public ConfigOprMacroProcessor(CcsiConfigSensor cfg, CcsiSavedState state)
    {
        super();

        if ((cfg != null) && (state != null))
        {
            theConfig = cfg;
            theState = state;

            initialize();
        } else
        {
            throw new IllegalArgumentException("Missing or null argument.");
        }
    }

    /**
     * This initializes the macro processor
     *
     */
    private void initialize()
    {
        super.textMacros = "\\{(oprName)\\}";
        super.macroMap.put( "oprname" , this );
    
    }

    /**
     * This method translates a particular macro into its string value or null
     * if there is no translation.
     *
     * @param macro the macro to be translated
     *
     * @return String containing the replacement value or null if no replacement
     */
    @Override
    public String translateMacro(String macro)
    {
        String retVal = "Unsupported";

        return (retVal);
    }
}
