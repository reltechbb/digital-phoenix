/*
 * ConfigLinksMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 * This class provides for the generation of LINK blocks of configuration channel reports.
 * 
 * @author kmiller
 */
public class ConfigLinksMacroProcessor extends MacroProcessor
{
  /** Macro formatter for LINKS blocks */
    public final String linksConfigReport = "<Block Name=\"LINKS\" Key=\"{linkKey}\"><Item"
            + " Name=\"LinkName\" Value=\"{linkName}\"/><Item Name=\"LinkNetType\" Value=\"{linkType}\"/>"
            + "<Item Name=\"LinkBandwidthHigh\" Value=\"{linkBandHi}\"/><Item Name=\"LinkEncryption\""
            + " Value=\"{linkEncryt}\"/><Item Name=\"LinkCompression\" Value=\"{linkCompress}\"/>"
            + "<Item Name=\"LinkCompressionThreshold\" Value=\"{linkCompressTh}\"/>"
            + "<Item Name=\"AccessRole\" Value=\"{accessRole}\"/><Item Name=\"ModifyRole\" Value=\"{modifyRole}\"/>"
            + "<Item Name=\"IpAddress\" Value=\"{ipAddr}\"/><Item Name=\"NetMask\" Value=\"{netMask}\"/>"
            + "<Item Name=\"GatewayAddress\" Value=\"{gwAddr}\"/><Item Name=\"IpAddressFixed\""
            + " Value=\"{ipFixed}\"/><Item Name=\"MaxTransmitSize\" Value=\"{maxTxSize}\"/><Item Name=\"LinkPortNbr\""
            + " Value=\"{linkPort}\"/><Item Name=\"BroadcastIp\" Value=\"{broadcastIp}\"/><Item Name=\"EmconMode\""
            + " Value=\"{emconMode}\"/><Item Name=\"LinkSpeed\" Value=\"{linkSpeed}\"/></Block>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   */
  public ConfigLinksMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  {   //linkKey|linkName|linkType|linkBandHi|linkEncryt|linkCompress|linkCompressTh|accessRole|modifyRole|ipAddr|netMask|gwAddr|ipFixed|maxTxSize|linkPort|broadcastIp|emconMode|linkSpeed
    super.textMacros	=
      "\\{(linkKey|linkName|linkType|linkBandHi|linkEncryt|linkCompress|linkCompressTh|accessRole|modifyRole|ipAddr|netMask|gwAddr|ipFixed|maxTxSize|linkPort|broadcastIp|emconMode|linkSpeed)\\}";
    super.macroMap.put( "linkkey" , this );
    super.macroMap.put( "linkname" , this );
    super.macroMap.put( "linktype" , this );
    super.macroMap.put( "linkbandhi" , this );
    super.macroMap.put( "linkencryt" , this );
    super.macroMap.put( "linkcompress" , this );
    super.macroMap.put( "linkcompressth" , this );
    super.macroMap.put( "accessrole" , this );
    super.macroMap.put( "modifyrole" , this );
    super.macroMap.put( "ipaddr" , this );
    super.macroMap.put( "netmask" , this );
    super.macroMap.put( "gwaddr" , this );
    super.macroMap.put( "ipfixed" , this );
    super.macroMap.put( "maxtxsize" , this );
    super.macroMap.put( "linkport" , this );
    super.macroMap.put( "broadcastip" , this );
    super.macroMap.put( "emconmode" , this );
    super.macroMap.put( "linkspeed" , this );
 }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  { //linkKey|linkName|linkType|linkBandHi|linkEncryt|linkCompress|linkCompressTh|accessRole|modifyRole|ipAddr|netMask|gwAddr|ipFixed|maxTxSize|linkPort|broadcastIp|emconMode|linkSpeed
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "linkkey" :
        retVal = this.theState.getStateLinkName().getLinkName();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "linkname" :
        retVal = this.theState.getStateLinkName().getLinkName();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "linktype" :
        retVal = this.theState.getStateLinkType().toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "linkbandhi" :
        retVal = Boolean.toString(this.theState.isStateLinkBWHigh());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
        
      case "linkencryt" :
        retVal = Boolean.toString(this.theState.isStateLinkEncrypt()).toLowerCase();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "linkcompress" :
        retVal = Boolean.toString(this.theState.isStateLinkCompress()).toLowerCase();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "linkcompressth" :
        retVal = Integer.toString(this.theState.getStateLinkCompressThreshold());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "accessrole" :
        retVal = "ANY";
        
        break;
        
      case "modifyrole" :
        retVal = "ANY";    
        
        break;

      case "ipaddr" :
        retVal = this.theConfig.getHostNetworkSettings().getPHIP().getHostAddress();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "netmask" :
        retVal = this.theConfig.getHostNetworkSettings().getNetmask().getHostAddress();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "gwaddr" :
        retVal = this.theConfig.getHostNetworkSettings().getGatewayAddr().getHostAddress();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
        
      case "ipfixed" :
        retVal = "true";
        
        break;

      case "maxtxsize" :
        retVal = Integer.toString(this.theConfig.getHostNetworkSettings().getMaxTransmit());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "linkport" :
        retVal = Integer.toString(this.theConfig.getHostNetworkSettings().getPHPort());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "broadcastip" :
        retVal = this.theConfig.getHostNetworkSettings().getBroadcastAddr().getHostAddress();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "emconmode" :
        retVal = this.theState.getEmcon().toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "linkspeed" :
        retVal = Integer.toString( this.theConfig.getIpLinkSpeed() );

        break;

      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
