/*
 * MaintOneLruBitReport.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.common.spark.data.StartSelfTestComponentEnum;
import com.domenix.utils.MacroProcessor;
import com.domenix.common.utils.NumberFormatter;

//~--- classes ----------------------------------------------------------------

/**
 * This class generates MAINT channel reports for a single LRU
 *
 * @author kmiller
 */
public class MaintOneLruBitReport extends MacroProcessor
{
  /** The macro processing text */
  public final static String MAINT_BIT_REPORT = "<Msg><MaintChn Type=\"bit\"><BIT Component=\"{comp}\""
                                        + " Executed=\"{extime}\"{compid} Result=\"{result}\"/></MaintChn></Msg>";

  /** The component */
  private StartSelfTestComponentEnum	theComp;

  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  private String keyVal = null;
  
  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param comp which component
   * @param cfg the sensor configuration
   * @param state the saved state
   */
  public MaintOneLruBitReport( StartSelfTestComponentEnum comp , CcsiConfigSensor cfg , CcsiSavedState state )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;
      this.theComp		= comp;
      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes
   *
   */
  private void initialize()
  {
    super.textMacros	= "\\{(comp|extime|compid|result)\\}";
    super.macroMap.put( "comp" , this );
    super.macroMap.put( "extime" , this );
    super.macroMap.put( "compid" , this );
    super.macroMap.put( "result" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {    // cccomponent|ccextime|ccresult|pdilcomponent|pdilextime|pdilresult|sccomponent|scccompid|scextime|scresult
    String	retVal	= "";
    int			pVal		= 0;

    switch ( macro )
    {
      case "comp" :
        switch ( this.theComp )
        {
          case CC :
            retVal	= this.theState.getComponentCC().name();

            break;

          case PDIL :
            retVal	= this.theState.getComponentPDIL().name();

            break;

          case SC :
            retVal	= this.theState.getComponentSCEnum(keyVal).toString();

            break;
            
          default:
              break;
        }

        break;

      case "extime" :
        switch ( this.theComp )
        {
          case CC :
            retVal	= NumberFormatter.getDate( this.theState.getComponentCCBitTime() , null );

            break;

          case PDIL :
            retVal	= NumberFormatter.getDate( this.theState.getComponentPDILBitTime() , null );

            break;

          case SC :
            retVal	= NumberFormatter.getDate( this.theState.getComponentSCBitTime(keyVal) , null );

            break;
        }

        break;

      case "compid" :
        switch ( this.theComp )
        {
          case CC :
            retVal	= "";

            break;

          case PDIL :
            retVal	= "";

            break;

          case SC :
            String	temp	= "ComponentId=\"" + this.theState.getComponentSCName().getScName() + "\"";

            retVal	= temp;

            break;
            
          default:
              break;
        }

        break;

      case "result" :
        switch ( this.theComp )
        {
          case CC :
            retVal	= this.theState.getComponentCCBitStatus().name();

            break;

          case PDIL :
            retVal	= this.theState.getComponentPDILBitStatus().name();

            break;

          case SC :
            retVal	= this.theState.getComponentSCBitStatus(keyVal).toString();

            break;
        }

        break;

      default :
        break;
    }

    return ( retVal );
  }

  public void setKeyVal (String value)
  {
      keyVal = value;
  }
  
  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return ( super.getTranslation( inString ) );
  }
}
