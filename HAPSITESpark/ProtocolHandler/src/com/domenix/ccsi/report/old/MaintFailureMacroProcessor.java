/*
 * MaintFailureMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/11/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.common.spark.data.BITEventMsg;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 * This class generates MAINT channel events for failures.
 * 
 * @author kmiller
 */
public class MaintFailureMacroProcessor extends MacroProcessor
{
  //component|exectime|errortext
  public final String maintFailureReport = "<BIT Component=\"{component}\""
  + " Executed=\"{exectime}\"><ErrorDescription>{errortext}</ErrorDescription></BIT>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  private BITEventMsg msg;
  
  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   * @param msg the message
   */
  public MaintFailureMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state , BITEventMsg msg)
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      if (msg != null) {
          this.msg = msg;
      }
      
      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  { //component|exectime|errortext
    super.textMacros	=
      "\\{(component|exectime|errortext)\\}";
    super.macroMap.put( "component" , this );
    super.macroMap.put( "exectime" , this );
    super.macroMap.put( "errortext" , this );

  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  { //component|exectime|errortext
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "component" :
        retVal = this.msg.getComponentId();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
 
      case "exectime" :
        retVal = this.msg.getBitTime().toXMLFormat();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "errortext" :
        retVal = this.msg.getError();     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
       
      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
