/*
 * ReportGenerator.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/8/10
 */
package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.ccsi.CcsiConfigBlockEnum;
import com.domenix.commonha.ccsi.CcsiModeEnum;
import com.domenix.ccsi.protocol.PHContext;
import com.domenix.ccsi.protocol.PendingAlertEntry;
import com.domenix.ccsi.protocol.PendingAlertList;
import com.domenix.ccsi.protocol.PendingBitList;
import com.domenix.ccsi.protocol.PendingConsumablesList;
import com.domenix.ccsi.protocol.PendingFailureList;
import com.domenix.ccsi.protocol.PendingNakDetailEntry;
import com.domenix.ccsi.protocol.PendingNakDetailList;
import com.domenix.ccsi.protocol.PendingReadingList;
import com.domenix.ccsi.protocol.PendingStatusEntry;
import com.domenix.ccsi.protocol.PendingStatusList;
import com.domenix.commonha.intfc.NSDSInterface;
import com.domenix.ccsi.protocol.ActiveAlertListEntry;
import com.domenix.common.spark.IPCWrapper;
import com.domenix.common.spark.data.BITEventMsg;
import com.domenix.common.spark.data.CcsiComponentTypeEnum;
import com.domenix.common.spark.data.ConsumableEventMsg;
import com.domenix.common.spark.data.GetCfgRspArg;
import com.domenix.common.spark.data.GetCfgRspArgs;
import com.domenix.common.spark.data.MsgContent;
import com.domenix.common.spark.data.ReadingEventMsg;
import com.domenix.common.spark.data.StartSelfTestComponentEnum;
import com.domenix.common.utils.NumberFormatter;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------
import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

//~--- classes ----------------------------------------------------------------
/**
 * This class provides CCSI report generation services for the other CCSI
 * components.
 *
 * @author kmiller
 */
public class ReportGenerator
{

    /**
     * The end of channel tag
     */
    private final String endChannel = "</";

    /**
     * End of message tag
     */
    private final String msgEnd = "</Msg>";

    /**
     * Message start tag
     */
    private final String msgStart = "<Msg><";

    /**
     * Error/Debug logger
     */
    private final Logger myLogger;

    /**
     * The Protocol Handler context
     */
    private final PHContext theContext;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs a class instance
     *
     * @param ctx the PH Context
     */
    public ReportGenerator(PHContext ctx)
    {
        this.myLogger = Logger.getLogger(ReportGenerator.class);
        this.theContext = ctx;
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Generates an identification channel report.
     *
     * @return String containing the generated report
     */
    public String genIdentReport()
    {
        IdentMacroProcessor imp = new IdentMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        StringBuilder msg
                = new StringBuilder(this.msgStart).append(CcsiChannelEnum.IDENT.getChanTag()).append('>');
        String identReport = imp.getTranslation(imp.fullIdentReport);

        msg.append(identReport).append(this.msgEnd);

        return (msg.toString());
    }

    /**
     * This method generates a time date channel report.
     *
     * @return String containing the message
     */
    public String genTDReport()
    {
        StringBuilder msg = new StringBuilder(this.msgStart).append(CcsiChannelEnum.TMDT.getChanTag());
        String now = NumberFormatter.getDate(null, null);

        msg.append(" Time=\"").append(now).append("\" Source=\"");
        msg.append(this.theContext.getTheSavedState().getTimesource().name().toLowerCase()).append("\"/>");
        msg.append(this.msgEnd);

        return (msg.toString());
    }

    /**
     * This method generates a location channel report
     *
     * @return String containing the report
     */
    public String genLocReport()
    {
        StringBuilder msg = new StringBuilder(this.msgStart).append(CcsiChannelEnum.LOC.getChanTag()).append('>');
        String lat, lon, alt;

        msg.append("<Location ");
        lat = NumberFormatter.getDouble(theContext.getTheSavedState().getSensorLat(), false, 1, 2, 1, 9);
        msg.append("Lat=\"").append(lat).append("\" ");
        lon = NumberFormatter.getDouble(theContext.getTheSavedState().getSensorLon(), false, 1, 3, 1, 9);
        msg.append("Lon=\"").append(lon).append("\" ");
        alt = NumberFormatter.getDouble(theContext.getTheSavedState().getSensorAlt(), false, 1, 2, 1, 9);
        msg.append("Alt=\"").append(alt).append("\"/>");
        msg.append("<Source>").append(theContext.getTheSavedState().getLocationsource()).append("</Source>");
        msg.append("</LocationChn>");
        msg.append(this.msgEnd);

        return (msg.toString());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Generate a partial configuration report.
     *
     * @param theBlock the block to be generated
     * @param key
     * @return the block generated
     */
    public String getPartialConfigReport(CcsiConfigBlockEnum theBlock, String key)
    {
        StringBuilder msg = new StringBuilder();
        String temp = null;

        switch (theBlock)
        {
            case BASE:
                ConfigBaseMacroProcessor bb = new ConfigBaseMacroProcessor(
                    this.theContext.getTheConfig(),
                    this.theContext.getTheSavedState());
                temp = bb.getTranslation(bb.baseConfigReport);
                break;

            case LOCAL:
                ConfigLocalMacroProcessor lb = new ConfigLocalMacroProcessor(
                    this.theContext.getTheConfig(),
                    this.theContext.getTheSavedState());
                temp = lb.getTranslation(lb.localConfigReport);
                break;

            case COMM:
                ConfigCommMacroProcessor cb = new ConfigCommMacroProcessor(
                    this.theContext.getTheConfig(),
                    this.theContext.getTheSavedState());
                temp = cb.getTranslation(cb.commConfigReport);
                break;

            case GENERAL:
                ConfigGeneralMacroProcessor genb = new ConfigGeneralMacroProcessor(
                    this.theContext.getTheConfig(),
                    this.theContext.getTheSavedState());
                temp = genb.getTranslation(genb.generalConfigReport);
                break;

            case SENSDESC:
                ConfigCcSensdecMacroProcessor ccb = new ConfigCcSensdecMacroProcessor(
                    this.theContext.getTheConfig(),
                    this.theContext.getTheSavedState());
                ConfigPdilSensdecMacroProcessor cpb = new ConfigPdilSensdecMacroProcessor(
                    this.theContext.getTheConfig(),
                    this.theContext.getTheSavedState());
                ConfigScSensdecMacroProcessor csb = new ConfigScSensdecMacroProcessor(
                    this.theContext.getTheConfig(),
                    this.theContext.getTheSavedState());
                switch (key)
                {
                    case "CC":
                        temp = ccb.getTranslation(ccb.CcSensdecConfigReport);
                        break;

                    case "PDIL":
                        temp = cpb.getTranslation(cpb.PdilSensdecConfigReport);
                        break;

                    case "SC":
                        List<String> componentSCList = theContext.getTheSavedState().getComponentSCList ();
                        StringBuilder scString = new StringBuilder ();
                        for (String x : componentSCList)
                        {
                            csb.setKeyVal(x);
                            scString.append(csb.getTranslation(csb.ScSensdecConfigReport));
                        }
                        temp = scString.toString();
                        break;

                    case "ALL":
                        temp = ccb.getTranslation(ccb.CcSensdecConfigReport);
                        msg.append(temp);
                        temp = cpb.getTranslation(cpb.PdilSensdecConfigReport);
                        msg.append(temp);
                        componentSCList = theContext.getTheSavedState().getComponentSCList ();
                        
                        for (String x : componentSCList)
                        {
                            csb.setKeyVal(x);
                            msg.append(csb.getTranslation(csb.ScSensdecConfigReport));
                        }

                        temp = msg.toString();
                        break;

                    default:
                        break;
                }
                break;

            case LINKS:
                ConfigLinksMacroProcessor linkb = new ConfigLinksMacroProcessor(
                    this.theContext.getTheConfig(),
                    this.theContext.getTheSavedState());
                temp = linkb.getTranslation(linkb.linksConfigReport);
                break;
                
            case ANNUN:
                ConfigAnnunMacroProcessor annun = new ConfigAnnunMacroProcessor(
                        theContext.getTheConfig(),
                    theContext.getTheSavedState());
                temp = annun.getTranslation(ConfigAnnunMacroProcessor.ANNUNCONFIGREPORT);
                break;
                
            case SECURITY:
                ConfigSecMacroProcessor sec = new ConfigSecMacroProcessor 
                    (theContext.getTheConfig(),
                    theContext.getTheSavedState());
                temp = sec.getTranslation(ConfigSecMacroProcessor.SECCONFIGREPORT);
                break;
                
            case OPERATORS:
                // OPERATORS is not supported
                //ConfigOprMacroProcessor opr = new ConfigOprMacroProcessor (
                    // theContext.getTheConfig(),
                    // theContext.getTheSavedState());
                //temp = opr.getTranslation(ConfigOprMacroProcessor.OPRCONFIGREPORT);
                break;
             
            case UNIQUE:
                ConfigUniqueProcessor unique = new ConfigUniqueProcessor 
                    (theContext.getTheConfig(),
                    theContext.getTheSavedState());
                temp = unique.translate();
                break;
                
            default:
                break;
        }

        return (temp);
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Generate a full configuration report.
     *
     * @return the report
     */
    public String genFullConfigReport()
    {
        ConfigBaseMacroProcessor bb = new ConfigBaseMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        ConfigLocalMacroProcessor lb = new ConfigLocalMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        ConfigCommMacroProcessor cb = new ConfigCommMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        ConfigGeneralMacroProcessor genb = new ConfigGeneralMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        ConfigCcSensdecMacroProcessor ccb = new ConfigCcSensdecMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        ConfigPdilSensdecMacroProcessor cpb = new ConfigPdilSensdecMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        ConfigScSensdecMacroProcessor csb = new ConfigScSensdecMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        ConfigLinksMacroProcessor linkb = new ConfigLinksMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        ConfigAnnunMacroProcessor annun = new ConfigAnnunMacroProcessor (theContext.getTheConfig(),
                theContext.getTheSavedState());
        ConfigSecMacroProcessor sec = new ConfigSecMacroProcessor (theContext.getTheConfig(),
                theContext.getTheSavedState());
        ConfigUniqueProcessor unique = new ConfigUniqueProcessor 
            (theContext.getTheConfig(),
            theContext.getTheSavedState());
        //ConfigOprMacroProcessor opr = new ConfigOprMacroProcessor (theContext.getTheConfig(),
        //        theContext.getTheSavedState());
        
        StringBuilder msg
                = new StringBuilder(this.msgStart).append(CcsiChannelEnum.CONFIG.getChanTag()).append('>');
        String temp;

        temp = bb.getTranslation(bb.baseConfigReport);
        msg.append(temp);
        temp = lb.getTranslation(lb.localConfigReport);
        msg.append(temp);
        temp = cb.getTranslation(cb.commConfigReport);
        msg.append(temp);
        temp = genb.getTranslation(genb.generalConfigReport);
        msg.append(temp);
        temp = ccb.getTranslation(ccb.CcSensdecConfigReport);
        msg.append(temp);
        temp = cpb.getTranslation(cpb.PdilSensdecConfigReport);
        msg.append(temp);
        List <String> componentSCList = theContext.getTheSavedState().getComponentSCList();
        for (String x : componentSCList)
        {
            csb.setKeyVal(x);
            temp = csb.getTranslation(csb.ScSensdecConfigReport);
            msg.append(temp);
        }
        temp = linkb.getTranslation(linkb.linksConfigReport);
        msg.append(temp);
        temp = annun.getTranslation(ConfigAnnunMacroProcessor.ANNUNCONFIGREPORT);
        msg.append(temp);
        temp = sec.getTranslation(ConfigSecMacroProcessor.SECCONFIGREPORT);
        msg.append(temp);
        temp = unique.translate();
        msg.append(temp);
        //temp = opr.getTranslation(ConfigOprMacroProcessor.OPRCONFIGREPORT);
        //msg.append(temp);
        msg.append(endChannel).append(CcsiChannelEnum.CONFIG.getChanTag()).append('>').append(msgEnd);

        return (msg.toString());
    }

    /**
     * Generate a single reading detail report
     *
     * @param rdg the reading message
     *
     * @return String containing the report
     */
    public String genSingleReadingReport(ReadingEventMsg rdg)
    {
        ReadingBaseMacroProcessor rdgGen = new ReadingBaseMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState(), rdg);
        String msg = rdgGen.getTranslation(rdgGen.readingBaseReport);

        return (msg);
    }

    /**
     * Generate a single BIT alert report
     *
     * @param bit the BIT message
     *
     * @return String containing the report
     */
    public String genSingleBitAlertReport(BITEventMsg bit)
    {
        AlertBitMacroProcessor altBGen = new AlertBitMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState(), bit);
        String msg = altBGen.getTranslation(altBGen.BIT_ALERT_REPORT);

        return (msg);
    }

    /**
     * Generate a single reading alert report
     *
     * @param rdg the reading message
     *
     * @return String containing the report
     */
//    public String genSingleReadingAlertReport(ReadingEventMsg rdg)
    public String genSingleReadingAlertReport(ActiveAlertListEntry rdg)
    {
        AlertReadingMacroProcessor altRGen = new AlertReadingMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState(), rdg);
        String msg = altRGen.getTranslation(altRGen.readingAlertReport);

        return (msg);
    }

    /**
     * Generates a full alert report
     *
     * @param alerts the list of pending alerts
     *
     * @return the full report message(s)
     */
    public String genFullAlertReport(PendingAlertList alerts)
    {
        StringBuilder msg = new StringBuilder();

        if ((alerts != null) && (!alerts.isEmpty()))
        {
            for (Object x : alerts.getAllEntries())
            {
                PendingAlertEntry altEntry = (PendingAlertEntry) x;

                if (altEntry.isReading())
                {
                    AlertReadingMacroProcessor altRGen = new AlertReadingMacroProcessor(this.theContext.getTheConfig(),
                            this.theContext.getTheSavedState(), altEntry);
                    String tempR = altRGen.getTranslation(altRGen.readingAlertReport);

                    msg.append(tempR);
                    myLogger.info ("Generating alert Reading report");
                } else
                {
                    AlertBitMacroProcessor altBGen = new AlertBitMacroProcessor(this.theContext.getTheConfig(),
                            this.theContext.getTheSavedState(), altEntry);
                    String tempR = altBGen.getTranslation(altBGen.BIT_ALERT_REPORT);

                    msg.append(tempR);
                    myLogger.info ("Generating alert BIT report");
                }
            }
        } else
        {
            msg.append(msgStart).append(CcsiChannelEnum.ALERTS.getChanTag());
            msg.append(" Event=\"NONE\" Source=\"none\" Time=\"").append(NumberFormatter.getDate(null,
                    null)).append("\"  AlertId=\"NN000000000\"/>");
            msg.append(msgEnd);
            
            myLogger.info ("Generating Event=NONE report");
        }

        return (msg.toString());
    }

    /**
     * Generate a single LRU
     *
     * @param which the component to generate
     *
     * @return the message
     */
    public String genOneLRUStatusReport(StartSelfTestComponentEnum which)
    {
        StatusOneLruMacroProcessor mproc = new StatusOneLruMacroProcessor(which, this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        String retVal;

        retVal = mproc.translateMacro(mproc.oneStatusReport);

        return (retVal);
    }

    /**
     * Generate a single LRU
     *
     * @param which the component to generate
     *
     * @return the message
     */
    public String genOneLRUBitReport(StartSelfTestComponentEnum which)
    {
        MaintOneLruBitReport oneProc = new MaintOneLruBitReport(which, this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        String retVal;

        if (which == StartSelfTestComponentEnum.SC)
        {
            List <String> componentSCList = this.theContext.getTheSavedState().getComponentSCList();
            StringBuilder scString = new StringBuilder ();
            for (String x : componentSCList)
            {
                oneProc.setKeyVal(x);
                scString.append (this.theContext.getTheSavedState());
            }
            retVal = scString.toString();
        }else
        {
            retVal = oneProc.getTranslation(oneProc.MAINT_BIT_REPORT);
        }
        return (retVal);
    }

    /**
     * Produce an event or timer related maintenance report.
     *
     * @param which the block to be generated
     * @param connId the current connection
     * @param naks pending NAK details
     * @param psl pending status event list
     *
     * @return the message
     */
    public String genEventStatusReport(String which, long connId, PendingNakDetailList naks, PendingStatusList psl)
    {
        StringBuilder retVal = new StringBuilder();
        boolean hasAlert = !this.theContext.getActiveAlerts().isEmpty();
        boolean hasBIT = this.theContext.getTheSavedState().isStateReadingAffected()
                || this.theContext.getTheSavedState().isStateSensorInop();
        boolean hasMaint = this.theContext.getTheSavedState().isStateMaintReq();
        String baseStatus = msgStart + CcsiChannelEnum.STATUS.getChanTag() + " Alert=\"" + Boolean.toString(hasAlert)
                + "\" BIT=\"" + Boolean.toString(hasBIT) + "\" Maint=\"" + Boolean.toString(hasMaint)
                + "\">";

        retVal.append(baseStatus);
        if ((which != null) && (!which.trim().isEmpty()))
        {
            switch (which.trim())
            {
                case "LRU":
                    StatusAllLruMacroProcessor genLru = new StatusAllLruMacroProcessor(
                            this.theContext.getTheConfig(),
                            this.theContext.getTheSavedState());
                    retVal.append(genLru.getTranslation(genLru.statusLruReport));
                    break;

                case "State":
                    StatusStateMacroProcessor genState = new StatusStateMacroProcessor(
                            this.theContext.getTheConfig(),
                            this.theContext.getTheSavedState());
                    retVal.append(genState.getTranslation(genState.statusStateReport));
                    break;

                case "Link":
                    StatusLinkMacroProcessor genLink = new StatusLinkMacroProcessor(
                            this.theContext.getTheConfig(),
                            this.theContext.getTheSavedState(), connId);
                    retVal.append(genLink.getTranslation(genLink.statusLinkReport));
                    break;

                case "Connect":
                    StatusConnectMacroProcessor genConn = new StatusConnectMacroProcessor(
                            this.theContext.getTheConfig(),
                            this.theContext.getTheSavedState(), connId);
                    retVal.append(genConn.getTranslation(genConn.statusConnectReport));
                    break;

                case "Power":
                    StatusPowerMacroProcessor genPwr = new StatusPowerMacroProcessor(
                            this.theContext.getTheConfig(),
                            this.theContext.getTheSavedState());
                    retVal.append(genPwr.getTranslation(genPwr.statusPowerReport));
                    break;

                case "NAK":
                    StatusNAKDetailsMacroProcessor genNak = new StatusNAKDetailsMacroProcessor(
                            this.theContext.getTheConfig(),
                            this.theContext.getTheSavedState(), naks);

                    if ((naks != null) && (!naks.isEmpty()))
                    {
                        retVal.append(genNak.getTranslation(genNak.nakDetailsStatusReport));
                    }

                    break;

                default:
                    retVal.append(this.genFullStatusReport(naks, connId, null, null));
                    break;
            }
        } else if ((psl != null) && (!psl.isEmpty()))
        {
            for (PendingStatusEntry x : psl.getAllEntries())
            {
                switch (x.getStatusBlock())
                {
                    case "LRU":
                        StatusAllLruMacroProcessor genLru = new StatusAllLruMacroProcessor(
                                this.theContext.getTheConfig(),
                                this.theContext.getTheSavedState());
                        retVal.append(genLru.getTranslation(genLru.statusLruReport));
                        break;

                    case "State":
                        StatusStateMacroProcessor genState = new StatusStateMacroProcessor(
                                this.theContext.getTheConfig(),
                                this.theContext.getTheSavedState());
                        retVal.append(genState.getTranslation(genState.statusStateReport));
                        break;

                    case "Link":
                        StatusLinkMacroProcessor genLink = new StatusLinkMacroProcessor(
                                this.theContext.getTheConfig(),
                                this.theContext.getTheSavedState(), connId);
                        retVal.append(genLink.getTranslation(genLink.statusLinkReport));
                        break;

                    case "Connect":
                        StatusConnectMacroProcessor genConn = new StatusConnectMacroProcessor(
                            this.theContext.getTheConfig(),
                            this.theContext.getTheSavedState(), connId);
                        retVal.append(genConn.getTranslation(genConn.statusConnectReport));
                        break;

                    case "Power":
                        StatusPowerMacroProcessor genPwr = new StatusPowerMacroProcessor(
                                this.theContext.getTheConfig(),
                                this.theContext.getTheSavedState());
                        retVal.append(genPwr.getTranslation(genPwr.statusPowerReport));
                        break;

                    case "NAK":
                        StatusNAKDetailsMacroProcessor genNak = new StatusNAKDetailsMacroProcessor(
                                this.theContext.getTheConfig(),
                                this.theContext.getTheSavedState(), naks);

                        if ((naks != null) && (!naks.isEmpty()))
                        {
                            retVal.append(genNak.getTranslation(genNak.nakDetailsStatusReport));
                        }

                        break;

                    default:
                        retVal.append(this.genFullStatusReport(naks, connId, null, null));
                        break;
                }
            }
        }

        retVal.append("</StatusChn>").append(msgEnd);
        return (retVal.toString());
    }

    /**
     * Produces a full status channel report
     *
     * @param naks list of pending NAKs
     * @param connId the current connection
     * @param items list of items to be generated
     * @param psl pending status events
     *
     * @return the message
     */
    public String genFullStatusReport(PendingNakDetailList naks, long connId, ArrayList<String> items,
            PendingStatusList psl)
    {
        // We're going to report everything, so clear the PSL
        psl.clear();

        StatusAllLruMacroProcessor genLru = new StatusAllLruMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        StatusStateMacroProcessor genState = new StatusStateMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        StatusLinkMacroProcessor genLink = new StatusLinkMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState(), connId);
        StatusConnectMacroProcessor genConn = new StatusConnectMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState(), connId);
        StatusPowerMacroProcessor genPwr = new StatusPowerMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        StatusNAKDetailsMacroProcessor genNak = new StatusNAKDetailsMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState(), naks);
        boolean hasAlert = this.theContext.getActiveAlerts().isEmpty();
        boolean hasBIT = this.theContext.getTheSavedState().isStateReadingAffected()
                || this.theContext.getTheSavedState().isStateSensorInop();
        boolean hasMaint = this.theContext.getTheSavedState().isStateMaintReq();
        String baseStatus = " Alert=\"" + Boolean.toString(hasAlert) + "\" BIT=\"" + Boolean.toString(hasBIT)
                + "\" Maint=\"" + Boolean.toString(hasMaint) + "\">";
        StringBuilder retVal = new StringBuilder(msgStart);

        retVal.append(CcsiChannelEnum.STATUS.getChanTag());
        retVal.append(baseStatus);

        if ((items != null) && (!items.isEmpty()))
        {
            for (String it : items)
            {
                switch (it)
                {
                    case "LRU":
                        retVal.append(genLru.getTranslation(genLru.statusLruReport));

                        break;

                    case "State":
                        retVal.append(genState.getTranslation(genState.statusStateReport));

                        break;

                    default:
                        break;
                }
            }

            if ((naks != null) && (!naks.isEmpty()))
            {
                retVal.append(genNak.getTranslation(genNak.nakDetailsStatusReport));
            }
        } else
        {
            retVal.append(genLru.getTranslation(genLru.statusLruReport));
            retVal.append(genState.getTranslation(genState.statusStateReport));
            retVal.append(genLink.getTranslation(genLink.statusLinkReport));
            retVal.append(genConn.getTranslation(genConn.statusConnectReport));
            retVal.append(genPwr.getTranslation(genPwr.statusPowerReport));

            if ((naks != null) && (!naks.isEmpty()))
            {
                retVal.append(genNak.getTranslation(genNak.nakDetailsStatusReport));
            }
        }

        retVal.append("</StatusChn>").append(msgEnd);

        return (retVal.toString());
    }

    /**
     * Generates a full maintenance report
     *
     * @param bit pending BIT data
     * @param consumables pending consumables data
     * @param fail pending failure list
     *
     * @return the generated message
     */
    public String genFullMaintReport(PendingBitList bit, PendingConsumablesList consumables, PendingFailureList fail)
    {
        boolean hadData = false;
        String temp;
        StringBuilder msg = new StringBuilder(msgStart).append(CcsiChannelEnum.MAINT.getChanTag()).append(
                " Type=\"periodic\"").append('>').append("<Periodic Time=\"").append(
                NumberFormatter.getDate(null, null)).append("\">");

        if ((bit != null) && (!bit.isEmpty()))
        {
            hadData = true;

            Object[] bitList = bit.getAllEntries();

            for (Object x : bitList)
            {
                BITEventMsg bitMsg = (BITEventMsg) x;

                temp = this.makeBitStatus(bitMsg, bitMsg.getComponentType());
                msg.append(temp);
            }
        }

        if ((consumables != null) && (!consumables.isEmpty()))
        {
            hadData = true;

            Object[] consList = consumables.getAllEntries();

            for (Object x : consList)
            {
                ConsumableEventMsg conMsg = (ConsumableEventMsg) x;

                temp = this.makeConsumableStatus(conMsg);
                msg.append(temp);
            }
        }

        if ((fail != null) && (!fail.isEmpty()))
        {
            hadData = true;

            Object[] failList = fail.getAllEntries();

            for (Object x : failList)
            {
                BITEventMsg bitMsg = (BITEventMsg) x;

                temp = this.makeBitStatus(bitMsg, bitMsg.getComponentType());
                msg.append(temp);
            }
        }

        if (!hadData)
        {
            StringBuilder work = new StringBuilder();

            temp = makeBitStatus(null, CcsiComponentTypeEnum.CC);
            work.append(temp);
            temp = makeBitStatus(null, CcsiComponentTypeEnum.PDIL);
            work.append(temp);
            temp = makeBitStatus(null, CcsiComponentTypeEnum.SC);
            work.append(temp);
            msg.append(work.toString());
        }

        msg.append("</Periodic></").append(CcsiChannelEnum.MAINT.getChanTag()).append(">").append(msgEnd);

        return (msg.toString());
    }

    /**
     * Method description
     *
     *
     * @param event
     *
     * @return
     */
    private String makeFailureStatus(BITEventMsg event)
    {
        MaintPeriodicFailureMacroProcessor genFail
                = new MaintPeriodicFailureMacroProcessor(this.theContext.getTheConfig(), this.theContext.getTheSavedState(),
                        event);
        String failRept = genFail.getTranslation(genFail.maintPeriodicFailureReport);

        return (failRept);
    }

    /**
     * Method description
     *
     *
     * @param event
     *
     * @return
     */
    private String makeConsumableStatus(ConsumableEventMsg event)
    {
        MaintPeriodicConsumeMacroProcessor genCon
                = new MaintPeriodicConsumeMacroProcessor(this.theContext.getTheConfig(), this.theContext.getTheSavedState(),
                        event);
        String conRept = genCon.getTranslation(genCon.maintPeriodicConsumeReport);

        return (conRept);
    }

    /**
     * Builds a BIT Status message
     *
     * @param event
     * @param comp
     *
     * @return
     */
    private String makeBitStatus(BITEventMsg event, CcsiComponentTypeEnum comp)
    {
        MaintPeriodicBitMacroProcessor genBit = new MaintPeriodicBitMacroProcessor(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState(), comp, event);
        String bitRept = null;
        if (comp == CcsiComponentTypeEnum.SC)
        {
            List <String> componentSCList = this.theContext.getTheSavedState().getComponentSCList();
            StringBuilder compSCString = new StringBuilder ();
            for (String x : componentSCList)
            {
                genBit.setKeyVal(x);
                compSCString.append(genBit.getTranslation(genBit.maintPeriodicBitReport));
            }
            bitRept = compSCString.toString();
        } else
        {
            bitRept = genBit.getTranslation(genBit.maintPeriodicBitReport);
        }
        return (bitRept);
    }

    /**
     * Method description
     *
     *
     * @param rdg
     *
     * @return
     */
    public String genReadingReport(PendingReadingList rdg)
    {
        StringBuilder msg = new StringBuilder(msgStart).append(CcsiChannelEnum.READGS.getChanTag()).append('>');

        if ((rdg != null) && (!rdg.isEmpty()))
        {
            String temp;
            Object[] work = rdg.getAllEntries();

            for (Object x : work)
            {
                ReadingEventMsg workOne = (ReadingEventMsg) x;
                ReadingBaseMacroProcessor genRdg = new ReadingBaseMacroProcessor(this.theContext.getTheConfig(),
                        this.theContext.getTheSavedState(), workOne);

                temp = genRdg.getTranslation(genRdg.readingBaseReport);

                if (temp != null)
                {
                    msg.append(temp);
                }
            }

            msg.append("</").append(CcsiChannelEnum.READGS.getChanTag()).append(">").append(msgEnd);
        } else
        {
            msg.append("<ReadingReport Sensor=\"").append(
                    this.theContext.getTheSavedState().getComponentSCName().getScName());
            msg.append("\" Time=\"").append(NumberFormatter.getDate(null, null));
            msg.append("\"><Data Detect=\"NONE\" Level=\"0\"/></ReadingReport>");
            msg.append("</").append(CcsiChannelEnum.READGS.getChanTag()).append(">").append(msgEnd);
        }

        return (msg.toString());
    }

    /**
     * Generates a response message from a Get_Config sent to the sensor
     * @param cmdResponse
     * @param intfc
     * @return the message
     */
    public String genSensorGetCfgResponse(IPCWrapper cmdResponse, NSDSInterface intfc)
    {
        MsgContent content = cmdResponse.getMsgBody();
        GetCfgRspArgs getCfgResp = content.getGetCfgResponse();
        List<GetCfgRspArg> argList = getCfgResp.getArg();

        StringBuilder general = new StringBuilder();
        StringBuilder local = new StringBuilder();

        List<StringBuilder> sensdesc = new ArrayList<>();
        List<String> sensdescKey = new ArrayList<>();

        StringBuilder annun = new StringBuilder();
        StringBuilder unique = new StringBuilder();
        StringBuilder tamper = new StringBuilder();

        // the arguments are receivied unordered.  they need to be sorted into 
        // blocks and, in the case of SENSDESC, {block, key} pairs
        for (GetCfgRspArg arg : argList)
        {
            switch (arg.getItem())
            {
                // GENERAL block
                case "TimeSource":
                case "LocationSource":
                case "SelectedTimeSource":
                case "SelectedLocationSource":
                case "UnitMode":
                case "LocationSupportOption":
                    createItem(general, arg);
                    break;
                // LOCAL block
                case "HostSensorID":
                case "HostSensorName":
                case "HostSensorMount":
                case "HostSensorDescription":
                    createItem(local, arg);
                    break;
                // SENSDESC block
                case "ComponentType":
                case "Present":
                case "HardwareVersion":
                case "SoftwareVersion":
                case "BitPass":
                case "SerialNumber":
                case "AnnunciationType":
                    StringBuilder block = null;
                    int index;
                    for (index = 0; index < sensdesc.size(); index++)
                    {
                        if ((arg.isSetKey()) && (arg.getKey().equals(sensdescKey.get(index))))
                        {
                            block = sensdesc.get(index);
                            break;
                        }
                    }

                    if (block == null)
                    {
                        // didn't find it, create one
                        block = new StringBuilder();
                        sensdesc.add(block);
                        sensdescKey.add(arg.getKey());
                    }
                    createItem(block, arg);
                    break;
                // ANNUN block
                case "AudibleAnnunState":
                case "VisualAnnunState":
                case "TactileAnnunState":
                case "LedAnnunState":
                case "ExternalAnnunState":
                    createItem(annun, arg);
                    break;
                // TAMPER block
                case "Count":
                case "Window":
                case "Action":
                case "Enable":
                    createItem(tamper, arg);
                    break;
                // UNIQUE block
                default:
                    // for sensor unique items
                    // verify this one is supported
                    if ((theContext.getTheConfig().getSensorUniqueConfig(arg.getItem()) != null)
                            || (theContext.getTheConfig().getSensorUniqueData(arg.getItem()) != null))
                    {
                        createItem(unique, arg);
                    } else
                    {
                        myLogger.error("Unsupported or unknown item reported");
                    }
                  
            }
        }

        StringBuilder message = new StringBuilder();
        message.append(msgStart).append(CcsiChannelEnum.CONFIG.getChanTag()).append(">");
        addConfigBlock(message, general, CcsiConfigBlockEnum.GENERAL.toString(), null);
        addConfigBlock(message, local, CcsiConfigBlockEnum.LOCAL.toString(), null);
        for (int i = 0; i < sensdesc.size(); i++)
        {
            addConfigBlock(message, sensdesc.get(i),
                    CcsiConfigBlockEnum.SENSDESC.toString(), sensdescKey.get(i));
        }
        addConfigBlock(message, annun, CcsiConfigBlockEnum.ANNUN.toString(), null);
        addConfigBlock(message, unique, CcsiConfigBlockEnum.UNIQUE.toString(), null);
        addConfigBlock(message, tamper, CcsiConfigBlockEnum.TAMPER.toString(), null);
        message.append("</ConfigChn>").append(this.msgEnd);

        return message.toString();
    }

    public String genSecurityConnectReport ()
    {
        String now = NumberFormatter.getDate(null, null);
        
        StringBuilder report = new StringBuilder (msgStart).
                append(CcsiChannelEnum.SEC.getChanTag());
        report.append (" EventType=\"connect\" EventTime=\"");
        report.append (now).append("\">");
        report.append ("<EventDetails>Connected Security channel</EventDetails>");
        report.append ("</").append(CcsiChannelEnum.SEC.getChanTag()).append(">");
        report.append(msgEnd);
        return report.toString();
    }
    
    /**
     * Creates a config Item for a Get_Config response message
     * @param item
     * @param arg 
     */
    private void createItem(StringBuilder item, GetCfgRspArg arg)
    {
        item.append("<Item Name=\"").append (arg.getItem()).append("\" Value=\"")
                .append(arg.getValue());
        if (arg.isSetKey())
        {
            item.append(" Key=\"").append(arg.getKey());
        }
        item.append("\"/>");
    }

    /**
     * Adds a block to a get_config response message
     * @param message
     * @param block
     * @param blockName
     * @param key 
     */
    private void addConfigBlock(StringBuilder message, StringBuilder block, 
            String blockName, String key)
    {
        if (block.length() > 0)
        {
            message.append("<Block Name=\"").append(blockName).append("\"");
            if (key != null)
            {
                message.append(" Key=\"").append(key).append("\"");
            }
            message.append(">").append(block.toString()).append("</Block>");
        }
    }

    /**
     * Method description
     *
     *
     * @param list
     *
     * @return
     */
    public String makeNakStatusReport(PendingNakDetailList list)
    {
        StringBuilder msg = new StringBuilder(msgStart).append(CcsiChannelEnum.STATUS.getChanTag());

        msg.append(" Alert=\"");

        if (this.theContext.getTheSavedState().getMode() == CcsiModeEnum.D)
        {
            msg.append("true\"").append("BIT=\"false\" Maint=\"false\">");
        } else
        {
            msg.append("false\"").append("BIT=\"false\" Maint=\"false\">");
        }

        for (PendingNakDetailEntry x : list.getTheList())
        {
            msg.append("<NakDetails Code=\"").append(x.getNakDetail().getXmlCode()).append("\" MSN=\"").append(
                    x.getNakMsn()).append("\">");

            if ((x.getDetailText() != null) && (!x.getDetailText().isEmpty()))
            {
                msg.append("<NakDescription>").append(x.getDetailText()).append("</NakDescription>");
            } else
            {
                msg.append("<NakDescription>").append(x.getNakDetail().getDefText()).append("</NakDescription>");
            }

            msg.append("</NakDetails>");
        }

        msg.append("</StatusChn>").append(this.msgEnd);

        return (msg.toString());
    }

    /**
     * This method makes a CCSI reporting date-time (yyyymmddhhmmss) from an
     * XMLGregorian calendar object
     *
     * @param val the calendar object to use
     *
     * @return a String formatted appropriately
     */
    private String makeTimeString(XMLGregorianCalendar val)
    {
        StringBuilder timeStr = new StringBuilder();

        try
        {
            timeStr.append(NumberFormatter.getInt(val.getYear(), 10, true, 4, '0'));
            timeStr.append(NumberFormatter.getInt(val.getMonth(), 10, true, 2, '0'));
            timeStr.append(NumberFormatter.getInt(val.getDay(), 10, true, 2, '0'));
            timeStr.append(NumberFormatter.getInt(val.getHour(), 10, true, 2, '0'));
            timeStr.append(NumberFormatter.getInt(val.getMinute(), 10, true, 2, '0'));
            timeStr.append(NumberFormatter.getInt(val.getSecond(), 10, true, 2, '0'));
        } catch (Exception ex)
        {
            myLogger.error("Exception making a time string from a calendar object", ex);
        }

        return (timeStr.toString());
    }
}
