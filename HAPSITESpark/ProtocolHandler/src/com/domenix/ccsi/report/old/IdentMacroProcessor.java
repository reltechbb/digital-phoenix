/*
 * IdentMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 * This class generates IDENT channel reports.
 * 
 * @author kmiller
 */
public class IdentMacroProcessor extends MacroProcessor
{
  public final String fullIdentReport = "<Instance SensorName=\"{sensName}\">" +
  "<InstanceSerial>{sensorSerial}</InstanceSerial>" +
  "<InstanceSoftwareVersion major=\"{swVersMaj}\" moderate=\"{swVersMod}\" minor=\"{swVersMin}\" patch=\"{swVersPat}\"/>" +
  "<InstanceHardwareVersion major=\"{hwVersMaj}\" moderate=\"{hwVersMod}\" minor=\"{hwVersMin}\" patch=\"{hwVersPat}\"/>" +
  "<InstanceCcsiVersion major=\"{ccsiVersMaj}\" moderate=\"{ccsiVersMod}\" minor=\"{ccsiVersMin}\" patch=\"{ccsiVersPat}\"/>" +
  "<InstanceUuid>{sensorUuid}</InstanceUuid>{sensHostId}</Instance></IdentChn>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   */
  public IdentMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  {
    super.textMacros	=
      "\\{(sensName|sensorSerial|swVersMaj|swVersMod|swVersMin|swVersPat|hwVersMaj|hwVersMod|hwVersMin|hwVersPat|ccsiVersMaj|ccsiVersMod|ccsiVersMin|ccsiVersPat|sensorUuid|sensHostId)\\}";
    super.macroMap.put( "sensname" , this );
    super.macroMap.put( "sensorserial" , this );
    super.macroMap.put( "swversmaj" , this );
    super.macroMap.put( "swversmod" , this );
    super.macroMap.put( "swversmin" , this );
    super.macroMap.put( "swverspat" , this );
    super.macroMap.put( "hwversmaj" , this );
    super.macroMap.put( "hwversmod" , this );
    super.macroMap.put( "hwversmin" , this );
    super.macroMap.put( "hwverspat" , this );
    super.macroMap.put( "ccsiversmaj" , this );
    super.macroMap.put( "ccsiversmod" , this );
    super.macroMap.put( "ccsiversmin" , this );
    super.macroMap.put( "ccsiverspat" , this );
    super.macroMap.put( "sensoruuid" , this );
    super.macroMap.put( "senshostid" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "sensname" :
        retVal = this.theConfig.getSensorName();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "sensorserial" :
        retVal = this.theConfig.getSerial();
        break;

      case "swversmaj" :
        pVal = this.theConfig.getSwVersion().getMajor();
        retVal = Integer.toString( pVal );
        break;

      case "swversmod" :
        pVal = this.theConfig.getSwVersion().getModerate();
        retVal = Integer.toString( pVal );
        break;

      case "swversmin" :
        pVal = this.theConfig.getSwVersion().getMinor();
        retVal = Integer.toString( pVal );
        break;

      case "swverspat" :
        pVal = this.theConfig.getSwVersion().getPatch();
        retVal = Integer.toString( pVal );
        break;
        
      case "hwversmaj" :
        pVal = this.theConfig.getHwVersion().getMajor();
        retVal = Integer.toString( pVal );
        break;

      case "hwversmod" :
        pVal = this.theConfig.getHwVersion().getMinor();
        retVal = Integer.toString( pVal );
        break;

      case "hwversmin" :
        pVal = this.theConfig.getHwVersion().getModerate();
        retVal = Integer.toString( pVal );
        break;

      case "hwverspat" :
        pVal = this.theConfig.getHwVersion().getPatch();
        retVal = Integer.toString( pVal );
        break;
        
      case "ccsiversmaj" :
        pVal = this.theConfig.getCcsiVersion().getMajor();
        retVal = Integer.toString( pVal );
        break;

      case "ccsiversmod" :
        pVal = this.theConfig.getCcsiVersion().getModerate();
        retVal = Integer.toString( pVal );
        break;

      case "ccsiversmin" :
        pVal = this.theConfig.getCcsiVersion().getMinor();
        retVal = Integer.toString( pVal );
        break;

      case "ccsiverspat" :
        pVal = this.theConfig.getCcsiVersion().getPatch();
        retVal = Integer.toString( pVal );
        break;
        
      case "sensoruuid" :
        retVal = this.theConfig.getUuid();
        break;
        
      case "senshostid" :
//        if ( ( this.theState.getHostSensorID() != null ) &&
//             ( !this.theState.getHostSensorID().trim().isEmpty() ) )
        if (theState.getHostNetworkSettings().getHostSensorIDOrUUID() != null )
        {
          StringBuilder hid = new StringBuilder( "<InstanceHostId>" );
          hid.append( this.theState.getHostSensorID() );
          hid.append( "</InstanceHostId>" );
          retVal = hid.toString();
        }
        break;

      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
