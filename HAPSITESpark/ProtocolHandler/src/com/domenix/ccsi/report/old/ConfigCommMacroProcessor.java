/*
 * ConfigCommMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 * This class provides generation for the COMM block of a configuration channel report.
 * 
 * @author kmiller
 */
public class ConfigCommMacroProcessor extends MacroProcessor
{
  /** Macro formatter for COMM blocks */
  public final String commConfigReport = "<Block Name=\"COMM\"><Item Name=\"HeartbeatRate\""
  + " Value=\"{hbRate}\"/><Item Name=\"HostNetworkType\" Value=\"{hostNwType}\"/><Item Name=\"OpenInterfacePort\""
  + " Value=\"{oiPort}\"/><Item Name=\"OpenInterfaceServer\" Value=\"{oiServer}\"/><Item Name=\"AckTimeout\""
  + " Value=\"{ackTimeout}\"/><Item Name=\"ConnectionLimit\" Value=\"{connLimit}\"/><Item Name=\"ConnectRetryTimeout\""
  + " Value=\"{connRetry}\"/></Block>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   */
  public ConfigCommMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  {//hbRate|hostNwType|oiPort|oiServer|ackTimeout|connLimit|connRetry
    super.textMacros	=
      "\\{(hbRate|hostNwType|oiPort|oiServer|ackTimeout|connLimit|connRetry)\\}";
    super.macroMap.put( "hbrate" , this );
    super.macroMap.put( "hostnwtype" , this );
    super.macroMap.put( "oiport" , this );
    super.macroMap.put( "oiserver" , this );
    super.macroMap.put( "acktimeout" , this );
    super.macroMap.put( "connlimit" , this );
    super.macroMap.put( "connretry" , this );
 }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {//hbRate|hostNwType|oiPort|oiServer|ackTimeout|connLimit|connRetry
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "hbrate" :
        retVal = Integer.toString(this.theState.getHeartbeat());     
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "hostnwtype" :
        retVal = this.theConfig.getHostNetworkSettings().getNetworkProtocolType().toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "oiport" :
        // if the PH is the server then want the PH port
        // else the Host port
        if (this.theConfig.getHostNetworkSettings().isOIServer())
        {
            retVal = Integer.toString(this.theConfig.getHostNetworkSettings().getPort());
        } else
        {
            retVal = Integer.toString(this.theConfig.getHostNetworkSettings().getPHPort());
        }
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "oiserver" :
        retVal = Boolean.toString(this.theConfig.getHostNetworkSettings().isOIServer());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
        
      case "acktimeout" :
        retVal = Integer.toString(this.theConfig.getAckTimeout());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "connlimit" :
        retVal = Integer.toString(this.theConfig.getConnLimit());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "connretry" :
        retVal = Integer.toString(this.theConfig.getHostNetworkSettings().getConnRetryTime());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
