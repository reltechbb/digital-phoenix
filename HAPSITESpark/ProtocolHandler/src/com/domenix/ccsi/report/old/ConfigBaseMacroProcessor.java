/*
 * ConfigBaseMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 * This class provides the base processing for Configuration channel report generation.
 *
 * @author kmiller
 */
public class ConfigBaseMacroProcessor extends MacroProcessor
{
  /** The base macros. */
  public final String	baseConfigReport	=
    "<Block Name=\"BASE\"><Item Name=\"EncryptionType\" Value=\"{encryptType}\"/></Block>";

  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   */
  public ConfigBaseMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;
      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes
   *
   */
  private void initialize()
  {
    super.textMacros	= "\\{(encryptType)\\}";
    super.macroMap.put( "encrypttype" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {
    String	retVal	= " ";
    int			pVal		= 0;

    switch ( macro )
    {
      case "encrypttype" :
        retVal	= this.theConfig.getEncryptType();

        if ( ( retVal == null ) || ( retVal.trim().isEmpty() ) )
        {
          retVal	= "NONE";
        }

        break;

      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return ( super.getTranslation( inString ) );
  }
}
