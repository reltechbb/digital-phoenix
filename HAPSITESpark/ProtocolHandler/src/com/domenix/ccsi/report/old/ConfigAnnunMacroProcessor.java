/*
 * ConfigAnnunMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains a macro for generating the Annunciator Configuration report
 *
 */
package com.domenix.ccsi.report.old;

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

/**
 * Macro for formatting the ANNUN block
 * @author jmerritt
 */
public class ConfigAnnunMacroProcessor extends MacroProcessor
{

    /**
     * Macro for building the Annunciator Report
     */
    public static final String ANNUNCONFIGREPORT
            = "<Block Name=\"ANNUN\">"
            + "<Item Name=\"AudibleAnnunState\" Value=\"{audibleAnnunState}\"/>"
            + "<Item Name=\"VisualAnnunState\" Value=\"{visualAnnunState}\"/>"
            + "<Item Name=\"TactileAnnunState\" Value=\"{tactileAnnunState}\"/>"
            + "<Item Name=\"LedAnnunState\" Value=\"{ledAnnunState}\"/>"
            + "<Item Name=\"ExternalAnnunState\" Value=\"{externalAnnunState}\"/>"
            + "</Block>";

    /**
     * The sensor configuration
     */
    private CcsiConfigSensor theConfig;

    /**
     * The saved state
     */
    private CcsiSavedState theState;

    /**
     * Constructs a class instance
     *
     * @param cfg the sensor configuration
     * @param state the saved state
     */
    public ConfigAnnunMacroProcessor(CcsiConfigSensor cfg, CcsiSavedState state)
    {
        super();

        if ((cfg != null) && (state != null))
        {
            theConfig = cfg;
            theState = state;

            initialize();
        } else
        {
            throw new IllegalArgumentException("Missing or null argument.");
        }
    }

    /**
     * This initializes the macro processor
     *
     */
    private void initialize()
    {
        super.textMacros
                = "\\{(audibleAnnunState|visualAnnunState|tactileAnnunState|" +
                "ledAnnunState|externalAnnunState)\\}";
        super.macroMap.put("audibleannunstate", this);
        super.macroMap.put("visualannunstate", this);
        super.macroMap.put("tactileannunstate", this);
        super.macroMap.put("ledannunstate", this);
        super.macroMap.put("externalannunstate", this);
    }

      /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "audibleannunstate" :
        retVal = theConfig.getAudibleAnnunState().toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "visualannunstate" :
        retVal = theConfig.getVisualAnnunState().toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "tactileannunstate" :
        retVal = theConfig.getTactileAnnunState().toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "ledannunstate" :
        retVal = theConfig.getLedAnnunState().toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
        
      case "externalannunstate" :
        retVal = theConfig.getExternalAnnunState().toString();
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
        
      default :
        break;
    }

    return ( retVal );
  }
}
