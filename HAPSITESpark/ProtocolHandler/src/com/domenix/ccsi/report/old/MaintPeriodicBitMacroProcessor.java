/*
 * IdentMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.common.spark.data.BITEventMsg;
import com.domenix.common.spark.data.CcsiComponentTypeEnum;
import com.domenix.utils.MacroProcessor;
import com.domenix.common.utils.NumberFormatter;

//~--- JDK imports ------------------------------------------------------------

import java.util.Date;

//~--- classes ----------------------------------------------------------------

/**
 * This class generates MAINT channel reports for period BIT results.
 * 
 * @author kmiller
 */
public class MaintPeriodicBitMacroProcessor extends MacroProcessor
{
  // comp|time|result

  /** Field description */
  public final String	maintPeriodicBitReport	=
    "<BIT Component=\"{comp}\"{compid} Executed=\"{time}\" Result=\"{result}\"></BIT>";

  /** Field description */
  private CcsiComponentTypeEnum	comp;

  /** Field description */
  private BITEventMsg	msg;

  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  private String keyVal = null;
  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   * @param comp
   * @param msg
   */
  public MaintPeriodicBitMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state , CcsiComponentTypeEnum comp ,
          BITEventMsg msg )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      if ( comp != null )
      {
        this.comp	= comp;
      }

      if ( msg != null )
      {
        this.msg	= msg;
      }

      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes
   *
   */
  private void initialize()
  {    // comp|time|result
    super.textMacros	= "\\{(comp|compid|time|result)\\}";
    super.macroMap.put( "comp" , this );
    super.macroMap.put( "compid" , this );
    super.macroMap.put( "time" , this );
    super.macroMap.put( "result" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {    // comp|time|result
    String	retVal	= "";
    int			pVal		= 0;

    if ( msg != null )
    {
      switch ( macro )
      {
        case "comp" :
          retVal = this.msg.getComponentType().name();
          if ( retVal == null )
          {
            retVal	= "";
          }

          break;

        case "compid" :
          if ( this.comp == CcsiComponentTypeEnum.SC )
          {
            retVal	= " ComponentId=\"" + this.theState.getComponentSCName(keyVal) + "\"";
          }
          else
          {
            retVal	= " ";
          }

          break;

        case "time" :
          Date	temp;

          if ( this.msg.isSetBitTime() )
          {
            temp	= this.msg.getBitTime().toGregorianCalendar().getTime();
          }
          else
          {
            temp	= null;
          }

          retVal	= NumberFormatter.getDate( temp , null );

          break;

        case "result" :
          if ( this.msg.isSetResult() )
          {
            retVal	= this.msg.getResult().value();
          }

          if ( retVal == null )
          {
            retVal	= "PASS";
          }

          break;

        default :
          break;
      }
    }
    else
    {
      switch ( macro )
      {
        case "comp" :
          retVal	= this.comp.toString();

          if ( retVal == null )
          {
            retVal	= "";
          }

          break;

        case "compid" :
          if ( this.comp == CcsiComponentTypeEnum.SC )
          {
            retVal	= " ComponentId=\"" + this.theState.getComponentSCName(keyVal) + "\"";
          }
          else
          {
            retVal	= " ";
          }

          break;

        case "time" :
          Date temp = null;
          if ( this.comp == CcsiComponentTypeEnum.CC )
          {
            temp = this.theState.getComponentCCBitTime();
          }
          else if ( this.comp == CcsiComponentTypeEnum.PDIL )
          {
            temp = this.theState.getComponentPDILBitTime();
          }
          else
          {
            temp = this.theState.getComponentSCBitTime(keyVal);
          }
          retVal = NumberFormatter.getDate( temp , null );
          break;

        case "result" :
          if ( this.comp == CcsiComponentTypeEnum.CC )
          {
            retVal = this.theState.getComponentCCBitStatus().name();
          }
          else if ( this.comp == CcsiComponentTypeEnum.PDIL )
          {
            retVal = this.theState.getComponentPDILBitStatus().name();
          }
          else
          {
            retVal = this.theState.getComponentSCBitStatus(keyVal).toString();
          }
          
          break;

        default :
          break;
      }
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return ( super.getTranslation( inString ) );
  }
  
  public void setKeyVal (String key)
  {
      keyVal = key;
  }
}
