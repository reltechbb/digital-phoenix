/*
 * IdentMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/03
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.common.spark.data.BITEventMsg;
import com.domenix.common.spark.data.BitResultEnum;
import com.domenix.common.spark.data.CcsiComponentTypeEnum;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 * This class generates MAINT channel reports for periodic failure events.
 * 
 * @author kmiller
 */
public class MaintPeriodicFailureMacroProcessor extends MacroProcessor
{
  /** Macro formatter for MAINT periodic failure reports */
  public final String maintPeriodicFailureReport = "<Failure Unit=\"{unit}\"{unitid} Inoperable=\"{inop}\" Degraded=\"{degrade}\"><Description>{descript}</Description></Failure>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  private BITEventMsg msg;
  
  private CcsiComponentTypeEnum comp;
  
  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   * @param msg the BIT event for failed component
   */
  public MaintPeriodicFailureMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state , BITEventMsg msg)
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      if ( msg != null )
      {
        this.msg = msg;
      }
      
      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  { //unit|inop|degrade|descript
    super.textMacros	=
      "\\{(unit|unitid|inop|degrade|descript)\\}";
    super.macroMap.put( "unit" , this );
    super.macroMap.put( "unitid" , this );
    super.macroMap.put( "inop" , this );
    super.macroMap.put( "degrade" , this );
    super.macroMap.put( "descript" , this );

  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  { //unit|inop|degrade|descript
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "unit" :
        if ( this.msg.isSetComponentType() )
        {
          this.comp = this.msg.getComponentType();
          retVal = this.msg.getComponentType().name();
        }
        break;
        
      case "unitid" :
        if ( this.msg.isSetComponentId() )
        {
          retVal = " UnitId=\"" + this.msg.getComponentId() + "\"";
        }
        else
        {
          if ( this.comp == CcsiComponentTypeEnum.SC )
          {
            retVal = " UnitId=\"" + this.theState.getComponentSCName().getScName() + "\"";
          }
          else
          {
            retVal = " ";
          }
        }
        break;
 
      case "inop" :
        if ( this.msg.isSetResult() )
        {
          if ( this.msg.getResult() == BitResultEnum.FAIL )
          {
            retVal = "true";
          }
          else
          {
            retVal = "false";
          }
        }
        else
        {
          retVal = "false";
        }
        break;

      case "degrade" :
        retVal = "false";
        
        break;
 
      case "descript" :
        if ( this.msg.isSetError() )
        {
          retVal = this.msg.getError();
        }
        break;
       
      default :
        break;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
