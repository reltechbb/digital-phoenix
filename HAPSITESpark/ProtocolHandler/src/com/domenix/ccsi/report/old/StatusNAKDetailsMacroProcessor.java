/*
 * StatusNAKDetailsMacroProcessor.java
 *
* Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
  *
 * This file contains ...
 *
 * Version: V1.0  15/10/20
 */


package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.ccsi.protocol.PendingNakDetailEntry;
import com.domenix.ccsi.protocol.PendingNakDetailList;
import com.domenix.utils.MacroProcessor;

//~--- classes ----------------------------------------------------------------

/**
 * This class produces a single NAK details status report element.
 * 
 * @author michaelnguyen27
 */
public class StatusNAKDetailsMacroProcessor extends MacroProcessor
{
  /** The macro definition */
  public final String	nakDetailsStatusReport	= "<NakDetails Code=\"{nakCode}\" MSN=\"{nakMSN}\""
                                                + "{nakDescription}</NakDetails>";

  /** The list of pending NAKs */
  private PendingNakDetailList	naks;

  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   * @param cfg the sensor configuration
   * @param state the saved state
   * @param details the list of pending NAK details
   */
  public StatusNAKDetailsMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state , PendingNakDetailList details )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;

      if ( details != null )
      {
        this.naks	= details;
      }

      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes
   *
   */
  private void initialize()
  {
    super.textMacros	= "\\{(nakCode|nakMSN|nakDescription)\\}";
    super.macroMap.put( "nakcode" , this );
    super.macroMap.put( "nakmsn" , this );
    super.macroMap.put( "nakdescription" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {
    String	retVal	= "";
    int			pVal		= 0;

    if ( naks != null )
    {
      for ( PendingNakDetailEntry x : naks.getTheList() )
      {
        switch ( macro )
        {
          case "nakcode" :
            NakDetailCodeEnum detail = x.getNakDetail();
            if (detail != null)
            {
                retVal = x.getNakDetail().getXmlCode();
            }

            if ( retVal == null )
            {
              retVal	= "";
            }

            break;

          case "nakmsn" :
            retVal	= Long.toString( x.getNakMsn() );

            break;

          case "nakdescription" :
            String	theText	= x.getDetailText();

            if ( ( theText == null ) || ( theText.trim().isEmpty() ) )
            {
              retVal	= ">";
            }
            else
            {
              retVal	= "><NakDescription>" + theText.trim() + "</NakDescription>";
            }

            break;

          default :
            break;
        }    // End switch
      }      // End for
    }        // End if

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return ( super.getTranslation( inString ) );
  }
}
