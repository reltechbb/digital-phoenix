/*
 * StatusConnectMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This class generates the connection status portion of a status channel report.
 *
 */
package com.domenix.ccsi.report.old;

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

/**
 * This class generates the connection status portion of a status channel report.
 * 
 * @author michaelnguyen27
 */
public class StatusConnectMacroProcessor extends MacroProcessor
{ //host|connSt|ipAddr|linkUsed|user
  public final String statusConnectReport = "<ConnectStatus ConnectType=\"{host}\""
  + " ConnectState=\"{connSt}\"><HostIpAddress>{ipAddr}</HostIpAddress><LinkUsed>\"{linkUsed}\"</LinkUsed></ConnectStatus>";
  
  /** The sensor configuration */
  private CcsiConfigSensor	theConfig;

  /** The saved state */
  private CcsiSavedState	theState;
  
  /** The connect ID number */
  private long connId;
  
  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param cfg the sensor configuration
   * @param state the saved state
   * @param connId the connection ID
   */
  public StatusConnectMacroProcessor( CcsiConfigSensor cfg , CcsiSavedState state , long connId )
  {
    super();

    if ( ( cfg != null ) && ( state != null ) )
    {
      this.theConfig	= cfg;
      this.theState		= state;
      this.connId = connId;

      initialize();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or null argument." );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This initializes the macro processor
   *
   */
  private void initialize()
  {//host|connSt|ipAddr|linkUsed|user
    super.textMacros	=
      "\\{(host|connSt|ipAddr|linkUsed)\\}";
    super.macroMap.put( "host" , this );
    super.macroMap.put( "connst" , this );
    super.macroMap.put( "ipaddr" , this );
    super.macroMap.put( "linkused" , this );
  }

  /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  { //host|connSt|ipAddr|linkUsed|user
    StringBuilder	retVal	= new StringBuilder();

    switch ( macro )
    {
      case "host" :
        retVal.append( "host" );
        
        break;

      case "connst" :
        retVal.append( "connected" );
        
        break;

      case "ipaddr" :
        String hostIp = this.theState.getTheOpenInterface().getHostIp( this.connId );
//        String hostIp = this.theConfig.getHostNetworkSettings().getIP().getHostAddress();
        if ( hostIp == null )
        {
          hostIp = "";
        }
        if ( hostIp.contains( ":" ) )
        {
          retVal.append( "<IpAddressTypeIpv6>").append( hostIp ).append( "</IpAddressTypeIpv6>" );
        }
        else
        {
          retVal.append( "<IpAddressTypeIpv4>").append( hostIp ).append( "</IpAddressTypeIpv4>" );
        }
        
        break;

      case "linkused" :
        retVal.append( this.theState.getStateLinkName().getLinkName() );
        
        break;

      default :
        break;
    }

    return ( retVal.toString() );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method runs macro processing on the input string and returns the result.
   *
   * @param inString the string to be processed
   *
   * @return String containing the result of macro processing
   */
  @Override
  public String getTranslation( String inString )
  {
    return( super.getTranslation(inString) );
  }
}
