/*
 * ConfigSecMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Macro for formatting the SECURITY block
 *
 */
package com.domenix.ccsi.report.old;

import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.utils.MacroProcessor;

/**
 * Macro for formatting the SECURITY block
 * @author jmerritt
 */
public class ConfigSecMacroProcessor extends MacroProcessor
{
    /**
     * Macro for building the Annunciator Report
     */
    public static final String SECCONFIGREPORT
            = "<Block Name=\"SECURITY\">"
            + "<Item Name=\"EncryptComm\" Value=\"{encryptComm}\"/>"
            + "<Item Name=\"EncryptData\" Value=\"{encryptData}\"/>"
            + "<Item Name=\"HostAuthentication\" Value=\"{hostAuthentication}\"/>"
            + "<Item Name=\"OperatorAuthentication\" Value=\"{operatorAuthentication}\"/>"
            + "<Item Name=\"UserPwdTimeout\" Value=\"{userPwdTimeout}\"/>"
            + "<Item Name=\"PwReuseLength\" Value=\"{pwReuseLength}\"/>"
            + "<Item Name=\"LoginFailTimeout\" Value=\"{loginFailTimeout}\"/>"
            + "<Item Name=\"ConnectFailTimeout\" Value=\"{connFailureTimeout}\"/>"
            + "</Block>";

    /**
     * The sensor configuration
     */
    private CcsiConfigSensor theConfig;

    /**
     * The saved state
     */
    private CcsiSavedState theState;

    /**
     * Constructs a class instance
     *
     * @param cfg the sensor configuration
     * @param state the saved state
     */
    public ConfigSecMacroProcessor(CcsiConfigSensor cfg, CcsiSavedState state)
    {
        super();

        if ((cfg != null) && (state != null))
        {
            theConfig = cfg;
            theState = state;

            initialize();
        } else
        {
            throw new IllegalArgumentException("Missing or null argument.");
        }
    }

    /**
     * This initializes the macro processor
     *
     */
    private void initialize()
    {
        super.textMacros
                = "\\{(encryptComm|encryptData|hostAuthentication|" +
                "operatorAuthentication|userPwdTimeout|pwReuseLength|" +
                "loginFailTimeout|connFailureTimeout)\\}";
        super.macroMap.put("encryptcomm", this);
        super.macroMap.put("encryptdata", this);
        super.macroMap.put("hostauthentication", this);
        super.macroMap.put("operatorauthentication", this);
        super.macroMap.put("userpwdtimeout", this);
        super.macroMap.put("pwreuselength", this);
        super.macroMap.put("loginfailtimeout", this);
        super.macroMap.put("connfailuretimeout", this);
    }

      /**
   * This method translates a particular macro into its string value or null if there is
   * no translation.
   *
   * @param macro the macro to be translated
   *
   * @return String containing the replacement value or null if no replacement
   */
  @Override
  public String translateMacro( String macro )
  {
    String	retVal	= "";
    int     pVal = 0;

    switch ( macro )
    {
      case "encryptcomm" :
        retVal = Boolean.toString(theConfig.getHostNetworkSettings().getEncryptComm());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "encryptdata" :
        retVal = Boolean.toString(theConfig.getEncryptData());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;

      case "hostauthentication":
          retVal = "false";
          break;
          
      case "operatorauthentication":
          retVal = "false";
          break;
          
       case "userpwdtimeout":
          retVal = "0";
          break;
          
        case "pwreuselength":
          retVal = "0";
          break;
          
        case "loginfailtimeout":
          retVal = "0";
          break;
           
      case "connfailuretimeout" :
        retVal = Integer.toString(theConfig.getConnFailureTimeout());
        if ( retVal == null )
        {
          retVal = "";
        }
        break;
       
      default :
        break;
    }

    return ( retVal );
  }
}
