/*
 * AlertReadingMacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/11/03
 */
package com.domenix.ccsi.report.old;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.ccsi.protocol.ActiveAlertListEntry;
import com.domenix.ccsi.protocol.PendingAlertEntry;
import com.domenix.common.spark.data.ReadingEventMsg;
import com.domenix.utils.MacroProcessor;
import com.domenix.common.utils.NumberFormatter;

//~--- JDK imports ------------------------------------------------------------
import java.util.Date;

//~--- classes ----------------------------------------------------------------
/**
 * This class generates alert report information for readings.
 *
 * @author kmiller
 */
public class AlertReadingMacroProcessor extends MacroProcessor {

    /**
     * Field description
     */
    public final String readingAlertReport = "<Msg><AlertsChn Event=\"{eventAlert}\" Source=\"{sourceAlert}\""
            + " Time=\"{alertDtg}\" AlertId=\"{alertId}\">{reading}</AlertsChn></Msg>";

    /**
     * The reading message
     */
    private PendingAlertEntry msg;

    /**
     * The enclosed reading
     */
    private ReadingEventMsg rdg;

    /**
     * The sensor configuration
     */
    private CcsiConfigSensor theConfig;

    /** alert id */
    String alertId = null;
    
    /**
     * The saved state
     */
    private CcsiSavedState theState;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs a class instance
     *
     * @param cfg the sensor configuration
     * @param state the saved state
     * @param msg the reading message
     */
    public AlertReadingMacroProcessor(CcsiConfigSensor cfg, CcsiSavedState state, PendingAlertEntry msg) {
        super();

        if ((cfg != null) && (state != null)) {
            this.theConfig = cfg;
            this.theState = state;
            this.msg = msg;
            this.alertId = msg.getAlertId();
            this.rdg = msg.getBaseMsg().getReading();
            initialize();
        } else {
            throw new IllegalArgumentException("Missing or null argument.");
        }
    }

    /**
     * Constructs a class instance
     *
     * @param cfg the sensor configuration
     * @param state the saved state
     * @param msg the reading message
     */
    public AlertReadingMacroProcessor(CcsiConfigSensor cfg, CcsiSavedState state, ActiveAlertListEntry msg) {
        super();

        if ((cfg != null) && (state != null)) {
            this.theConfig = cfg;
            this.theState = state;
            this.alertId = msg.getAlertId();
            this.rdg = msg.getEntry().getMsgBody().getReading();
            initialize();
        } else {
            throw new IllegalArgumentException("Missing or null argument.");
        }
    }

    //~--- methods --------------------------------------------------------------
    /**
     * This initializes
     *
     */
    private void initialize() {
        super.textMacros = "\\{(eventAlert|sourceAlert|alertDtg|alertId|reading)\\}";
        super.macroMap.put("eventalert", this);
        super.macroMap.put("sourcealert", this);
        super.macroMap.put("alertdtg", this);
        super.macroMap.put("alertid", this);
        super.macroMap.put("reading", this);
    }

    /**
     * This method translates a particular macro into its string value or null if there is no
     * translation.
     *
     * @param macro the macro to be translated
     *
     * @return String containing the replacement value or null if no replacement
     */
    @Override
    public String translateMacro(String macro) {
        String retVal = "";
        long pVal = 0;

        if (this.alertId != null) {
            switch (macro) {
                case "eventalert":
                    /*          if ( this.msg.isTrigger() )
          {
            retVal	= this.rdg.getAlert().name();
          }
          else
          {
            retVal	= "NONE";
          }
                     */
                    retVal = this.rdg.getAlert().name();
                    break;

                case "sourcealert":
                    retVal = "detector";

                    break;

                case "alertdtg":
                    Date temp;

                    if (this.rdg.isSetTime()) {
                        temp = this.rdg.getTime().toGregorianCalendar().getTime();
                    } else {
                        temp = null;
                    }

                    retVal = NumberFormatter.getDate(temp, null);

                    break;

                case "alertid":
                    retVal = this.alertId;

                    break;

                case "reading":
                    ReadingBaseMacroProcessor genRdg = new ReadingBaseMacroProcessor(this.theConfig, this.theState,
                            this.rdg);

                    retVal = genRdg.getTranslation(genRdg.readingAlertReport);

                    break;

                default:
                    break;
            }
        }

        return (retVal);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * This method runs macro processing on the input string and returns the result.
     *
     * @param inString the string to be processed
     *
     * @return String containing the result of macro processing
     */
    @Override
    public String getTranslation(String inString) {
        return (super.getTranslation(inString));
    }
}
