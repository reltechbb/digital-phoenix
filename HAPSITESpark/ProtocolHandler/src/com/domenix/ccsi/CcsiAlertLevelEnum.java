/*
 * CcsiAlertLevelEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the three different alert levels.
 *
 * Version: V1.0  15/09/10
 */


package com.domenix.ccsi;

/**
 * This enumeration defines the three levels for a CCSI alert
 * 
 * @author kmiller
 */
public enum CcsiAlertLevelEnum
{
  /** The alert has been triggered and is active */
  TRIGGER , 
  /** The alert has been untriggered and is not active */
  UNTRIGGER , 
  /** There is no alert */
  NONE; }
