/*
 * ActiveAlertsListType.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains a list of active alerts from the sensor.
 *
 * Version: V1.0  15/09/10
 */


package com.domenix.ccsi;

//~--- JDK imports ------------------------------------------------------------

import com.domenix.common.utils.NumberFormatter;
import java.util.ArrayList;

//~--- classes ----------------------------------------------------------------

/**
 * Container for a list of active sensor alerts that is saved and can be restored after a sensor
 * restart.
 *
 * @author kmiller
 */
public class ActiveAlertsListType
{
  /** Field description */
  ArrayList<ActiveAlertsType>	theList	= new ArrayList<>();

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   */
  public ActiveAlertsListType()
  {
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Clears the contents of the list
   *
   */
  public void clearList()
  {
    this.theList.clear();
  }

  /**
   * Adds a new active alert
   *
   * @param alert the active alert to be added
   *
   * @return flag indicating success (true) or failure (false)
   */
  public boolean addActiveAlert( ActiveAlertsType alert )
  {
    boolean	retValue;

    retValue	= this.theList.add( alert );

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns a flag indicating if the specified alert ID exists in the list
   *
   * @param id the alert ID to be found
   *
   * @return flag indicating found (true) or not found (false)
   */
  public boolean hasAlertId( long id )
  {
    boolean	retValue	= false;

    for ( ActiveAlertsType x : this.theList )
    {
      if ( x.getAlertId() == id )
      {
        retValue	= true;

        break;
      }
    }

    return ( retValue );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Removes an active alert entry from the list
   *
   * @param id the alert ID to be removed
   *
   * @return flag indicating success or failure
   */
  public boolean removeActiveAlert( long id )
  {
    boolean	retValue	= false;

    for ( ActiveAlertsType x : this.theList )
    {
      if ( x.getAlertId() == id )
      {
        retValue	= this.theList.remove( x );

        break;
      }
    }

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns an active alert record from the list using its ID to support status
   * updates of the alert.
   *
   * @param id the active alert ID to be returned
   *
   * @return the record or null if not found
   */
  public ActiveAlertsType getActiveAlert( long id )
  {
    ActiveAlertsType	retValue	= null;

    for ( ActiveAlertsType x : this.theList )
    {
      if ( x.getAlertId() == id )
      {
        retValue	= x;

        break;
      }
    }

    return ( retValue );
  }

  /**
   * Returns a a list of strings containing active alert information to be used to update
   * the saved state.  Each string is formatted as:  key|value using a vertical line to separate
   * the key string from the state value.
   *
   * @return list of alert saved state strings which may be empty if none are present
   */
  public ArrayList<String> getAlertSavedStates()
  {
    ArrayList<String>	alertStates	= new ArrayList<>();
    int								index				= 1;
    StringBuilder     msg         = new StringBuilder();

    for ( ActiveAlertsType x : this.theList )
    {
      msg.setLength( 0 );
      msg.append( "ccsi.alert." ).append( NumberFormatter.getInt(index, 10, true, 2, '0') ).append( '|' );
      msg.append( x.toStateString(index++) );
      alertStates.add( msg.toString() );
    }

    return ( alertStates );
  }
}
