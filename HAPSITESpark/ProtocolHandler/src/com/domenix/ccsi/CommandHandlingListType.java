/*
 * CommandHandlingListType.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains a list of defined command handling records for the sensor.
 *
 * Version: V1.0  15/09/10
 */


package com.domenix.ccsi;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import org.apache.log4j.Logger;

//~--- classes ----------------------------------------------------------------

/**
 * This class encapsulates a list of defined commands for the sensor and how they
 * are to be handled.
 * 
 * @author kmiller
 */
public class CommandHandlingListType
{
  /** The list */
  private final HashMap<String,CommandHandlingType>	theList	= new HashMap<>();
  
  /** Error/Debug Logger */
  private final Logger myLogger = Logger.getLogger( CommandHandlingListType.class );

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs the class instance
   */
  public CommandHandlingListType()
  {
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Erases the list content
   */
  public void clearList()
  {
    this.theList.clear();
  }

  /**
   * Adds a new command definition to the list
   *
   * @param cmd the command handling to be added
   *
   * @return flag indicating success or failure
   */
  public boolean addCommand( CommandHandlingType cmd )
  {
    boolean	retValue	= false;

    if ( this.theList.put( cmd.getCommand() , cmd ) == null )
    {
      retValue = true;
    }
    else
    {
      myLogger.error( "Multiple entries for command " + cmd.getCommand() );
    }

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the definition for the specified command.
   *
   * @param name the name of the command to be found
   *
   * @return the command handling or null if not found
   */
  public CommandHandlingType getCommand( String name )
  {
    CommandHandlingType	retValue;

    retValue = this.theList.get( name );
    
    return ( retValue );
  }
}
