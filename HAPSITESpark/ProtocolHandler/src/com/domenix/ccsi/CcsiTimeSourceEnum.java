/*
 * CcsiTimeSourceEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the enumeration of CCSI time sources.
 *
 * Version: V1.0  15/08/30
 */


package com.domenix.ccsi;

/**
 * This enumerates the sources of sensor time.
 *
 * @author kmiller
 */
public enum CcsiTimeSourceEnum
{
  /** No time source, using host message headers for synchronization */
  none ,

  /** Manually set by a user */
  manual ,

  /** Internal GPS or other mechanism */
  internal ,

  /** Host set the time */
  host;
}
