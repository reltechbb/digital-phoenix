/*
 * CcsiLinkNameType.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This defines a CCSI link name which must a pattern of [UWORF][0-9]{3}.
 *
 * Version: V1.0  15/08/30
 */


package com.domenix.ccsi;

//~--- JDK imports ------------------------------------------------------------

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//~--- classes ----------------------------------------------------------------

/**
 * This defines a CCSI link name which must a pattern of [UWORF][0-9]{3}.
 *
 * @author kmiller
 */
public class CcsiLinkNameType
{
  /** The pattern of a valid name */
  private final static String	LINK_NAME_REGEX	= "[UWORF][0-9]{3}";

  /** The Regex matching pattern */
  private final Pattern	linkNamePattern	= Pattern.compile( LINK_NAME_REGEX );

  /** The link name */
  private String	linkName;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a default instance without a name
   */
  public CcsiLinkNameType()
  {
    this.linkName	= "";
  }

  /**
   * Constructs a link name with an input string and throws an IllegalArgumentException if
   * the name is not valid.
   *
   * @param linkName - the link name
   */
  public CcsiLinkNameType( String linkName )
  {
    Matcher	m	= linkNamePattern.matcher( linkName );

    if ( m.matches() )
    {
      this.linkName	= linkName;
    }
    else
    {
      throw new IllegalArgumentException( "Invalid link name in constructor" );
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the linkName or a null if not set
   */
  public String getLinkName()
  {
    if ( linkName.length() == 0 )
    {
      return( null );
    }
    else
    {
      return linkName;
    }
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param linkName the linkName to set
   */
  public void setLinkName( String linkName )
  {
    Matcher	m	= linkNamePattern.matcher( linkName );

    if ( m.matches() )
    {
      this.linkName	= linkName;
    }
    else
    {
      throw new IllegalArgumentException( "Invalid link name in setLinkName" );
    }
  }
}
