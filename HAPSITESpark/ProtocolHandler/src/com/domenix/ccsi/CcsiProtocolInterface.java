/*
 * CcsiProtocolInterface.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Interface to define NIO selector action handlers.
 *
 * Version: V1.0  15/08/01
 */
package com.domenix.ccsi;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

/**
 * Interface to define NIO selector action handlers.
 * 
 * @author kmiller
 */
public interface CcsiProtocolInterface
{
  /**
   * Handle an accept connection selector trigger indicating that a server socket is ready to
   * accept another connection or has an error pending.
   * 
   * @param key SelectionKey for the accept action
   * @return the SocketChannel of the accepted connection
   * 
   * @throws IOException - an IO Exception occurred
   */
  SocketChannel handleAccept( SelectionKey key ) throws IOException;
  
  /**
   * Handle a read ready selector trigger indicating that a channel is ready for reading, has
   * reached the end of stream, or has an error pending.
   * 
   * @param key SelectionKey for the read action
   * @param buf the buffer for reading
   * 
   * @return count of the bytes read where 0 = none, -1 = socket closed , &gt; 0 = byte count
   * 
   * @throws IOException 
   */
  int handleRead( SelectionKey key , ByteBuffer buf ) throws IOException;
  
  /**
   * Handle a write ready selector trigger indicating that a channel is ready for writing, has
   * been closed for writing, or has an error pending.
   * 
   * @param key SelectionKey for the write action
   * 
   * @throws IOException 
   */
  void handleWrite( SelectionKey key ) throws IOException;
  
  /**
   * Handle a connect ready selector trigger indicating that a channel is
   * ready to complete its connection sequence or has an error pending.
   * 
   * @param key SelectionKey for the connect action
   * 
   * @return the connected socket channel or null
   */
  SocketChannel handleConnect( SelectionKey key );
}
