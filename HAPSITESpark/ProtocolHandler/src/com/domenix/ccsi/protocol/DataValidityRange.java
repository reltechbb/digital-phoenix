/*
 * DataValidityRange.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements the numeric range data validity type.
 *
 * @author kmiller
 */
public class DataValidityRange implements DataValidityInterface
{
  /** Integer range high value. */
  private long	highLong	= 0L;

  /** Floating point range high value. */
  private double	highValue	= 0.0;

  /** Integer range low value. */
  private long	lowLong	= 0L;

  /** Floating point range low value. */
  private double	lowValue	= 0.0;

  /** Pattern of a range value. */
  Pattern	rangePat	= Pattern.compile( "([\\-+]?[0-9]+(\\.[0-9]+)?)((\\s+)?-(\\s+)?)([\\-+]?[0-9]+(\\.[0-9]+)?)" );

  /** Flag indicating integer or floating point type range. */
  private boolean	integerRange	= false;

  /** Have we been initialized. */
  private boolean	initialized	= false;

  /** Type of validity definition. */
  private final DataValidityType	theType	= DataValidityType.RANGE;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an empty range.
   */
  public DataValidityRange()
  {
  }

  /**
   * Constructs a range from a string that must contain a defined low and high value.
   *
   * @param def String containing the range definition
   *
   * @throws IllegalArgumentException
   */
  public DataValidityRange( String def ) throws IllegalArgumentException
  {
    boolean	validRange	= false;

    if ( ( def != null ) && ( def.trim().length() >= 3 ) && ( def.trim().length() <= 38 ) )
    {
      Matcher	rangeMatch	= rangePat.matcher( def.trim() );
      boolean	found				= rangeMatch.matches();

      if ( found )
      {
        String	group	= rangeMatch.group();

        //
        // Make sure the entire input string matched the pattern
        //
        if ( group.equalsIgnoreCase( def.trim() ) )
        {
          boolean	foundFrac	= false;
          String	value1		= null ,    // Low range value string
									value2		= null;     // High range value string
          int			grpCnt		= rangeMatch.groupCount();

          for ( int i = 1 ; i < grpCnt ; i++ )
          {
            String	work	= rangeMatch.group( i );

            if ( work == null )
            {
              //
            }
            else if ( work.trim().length() == 0 )
            {
              //
            }
            else if ( work.startsWith( "." ) )
            {
              foundFrac	= true;
            }
            else
            {
              if ( value1 == null )
              {
                value1	= work;
              }
              else
              {
                value2	= work;
              }
            }
          }

          if ( ( value1 != null ) && ( value2 != null ) )
          {
            try
            {
              if ( foundFrac )
              {
                integerRange	= false;
                lowValue			= Double.parseDouble( value1 );
                highValue			= Double.parseDouble( value2 );
              }
              else
              {
                integerRange	= true;
                lowLong				= Long.parseLong( value1 );
                highLong			= Long.parseLong( value2 );
              }

              validRange	= true;
            }
            catch ( NumberFormatException ex )
            {
              validRange	= false;
            }
          }
        }
        else
        {
          validRange	= false;
        }
      }
    }

    if ( !validRange )
    {
      throw new IllegalArgumentException( "Invalid range definition string passed to constructor." );
    }
    else
    {
      this.initialized	= checkInitialization();
    }
  }

  /**
   * Constructs an instance using floating point type ranges.  The relationship
   * ( low &le; high ) must be true.
   *
   * @param high double containing the high range value
   * @param low double containing the low range value
   *
   * @throws IllegalArgumentException
   */
  public DataValidityRange( double high , double low ) throws IllegalArgumentException
  {
    if ( low <= high )
    {
      integerRange	= false;
      initialized		= true;
      highValue			= high;
      lowValue			= low;
    }
    else
    {
      throw new IllegalArgumentException( "Invalid floating point range in constructor, low must be <= high" );
    }
  }

  /**
   * Constructs an instance using integer type values.  The relationship
   * (low &le; high) must be true.
   *
   * @param high long containing the high range value
   * @param low long containing the low range value
   *
   * @throws IllegalArgumentException
   */
  public DataValidityRange( long high , long low ) throws IllegalArgumentException
  {
    if ( low <= high )
    {
      integerRange	= true;
      initialized		= true;
      highLong			= high;
      lowLong				= low;
    }
    else
    {
      throw new IllegalArgumentException( "Invalid integer range in constructor, low must be <= high" );
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the CCSI data validity type.
   *
   * @return DataValidityType which will be RANGE
   */
  @Override
  public DataValidityType getTheType()
  {
    return ( this.theType );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Validates the provided value against the range and indicates whether the value is
   * valid or invalid.  The value is valid if it falls within the range end points
   * (low &le; value &le; high) or if the range is not defined.
   *
   * @param value String containing the value to be validated
   *
   * @return boolean indication of valid (true) or invalid (false)
   */
  @Override
  public boolean validateValue( String value )
  {
    boolean	retValue	= false;

    try
    {
      if ( ( initialized ) && ( value != null ) && ( value.trim().length() > 0 ) )
      {
        if ( integerRange )
        {
          long	workVal	= Long.parseLong( value.trim() );

          if ( ( workVal >= lowLong ) && ( workVal <= highLong ) )
          {
            retValue	= true;
          }
        }
        else
        {
          double	workVal	= Double.parseDouble( value.trim() );

          if ( ( workVal >= lowValue ) && ( workVal <= highValue ) )
          {
            retValue	= true;
          }
        }
      }
      else if ( !initialized )
      {
        retValue	= true;
      }
    }
    catch ( NumberFormatException ex )
    {
      retValue	= false;

      // No propagation of data conversion exceptions
    }

    return ( retValue );
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param highValue the highValue to set
   */
  public void setHighValue( double highValue )
  {
    if ( !this.initialized )
    {
      this.highValue		= highValue;
      this.initialized	= checkInitialization();
      this.integerRange	= false;
    }
  }

  /**
   * @param lowValue the lowValue to set
   */
  public void setLowValue( double lowValue )
  {
    if ( ( highLong == 0L ) && ( lowLong == 0L ) )
    {
      this.lowValue			= lowValue;
      this.initialized	= checkInitialization();
      this.integerRange	= false;
    }
  }

  /**
   * @param highLong the highLong to set
   */
  public void setHighLong( long highLong )
  {
    if ( ( highValue == 0.0 ) && ( lowValue == 0.0 ) )
    {
      this.highLong			= highLong;
      this.initialized	= checkInitialization();
      this.integerRange	= true;
    }
  }

  /**
   * @param lowLong the lowLong to set
   */
  public void setLowLong( long lowLong )
  {
    if ( ( highValue == 0.0 ) && ( lowValue == 0.0 ) )
    {
      this.lowLong			= lowLong;
      this.initialized	= checkInitialization();
      this.integerRange	= true;
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Checks to see that the range is properly defined.  This means that the low value
   * must be &le; the high value and one of the values must be non-zero.
   *
   * @return
   */
  private boolean checkInitialization()
  {
    boolean	retValue	= false;

    if ( this.integerRange )
    {
      // Something has to be non-zero
      if ( ( this.lowLong != 0L ) || ( this.highLong != 0L ) )
      {
        if ( this.lowLong <= this.highLong )
        {
          retValue			= true;
          this.lowValue	= this.highValue = 0.0;
        }
      }
    }
    else
    {
      // Something has to be non-zero
      if ( ( this.lowValue != 0.0 ) || ( this.highValue != 0.0 ) )
      {
        if ( this.lowValue <= this.highValue )
        {
          retValue			= true;
          this.lowLong	= this.highLong = 0L;
        }
      }
    }

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the description of what is valid using this validator.
   *
   * @return String containing the description
   */
  @Override
  public String getValidityString()
  {
    StringBuilder	msg	= new StringBuilder( " value must be >= " );

    if ( this.integerRange )
    {
      msg.append( this.lowLong );
      msg.append( " and <= " );
      msg.append( this.highLong );
    }
    else
    {
      msg.append( this.lowValue );
      msg.append( " and <= " );
      msg.append( this.highValue );
    }

    return ( msg.toString() );
  }
}
