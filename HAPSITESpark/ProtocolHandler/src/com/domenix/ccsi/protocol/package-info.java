/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * This package provides CCSI protocol handling related types for CCSI V1.1.1 implementations for sensors.
 *
 */
package com.domenix.ccsi.protocol;