/*
 * SensorAutoRegisterItem.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of the interface that must be implemented by all sensor
 * adapter writers.
 *
 */
package com.domenix.ccsi.protocol;

import org.apache.log4j.Logger;

/**
 * Encapsulates a SensorAutoRegisterItem
 * @author jmerritt
 */
public class SensorAutoRegisterItem
{
    private String channel;
    private String type;
    private int time;
    private boolean details;
    
    private final Logger logger  = Logger.getLogger(SensorAutoRegisterItem.class);
    
    /**
     * Constuctor
     * @param values - comma separated list of 4 values
     */
    public SensorAutoRegisterItem (String values)
    {
        String [] valueList = values.split("\\,");
        if (valueList.length == 4)
        {
            channel = valueList [0];
            type = valueList [1];
            time = Integer.parseInt(valueList[2]);
            details = Boolean.valueOf(valueList [3]);
        } else
        {
            logger.error ("Incorrect number of values for SensorAutoRegisterItem: " 
                    + values);
        }
    }
    
    public void setChannel (String channel)
    {
        this.channel = channel;
    }
    
    public String getChannel ()
    {
        return channel;
    }
    
    public void setType (String type)
    {
        this.type = type;
    }
    
    public String getType ()
    {
        return type;
    }
    
    public void setTime (int time)
    {
        this.time = time;
    }
    
    public int getTime ()
    {
        return time;
    }
    
    public void setDetails (boolean details)
    {
        this.details = details;
    }
    
    public boolean getDetails ()
    {
        return details;
    }
}
