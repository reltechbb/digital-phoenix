/*
 * PendingSensorResponse.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/16
 */


package com.domenix.ccsi.protocol;

/**
 * This class encapsulates a pending response from the sensor to a CCSI command.
 * 
 * @author kmiller
 */
public class PendingSensorResponse
{
  /** The timestamp of when the entry was created. */
  private final long	timeQueued	= System.currentTimeMillis();

  /** The routing key expected to be received */
  private String	routing;

  //~--- constructors ---------------------------------------------------------

  /**
   * An empty event
   */
  public PendingSensorResponse()
  {
    this.routing	= "";
  }

  /**
   * Constructs an entry with a routing name
   *
   * @param name the routing key name
   */
  public PendingSensorResponse( String name )
  {
    this.routing	= name;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the cmdName
   */
  public String getRouting()
  {
    return routing;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param routing the cmdName to set
   */
  public void setRouting( String routing )
  {
    this.routing	= routing;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the timeQueued
   */
  public long getTimeQueued()
  {
    return timeQueued;
  }
}
