/*
 * PendingBitList
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This class implements a list of pending BIT events to be reported to the host
 *
 */
package com.domenix.ccsi.protocol;

import com.domenix.common.spark.data.BITEventMsg;
import java.util.concurrent.LinkedBlockingQueue;
import org.apache.log4j.Logger;

/**
 * This class implements a list of pending BIT events to be reported to the host
 * 
 * @author kmiller
 */
public class PendingBitList
{
  /** Error/Debug logger */
  private Logger	myLogger	= null;

  /** The queue of readings */
  private final LinkedBlockingQueue<BITEventMsg>	theList	= new LinkedBlockingQueue<>();

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   */
  public PendingBitList()
  {
    myLogger	= Logger.getLogger( PendingBitList.class );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns indication of whether or not the list is empty
   *
   * @return flag indicating empty or not empty
   */
  public boolean isEmpty()
  {
    return ( theList.isEmpty() );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Adds a reading to the tail of the list
   *
   * @param msg
   */
  public void addBit( BITEventMsg msg )
  {
    try
    {
      this.theList.put( msg );
    }
    catch ( InterruptedException ignored )
    {
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns all of the list contents as an array and clears the list.
   *
   * @return Object[]
   */
  public Object[] getAllEntries()
  {
    Object[] ret = this.theList.toArray();
    this.theList.clear();
    return ( ret );
  }
}
