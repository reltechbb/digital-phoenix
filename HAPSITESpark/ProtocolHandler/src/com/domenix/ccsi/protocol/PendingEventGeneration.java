/*
 * PendingEventGeneration.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/12
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.commonha.ccsi.CcsiChannelEnum;
import java.util.ArrayList;

import org.apache.log4j.Logger;

//~--- classes ----------------------------------------------------------------

/**
 * This class encapsulates a single event reporting need for data to the host.
 * 
 * @author kmiller
 */
public class PendingEventGeneration
{
  /** The event channel */
  private CcsiChannelEnum	eventChannel	= null;
  
  /** Array of strings naming specifics that changed */
  private final ArrayList<String> items = new ArrayList<>();

  /** Error/Debug logger */
  private final Logger	myLogger	= Logger.getLogger( PendingEventGeneration.class );

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance indicating which channel should be processed for
   * event handling.
   *
   * @param channel the channel to be processed
   */
  public PendingEventGeneration( CcsiChannelEnum channel )
  {
    this.eventChannel	= channel;
    myLogger.debug( "Created new pending event generation entry" );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the eventChannel
   */
  public CcsiChannelEnum getEventChannel()
  {
    return eventChannel;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param eventChannel the eventChannel to set
   */
  public void setEventChannel( CcsiChannelEnum eventChannel )
  {
    this.eventChannel	= eventChannel;
  }

  /**
   * @return the items
   */
  public ArrayList<String> getItems()
  {
    return items;
  }
  
  /**
   * @param item the item to be added to the list
   */
  public void addItem( String item )
  {
    this.items.add( item );
  }
}
