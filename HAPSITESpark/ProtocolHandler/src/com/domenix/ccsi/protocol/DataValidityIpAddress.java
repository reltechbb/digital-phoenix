/*
 * DataValidityIpAddress.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//~--- classes ----------------------------------------------------------------

/**
 * This class validates a numeric IP address as IPv4 or IPv6.
 *
 * @author kmiller
 */
public class DataValidityIpAddress implements DataValidityInterface
{
  /** IPv4 Pattern */
  private final Pattern	ipV4Pattern	=
    Pattern.compile( "(([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])[.]){3}([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])" );

  /** IPv6 Pattern */
  private final Pattern	ipV6Pattern	= Pattern.compile( "(:{0,2})([0-9a-fA-F]{4}:){0,7}([0-9a-fA-F]){4}" );

  /** Data validity check type */
  private final DataValidityType	theType	= DataValidityType.IPADDRESS;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance.
   */
  public DataValidityIpAddress()
  {
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the data validity check type.
   *
   * @return DataValidityType
   */
  @Override
  public DataValidityType getTheType()
  {
    return ( this.theType );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Validates the input string and return a valid/invalid indication.
   *
   * @param value String containing the value to be validated
   *
   * @return boolean valid (true) or invalid (false)
   */
  @Override
  public boolean validateValue( String value )
  {
    boolean	retValue	= false;
    Matcher	v4				= ipV4Pattern.matcher( value );
    Matcher	v6				= ipV6Pattern.matcher( value );

    if ( v4.matches() )
    {
      retValue	= true;
    }
    else if ( v6.matches() )
    {
      retValue	= true;
    }

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the description of the validator.
   *
   * @return String containing the valid element description
   */
  @Override
  public String getValidityString()
  {
    return ( " must be a valid numeric IPv4 or IPv6 address." );
  }
}
