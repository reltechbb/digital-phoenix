/*
 * DataValidityMaxLength.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/12/11
 */


package com.domenix.ccsi.protocol;

/**
 * This class implements a maximum string length data validity type.  The specified
 * length must be less than 0.
 *
 * @author kmiller
 */
public class DataValidityMaxLength implements DataValidityInterface
{
  /** The type of validity check. */
  private final DataValidityType	theType	= DataValidityType.MAXSTRINGLENGTH;

  /** The minimum valid string length. */
  private int	maxLength	= Short.MIN_VALUE;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an empty instance.
   */
  public DataValidityMaxLength()
  {
  }

  /**
   * Constructs an instance of the class with a maximum value.
   *
   * @param max int containing the maximum valid string length
   *
   * @throws IllegalArgumentException
   */
  public DataValidityMaxLength( int max ) throws IllegalArgumentException
  {
    if ( max > 0 )
    {
      this.maxLength	= max;
    }
    else
    {
      throw new IllegalArgumentException( "Maximum string length must be > 0" );
    }
  }

  /**
   * Constructs an instance of the class with a maximum value.
   *
   *
   * @param max
   *
   * @throws IllegalArgumentException
   * @throws NumberFormatException
   */
  public DataValidityMaxLength( String max ) throws IllegalArgumentException , NumberFormatException
  {
    if ( ( max != null ) && ( max.trim().length() > 0 ) )
    {
      this.maxLength	= Short.parseShort( max.trim() );
    }
    else
    {
      throw new IllegalArgumentException( "Maximum string length must be > 0" );
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the validity check type of this class.
   *
   * @return DataValidityType which will be MAXSTRINGLENGTH
   */
  @Override
  public DataValidityType getTheType()
  {
    return ( this.theType );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Validates an input string and returns an indication if it meets the maximum
   * length requirement.  The value is considered valid if it is not null, and if
   * the length, including any spaces, is less than or equal to the maximum required length.  If the
   * value is not set the validation will also be true.
   *
   * @param value String whose length is to be validated
   *
   * @return boolean indication of valid (true) or invalid (false)
   */
  @Override
  public boolean validateValue( String value )
  {
    boolean	retValue	= true;

    if ( this.maxLength > 0 )
    {
      if ( ( value == null ) || ( value.length() > (int) this.maxLength ) )
      {
        retValue	= false;
      }
    }

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the description of what is valid using this validator.
   *
   * @return String containing the description
   */
  @Override
  public String getValidityString()
  {
    StringBuilder	msg	= new StringBuilder( " length must be <= " );

    msg.append( this.maxLength );

    return ( msg.toString() );
  }
}
