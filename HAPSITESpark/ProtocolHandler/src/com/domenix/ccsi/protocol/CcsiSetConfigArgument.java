/*
 * CcsiSetConfigArgument.java
 *
 * Copyright (c) 2015 Domenix Corp
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Version: V1.0  15/09/10
 */
package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.ccsi.CcsiConfigBlockEnum;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------
import java.net.InetAddress;
import java.net.UnknownHostException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//~--- classes ----------------------------------------------------------------
/**
 * This class encapsulates a single set configuration command argument and
 * provides a function to validate that the argument is valid. A list of NAK
 * details for each validation is maintained to be used if desired. This list
 * includes the reason and text and the caller must supply the connection ID,
 * dtg, and msn.
 *
 * @author kmiller
 */
public class CcsiSetConfigArgument
{

    /**
     * The connection ID of our host
     */
    private long connId = 0L;

    /**
     * The DTG of the received command.
     */
    private long rcvdDtg = -1L;

    /**
     * The MSN of the received command
     */
    private long rcvdMsn = -1L;

    /**
     * Sensing component name pattern
     */
    private final Pattern scNamePattern = Pattern.compile("SC([0-9]{3})");

    /**
     * Pattern for a link name type
     */
    private final Pattern linkNamePattern = Pattern.compile("[UWRF]([0-9]{3})");

    /**
     * The item's block
     */
    private CcsiConfigBlockEnum itemBlock;

    /**
     * The block key
     */
    private String itemKey;

    /**
     * The item name
     */
    private String itemName;

    /**
     * The item value
     */
    private String itemValue;

    /**
     * Error/Debug logger
     */
    private final Logger myLogger;

    /**
     * NAK details list
     */
    private PendingNakDetailList nakDetails;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs an empty class instance
     *
     * @param connId - connection ID
     * @param dtg - date time
     * @param msn - message serial number
     * @param naks - list of pending Naks
     */
    public CcsiSetConfigArgument(long connId, long dtg, long msn, PendingNakDetailList naks)
    {
        this.itemName = null;
        this.itemKey = null;
        this.itemValue = null;
        this.itemBlock = null;
        this.connId = connId;
        this.rcvdDtg = dtg;
        this.rcvdMsn = msn;
        this.nakDetails = naks;
        this.myLogger = Logger.getLogger(CcsiSetConfigArgument.class);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the itemName
     */
    public String getItemName()
    {
        return itemName;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param itemName the itemName to set
     */
    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the itemBlock
     */
    public CcsiConfigBlockEnum getItemBlock()
    {
        return itemBlock;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param itemBlock the itemBlock to set
     */
    public void setItemBlock(CcsiConfigBlockEnum itemBlock)
    {
        this.itemBlock = itemBlock;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the itemKey
     */
    public String getItemKey()
    {
        return itemKey;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param itemKey the itemKey to set
     */
    public void setItemKey(String itemKey)
    {
        this.itemKey = itemKey;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the itemValue
     */
    public String getItemValue()
    {
        return itemValue;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param itemValue the itemValue to set
     */
    public void setItemValue(String itemValue)
    {
        this.itemValue = itemValue;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Perform a validity check for this argument.
     *
     * @return flag indicating valid (true) or invalid (false)
     */
    public boolean isValid()
    {
        boolean retVal = false;

        if ((itemName != null) && (itemValue != null)
                && (!itemValue.trim().isEmpty()))
        {
            switch (itemName)
            {
                // BASE block
                case "EncryptionType":
                    retVal = isBlockOK(CcsiConfigBlockEnum.BASE);
                    break;
                // COMM block
                case "HeartbeatRate":
                    myLogger.error("HeartbeatRate: Use Set_Heartbeat command");
                    nakDetails.addEntry(new PendingNakDetailEntry(connId,
                            rcvdDtg, rcvdMsn,
                            NakDetailCodeEnum.INVALID_ARGUMENT));
                    break;
                case "HostNetworkType":
                    if ((isBlockOK(CcsiConfigBlockEnum.COMM)))
                    {
                        if ((itemValue.trim().equalsIgnoreCase("NCES"))
                                || (itemValue.trim().equalsIgnoreCase("Open"))
                                || (itemValue.trim().equalsIgnoreCase("None")))
                        {
                            retVal = true;
                        } else
                        {
                            myLogger.error("Invalid host network type: " + this.itemValue);
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } else
                    {
                        myLogger.error("Missing host network type: " + itemValue);
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.MISSING_ARGUMENT));
                    }
                    break;
                case "OpenInterfacePort":
                    if ((isBlockOK(CcsiConfigBlockEnum.COMM)))
                    {
                        try
                        {
                            int oiPort = Integer.parseInt(this.itemValue.trim());

                            if ((oiPort > 1024) && (oiPort < 65536))
                            {
                                retVal = true;
                            } else
                            {
                                myLogger.error("Invalid OI port: " + itemValue.trim());
                                nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                        NakDetailCodeEnum.INVALID_ARGUMENT));
                            }
                        } catch (NumberFormatException ex)
                        {
                            myLogger.error("Exception validating OI port: " + itemValue, ex);
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } else
                    {
                        myLogger.error("Missing OI port");
                        this.nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.MISSING_ARGUMENT));
                    }

                    break;
                case "OpenInterfaceServer":
                    if ((isBlockOK(CcsiConfigBlockEnum.COMM)
                            && (itemValue.trim().equalsIgnoreCase("true"))
                            || (itemValue.trim().equalsIgnoreCase("false"))))

                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("Invalid boolean value for OI server: " + this.itemValue.trim());
                    }

                    break;

                case "AckTimeout":
                    try
                    {
                        int intArg = Integer.parseInt(itemValue.trim());

                        if ((isBlockOK(CcsiConfigBlockEnum.COMM))
                                && (intArg > 0) && (intArg <= 60))
                        {
                            retVal = true;
                        } else
                        {
                            myLogger.error("Invalid ACK timeout: " + itemValue.trim());
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } catch (Exception ex)
                    {
                        myLogger.error("Exception validating ACK timeout: " + itemValue, ex);
                        this.nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }

                    break;

                case "ConnectionLimit":
                    try
                    {
                        int intArg = Integer.parseInt(this.itemValue.trim());

                        if ((isBlockOK(CcsiConfigBlockEnum.COMM))
                                && (intArg >= 0) && (intArg <= 100))
                        {
                            retVal = true;
                        } else
                        {
                            myLogger.error("Invalid connection limite: " + itemValue.trim());
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } catch (Exception ex)
                    {
                        myLogger.error("Exception validating connection limit: " + itemValue, ex);
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }

                    break;

                case "ConnRetryTimeout":
                    try
                    {
                        int intArg = Integer.parseInt(this.itemValue.trim());

                        if ((isBlockOK(CcsiConfigBlockEnum.COMM))
                                && (intArg >= 0) && (intArg <= 100))
                        {
                            retVal = true;
                        } else
                        {
                            myLogger.error("Invalid connection retry timeout: " + itemValue.trim());
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } catch (Exception ex)
                    {
                        myLogger.error("Exception validating connection limit: " + itemValue, ex);
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }

                    break;

                // GENERAL block
                case "TimeSource":
                case "LocationSource":
                case "SelectedTimeSource":
                case "SelectedLocationSource":
                    myLogger.error("Unsupported set config item: use the appropriate command instead");
                    nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg,
                            rcvdMsn, NakDetailCodeEnum.UNSUPPORTED_SET_ITEM));
                    break;

                case "UnitMode":
                    if ((isBlockOK(CcsiConfigBlockEnum.GENERAL)))
                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("Unsupported set config item: use the command instead");
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg,
                                rcvdMsn, NakDetailCodeEnum.INVALID_ARGUMENT));
                    }
                    break;
                    
                case "LocationReportOption":
                case "DismountedPowerWarningLevel":
                case "DismountedPowerShutdownLevel":
                    myLogger.error("Unsupported set config item");
                    nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg,
                            rcvdMsn, NakDetailCodeEnum.UNSUPPORTED_SET_ITEM));
                    break;
                // LOCAL block
                case "HostSensorId":
                case "HostSensorName":
                case "HostSensorMount":
                case "HostSensorDescription":
                    if ((isBlockOK(CcsiConfigBlockEnum.LOCAL)))
                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("Invalid argument.");
                        this.nakDetails.addEntry(new PendingNakDetailEntry(this.connId, this.rcvdDtg, this.rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }
                    break;
                // SENSDESC block
                case "ComponentType":
                case "Present":
                case "HardwareVersion":
                case "SoftwareVersion":
                case "BitPass":
                    myLogger.error("Unsupported set config item");
                    nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg,
                            rcvdMsn, NakDetailCodeEnum.UNSUPPORTED_SET_ITEM));
                    break;

                case "SerialNumber":
                    if ((isBlockOK(CcsiConfigBlockEnum.LOCAL)))
                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("Invalid argument.");
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }
                    break;
                case "AccessRole":
                    // AccessRole requires a BLOCK since the same tag is used twice
                    if ((itemBlock != null) && 
                            ((itemBlock == CcsiConfigBlockEnum.LINKS) || 
                            (itemBlock == CcsiConfigBlockEnum.SENSDESC)))
                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("BLOCK is requires for AccessRole.");
                        nakDetails.addEntry(new PendingNakDetailEntry(this.connId, this.rcvdDtg, this.rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }
                    break;
                case "AnnunciationType":
                    myLogger.error("Unsupported set config item");
                    nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg,
                            rcvdMsn, NakDetailCodeEnum.UNSUPPORTED_SET_ITEM));
                    break;
                // LINKS block
                case "LinkName":
                    if ((isBlockOK(CcsiConfigBlockEnum.LINKS))
                            && (validateKey(linkNamePattern)))
                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("Invalid argument.");
                        this.nakDetails.addEntry(new PendingNakDetailEntry(this.connId, this.rcvdDtg, this.rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }
                    break;
                case "LinkNetType":
                case "LinkBandwidthHigh":
                case "LinkEncryption":
                case "LinkCompression":
                    if ((isBlockOK(CcsiConfigBlockEnum.LINKS)))
                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("Invalid argument.");
                        this.nakDetails.addEntry(new PendingNakDetailEntry(this.connId, this.rcvdDtg, this.rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }
                    break;

                case "LinkCompressionThreshold":
                    try
                    {
                        int intArg = Integer.parseInt(this.itemValue.trim());

                        if ((isBlockOK(CcsiConfigBlockEnum.LINKS))
                                && (intArg >= 0) && (intArg <= 10000))
                        {
                            retVal = true;
                        } else
                        {
                            myLogger.error("Invalid connection retry timeout: " + itemValue.trim());
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } catch (Exception ex)
                    {
                        myLogger.error("Exception validating connection limit: " + itemValue, ex);
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }

                    break;

                // This case validated under the LOCAL block
                // case "AccessRole":
                case "ModifyRole":
                    if ((isBlockOK(CcsiConfigBlockEnum.LINKS)))
                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("Invalid argument.");
                        this.nakDetails.addEntry(new PendingNakDetailEntry(this.connId, this.rcvdDtg, this.rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }
                    break;
                case "IpAddress":
                    if ((isBlockOK(CcsiConfigBlockEnum.LINKS)))
                    {
                        try
                        {
                            InetAddress test = InetAddress.getByName(itemValue.trim());

                            retVal = true;
                        } catch (UnknownHostException ex)
                        {
                            myLogger.error("Invalid IP address: " + itemValue, ex);
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } else
                    {
                        myLogger.error("Missing argument value.");
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.MISSING_ARGUMENT));
                    }

                    break;

                case "NetMask":
                    if ((isBlockOK(CcsiConfigBlockEnum.LINKS)))
                    {
                        retVal = true;
                    } else if ((itemValue != null) && (itemValue.trim().isEmpty()))
                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("The netmask cannot be blank.");
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }

                    break;

                case "GatewayAddress":
                    if ((isBlockOK(CcsiConfigBlockEnum.LINKS)))
                    {
                        try
                        {
                            InetAddress test = InetAddress.getByName(itemValue.trim());

                            retVal = true;
                        } catch (UnknownHostException ex)
                        {
                            myLogger.error("Invalid Gateway IP address: " + itemValue, ex);
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } else if ((itemValue != null) && (itemValue.trim().isEmpty()))
                    {
                        retVal = true;
                    }

                    break;

                case "IpAddressFixed":
                    if ((isBlockOK(CcsiConfigBlockEnum.LINKS))
                            && (itemValue.trim().equalsIgnoreCase("true"))
                            || (itemValue.trim().equalsIgnoreCase("false")))
                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("Invalid boolean value for IP fixed: " + itemValue.trim());
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }

                    break;
                case "MaxTransmitSize":
                    try
                    {
                        int intArg = Integer.parseInt(this.itemValue.trim());

                        if ((isBlockOK(CcsiConfigBlockEnum.LINKS))
                                && (intArg >= 0) && (intArg <= 32767))
                        {
                            retVal = true;
                        } else
                        {
                            myLogger.error("Invalid MaxTransmitSize: " + itemValue.trim());
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } catch (Exception ex)
                    {
                        myLogger.error("Exception validating connection limit: " + itemValue, ex);
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }

                    break;
                case "LinkPortNbr":
                    try
                    {
                        long longArg = Long.parseLong(itemValue.trim());

                        if ((isBlockOK(CcsiConfigBlockEnum.LINKS))
                                && (longArg >= 0) && (longArg <= 65535))
                        {
                            retVal = true;
                        } else
                        {
                            myLogger.error("Invalid MaxTransmitSize: " + itemValue.trim());
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } catch (Exception ex)
                    {
                        myLogger.error("Exception validating connection limit: " + itemValue, ex);
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }

                    break;
                case "BroadcastIp":
                    if ((isBlockOK(CcsiConfigBlockEnum.LINKS))
                            && (itemValue != null) && (!itemValue.trim().isEmpty()))
                    {
                        try
                        {
                            InetAddress test = InetAddress.getByName(this.itemValue.trim());

                            retVal = true;
                        } catch (UnknownHostException ex)
                        {
                            myLogger.error("Invalid Broadcast IP address: " + itemValue, ex);
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } else if ((this.itemValue != null) && (itemValue.trim().isEmpty()))
                    {
                        retVal = true;
                    }

                    break;
                case "EmconMode":
                    if ((isBlockOK(CcsiConfigBlockEnum.LINKS)) && (itemValue != null))
                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("Invalid argument.");
                        this.nakDetails.addEntry(new PendingNakDetailEntry(this.connId, this.rcvdDtg, this.rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }
                    break;
                case "LinkSpeed":
                    try
                    {
                        long longArg = Long.parseLong(itemValue.trim());

                        if ((isBlockOK(CcsiConfigBlockEnum.LINKS))
                                && (longArg >= 0))
                        {
                            retVal = true;
                        } else
                        {
                            myLogger.error("Invalid MaxTransmitSize: " + itemValue.trim());
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } catch (Exception ex)
                    {
                        myLogger.error("Exception validating connection limit: " + itemValue, ex);
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }

                    break;
                // ANNUN block
                case "AudibleAnnunState":
                case "VisualAnnunState":
                case "TactileAnnunState":
                case "LedAnnunState":
                case "ExternalAnnunState":
                    if ((isBlockOK(CcsiConfigBlockEnum.LOCAL)))
                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("Invalid argument.");
                        this.nakDetails.addEntry(new PendingNakDetailEntry(this.connId, this.rcvdDtg, this.rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }
                    break;
                // UNIQUE block
                // handled by default case
                // case "UniqueName":
                case "OperatorName":
                case "OperatorOperatorRoles":
                case "OperatorLastLogin":
                case "OperatorFailureCount":
                case "OperatorLastPwChange":
                    myLogger.error("Unsupported set config item:" + itemName);
                    nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg,
                            rcvdMsn, NakDetailCodeEnum.UNSUPPORTED_SET_ITEM));
                    break;
                // SECURITY block
                case "EncryptComm":
                case "EncryptData":
                case "HostAuthentication":
                case "OperatorAuthentication":
                    if ((isBlockOK(CcsiConfigBlockEnum.SECURITY)) && (validateBoolean()))
                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("Invalid argument.");
                        this.nakDetails.addEntry(new PendingNakDetailEntry(this.connId, this.rcvdDtg, this.rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }
                    break;
                case "UserPwdTimeout":
                    try
                    {
                        long longArg = Long.parseLong(itemValue.trim());

                        if ((isBlockOK(CcsiConfigBlockEnum.LINKS))
                                && (longArg >= 0) && (longArg <= 31536000))
                        {
                            retVal = true;
                        } else
                        {
                            myLogger.error("Invalid MaxTransmitSize: " + itemValue.trim());
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } catch (Exception ex)
                    {
                        myLogger.error("Exception validating connection limit: " + itemValue, ex);
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }

                    break;

                case "PwReuseLength":
                    try
                    {
                        long longArg = Long.parseLong(itemValue.trim());

                        if ((isBlockOK(CcsiConfigBlockEnum.SECURITY))
                                && (longArg >= 0) && (longArg <= 99))
                        {
                            retVal = true;
                        } else
                        {
                            myLogger.error("Invalid MaxTransmitSize: " + itemValue.trim());
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } catch (Exception ex)
                    {
                        myLogger.error("Exception validating connection limit: " + itemValue, ex);
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }

                    break;
                case "LoginFailTimeout":
                case "ConnectFailTimeout":
                    try
                    {
                        long longArg = Long.parseLong(itemValue.trim());

                        if ((isBlockOK(CcsiConfigBlockEnum.SECURITY))
                                && (longArg >= 0))
                        {
                            retVal = true;
                        } else
                        {
                            myLogger.error("Invalid MaxTransmitSize: " + itemValue.trim());
                            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                    NakDetailCodeEnum.INVALID_ARGUMENT));
                        }
                    } catch (Exception ex)
                    {
                        myLogger.error("Exception validating connection limit: " + itemValue, ex);
                        nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }

                    break;

                // TAMPER block
                case "Count":
                case "Window":
                case "Action":
                case "Enable":
                    myLogger.error("Unsupported set config item:" + itemName);
                    nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg,
                            rcvdMsn, NakDetailCodeEnum.UNSUPPORTED_SET_ITEM));
                    break;

                default:
                    // Handles the UNIQUE block
                    if ((isBlockOK(CcsiConfigBlockEnum.UNIQUE)))
                    {
                        retVal = true;
                    } else
                    {
                        myLogger.error("Missing argument.");
                        this.nakDetails.addEntry(new PendingNakDetailEntry(this.connId, this.rcvdDtg, this.rcvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT));
                    }
                    break;
            }
        } else
        {
            myLogger.error("Set config name or value is missing");
            nakDetails.addEntry(new PendingNakDetailEntry(connId, rcvdDtg, rcvdMsn,
                    NakDetailCodeEnum.MISSING_ARGUMENT, "Set config name or value is missing"));
        }

        return (retVal);
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Validates the key
     *
     * @param pattern - the pattern to match
     * @return flag indicating valid (true) or invalid (false)
     */
    private boolean validateKey(Pattern pattern)
    {
        boolean retVal = true;
        Matcher keyMatcher;

        if (itemKey != null)
        {
            keyMatcher = pattern.matcher(itemKey);

            if (keyMatcher.find())
            {
                retVal = false;
            }
        }

        return (retVal);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the nakDetails
     */
    public PendingNakDetailList getNakDetails()
    {
        return nakDetails;
    }

    /**
     * @return the connId
     */
    public long getConnId()
    {
        return connId;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param connId the connId to set
     */
    public void setConnId(long connId)
    {
        this.connId = connId;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the rcvdMsn
     */
    public long getRcvdMsn()
    {
        return rcvdMsn;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param rcvdMsn the rcvdMsn to set
     */
    public void setRcvdMsn(long rcvdMsn)
    {
        this.rcvdMsn = rcvdMsn;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the rcvdDtg
     */
    public long getRcvdDtg()
    {
        return rcvdDtg;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param rcvdDtg the rcvdDtg to set
     */
    public void setRcvdDtg(long rcvdDtg)
    {
        this.rcvdDtg = rcvdDtg;
    }

    /**
     * @param nakDetails the nakDetails to set
     */
    public void setNakDetails(PendingNakDetailList nakDetails)
    {
        this.nakDetails = nakDetails;
    }

    private boolean isBlockOK(CcsiConfigBlockEnum block)
    {
        boolean returnValue = true;

        if ((block != null) && (itemBlock != null) && (itemBlock != block))
        {
            returnValue = false;
        }
        return returnValue;
    }

    private boolean validateBoolean()
    {
        boolean retVal = false;
        if ((itemValue != null) && ((itemValue.contentEquals("true")
                || itemValue.contentEquals("false"))))
        {
            retVal = true;
        }
        return retVal;
    }
}
