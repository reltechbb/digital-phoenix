/*
 * PendingFailureList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.common.spark.data.BITEventMsg;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.concurrent.LinkedBlockingQueue;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a list of pending failures to be reported.
 *
 * @author kmiller
 */
public class PendingFailureList
{
  /** Error/Debug logger */
//  private Logger	myLogger	= null;

  /** The queue of readings */
  private final LinkedBlockingQueue<BITEventMsg>	theList	= new LinkedBlockingQueue<>();

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   */
  public PendingFailureList()
  {
//    myLogger	= Logger.getLogger( PendingReadingList.class );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns indication of whether or not the list is empty
   *
   * @return flag indicating empty or not empty
   */
  public boolean isEmpty()
  {
    return ( theList.isEmpty() );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Adds a reading to the tail of the list
   *
   * @param msg
   */
  public void addReading( BITEventMsg msg )
  {
    try
    {
      this.theList.put( msg );
    }
    catch ( InterruptedException ignored )
    {
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns all of the list contents as an array and clears the list.
   *
   * @return Object[]
   */
  public Object[] getAllEntries()
  {
    Object[]	ret	= this.theList.toArray();

    this.theList.clear();

    return ( ret );
  }
}
