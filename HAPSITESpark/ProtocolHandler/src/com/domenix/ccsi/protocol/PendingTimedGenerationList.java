/*
 * PendingTimedGenerationList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/12
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a list of timed generation events.
 * 
 * @author kmiller
 */
public class PendingTimedGenerationList
{
  /** Error/Debug Logger */
  private final Logger	myLogger	= Logger.getLogger( PendingTimedGenerationList.class );

  /** The list */
  private final LinkedBlockingQueue<PendingTimedGeneration>	theList	= new LinkedBlockingQueue<>();

  /** Flag indicating if there are more events to process */
  private boolean	needsProcessing	= false;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a list instance
   */
  public PendingTimedGenerationList()
  {
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Clear the list
   */
  public void clear()
  {
    this.theList.clear();
    this.needsProcessing	= false;
  }

  /**
   * Adds a new event to the list
   *
   * @param evt the new event
   *
   * @return flag indicating the entry was added or not
   */
  public boolean addPendingTimed( PendingTimedGeneration evt )
  {
    boolean	retVal	= false;

    retVal								= this.theList.add( evt );
    this.needsProcessing	= true;

    return ( retVal );
  }

  /**
   * Returns the top list entry without removing it from the queue
   *
   * @return the first event in the list or null if none
   */
  public PendingTimedGeneration peekPendingEvent()
  {
    PendingTimedGeneration	retVal	= null;

    retVal	= this.theList.peek();

    return ( retVal );
  }

  /**
   * Removes the top entry from the list
   */
  public void removeFirstEvent()
  {
    if ( !this.theList.isEmpty() )
    {
      this.theList.remove();
    }

    this.needsProcessing	= this.theList.size() > 0;
  }

  /**
   * Reads, without a wait, the next entry from the list
   *
   * @return the item read or null if none
   */
  public PendingTimedGeneration poll()
  {
    PendingTimedGeneration	retVal	= null;

    retVal								= this.theList.poll();
    this.needsProcessing	= this.theList.size() > 0;

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns a flag indicating if the list is empty
   *
   * @return empty or not empty
   */
  public boolean isEmpty()
  {
    return ( this.theList.isEmpty() );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Polls the list for the next entry and waits up to timeout time units.
   *
   * @param timeout the number of time units to wait
   * @param units the time units
   *
   * @return the entry received or null if none
   */
  public PendingTimedGeneration poll( long timeout , TimeUnit units )
  {
    PendingTimedGeneration	retVal	= null;

    try
    {
      retVal	= this.theList.poll( timeout , units );
    }
    catch ( InterruptedException iEx )
    {
    }

    this.needsProcessing	= this.theList.size() > 0;

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the theList
   */
  public LinkedBlockingQueue<PendingTimedGeneration> getTheList()
  {
    return theList;
  }

  /**
   * @return the needsProcessing
   */
  public boolean isNeedsProcessing()
  {
    return needsProcessing;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param needsProcessing the needsProcessing to set
   */
  public void setNeedsProcessing( boolean needsProcessing )
  {
    this.needsProcessing	= needsProcessing;
  }
}
