/*
 * DataValidityRoleList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.StringTokenizer;

//~--- classes ----------------------------------------------------------------

/**
 * This class validates an XML list of CCSI roles.  It does not accept the NOS value
 * specified in the data model since a sensor cannot report that it doesn't know what
 * access to one if it's elements is.
 *
 * @author kmiller
 */
public class DataValidityRoleList implements DataValidityInterface
{
  /** List of valid values */
  private static final ArrayList<String>	valid	= new ArrayList<>();

  //~--- static initializers --------------------------------------------------

  static
  {
    valid.add( "ANY" );
    valid.add( "MAINT" );
    valid.add( "NKN" );

    // Not implemented, a sensor telling us it doesn't known is invalid   valid.add( "NOS" );
    valid.add( "OPER" );
    valid.add( "PRIV" );
    valid.add( "SECRTY" );
  }

  //~--- fields ---------------------------------------------------------------

  /** The data validity check type */
  private final DataValidityType	theType	= DataValidityType.ROLELIST;

  //~--- constructors ---------------------------------------------------------

  /**
   * Default constructor
   */
  public DataValidityRoleList()
  {
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the validation check type.
   *
   * @return the validation type
   */
  @Override
  public DataValidityType getTheType()
  {
    return ( this.theType );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Returns an indication that the provided value is valid or invalid.
   *
   * @param value the value to be checked
   *
   * @return boolean valid (true) or invalid (false)
   */
  @Override
  public boolean validateValue( String value )
  {
    StringTokenizer	tokenizer	= new StringTokenizer( value );

    while ( tokenizer.hasMoreTokens() )
    {
      if ( !valid.contains( tokenizer.nextToken() ) )
      {
        return ( false );
      }
    }

    return ( true );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the description of what is valid using this validator.
   *
   * @return String containing the description
   */
  @Override
  public String getValidityString()
  {
    StringBuilder	msg				= new StringBuilder( " must be one of: " );
    boolean				firstOne	= true;

    for ( String x : valid )
    {
      if ( !firstOne )
      {
        msg.append( " , " );
      }

      firstOne	= false;
      msg.append( x );
    }

    return ( msg.toString() );
  }
}
