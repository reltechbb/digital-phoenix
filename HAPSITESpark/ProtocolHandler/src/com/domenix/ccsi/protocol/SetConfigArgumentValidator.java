/*
 * SetConfigArgumentValidator.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/09/27
 */
package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.ccsi.CcsiConfigBlockEnum;
import com.domenix.common.config.SensorUniqueConfigItem;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//~--- classes ----------------------------------------------------------------
/**
 * This class validates Set_Config command arguments.
 *
 * @author kmiller
 */
public class SetConfigArgumentValidator
{

    /**
     * Sensing component name pattern
     */
    private final Pattern scNamePattern = Pattern.compile("SC([0-9]{3})");

    /**
     * Pattern for a link name type
     */
    private final Pattern linkNamePattern = Pattern.compile("[UWRF]([0-9]{3})");

    /**
     * Pattern for a sensing component name type
     */
    private final Pattern sensingUnitNamePattern = Pattern.compile("SC([0-9]{3})");

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger = Logger.getLogger(SetConfigArgumentValidator.class);

    /**
     * Pattern for a sensor component
     */
    private final Pattern componentTypePattern = Pattern.compile("CC|SC|PDIL|WCC|UIC|DPC");

    /**
     * Our checker
     */
    private final ConfigDataBlockList blockList;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructor
     * @param theContext
     */
    public SetConfigArgumentValidator(PHContext theContext)
    {
        blockList = new ConfigDataBlockList(theContext, false);
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Validates a set configuration argument against the validation lists.
     *
     * @param arg the argument to be validated
     *
     * @return valid (true) or not valid (false)
     */
    public boolean validateArgument(CcsiSetConfigArgument arg)
    {
        boolean retVal = false;
        ConfigDataBlockValidation itemBlock = this.blockList.getBlock(arg.getItemBlock());

        if (itemBlock != null)
        {
            ConfigDataBlockItem item = itemBlock.getItem(arg.getItemName());

            if (item != null)
            {
                retVal = item.setItemValue(arg.getItemValue());
            }
        }

        return (retVal);
    }

    //~--- inner classes --------------------------------------------------------
    /**
     * Class description
     *
     */
    class ConfigDataBlockItem
    {

        /**
         * Field description
         */
        String itemName = null;

        /**
         * Field description
         */
        boolean itemRequired = false;

        /**
         * Field description
         */
        boolean itemFound = false;

        /**
         * Field description
         */
        boolean itemValid = false;

        /**
         * Field description
         */
        DataValidityInterface itemValidator = null;

        /**
         * Field description
         */
        String itemValue = null;

        //~--- constructors -------------------------------------------------------
        /**
         * Constructs ...
         *
         *
         * @param nm
         * @param req
         * @param val
         * @param vi
         */
        public ConfigDataBlockItem(String nm, boolean req, String val, DataValidityInterface vi)
        {
            itemName = nm;
            itemRequired = req;

            if ((val != null) && (val.trim().length() > 0))
            {
                itemValue = val;
            }

            if (vi != null)
            {
                itemValidator = vi;
            }
        }

        //~--- set methods --------------------------------------------------------
        /**
         * Method description
         *
         *
         * @param vi
         */
        public void setItemValidator(DataValidityInterface vi)
        {
            itemValidator = vi;
        }

        /**
         * Method description
         *
         */
        public void setItemFound()
        {
            itemFound = true;
        }

        /**
         * Method description
         *
         *
         * @param val
         *
         * @return
         */
        public boolean setItemValue(String val)
        {
            this.itemValue = val;
            this.itemValid = this.isValid();

            return (this.itemValid);
        }

        //~--- get methods --------------------------------------------------------
        /**
         * Method description
         *
         *
         * @return
         */
        public boolean isValid()
        {
            if (this.itemValidator != null)
            {
                return (this.itemValidator.validateValue(this.itemValue));
            } else
            {
                return (true);
            }
        }
    }

    /**
     * This class encapsulates a list of configuration data block validation
     * values.
     *
     */
    class ConfigDataBlockList
    {

        /**
         * Field description
         */
        private final DataValidityEnumeration annunOptions = new DataValidityEnumeration("ABSENT", "NORMAL", "OFF",
                "SILNCD");

        /**
         * Field description
         */
        private final DataValidityRange timeoutBasic = new DataValidityRange(86400L, 0L);

        /**
         * Field description
         */
        private final DataValidityEnumeration timeSource = new DataValidityEnumeration("network", "host", "internal");

        /**
         * Field description
         */
        private final DataValidityEnumeration sensorMode = new DataValidityEnumeration("EXERCS", "TEST", "TRAIN",
                "CONFIG");

        /**
         * Field description
         */
        private final DataValidityEnumeration locationSource = new DataValidityEnumeration("manual", "host",
                "internal", "none");

        /**
         * Field description
         */
        private final DataValidityMaxLength ccsiNameType = new DataValidityMaxLength(64);

        /**
         * Field description
         */
        public ArrayList<ConfigDataBlockValidation> theBlocks;

        //~--- constructors -------------------------------------------------------
        /**
         * Constructs ...
         *
         *
         * @param full
         */
        public ConfigDataBlockList(PHContext theContext, boolean full)
        {
            buildConfigDataBlockList(theContext, full);
        }

        //~--- methods ------------------------------------------------------------
        /**
         * Constructs a class instance.
         *
         *
         * @param fullReport
         */
        private void buildConfigDataBlockList(PHContext theContext, boolean fullReport)
        {
            theBlocks = new ArrayList<>();

            //
            // Build the list of blocks
            //
            ConfigDataBlockValidation baseBlock = new ConfigDataBlockValidation(CcsiConfigBlockEnum.BASE, fullReport,
                    false);

            baseBlock.addItem(new ConfigDataBlockItem("EncryptionType", true, null,
                    new DataValidityMaxLength((short) 64)));
            theBlocks.add(baseBlock);

            ConfigDataBlockValidation annunBlock = new ConfigDataBlockValidation(CcsiConfigBlockEnum.ANNUN, false,
                    false);

            annunBlock.addItem(new ConfigDataBlockItem("AudibleAnnunState", fullReport, null, annunOptions));
            annunBlock.addItem(new ConfigDataBlockItem("VisualAnnunState", fullReport, null, annunOptions));
            annunBlock.addItem(new ConfigDataBlockItem("TactileAnnunState", fullReport, null, annunOptions));
            annunBlock.addItem(new ConfigDataBlockItem("ExternalAnnunState", fullReport, null, annunOptions));
            annunBlock.addItem(new ConfigDataBlockItem("LedAnnunState", fullReport, null, annunOptions));
            theBlocks.add(annunBlock);

            ConfigDataBlockValidation commBlock = new ConfigDataBlockValidation(CcsiConfigBlockEnum.COMM, fullReport,
                    false);

            commBlock.addItem(new ConfigDataBlockItem("HeartbeatRate", fullReport, "60",
                    new DataValidityRange(3600L, 5L)));
            commBlock.addItem(new ConfigDataBlockItem("HostNetworkType", fullReport, "Open",
                    new DataValidityEnumeration("NCES", "None", "Open")));
            commBlock.addItem(new ConfigDataBlockItem("OpenInterfacePort", fullReport, "5674",
                    new DataValidityRange(65535L, 1L)));
            commBlock.addItem(new ConfigDataBlockItem("OpenInterfaceServer", fullReport, "true",
                    new DataValidityBoolean()));
            commBlock.addItem(new ConfigDataBlockItem("AckTimeout", fullReport, "5",
                    new DataValidityRange(60L, 1L)));
            commBlock.addItem(new ConfigDataBlockItem("ConnectionLimit", fullReport, null, null));
            commBlock.addItem(new ConfigDataBlockItem("ConnectRetryTimeout", fullReport, null,
                    new DataValidityRange(999999L, 0L)));
            theBlocks.add(commBlock);

            ConfigDataBlockValidation genBlock = new ConfigDataBlockValidation(CcsiConfigBlockEnum.GENERAL, fullReport,
                    false);

            genBlock.addItem(new ConfigDataBlockItem("TimeSource", fullReport, null, timeSource));
            genBlock.addItem(new ConfigDataBlockItem("LocationSource", fullReport, null, locationSource));
            genBlock.addItem(new ConfigDataBlockItem("SelTimeSource", false, null, timeSource));
            genBlock.addItem(new ConfigDataBlockItem("SelLocationSource", false, null, locationSource));
            genBlock.addItem(new ConfigDataBlockItem("UnitMode", fullReport, null, sensorMode));
            genBlock.addItem(new ConfigDataBlockItem("LocationReportOption", false, null,
                    new DataValidityEnumeration("NONE", "ERROR", "LAST")));
            genBlock.addItem(new ConfigDataBlockItem("DismountedPowerWarningLevel", false, null,
                    new DataValidityRange(1.0, 0.0)));
            genBlock.addItem(new ConfigDataBlockItem("DismountedPowerShutdownLevel", false, null,
                    new DataValidityRange(1.0, 0.0)));
            theBlocks.add(genBlock);

            ConfigDataBlockValidation localBlock = new ConfigDataBlockValidation(CcsiConfigBlockEnum.LOCAL, fullReport,
                    false);

            localBlock.addItem(new ConfigDataBlockItem("HostSensorId", fullReport, null,
                    new DataValidityLengthRange(2, 63)));
            localBlock.addItem(new ConfigDataBlockItem("HostSensorName", fullReport, null, ccsiNameType));
            localBlock.addItem(new ConfigDataBlockItem("HostSensorMount", fullReport, null,
                    new DataValidityEnumeration("DISMTD", "EXTRNL", "INTRNL", "REMOTE")));
            localBlock.addItem(new ConfigDataBlockItem("HostSensorDescription", false, null,
                    new DataValidityMaxLength(255)));
            theBlocks.add(localBlock);

            ConfigDataBlockValidation securityBlock = new ConfigDataBlockValidation(CcsiConfigBlockEnum.SECURITY, false,
                    false);

//    if ( ( CCSI_Comm_Validator.testContext.getCtxSensorVersion().hasStdCmd( "Erase_Sec_Log" ) ) ||
//         ( CCSI_Comm_Validator.testContext.getCtxSensorVersion().hasStdCmd( "Get_Sec_Log" ) ) ||
//         ( CCSI_Comm_Validator.testContext.getCtxSensorVersion().hasStdCmd( "Set_Security_Timeout" ) ) ||
//         ( CCSI_Comm_Validator.testContext.getCtxSensorVersion().getTheChannelList().channelList.get(
//           "SEC" ).versionUses ) )
//    {
//      securityBlock.cfgRequired = fullReport;
//    }
            securityBlock.addItem(new ConfigDataBlockItem("EncryptComm", fullReport, "false",
                    new DataValidityBoolean()));
            securityBlock.addItem(new ConfigDataBlockItem("EncryptData", fullReport, "false",
                    new DataValidityBoolean()));
            securityBlock.addItem(new ConfigDataBlockItem("HostAuthentication", fullReport, "false",
                    new DataValidityBoolean()));
            securityBlock.addItem(new ConfigDataBlockItem("OperatorAuthentication", fullReport, "false",
                    new DataValidityBoolean()));
            securityBlock.addItem(new ConfigDataBlockItem("UserPwdTimeout", fullReport, null,
                    new DataValidityRange(31536000L, 0L)));
            securityBlock.addItem(new ConfigDataBlockItem("PwReuseLength", fullReport, "1",
                    new DataValidityRange(99L, 0L)));
            securityBlock.addItem(new ConfigDataBlockItem("LoginFailTimeout", fullReport, "30",
                    new DataValidityRange(999999L, 0L)));
            securityBlock.addItem(new ConfigDataBlockItem("ConnectFailTimeout", fullReport, "300",
                    new DataValidityRange(999999L, 0L)));
            theBlocks.add(securityBlock);

            ConfigDataBlockValidation tamperBlock = new ConfigDataBlockValidation(CcsiConfigBlockEnum.TAMPER, false,
                    false);

//    if ( CCSI_Comm_Validator.testContext.getCtxSensorVersion().hasStdCmd( "Set_Tamper" ) )
//    {
//      tamperBlock.cfgRequired = fullReport;
//    }
            tamperBlock.addItem(new ConfigDataBlockItem("Count", fullReport, null, new DataValidityRange(10L, 0L)));
            tamperBlock.addItem(new ConfigDataBlockItem("Window", fullReport, null, timeoutBasic));
            tamperBlock.addItem(new ConfigDataBlockItem("Action", fullReport, null,
                    new DataValidityEnumeration("ALERT", "DISABL", "RENDER")));
            tamperBlock.addItem(new ConfigDataBlockItem("Enable", fullReport, null, new DataValidityBoolean()));
            theBlocks.add(tamperBlock);

            ConfigDataBlockValidation linksBlock = new ConfigDataBlockValidation(CcsiConfigBlockEnum.LINKS, fullReport,
                    true);

            linksBlock.addItem(new ConfigDataBlockItem("LinkName", fullReport, null,
                    new DataValidityRegex("[UWRF]([0-9]{3})")));
            linksBlock.addItem(new ConfigDataBlockItem("LinkNetType", fullReport, null,
                    new DataValidityEnumeration("ARCNET", "ETHRNT", "MIXED", "TKNRNG", "WRLESS")));
            linksBlock.addItem(new ConfigDataBlockItem("LinkBandwidthHigh", fullReport, null,
                    new DataValidityBoolean()));
            linksBlock.addItem(new ConfigDataBlockItem("LinkEncryption", fullReport, null, new DataValidityBoolean()));
            linksBlock.addItem(new ConfigDataBlockItem("LinkCompression", fullReport, null,
                    new DataValidityBoolean()));
            linksBlock.addItem(new ConfigDataBlockItem("LinkCompressionThreshold", fullReport, null,
                    new DataValidityRange((Long.MAX_VALUE - 1L), 0L)));
            linksBlock.addItem(new ConfigDataBlockItem("AccessRole", fullReport, null, new DataValidityRoleList()));
            linksBlock.addItem(new ConfigDataBlockItem("ModifyRole", fullReport, null, new DataValidityRoleList()));
            linksBlock.addItem(new ConfigDataBlockItem("IpAddress", fullReport, null, new DataValidityIpAddress()));
            linksBlock.addItem(new ConfigDataBlockItem("NetMask", fullReport, null, new DataValidityNetMask()));
            linksBlock.addItem(new ConfigDataBlockItem("GatewayAddress", fullReport, null,
                    new DataValidityIpAddress()));
            linksBlock.addItem(new ConfigDataBlockItem("IpAddressFixed", fullReport, null, new DataValidityBoolean()));
            linksBlock.addItem(new ConfigDataBlockItem("MaxTransmitSize", fullReport, null,
                    new DataValidityRange((Long.MAX_VALUE - 1), 1024L)));
            linksBlock.addItem(new ConfigDataBlockItem("LinkUsageList", false, null, null));
            linksBlock.addItem(new ConfigDataBlockItem("LinkPortNbr", fullReport, null,
                    new DataValidityRange(65535L, 0L)));
            linksBlock.addItem(new ConfigDataBlockItem("BroadcastIp", fullReport, null, new DataValidityIpAddress()));
            linksBlock.addItem(new ConfigDataBlockItem("EmconMode", fullReport, null,
                    new DataValidityEnumeration("NONE", "RECEIVE", "ALERT", "OFF", "SLEEP")));
            linksBlock.addItem(new ConfigDataBlockItem("LinkSpeed", fullReport, null,
                    new DataValidityRange((Long.MAX_VALUE - 1L), 0L)));
            theBlocks.add(linksBlock);

            ConfigDataBlockValidation operatorBlock = new ConfigDataBlockValidation(CcsiConfigBlockEnum.OPERATORS, false,
                    true);

//    if ( ( CCSI_Comm_Validator.testContext.getCtxSensorVersion().hasStdCmd( "Get_Users" ) ) ||
//         ( CCSI_Comm_Validator.testContext.getCtxSensorVersion().hasStdCmd( "User_Change" ) ) )
//    {
//      operatorBlock.cfgRequired = fullReport;
//    }
            operatorBlock.addItem(new ConfigDataBlockItem("OperatorName", true, null,
                    new DataValidityMaxLength(32)));
            operatorBlock.addItem(new ConfigDataBlockItem("OperatorOperatorRoles", fullReport, null,
                    new DataValidityRoleList()));
            operatorBlock.addItem(new ConfigDataBlockItem("OperatorLastLogin", fullReport, null,
                    new DataValidityDateTime()));
            operatorBlock.addItem(new ConfigDataBlockItem("OperatorFailureCount", fullReport, null,
                    new DataValidityRange(10L, 0L)));
            operatorBlock.addItem(new ConfigDataBlockItem("OperatorLastPwChange", fullReport, null,
                    new DataValidityDateTime()));
            theBlocks.add(operatorBlock);

            ConfigDataBlockValidation uniqueBlock = new ConfigDataBlockValidation(CcsiConfigBlockEnum.UNIQUE, false,
                    false);
            List<SensorUniqueConfigItem> sud = theContext.getTheConfig().getSensorUniqueConfig();
            for (SensorUniqueConfigItem sudItem : sud)
            {
                uniqueBlock.addItem(new ConfigDataBlockItem( sudItem.getItemName() , false , null , null ));
            }
            theBlocks.add(uniqueBlock);

            ConfigDataBlockValidation sensorBlock = new ConfigDataBlockValidation(CcsiConfigBlockEnum.SENSDESC,
                    fullReport, true);

            sensorBlock.addItem(new ConfigDataBlockItem("ComponentType", fullReport, null,
                    new DataValidityEnumeration("CC", "SC", "PDIL", "WCC", "UIC", "DPC")));
            sensorBlock.addItem(new ConfigDataBlockItem("Present", fullReport, null, new DataValidityBoolean()));
            sensorBlock.addItem(new ConfigDataBlockItem("HardwareVersion", fullReport, null, null));
            sensorBlock.addItem(new ConfigDataBlockItem("SoftwareVersion", fullReport, null, null));
            sensorBlock.addItem(new ConfigDataBlockItem("BitPass", fullReport, null,
                    new DataValidityEnumeration("PASS", "FAIL")));
            sensorBlock.addItem(new ConfigDataBlockItem("SerialNumber", fullReport, null, null));
            sensorBlock.addItem(new ConfigDataBlockItem("AccessRole", fullReport, null, new DataValidityRoleList()));
            sensorBlock.addItem(new ConfigDataBlockItem("AnnunciationType", fullReport, null, null));
            theBlocks.add(sensorBlock);
        }

        /**
         * Method description
         *
         *
         * @param blk
         * @param key
         */
        public void foundBlock(CcsiConfigBlockEnum blk, String key)
        {
            for (int i = 0; i < theBlocks.size(); i++)
            {
                if (theBlocks.get(i).cfgBlock == blk)
                {
                    if (!theBlocks.get(i).blockFound(key))
                    {
//          reportError( "Block " + blk.name() + " with key " + key + " can only be sent once." ,
//                       NakReasonCode.MSGCON );
                    }
                }
            }
        }

        //~--- get methods --------------------------------------------------------
        /**
         * Method description
         *
         *
         * @param blk
         *
         * @return
         */
        public ConfigDataBlockValidation getBlock(CcsiConfigBlockEnum blk)
        {
            for (int i = 0; i < theBlocks.size(); i++)
            {
                if (theBlocks.get(i).cfgBlock == blk)
                {
                    return (theBlocks.get(i));
                }
            }

            return (null);
        }
    }

    /**
     * Class description
     *
     */
    class ConfigDataBlockValidation
    {

        /**
         * Field description
         */
        public int blockCount;

        /**
         * Field description
         */
        public boolean blockFound;

        /**
         * Field description
         */
        public CcsiConfigBlockEnum cfgBlock;

        /**
         * Field description
         */
        public HashSet<String> cfgBlockKeys;

        /**
         * Field description
         */
        public ArrayList<ConfigDataBlockItem> cfgItems;

        /**
         * Field description
         */
        public boolean cfgMultAllowed;

        /**
         * Field description
         */
        public boolean cfgRequired;

        //~--- constructors -------------------------------------------------------
        /**
         * Constructs ...
         *
         *
         * @param blk
         * @param req
         * @param mult
         */
        public ConfigDataBlockValidation(CcsiConfigBlockEnum blk, boolean req, boolean mult)
        {
            this.cfgBlock = blk;
            this.cfgRequired = req;
            this.cfgMultAllowed = mult;
            cfgItems = new ArrayList<>();
            cfgBlockKeys = new HashSet<>();
        }

        //~--- methods ------------------------------------------------------------
        /**
         * Method description
         *
         *
         *
         * @param dbi
         */
        public void addItem(ConfigDataBlockItem dbi)
        {
            for (ConfigDataBlockItem ci : this.cfgItems)
            {
//      if ( ci.itemName.equalsIgnoreCase( dbi.itemName ) )
//      {
//        myLogger.warn( "Duplicate item named " + dbi.itemName + " attempted to be added." );
//
//        return;
//      }
            }

            this.cfgItems.add(dbi);
        }

        //~--- get methods --------------------------------------------------------
        /**
         * Method description
         *
         *
         * @param name
         *
         * @return
         */
        public ConfigDataBlockItem getItem(String name)
        {
            for (ConfigDataBlockItem ci : this.cfgItems)
            {
                if (ci.itemName.equalsIgnoreCase(name))
                {
                    return (ci);
                }
            }

            return (null);
        }

        //~--- methods ------------------------------------------------------------
        /**
         * Method description
         *
         *
         * @param key
         *
         * @return
         */
        public boolean blockFound(String key)
        {
            boolean multKey = false;

            this.blockFound = true;
            this.blockCount++;

//    if ( ( this.blockCount > 1 ) && ( !this.cfgMultAllowed ) )
//    {
//      reportError( "Block " + this.cfgBlock.name() + " cannot occur multiple times." , NakReasonCode.MSGCON );
//    }
            if ((key != null) && (key.trim().length() > 0))
            {
                multKey = this.cfgBlockKeys.add(key);

                switch (this.cfgBlock)
                {
                    case LINKS:
                        Matcher m1 = linkNamePattern.matcher(key);

//          if ( ( !key.trim().equalsIgnoreCase( "<<empty>>" ) ) && ( !m1.matches() ) )
//          {
//            reportError( "Key [" + key + "] is not valid for LINKS block." , NakReasonCode.MSGCON );
//          }
                        break;

                    case SENSDESC:
                        Matcher m4 = linkNamePattern.matcher(key);
                        Matcher m2 = componentTypePattern.matcher(key);
                        Matcher m3 = sensingUnitNamePattern.matcher(key);

//          if ( ( !m4.matches() ) && ( !m2.matches() ) && ( !m3.matches() ) )
//          {
//            reportError( "Key [" + key + "] is not valid for SENSDESC block." , NakReasonCode.MSGCON );
//          }
                        break;

                    default:
                        break;
                }
            } else
            {
                multKey = this.cfgBlockKeys.add("<<empty>>");
            }

            for (ConfigDataBlockItem ci : this.cfgItems)
            {
                ci.itemFound = false;
            }

            return (multKey);
        }

        /**
         * Method description
         *
         *
         * @param item
         *
         * @return
         */
        public boolean foundItem(String item)
        {
            boolean retVal = false;

            for (ConfigDataBlockItem ci : this.cfgItems)
            {
                if (ci.itemName.equalsIgnoreCase(item))
                {
                    ci.itemFound = true;
                    retVal = true;
                }
            }

            return (retVal);
        }

        //~--- get methods --------------------------------------------------------
        /**
         * Method description
         *
         *
         * @return
         */
        public String getItemsNotFound()
        {
            StringBuilder itemList = new StringBuilder();

            if (this.cfgItems.size() > 0)
            {
                for (ConfigDataBlockItem ci : this.cfgItems)
                {
                    if ((!ci.itemFound) && (ci.itemRequired))
                    {
                        if (itemList.length() > 0)
                        {
                            itemList.append(", ");
                        }

                        itemList.append(ci.itemName);
                    }
                }

                if (itemList.length() > 2)
                {
                    return (itemList.toString());
                }
            }

            return (null);
        }
    }
}
