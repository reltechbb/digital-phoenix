/*
 * CcsiPendingAck.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of an acknowledgement expected from the host.
 *
 * Version: V1.0  15/09/10
 */


package com.domenix.ccsi.protocol;

import com.domenix.commonha.ccsi.CcsiChannelEnum;
import java.util.Arrays;

/**
 * This class encapsulates the information about an ACK that is expected from the host.
 *
 * @author kmiller
 */
public class CcsiPendingAck
{
  /** The channel to be acknowledged */
  private CcsiChannelEnum[]	channels;

  /** The MSN to be acknowledged */
  private long	msn;

  /** The wait timer ID */
  private long	timer;

  /** The cache key */
  private String[] cacheKey;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a default class instance.
   */
  public CcsiPendingAck()
  {
    this.msn			= -1L;
    this.timer		= -1L;
    this.channels	= null;
    this.cacheKey = null;
  }

  /**
   * Constructs a class instance with all parameters.
   *
   * @param chn the channel to be ack'ed
   * @param msn the MSN to be ack'ed
   * @param timer the wait timer ID
   * @param cacheKey the cache key for the event
   */
  public CcsiPendingAck( CcsiChannelEnum[] chn , long msn , long timer , String[] cacheKey )
  {
    if (chn != null)
    {
        this.channels = (CcsiChannelEnum[]) chn.clone();
    }
    this.msn = msn;
    this.timer = timer;
    if (cacheKey != null)
    {
        this.cacheKey = (String [])cacheKey.clone();
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the MSN
   */
  public long getMsn()
  {
    return msn;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param msn the MSN to set
   */
  public void setMsn( long msn )
  {
    this.msn	= msn;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the timer
   */
  public long getTimer()
  {
    return timer;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param timer the timer to set
   */
  public void setTimer( long timer )
  {
    this.timer	= timer;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the channels
   */
  public CcsiChannelEnum[] getChannels()
  {
    CcsiChannelEnum[] retVal = null;
    if (channels != null)
    {
        retVal = (CcsiChannelEnum[]) this.channels.clone();
    }
    return retVal;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param channels the channels to set
   */
  public void setChannels( CcsiChannelEnum[] channels )
  {
    if (channels != null)
    {
        this.channels = (CcsiChannelEnum[])channels.clone();
    } else
    {
        this.channels = null;
    }
  }
  
  public void addCacheKey( String[] key )
  {
    if (key != null)
    {
        this.cacheKey = (String [])key.clone();
    } else
    {
        this.cacheKey = null;
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Return the current object state as a string.
   *
   * @return String containing the channel, MSN, timer ID cache Key
   */
  @Override
  public String toString()
  {
    StringBuilder	msg	= new StringBuilder( "Pending ACK for: " );

    msg.append( this.msn ).append( ' ' ).append( this.channels[0].name() ).append( " (" ).append(
        this.channels[0].getAbbreviation() ).append( ") timer: " ).append( this.timer ).append(
            " cache: " ).append( Arrays.toString(this.cacheKey) );

    return ( msg.toString() );
  }
}
