/*
 * DataValidityEnumeration.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/12/11
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a string enumeration data validity check.
 *
 * @author kmiller
 */
public class DataValidityEnumeration implements DataValidityInterface
{
  /** The type of validity checking defined. */
  private final DataValidityType	theType	= DataValidityType.ENUMERATION;

  /** The list of valid enumeration values. */
  private final HashMap<String , String>	enumList	= new HashMap<>();

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an empty instance of the type.
   */
  public DataValidityEnumeration()
  {
  }

  /**
   * Constructs an instance with a list of string values for the enumerations
   * 
   * @param args
   */
  public DataValidityEnumeration( String... args )
  {
    for ( String x : args )
    {
      enumList.put( x , x );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Add a new string to the list of valid values.
   *
   * @param enumValue String to be added to the list of valid values
   *
   * @throws IllegalArgumentException if the string is null or empty
   */
  public void addEnumeration( String enumValue ) throws IllegalArgumentException
  {
    if ( ( enumValue != null ) && ( !enumValue.trim().isEmpty() ) )
    {
      this.enumList.put( enumValue , enumValue );
    }
    else
    {
      throw new IllegalArgumentException( "Enumeration string cannot be null or empty string." );
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the type of validity check implemented by this class.
   *
   * @return DataValidityType.ENUMERATION
   */
  @Override
  public DataValidityType getTheType()
  {
    return ( this.theType );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Validates an input string and returns a valid/invalid indication.  The
   * enumeration list must contain the string to be valid.  The string will
   * be considered invalid if it null or empty.
   *
   * @param value String containing the value to be validated
   *
   * @return boolean indication of valid (true) or invalid (false)
   */
  @Override
  public boolean validateValue( String value )
  {
    boolean	retValue	= false;

    if ( ( value != null ) && ( !value.trim().isEmpty() ) )
    {
      if ( this.enumList.size() > 0 )
      {
        retValue	= this.enumList.containsKey( value.trim() );
      }
      else
      {
        retValue	= true;
      }
    }

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Method description
   *
   *
   * @return
   */
  @Override
  public String getValidityString()
  {
    StringBuilder	msg	= new StringBuilder( " must be one of: " );

    for ( String x : enumList.keySet() )
    {
      msg.append( x );
      msg.append( ' ' );
    }

    return ( msg.toString() );
  }
}
