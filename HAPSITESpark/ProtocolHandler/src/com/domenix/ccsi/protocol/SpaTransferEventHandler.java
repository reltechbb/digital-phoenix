/*
 * NsdsTransferEventHandler.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/17
 */
package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.commonha.ccsi.CcsiAckNakEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.intfc.NSDSTransferEvent;
import com.domenix.common.utils.SystemDateTime;
import com.domenix.commonsa.interfaces.SIATransferEvent;
import com.domenix.commonsa.interfaces.SaCcsiChannelEnum;

import org.apache.log4j.Logger;


//~--- classes ----------------------------------------------------------------
/**
 * This class handles transfer events from the NSDS interface to deal with messages sent and
 * received, ACK/NAK sent and received, and heartbeats.
 *
 * @author kmiller
 */
public class SpaTransferEventHandler {

    /**
     * Error/Debug logger
     */
    private final Logger myLogger = Logger.getLogger(TimerEventHandler.class);

    /**
     * The PH Context
     */
    private final PHContext theContext;

    private final SystemDateTime systemDateTime = new SystemDateTime();
    //~--- constructors ---------------------------------------------------------

    /**
     * Constructs ...
     *
     *
     * @param ctx PH Context
     */
    public SpaTransferEventHandler(PHContext ctx) {
        this.theContext = ctx;
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Handles an ACK that was sent.
     *
     * @param evt the sent ACK event
     *
     * @return flag indicating success/failure
     */
//    public boolean handleSentAck(NSDSTransferEvent evt) {
//        boolean retVal = false;
//        StringBuilder txt = new StringBuilder("Send ACK MSN: ").append(evt.getAckMsn()).append(" for ");
//
//        if (evt.isEventStatus()) {
//            this.theContext.getTimerEventHandler().resetHeartbeatTimer();
//            txt.append(evt.getSentMsn()).append(" for ");
//
//            if ((evt.getChannels() != null) && (evt.getChannels().length > 0)) {
//                for (CcsiChannelEnum x : evt.getChannels()) {
//                    txt.append(x.getLongName()).append(' ');
//                }
//            }
//
//            if ((this.theContext.getPendingSends().peekNextEntry() != null)
//                    && ((this.theContext.getPendingSends().peekNextEntry().isSendAck())
//                    || (this.theContext.getPendingSends().peekNextEntry().isSendNak()))) {
//                this.theContext.getPendingSends().removeNextEntry();
//            }
//
//            this.theContext.getTimerEventHandler().resetHeartbeatTimer();
//            retVal = true;
//        } else {
//            txt.append(evt.getSentMsn()).append(" for NAK on ");
//
//            if ((evt.getChannels() != null) && (evt.getChannels().length > 0)) {
//                for (CcsiChannelEnum x : evt.getChannels()) {
//                    txt.append(x.getLongName()).append(' ');
//                }
//            }
//
//            if ((this.theContext.getPendingSends().peekNextEntry() != null)
//                    && ((this.theContext.getPendingSends().peekNextEntry().isSendAck())
//                    || (this.theContext.getPendingSends().peekNextEntry().isSendNak()))) {
//                this.theContext.getPendingSends().removeNextEntry();
//            }
//
//            this.theContext.getTimerEventHandler().resetHeartbeatTimer();
//            retVal = true;
//        }
//
//        myLogger.debug(txt.toString());
//
//        return (retVal);
//    }







    public boolean handleSentAck(SIATransferEvent evt) {
        boolean retVal = false;
        StringBuilder txt = new StringBuilder("Send ACK MSN: ").append(evt.getAckMsn()).append(" for ");

        if (evt.isEventStatus()) {
            this.theContext.getTimerEventHandler().resetHeartbeatTimer();
            txt.append(evt.getSentMsn()).append(" for ");

            if ((evt.getChannels() != null) && (evt.getChannels().length > 0)) {
                for (SaCcsiChannelEnum x : evt.getChannels()) {
                    txt.append(x.getLongName()).append(' ');
                }
            }

            if ((this.theContext.getPendingSends().peekNextEntry() != null)
                    && ((this.theContext.getPendingSends().peekNextEntry().isSendAck())
                    || (this.theContext.getPendingSends().peekNextEntry().isSendNak()))) {
                this.theContext.getPendingSends().removeNextEntry();
            }

            this.theContext.getTimerEventHandler().resetHeartbeatTimer();
            retVal = true;
        } else {
            txt.append(evt.getSentMsn()).append(" for NAK on ");

            if ((evt.getChannels() != null) && (evt.getChannels().length > 0)) {
                for (SaCcsiChannelEnum x : evt.getChannels()) {
                    txt.append(x.getLongName()).append(' ');
                }
            }

            if ((this.theContext.getPendingSends().peekNextEntry() != null)
                    && ((this.theContext.getPendingSends().peekNextEntry().isSendAck())
                    || (this.theContext.getPendingSends().peekNextEntry().isSendNak()))) {
                this.theContext.getPendingSends().removeNextEntry();
            }

            this.theContext.getTimerEventHandler().resetHeartbeatTimer();
            retVal = true;
        }

        myLogger.debug(txt.toString());

        return (retVal);
    }





    /**
     * Handles a sent heartbeat by putting it in the pending ACKs list and resetting the heartbeat
     * timer.
     *
     * @param evt the sent event
     *
     * @return flag indicating success/failure
     */
//    public boolean handleSentHrtbt(NSDSTransferEvent evt) {
//        boolean retVal = false;
//
//        if (evt.isEventStatus()) {
//            if (!evt.isAckFlag()) {
//                this.theContext.getTimerEventHandler().resetHeartbeatTimer();
//                retVal = this.theContext.getPendingAcks().setupUpPendingAck(CcsiChannelEnum.HRTBT, evt.getSentMsn(),
//                        this.theContext.getTimerEventHandler());
//
////      myLogger.info( "Sent HRTBT" );
//            } else {
//                this.theContext.getPendingSends().removeNextEntry();
//
////      myLogger.info( "Sent heartbeat ACK" );
//            }
//
//            retVal = true;
//        } else {
//            // Heartbeat send failed
////    myLogger.error( "Error sending HRTBT message" );
//            this.theContext.getPendingAcks().removeWaitingAck(CcsiChannelEnum.HRTBT);
//            this.theContext.getPendingSends().removeNextEntry();
//            retVal = true;
//        }
//
//        return (retVal);
//    }

    
    
    public boolean handleSentHrtbt(SIATransferEvent evt) {
        boolean retVal = false;

        if (evt.isEventStatus()) {
            if (!evt.isAckFlag()) {
                this.theContext.getTimerEventHandler().resetHeartbeatTimer();
                retVal = this.theContext.getPendingAcks().setupUpPendingAck(CcsiChannelEnum.HRTBT, evt.getSentMsn(),
                        this.theContext.getTimerEventHandler());

//      myLogger.info( "Sent HRTBT" );
            } else {
                this.theContext.getPendingSends().removeNextEntry();

//      myLogger.info( "Sent heartbeat ACK" );
            }

            retVal = true;
        } else {
            // Heartbeat send failed
//    myLogger.error( "Error sending HRTBT message" );
            this.theContext.getPendingAcks().removeWaitingAck(CcsiChannelEnum.HRTBT);
            this.theContext.getPendingSends().removeNextEntry();
            retVal = true;
        }

        return (retVal);
    }

        
        
        
    /**
     * Sent a report to the host
     *
     * @param evt the transfer event
     *
     * @return success/failure
     */
//    public boolean handleSentReport(NSDSTransferEvent evt) {
//        boolean retVal = true;
//        StringBuilder msg = new StringBuilder("Sent report MSN ").append(evt.getSentMsn()).append(" for ");
//
//        if (evt.isEventStatus()) {
//            this.theContext.getTimerEventHandler().resetHeartbeatTimer();
//
//            PendingSendEntry pnd = this.theContext.getPendingSends().peekNextEntry();
//
//            if (pnd != null) {
//                myLogger.debug("Pending report sent is: " + pnd.getMessage());
//
//                if (!pnd.isAckRequired()) {
//                    for (CcsiChannelEnum x : evt.getChannels()) {
//                        msg.append(x.getLongName()).append(' ');
//                    }
//
////        msg.append( " no ACK required." );
//                    String[] cacheKeys = evt.getCacheKeys();
//
//                    if ((cacheKeys != null) && (cacheKeys.length > 0)) {
//                        for (String x : cacheKeys) {
//                            boolean worked = this.theContext.getTheCacheMgr().removeCachedMessage(x);
//
//                            if (!worked) {
//                                retVal = false;
//                                myLogger.error("Failed to remove cached item " + x + " from the cache");
//                            }
//                        }
//                    }
//
//                    this.theContext.getPendingSends().removeNextEntry();
//                } else {
//                    CcsiChannelEnum[] channels = evt.getChannels();
//
//                    retVal = this.theContext.getPendingAcks().setupUpPendingAck(channels[0], evt.getSentMsn(),
//                            this.theContext.getTimerEventHandler());
//
//                    for (CcsiChannelEnum x : channels) {
//                        msg.append(x.getLongName()).append(' ');
//                    }
//                }
//
//                myLogger.debug(msg.toString());
//            }
//        } else {
//            CcsiChannelEnum[] chans = evt.getChannels();
//
//            if ((chans != null) && (chans.length > 0)) {
//                for (CcsiChannelEnum x : chans) {
////        msg2.append( x.getLongName() ).append( ' ' );
//                }
//            }
//
//            this.theContext.getPendingSends().removeNextEntry();
//        }
//
//        return (retVal);
//    }

    
    
    
    
    
    public boolean handleSentReport(SIATransferEvent evt) {
        boolean retVal = true;
        StringBuilder msg = new StringBuilder("Sent report MSN ").append(evt.getSentMsn()).append(" for ");

        if (evt.isEventStatus()) {
            this.theContext.getTimerEventHandler().resetHeartbeatTimer();

            PendingSendEntry pnd = this.theContext.getPendingSends().peekNextEntry();

            if (pnd != null) {
                myLogger.debug("Pending report sent is: " + pnd.getMessage());

                if (!pnd.isAckRequired()) {
                    for (SaCcsiChannelEnum x : evt.getChannels()) {
                        msg.append(x.getLongName()).append(' ');
                    }

//        msg.append( " no ACK required." );
                    String[] cacheKeys = evt.getCacheKeys();

                    if ((cacheKeys != null) && (cacheKeys.length > 0)) {
                        for (String x : cacheKeys) {
                            boolean worked = this.theContext.getTheCacheMgr().removeCachedMessage(x);

                            if (!worked) {
                                retVal = false;
                                myLogger.error("Failed to remove cached item " + x + " from the cache");
                            }
                        }
                    }

                    this.theContext.getPendingSends().removeNextEntry();
                } else {
                    SaCcsiChannelEnum[] channels = evt.getChannels();

                    
//TODO TODO TODO    hey TOM!!!                    
//TODO TODO TODO    hey TOM!!!                    
//TODO TODO TODO    hey TOM!!!                    
//TODO TODO TODO    hey TOM!!!                    
//TODO TODO TODO    hey TOM!!!                    
//TODO TODO TODO    hey TOM!!!                    
//TODO TODO TODO    hey TOM!!!                    
                    retVal = true;
//                    retVal = this.theContext.getPendingAcks().setupUpPendingAck(channels[0], evt.getSentMsn(),
//                            this.theContext.getTimerEventHandler());

                    for (SaCcsiChannelEnum x : channels) {
                        msg.append(x.getLongName()).append(' ');
                    }
                }

                myLogger.debug(msg.toString());
            }
        } else {
            SaCcsiChannelEnum[] chans = evt.getChannels();

            if ((chans != null) && (chans.length > 0)) {
                for (SaCcsiChannelEnum x : chans) {
//        msg2.append( x.getLongName() ).append( ' ' );
                }
            }

            this.theContext.getPendingSends().removeNextEntry();
        }

        return (retVal);
    }

        
        
        
        
        
    /**
     * Handles receiving an ACK or NAK
     *
     * @param evt the transfer event
     *
     * @return success or failure
     */
//    public boolean handleRcvAck(NSDSTransferEvent evt) {
//        boolean retVal = false;
//        StringBuilder msg = new StringBuilder("Rcvd ").append(evt.isEventStatus()
//                ? "ACK"
//                : "NAK").append(" MSN ").append(evt.getRecvdMsn()).append(" for MSN ").append(
//                evt.getAckMsn()).append(" chans=").append(evt.getChannels()[0]);
//
////  myLogger.info( msg.toString() );
//        if (evt.isEventStatus()) {
//            long ackMsn = evt.getAckMsn();
//
//            this.theContext.getTimerEventHandler().resetHeartbeatTimer();
//
//            if (this.theContext.getPendingAcks().hasPendingAck(ackMsn)) {
//                this.theContext.getPendingAcks().ackReceived(ackMsn);
//
//                CcsiChannelEnum[] chans = evt.getChannels();
//
//                this.theContext.getPendingSends().removeNextEntry();
//
//                if ((chans != null) && (chans.length > 0)) {
//                    for (CcsiChannelEnum x : chans) {
//                        msg.append(x.getLongName()).append(' ');
//
//                        if (x == CcsiChannelEnum.HRTBT) {
//                            this.theContext.getTimerEventHandler().resetHeartbeatTimer();
//                        }
//                    }
//                }
//            } else {
//                if (ackMsn != -1)
//                {
//                    myLogger.info("No pending ACK found for " + ackMsn);
//                }
//            }
//        } else {
//            // This is a NAK
//            myLogger.info("Received NAK for " + evt.getAckMsn());
//            for (CcsiChannelEnum x : evt.getChannels()) {
//                msg.append(x.getLongName()).append(' ');
//            }
//
//            msg.append(" code ").append(evt.getNakCode());
//
//            if (this.theContext.getPendingAcks().hasPendingAck(evt.getAckMsn())) {
//                this.theContext.getPendingAcks().ackReceived(evt.getAckMsn());
//                this.theContext.getPendingSends().removeNextEntry();
//            }
//        }
//
//        return (retVal);
//    }

    
    
    
    public boolean handleRcvAck(SIATransferEvent evt) {
        boolean retVal = false;
        StringBuilder msg = new StringBuilder("Rcvd ").append(evt.isEventStatus()
                ? "ACK"
                : "NAK").append(" MSN ").append(evt.getRecvdMsn()).append(" for MSN ").append(
                evt.getAckMsn()).append(" chans=").append(evt.getChannels()[0]);

//  myLogger.info( msg.toString() );
        if (evt.isEventStatus()) {
            long ackMsn = evt.getAckMsn();

            this.theContext.getTimerEventHandler().resetHeartbeatTimer();

            if (this.theContext.getPendingAcks().hasPendingAck(ackMsn)) {
                this.theContext.getPendingAcks().ackReceived(ackMsn);

                SaCcsiChannelEnum[] chans = evt.getChannels();

                this.theContext.getPendingSends().removeNextEntry();

                if ((chans != null) && (chans.length > 0)) {
                    for (SaCcsiChannelEnum x : chans) {
                        msg.append(x.getLongName()).append(' ');

                        if (x == SaCcsiChannelEnum.HRTBT) {
                            this.theContext.getTimerEventHandler().resetHeartbeatTimer();
                        }
                    }
                }
            } else {
                if (ackMsn != -1)
                {
                    myLogger.info("No pending ACK found for " + ackMsn);
                }
            }
        } else {
            // This is a NAK
            myLogger.info("Received NAK for " + evt.getAckMsn());
            for (SaCcsiChannelEnum x : evt.getChannels()) {
                msg.append(x.getLongName()).append(' ');
            }

            msg.append(" code ").append(evt.getNakCode());

            if (this.theContext.getPendingAcks().hasPendingAck(evt.getAckMsn())) {
                this.theContext.getPendingAcks().ackReceived(evt.getAckMsn());
                this.theContext.getPendingSends().removeNextEntry();
            }
        }

        return (retVal);
    }

        
        
        
        
        
        
        
    /**
     * Handle a command received from the host
     *
     * @param evt the transfer event
     *
     * @return success or failure
     */
//    public boolean handleRcvCmd(NSDSTransferEvent evt) {
//        boolean retVal = false;
//
////  myLogger.info( "Received command: " + evt.getRecvdMsn() + "\n" + evt.getRecvdBody() );
//        retVal = this.theContext.getCommandHandler().processCommand(evt);
//
//        return (retVal);
//    }

 
    public boolean handleRcvCmd(SIATransferEvent evt) {
        boolean retVal = false;

//  myLogger.info( "Received command: " + evt.getRecvdMsn() + "\n" + evt.getRecvdBody() );
//TODO TODO TODO   hey TOM!!!
//TODO TODO TODO   hey TOM!!!
//TODO TODO TODO   hey TOM!!!
//TODO TODO TODO   hey TOM!!!
//TODO TODO TODO   hey TOM!!!
//TODO TODO TODO   hey TOM!!!
        retVal = false;
//        retVal = this.theContext.getCommandHandler().processCommand(evt);

        return (retVal);
    }



    /**
     * Handle receiving a heartbeat message.
     *
     * @param evt the receive event
     *
     * @return success or failure
     */
//    public boolean handleRcvHrtbt(NSDSTransferEvent evt) {
//        boolean retVal = true;
//
//        // check the date time and update if necessary
//        systemDateTime.checkSystemDateTimeLong(evt.getRcvdDtg());
//
//        if (!evt.isAckFlag()) {
//            this.theContext.getTimerEventHandler().resetHeartbeatTimer();
//
//            PendingSendEntry rhPse = new PendingSendEntry();
//
//            rhPse.setConnectionId(evt.getConnectionId());
//            rhPse.addChannel(CcsiChannelEnum.HRTBT);
//            rhPse.setSendAck(true);
//            rhPse.setAckMsn(evt.getRecvdMsn());
//            rhPse.setAckType(CcsiAckNakEnum.ACK);
//            rhPse.setAckRequired(false);
//            this.theContext.getPendingSends().addPendingSend(rhPse);
//        }    // endif is an ACK - ACKs are handled by received ACK processing
//
//        return (retVal);
//    }

    
 
    
    
    
    public boolean handleRcvHrtbt(SIATransferEvent evt) {
        boolean retVal = true;

        // check the date time and update if necessary
        systemDateTime.checkSystemDateTimeLong(evt.getRcvdDtg());

        if (!evt.isAckFlag()) {
            this.theContext.getTimerEventHandler().resetHeartbeatTimer();

            PendingSendEntry rhPse = new PendingSendEntry();

            rhPse.setConnectionId(evt.getConnectionId());
            rhPse.addChannel(CcsiChannelEnum.HRTBT);
            rhPse.setSendAck(true);
            rhPse.setAckMsn(evt.getRecvdMsn());
            rhPse.setAckType(CcsiAckNakEnum.ACK);
            rhPse.setAckRequired(false);
            this.theContext.getPendingSends().addPendingSend(rhPse);
        }    // endif is an ACK - ACKs are handled by received ACK processing

        return (retVal);
    }





        
        
        
    /**
     * Handles receiving a report?
     *
     * @param evt
     *
     * @return
     */
//    public boolean handleRcvReport(NSDSTransferEvent evt) {
//        boolean retVal = false;
//
//        myLogger.error("Received: " + evt.getFlags() + " " + evt.getAckMsn() + " " + evt.getRecvdMsn());
//
//        return (retVal);
//    }



    public boolean handleRcvReport(SIATransferEvent evt) {
        boolean retVal = false;

        myLogger.error("Received: " + evt.getFlags() + " " + evt.getAckMsn() + " " + evt.getRecvdMsn());

        return (retVal);
    }





}
