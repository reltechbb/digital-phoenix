/*
 * PendingEventGeneration.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/12
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.commonha.ccsi.CcsiChannelEnum;

import org.apache.log4j.Logger;

//~--- classes ----------------------------------------------------------------

/**
 * This class handles preparing the generation of reports on a timed basis.
 * 
 * @author kmiller
 */
public class PendingTimedGeneration
{
  /** The event channel */
  private CcsiChannelEnum	eventChannel	= null;

  /** Error/Debug logger */
  private final Logger	myLogger	= Logger.getLogger(PendingTimedGeneration.class );

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance indicating which channel should be processed for
   * timer expiration handling.
   *
   * @param channel the channel to be processed
   */
  public PendingTimedGeneration( CcsiChannelEnum channel )
  {
    this.eventChannel	= channel;
    myLogger.debug( "Created new pending timed generation entry" );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the eventChannel
   */
  public CcsiChannelEnum getEventChannel()
  {
    return eventChannel;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param eventChannel the eventChannel to set
   */
  public void setEventChannel( CcsiChannelEnum eventChannel )
  {
    this.eventChannel	= eventChannel;
  }
}
