/*
 * PHContext.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains a container for all of the lists, queues, and
 * other data structures that support the execution context of the
 * PH.
 *
 * Version: V1.0  15/10/06
 */
package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.ccsi.report.old.ReportGenerator;
import com.domenix.common.spark.CtlWrapper;
import com.domenix.common.spark.CtlWrapperQueue;
import com.domenix.ccsi.cache.CcsiCacheManager;
import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.ccsi.config.XmlErrorHandler;
import com.domenix.commonha.intfc.NSDSTransferEvent;
import com.domenix.commonha.queues.NSDSXferEventQueue;
import com.domenix.commonha.queues.ConnEventQueue;
import com.domenix.commonha.intfc.NSDSConnEvent;
import com.domenix.commonha.intfc.NSDSInterface;
import com.domenix.commonha.intfc.HostProtocolAdapterInterface;
import com.domenix.common.spark.IPCWrapper;
import com.domenix.common.spark.IPCWrapperQueue;
import com.domenix.common.spark.IPCQueueListenerInterface;
import com.domenix.common.utils.TimerChangeEvent;
import com.domenix.common.utils.UtilTimer;
import com.domenix.commonsa.interfaces.SIATransferEvent;
import com.domenix.commonsa.interfaces.SIAXferEventQueue;
import com.domenix.sensorinterfaceadapter.SIAInterface;

//~--- JDK imports ------------------------------------------------------------
import java.io.File;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

//~--- classes ----------------------------------------------------------------
/**
 * This class provides a single encapsulation of the protocol handler execution context
 *
 * @author kmiller
 */
public class PHContext {

    /**
     * The sensor's active alerts
     */
    private ActiveAlertsList activeAlerts;

    /**
     * The timer to use
     */
    private UtilTimer baseTimer;

    /**
     * The CCSI communications schema file
     */
    private File commSchemaFile;

    /**
     * Host command handler
     */
    private CcsiCommandHandler commandHandler;

    /**
     * Sensor event handler
     */
    private ConnEventHandler connEventHandler;

    /**
     * The queue for lost connection event
     */
    private ConnEventQueue connEventQueue;
    private ConnEventQueue hpaConnEventQueue;
    /**
     * The protocol handler control queue.
     */
    private CtlWrapperQueue controlQueue;

    /**
     * The IPC Queue listener for the PH
     */
    private IPCQueueListenerInterface listener;

    /**
     * Pending ACK list
     */
    private CcsiPendingAckList pendingAcks;

    /**
     * The pending alerts list for event/timed transmission
     */
    private PendingAlertList pendingAlerts;

    /**
     * The pending list for event/timed transmission
     */
    private PendingBitList pendingBits;

    /**
     * The pending list of configuration block changes
     */
    private PendingConfigList pendingConfig;

    /**
     * Conn events pending
     */
    private LinkedBlockingQueue<NSDSConnEvent> pendingConn;

    /**
     * The pending consumables list for event/timed transmission
     */
    private PendingConsumablesList pendingConsumables;

    /**
     * CtlWrapper Events pending
     */
    private LinkedBlockingQueue<CtlWrapper> pendingCtl;

    /**
     * Pending event generation list
     */
    private PendingEventGenerationList pendingEventGen;

    /**
     * The pending failure list for event/timed transmission
     */
    private PendingFailureList pendingFailures;

    /**
     * IPC Events pending
     */
    private LinkedBlockingQueue<IPCWrapper> pendingIPC;

    /**
     * Pending NAK details
     */
    private PendingNakDetailList pendingNakDetails;

    /**
     * The pending reading list for event/timed transmission
     */
    private PendingReadingList pendingReadings;

    /**
     * Pending send messages
     */
    private PendingSendList pendingSends;

    /**
     * List of pending status entries
     */
    private PendingStatusList pendingStatusList;

    /**
     * Pending timed event list
     */
    private PendingTimedGenerationList pendingTimedGen;

    /**
     * Conn events pending
     */
    private LinkedBlockingQueue<TimerChangeEvent> pendingTimer;

    /**
     * Transfer events pending
     */
    private LinkedBlockingQueue<NSDSTransferEvent> nsdsPendingXfer;
    private LinkedBlockingQueue<SIATransferEvent> spaPendingXfer;

    /**
     * Queue read needed
     */
    private AtomicBoolean queueReadNeeded;

    /**
     * The report generator
     */
    private ReportGenerator reportGenerator;

    /**
     * Sensor event handler
     */
    private SensorEventHandler sensorEventHandler;

    /**
     * The queue for input from the sensor
     */
    private IPCWrapperQueue sensorInQueue;

    /**
     * The queue for writing entries to the sensor.
     */
    private IPCWrapperQueue sensorOutQueue;

    /**
     * Shutdown flag
     */
    private AtomicBoolean shutDownNow;

    /**
     * The cache manager
     */
    private CcsiCacheManager theCacheMgr;

    /**
     * The sensor configuration.
     */
    private CcsiConfigSensor theConfig;

    /**
     * The open interface
     */
    private NSDSInterface theOpenInterfaceHost;
    private SIAInterface theOpenInterfaceSensor;

    /**
     * The Host Protocol Adapter Interface
     */
    private HostProtocolAdapterInterface hpaInterface;

    /**
     * The sensor saved state.
     */
    private CcsiSavedState theSavedState;

    /**
     * Timer event handler
     */
    private TimerEventHandler timerEventHandler;

    /**
     * Transfer event handler
     */
    private NsdsTransferEventHandler nsdsTransferEventHandler;
    private SpaTransferEventHandler spaTransferEventHandler;

    /**
     * Our transfer event queue - we only write to this
     */
    private SIAXferEventQueue spaTransferEventQueue;
    private NSDSXferEventQueue hpaTransferEventQueue;

    /**
     * XML parse error handler
     */
    private XmlErrorHandler xmlErrorHandler;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs a class instance
     */
    public PHContext() {
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingStatusList
     */
    public PendingStatusList getPendingStatusList() {
        return pendingStatusList;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingStatusList the pendingStatusList to set
     */
    public void setPendingStatusList(PendingStatusList pendingStatusList) {
        this.pendingStatusList = pendingStatusList;
    }

//~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingConfigList
     */
    public PendingConfigList getPendingConfigList() {
        return (this.pendingConfig);
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingConfigList a config list to be set
     */
    public void setPendingConfigList(PendingConfigList pendingConfigList) {
        this.pendingConfig = pendingConfigList;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingIPC
     */
    public LinkedBlockingQueue<IPCWrapper> getPendingIPC() {
        return pendingIPC;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingIPC the pendingIPC to set
     */
    public void setPendingIPC(LinkedBlockingQueue<IPCWrapper> pendingIPC) {
        this.pendingIPC = pendingIPC;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingCtl
     */
    public LinkedBlockingQueue<CtlWrapper> getPendingCtl() {
        return pendingCtl;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingCtl the pendingCtl to set
     */
    public void setPendingCtl(LinkedBlockingQueue<CtlWrapper> pendingCtl) {
        this.pendingCtl = pendingCtl;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the nsdsPendingXfer
     */
    public LinkedBlockingQueue<NSDSTransferEvent> getNsdsPendingXfer() {
        return nsdsPendingXfer;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingXfer the nsdsPendingXfer to set
     */
    public void setNsdsPendingXfer(LinkedBlockingQueue<NSDSTransferEvent> nsdsPendingXfer) {
        this.nsdsPendingXfer = nsdsPendingXfer;
    }

    
    
    public LinkedBlockingQueue<SIATransferEvent> getSpaPendingXfer() {
        return spaPendingXfer;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingXfer the nsdsPendingXfer to set
     */
    public void setSpaPendingXfer(LinkedBlockingQueue<SIATransferEvent> spaPendingXfer) {
        this.spaPendingXfer = spaPendingXfer;
    }    
    
    
    
    
    
    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingTimer
     */
    public LinkedBlockingQueue<TimerChangeEvent> getPendingTimer() {
        return pendingTimer;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingTimer the pendingTimer to set
     */
    public void setPendingTimer(LinkedBlockingQueue<TimerChangeEvent> pendingTimer) {
        this.pendingTimer = pendingTimer;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingSends
     */
    public PendingSendList getPendingSends() {
        return pendingSends;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingSends the pendingSends to set
     */
    public void setPendingSends(PendingSendList pendingSends) {
        this.pendingSends = pendingSends;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingNakDetails
     */
    public PendingNakDetailList getPendingNakDetails() {
        return pendingNakDetails;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingNakDetails the pendingNakDetails to set
     */
    public void setPendingNakDetails(PendingNakDetailList pendingNakDetails) {
        this.pendingNakDetails = pendingNakDetails;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingConn
     */
    public LinkedBlockingQueue<NSDSConnEvent> getPendingConn() {
        return pendingConn;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingConn the pendingConn to set
     */
    public void setPendingConn(LinkedBlockingQueue<NSDSConnEvent> pendingConn) {
        this.pendingConn = pendingConn;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the queueReadNeeded
     */
    public AtomicBoolean getQueueReadNeeded() {
        return queueReadNeeded;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param queueReadNeeded the queueReadNeeded to set
     */
    public void setQueueReadNeeded(AtomicBoolean queueReadNeeded) {
        this.queueReadNeeded = queueReadNeeded;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingReadings
     */
    public PendingReadingList getPendingReadings() {
        return pendingReadings;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingReadings the pendingReadings to set
     */
    public void setPendingReadings(PendingReadingList pendingReadings) {
        this.pendingReadings = pendingReadings;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingFailures
     */
    public PendingFailureList getPendingFailures() {
        return pendingFailures;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingFailures the pendingFailures to set
     */
    public void setPendingFailures(PendingFailureList pendingFailures) {
        this.pendingFailures = pendingFailures;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingConsumables
     */
    public PendingConsumablesList getPendingConsumables() {
        return pendingConsumables;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingConsumables the pendingConsumables to set
     */
    public void setPendingConsumables(PendingConsumablesList pendingConsumables) {
        this.pendingConsumables = pendingConsumables;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingBits
     */
    public PendingBitList getPendingBits() {
        return pendingBits;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingBits the pendingBits to set
     */
    public void setPendingBits(PendingBitList pendingBits) {
        this.pendingBits = pendingBits;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingAlerts
     */
    public PendingAlertList getPendingAlerts() {
        return pendingAlerts;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingAlerts the pendingAlerts to set
     */
    public void setPendingAlerts(PendingAlertList pendingAlerts) {
        this.pendingAlerts = pendingAlerts;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the activeAlerts
     */
    public ActiveAlertsList getActiveAlerts() {
        return activeAlerts;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param activeAlerts the activeAlerts to set
     */
    public void setActiveAlerts(ActiveAlertsList activeAlerts) {
        this.activeAlerts = activeAlerts;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the baseTimer
     */
    public UtilTimer getBaseTimer() {
        return baseTimer;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param baseTimer the baseTimer to set
     */
    public void setBaseTimer(UtilTimer baseTimer) {
        this.baseTimer = baseTimer;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the commSchemaFile
     */
    public File getCommSchemaFile() {
        return commSchemaFile;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param commSchemaFile the commSchemaFile to set
     */
    public void setCommSchemaFile(File commSchemaFile) {
        this.commSchemaFile = commSchemaFile;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the commandHandler
     */
    public CcsiCommandHandler getCommandHandler() {
        return commandHandler;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param commandHandler the commandHandler to set
     */
    public void setCommandHandler(CcsiCommandHandler commandHandler) {
        this.commandHandler = commandHandler;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the connEventHandler
     */
    public ConnEventHandler getConnEventHandler() {
        return connEventHandler;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param connEventHandler the connEventHandler to set
     */
    public void setConnEventHandler(ConnEventHandler connEventHandler) {
        this.connEventHandler = connEventHandler;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the connEventQueue
     */
    public ConnEventQueue getConnEventQueue() {
        return connEventQueue;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param connEventQueue the connEventQueue to set
     */
    public void setConnEventQueue(ConnEventQueue connEventQueue) {
        this.connEventQueue = connEventQueue;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the connEventQueue
     */
    public ConnEventQueue getHPAConnEventQueue() {
        return hpaConnEventQueue;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param connEventQueue the connEventQueue to set
     */
    public void setHPAConnEventQueue(ConnEventQueue connEventQueue) {
        this.hpaConnEventQueue = connEventQueue;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the controlQueue
     */
    public CtlWrapperQueue getControlQueue() {
        return controlQueue;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param controlQueue the controlQueue to set
     */
    public void setControlQueue(CtlWrapperQueue controlQueue) {
        this.controlQueue = controlQueue;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingAcks
     */
    public CcsiPendingAckList getPendingAcks() {
        return pendingAcks;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingAcks the pendingAcks to set
     */
    public void setPendingAcks(CcsiPendingAckList pendingAcks) {
        this.pendingAcks = pendingAcks;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingEventGen
     */
    public PendingEventGenerationList getPendingEventGen() {
        return pendingEventGen;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingEventGen the pendingEventGen to set
     */
    public void setPendingEventGen(PendingEventGenerationList pendingEventGen) {
        this.pendingEventGen = pendingEventGen;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingTimedGen
     */
    public PendingTimedGenerationList getPendingTimedGen() {
        return pendingTimedGen;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingTimedGen the pendingTimedGen to set
     */
    public void setPendingTimedGen(PendingTimedGenerationList pendingTimedGen) {
        this.pendingTimedGen = pendingTimedGen;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the reportGenerator
     */
    public ReportGenerator getReportGenerator() {
        return reportGenerator;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param reportGenerator the reportGenerator to set
     */
    public void setReportGenerator(ReportGenerator reportGenerator) {
        this.reportGenerator = reportGenerator;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the sensorEventHandler
     */
    public SensorEventHandler getSensorEventHandler() {
        return sensorEventHandler;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param sensorEventHandler the sensorEventHandler to set
     */
    public void setSensorEventHandler(SensorEventHandler sensorEventHandler) {
        this.sensorEventHandler = sensorEventHandler;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the sensorInQueue
     */
    public IPCWrapperQueue getSensorInQueue() {
        return sensorInQueue;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param sensorInQueue the sensorInQueue to set
     */
    public void setSensorInQueue(IPCWrapperQueue sensorInQueue) {
        this.sensorInQueue = sensorInQueue;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the sensorOutQueue
     */
    public IPCWrapperQueue getSensorOutQueue() {
        return sensorOutQueue;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param sensorOutQueue the sensorOutQueue to set
     */
    public void setSensorOutQueue(IPCWrapperQueue sensorOutQueue) {
        this.sensorOutQueue = sensorOutQueue;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the shutDownNow
     */
    public AtomicBoolean getShutDownNow() {
        return shutDownNow;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param shutDownNow the shutDownNow to set
     */
    public void setShutDownNow(AtomicBoolean shutDownNow) {
        this.shutDownNow = shutDownNow;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the theCacheMgr
     */
    public CcsiCacheManager getTheCacheMgr() {
        return theCacheMgr;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param theCacheMgr the theCacheMgr to set
     */
    public void setTheCacheMgr(CcsiCacheManager theCacheMgr) {
        this.theCacheMgr = theCacheMgr;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the theConfig
     */
    public CcsiConfigSensor getTheConfig() {
        return theConfig;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param theConfig the theConfig to set
     */
    public void setTheConfig(CcsiConfigSensor theConfig) {
        this.theConfig = theConfig;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the theOpenInterfaceHost
     */
    public NSDSInterface getTheOpenInterfaceHost() {
        return theOpenInterfaceHost;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param theOpenInterfaceHost the theOpenInterfaceHost to set
     */
    public void setTheOpenInterfaceHost(NSDSInterface theOpenInterfaceHost) {
        this.theOpenInterfaceHost = theOpenInterfaceHost;
    }

    
    
        /**
     * @return the theOpenInterfaceSensor
     */
    public SIAInterface getTheOpenInterfaceSensor() {
        return theOpenInterfaceSensor;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param theOpenInterfaceSensor the theOpenInterfaceSensor to set
     */
    public void setTheOpenInterfaceSensor(SIAInterface theOpenInterfaceSensor) {
        this.theOpenInterfaceSensor = theOpenInterfaceSensor;
    }
    
    
    
    /**
     * @return the Host Protocol Adapter Interface
     */
    public HostProtocolAdapterInterface getTheHPAInterface() {
        return hpaInterface;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param theHPAInterface
     */
    public void setTheHPAInterface(HostProtocolAdapterInterface theHPAInterface) {
        this.hpaInterface = theHPAInterface;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the theSavedState
     */
    public CcsiSavedState getTheSavedState() {
        return theSavedState;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param theSavedState the theSavedState to set
     */
    public void setTheSavedState(CcsiSavedState theSavedState) {
        this.theSavedState = theSavedState;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the timerEventHandler
     */
    public TimerEventHandler getTimerEventHandler() {
        return timerEventHandler;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param timerEventHandler the timerEventHandler to set
     */
    public void setTimerEventHandler(TimerEventHandler timerEventHandler) {
        this.timerEventHandler = timerEventHandler;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the transferEventHandler
     */
    public NsdsTransferEventHandler getNsdsTransferEventHandler() {
        return nsdsTransferEventHandler;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param transferEventHandler the transferEventHandler to set
     */
    public void setNsdsTransferEventHandler(NsdsTransferEventHandler transferEventHandler) {
        this.nsdsTransferEventHandler = transferEventHandler;
    }

    
    
    public SpaTransferEventHandler getSpaTransferEventHandler() {
        return spaTransferEventHandler;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param transferEventHandler the transferEventHandler to set
     */
    public void setSpaTransferEventHandler(SpaTransferEventHandler transferEventHandler) {
        this.spaTransferEventHandler = transferEventHandler;
    }
    
    
    
    //~--- get methods ----------------------------------------------------------
    /**
     * @return the transferEventQueue
     */
    public SIAXferEventQueue getSpaTransferEventQueue() {
        return spaTransferEventQueue;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param transferEventQueue the transferEventQueue to set
     */
    public void setSpaTransferEventQueue(SIAXferEventQueue spaTransferEventQueue) {
        this.spaTransferEventQueue = spaTransferEventQueue;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the transferEventQueue
     */
    public NSDSXferEventQueue getHPATransferEventQueue() {
        return hpaTransferEventQueue;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param transferEventQueue the transferEventQueue to set
     */
    public void setHPATransferEventQueue(NSDSXferEventQueue transferEventQueue) {
        this.hpaTransferEventQueue = transferEventQueue;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the xmlErrorHandler
     */
    public XmlErrorHandler getXmlErrorHandler() {
        return xmlErrorHandler;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param xmlErrorHandler the xmlErrorHandler to set
     */
    public void setXmlErrorHandler(XmlErrorHandler xmlErrorHandler) {
        this.xmlErrorHandler = xmlErrorHandler;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the listener
     */
    public IPCQueueListenerInterface getListener() {
        return listener;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param listener the listener to set
     */
    public void setListener(IPCQueueListenerInterface listener) {
        this.listener = listener;
    }
}
