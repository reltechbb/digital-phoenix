/*
 * CcsiCommandHandler.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the implementation for varification and
 *
 * Version: V1.0  15/08/23
 */
package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.ChannelRegistrationList;
import com.domenix.ccsi.CommandHandlingType;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;
import com.domenix.common.ccsi.NameValuePair;
import com.domenix.commonha.intfc.NSDSTransferEvent;
import com.domenix.commonha.intfc.NSDSConnection;
import com.domenix.common.spark.data.CcsiComponentTypeEnum;
import com.domenix.common.utils.NumberFormatter;
import com.domenix.common.utils.UtilTimerListener;

import org.apache.log4j.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

//~--- JDK imports ------------------------------------------------------------
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.regex.Pattern;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

//~--- classes ----------------------------------------------------------------
/**
 * This class processes CCSI Standard and Sensor Unique commands received from a
 * host.
 *
 * @author kmiller
 */
public class CcsiCommandHandler
{

    /**
     * The document produced by the validation.
     */
    Document document = null;

    /**
     * The document builder factory used to get a document builder.
     */
    DocumentBuilderFactory factory = null;

    /**
     * The schema being used to validate the sensor definition.
     */
    Schema mySchema = null;

    /**
     * The received Connection ID
     */
    private long recvdConnId = -1L;

    /**
     * The parsed received message DTG
     */
    private long recvdDtg = -1L;

    /**
     * The received MSN
     */
    private long recvdMsn = -1L;

    /**
     * The schema factory used to generate a new schema for validation.
     */
    SchemaFactory sFact = null;

    /**
     * Reading ID pattern
     */
    private Pattern rdgIdPattern = Pattern.compile("R[0-9]{9}");

    /**
     * Get Status arguments
     */
    private Pattern getStatusPattern = Pattern.compile("ALL|LRU|State|Link|Connect|Power|NAK");

    /**
     * Alert ID pattern
     */
    private Pattern alertIdPattern = Pattern.compile("(([AW][UT])|NN)[0-9]{9}");

    /**
     * The argument for the set heartbeat command
     */
    private NameValuePair setHeartbeatArg = null;

    /**
     * Set configuration argument validator
     */
    private SetConfigArgumentValidator setConfigArgVal;

    /**
     * Command argument collector
     */
    private CommandArgumentList cmdArgList;

    /**
     * Get Configuration argument list
     */
    private CcsiGetConfigArgumentList getCfgArgList;

    /**
     * Field description
     */
    private UtilTimerListener listener;

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger;

    /**
     * A channel registration list for Register commands
     */
    private ChannelRegistrationList regList;

    /**
     * Set Configuration argument list
     */
    private CcsiSetConfigArgumentList setCfgArgList;

    /**
     * Start self test arguments
     */
    private CcsiStartSelfTestArgs sstArgs;

    /**
     * The PH Context
     */
    private PHContext theContext;

    /**
     * A working channel registration list for register commands.
     */
    private ChannelRegistrationList workList;

    /**
     * Validates commands
     */
    private CommandValidator commandValidator;

    /**
     * Executes commands
     */
    private CommandExecuter commandExecuter;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs an instance of the command handler
     *
     * @param ctx PH Context
     * @param listener
     *
     * @throws IllegalArgumentException
     */
    public CcsiCommandHandler(PHContext ctx, UtilTimerListener listener) throws IllegalArgumentException
    {
        myLogger = Logger.getLogger(CcsiCommandHandler.class);
        this.theContext = ctx;
        this.regList = new ChannelRegistrationList();
        this.workList = new ChannelRegistrationList();
        this.setCfgArgList = new CcsiSetConfigArgumentList();
        this.getCfgArgList = new CcsiGetConfigArgumentList();
        this.sstArgs = new CcsiStartSelfTestArgs();
        this.setHeartbeatArg = new NameValuePair();
        this.cmdArgList = new CommandArgumentList(this.theContext.getTheConfig(),
                this.theContext.getTheSavedState());
        this.setConfigArgVal = new SetConfigArgumentValidator(theContext);
        this.listener = listener;

        commandValidator = new CommandValidator(theContext);
        commandExecuter = new CommandExecuter(theContext);
        try
        {
            sFact = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            mySchema = sFact.newSchema(this.theContext.getCommSchemaFile());
            factory = DocumentBuilderFactory.newInstance();

            // Do not set this if using a Schema!!! factory.setValidating(true);
            factory.setNamespaceAware(true);
            factory.setSchema(mySchema);
        } catch (IllegalArgumentException | SAXException iae)
        {
            myLogger.error(iae);
        }
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Process a received command.
     *
     * @param event the transfer event containing the command
     *
     * @return flag indicating success/failure
     */
    public boolean processCommand(NSDSTransferEvent event)
    {
        boolean retVal = false;

        if (event != null)
        {
            this.theContext.getTheSavedState().getParseErrors().clear();

            String msg = event.getRecvdBody();

            this.recvdMsn = event.getRecvdMsn();
            this.recvdConnId = event.getConnectionId();

            Document theDoc = this.validate(msg);

            if (theDoc != null)
            {
                if (this.theContext.getTheSavedState().getParseErrors().isEmpty())
                {
                    this.regList
                            = this.theContext.getTheOpenInterfaceHost().getActiveConnections().getItem(recvdConnId).getRegisteredChannels();

                    NodeList docNodes = theDoc.getChildNodes();
                    int len = (docNodes != null)
                            ? docNodes.getLength()
                            : 0;
                    Node hdrNode;
                    ValHdrResults valHdrResults = new ValHdrResults();

                    //
                    // Find and process the root node
                    //
                    NodeList childNodes = null;
                    int cLen = 0;

                    for (int z = 0; z < len; z++)
                    {
                        if ((docNodes != null) && (docNodes.item(z) != null) && (docNodes.item(z).getNodeType() == Node.ELEMENT_NODE)
                                && (docNodes.item(z).getLocalName().equalsIgnoreCase("Hdr")))
                        {
                            //
                            // Get the child nodes and find the header node
                            //
                            hdrNode = docNodes.item(z);
                            childNodes = docNodes.item(z).getChildNodes();
                            cLen = (childNodes != null)
                                    ? childNodes.getLength()
                                    : 0;

                            //
                            // Process attributes
                            //
                            ArrayList<NameValuePair> attr = this.getAttributes(hdrNode);

                            if ((attr != null) && (!attr.isEmpty()))
                            {
                                for (NameValuePair x : attr)
                                {
                                    String attrName = x.getTheName();
                                    String attrValue = x.getTheValue();

                                    switch (attrName)
                                    {
                                        case "sid":
                                            valHdrResults.sid = attrValue;

                                            break;

                                        case "msn":
                                            valHdrResults.msn = Long.parseLong(attrValue);

                                            break;

                                        case "chn":
                                            String chan = attrValue;

                                            if ((chan != null) && (chan.trim().length() > 0))
                                            {
                                                for (int c = 0; c < chan.trim().length(); c++)
                                                {
                                                    char val = chan.trim().charAt(c);
                                                    CcsiChannelEnum thisChan = CcsiChannelEnum.value(val);

                                                    if (thisChan != null)
                                                    {
                                                        valHdrResults.addChannel(thisChan);
                                                    } else
                                                    {
                                                        myLogger.error("Invalid channel: " + val);
                                                        this.theContext.getPendingNakDetails().addEntry(new PendingNakDetailEntry(
                                                                        this.recvdConnId, this.recvdDtg, this.recvdMsn,
                                                                        NakDetailCodeEnum.INVALID_ARGUMENT));
                                                    }
                                                }
                                            }

                                            break;

                                        case "dtg":
                                            valHdrResults.dtg = Long.parseLong(attrValue);

                                            break;

                                        case "mod":
                                            valHdrResults.mode = attrValue.trim().charAt(0);

                                            break;

                                        case "len":
                                            valHdrResults.msgLength = Integer.parseInt(attrValue);

                                            break;

                                        case "flg":
                                            String work = attrValue.trim();

                                            for (char w : work.toCharArray())
                                            {
                                                switch (w)
                                                {
                                                    case 'C':
                                                        valHdrResults.compressFlag = true;

                                                        break;

                                                    case 'E':
                                                        valHdrResults.encryptFlag = true;

                                                        break;

                                                    case 'R':
                                                        valHdrResults.retransFlag = true;

                                                        break;

                                                    default:
                                                        break;
                                                }
                                            }

                                            break;

                                        case "ack":
                                            if (attrValue.trim().equalsIgnoreCase("ACK"))
                                            {
                                                valHdrResults.ackType = true;
                                            } else if (attrValue.trim().equalsIgnoreCase("MACK"))
                                            {
                                                valHdrResults.nakType = true;
                                            }

                                            break;

                                        case "ackmsn":
                                            valHdrResults.ackMsn = Long.parseLong(attrValue);

                                            break;

                                        case "nakcod":
                                            valHdrResults.nakCode = NakReasonCodeEnum.valueOf(attrValue.trim().toUpperCase());

                                            break;

                                        default:
                                            break;
                                    }
                                }
                            } else
                            {
                                myLogger.error("No header attributes found.");
                            }

                            for (int i = 0; i < cLen; i++)
                            {
                                if ((childNodes != null) && (childNodes.item(i) != null))
                                {
                                    Node currentNode = childNodes.item(i);

                                    if ((currentNode.getNodeType() == Node.ELEMENT_NODE))
                                    {
                                        if (childNodes.item(i).getLocalName().equalsIgnoreCase("CCSIDoc"))
                                        {
                                            NodeList cdocNodes = childNodes.item(i).getChildNodes();
                                            int cdocLen = ((cdocNodes != null)
                                                    ? cdocNodes.getLength()
                                                    : 0);

                                            for (int j = 0; j < cdocLen; j++)
                                            {
                                                if ((cdocNodes != null) && (cdocNodes.item(j) != null)
                                                        && (cdocNodes.item(j).getNodeType() == Node.ELEMENT_NODE)
                                                        && (cdocNodes.item(j).getLocalName().equalsIgnoreCase("Msg")))
                                                {
                                                    ArrayList<NameValuePair> msgAttr = this.getAttributes(cdocNodes.item(j));

                                                    if ((msgAttr != null) && (msgAttr.size() > 0))
                                                    {
                                                        for (NameValuePair x : msgAttr)
                                                        {
                                                            if (x.getTheName().equalsIgnoreCase("flg"))
                                                            {
                                                                for (int m = 0; m < x.getTheValue().length(); m++)
                                                                {
                                                                    if (x.getTheValue().trim().charAt(m) == 'C')
                                                                    {
                                                                        valHdrResults.cmdCompressFlag = true;
                                                                    } else if (x.getTheValue().trim().charAt(m) == 'E')
                                                                    {
                                                                        valHdrResults.cmdEncryptFlag = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    NodeList chanNodes = cdocNodes.item(j).getChildNodes();
                                                    int chanLen = ((chanNodes != null)
                                                            ? chanNodes.getLength()
                                                            : 0);

                                                    for (int n = 0; n < chanLen; n++)
                                                    {
                                                        if ((chanNodes != null) && (chanNodes.item(n) != null)
                                                                && (chanNodes.item(n).getNodeType() == Node.ELEMENT_NODE)
                                                                && (chanNodes.item(n).getLocalName().equalsIgnoreCase("CmdChn")))
                                                        {
                                                            ArrayList<NameValuePair> chanAttr = this.getAttributes(chanNodes.item(n));

                                                            if ((chanAttr != null) && (chanAttr.size() > 0))
                                                            {
                                                                for (NameValuePair x : chanAttr)
                                                                {
                                                                    if (x.getTheName().equalsIgnoreCase("cmd"))
                                                                    {
                                                                        String cmdName = x.getTheValue();
                                                                        CommandHandlingType h
                                                                                = this.theContext.getTheConfig().getCmdHandling().getCommand(cmdName);

                                                                        if (h != null)
                                                                        {
                                                                            ArrayList<NameValuePair> argList
                                                                                    = this.cmdArgList.getCommandArguments(chanNodes.item(n));

                                                                            if (argList != null)
                                                                            {
                                                                                boolean valid = false;

                                                                                try
                                                                                {
                                                                                    valid = this.validateCommand(h, argList, valHdrResults);
                                                                                } catch (Exception vcEx)
                                                                                {
                                                                                    myLogger.error("Exception validating command " + h.getCommand(), vcEx);
                                                                                    valid = false;
                                                                                }

                                                                                if (valid)
                                                                                {
                                                                                    try
                                                                                    {
                                                                                        retVal = this.executeCmd(h, this.cmdArgList, workList, setCfgArgList,
                                                                                                sstArgs, getCfgArgList, valHdrResults);
                                                                                    } catch (Exception cxEx)
                                                                                    {
                                                                                        retVal = false;
                                                                                        myLogger.error("Exception executing command " + h.getCommand(), cxEx);
                                                                                    }
                                                                                }
                                                                            } else
                                                                            {
                                                                                try
                                                                                {
                                                                                    retVal = this.executeCmd(h, null, null, null, null, null,
                                                                                            valHdrResults);
                                                                                } catch (Exception nclEx)
                                                                                {
                                                                                    retVal = false;
                                                                                    myLogger.error("Exception executing command " + h.getCommand(), nclEx);
                                                                                }
                                                                            }
                                                                        } else
                                                                        {
                                                                            PendingSendEntry uscPse = new PendingSendEntry();

                                                                            uscPse.addChannel(CcsiChannelEnum.CMD);
                                                                            uscPse.setConnectionId(recvdConnId);
                                                                            uscPse.setSendNak(true);
                                                                            uscPse.setAckMsn(recvdMsn);
                                                                            uscPse.setNakCode(NakReasonCodeEnum.MSGCON);
                                                                            uscPse.setNakDetailCode(NakDetailCodeEnum.UNSUPPORTED_COMMAND);
                                                                            this.theContext.getPendingSends().addPendingSend(uscPse);
                                                                            myLogger.error("Unsupported command: " + cmdName);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else
                {
                    StringBuilder msg2 = new StringBuilder("Message parsing errors:\n");

                    for (String x : this.theContext.getTheSavedState().getParseErrors())
                    {
                        msg2.append(x).append('\n');
                    }

                    myLogger.error(msg2.toString());
                    this.theContext.getPendingNakDetails().addEntry(new PendingNakDetailEntry(this.recvdConnId,
                            this.recvdDtg, this.recvdMsn, NakDetailCodeEnum.INVALID_MESSAGE, "Parsing errors."));
                }
            } else
            {
                myLogger.error("Prasing failed for received command message.");
                this.theContext.getPendingNakDetails().addEntry(new PendingNakDetailEntry(this.recvdConnId, this.recvdDtg,
                        this.recvdMsn, NakDetailCodeEnum.INVALID_MESSAGE, "Parsing errors."));
            }
        } else
        {
            myLogger.error("Event to be processed is null");
        }

        return (retVal);
    }

    /**
     * This executes a valid command
     *
     * @param h the command handling type
     * @param argList the argument list
     * @param work the channel registration list
     * @param setCfgList the set configuration list
     * @param sst the start self test arguments
     * @param getCfgList the get configuration arguments
     * @param val the header validation results
     *
     * @return flag indicating success or failure
     */
    private boolean executeCmd(CommandHandlingType h, CommandArgumentList argList,
            ChannelRegistrationList work,
            CcsiSetConfigArgumentList setCfgList, CcsiStartSelfTestArgs sst,
            CcsiGetConfigArgumentList getCfgList, ValHdrResults val)
    {
        boolean displayConfigDialog = false;
        String ip = "";
        String mask = "";
        String gate = "";
        boolean retVal = false;
        NSDSConnection conn = this.theContext.getTheSavedState().getConnList().
                getItem(recvdConnId);

        switch (h.getCommand())
        {
            case "Power_Off":
                retVal = commandExecuter.executePower_Off(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Render_Useless":
                retVal = commandExecuter.executeRender_Useless(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Set_Tamper":
                retVal = commandExecuter.executeSet_Tamper(h, recvdConnId, recvdMsn);
                break;
            case "Set_Emcon_Mode":
                retVal = commandExecuter.executeSet_Emcon_Mode(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Set_Encryption":
                retVal = commandExecuter.executeSet_Encryption(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Erase_Sec_Log":
                retVal = commandExecuter.executeErase_Sec_Log(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Get_Sec_Log":
                retVal = commandExecuter.executeGet_Sec_Log(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Sanitize":
                retVal = commandExecuter.executeSanitize(h, recvdConnId, recvdMsn);
                break;
            case "Zeroize":
                retVal = commandExecuter.executeZeroize(h, recvdConnId, recvdMsn);
                break;
            case "Set_Cmd_Privilege":
                retVal = commandExecuter.executeSet_Cmd_Privilege(h,
                        recvdConnId, recvdMsn);
                break;
            case "Set_Config_Privilege":
                retVal = commandExecuter.executeSet_Config_Privilege(h,
                        recvdConnId, recvdMsn);
                break;
            case "Reset_Msg_Seq":
                retVal = commandExecuter.executeReset_Msg_Seq(h, recvdConnId,
                        recvdMsn, conn);
                break;
            case "Start_Stop":
                retVal = commandExecuter.executeStart_Stop(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Reboot":
                retVal = commandExecuter.executeReboot(h, recvdConnId, recvdMsn, argList);
                break;
            case "Set_Comm_Permission":
                retVal = commandExecuter.executeSet_Comm_Permission(h,
                        recvdConnId, recvdMsn);
                break;
            case "Set_Local_Alert_Mode":
                retVal = commandExecuter.executeSet_Local_Alert_Mode(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Start_Self_Test":
                retVal = commandExecuter.executeStart_Self_Test(h, recvdConnId,
                        recvdMsn, regList, sstArgs, this);
                break;
            case "Set_Security_Timeout":
                retVal = commandExecuter.executeSet_Security_Timeout(h,
                        recvdConnId, recvdMsn);
                break;
            case "Clear_Alert":
                retVal = commandExecuter.executeClear_Alert(h, recvdConnId, recvdMsn, argList);
                break;
            case "Silence":
                retVal = commandExecuter.executeSilence(h, recvdConnId, recvdMsn, argList);
                break;
            case "Set_Heartbeat":
                retVal = commandExecuter.executeSet_Heartbeat(h, recvdConnId, recvdMsn, setHeartbeatArg);
                break;
            case "Set_Location":
                retVal = commandExecuter.executeSet_Location(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Set_Data_Compression":
                retVal = commandExecuter.executeSet_Data_Compression(h,
                        recvdConnId, recvdMsn);
                break;
            case "Set_Local_Enable":
                retVal = commandExecuter.executeSet_Local_Enable(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Set_Bw_Mode":
                retVal = commandExecuter.executeSet_Bw_Mode(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Get_Status":
                retVal = commandExecuter.executeGet_Status(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Get_Configuration":
                retVal = commandExecuter.executeGet_Configuration(h, recvdConnId,
                        recvdMsn, getCfgArgList);
                break;
            case "Install_Start":
                retVal = commandExecuter.executeInstall_Start(h, recvdConnId, recvdMsn);
                break;
            case "Install":
                retVal = commandExecuter.executeInstall(h, recvdConnId, recvdMsn);
                break;
            case "Install_Complete":
                retVal = commandExecuter.executeInstall_Complete(h, recvdConnId, recvdMsn);
                break;
            case "Set_Ccsi_Mode":
                retVal = commandExecuter.executeSet_Ccsi_Mode(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "User_Change":
                retVal = commandExecuter.executeUser_Change(h, recvdConnId, recvdMsn);
                break;
            case "Get_Users":
                retVal = commandExecuter.executeGet_Users(h, recvdConnId, recvdMsn);
                break;
            case "Register":
                retVal = commandExecuter.executeRegister(h, recvdConnId, recvdMsn,
                        work, regList, workList);
                break;
            case "Deregister":
                retVal = commandExecuter.executeDeregister(h, recvdConnId, recvdMsn);
                break;
            case "Set_Config":
                retVal = commandExecuter.executeSet_Config(h, recvdConnId,
                        recvdMsn, setCfgList);
                break;
            case "Get_Identification":
                retVal = commandExecuter.executeGet_Identification(h, recvdConnId,
                        recvdMsn);
                break;
            case "Set_Date_Time":
                retVal = commandExecuter.executeSet_Date_Time(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Get_Alert_Details":
                retVal = commandExecuter.executeGet_Alert_Details(h, recvdConnId,
                        recvdMsn, argList);
                break;
            case "Get_Reading_Details":
                retVal = commandExecuter.executeGet_Reading_Details(h, recvdConnId,
                        recvdMsn, argList);
                break;
            default:
                // handles sensor unique commands
                retVal = commandExecuter.executeSensorUniqueCommand (h, recvdConnId,
                        recvdMsn, argList);
                break;
        }

        if (displayConfigDialog)
        {
            displayConfigDialog = false;
            this.writeNoticeIpAddress();
        }

        return (retVal);
    }

    /**
     * Writes a notice file containing four lines which are:
     *
     * The time of the change (yyyymmddhhmmss)<br>
     * The IP address <br>
     * The broadcast address <br>
     * The netmask <br>
     */
    private void writeNoticeIpAddress()
    {
        StringBuilder fName = new StringBuilder("C:\\NGCD_CCSI\\notices\\notice-");
        long curTime = System.currentTimeMillis();

        fName.append(NumberFormatter.getLong(curTime, 10, true, 14, '0'));
        fName.append(".txt");
        myLogger.info("Creating notice file: " + fName.toString());

        File noticeFile = new File(fName.toString());

        BufferedWriter writer = null;
        try
        {
            if (noticeFile.createNewFile())
            {
                writer = new BufferedWriter(new FileWriter(noticeFile));

                writer.write(NumberFormatter.getDate(null, null));
                writer.newLine();
                writer.write(this.theContext.getTheConfig().getHostNetworkSettings().getPHIP().getHostAddress());
                writer.newLine();
                writer.write(this.theContext.getTheConfig().getHostNetworkSettings().getBroadcastAddr().getHostAddress());
                writer.newLine();
                writer.write(this.theContext.getTheConfig().getHostNetworkSettings().getNetmask().getHostAddress());
                writer.newLine();
                writer.write(this.theContext.getTheConfig().getHostNetworkSettings().getGatewayAddr().getHostAddress());
                writer.newLine();
//                writer.close();
//                writer = null;
                myLogger.info("Notice file written and closed" + noticeFile.getAbsolutePath());
            } else
            {
                myLogger.error("Could not create new file " + noticeFile.getAbsolutePath());
            }
        } catch (IOException ex)
        {
            myLogger.error("Exception writing the notice file", ex);
        } finally
        {
            if (writer != null)
            {
                try {
                    writer.close ();
                } catch (IOException ex) {
                    myLogger.error("Exception closing the notice file", ex);
                }
            }
        }
    }

    /**
     * The method validates the received command to ensure that we support the
     * command and that the arguments are valid.
     *
     * @param handling the command handling for the command
     * @param args the parsed arguments
     * @param hdr the parsed header information
     *
     * @return flag indicating that the command is valid (true) or invalid
     * (false)
     */
    private boolean validateCommand(CommandHandlingType handling, ArrayList<NameValuePair> args, ValHdrResults hdr)
    {
        boolean retVal = false;

        switch (handling.getCommand())
        {
            case "Power_Off":
                retVal = commandValidator.validatePower_OffCommand(recvdConnId, recvdMsn, args);
                break;
            case "Render_Useless":
                retVal = commandValidator.validateRender_UselessCommand(recvdConnId, recvdMsn, args);
                break;
            case "Set_Tamper":
                retVal = commandValidator.validateSet_TamperCommand();
                break;
            case "Set_Emcon_Mode":
                retVal = commandValidator.validateSet_Emcon_ModeCommand(recvdConnId, recvdMsn, args);
                break;
            case "Set_Encryption":
                retVal = commandValidator.validateSet_EncryptionCommand(recvdConnId, recvdMsn, args);
                break;
            case "Erase_Sec_Log":
                retVal = commandValidator.validateErase_Sec_LogCommand(recvdConnId, recvdMsn, args);
                break;
            case "Get_Sec_Log":
                retVal = commandValidator.validateGet_Sec_LogCommand(recvdConnId, recvdMsn, args);
                break;
            case "Sanitize":
                retVal = commandValidator.validateSanitizeCommand(recvdConnId, recvdMsn, args);
                break;
            case "Zeroize":
                retVal = commandValidator.validateZeroizeCommand(recvdConnId, recvdMsn, args);
                break;
            case "Set_Cmd_Privilege":
                retVal = commandValidator.validateSet_Cmd_PrivilegeCommand();
                break;
            case "Set_Config_Privilege":
                retVal = commandValidator.validateSet_Config_PrivilegeCommand();
                break;
            case "Reset_Msg_Seq":
                retVal = commandValidator.validateReset_Msg_SeqCommand(recvdConnId, recvdMsn, args, hdr);
                break;
            case "Start_Stop":
                retVal = commandValidator.validateStart_StopCommand(recvdConnId, recvdMsn, args);
                break;
            case "Reboot":
                retVal = commandValidator.validateRebootCommand(recvdConnId, recvdMsn, args);
                break;
            case "Set_Comm_Permission":
                retVal = commandValidator.validateSet_Comm_PermissionCommand();
                break;
            case "Set_Local_Alert_Mode":
                retVal = commandValidator.validateSet_Local_Alert_ModeCommand(recvdConnId, recvdMsn, args);
                break;
            case "Start_Self_Test":
                retVal = commandValidator.validateStart_Self_TestCommand(recvdConnId, recvdMsn, args, sstArgs);
                break;
            case "Set_Security_Timeout":
                retVal = commandValidator.validateSet_Security_TimeoutCommand();
                break;
            case "Clear_Alert":
                retVal = commandValidator.validateClear_AlertCommand(recvdConnId, recvdMsn, args, alertIdPattern);
                break;
            case "Silence":
                retVal = commandValidator.validateSilenceCommand(recvdConnId, recvdMsn, args);
                break;
            case "Set_Heartbeat":
                retVal = commandValidator.validateSet_HeartbeatCommand(recvdConnId, recvdMsn, args, setHeartbeatArg, this);
                break;
            case "Set_Location":
                retVal = commandValidator.validateSet_LocationCommand(recvdConnId, recvdMsn, args);
                break;
            case "Set_Data_Compression":
                retVal = commandValidator.validateSet_Data_CompressionCommand(recvdConnId, recvdMsn, args);
                break;
            case "Set_Local_Enable":
                retVal = commandValidator.validateSet_Local_EnableCommand(recvdConnId, recvdMsn, args);
                break;
            case "Set_Bw_Mode":
                retVal = commandValidator.validateSet_Bw_ModeCommand(recvdConnId, recvdMsn, args);
                break;
            case "Get_Status":
                retVal = commandValidator.validateGet_StatusCommand(recvdConnId, recvdMsn, args, getStatusPattern);
                break;
            case "Get_Configuration":
                retVal = commandValidator.validateGet_ConfigurationCommand(recvdConnId, recvdMsn, args, getCfgArgList);
                break;
            case "Install_Start":
                retVal = commandValidator.validateInstall_StartCommand(recvdConnId, recvdMsn, args);
                break;
            case "Install":
                retVal = commandValidator.validateInstallCommand(recvdConnId, recvdMsn, args);
                break;
            case "Install_Complete":
                retVal = commandValidator.validateInstall_CompleteCommand(recvdConnId, recvdMsn, args);
                break;
            case "Set_Ccsi_Mode":
                retVal = commandValidator.validateSet_Ccsi_ModeCommand(recvdConnId, recvdMsn, args);
                break;
            case "User_Change":
                retVal = commandValidator.validateUser_ChangeCommand();
                break;
            case "Get_Users":
                retVal = commandValidator.validateGet_UsersCommand();
                break;
            case "Register":
                retVal = commandValidator.validateRegisterCommand(recvdConnId, 
                        recvdMsn, args, workList);
                break;
            case "Deregister":
                retVal = commandValidator.validateDeregisterCommand(recvdConnId, 
                        recvdMsn, args);
                break;
            case "Set_Config":
                retVal = commandValidator.validateSet_ConfigCommand(recvdConnId, 
                        recvdMsn, args, setCfgArgList, recvdDtg);
                break;
            case "Get_Identification":
                retVal = commandValidator.validateGet_IdentificationCommand(recvdConnId, 
                        recvdMsn, args);
                break;
            case "Set_Date_Time":
                retVal = commandValidator.validateSet_Date_TimeCommand(recvdConnId, 
                        recvdMsn, args);
                break;
            case "Get_Alert_Details":
                retVal = commandValidator.validateGet_Alert_DetailsCommand(recvdConnId, 
                        recvdMsn, args, alertIdPattern);
                break;
            case "Get_Reading_Details":
                retVal = commandValidator.validateGet_Reading_DetailsCommand(recvdConnId, 
                        recvdMsn, args, rdgIdPattern);
                break;
            default:
                retVal = commandValidator.validateUniqueCommand ();
                break;
        }

        return (retVal);
    }

    /**
     * Parse a CCSI message and return the parsed document or null if it fails
     *
     * @param msgBuffer the message to be parsed
     *
     * @return Document parsed from the command or null on failure
     */
    private Document validate(String msgBuffer)
    {
        try
        {
            factory.setNamespaceAware(true);

            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(msgBuffer));

            this.theContext.getTheSavedState().getParseErrors().clear();
            builder.setErrorHandler(this.theContext.getXmlErrorHandler());
            document = builder.parse(is);

            return (document);
        } catch (SAXException sxe)
        {
            // Error generated during parsing
            Exception x = sxe;

            if (sxe.getException() != null)
            {
                x = sxe.getException();
            }

            myLogger.error(x);
        } catch (ParserConfigurationException | IOException pce)
        {
            // Parser with specified options can't be built
            myLogger.error(pce);
        }

        return (null);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Returns a ArrayList containing name value pairs with the attribute name
     * and attribute value for a specific node.
     *
     * @param node - the node to extract parameters from
     * @return ArrayList containing name value pairs or null if none are found.
     */
    private ArrayList<NameValuePair> getAttributes(Node node)
    {
        NamedNodeMap attrs = node.getAttributes();
        int len = (attrs != null)
                ? attrs.getLength()
                : 0;

        if ((len > 0) && (attrs != null))
        {
            ArrayList<NameValuePair> attArrayList = new ArrayList<>(len);

            for (int i = 0; i < len; i++)
            {
                Node attr = attrs.item(i);
                NameValuePair pair = new NameValuePair();

                pair.setTheName(attr.getLocalName().trim());
                pair.setTheValue(normalizeString(attr.getNodeValue()));
                attArrayList.add(pair);
            }

            return (attArrayList);
        } else
        {
            return (null);
        }
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Normalize a string from the parsed XML document to recognize empty ones.
     *
     * @param s String the value to be normalized
     * @return String the normalized string
     */
    private String normalizeString(String s)
    {
        StringBuilder sb = new StringBuilder();
        int len = (s != null)
                ? s.trim().length()
                : 0;

        if ((len == 0) || (s == null))
        {
            sb.append("<<empty>>");
        } else
        {
            String tmp = s.trim();
            boolean lastBlank = false;

            for (int i = 0; i < len; i++)
            {
                if (Character.isWhitespace(tmp.charAt(i)))
                {
                    if (!lastBlank)
                    {
                        lastBlank = true;
                        sb.append(' ');
                    }
                } else if (tmp.charAt(i) == '\n')
                {
                    lastBlank = true;
                    sb.append(' ');
                } else
                {
                    lastBlank = false;
                    sb.append(tmp.charAt(i));
                }
            }
        }

        return (sb.toString());
    }

    /**
     * @return the cmdArgList
     */
//    private CommandArgumentList getCmdArgList()
//    {
//        return cmdArgList;
//    }

    /**
     * @return the regList
     */
    public ChannelRegistrationList getRegList()
    {
        return regList;
    }

    /**
     * @return the setCfgArgList
     */
    public CcsiSetConfigArgumentList getSetCfgArgList()
    {
        return setCfgArgList;
    }

    /**
     * @return the sstArgs
     */
    public CcsiStartSelfTestArgs getSstArgs()
    {
        return sstArgs;
    }

    /**
     * @return the recvdConnId
     */
    public long getRecvdConnId()
    {
        return recvdConnId;
    }

    /**
     * @return the recvdDtg
     */
    public long getRecvdDtg()
    {
        return recvdDtg;
    }

    /**
     * @return the recvdMsn
     */
    public long getRecvdMsn()
    {
        return recvdMsn;
    }

    /**
     * Sets the setHeartbeatArg
     *
     * @param arg
     */
    public void setSetHeartbeatArg(NameValuePair arg)
    {
        setHeartbeatArg = arg;
    }

    /**
     * Gets the setHeartbeatArg
     *
     * @return
     */
    public NameValuePair getSetHeartbeatArg()
    {
        return setHeartbeatArg;
    }

    //~--- inner classes --------------------------------------------------------
    /**
     * This class assembles command arguments from the XML DOM
     */
    /**
     * Class description
     *
     *
     * @version Enter version here.., 15/12/02
     * @author Enter your name here..
     */
    class SstCmdArgs
    {

        /**
         * Field description
         */
        public CcsiComponentTypeEnum comp;

        /**
         * Field description
         */
        public Integer level;

        //~--- constructors -------------------------------------------------------
        /**
         * Constructs ..
         *
         *
         * @param c
         * @param l
         */
        public SstCmdArgs(CcsiComponentTypeEnum c, Integer l)
        {
            this.comp = c;
            this.level = l;
        }
    }

    /**
     * This class is a container for header parsing and validation results.
     */
    class ValHdrResults
    {

        /**
         * The ACK/NAK message sequence number.
         */
        long ackMsn = -1L;

        /**
         * The actual message length.
         */
        int actualMsgLength = 0;

        /**
         * The channels that were contained in the report.
         */
        ArrayList<CcsiChannelEnum> channels = new ArrayList<>();

        /**
         * The header DTG.
         */
        long dtg = 0L;

        /**
         * The reported sensor CCSI mode.
         */
        char mode = ' ';

        /**
         * The message length.
         */
        int msgLength = 0;

        /**
         * The message MSN.
         */
        long msn = -1L;

        /**
         * The NAK code received.
         */
        NakReasonCodeEnum nakCode = null;

        /**
         * The reporting sensor ID.
         */
        String sid = null;

        /**
         * Retransmit flag value.
         */
        boolean retransFlag = false;

        /**
         * This is a NAK carrying message.
         */
        boolean nakType = false;

        /**
         * This is a heartbeat message.
         */
        boolean hrtbtMsg = false;

        /**
         * The transmission contains encrypted text.
         */
        boolean encryptFlag = false;

        /**
         * The transmission contains compressed text.
         */
        boolean compressFlag = false;

        /**
         * Command encrypt flag
         */
        boolean cmdEncryptFlag = false;

        /**
         * Command compress flag
         */
        boolean cmdCompressFlag = false;

        /**
         * This is an ACK carrying message.
         */
        boolean ackType = false;

        /**
         * This is an ACK/NAK resend
         */
        boolean ackNakResend = false;

        //~--- constructors -------------------------------------------------------
        /**
         * Constructs an instance of validation header results
         */
        public ValHdrResults()
        {
        }

        //~--- methods ------------------------------------------------------------
        /**
         * This method adds a new channel to the list. If multiple instances of
         * a channel are attempted it will return false indicating that a
         * channel spec error was detected.
         *
         * @param chn ChannelVal for the channel to be added
         *
         * @return boolean indication of successful (true) or invalid (false)
         */
        public boolean addChannel(CcsiChannelEnum chn)
        {
            boolean retValue = true;

            if (chn != null)
            {
                channels.add(chn);
            }

            return (retValue);
        }
    }
}
