/*
 * CcsiProtocolHandler.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/02
 */
package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.ccsi.report.old.ReportGenerator;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.CcsiChannelRegistrationEnum;
import com.domenix.ccsi.CcsiConfigBlockEnum;
import com.domenix.ccsi.VersionNumberType;
import com.domenix.commonha.ccsi.ChannelRegistrationType;
import com.domenix.common.spark.CtlWrapper;
import com.domenix.common.spark.CtlWrapperQueue;
import com.domenix.ccsi.cache.CcsiCacheManager;
import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.ccsi.config.XmlErrorHandler;
import com.domenix.ccsi.jaxb.ccsibasetypes.CCSIDocType;
import com.domenix.ccsi.jaxb.ccsibasetypes.CCSIMsg;
import com.domenix.commonha.intfc.NSDSInterface;
import com.domenix.commonha.intfc.NSDSTransferEvent;
import com.domenix.commonha.queues.NSDSXferEventQueue;
import com.domenix.commonha.queues.ConnEventQueue;
import com.domenix.commonha.intfc.NSDSConnEvent;
import com.domenix.commonha.intfc.NSDSConnection;
import com.domenix.commonha.intfc.NSDSConnectionTypeEnum;
import com.domenix.commonha.intfc.HostProtocolAdapterInterface;
import com.domenix.common.spark.IPCWrapper;
import com.domenix.common.spark.IPCWrapperQueue;
import com.domenix.common.spark.data.AlertTypeEnum;
import com.domenix.common.spark.data.BitResultEnum;
import com.domenix.common.spark.IPCQueueEvent;
import com.domenix.common.spark.IPCQueueListenerInterface;
import com.domenix.common.utils.NumberFormatter;
import com.domenix.common.utils.TimerChangeEvent;
import com.domenix.common.utils.UtilTimer;
import com.domenix.common.utils.UtilTimerListener;
import com.domenix.sensorinterfaceadapter.SIAInterface;
import com.domenix.ccsi.jaxb.ccsibasetypes.CCSIMsgType;
import com.domenix.ccsi.jaxb.ccsibasetypes.IdentificationReportType;
import com.domenix.ccsi.jaxb.ccsibasetypes.MaintPointOfContactType;
import com.domenix.ccsi.jaxb.ccsibasetypes.SensorIdentificationDataInstanceType;
import com.domenix.ccsi.jaxb.ccsibasetypes.VersionType;
import com.domenix.common.ccsi.MSN;
import com.domenix.commonsa.interfaces.SIATransferEvent;
import com.domenix.commonsa.interfaces.SIAXferEventQueue;


import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------
import java.io.File;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

//~--- classes ----------------------------------------------------------------
/**
 * This class is the controlling logic for the CCSI Protocol Handler that
 * controls the timing, sequencing, parsing, and formatting of CCSI commands and
 * messages.
 *
 * @author kmiller
 */
public class CcsiProtocolHandler implements UtilTimerListener, IPCQueueListenerInterface, Runnable
{
    String quote = "\"";
    
    /**
     * List of pending status entries
     */
    private final PHContext theContext = new PHContext();

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger = Logger.getLogger(CcsiProtocolHandler.class);
    
    /** is logging level debug */
    private final boolean logLevelDebug;
    
    /**
     * baseDirectory
     */
    private final String baseDir;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs an instance of the protocol handler.
     *
     * @param cfg the configuration
     * @param state the saved state
     * @param mgr the cache manager
     * @param senseIn the sensor input queue (ReceiverTopic fed)
     * @param senseOut the sensor output queue (Feed to Writer Topic)
     * @param ctlQ the protocol handler control queue
     * @param baseTimer the UtilTimer instance we must use
     * @param intfc the NSDS open interface
     * @param hpaIntfc
     * @param baseDir
     */
    public CcsiProtocolHandler(CcsiConfigSensor cfg, CcsiSavedState state, CcsiCacheManager mgr,
            IPCWrapperQueue senseIn, IPCWrapperQueue senseOut, CtlWrapperQueue ctlQ,
            UtilTimer baseTimer, NSDSInterface intfcHost, SIAInterface intfcSensor, HostProtocolAdapterInterface hpaIntfc, 
            String baseDir)
    {
//        System.out.println("CCSI PH.constructor -> STARTING construction.");
                
        this.baseDir = baseDir;

        logLevelDebug = myLogger.isDebugEnabled();
        
        theContext.setPendingTimedGen(new PendingTimedGenerationList());
        theContext.setPendingEventGen(new PendingEventGenerationList());
        theContext.setShutDownNow(new AtomicBoolean(false));
        theContext.setTheHPAInterface(hpaIntfc);
        theContext.setTheOpenInterfaceHost(intfcHost);
        theContext.setTheOpenInterfaceSensor(intfcSensor);
        theContext.setControlQueue(ctlQ);
        theContext.getControlQueue().addIPCQueueEventListener(this);
        theContext.setSensorInQueue(senseIn);
        theContext.getSensorInQueue().addIPCQueueEventListener(this);
        theContext.setSensorOutQueue(senseOut);
        theContext.setConnEventQueue(state.getConnEventQueue());
        theContext.setHPAConnEventQueue(state.getHPAConnEventQueue());
        theContext.getHPAConnEventQueue().addIPCQueueEventListener(this);
        theContext.setSpaTransferEventQueue(state.getSpaXferEventQueue());
        theContext.getSpaTransferEventQueue().addIPCQueueEventListener(this);
        theContext.setHPATransferEventQueue(state.getHPAXferEventQueue());
        theContext.getHPATransferEventQueue().addIPCQueueEventListener(this);
        theContext.setPendingAcks(new CcsiPendingAckList(cfg, state));
        theContext.setTheCacheMgr(mgr);
        theContext.setTheSavedState(state);
        theContext.setTheConfig(cfg);
        theContext.setActiveAlerts(new ActiveAlertsList());
        theContext.setPendingAlerts(new PendingAlertList());
        theContext.setPendingBits(new PendingBitList());
        theContext.setPendingConn(new LinkedBlockingQueue<>());
        theContext.setPendingConsumables(new PendingConsumablesList());
        theContext.setPendingCtl(new LinkedBlockingQueue<>());
        theContext.setPendingFailures(new PendingFailureList());
        theContext.setPendingIPC(new LinkedBlockingQueue<>());
        theContext.setPendingNakDetails(new PendingNakDetailList());
        theContext.setPendingReadings(new PendingReadingList());
        theContext.setPendingSends(new PendingSendList());
        theContext.setPendingStatusList(new PendingStatusList());
        theContext.setPendingTimer(new LinkedBlockingQueue<>());
        theContext.setNsdsPendingXfer(new LinkedBlockingQueue<>());
        theContext.setSpaPendingXfer(new LinkedBlockingQueue<>());
        theContext.setQueueReadNeeded(new AtomicBoolean(false));
        theContext.setReportGenerator(new ReportGenerator(theContext));
        theContext.setBaseTimer(baseTimer);
        theContext.setXmlErrorHandler(new XmlErrorHandler(theContext.getTheSavedState()));
        theContext.setSensorEventHandler(new SensorEventHandler(theContext));
        theContext.setTimerEventHandler(new TimerEventHandler(theContext, this));
        theContext.setConnEventHandler(new ConnEventHandler(theContext));
        theContext.setPendingConfigList(new PendingConfigList());

        File temp = new File(baseDir + "/CCSI_XSD/CCSI_XML_Communications.xsd");

        theContext.setCommSchemaFile(temp);
        theContext.setCommandHandler(new CcsiCommandHandler(theContext, this));
        theContext.setNsdsTransferEventHandler(new NsdsTransferEventHandler(theContext));
        theContext.setSpaTransferEventHandler(new SpaTransferEventHandler(theContext));
        if (!restoreFromCache())
        {
            myLogger.error("Could not restore all values from cache to memory");
        }

//        System.out.println("BEFORE call to SparkMain.initiateSensorComms.");
//        System.out.println();
//        boolean result = DigitalPhoenixSparkMain.initiateSensorComms();
//        System.out.println("AFTER call to SparkMain.initiateSensorComms.    result = " + result);   
//        System.out.println();
//        System.out.println("CCSI PH.constructor -> finished construction.");

    }

    /**
     * Restore memory list entries from the cache
     *
     * @return flag indicating success (true) or failure (false)
     */
    private boolean restoreFromCache()
    {
        boolean retVal = true;
        CcsiCacheManager cache = this.theContext.getTheCacheMgr();
        StringBuilder idStr = new StringBuilder();

        //
        // First, we restore alerts
        //
        ArrayList<IPCWrapper> alerts = cache.getAllForRouting("alert-[0-9]*");
        if ((!alerts.isEmpty()))
        {
            for (IPCWrapper x : alerts)
            {
                if (x.getMsgBody().isSetReading())
                {
                    if (x.getMsgBody().getReading().isSetAlert())
                    {
                        boolean trigger = x.getMsgBody().getReading().getAlert() == AlertTypeEnum.ALERT;
                        PendingAlertEntry ent = new PendingAlertEntry(x.getAlertId(), trigger, x.getMsgBody());
                        ent.setReading(true);
                        idStr.setLength(0);
                        idStr.append('A').append((trigger ? 'T' : 'U'));
                        idStr.append(NumberFormatter.getLong(x.getAlertId(), 10, true, 9, '0'));
                        ent.setAlertId(idStr.toString());
                        this.theContext.getPendingAlerts().addAlert(ent);
                        if (trigger)
                        {
                            this.theContext.getActiveAlerts().addAlert(new ActiveAlertListEntry(idStr.toString(), x));
                        }
                    }
                } else if (x.getMsgBody().isSetBIT())
                {
                    if (x.getMsgBody().getBIT().get(0).isSetResult())
                    {
                        boolean failed = x.getMsgBody().getBIT().get(0).getResult() == BitResultEnum.FAIL;
                        PendingAlertEntry ent = new PendingAlertEntry(x.getAlertId(), failed, x.getMsgBody());
                        ent.setReading(false);
                        idStr.setLength(0);
                        idStr.append('A').append((failed ? 'T' : 'U'));
                        idStr.append(NumberFormatter.getLong(x.getAlertId(), 10, true, 9, '0'));
                        ent.setAlertId(idStr.toString());
                        this.theContext.getPendingAlerts().addAlert(ent);
                        if (failed)
                        {
                            this.theContext.getActiveAlerts().addAlert(new ActiveAlertListEntry(idStr.toString(), x));
                        }
                    }
                }
            }
        }

        //
        // Next we restore readings that haven't been sent to the
        // pending readings list
        //
        ArrayList<IPCWrapper> rdgs = cache.getAllForRouting("reading-[0-9]*");
        if ((!rdgs.isEmpty()))
        {
            for (IPCWrapper x : rdgs)
            {
                if (x.getMsgBody().isSetReading())
                {
                    this.theContext.getPendingReadings().addReading(x.getMsgBody().getReading());
                }
            }
        }

        //
        // Next we restore BIT that haven't been sent to the
        // pending BIT list
        //
        ArrayList<IPCWrapper> bits = cache.getAllForRouting("bit-[0-9]*");
        if ((!bits.isEmpty()))
        {
            for (IPCWrapper x : bits)
            {
                if (x.getMsgBody().isSetBIT())
                {
                    this.theContext.getPendingBits().addBit(x.getMsgBody().getBIT().get(0));
                }
            }
        }

        //
        // Next we restore consumables that haven't been sent to the
        // pending consumables list
        //
        ArrayList<IPCWrapper> consume = cache.getAllForRouting("consume-[0-9]*");
        if ((!consume.isEmpty()))
        {
            for (IPCWrapper x : consume)
            {
                if (x.getMsgBody().isSetConsumable())
                {
                    this.theContext.getPendingConsumables().addConsumable(x.getMsgBody().getConsumable().get(0));
                }
            }
        }

        //
        // Finally we restore status messages that haven't been sent to the
        // pending status list
        //
        ArrayList<IPCWrapper> status = cache.getAllForRouting("status-[0-9]*");
        if ((!status.isEmpty()))
        {
            for (IPCWrapper x : status)
            {
                if (x.getMsgBody().isSetStatus())
                {
                    this.theContext.getPendingStatusList().addPendingStatus(new PendingStatusEntry("State"));
                }
            }
        }

        return (retVal);
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Event action handler for IPC Queue events
     *
     * @param evt the IPC Queue event that occurred
     */
    @Override
    public void queueEventFired(IPCQueueEvent evt)
    {
        IPCWrapper ipcWrapper;
        CtlWrapper ctlWrapper;
        NSDSConnEvent conWrapper;
        NSDSTransferEvent nsdsXferWrapper;
        SIATransferEvent spaXferWrapper;
        myLogger.info ("PH Queue event fired");
        if (evt.getEventType() == IPCQueueEvent.IPCQueueEventType.IPC_NEW)
        {
            if (evt.getEventSource() instanceof IPCWrapperQueue)
            {
                myLogger.info ("PH IPC Queue event fired");
                ipcWrapper = this.theContext.getSensorInQueue().readFirst(0L);

                if (ipcWrapper != null)
                {                     
                    boolean ipcStat = this.theContext.getPendingIPC().offer(ipcWrapper);
                    if (logLevelDebug)
                    {
                        myLogger.debug ("Reveived IPC message");
                    }
                    if (!ipcStat)
                    {
                        myLogger.error("Cannot add event to pending IPC event " + ipcWrapper.getRoutingKey());
                    } else
                    {
                        this.theContext.getQueueReadNeeded().set(true);
                    }
                }
            //NOT an IPC Wrapper.....test to see if Control Wrapper.    
            } else if (evt.getSource() instanceof CtlWrapperQueue)
            {
                ctlWrapper = this.theContext.getControlQueue().readFirst(0);

                if (ctlWrapper != null)
                {
                    boolean ctlStat = this.theContext.getPendingCtl().offer(ctlWrapper);

                    {
                        if (!ctlStat)
                        {
                            myLogger.error("Cannot add event to pending control event " + ctlWrapper.getCtlCommand());
                        } else
                        {
                            this.theContext.getQueueReadNeeded().set(true);
                        }
                    }
                }
            //NOT a Control Wrapper.....might be a Connection Event?    
            } else if (evt.getEventSource() instanceof ConnEventQueue)
            {
                conWrapper = this.theContext.getConnEventQueue().readFirst(0);

                if (conWrapper != null)
                {
                    boolean connStat = this.theContext.getPendingConn().offer(conWrapper);

                    if (!connStat)
                    {
                        myLogger.error("Cannot add event to pending conn event " + conWrapper.getConnEventType());
                    } else
                    {
                        this.theContext.getQueueReadNeeded().set(true);
                    }
                }
            //NOT a Connection Event.....might be an NSDS Transfer Event?    
            } else if (evt.getEventSource() instanceof NSDSXferEventQueue)             //both HOST and SENSOR side Open Interface msg.
            {
                //this should be the Sensor Side SPA IPC Queue -- remember, we are switching roles
                nsdsXferWrapper = this.theContext.getHPATransferEventQueue().readFirst(0);

                if (nsdsXferWrapper != null)
                {
                    boolean nsdaXferStat = this.theContext.getNsdsPendingXfer().offer(nsdsXferWrapper);

                    if (!nsdaXferStat)
                    {
                        myLogger.error("Cannot add event to pending transfer event " + nsdsXferWrapper.getXferType().name());
                    } else
                    {
                        this.theContext.getQueueReadNeeded().set(true);
                    }
                }
                
                
            } else if (evt.getEventSource() instanceof SIAXferEventQueue)             //both HOST and SENSOR side Open Interface msg.
            {
                //this should be the Sensor Side SPA IPC Queue -- remember, we are switching roles
                spaXferWrapper = this.theContext.getSpaTransferEventQueue().readFirst(0);

                if (spaXferWrapper != null)
                {
                    boolean spaXferStat = this.theContext.getSpaPendingXfer().offer(spaXferWrapper);

                    if (!spaXferStat)
                    {
                        myLogger.error("Cannot add event to pending transfer event " + spaXferWrapper.getXferType().name());
                    } else
                    {
                        this.theContext.getQueueReadNeeded().set(true);
                    }
                }       

            //NOT any recognized Event!    
            } else
            {
                myLogger.info("Unhandled IPCQueue event received: " + evt.toString());
                System.out.println((char)27 + "[32m" + "PH -> Unhandled IPCQueue event received: " + evt.toString());
                System.out.println();
            }
        } else
        {
            myLogger.error("Received queue fired for non-new event" + evt.getEventType());
        }
    }

    /**
     * Routes queued entries to handlers
     *
     * @return success or failure
     */
    private boolean routeQueueEntries()
    {
        boolean retVal = false;
        long myConn = -1L;
        if (logLevelDebug)
        {
            myLogger.debug("Routing queue entries");
            System.out.println((char)27 + "[32m" + "CCSI PH.routeQueueEntries -> Routing queue entries");
        }

        for (NSDSConnection x : this.theContext.getTheOpenInterfaceHost().getActiveConnections().getConnectionList())
        {
            if (x.getConnType() == NSDSConnectionTypeEnum.PERSISTENT_CONNECTION)
            {
                myConn = x.getConnectionId();
            }
        }

        if (this.theContext.getPendingTimer().size() > 0)
        {
            if (logLevelDebug)
            {
                myLogger.debug("Handling timer expired events");
            }

            do
            {
                TimerChangeEvent timerEvent = this.theContext.getPendingTimer().poll();

                if (timerEvent != null)
                {
                    switch (timerEvent.getTimerPurpose())
                    {
                        case PROTOCOLHANDLER_TIMER:
                            this.theContext.getTimerEventHandler().handleProtocolHandlerTimer(timerEvent);
                            retVal = true;

                            break;

                        case ACK_TIMER:
                            this.theContext.getTimerEventHandler().handleAckTimer(timerEvent);
                            retVal = true;

                            break;

                        case PERIODIC_TIMER:
                            this.theContext.getTimerEventHandler().handlePeriodicTimer(timerEvent);
                            retVal = true;

                            break;

                        case HEARTBEAT_TIMER:
                            this.theContext.getTimerEventHandler().handleHeartbeatTimer(timerEvent);
                            retVal = true;

                            break;

                        default:
                            break;
                    }
                }
            } while (this.theContext.getPendingTimer().size() > 0);
        }

        if (this.theContext.getPendingCtl().size() > 0)
        {
            CtlWrapper ctlWrapper = this.theContext.getPendingCtl().poll();

            if (ctlWrapper != null)
            {
                System.out.println((char)27 + "[32m" + "CCSI PH.routeQueueEntries -> Got a CONTROL WRAPPER..." + ctlWrapper.getCtlCommand());
                System.out.println();
                
                if (ctlWrapper.getCtlCommand() == CtlWrapper.STOP_THREAD)
                {
                    this.theContext.getShutDownNow().set(true);
                }
            }
        }

        if (this.theContext.getPendingConn().size() > 0)
        {
            do
            {
                if (logLevelDebug)
                {
                    myLogger.debug("Process conn events");
                }

                NSDSConnEvent conWrapper = this.theContext.getPendingConn().poll();

                if (conWrapper != null)
                {
                    myLogger.info("Conn: " + conWrapper.getConnEventType().name());
                    retVal = this.theContext.getConnEventHandler().handleConnEvent(conWrapper);
                } else
                {
                    retVal = false;
                }
            } while (this.theContext.getPendingConn().size() > 0);
        }

        Integer pendingIPCSize = this.theContext.getPendingIPC().size();
        if ( pendingIPCSize > 0)
        {
            if (logLevelDebug)
            {
                myLogger.info ("Process IPC events: size: " + pendingIPCSize);
            }

            do
            {
                IPCWrapper ipcWrapper = this.theContext.getPendingIPC().poll();

                if (ipcWrapper != null)
                {
                    String routingKey = ipcWrapper.getRoutingKey();
                    
                    String ccsiCmd = ipcWrapper.getTheCcsiCmd();
                    
                    System.out.println((char)27 + "[32m" + "CCSI PH.routeQueueEntries -> Got a CONTROL WRAPPER: " 
                                                                    + ccsiCmd);         //ipcWrapper.getMsgBody().getCmd().getCmd());
                    System.out.println((char)27 + "[32m" + "CCSI PH.routeQueueEntries -> Got a CONTROL WRAPPER (routing key)" 
                                                                    + ipcWrapper.getRoutingKey());                    
                    System.out.println();

                    if (routingKey == null)
                    {
                        myLogger.error ("Routing key is null - discarding message");
                    } else if (routingKey.equalsIgnoreCase("ccsi.event.reading"))
                    {
                        retVal = this.theContext.getSensorEventHandler().receiveSensorReadings(ipcWrapper,
                                this.theContext.getTheOpenInterfaceHost());
                    } else if (routingKey.equalsIgnoreCase("ccsi.event.bit"))
                    {
                        retVal = this.theContext.getSensorEventHandler().receiveSensorBIT(ipcWrapper,
                                this.theContext.getTheOpenInterfaceHost());
                    } else if (routingKey.equalsIgnoreCase("ccsi.event.consumable"))
                    {
                        retVal = this.theContext.getSensorEventHandler().receiveSensorConsumable(ipcWrapper,
                                this.theContext.getTheOpenInterfaceHost());
                    } else if (routingKey.equalsIgnoreCase("ccsi.event.status"))
                    {
                        retVal = this.theContext.getSensorEventHandler().receiveSensorStatus(ipcWrapper,
                                this.theContext.getTheOpenInterfaceHost());
                    } else if (routingKey.equalsIgnoreCase("ccsi.cmd.Get_Identification"))
                    {
                        //ASSUMING THE SIZE WILL *ALWAYS* BE 1 (for msgList).
                        //ASSUMING THE SIZE WILL *ALWAYS* BE 1 (for msgList).
                        String Uuid = null;
                        CCSIMsgType ccsiMsg = new CCSIMsgType();
                        ccsiMsg = ccsiMsg.parseMsg(ccsiCmd);          //ipcWrapper.getMsgBody().getCmd().getCmd());
                        CCSIDocType doc = ccsiMsg.getCCSIDoc();
                        List<CCSIMsg> msgList = doc.getMsg();
                        int i = msgList.size();
                        for (int j = 0; j<i; j++) {
                            CCSIMsg msg;
                            msg = msgList.get(j);
                            IdentificationReportType irt = new IdentificationReportType();
                            irt = msg.getIdentChn();
                            List<SensorIdentificationDataInstanceType> sidit = irt.getInstance();
                            MaintPointOfContactType mpoct = irt.getMaintPOC();
                            int z = sidit.size();
                            for (int h = 0; h<z; h++) {
                                SensorIdentificationDataInstanceType unit = sidit.get(h);  
                                
                                VersionType vt = unit.getInstanceCcsiVersion();  
                                VersionNumberType ccsiVersion = new VersionNumberType();
                                ccsiVersion.setMajor(vt.getMajor());
                                ccsiVersion.setMinor(vt.getMinor());
                                ccsiVersion.setModerate(vt.getModerate());
                                ccsiVersion.setPatch(vt.getPatch());
                                this.theContext.getTheConfig().setCcsiVersion(ccsiVersion);
                                
                                this.theContext.getTheConfig().setSensorName(unit.getSensorName());
                                this.theContext.getTheConfig().setSensorHostID(unit.getInstanceHostId());
                                Uuid = unit.getInstanceHostId();
                            }
                        }
                                
                        String xmlRegister = "<CCSIDoc>\n" +
                            "<Msg>\n" +
                            "<CmdChn Cmd=\"Register\">\n" +
                            "<Arg>\n" +
                            "<ArgName>Channel</ArgName>\n" +
                            "<ArgValue>HRTBT</ArgValue>\n" +
                            "</Arg>\n" +
                            "<Arg>\n" +
                            "<ArgName>Type</ArgName>\n" +
                            "<ArgValue>PERIOD</ArgValue>\n" +
                            "</Arg>\n" +
                            "<Arg>\n" +
                            "<ArgName>Rate</ArgName>\n" +
                            "<ArgValue>15</ArgValue>\n" +
                            "</Arg>\n" +
                            "</CmdChn>\n" +
                            "</Msg>\n" +
                            "</CCSIDoc>\n" +
                            "</ccsi:Hdr>";       
                        
                        IPCWrapper wrapper = new IPCWrapper();
                        wrapper.setXMLCmd(xmlRegister);   
                        System.out.println((char)27 + "[32m" + ">>>>>>>>>>  PH.routeQueueEntries => sending REGISTRATION MSG.");
                        System.out.println();
                        CcsiPendingAck ack = new CcsiPendingAck();
                        MSN msn = MSN.getMSN();
                        ack.setMsn(msn.getCurrentMSN()+1);
                        CcsiChannelEnum chan[] = new CcsiChannelEnum[]{CcsiChannelEnum.HRTBT};
                        ack.setChannels(chan);
                        ack.setTimer(15);
                        theContext.getPendingAcks().addPendingAck(ack);
                        theContext.getSensorOutQueue().insertLast(wrapper);                               
                                
                    } else if (routingKey.startsWith("ccsi.evt"))
                    {
                        retVal = this.theContext.getSensorEventHandler().receiveCmdResponse(ipcWrapper,
                                this.theContext.getTheOpenInterfaceHost());
                    } else if (routingKey.startsWith("ccsi.cmd"))
                    {
//PARSE THE ACK USING JAXB.
//PARSE THE ACK USING JAXB.
//PARSE THE ACK USING JAXB.
                                       
                        //THECONTEZT HAS THE PENDING ACKS, and there really should only be 1 
                                            //(the exception, a register command with multiple channels).
                        
                        //NAK??   1) retransmit the REGISTRATION (or whatever command it was) WITH AN "R" flag, using the same MSN.
                        
                        if (!ccsiCmd.isEmpty()) {
                            
                            //GET THE CCSI MSG.
                            CCSIMsgType ccsiMsg = new CCSIMsgType();
                            ccsiMsg = ccsiMsg.parseMsg(ccsiCmd);         //ipcWrapper.getMsgBody().getCmd().getCmd());
                                
                            //DO WE HAVE AN ACK?????
                            //DO WE HAVE AN ACK?????
                            //DO WE HAVE AN ACK?????
                            if (ccsiCmd.contains("ack=\"ACK\"")) {
                                CcsiPendingAckList pal = theContext.getPendingAcks();
                                
                                if (pal.hasPendingAck(ccsiMsg.getMsn().longValue())) {
                                    
//TODO                              //IF ACK FOR HEARTBEAT, NEED TO SET PERSISTENT CONNECTION.    TODO TODO TODO
//TODO                              //IF ACK FOR HEARTBEAT, NEED TO SET PERSISTENT CONNECTION.    TODO TODO TODO
//TODO                              //IF ACK FOR HEARTBEAT, NEED TO SET PERSISTENT CONNECTION.    TODO TODO TODO
                                    
                                    //Acknowledge the ACK.
                                    pal.ackReceived(ccsiMsg.getMsn().longValue());
                                    
                                    
                                } else {
                                    //WE NEED TO RETRANSMIT THE REGISTRATION MESSAGE, BUT NOT DOING RIGHT NOW.  
                                    //RETRANSMIT WITH "R" FLAG, USING SAME MSN.
                                }
                                
                                
                                
                                
                            //DO WE HAVE AN NACK?????
                            //DO WE HAVE AN NACK?????
                            //DO WE HAVE AN NACK?????                            
                            } else if (ccsiCmd.contains("ack=\"NACK\"")) {
                            
                            
                            
                            
                            }
                        }
                        
                        
                        
                        myLogger.error("Received command " + routingKey + " from the sensor, NOT A RESPONSE.");
                    } else
                    {
                        myLogger.error("Invalid routing key received from sensing component " + routingKey);
                    }
                } else
                {
                    retVal = false;
                }
            } while (this.theContext.getPendingIPC().size() > 0);
        }

        if (this.theContext.getNsdsPendingXfer().size() > 0)
        {
            if (logLevelDebug)
            {
                myLogger.debug("Process pending transfer.");
            }

            do
            {
                NSDSTransferEvent nsdsXferWrapper = this.theContext.getNsdsPendingXfer().poll();

                if (nsdsXferWrapper != null)
                {
                    switch (nsdsXferWrapper.getXferType())
                    {
                        case XFER_RCV_ACK:
                            retVal = this.theContext.getNsdsTransferEventHandler().handleRcvAck(nsdsXferWrapper);

                            break;

                        case XFER_RCV_HRTBT:
                            retVal = this.theContext.getNsdsTransferEventHandler().handleRcvHrtbt(nsdsXferWrapper);

                            break;

                        case XFER_RCV_CMD:
                            retVal = this.theContext.getNsdsTransferEventHandler().handleRcvCmd(nsdsXferWrapper);

                            break;

                        case XFER_SENT_ACK:
                            retVal = this.theContext.getNsdsTransferEventHandler().handleSentAck(nsdsXferWrapper);

                            break;

                        case XFER_SENT_HRTBT:
                            retVal = this.theContext.getNsdsTransferEventHandler().handleSentHrtbt(nsdsXferWrapper);

                            break;

                        case XFER_SENT_REPORT:
                            retVal = this.theContext.getNsdsTransferEventHandler().handleSentReport(nsdsXferWrapper);

                            break;
                    }
                } else
                {
                    retVal = false;
                }
            } while (this.theContext.getNsdsPendingXfer().size() > 0);
        }

        
        
        
        
        if (this.theContext.getSpaPendingXfer().size() > 0)
        {
            if (logLevelDebug)
            {
                myLogger.debug("Process pending transfer.");
            }

            do
            {
                SIATransferEvent spaXferWrapper = this.theContext.getSpaPendingXfer().poll();

                if (spaXferWrapper != null)
                {
                    switch (spaXferWrapper.getXferType())
                    {
                        case XFER_RCV_ACK:
                            retVal = this.theContext.getSpaTransferEventHandler().handleRcvAck(spaXferWrapper);

                            break;

                        case XFER_RCV_HRTBT:
                            retVal = this.theContext.getSpaTransferEventHandler().handleRcvHrtbt(spaXferWrapper);

                            break;

                        case XFER_RCV_CMD:
                            retVal = this.theContext.getSpaTransferEventHandler().handleRcvCmd(spaXferWrapper);

                            break;

                        case XFER_SENT_ACK:
                            retVal = this.theContext.getSpaTransferEventHandler().handleSentAck(spaXferWrapper);

                            break;

                        case XFER_SENT_HRTBT:
                            retVal = this.theContext.getSpaTransferEventHandler().handleSentHrtbt(spaXferWrapper);

                            break;

                        case XFER_SENT_REPORT:
                            retVal = this.theContext.getSpaTransferEventHandler().handleSentReport(spaXferWrapper);

                            break;
                    }
                } else
                {
                    retVal = false;
                }
            } while (this.theContext.getNsdsPendingXfer().size() > 0);
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        if (this.theContext.getPendingEventGen().isNeedsProcessing())
        {
            do
            {
                PendingEventGeneration x = this.theContext.getPendingEventGen().poll();

                if (logLevelDebug)
                {
                    myLogger.debug("Processing pending event gen " + x.getEventChannel().name());
                }

                switch (x.getEventChannel())
                {
                    case ALERTS:
                        String aMsg
                                = this.theContext.getReportGenerator().
                                        genFullAlertReport(this.theContext.getPendingAlerts());
                        PendingSendEntry apse = new PendingSendEntry();

                        apse.addChannel(CcsiChannelEnum.ALERTS);
                        apse.setMessage(aMsg);
                        apse.setAckRequired(true);
                        apse.setConnectionId(myConn);
                        apse.setSendReport(true);
                        this.theContext.getPendingSends().addPendingSend(apse);
                        myLogger.info("Sending ALERTS");

                        break;

                    case CONFIG:
                        StringBuilder msg;
                        if (!this.theContext.getPendingConfigList().isEmpty())
                        {
                            String theKey = "ALL";
                            msg = new StringBuilder("<Msg><").append(CcsiChannelEnum.CONFIG.getChanTag()).append('>');
                            for (CcsiConfigBlockEnum xx : this.theContext.getPendingConfigList().getTheList())
                            {
                                msg.append(this.theContext.getReportGenerator().getPartialConfigReport(xx, theKey));
                            }
                            msg.append("</").append(CcsiChannelEnum.CONFIG.getChanTag()).append("></Msg>");
                            this.theContext.getPendingConfigList().clear();
                        } else
                        {
                            //msg = new StringBuilder("<Msg><").append(CcsiChannelEnum.CONFIG.getChanTag()).append('>');
                            msg = new StringBuilder();
                            msg.append(this.theContext.getReportGenerator().genFullConfigReport());
                            //msg.append("</").append(CcsiChannelEnum.CONFIG.getChanTag()).append("></Msg>");
                        }
                        PendingSendEntry cpse = new PendingSendEntry();

                        cpse.addChannel(CcsiChannelEnum.CONFIG);
                        cpse.setMessage(msg.toString());
                        cpse.setAckRequired(true);
                        cpse.setConnectionId(myConn);
                        cpse.setSendReport(true);
                        this.theContext.getPendingSends().addPendingSend(cpse);

                        break;

                    case READGS:
                        String rMsg
                                = this.theContext.getReportGenerator().genReadingReport(this.theContext.getPendingReadings());
                        PendingSendEntry rpse = new PendingSendEntry();
                        //Integer msn  = MSN.getMSN().getNextMSN();
                        //rpse.setAckMsn(msn);
                        rpse.addChannel(CcsiChannelEnum.READGS);
                        rpse.setMessage(rMsg);
                        rpse.setAckRequired(false);
                        rpse.setConnectionId(myConn);
                        rpse.setSendReport(true);
                        this.theContext.getPendingSends().addPendingSend(rpse);

                        break;

                    case STATUS:

                        ArrayList<String> items = x.getItems();
                        String oneItem = null;

                        if ((items != null) && (!items.isEmpty()))
                        {
                            oneItem = items.get(0);
                        }

                        String sMsg = this.theContext.getReportGenerator().genEventStatusReport(oneItem, myConn,
                                this.theContext.getPendingNakDetails(), this.theContext.getPendingStatusList());
                        PendingSendEntry spse = new PendingSendEntry();

                        spse.addChannel(CcsiChannelEnum.STATUS);
                        spse.setMessage(sMsg);
                        spse.setAckRequired(true);
                        spse.setConnectionId(myConn);
                        spse.setSendReport(true);
                        this.theContext.getPendingSends().addPendingSend(spse);

                        break;

                    case MAINT:
                        String mMsg = this.theContext.getReportGenerator().genFullMaintReport(this.theContext.getPendingBits(),
                                this.theContext.getPendingConsumables(), this.theContext.getPendingFailures());
                        PendingSendEntry mpse = new PendingSendEntry();

                        mpse.addChannel(CcsiChannelEnum.MAINT);
                        mpse.setMessage(mMsg);
                        mpse.setAckRequired(true);
                        mpse.setConnectionId(myConn);
                        mpse.setSendReport(true);
                        this.theContext.getPendingSends().addPendingSend(mpse);

                        break;

                    case IDENT:
                        String iMsg = this.theContext.getReportGenerator().genIdentReport();
                        PendingSendEntry ipse = new PendingSendEntry();

                        ipse.addChannel(CcsiChannelEnum.IDENT);
                        ipse.setMessage(iMsg);
                        ipse.setAckRequired(true);
                        ipse.setConnectionId(myConn);
                        ipse.setSendReport(true);
                        this.theContext.getPendingSends().addPendingSend(ipse);

                        break;
                        
//NO HEARTBEAT?>!?>!?>!?>!>?>!!?>?!?!?!??!?!?!?!?!?!?>
//NO HEARTBEAT?>!?>!?>!?>!>?>!!?>?!?!?!??!?!?!?!?!?!?>
//NO HEARTBEAT?>!?>!?>!?>!>?>!!?>?!?!?!??!?!?!?!?!?!?>
//NO HEARTBEAT?>!?>!?>!?>!>?>!!?>?!?!?!??!?!?!?!?!?!?>
//NO HEARTBEAT?>!?>!?>!?>!>?>!!?>?!?!?!??!?!?!?!?!?!?>
//NO HEARTBEAT?>!?>!?>!?>!>?>!!?>?!?!?!??!?!?!?!?!?!?>
                        
                        
                    default:
                        break;
                }
            } while (this.theContext.getPendingEventGen().isNeedsProcessing());
        }

        if (this.theContext.getPendingTimedGen().isNeedsProcessing())
        {
            do
            {
                PendingTimedGeneration x = this.theContext.getPendingTimedGen().poll();

                if (logLevelDebug)
                {
                    myLogger.debug("Processing timed generation " + x.getEventChannel().name());
                }

                switch (x.getEventChannel())
                {
                    case ALERTS:
                        String aMsg
                                = this.theContext.getReportGenerator().
                                        genFullAlertReport(this.theContext.getPendingAlerts());
                        PendingSendEntry apse = new PendingSendEntry();

                        apse.addChannel(CcsiChannelEnum.ALERTS);
                        apse.setMessage(aMsg);
                        apse.setAckRequired(true);
                        apse.setConnectionId(myConn);
                        apse.setSendReport(true);
                        this.theContext.getPendingSends().addPendingSend(apse);

                        break;

                    case CONFIG:
                        StringBuilder msg;
                        if (!this.theContext.getPendingConfigList().isEmpty())
                        {
                            String theKey = "ALL";
                            msg = new StringBuilder("<Msg><").append(CcsiChannelEnum.CONFIG.getChanTag()).append('>');
                            for (CcsiConfigBlockEnum xx : this.theContext.getPendingConfigList().getTheList())
                            {
                                msg.append(this.theContext.getReportGenerator().getPartialConfigReport(xx, theKey));
                            }
                            msg.append("</").append(CcsiChannelEnum.CONFIG.getChanTag()).append("></Msg>");
                            this.theContext.getPendingConfigList().clear();
                        } else
                        {
                            msg = new StringBuilder("<Msg><").append(CcsiChannelEnum.CONFIG.getChanTag()).append('>');
                            msg.append(this.theContext.getReportGenerator().getPartialConfigReport(CcsiConfigBlockEnum.BASE, null));
                            msg.append("</").append(CcsiChannelEnum.CONFIG.getChanTag()).append("></Msg>");
                        }
                        PendingSendEntry cpse = new PendingSendEntry();

                        cpse.addChannel(CcsiChannelEnum.CONFIG);
                        cpse.setMessage(msg.toString());
                        cpse.setAckRequired(true);
                        cpse.setConnectionId(myConn);
                        cpse.setSendReport(true);
                        this.theContext.getPendingSends().addPendingSend(cpse);

                        break;

                    case READGS:
                        String rMsg
                                = this.theContext.getReportGenerator().genReadingReport(this.theContext.getPendingReadings());
                        PendingSendEntry rpse = new PendingSendEntry();

                        rpse.addChannel(CcsiChannelEnum.READGS);
                        rpse.setMessage(rMsg);
                        rpse.setAckRequired(true);
                        rpse.setConnectionId(myConn);
                        rpse.setSendReport(true);
                        this.theContext.getPendingSends().addPendingSend(rpse);
                        if (logLevelDebug)
                        {
                            myLogger.debug("Sending READGS");
                        }

                        break;

                    case STATUS:

                        String sMsg = this.theContext.getReportGenerator().genEventStatusReport(null, myConn,
                                this.theContext.getPendingNakDetails(), this.theContext.getPendingStatusList());
                        PendingSendEntry spse = new PendingSendEntry();

                        spse.addChannel(CcsiChannelEnum.STATUS);
                        spse.setMessage(sMsg);
                        spse.setAckRequired(true);
                        spse.setConnectionId(myConn);
                        spse.setSendReport(true);
                        this.theContext.getPendingSends().addPendingSend(spse);

                        break;

                    case MAINT:
                        PendingSendEntry mpse = new PendingSendEntry();

                        mpse.addChannel(CcsiChannelEnum.MAINT);

                        String mMsg = this.theContext.getReportGenerator().genFullMaintReport(this.theContext.getPendingBits(),
                                this.theContext.getPendingConsumables(), this.theContext.getPendingFailures());

                        mpse.setMessage(mMsg);
                        mpse.setAckRequired(true);
                        mpse.setConnectionId(myConn);
                        mpse.setSendReport(true);
                        this.theContext.getPendingSends().addPendingSend(mpse);

                        break;

                    case IDENT:
                        String iMsg = this.theContext.getReportGenerator().genIdentReport();
                        PendingSendEntry ipse = new PendingSendEntry();

                        ipse.setAckRequired(true);
                        ipse.setMessage(iMsg);
                        ipse.addChannel(CcsiChannelEnum.IDENT);
                        ipse.setConnectionId(myConn);
                        ipse.setSendReport(true);
                        this.theContext.getPendingSends().addPendingSend(ipse);

                        break;
                        
//NO HEARTBEAT?>!?>!?>!?>!>?>!!?>?!?!?!??!?!?!?!?!?!?>
//NO HEARTBEAT?>!?>!?>!?>!>?>!!?>?!?!?!??!?!?!?!?!?!?>
//NO HEARTBEAT?>!?>!?>!?>!>?>!!?>?!?!?!??!?!?!?!?!?!?>
//NO HEARTBEAT?>!?>!?>!?>!>?>!!?>?!?!?!??!?!?!?!?!?!?>
//NO HEARTBEAT?>!?>!?>!?>!>?>!!?>?!?!?!??!?!?!?!?!?!?>
//NO HEARTBEAT?>!?>!?>!?>!>?>!!?>?!?!?!??!?!?!?!?!?!?>
                        
                    default:
                        break;
                }
            } while (this.theContext.getPendingEventGen().isNeedsProcessing());
        }
if (this.theContext.getPendingSends().getSize() > 0)
        //if (this.theContext.getPendingSends().needsProcessing())
        {
            if (logLevelDebug)
            {
                myLogger.info("Pending sends has " + this.theContext.getPendingSends().getSize() + " entries.");
            }

            PendingSendEntry temp = this.theContext.getPendingSends().peekNextEntry();
            //myLogger.info ("Was Sent: " + temp.wasMessageSent() + " " + temp.getMessage());
            if ((temp != null) && (temp.wasMessageSent() == false)) {
                if (logLevelDebug) {
                    String type = "";
                    if (temp.isSendReport()) {
                        type = type + " report";
                    }
                    if (temp.isAckRequired()) {
                        type = type + " ackRequired";
                    }
                    if (temp.isSendAck()) {
                        type = type + " sendAck";
                    }
                    if (temp.isSendDisconnect()) {
                        type = type + " disconnect";
                    }
                    if (temp.isSendHrtbt()) {
                        type = type + " heartbeat";
                    }
                    if (temp.isSendNak()) {
                        type = type + " nak";
                    }

                    //myLogger.info("Processing pending send: " + temp.getMessage() + " type: " + type);
                } else
                {
                    //myLogger.info ("Was Sent true");
                }

                if (temp.isSendDisconnect())
                {
                    this.theContext.getTheOpenInterfaceHost().disconnect(temp.getConnectionId());
                    this.theContext.getPendingSends().removeNextEntry();
                } else if (temp.isSendAck())
                {
                    this.theContext.getTheHPAInterface().sendAck(temp.getConnectionId(), temp.getChannel()[0],
                            temp.getAckMsn());
                } else if (temp.isSendHrtbt())
                {
                    this.theContext.getTheHPAInterface().sendHrtbt(temp.getConnectionId());
                } else if (temp.isSendNak())
                {
                    ChannelRegistrationType statusReg = this.getRegistration(temp.getConnectionId(), CcsiChannelEnum.STATUS);

                    if ((statusReg != null) && (statusReg.getRegType() == CcsiChannelRegistrationEnum.PERIOD))
                    {
                        PendingNakDetailEntry nakDtl = new PendingNakDetailEntry();

                        nakDtl.setConnectionId(temp.getConnectionId());
                        nakDtl.setNakMsn(temp.getAckMsn());
                        nakDtl.setNakDetail(temp.getNakDetailCode());
                        nakDtl.setDetailText(temp.getNakDetailMessage());
                        this.theContext.getPendingNakDetails().addEntry(nakDtl);
                    } else if ((statusReg != null) && (statusReg.getRegType() == CcsiChannelRegistrationEnum.EVENT))
                    {
                        PendingNakDetailEntry nakDtl = new PendingNakDetailEntry();

                        nakDtl.setConnectionId(temp.getConnectionId());
                        nakDtl.setNakMsn(temp.getAckMsn());
                        nakDtl.setNakDetail(temp.getNakDetailCode());
                        nakDtl.setDetailText(temp.getNakDetailMessage());
                        this.theContext.getPendingNakDetails().addEntry(nakDtl);
                        this.theContext.getPendingEventGen().addPendingEvent(
                                new PendingEventGeneration(CcsiChannelEnum.STATUS));
                    }

                    this.theContext.getTheHPAInterface().sendNak(temp.getConnectionId(), temp.getChannel()[0],
                            temp.getAckMsn(), temp.getNakCode(), temp.getNakDetailCode(), temp.getNakDetailMessage());
                } else if (temp.isSendReport())
                {
                    this.theContext.getTheHPAInterface().sendReport(temp.getConnectionId(), temp.getChannel(),
                            temp.getCacheKeys(), temp.getMessage(), temp.getAckType(), 
                            temp.getAckMsn(), temp.getNakCode(), temp.isAckRequired());
                    if (temp.getMessage().startsWith("<Msg><ConfigChn>"))
                    {
                        this.theContext.getPendingSends().removeNextEntry();
                    }
                }

                temp.setMessageSent();
                
                if (temp.isAckRequired())
                {
                    if (logLevelDebug)
                    {
                        myLogger.debug("Set up pending ACK for " + temp.getChannel()[0]);
                    }
 
                    CcsiPendingAck work = new CcsiPendingAck();

                    work.setChannels(temp.getChannel());
                    work.setMsn(-1L);
                    work.setTimer(-1L);
                    theContext.getPendingAcks().addPendingAck(work);
                }
            }
        }

        if ((theContext.getPendingConn().isEmpty()) 
                && (theContext.getPendingCtl().isEmpty())
                && (theContext.getPendingIPC().isEmpty()) 
                && (theContext.getNsdsPendingXfer().isEmpty())
                && (theContext.getSpaPendingXfer().isEmpty())
                && (theContext.getPendingTimer().isEmpty()) 
                && (theContext.getPendingEventGen().isEmpty())
                && (theContext.getPendingTimedGen().isEmpty()))
        {
            if (logLevelDebug)
            {
                myLogger.debug("Setting needs processing to false.");
            }
            theContext.getQueueReadNeeded().set(false);
        }

        return (retVal);
    }
    
    
    public PHContext getTheContext() {
        return this.theContext;
    }

    
    /**
     * This is the thread that runs after a startup command is received.
     */
    @Override
    public void run()
    {
//        System.out.println("CCSI PH.run -> starting.....");
        this.theContext.getTheSavedState().getPendingAcks().clearList();

        Object syncObject = new Object ();
//        String emptyString = "";


//This should be the code block for Host Connection, I think.    KBM 7/30/2020
//KBM  START:   Origianl code block.
//        //
//        // Start the connection
//        //
//        NSDSLinkList links = this.theContext.getTheOpenInterfaceHost().getAvailableLinks();
//
//        if ((links != null) && (links.getLinkList().size() > 0))
//        {
//            for (NSDSLink x : links.getLinkList())
//            {
//                this.theContext.getTheOpenInterfaceHost().connect(x.getLinkId());
//            }
//        } else
//        {
//            myLogger.error("No link available!!!!!");
//        }
//KBM  END:   Origianl code block.      



//Tom said to use the next code block to get a Socket Connection.   It produces a NullPointerExecption.
//I am, however, able to get a Socket Connection in SparkMain constructor after I instantiate the SIA.
        //
        // Start the connection
        //     
//as of 7/30/2020, the Socket Connection is successfully being done in SparkMain constructor upon instantiation
//   of the SIA.    Therefore, the follow code block should be not needed.
//        System.out.println("CCSI PH.run -> starting OI Connect!");
//        
//        NSDSLinkList links = this.theContext.getTheOpenInterfaceSensor().getAvailableLinks();
//
//        if ((links != null) && (links.getLinkList().size() > 0))
//        {
//            for (NSDSLink x : links.getLinkList())
//            {
//                this.theContext.getTheOpenInterfaceSensor().connect(x.getLinkId());
//            }
//        } else
//        {
//            myLogger.error("No link available!!!!!");
//        }

        


        try
        {
            while (!this.theContext.getShutDownNow().get())
            {
                try
                {
//                    System.out.println("CCSI PH.run -> running.....");
                    synchronized (syncObject)
                    {
                        syncObject.wait(500L);
                    }

                    if ((this.theContext.getQueueReadNeeded().get()) || (!this.theContext.getPendingEventGen().isEmpty())
                            || (!this.theContext.getPendingTimedGen().isEmpty()) 
                            || (this.theContext.getPendingSends().getSize() > 0))
                    {
                        this.routeQueueEntries();
                    }
                } catch (InterruptedException ex)
                {
                    myLogger.debug("Interrupted exception" + ex);
                } catch (Exception ex)
                {
                    myLogger.error("Exception waiting for input", ex);
                }
            }
        } catch (Exception ex)
        {
            myLogger.error("Exception during run, exiting", ex);
        } finally
        {
            myLogger.info("Exiting the Protocol Handler");
        }
    }

    /**
     * Event action handler for timer events
     *
     * @param evt the timer event that occurred
     */
    @Override
    public void timerExpired(TimerChangeEvent evt)
    {
        switch (evt.getTimerEvent())
        {
            case TimerChangeEvent.TEST_TIMER_EXPIRED:
                switch (evt.getTimerPurpose())
                {
                    case PROTOCOLHANDLER_TIMER:
                        this.theContext.getPendingTimer().add(evt);
                        this.theContext.getQueueReadNeeded().set(true);

                        break;

                    case ACK_TIMER:
                        this.theContext.getPendingTimer().add(evt);
                        this.theContext.getQueueReadNeeded().set(true);

                        break;

                    case PERIODIC_TIMER:
                        this.theContext.getPendingTimer().add(evt);
                        this.theContext.getQueueReadNeeded().set(true);

                        break;

                    case HEARTBEAT_TIMER:
                        this.theContext.getPendingTimer().add(evt);
                        this.theContext.getQueueReadNeeded().set(true);

                        break;

                    case CONNRETRY_TIMER:
                        this.theContext.getPendingTimer().add(evt);
                        this.theContext.getQueueReadNeeded().set(true);

                        break;

                    default:
                        break;
                }

                break;

            default:
                break;
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Returns the registration type for the specified channel
     *
     * @param conn the NSDSConnection
     * @param which the channel to be found
     *
     * @return the type of registration for that channel
     */
    private ChannelRegistrationType getRegistration(long conn, CcsiChannelEnum which)
    {
        ChannelRegistrationType retVal = null;
        NSDSConnection theConn = null;

        if (conn >= 0L)
        {
            for (NSDSConnection z : this.theContext.getTheSavedState().getConnList().getConnectionList())
            {
                if (z.getConnectionId() == conn)
                {
                    theConn = z;

                    break;
                }
            }
        }

        if (theConn != null)
        {
            if ((theConn.getRegisteredChannels() != null) && (!theConn.getRegisteredChannels().isEmpty())
                    && (theConn.getRegisteredChannels().hasChannel(which)))
            {
                retVal = theConn.getRegisteredChannels().getRegistration(which);
            }
        }

        return (retVal);
    }

    /**
     * Returns the registration type for the specified channel
     *
     * @param conn the NSDSConnection
     * @param which the channel to be found
     *
     * @return whether or no details have been requested
     */
    private boolean getRegistrationDetails(long conn, CcsiChannelEnum which)
    {
        boolean retVal = false;
        NSDSConnection theConn = null;

        if (conn >= 0L)
        {
            for (NSDSConnection z : this.theContext.getTheSavedState().getConnList().getConnectionList())
            {
                if (z.getConnectionId() == conn)
                {
                    theConn = z;

                    break;
                }
            }
        }

        if (theConn != null)
        {
            if ((theConn.getRegisteredChannels() != null) && (!theConn.getRegisteredChannels().isEmpty())
                    && (theConn.getRegisteredChannels().hasChannel(which)))
            {
                retVal = theConn.getRegisteredChannels().getRegistration(which).isDetails();
            }
        }

        return (retVal);
    }
}
