/*
 * DataValidityMinLength.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

/**
 * This class implements a minimum string length data validity type.  The minimum
 * length must be
 *
 * @author kmiller
 */
public class DataValidityMinLength implements DataValidityInterface
{
  /** The type of validity check. */
  private final DataValidityType	theType	= DataValidityType.MINSTRINGLENGTH;

  /** The minimum valid string length. */
  private int	minLength	= Short.MIN_VALUE;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an empty instance.
   */
  public DataValidityMinLength()
  {
  }

  /**
   * Constructs an instance of the class with a minimum value.
   *
   * @param min int containing the minimum valid string length
   *
   * @throws IllegalArgumentException
   */
  public DataValidityMinLength( int min ) throws IllegalArgumentException
  {
    if ( min > 0 )
    {
      this.minLength	= min;
    }
    else
    {
      throw new IllegalArgumentException( "Minimum string length must be > 0" );
    }
  }

  /**
   * Constructs an instance of the class with a minimum value.
   *
   * @param min String containing the minimum valid string length
   *
   * @throws IllegalArgumentException
   * @throws NumberFormatException
   */
  public DataValidityMinLength( String min ) throws IllegalArgumentException , NumberFormatException
  {
    if ( ( min != null ) && ( min.trim().length() > 0 ) )
    {
      this.minLength	= Short.parseShort( min.trim() );
    }
    else
    {
      throw new IllegalArgumentException( "Minimum string length must be > 0" );
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the validity check type of this class.
   *
   * @return DataValidityType which will be MINSTRINGLENGTH
   */
  @Override
  public DataValidityType getTheType()
  {
    return ( this.theType );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Validates an input string and returns an indication if it meets the minimum
   * length requirement.  The value is considered valid if it is not null, and if
   * the length, including any spaces, is &le; the minimum required length.  If the
   * value is not set the validation will also be true.
   *
   * @param value String whose length is to be validated
   *
   * @return boolean indication of valid (true) or invalid (false)
   */
  @Override
  public boolean validateValue( String value )
  {
    boolean	retValue	= true;

    if ( this.getMinLength() > 0 )
    {
      if ( ( value == null ) || ( value.length() < (int) this.getMinLength() ) )
      {
        retValue	= false;
      }
    }

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the minLength
   */
  public int getMinLength()
  {
    return minLength;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param minLength the minLength to set
   *
   * @throws IllegalArgumentException
   */
  public void setMinLength( short minLength ) throws IllegalArgumentException
  {
    if ( minLength > 0 )
    {
      this.minLength	= minLength;
    }
    else
    {
      throw new IllegalArgumentException( "The minimum length must be > 0" );
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the description of what is valid using this validator.
   *
   * @return String containing the description
   */
  @Override
  public String getValidityString()
  {
    StringBuilder	msg	= new StringBuilder( " length must be >= " );

    msg.append( this.minLength );

    return ( msg.toString() );
  }
}
