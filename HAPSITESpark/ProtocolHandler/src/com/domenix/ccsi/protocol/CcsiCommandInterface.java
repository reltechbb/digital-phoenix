/*
 * CcsiCommandInterface.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Interface for command processing of CCSI standard and sensor 
 * unique commands. 
 *
 * Version: V1.0  15/10/07
 */


package com.domenix.ccsi.protocol;

/**
 * Interface for command processing of CCSI standard and sensor 
 * unique commands.  Commands must:
 * 
 * 1 - Maintain an internal list of arguments for execution
 * 2 - Emit NAK information through the context
 * 3 - Execute commands using context provided information
 * 4 - Trigger reports required through the context
 * 
 * @author kmiller
 */
public interface CcsiCommandInterface
{
  /**
   * Validate the command for supported, correct execution state, 
   * and argument validity.
   *
   * @param context the protocol handler context
   * @param cmdArgList the command arguments
   *
   * @return flag indicating valid (true) or not valid (false)
   */
  public boolean validate( PHContext context , CommandArgumentList cmdArgList );

  /**
   * Execute a valid command.
   *
   * @param context the protocol handler context
   *
   * @return flag indicating success (true) or failure (false)
   */
  public boolean execute( PHContext context );
}
