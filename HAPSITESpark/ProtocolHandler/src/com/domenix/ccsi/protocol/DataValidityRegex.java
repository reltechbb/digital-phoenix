/*
 * DataValidityRegex.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a regular expression data validity check.
 *
 * @author kmiller
 */
public class DataValidityRegex implements DataValidityInterface
{
  /** The regular expression pattern used to validate. */
  private Pattern	thePattern	= null;

  /** The type of validity checking defined. */
  private final DataValidityType	theType	= DataValidityType.REGEX;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance of the validator using a precompiled pattern.
   *
   * @param pat Pattern used for validation
   */
  public DataValidityRegex( Pattern pat )
  {
    this.thePattern	= pat;
  }

  /**
   * Constructs an instance of the validator using a string parameter that is compiled
   * into a Pattern.
   *
   * @param pat String containing the pattern to be used.
   *
   * @throws PatternSyntaxException
   */
  public DataValidityRegex( String pat ) throws PatternSyntaxException
  {
    this.thePattern	= Pattern.compile( pat );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the validator type which is always REGEX
   *
   * @return DataValidityType.REGEX
   */
  @Override
  public DataValidityType getTheType()
  {
    return ( theType );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Validates the input against the pattern and returns a valid/not valid indication
   *
   * @param value String containing the value to be validated
   *
   * @return boolean valid (true) or not valid (false)
   */
  @Override
  public boolean validateValue( String value )
  {
    Matcher	m1	= thePattern.matcher( value );

    return ( m1.matches() );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the description of what is valid using this validator.
   *
   * @return String containing the description
   */
  @Override
  public String getValidityString()
  {
    StringBuilder	msg	= new StringBuilder( " must match: " );

    msg.append( thePattern.toString() );

    return ( msg.toString() );
  }
}
