/*
 * DataValidityInterface.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

/**
 * Interface implemented for all data validity types.
 *
 * @author kmiller
 */
public interface DataValidityInterface
{
  /**
   * Returns the type of validity checking defined.
   *
   * @return DataValidityType for this entry.
   */
  public DataValidityType getTheType();

  //~--- methods --------------------------------------------------------------

  /**
   * Validate the supplied value.
   *
   * @param value String containing the data element value
   *
   * @return boolean indicator of valid (true) or not valid (false)
   */
  public boolean validateValue( String value );

  //~--- get methods ----------------------------------------------------------

  /**
   * Get validity description string.
   *
   * @return String containing the description of the valid value(s)
   */
  public String getValidityString();
}
