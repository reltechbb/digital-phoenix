/*
 * PendingNakDetailList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains a list of NAK details being held for later transmit.
 *
 * Version: V1.0  15/9/28
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a list of buffered NAKs that allows retrieval, searching, and removing
 * by the connection ID of the entry as well as list erasure.
 *
 * @author kmiller
 */
public class PendingNakDetailList
{
  /** Field description */
  private final ArrayList<PendingNakDetailEntry>	theList;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs the class.
   */
  public PendingNakDetailList()
  {
    this.theList	= new ArrayList<>();
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Erase all entries in the list.
   */
  public void clear()
  {
    this.theList.clear();
  }

  /**
   * Adds a new record to the list.
   *
   * @param entry
   */
  public void addEntry( PendingNakDetailEntry entry )
  {
    this.theList.add( entry );
  }

  /**
   * Erases all entries for the specified connection ID.
   *
   * @param conn the host connection ID
   */
  public void removeAllForConnection( long conn )
  {
    for ( PendingNakDetailEntry x : this.theList )
    {
      if ( x.getConnectionId() == conn )
      {
        this.theList.remove( x );
      }
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns a list of all entries for the specified connection.
   *
   * @param conn the host connection ID
   *
   * @return a list of entries which may be empty
   */
  public List<PendingNakDetailEntry> getAllForConnection( long conn )
  {
    ArrayList<PendingNakDetailEntry>	retVal	= new ArrayList<>();

    for ( PendingNakDetailEntry x : this.theList )
    {
      if ( x.getConnectionId() == conn )
      {
        retVal.add( x );
      }
    }

    return ( retVal );
  }

  /**
   * @return the theList
   */
  public ArrayList<PendingNakDetailEntry> getTheList()
  {
    return theList;
  }
  
  /**
   * Return whether or not the list is empty
   *
   * @return flag indicating if no entries
   */
  public boolean isEmpty()
  {
    return( theList.isEmpty() );
  }
  
  /**
   * Return the size (depth) of the list
   * 
   * @return the size of the list
   */
  public int size()
  {
    return( theList.size() );
  }
}
