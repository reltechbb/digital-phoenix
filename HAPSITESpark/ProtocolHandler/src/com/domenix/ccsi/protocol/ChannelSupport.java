/*
 * ChannelSupport.java
 * Contains Channel Support settings
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of the interface that must be implemented by all sensor
 * adapter writers.
 *
 */
package com.domenix.ccsi.protocol;

import com.domenix.commonha.ccsi.CcsiChannelEnum;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Contains Channel Support settings
 * @author jmerritt
 */
public class ChannelSupport
{
    /** the channel to which this applies */
    CcsiChannelEnum channel;
    
    /** is Compression enabled */
    boolean compressionEnabled;
    
    /** threshold that triggers compression */
    int compressionThreshold;
    
    /** this channel is supported */
    boolean supported;
    
    /** metadata associated with this channel */
    private final List <String> metadataList;
    
    /** sensor unique data associated with this channel */
    private final List <String> sudList;
    
    /** field 4 from the configuration file */
    private final String keyNumber;
    
    /**
     * Constructor - creates a new entry with default values
     * @param keyNumber - item 4 from the configuration file
     *  
     */
    public ChannelSupport (String keyNumber)
    {
        //this.channel = channel;
        compressionEnabled = false;
        supported = false;
        compressionThreshold = -1;
        sudList = new ArrayList <> ();
        metadataList = new ArrayList <>();
        this.keyNumber = keyNumber;
    }
    
    /**
     * Replaces the existing metadataList with the supplied values 
     * @param values 
     */
    public void setMetadata (String values)
    {
        String [] valueArray = values.split("\\,");
        metadataList.clear ();
        metadataList.addAll(Arrays.asList(valueArray));
    }
    
    /**
     * gets the metadata list for this channel
     * @return 
     */
    public List <String> getMetadata ()
    {
        return metadataList;
    }
    
    /**
     * Sets the compression threshold and enabled flag
     * @param threshold 
     */
    public void setCompressionThreshold (int threshold)
    {
        compressionThreshold = threshold;
        compressionEnabled = threshold != -1;
    }
    
    /**
     * gets the compression threshold
     * 
     * @return -1 = disabled, 0 = always enabled, anything else is the 
     * value in bytes
     */
    public int getCompressionThreshold ()
    {
        return compressionThreshold;
    }
    
    /**
     * Tests to see if compression is enabled
     * @return 
     */
    public boolean isCompressionEnabled ()
    {
        return compressionEnabled;
    }
    
    /**
     * Replaces the existing sudList with the supplied values 
     * @param values 
     */
    public void setSUD (String values)
    {
        String [] valueArray = values.split("\\,");
        sudList.clear ();
        sudList.addAll(Arrays.asList(valueArray));
    }
    
    /**
     * gets the Sensor Unique Data list for this channel
     * @return 
     */
    public List <String> getSUD ()
    {
        return sudList;
    }
    
    public void setChannel (CcsiChannelEnum channel)
    {
        this.channel = channel;
    }
    
    public CcsiChannelEnum getChannel ()
    {
        return channel;
    }
 
    public void setCompressionEnabled (boolean enabled)
    {
        compressionEnabled = enabled;
    }
    
    public void setSupported (boolean supported)
    {
        this.supported = supported;
    }
    
    public boolean isSupported ()
    {
        return supported;
    }
    
    public String getKeyNumber ()
    {
        return keyNumber;
    }
}
