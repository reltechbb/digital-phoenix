/*
 * DataValidityRegexList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a data validity check for a list of regular expressions.
 *
 * @author kmiller
 */
public class DataValidityRegexList implements DataValidityInterface
{
  /** The regular expression pattern used to validate. */
  private final ArrayList<Pattern>	thePatterns	= new ArrayList<>();

  /** The type of validity checking defined. */
  private final DataValidityType	theType	= DataValidityType.REGEXLIST;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance using a list of Patterns or Strings
   *
   *
   * @param args
   *
   * @throws IllegalArgumentException
   * @throws PatternSyntaxException
   */
  public DataValidityRegexList( Object... args ) throws IllegalArgumentException , PatternSyntaxException
  {
    for ( Object x : args )
    {
      if ( x instanceof Pattern )
      {
        thePatterns.add( (Pattern) x );
      }
      else if ( x instanceof String )
      {
        thePatterns.add( Pattern.compile( (String) x ) );
      }
      else
      {
        throw new IllegalArgumentException( "Invalid argument type, must be Pattern or String" );
      }
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the validator type which is always REGEXLIST
   *
   * @return DataValidityType.REGEXLIST
   */
  @Override
  public DataValidityType getTheType()
  {
    return ( theType );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Validates the input against the pattern and returns a valid/not valid indication
   *
   * @param value String containing the value to be validated
   *
   * @return boolean valid (true) or not valid (false)
   */
  @Override
  public boolean validateValue( String value )
  {
    for ( Pattern vp : thePatterns )
    {
      Matcher	m	= vp.matcher( value );

      if ( m.matches() )
      {
        return ( true );
      }
    }

    return ( false );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the description of what is valid using this validator.
   *
   * @return String containing the description
   */
  @Override
  public String getValidityString()
  {
    StringBuilder	msg	= new StringBuilder( " must match one of:\n" );

    for ( Pattern x : thePatterns )
    {
      msg.append( '\t' );
      msg.append( x.toString() );
      msg.append( '\n' );
    }

    return ( msg.toString() );
  }
}
