/*
 * PendingAlertList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This class implements a list of pending alerts to be reported to the host
 *
 * Version: V1.0  15/10/11
 */
package com.domenix.ccsi.protocol;

import java.util.concurrent.LinkedBlockingQueue;
import org.apache.log4j.Logger;

/**
 * This class implements a list of pending alerts to be reported to the host
 * 
 * @author kmiller
 */
public class PendingAlertList
{
  /** Error/Debug logger */
//  private Logger	myLogger	= null;

  /** The queue of readings */
  private final LinkedBlockingQueue<PendingAlertEntry>	theList	= new LinkedBlockingQueue<>();

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   */
  public PendingAlertList()
  {
//    myLogger	= Logger.getLogger( PendingBitList.class );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns indication of whether or not the list is empty
   *
   * @return flag indicating empty or not empty
   */
  public boolean isEmpty()
  {
    return ( theList.isEmpty() );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Adds a reading to the tail of the list
   *
   * @param msg
   */
  public void addAlert( PendingAlertEntry msg )
  {
    try
    {
      this.theList.put( msg );
    }
    catch ( InterruptedException ignored )
    {
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns all of the list contents as an array and clears the list.
   *
   * @return Object[]
   */
  public Object[] getAllEntries()
  {
    Object[] ret = this.theList.toArray();
    this.theList.clear();
    return ( ret );
  }
}
