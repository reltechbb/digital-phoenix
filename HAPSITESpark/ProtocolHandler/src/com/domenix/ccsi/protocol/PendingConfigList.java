/*
 * PendingConfigList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/02
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.CcsiConfigBlockEnum;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.concurrent.LinkedBlockingQueue;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a list of configuration change events to be reported to the host
 *
 * @author kmiller
 */
public class PendingConfigList
{
  /** Error/Debug logger */
  private Logger	myLogger	= null;

  /** The queue of readings */
  private final LinkedBlockingQueue<CcsiConfigBlockEnum>	theList	= new LinkedBlockingQueue<>();

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   */
  public PendingConfigList()
  {
    myLogger	= Logger.getLogger( PendingConfigList.class );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns indication of whether or not the list is empty
   *
   * @return flag indicating empty or not empty
   */
  public boolean isEmpty()
  {
    return ( theList.isEmpty() );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Adds a config item to the tail of the list
   *
   * @param block
   */
  public void addConfigBlock( CcsiConfigBlockEnum block )
  {
    try
    {
      this.theList.put( block );
    }
    catch ( InterruptedException ignored )
    {
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns all of the list contents as an array and clears the list.
   *
   * @return Object[]
   */
  public Object[] getAllEntries()
  {
    Object[]	ret	= this.theList.toArray();

    this.theList.clear();

    return ( ret );
  }

  /**
   * @return the theList
   */
  public LinkedBlockingQueue<CcsiConfigBlockEnum> getTheList()
  {
    return theList;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Clear all entries from the list
   */
  public void clear()
  {
    this.theList.clear();
  }
}
