/*
 * PendingAlertEntry.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/22
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.common.spark.data.MsgContent;

//~--- classes ----------------------------------------------------------------

/**
 * This class encapsulates a single pending alert list entry
 * 
 * @author kmiller
 */
public class PendingAlertEntry
{
  /** The received event message */
  private MsgContent	baseMsg	= null;

  /** The alert ID number */
  private long	idNumber	= -1;
  
  /** The alert string */
  private String alertId = null;

  /** Is this a trigger or untrigger */
  private boolean	trigger	= false;

  /** Is this a reading or a BIT */
  private boolean	reading	= false;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   *
   * @param id the alert ID number
   * @param trigger whether this is a trigger
   * @param msg the event message that caused the alert
   */
  public PendingAlertEntry( long id , boolean trigger , MsgContent msg )
  {
    this.idNumber	= id;
    this.trigger	= trigger;
    this.baseMsg	= msg;

    if ( msg.isSetReading() )
    {
      this.reading	= true;
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the idNumber
   */
  public long getIdNumber()
  {
    return idNumber;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param idNumber the idNumber to set
   */
  public void setIdNumber( long idNumber )
  {
    this.idNumber	= idNumber;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the trigger
   */
  public boolean isTrigger()
  {
    return trigger;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param trigger the trigger to set
   */
  public void setTrigger( boolean trigger )
  {
    this.trigger	= trigger;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the reading
   */
  public boolean isReading()
  {
    return reading;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param reading the reading to set
   */
  public void setReading( boolean reading )
  {
    this.reading	= reading;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the baseMsg
   */
  public MsgContent getBaseMsg()
  {
    return baseMsg;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param baseMsg the baseMsg to set
   */
  public void setBaseMsg( MsgContent baseMsg )
  {
    this.baseMsg	= baseMsg;
  }

  /**
   * @return the alertId
   */
  public String getAlertId()
  {
    return alertId;
  }

  /**
   * @param alertId the alertId to set
   */
  public void setAlertId(String alertId)
  {
    this.alertId = alertId;
  }
}
