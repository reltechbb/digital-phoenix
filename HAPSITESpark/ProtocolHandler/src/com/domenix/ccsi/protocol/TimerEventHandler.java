/*
 * TimerEventHandler.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/16
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.ChannelRegistrationType;
import com.domenix.commonha.intfc.NSDSConnection;
import com.domenix.commonha.intfc.NSDSConnectionList;
import com.domenix.commonha.intfc.NSDSConnectionTypeEnum;
import com.domenix.common.utils.TimerChangeEvent;
import com.domenix.common.utils.TimerPurposeEnum;
import com.domenix.common.utils.UtilTimerListener;

import org.apache.log4j.Logger;

//~--- classes ----------------------------------------------------------------

/**
 * This class handles timer expiration events and provides generic functionality for
 * resetting timer values, resetting heartbeat timing, and starting new timers.
 *
 * @author kmiller
 */
public class TimerEventHandler
{
  /** The heartbeat timer */
  private long	heartbeatTimerId	= -1L;

  /** Error/Debug logger */
  private final Logger	myLogger	= Logger.getLogger( TimerEventHandler.class );

  /** The timer listener */
  private final UtilTimerListener	listener;

  /** The PH Context */
  private final PHContext	theContext;

  //~--- constructors ---------------------------------------------------------

  /**
   * ConstructsA class
   *
   * @param ctx the PH Context
   * @param listener the timer listener
   */
  public TimerEventHandler( PHContext ctx , UtilTimerListener listener )
  {
    this.theContext	= ctx;
    this.listener		= listener;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Handles an ACK timer expiration
   *
   * @param x the timer change event
   */
  public void handleAckTimer( TimerChangeEvent x )
  {
    // If the ACK timer expirees remove the penind send entry to free up comms again.
    if ( x.getTimerEvent() == TimerChangeEvent.TEST_TIMER_EXPIRED )
    {
      CcsiChannelEnum	chan	= (CcsiChannelEnum) x.getPayload();

      if ( chan != null )
      {
        this.theContext.getPendingAcks().removeWaitingAck( chan );
      }
      else
      {
        this.theContext.getBaseTimer().cancelTimer( x.getTimerId() );
      }

      this.theContext.getPendingSends().removeNextEntry();
    }
  }

  /**
   * Handles a periodic report timer expiration
   *
   * @param x
   */
  public void handlePeriodicTimer( TimerChangeEvent x )
  {
    if ( x.getTimerEvent() == TimerChangeEvent.TEST_TIMER_EXPIRED )
    {
      ChannelRegistrationType	type	= (ChannelRegistrationType) x.getPayload();

      switch ( type.getMsgChannel() )
      {
        case ALERTS :
          this.theContext.getPendingTimedGen().addPendingTimed( new PendingTimedGeneration( CcsiChannelEnum.ALERTS ) );

          break;

        case CONFIG :
          this.theContext.getPendingTimedGen().addPendingTimed( new PendingTimedGeneration( CcsiChannelEnum.CONFIG ) );

          break;

        case STATUS :
          this.theContext.getPendingTimedGen().addPendingTimed( new PendingTimedGeneration( CcsiChannelEnum.STATUS ) );

          break;

        case READGS :
          this.theContext.getPendingTimedGen().addPendingTimed( new PendingTimedGeneration( CcsiChannelEnum.READGS ) );

          break;

        case MAINT :
          this.theContext.getPendingTimedGen().addPendingTimed( new PendingTimedGeneration( CcsiChannelEnum.MAINT ) );

          break;

        case IDENT :
          this.theContext.getPendingTimedGen().addPendingTimed( new PendingTimedGeneration( CcsiChannelEnum.IDENT ) );

          break;

        default :
          break;
      }
    }
  }

  /**
   * Handles a connect retry timer expiration
   *
   * @param x the timer change event
   */
  public void handleConnRetryTimer( TimerChangeEvent x )
  {
    this.theContext.getTheOpenInterfaceHost().connect(
        this.theContext.getTheOpenInterfaceHost().getAvailableLinks().getItem( 0 ).getLinkId() );
  }

  /**
   * Handles a heartbeat timer expiration
   *
   * @param x timer change event
   */
  public void handleHeartbeatTimer( TimerChangeEvent x )
  {
    NSDSConnectionList	conns	= this.theContext.getTheOpenInterfaceHost().getActiveConnections();

    if ( ( conns != null ) && ( !conns.isEmpty() ) )
    {
      for ( NSDSConnection z : conns.getConnectionList() )
      {
        if ( ( z != null ) && ( z.getConnType() == NSDSConnectionTypeEnum.PERSISTENT_CONNECTION ) )
        {
          PendingSendEntry	hbPse	= new PendingSendEntry();

          hbPse.setConnectionId( z.getConnectionId() );
          hbPse.addChannel( CcsiChannelEnum.HRTBT );
          hbPse.setSendHrtbt( true );
          hbPse.setAckRequired( true );
          this.theContext.getPendingSends().addPendingSend( hbPse );
          this.resetHeartbeatTimer();

          break;
        }
      }
    }
  }

  /**
   * Handles a generic protocol handler timer expiration
   *
   * @param x
   */
  public void handleProtocolHandlerTimer( TimerChangeEvent x )
  {
  }

  /**
   * Handles a connection failure timer expiration and attempts
   * to connect (three times) again.
   *
   * @param x the timer change event
   */
  public void handleConnFailureTimer( TimerChangeEvent x )
  {
    this.theContext.getTheSavedState().getTheOpenInterface().connect( 1 );
  }

  /**
   * Cancel a timer and remove the listener for this listener
   *
   * @param timerId the ID of the timer to be canceled
   */
  public void cancelTimer( long timerId )
  {
    myLogger.info( "Cancelling timer: " + timerId );

    if ( timerId >= 0L )
    {
      myLogger.info( "Entering cancel" );
      this.theContext.getBaseTimer().cancelTimer( timerId );
      myLogger.info( "Exiting cancel" );
    }

    myLogger.info( "Cancelled timer: " + timerId );
  }

  /**
   * This method starts a new timer and adds a listener for timer expired events
   *
   * @param secs the number of seconds
   * @param repeats whether or not the timer repeats
   * @param pld the timer payload
   * @param name the timer name
   * @param purpose the timer purpose
   *
   * @return the new timer ID
   */
  public long startNewTimer( long secs , boolean repeats , Object pld , String name , TimerPurposeEnum purpose )
  {
    long	newId;

    newId	= this.theContext.getBaseTimer().setNewTimer( secs , repeats , this , pld , name , purpose );
    this.theContext.getBaseTimer().addUtilTimerListener( listener , newId , TimerChangeEvent.TEST_TIMER_EXPIRED );

    if ( purpose == TimerPurposeEnum.HEARTBEAT_TIMER )
    {
      this.heartbeatTimerId	= newId;
    }

    return ( newId );
  }

  /**
   * Resets the heartbeat timer.
   *
   */
  public void resetHeartbeatTimer()
  {
    if ( this.heartbeatTimerId >= 0L )
    {
      this.theContext.getBaseTimer().resetUtilTimer( this.heartbeatTimerId );
    }
  }

  /**
   * Resets the heartbeat timer.
   *
   *
   * @param timerId
   */
  public void resetTimer( long timerId )
  {
    if ( timerId >= 0L )
    {
      this.theContext.getBaseTimer().resetUtilTimer( timerId );
    }
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * Method description
   *
   *
   * @param timerId
   * @param value
   *
   * @return
   */
  public boolean setNewTimePeriod( long timerId , int value )
  {
    boolean	retVal	= this.theContext.getBaseTimer().setNewTimePeriod( timerId , value );

    return ( retVal );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Cancel the heartbeat timer.
   *
   */
  public void cancelHeartbeatTimer()
  {
    if ( this.heartbeatTimerId >= 0L )
    {
      this.theContext.getBaseTimer().cancelTimer( this.heartbeatTimerId );
      this.heartbeatTimerId	= -1L;
    }
  }
}
