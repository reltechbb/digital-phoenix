/*
 * DataValidityRangeList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

//~--- classes ----------------------------------------------------------------

/**
 * This class defines a list of numeric data validity ranges.
 *
 * @author kmiller
 */
public class DataValidityRangeList implements DataValidityInterface
{
  /** The type of validation. */
  private final DataValidityType	theType	= DataValidityType.RANGELIST;

  /** The list of ranges. */
  private final ArrayList<DataValidityRange>	ranges;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance of the class and allocates the storage list.
   */
  public DataValidityRangeList()
  {
    ranges	= new ArrayList<DataValidityRange>();
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Add a data validity range to the list.
   *
   * @param rng DataValidityRange to be added
   *
   * @return boolean indication of success (true) or failure (false)
   */
  public boolean addValidityRange( DataValidityRange rng )
  {
    ranges.add( rng );

    return ( true );
  }

  /**
   * Adds a validity range to the list by creating a new range from
   * the supplied string and adding it to the list.
   *
   * @param rng String containing the range definition
   *
   * @return boolean indication of success (true) or failure (false)
   */
  public boolean addValidityRange( String rng )
  {
    boolean	retValue	= false;

    try
    {
      DataValidityRange	myRange	= new DataValidityRange( rng );

      ranges.add( myRange );
      retValue	= true;
    }
    catch ( Exception ex )
    {
      retValue	= false;
    }

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the data validity type of this class.
   *
   * @return DataValidityType constant for a range list
   */
  @Override
  public DataValidityType getTheType()
  {
    return ( this.theType );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Validates the provided string value against the list.  The string is
   * considered valid if it matches any of the definitions in the list, if
   * or if the list is empty and there is nothing to compare the value with.
   *
   * @param value String containing the value to be validated.
   *
   * @return boolean indication of valid (true) or invalid (false)
   */
  @Override
  public boolean validateValue( String value )
  {
    boolean	retValue	= true;

    if ( ( value != null ) && ( value.trim().length() > 0 ) && ( ranges.size() > 0 ) )
    {
      retValue	= false;

      for ( DataValidityRange rng : ranges )
      {
        if ( rng.validateValue( value.trim() ) )
        {
          retValue	= true;

          break;
        }
      }
    }

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the description of what is valid using this validator.
   *
   * @return String containing the description
   */
  @Override
  public String getValidityString()
  {
    StringBuilder	msg	= new StringBuilder( " must be within one of:\n" );

    for ( DataValidityRange x : this.ranges )
    {
      msg.append( '\t' );
      msg.append( x.getValidityString() );
      msg.append( '\n' );
    }

    return ( msg.toString() );
  }
}
