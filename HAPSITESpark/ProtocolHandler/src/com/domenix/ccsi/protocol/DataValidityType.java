/*
 * DataValidityType.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

/**
 * Defines the types of data validity definitions that can be used.
 *
 * @author kmiller
 */
public enum DataValidityType
{
  /** Validates a string enumeration of valid values */
  ENUMERATION("Enumeration") ,

  /** Validates a range of numeric data values */
  RANGE("Range") ,

  /** Validates a multiple entry list of numeric ranges */
  RANGELIST("RangeList") ,

  /** Validates a date plus time */
  DATETIME("DateTime") ,

  /** Validates that a string is &le; a specified length */
  MINSTRINGLENGTH("MinStringLength") ,

  /** Validates that a string is &le; a specified length */
  MAXSTRINGLENGTH("MaxStringLength") ,

  /** Validates a date */
  DATE("Date") ,

  /** Validates a time */
  TIME("Time") ,

  /** Validates using a regular expression */
  REGEX("Regular Expression") ,

  /** Validates using a list regular expressions */
  REGEXLIST("Regex List") ,

  /** Validates that a string's length is &ge; min and &le; max */
  STRINGLENGTHRANGE("String length range") ,

  /** Validates that a value is a valid XML boolean */
  BOOLEAN("Boolean") ,

  /** Validates that a value is a valid role list */
  ROLELIST("Role List") ,

  /** Validates that a value is a valid IPv4 or IPv6 numeric address */
  IPADDRESS("IP Address") ,

  /** Validates that a value is a valid IPv4 or IPv6 net mask */
  NETMASK("Net Mask") ,

  /** Validates that a value is a valid CCSI version number */
  VERSIONNBR("Version Number");

  /** The XML type for this enumeration. */
  private String	xml;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance with the associated XML string.
   *
   * @param xml String containing the XML that identifies this type.
   *
   * @throws IllegalArgumentException
   */
  private DataValidityType( String xml ) throws IllegalArgumentException
  {
    if ( ( xml != null ) && ( xml.trim().length() > 0 ) )
    {
      this.xml	= xml.trim();
    }
    else
    {
      throw new IllegalArgumentException( "Missing or empty XML string in constructor." );
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the XML string for this enumeration instance.
   *
   * @return String containing the XML
   */
  public String getXml()
  {
    return ( this.xml );
  }

  /**
   * Returns an enumeration value that matches the provided argument string.  The input is compared to
   * both the enumeration name and its XML string to determine which enumeration is matched.  All comparisons
   * are case insensitive.
   *
   * @param name String containing the name to find
   *
   * @return DataValidityType whose name or XML string matches the input
   *
   * @throws IllegalArgumentException
   */
  public static DataValidityType getEnum( String name ) throws IllegalArgumentException
  {
    if ( name != null )
    {
      if ( name.trim().length() > 0 )
      {
        String	compStr	= name.trim().toUpperCase();

        for ( DataValidityType emt : DataValidityType.values() )
        {
          if ( ( emt.name().equalsIgnoreCase( compStr ) ) || ( emt.getXml().trim().equalsIgnoreCase( compStr ) ) )
          {
            return ( emt );
          }
        }

        throw new IllegalArgumentException( "Name argument is not a valid enumeration value." );
      }
    }

    throw new IllegalArgumentException( "Name argument cannot be null or empty string." );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Return a string representation of the enumeration including both the name and XML value.
   *
   * @return String containing the enumeration representation
   */
  @Override
  public String toString()
  {
    StringBuilder	msg	= new StringBuilder();

    msg.append( this.name() );
    msg.append( " (" );
    msg.append( this.getXml() );
    msg.append( ")" );

    return ( msg.toString() );
  }
}
