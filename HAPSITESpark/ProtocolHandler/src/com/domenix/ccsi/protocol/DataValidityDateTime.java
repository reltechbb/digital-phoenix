/*
 * DataValidityDateTime.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements validation of strings containing a date, time, or datetime.
 *
 * @author kmiller
 */
public class DataValidityDateTime implements DataValidityInterface
{
  /** The type of validity check implemented. */
  private DataValidityType	theType	= DataValidityType.DATETIME;

  /** REGEX patterns for validating string. */
  private final String[]	validityPatterns	=
  {
    "[0-2][0-9][0-5][0-9][0-5][0-9]" ,                                         // TIME pattern
    "([1-2][0-9]{3})([0-1][0-9])([0-3][0-9])" ,                                // DATE pattern
    "([1-2][0-9]{3})([0-1][0-9])([0-3][0-9])[0-2][0-9][0-5][0-9][0-5][0-9]"    // DATETIME pattern
  };

  /** The pattern to be used. */
  private Pattern	valPattern;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a default instance of a DATETIME validation check.
   */
  public DataValidityDateTime()
  {
    valPattern	= Pattern.compile( this.validityPatterns[2] );
  }

  /**
   * Constructs a validation of the specified type which must be one of:
   * date, time, or datetime.
   *
   * @param type String containing the type to be created.
   *
   * @throws IllegalArgumentException
   */
  public DataValidityDateTime( String type ) throws IllegalArgumentException
  {
    if ( ( type != null ) && ( type.trim().length() > 0 ) )
    {
      if ( type.trim().toUpperCase().equalsIgnoreCase( DataValidityType.DATE.name() ) )
      {
        this.theType	= DataValidityType.DATE;
        valPattern		= Pattern.compile( this.validityPatterns[1] );
      }
      else if ( type.trim().toUpperCase().equalsIgnoreCase( DataValidityType.TIME.name() ) )
      {
        this.theType	= DataValidityType.TIME;
        valPattern		= Pattern.compile( this.validityPatterns[0] );
      }
      else if ( type.trim().toUpperCase().equalsIgnoreCase( DataValidityType.DATETIME.name() ) )
      {
        this.theType	= DataValidityType.DATETIME;
        valPattern		= Pattern.compile( this.validityPatterns[2] );
      }
      else
      {
        throw new IllegalArgumentException( "Invalid type in constructor." );
      }
    }
    else
    {
      throw new IllegalArgumentException( "Null or empty type name in the constructor." );
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the type of validity check implemented by this class.
   *
   * @return DataValidityType of this class
   */
  @Override
  public DataValidityType getTheType()
  {
    return ( this.theType );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Validates the input string as being a CCSI valid date, time, or datetime.
   *
   * @param value String to be validated
   *
   * @return boolean indication of valid (true) or invalid (false)
   */
  @Override
  public boolean validateValue( String value )
  {
    boolean	retValue	= false;

    if ( ( value != null ) && ( value.trim().length() > 0 ) )
    {
      Matcher	dtMatch	= valPattern.matcher( value.trim() );
      boolean	found		= dtMatch.matches();

      if ( found )
      {
        //
        // Ensure that the entire string was consumed by the pattern
        //
        String	group	= dtMatch.group();

        if ( group.equalsIgnoreCase( value.trim() ) )
        {
          retValue	= true;
        }
      }
    }

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Method description
   *
   *
   * @return
   */
  @Override
  public String getValidityString()
  {
    StringBuilder	msg	= new StringBuilder( "must match one of: " );

    for ( int i = 0 ; i < validityPatterns.length ; i++ )
    {
      msg.append( validityPatterns[i] );

      if ( i < ( validityPatterns.length - 1 ) )
      {
        msg.append( " -OR- " );
      }
    }

    return ( msg.toString() );
  }
}
