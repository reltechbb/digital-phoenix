/*
 * ActiveAlertsList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This class implements a list of active alerts on the sensor.
 *
 * Version: V1.0  15/12/11
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.concurrent.LinkedBlockingQueue;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a list of active alerts on the sensor.
 *
 * @author kmiller
 */
public class ActiveAlertsList
{
  /** Error/Debug logger */
  private Logger	myLogger	= null;

  /** The queue of readings */
  private final LinkedBlockingQueue<ActiveAlertListEntry>	theList	= new LinkedBlockingQueue<>();

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   */
  public ActiveAlertsList()
  {
    myLogger	= Logger.getLogger( ActiveAlertsList.class );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns indication of whether or not the list is empty
   *
   * @return flag indicating empty or not empty
   */
  public boolean isEmpty()
  {
    return ( theList.isEmpty() );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Clears the list of active alerts
   */
  public void clear()
  {
    this.theList.clear();
    myLogger.info ("Clearing active alerts list");
  }

  /**
   * Adds a reading to the tail of the list
   *
   * @param msg
   */
  public void addAlert( ActiveAlertListEntry msg )
  {
    try
    {
      this.theList.put( msg );
    }
    catch ( InterruptedException ignored )
    {
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns all of the list contents as an array and clears the list.
   *
   * @return Object[]
   */
  public Object[] getAllEntries()
  {
    Object[]	ret	= this.theList.toArray();

//    this.theList.clear();

    return ( ret );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Remove any/all entries that have the matching alert ID.
   *
   * @param alertId the alert ID to be removed
   */
  public void removeActiveAlert( String alertId )
  {
    for ( ActiveAlertListEntry x : this.theList )
    {
      if ( x.getAlertId().contentEquals( alertId ) )
      {
        this.theList.remove( x );
      }
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Get the entry for the matching alert ID
   *
   * @param alertId the ID of the alert to retrieve
   *
   * @return the active alert entry or null if not found in the list
   */
  public ActiveAlertListEntry getAlert( String alertId )
  {
    ActiveAlertListEntry	retVal	= null;

    for ( ActiveAlertListEntry x : this.theList )
    {
      if ( x.getAlertId().contentEquals( alertId ) )
      {
        retVal	= x;

        break;
      }
    }

    return ( retVal );
  }
}
