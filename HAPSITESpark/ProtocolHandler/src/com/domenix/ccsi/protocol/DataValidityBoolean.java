/*
 * DataValidityBoolean.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a data validity check for boolean values.
 *
 * @author kmiller
 */
public class DataValidityBoolean implements DataValidityInterface
{
  /** The data validity check type */
  private final DataValidityType	theType	= DataValidityType.BOOLEAN;

  /** Valid XML boolean values */
  private final ArrayList<String>	valid	= new ArrayList<>();

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructor
   */
  public DataValidityBoolean()
  {
    valid.add( "true" );
    valid.add( "false" );
    valid.add( "1" );
    valid.add( "0" );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the type of this data validity check function.
   *
   * @return DataValidityType
   */
  @Override
  public DataValidityType getTheType()
  {
    return ( this.theType );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Validates the supplied string to determine if it is an XML xs:boolean
   *
   * @param value String containing the value to be validated
   *
   * @return boolean valid (true) or invalid (false)
   */
  @Override
  public boolean validateValue( String value )
  {
    for ( String x : valid )
    {
      if ( x.equalsIgnoreCase( value ) )
      {
        return ( true );
      }
    }

    return ( false );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns a string indicating which values are valid.
   *
   * @return String containing the validity definition
   */
  @Override
  public String getValidityString()
  {
    StringBuilder	msg				= new StringBuilder( " must be one of: " );
    boolean				firstOne	= true;

    for ( String x : valid )
    {
      if ( !firstOne )
      {
        msg.append( " , " );
      }

      firstOne	= false;
      msg.append( x );
    }

    return ( msg.toString() );
  }
}
