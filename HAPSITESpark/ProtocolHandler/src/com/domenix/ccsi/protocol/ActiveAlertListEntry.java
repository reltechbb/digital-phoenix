/*
 * ActiveAlertListEntry.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This class encapsulates a single active alert.
 *
 */
package com.domenix.ccsi.protocol;

import com.domenix.common.spark.IPCWrapper;

/**
 * This class encapsulates a single active alert.
 * 
 * @author kmiller
 */
public class ActiveAlertListEntry
{
  /** The ID of this active alert [AW][TU]999999999 */
  private String alertId = null;
  
  /** The reading or BIT that caused the alert */
  private IPCWrapper entry;
  
  /** The cache key for the alert */
  private String cacheKey;
  
  /**
   * Default constructor
   */
  public ActiveAlertListEntry()
  {
    
  }
  
  /**
   * Full constructor
   * 
   * @param id the alert ID string
   * @param entry the entry that caused the active alert
   * 
   * @exception IllegalArgumentException if either argument is null
   */
  public ActiveAlertListEntry( String id , IPCWrapper entry ) throws IllegalArgumentException
  {
    if ( ( id != null ) && ( id.trim().length() == 11 ) && ( entry != null ) )
    {
      this.alertId = id;
      this.entry   = entry;
    }
    else
    {
      throw new IllegalArgumentException( "Missing mandatory argument in constructor" );
    }
  }

  /**
   * @return the alertId
   */
  public String getAlertId()
  {
    return alertId;
  }

  /**
   * @param alertId the alertId to set
   */
  public void setAlertId(String alertId)
  {
    this.alertId = alertId;
  }

  /**
   * @return the entry
   */
  public IPCWrapper getEntry()
  {
    return entry;
  }

  /**
   * @param entry the entry to set
   */
  public void setEntry(IPCWrapper entry)
  {
    this.entry = entry;
  }

  /**
   * @return the cacheKey
   */
  public String getCacheKey()
  {
    return cacheKey;
  }

  /**
   * @param cacheKey the cacheKey to set
   */
  public void setCacheKey(String cacheKey)
  {
    this.cacheKey = cacheKey;
  }
}
