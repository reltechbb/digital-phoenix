/*
 * CommandValidator.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains methods for validating command messages received from the host
 *
 */
package com.domenix.ccsi.protocol;

import com.domenix.commonha.ccsi.CcsiAckNakEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.CcsiChannelRegistrationEnum;
import com.domenix.ccsi.CcsiConfigBlockEnum;
import com.domenix.commonha.ccsi.CcsiModeEnum;
import com.domenix.commonha.ccsi.ChannelRegistrationList;
import com.domenix.commonha.ccsi.ChannelRegistrationType;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;
import com.domenix.common.ccsi.NameValuePair;
import com.domenix.common.config.SensorUniqueConfigItem;
import com.domenix.ccsi.protocol.CcsiCommandHandler.ValHdrResults;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 * Methods for validating command messages received from the host
 *
 * @author jmerritt
 */
public class CommandValidator
{

    /**
     * The PH Context
     */
    private final PHContext theContext;

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger;

    /**
     * Set configuration argument validator
     */
    private final SetConfigArgumentValidator setConfigArgVal;

    /**
     * Constructor
     *
     * @param theContext
     */
    public CommandValidator(PHContext theContext)
    {
        this.theContext = theContext;
        myLogger = Logger.getLogger(CommandValidator.class);
        setConfigArgVal = new SetConfigArgumentValidator(theContext);
    }

    /**
     * Handles Sensor Unique Commands
     * @return 
     */
    public boolean validateUniqueCommand()
    {
        return true;
    }

    /**
     * Validates a Power_Off command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validatePower_OffCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() > 1)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.TOO_MANY_ARGUMENTS,
                    "Too Many Arguments");
        }
        return retVal;
    }

    /**
     * Validates a Render_Useless command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateRender_UselessCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() > 2)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.TOO_MANY_ARGUMENTS,
                    "Too Many Arguments");
        }
        return retVal;
    }

    /**
     * Validates a Set_Tamper command.
     *
     * This command is never supported.
     *
     * @return
     */
    public boolean validateSet_TamperCommand()
    {
        return true;
    }

    /**
     * Validates a Set
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return
     */
    public boolean validateSet_Emcon_ModeCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() < 2)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.TOO_MANY_ARGUMENTS,
                    "Too Many Arguments");
        }
        return retVal;
    }

    /**
     * Validates a Set_Encryption command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateSet_EncryptionCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() != 2)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.TOO_MANY_ARGUMENTS,
                    "Must Have Exactly 2 Arguments");
        } else
        {
            if (!(args.get(0).getTheValue().equalsIgnoreCase("ALL") && 
                    (!(args.get(0).getTheValue().equals(
                            theContext.getTheSavedState().getStateLinkName())))))
            {
                retVal = false;
                sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.INVALID_ARGUMENT,
                    "Incorrect Link Name:" + args.get(0).getTheValue());                
            }
        }
        return retVal;
    }

    /**
     * Validates an Erase_Sec_Log command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateErase_Sec_LogCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() > 1)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.TOO_MANY_ARGUMENTS,
                    "Too Many Arguments");
        }
        return retVal;
    }

    /**
     * Validates a Get_Sec_Log command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateGet_Sec_LogCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() > 1)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.TOO_MANY_ARGUMENTS,
                    "Too Many Arguments");
        }
        return retVal;
    }

    /**
     * Validates a Sanitize command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateSanitizeCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() > 0)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.TOO_MANY_ARGUMENTS,
                    "Too Many Arguments");
        }
        return retVal;
    }

    /**
     * Validates a Zeroize command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateZeroizeCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() > 0)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.TOO_MANY_ARGUMENTS,
                    "Too Many Arguments");
        }
        return retVal;
    }

    /**
     * Validates the Set_Cmd_Privilege command
     *
     * No sensor implements this so always returns true
     *
     * @return
     */
    public boolean validateSet_Cmd_PrivilegeCommand()
    {
        return true;
    }

    /**
     * Validates the Set_Config_Privilege command
     *
     * No sensor implements this so always returns true
     *
     * @return
     */
    public boolean validateSet_Config_PrivilegeCommand()
    {
        return true;
    }

    /**
     * Validates a Reset_Msg_Seq command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @param hdr
     * @return true = success
     */
    public boolean validateReset_Msg_SeqCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args, ValHdrResults hdr)
    {
        boolean retVal = false;
        if (args.isEmpty())
        {
            if (hdr.msn == 0L)
            {
                retVal = true;
            }
        } else
        {
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.TOO_MANY_ARGUMENTS, null);
            retVal = false;
        }

        return retVal;
    }

    /**
     * Validates a Start_Stop command
     *
     * @param recvdConnId - connection ID
     * @param recvdMsn - MSN
     * @param args - list of arguments
     * @return true = OK
     */
    public boolean validateStart_StopCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal;
        boolean nakSsc = false;
        NakDetailCodeEnum sscDetailCode = null;

        if (args.size() == 1)
        {
            NameValuePair x = args.get(0);

            if (x.getTheName().contentEquals("StartOrStop"))
            {
                if ((x.getTheValue() != null) && (!x.getTheValue().trim().isEmpty()))
                {
                    if ((x.getTheValue().trim().contentEquals("START"))
                            || (x.getTheValue().trim().contentEquals("END")))
                    {
                        retVal = true;
                    } else
                    {
                        nakSsc = true;
                        sscDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                        myLogger.error("Invalid mode argument");
                    }
                } else
                {
                    nakSsc = true;
                    sscDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                    myLogger.error("Missing start stop argument value");
                }
            } else
            {
                nakSsc = true;
                sscDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                myLogger.error("Missing or invalid argument name");
            }
        } else if (args.size() > 1)
        {
            nakSsc = true;
            sscDetailCode = NakDetailCodeEnum.TOO_MANY_ARGUMENTS;
            myLogger.error("Too many arguments present");
        } else
        {
            nakSsc = true;
            sscDetailCode = NakDetailCodeEnum.INSUFFICIENT_ARGUMENTS;
            myLogger.error("Not enough arguments present");
        }

        if (nakSsc)
        {
            sendNack(recvdConnId, recvdMsn, sscDetailCode, null);
            retVal = false;
        } else
        {
            retVal = true;
        }

        return retVal;
    }

    public boolean validateRebootCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() > 2)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.TOO_MANY_ARGUMENTS,
                    "Too Many Arguments");
        }
        return retVal;
    }

    /**
     * Validates a Set_Comm_Permission command
     *
     * No sensor implements this so always returns true
     *
     * @return true = OK
     */
    public boolean validateSet_Comm_PermissionCommand()
    {
        return true;
    }

    /**
     * Validates a Set_Local_Alert command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateSet_Local_Alert_ModeCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() != 1)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.INVALID_ARGUMENT,
                    "Incorrect Number of Arguments");
        }
        return retVal;
    }

    /**
     * Validates a Start_Self_Test command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @param sstArgs
     * @return - true = OK
     */
    public boolean validateStart_Self_TestCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args, CcsiStartSelfTestArgs sstArgs)
    {
        boolean retVal;
        boolean nakSst = false;
        NakDetailCodeEnum sstDetailCode = null;

        sstArgs.clear();

        if (!args.isEmpty())
        {
            String compName = "";
            String level = "";
            boolean firstArg = true;

            for (NameValuePair x : args)
            {
                if ((firstArg) && (x.getTheName().isEmpty()))
                {
                    // If the first arg is empty, it's a valid full test of all components
                    sstArgs.addArgument("ALL", "0");

                    break;
                } else if (x.getTheName().contentEquals("Component"))
                {
                    compName = x.getTheValue();
                } else if (x.getTheName().contentEquals("Level"))
                {
                    level = x.getTheValue();

                    if (!level.isEmpty())
                    {
                        sstArgs.addArgument(compName, level);
                        compName = "ALL";
                        level = "0";
                        retVal = true;
                    } else
                    {
                        sstArgs.addArgument(compName, "0");
                        compName = "ALL";
                        level = "0";
                        retVal = true;
                    }
                } else
                {
                    nakSst = true;
                    sstDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                    myLogger.error("Invalid argument name");
                    retVal = false;
                }
            }

            if ((compName != null) && (!compName.contentEquals("ALL")))
            {
                sstArgs.addArgument(compName, level);
                compName = null;
                level = null;
                retVal = true;
            }
        } else
        {
            sstArgs.addArgument("ALL", "0");
            retVal = true;
        }

        if (nakSst)
        {
            sendNack(recvdConnId, recvdMsn, sstDetailCode, null);
            retVal = false;
        } else
        {
            retVal = true;
        }
        return retVal;
    }

    /**
     * Validates a Set_Security_Timeout command
     *
     * No sensor implements this so always returns true
     *
     * @return true = OK
     */
    public boolean validateSet_Security_TimeoutCommand()
    {
        return true;
    }

    public boolean validateClear_AlertCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args, Pattern alertIdPattern)
    {
        boolean retVal;
        boolean nakCac = false;
        NakDetailCodeEnum cacDetailCode = null;
        String nakDetailText = null;

        if (args.size() == 1)
        {
            NameValuePair x = args.get(0);

            if (x.getTheName().equals("Alert_ID"))
            {
                if ((x.getTheValue() != null) && (!x.getTheValue().trim().isEmpty()))
                {
                    if (!x.getTheValue().trim().contentEquals("ALL"))
                    {
                        Matcher aidM = alertIdPattern.matcher(x.getTheValue().trim());

                        if (!aidM.matches())
                        {
                            nakCac = true;
                            cacDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                            nakDetailText = "Invalid argument";
                            myLogger.error("Invalid alert ID argument" + x.getTheValue());
                        } else
                        {
                            retVal = true;
                        }
                    } else
                    {
                        retVal = true;
                    }
                }
            }
        } else if (args.size() > 1)
        {
            nakCac = true;
            cacDetailCode = NakDetailCodeEnum.TOO_MANY_ARGUMENTS;
            nakDetailText = "Too Many Arguments";
        } else
        {
            nakCac = true;
            cacDetailCode = NakDetailCodeEnum.INSUFFICIENT_ARGUMENTS;
            nakDetailText = "Not enough Arguments";
        }

        if (nakCac)
        {
            sendNack(recvdConnId, recvdMsn, cacDetailCode, nakDetailText);
            retVal = false;
        } else
        {
            retVal = true;
        }
        return retVal;
    }

    /**
     * Validates a Silence command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateSilenceCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if ((args.isEmpty()) || (args.size() > 2))
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.INVALID_ARGUMENT,
                    "Incorrect Number of Arguments");
        }
        return retVal;
    }

    /**
     * This method validates the parameters of the Set_Heartbeat command.
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args list of the arguments
     * @param setHeartbeatArg
     * @param commandHandler
     * @return flag indicating success or failure
     */
    public boolean validateSet_HeartbeatCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args, NameValuePair setHeartbeatArg,
            CcsiCommandHandler commandHandler)
    {
        boolean retVal;
        boolean nakShc = false;
        NakDetailCodeEnum shcDetailCode = null;

        if ((args != null) && (args.size() == 1))
        {
            NameValuePair arg = args.get(0);

            if ((arg.getTheValue() != null) && 
                    (arg.getTheName().trim().equalsIgnoreCase("Rate")) &&
                    (!arg.getTheValue().isEmpty()))
            {
                try
                {
                    int rate = Integer.parseInt(args.get(0).getTheValue());

                    if (((rate >= 5) && (rate <= 3600)) || (rate == 0))
                    {
                        retVal = true;

                        commandHandler.setSetHeartbeatArg(new NameValuePair("Rate", args.get(0).getTheValue()));
                    } else
                    {
                        nakShc = true;
                        shcDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                        myLogger.error("Invalid rate argument: " + args.get(0).getTheValue());
                    }
                } catch (NumberFormatException nfe)
                {
                    nakShc = true;
                    shcDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                    myLogger.error("Invalid setHeartbeat rate", nfe);
                }
            } else
            {
                nakShc = true;
                shcDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                myLogger.error("Invalid setHeartbeat rate argument");
            }
        } else if (args == null)
        {
            nakShc = true;
            shcDetailCode = NakDetailCodeEnum.TOO_MANY_ARGUMENTS;
            myLogger.error("Missing mandatory argument - heartbeat rate");
        }

        if (nakShc)
        {
            sendNack(recvdConnId, recvdMsn, shcDetailCode, null);
            retVal = false;
        } else
        {
            retVal = true;
        }

        return retVal;
    }

    /**
     * Validates a Set_Location command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateSet_LocationCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if ((args.size() <= 3) || (args.size() > 4))
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.INVALID_ARGUMENT,
                    "Incorrect Number of Arguments");
        }
        return retVal;
    }

    /**
     * Validates a Set_Data_Compression command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateSet_Data_CompressionCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() <= 2)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.INVALID_ARGUMENT,
                    "Incorrect Number of Arguments");
        }
        return retVal;
    }

    /**
     * Validates a Set_Local_Enable command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateSet_Local_EnableCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() != 2)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.INVALID_ARGUMENT,
                    "Incorrect Number of Arguments");
        }
        return retVal;
    }

    /**
     * Validates a Set_Bw_Mode command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateSet_Bw_ModeCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (!(args.size() == 2))
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.INVALID_ARGUMENT,
                    "Incorrect Number of Arguments");
        }
        return retVal;
    }

    /**
     * Validates the Get_Status command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @param getStatusPattern
     * @return - true = OK
     */
    public boolean validateGet_StatusCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args, Pattern getStatusPattern)
    {
        boolean retVal;
        boolean nakGsc = false;
        NakDetailCodeEnum gscDetailCode = null;
        String gscDetailText = null;

        if (args.size() >= 1)
        {
            for (NameValuePair x : args)
            {
                if (x.getTheName() != null)
                {
                    if (x.getTheName().contentEquals("Item"))
                    {
                        Matcher gsMatcher = getStatusPattern.matcher(x.getTheValue().trim());

                        if (!gsMatcher.find())
                        {
                            nakGsc = true;
                            gscDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                            gscDetailText = "Invalid get status argument: " + x.getTheValue().trim();
                            myLogger.error(gscDetailText);

                            break;
                        }
                    } else
                    {
                        nakGsc = true;
                        gscDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                        gscDetailText = "Invalid get status argument: " + x.getTheName().trim();
                        myLogger.error(gscDetailText);

                        break;
                    }
                } else
                {
                    nakGsc = true;
                    gscDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                    gscDetailText = "Missing argument: Get_Status";
                    myLogger.error(gscDetailText);
                    myLogger.error("Missing argument");
                }
            }    // end for each argument
        }

        if (nakGsc)
        {
            sendNack(recvdConnId, recvdMsn, gscDetailCode, gscDetailText);
            retVal = false;
        } else
        {
            retVal = true;
        }

        return retVal;
    }

    /**
     * Validates a Get_Configuration command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @param getCfgArgList
     * @return true = OK
     */
    public boolean validateGet_ConfigurationCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args, CcsiGetConfigArgumentList getCfgArgList)
    {
        boolean retVal;
        if (args.size() < 2)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.MISSING_ARGUMENT,
                    "Incorrect Number of Arguments");
        } else
        {
            retVal = validateGetConfigArguments(recvdConnId, recvdMsn,
                    args, getCfgArgList);
        }
        return retVal;
    }

    /**
     * Validates an Install_Start command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateInstall_StartCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() != 3)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.INVALID_ARGUMENT,
                    "Incorrect Number of Arguments");
        }
        return retVal;
    }

    /**
     * Validates and Install command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateInstallCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() != 5)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.INVALID_ARGUMENT,
                    "Incorrect Number of Arguments");
        }
        return retVal;
    }

    /**
     * Validates an Install_Complete command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateInstall_CompleteCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if ((args.size() != 1) && (args.size() != 2))
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.INVALID_ARGUMENT,
                    "Incorrect Number of Arguments");
        }
        return retVal;
    }

    /**
     * Validates the Set_Ccsi_Mode command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args - argument list
     * @return - true = OK
     */
    public boolean validateSet_Ccsi_ModeCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = false;
        boolean nakScm = false;
        NakDetailCodeEnum scmDetailCode = null;
        String scmDetailText = null;

        if (args.size() == 1)
        {
            NameValuePair x = args.get(0);

            if (x.getTheName().contentEquals("Mode"))
            {
                if ((x.getTheValue() != null) && (!x.getTheValue().trim().isEmpty()))
                {
                    try
                    {
                        CcsiModeEnum test = CcsiModeEnum.value(x.getTheValue().trim());

                        if (test.isCommandable())
                        {
                            retVal = true;
                        } else
                        {
                            nakScm = true;
                            scmDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                            scmDetailText = "Invalid mode argument: " + x.getTheValue().trim();
                            myLogger.error(scmDetailText);
                        }
                    } catch (IllegalArgumentException ex)
                    {
                        nakScm = true;
                        scmDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                        scmDetailText = "Invalid mode argument: " + x.getTheValue().trim();
                        myLogger.error(scmDetailText);
                    }
                } else
                {
                    nakScm = true;
                    scmDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                    scmDetailText = "Missing mode argument value";
                    myLogger.error(scmDetailText);
                }
            } else
            {
                nakScm = true;
                scmDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                scmDetailText = "Missing or invalid argument name";
                myLogger.error(scmDetailText);
            }
        } else if (args.size() > 1)
        {
            nakScm = true;
            scmDetailCode = NakDetailCodeEnum.TOO_MANY_ARGUMENTS;
            scmDetailText = "Too many arguments for command.";
            myLogger.error(scmDetailText);
        } else
        {
            nakScm = true;
            scmDetailCode = NakDetailCodeEnum.INSUFFICIENT_ARGUMENTS;
            scmDetailText = "Not enough arguments present";
            myLogger.error(scmDetailText);
        }

        if (nakScm)
        {
            sendNack(recvdConnId, recvdMsn, scmDetailCode, scmDetailText);
            retVal = false;
        } else
        {
            retVal = true;
        }
        return retVal;
    }

    /**
     * Validates a User_Change message
     *
     * No sensor implements this so always returns true
     *
     * @return true = OK
     */
    public boolean validateUser_ChangeCommand()
    {
        return true;
    }

    /**
     * Validates a Get_Users message
     *
     * No sensor implements this so always returns true
     *
     * @return true = OK
     */
    public boolean validateGet_UsersCommand()
    {
        return true;
    }

    /**
     * This method validates the arguments of a Register command to ensure that
     * we support the channel, that the arguments in the correct sequence, and
     * that each repeating group is valid.
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args the parsed command arguments
     * @param workList
     *
     * @return flag indicating valid (true) or invalid (false)
     */
    public boolean validateRegisterCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args, ChannelRegistrationList workList)
    {
        boolean retVal = false;
        ChannelRegistrationType regType = null;
        boolean needPeriodValue = false;
        boolean nakReg = false;
        NakDetailCodeEnum regDetailCode = null;
        String regDetailText = null;

        workList.clearList(null);

        for (NameValuePair x : args)
        {
            if (x.getTheName().equalsIgnoreCase("Channel"))
            {
                needPeriodValue = false;

                if (regType != null)
                {
                    workList.addChannelRegistration(regType);
                    regType = null;
                }

                regType = new ChannelRegistrationType();

                if ((x.getTheValue() != null) && (!x.getTheValue().trim().isEmpty()))
                {
                    CcsiChannelEnum chan = null;

                    try
                    {
                        chan = CcsiChannelEnum.value(x.getTheValue().trim());
                    } catch (Exception chanEx)
                    {
                        nakReg = true;
                        regDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                        regDetailText = "Unknown/Invalid channel " + x.getTheValue();
                        myLogger.error(regDetailText, chanEx);

                        break;
                    }

                    if (this.theContext.getTheConfig().getTheChannels()
                            .isChannelSupported(chan))
                    {
                        regType.setMsgChannel(chan);
                    } else
                    {
                        nakReg = true;
                        regDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                        regDetailText = "Unsupported channel: " + x.getTheValue();
                        myLogger.error(regDetailText);

                        break;
                    }
                } else
                {
                    nakReg = true;
                    regDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                    regDetailText = "Missing channel name";
                    myLogger.error(regDetailText);

                    break;
                }
            } else if (x.getTheName().equalsIgnoreCase("Type"))
            {
                CcsiChannelRegistrationEnum type = null;

                if (regType != null)
                {
                    try
                    {
                        type = CcsiChannelRegistrationEnum.valueOf(x.getTheValue().trim());
                    } catch (Exception typeEx)
                    {
                        nakReg = true;
                        regDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                        regDetailText = "Invalid channel registration type " + x.getTheValue();
                        myLogger.error(regDetailText, typeEx);
                        type = null;

                        break;
                    }

                    if (type != null)
                    {
                        regType.setRegType(type);

                        if (type == CcsiChannelRegistrationEnum.PERIOD)
                        {
                            needPeriodValue = true;
                        }
                    }
                } else
                {
                    nakReg = true;
                    regDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                    regDetailText = "Missing registration type";
                    myLogger.error(regDetailText);

                    break;
                }
            } else if (x.getTheName().equalsIgnoreCase("Rate"))
            {
                if (regType != null)
                {
                    int rate = -1;

                    try
                    {
                        rate = Integer.parseInt(x.getTheValue().trim());
                    } catch (NumberFormatException nfe)
                    {
                        nakReg = true;
                        regDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                        regDetailText = "Invalid register rate value of: " + x.getTheValue().trim();
                        myLogger.error(regDetailText, nfe);

                        break;
                    }

                    if ((rate >= 0) && (rate <= 86400))
                    {
                        if (regType.getMsgChannel() == CcsiChannelEnum.HRTBT)
                        {
                            if (rate <= 3600)
                            {
                                regType.setPeriod(rate);
                                needPeriodValue = false;
                            } else
                            {
                                nakReg = true;
                                regDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                                regDetailText = "Invalid rate value: " + x.getTheValue() + " for HRTBT";
                                myLogger.error(regDetailText);

                                break;
                            }
                        } else
                        {
                            needPeriodValue = false;
                            regType.setPeriod(rate);
                        }
                    } else
                    {
                        nakReg = true;
                        regDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                        regDetailText = "Invalid rate value: " + x.getTheValue();
                        myLogger.error(regDetailText);

                        break;
                    }
                } else
                {
                    nakReg = true;
                    regDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                    regDetailText = "Invalid rate argument";
                    myLogger.error(regDetailText);

                    break;
                }
            } else if (x.getTheName().equalsIgnoreCase("Details"))
            {
                Boolean details = false;

                if (regType != null)
                {
                    if (needPeriodValue)
                    {
                        nakReg = true;
                        regDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                        regDetailText = "Missing rate argument for periodic registration";
                        myLogger.error(regDetailText);

                        break;
                    } else
                    {
                        try
                        {
                            details = Boolean.parseBoolean(x.getTheValue().trim());
                        } catch (Exception dtlEx)
                        {
                            nakReg = true;
                            regDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                            regDetailText = "Invalid boolean argument " + x.getTheValue();
                            myLogger.error(regDetailText, dtlEx);

                            break;
                        }

                        regType.setDetails(details);
                        workList.addChannelRegistration(regType);
                        regType = null;
                    }
                } else
                {
                    nakReg = true;
                    regDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                    regDetailText = "Invalid argument - out of sequence.";
                    myLogger.error(regDetailText);

                    break;
                }
            }
        }    // End for each argument

        if ((regType != null) && (!nakReg))
        {
            workList.addChannelRegistration(regType);
            regType = null;
        }

        if (nakReg)
        {
            sendNack(recvdConnId, recvdMsn, regDetailCode, regDetailText);
            retVal = false;
        } else
        {
            retVal = true;
        }

        return retVal;
    }

    /**
     * Validates a Deregister command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return - true = OK
     */
    public boolean validateDeregisterCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal;
        if (args.isEmpty())
        {
            retVal = true;
        } else
        {
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.TOO_MANY_ARGUMENTS,
                    "Too many arguments - Deregister");
            retVal = false;
        }
        return retVal;
    }

    /**
     * This method validates the parameters of the Set_Config command and builds
     * the argument list.
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args the parsed arguments
     * @param setCfgArgList
     * @param recvdDtg
     *
     * @return flag indicating success/failure
     */
    public boolean validateSet_ConfigCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args, CcsiSetConfigArgumentList setCfgArgList,
            long recvdDtg)
    {
        boolean retVal = false;
        CcsiSetConfigArgument arg = null;
        boolean nakScc = false;
        NakDetailCodeEnum sccDetailCode = null;

        if (args.size() >= 3)
        {
            setCfgArgList.getArgList().clear();

            for (NameValuePair x : args)
            {
                switch (x.getTheName())
                {
                    case "Name":
                        if (arg != null)
                        {
                            // done with one configuration item
                            // validate the item
                            // add it to the list
                            if (arg.isValid() && validateConfigItem(
                                    arg.getItemName(), arg.getItemBlock(), true))
                            {
                                setCfgArgList.addArgument(arg);
                                arg = null;
                            } else
                            {
                                nakScc = true;
                                sccDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                            }
                        } 
                        // start the next configuration item
                        arg = new CcsiSetConfigArgument(recvdConnId, recvdDtg, recvdMsn,
                                this.theContext.getPendingNakDetails());
                        arg.setItemName(x.getTheValue());
                        break;

                    case "Block":
                        if ((arg != null) && (arg.getItemName() != null) && (x.getTheValue() != null))
                        {
                            arg.setItemBlock(CcsiConfigBlockEnum.valueOf(x.getTheValue().trim()));
                        }
                        break;

                    case "Key":
                        if ((arg != null) && (x.getTheValue() != null) && 
                                (arg.getItemName() != null))
                        {
                            arg.setItemKey(x.getTheValue().trim());
                        }
                        break;

                    case "Value":
                        if ((arg != null) && (x.getTheValue() != null) 
                                && (arg.getItemName() != null))
                        {
                            arg.setItemValue(x.getTheValue());

                        } else
                        {
                            nakScc = true;
                            sccDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                            myLogger.error("Invalid value received: " + x.getTheValue());
                        }
                        break;

                    default:
                        nakScc = true;
                        sccDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                        myLogger.error("Invalid argument received: " + x.getTheName());
                }
            }

            if ((arg != null) && (arg.isValid() && validateConfigItem(
                                    arg.getItemName(), arg.getItemBlock(), false)))
            {
                setCfgArgList.addArgument(arg);
            } else
            {
                nakScc = true;
                sccDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                myLogger.error("Invalid argument received");
            }

            if (!nakScc)
            {
                // Validate each argument
                for (CcsiSetConfigArgument x : setCfgArgList.getArgList())
                {
                    retVal = this.setConfigArgVal.validateArgument(x);

                    if (!retVal)
                    {
                        nakScc = true;
                        break;
                    }
                }
            }
        } else
        {
            nakScc = true;
            sccDetailCode = NakDetailCodeEnum.INSUFFICIENT_ARGUMENTS;
            myLogger.error("Not enough arguments for a valid Set_Config command.");
        }

        if (nakScc)
        {
            retVal = false;

            sendNack(recvdConnId, recvdMsn, sccDetailCode, null);
        }

        return retVal;
    }

    /**
     * Validates a Get_Identification command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args - arguments list
     * @return - true = OK
     */
    public boolean validateGet_IdentificationCommand(long recvdConnId,
            long recvdMsn, ArrayList<NameValuePair> args)
    {
        boolean retVal;
        if (args.isEmpty())
        {
            retVal = true;
        } else
        {
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.TOO_MANY_ARGUMENTS,
                    "Too many arguments: Get_Identification");
            myLogger.error("Too many arguments: Get_Identification");
            retVal = false;
        }
        return retVal;
    }

    /**
     * Validates a Set_Date_Time command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @return true = OK
     */
    public boolean validateSet_Date_TimeCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args)
    {
        boolean retVal = true;
        if (args.size() > 2)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.INVALID_ARGUMENT,
                    "Incorrect Number of Arguments");
        }
        return retVal;
    }

    /**
     * Validates a Get_Alert_Details command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @param alertIdPattern
     * @return true = success
     */
    public boolean validateGet_Alert_DetailsCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args, Pattern alertIdPattern)
    {
        boolean retVal;
        boolean nakGad = false;
        NakDetailCodeEnum gadDetailCode = null;

        if (args.size() == 1)
        {
            NameValuePair x = args.get(0);

            if (x.getTheName().equals("AlertID"))
            {
                if ((x.getTheValue() != null) && (!x.getTheValue().trim().isEmpty()))
                {
                    Matcher aidMatcher = alertIdPattern.matcher(x.getTheValue().trim());

                    if (aidMatcher.find())
                    {
                        retVal = true;
                    }
                }
            } else
            {
                nakGad = true;
                gadDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                myLogger.error("Invalid alert ID argument");
            }
        } else if (args.size() > 1)
        {
            nakGad = true;
            gadDetailCode = NakDetailCodeEnum.TOO_MANY_ARGUMENTS;
            myLogger.error("Too many arguments");
        } else
        {
            nakGad = true;
            gadDetailCode = NakDetailCodeEnum.INSUFFICIENT_ARGUMENTS;
            myLogger.error("Not enough arguments");
        }

        if (nakGad)
        {
            sendNack(recvdConnId, recvdMsn, gadDetailCode,
                    "Too many arguments: Get_Alert_Details");
            retVal = false;
        } else
        {
            retVal = true;
        }

        return retVal;
    }

    /**
     * Validates the Get_Readings_Details command
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args
     * @param rdgIdPattern
     * @return true = success
     */
    public boolean validateGet_Reading_DetailsCommand(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args, Pattern rdgIdPattern)
    {
        boolean retVal;
        boolean nakGrd = false;
        NakDetailCodeEnum grdDetailCode = null;

        if (args.size() == 1)
        {
            NameValuePair x = args.get(0);

            if (x.getTheName().equals("ReadingID"))
            {
                if ((x.getTheValue() != null) && (!x.getTheValue().trim().isEmpty()))
                {
                    Matcher rdgMatcher = rdgIdPattern.matcher(x.getTheValue().trim());

                    if (rdgMatcher.find())
                    {
                        retVal = true;
                    } else
                    {
                        nakGrd = true;
                        grdDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                        myLogger.error("Invalid reading ID argument");
                    }
                }
            } else
            {
                nakGrd = true;
                grdDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                myLogger.error("Invalid reading ID argument");
            }
        } else if (args.size() > 1)
        {
            nakGrd = true;
            grdDetailCode = NakDetailCodeEnum.TOO_MANY_ARGUMENTS;
            myLogger.error("Too many arguments");
        } else
        {
            nakGrd = true;
            grdDetailCode = NakDetailCodeEnum.INSUFFICIENT_ARGUMENTS;
            myLogger.error("Not enough arguments");
        }

        if (nakGrd)
        {
            sendNack(recvdConnId, recvdMsn, grdDetailCode,
                    "Not enough arguments: Get_Get_Readings_Details");
            retVal = false;
        } else
        {
            retVal = true;
        }
        return retVal;
    }

    /**
     * This method validates the parameters of the Get_Config command and builds
     * the argument list.
     *
     * @param recvdConnId
     * @param recvdMsn
     * @param args the parsed arguments
     * @param getCfgArgList
     *
     * @return flag indicating success/failure
     */
    public boolean validateGetConfig(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args,
            CcsiGetConfigArgumentList getCfgArgList)
    {
        boolean retVal;
        CcsiGetConfigArgument arg = null;
        boolean nakGcc = false;
        NakDetailCodeEnum gccDetailCode = null;

        if (args.size() >= 2)
        {
            if (getCfgArgList != null)
            {
                getCfgArgList.clear();
            }

            for (NameValuePair x : args)
            {
                switch (x.getTheName())
                {
                    case "Item":
                        if (getCfgArgList != null && arg != null)
                        {
                            getCfgArgList.addArgument(arg);
                            arg = null;
                        }

                        if ((x.getTheValue() != null) && (!x.getTheValue().isEmpty()))
                        {
                            arg = new CcsiGetConfigArgument();
                            arg.setItemName(x.getTheValue());
                        } else
                        {
                            nakGcc = true;
                            gccDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                        }

                        break;

                    case "Block":
                        if (arg == null)
                        {
                            nakGcc = true;
                            gccDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                        } else
                        {
                            try
                            {
                                CcsiConfigBlockEnum blk = CcsiConfigBlockEnum.valueOf(x.getTheValue().trim());

                                arg.setItemBlock(blk);
                            } catch (Exception bnEx)
                            {
                                nakGcc = true;
                                gccDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                            }
                        }

                        break;

                    case "Key":
                        if (arg == null)
                        {
                            nakGcc = true;
                            gccDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                        } else
                        {
                            if (arg.getItemBlock() == null)
                            {
                                nakGcc = true;
                                gccDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                            } else if (!arg.isValid())
                            {
                                nakGcc = true;
                                gccDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                            } else
                            {
                                arg.setItemKey(x.getTheValue());

                                if (!arg.isValid())
                                {
                                    nakGcc = true;
                                    gccDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                                }
                            }
                        }

                        break;

                    default:
                        nakGcc = true;
                        gccDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;

                        break;
                }

                if ((arg == null) || (!arg.isValid()))
                {
                    nakGcc = true;
                    gccDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                }
            }

            if (arg != null)
            {
                if (getCfgArgList != null)
                {
                    getCfgArgList.addArgument(arg);
                }
            }
        } else
        {
            nakGcc = true;
            gccDetailCode = NakDetailCodeEnum.INSUFFICIENT_ARGUMENTS;
        }

        if (nakGcc)
        {
            sendNack(recvdConnId, recvdMsn, gccDetailCode, null);
            retVal = false;
        } else
        {
            retVal = true;
        }

        return retVal;
    }

    /**
     * This method validates the parameters of the Get_Config command and builds
     * the argument list.
     *
     * @param args the parsed arguments
     *
     * @return flag indicating success/failure
     */
    private boolean validateGetConfigArguments(long recvdConnId, long recvdMsn,
            ArrayList<NameValuePair> args, CcsiGetConfigArgumentList getCfgArgList)
    {
        boolean retVal = false;
        CcsiGetConfigArgument arg = null;
        boolean nakGcc = false;
        NakDetailCodeEnum gccDetailCode = null;

        if (args.size() >= 2)
        {
            if (getCfgArgList != null)
            {
                getCfgArgList.clear();
            }

            for (NameValuePair x : args)
            {
                switch (x.getTheName())
                {
                    case "Item":
                        if (arg != null && validateConfigItem(
                                    arg.getItemName(), arg.getItemBlock(), true))
                        {
                            getCfgArgList.addArgument(arg);
                            arg = null;
                        }

                        if ((x.getTheValue() != null) && (!x.getTheValue().isEmpty()))
                        {
                            arg = new CcsiGetConfigArgument();
                            arg.setItemName(x.getTheValue());
                        } else
                        {
                            nakGcc = true;
                            gccDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                        }

                        break;

                    case "Block":
                        if (arg == null)
                        {
                            nakGcc = true;
                            gccDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                        } else
                        {
                            try
                            {
                                CcsiConfigBlockEnum blk = CcsiConfigBlockEnum.valueOf(x.getTheValue().trim());

                                arg.setItemBlock(blk);
                            } catch (Exception bnEx)
                            {
                                nakGcc = true;
                                gccDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                            }
                        }

                        break;

                    case "Key":
                        if (arg == null)
                        {
                            nakGcc = true;
                            gccDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                        } else
                        {
                            if (!arg.isValid())
                            {
                                nakGcc = true;
                                gccDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                            } else
                            {
                                arg.setItemKey(x.getTheValue());

                                if (!arg.isValid())
                                {
                                    nakGcc = true;
                                    gccDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                                }
                            }
                        }

                        break;

                    default:
                        nakGcc = true;
                        gccDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;

                        break;
                }

                if ((arg == null) || (!arg.isValid()))
                {
                    nakGcc = true;
                    gccDetailCode = NakDetailCodeEnum.MISSING_ARGUMENT;
                }
            }

            if (arg != null && validateConfigItem(
                arg.getItemName(), arg.getItemBlock(), true))
            {
                getCfgArgList.addArgument(arg);
            } else
            {
                nakGcc = true;
                gccDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
            }
        } else
        {
            nakGcc = true;
            gccDetailCode = NakDetailCodeEnum.INSUFFICIENT_ARGUMENTS;
        }

        if (nakGcc)
        {
            PendingSendEntry vdrcPse = new PendingSendEntry();

            vdrcPse.setConnectionId(recvdConnId);
            vdrcPse.addChannel(CcsiChannelEnum.CMD);
            vdrcPse.setSendNak(true);
            vdrcPse.setAckMsn(recvdMsn);
            vdrcPse.setAckType(CcsiAckNakEnum.NACK);
            vdrcPse.setNakCode(NakReasonCodeEnum.MSGCON);
            vdrcPse.setNakDetailCode(gccDetailCode);
            this.theContext.getPendingSends().addPendingSend(vdrcPse);
            retVal = false;
        } else
        {
            retVal = true;
        }

        return (retVal);
    }

    private boolean validateConfigItem (String itemName, CcsiConfigBlockEnum block, boolean getConfig)
    {
        boolean supported = false;
        boolean blockValid = true;
        String blockString = null;
        String blockName = null;
        
        if (block != null)
        {
            blockString = block.toString();
        }
        
            switch (itemName)
            {
                // BASE block
                case "EncryptionType":
                    supported = true;
                    blockName = "BASE";
                    break;
                // COMM block
                case "HeartbeatRate":
                case "HostNetworkType":
                case "OpenInterfacePort":
                case "OpenInterfaceServer":
                case "AckTimeout":
                case "ConnectionLimit":
                case "ConnRetryTimeout":
                    supported = true;
                    blockName = "COMM";
                    break;
                    
                // GENERAL block
                case "TimeSource":
                case "LocationSource":
                case "SelectedTimeSource":
                case "SelectedLocationSource":
                case "UnitMode":
                case "LocationReportOption":
                    supported = true;
                    blockName = "GENERAL";
                    break;

                case "DismountedPowerWarningLevel":
                case "DismountedPowerShutdownLevel":
                    supported = false;
                    break;

                // LOCAL block
                case "HostSensorId":
                case "HostSensorName":
                case "HostSensorMount":
                case "HostSensorDescription":
                    supported = true;
                    blockName = "LOCAL";
                    break;

                // SENSDESC block
                case "ComponentType":
                case "Present":
                case "HardwareVersion":
                case "SoftwareVersion":
                case "BitPass":
                    // can't set these items
                    if (getConfig)
                    {
                        supported = true;
                        blockName = "SENSDESC";
                    }
                    break;

                case "SerialNumber":
                    supported = true;
                    blockName = "SENSDESC";
                    break;
                    
                case "AccessRole":
                case "AnnunciationType":
                    supported = false;
                    break;
                    
                // LINKS block
                case "LinkName":
                case "LinkNetType":
                case "LinkBandwidthHigh":
                case "LinkEncryption":
                case "LinkCompression":
                case "LinkCompressionThreshold":
                case "ModifyRole":
                case "IpAddress":
                case "NetMask":
                case "GatewayAddress":
                case "IpAddressFixed":
                case "MaxTransmitSize":
                case "LinkPortNbr":
                case "BroadcastIp":
                case "EmconMode":
                case "LinkSpeed":
                    supported = true;
                    blockName = "LINKS";
                    break;

                // ANNUN block
                case "AudibleAnnunState":
                case "VisualAnnunState":
                case "TactileAnnunState":
                case "LedAnnunState":
                case "ExternalAnnunState":
                    supported = true;
                    blockName = "ANNUN";
                    break;

                // OPERATORS block
                case "OperatorName":
                case "OperatorOperatorRoles":
                case "OperatorLastLogin":
                case "OperatorFailureCount":
                case "OperatorLastPwChange":
                    supported = false;
                    break;

                // SECURITY block
                case "EncryptComm":
                case "EncryptData":
                case "ConnectFailTimeout":
                    supported = true;
                    blockName = "SECURITY";
                    break;

                case "HostAuthentication":
                case "OperatorAuthentication":
                case "UserPwdTimeout":
                case "PwReuseLength":
                case "LoginFailTimeout":
                    supported = false;
                    break;

                case "Count":
                case "Window":
                case "Action":
                case "Enable":
                    // can't set these items
                    supported = false;
                    break;

                default:
                    // Sensor UNIQUE items
                    SensorUniqueConfigItem configItem = theContext.
                            getTheConfig().getSensorUniqueConfig(itemName);
                    
                    if (configItem != null)
                    {
                        supported = true;
                        blockName = "UNIQUE";
                        break;
                    }
                    break;
            }
            if (blockString != null)
            {
                blockValid = (blockString.isEmpty() || blockString.equals(blockName));
            }
            return (supported && blockValid);
    }
    
    /**
     * Sends a NACK back to the host
     *
     * @param receivedConnectionID
     * @param recvdMsn
     * @param nakDetailCode
     * @param detailText - optional, may be null
     */
    private void sendNack(long receivedConnectionID, long recvdMsn,
            NakDetailCodeEnum nakDetailCode,
            String detailText)
    {
        PendingSendEntry grdPse = new PendingSendEntry();
        grdPse.setConnectionId(receivedConnectionID);
        grdPse.setSendNak(true);
        grdPse.setAckType(CcsiAckNakEnum.NACK);
        grdPse.setNakCode(NakReasonCodeEnum.MSGCON);
        grdPse.setNakDetailCode(nakDetailCode);
        grdPse.setAckMsn(recvdMsn);
        grdPse.addChannel(CcsiChannelEnum.CMD);
        if (detailText != null)
        {
            grdPse.setNakDetailMessage(detailText);
        }
        theContext.getPendingSends().addPendingSend(grdPse);
    }

}
