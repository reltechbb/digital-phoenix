/*
 * PendingSendList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/16
 */
package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------
import java.util.concurrent.LinkedBlockingQueue;

//~--- classes ----------------------------------------------------------------
/**
 * This class implements a list of pending sends to the host.
 *
 * @author kmiller
 */
public class PendingSendList {

    /**
     * The state has changed and needs processing
     */
    private boolean hasChanged = false;

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger = Logger.getLogger(PendingSendList.class);

    /**
     * The list of pending entries
     */
    private final LinkedBlockingQueue<PendingSendEntry> theList;

    /**
     * is logging level debug
     */
    private final boolean logLevelDebug;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs the list
     */
    public PendingSendList() {
        theList = new LinkedBlockingQueue<>(20);

        logLevelDebug = myLogger.isDebugEnabled();
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Adds an item to the list and returns success/failure
     *
     * @param ent the entry to be added
     *
     * @return success or failure flag
     */
    public boolean addPendingSend(PendingSendEntry ent) {
        boolean retVal = false;

        if (ent != null) {
            try {
                retVal = this.theList.add(ent);
            } catch (IllegalStateException ex) {
                retVal = false;
                myLogger.error("PendingSendQueue full");
                theList.poll();
            }
            //myLogger.info ("Pending Send Queue size: " + theList.size());
            if (this.theList.size() >= 1) {
                this.hasChanged = true;
            }
        }

        if (logLevelDebug) {
            if (ent != null) {
                myLogger.info("Added pending send entry: ACK " + ent.isSendAck() + " NAK " + ent.isSendNak() + " HRTBT "
                        + ent.isSendHrtbt() + " Rept " + ent.isSendReport());
            } else {
                myLogger.info("Added pending send entry");
            }
        }

        return (retVal);
    }

    /**
     * Pops the next entry from the list returning either the entry of null if the list is empty
     *
     * @return the top entry or null if empty
     */
    public PendingSendEntry removeNextEntry() {
        PendingSendEntry temp = null;

        if (!theList.isEmpty()) {
            temp = theList.poll();
            this.hasChanged = true;
            //myLogger.info("Removed pending send entry: " + temp.getMessage());
        }

        return (temp);
    }

    /**
     * Returns the next entry from the list without removing it from the queue
     *
     * @return the top entry or null if empty
     */
    public PendingSendEntry peekNextEntry() {
        if (theList.isEmpty()) {
            return (null);
        } else {
            return (theList.peek());
        }
    }

    /**
     * Clears all the entries from the list
     */
    public void clearList() {
        myLogger.debug("List cleared.");
        this.theList.clear();
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Returns the number of entries in the list.
     *
     * @return the entry count
     */
    public int getSize() {
        return (this.theList.size());
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Returns an indication of whether or not the queue had an entry removed or if the first new
     * entry was inserted.
     *
     * @return boolean needsProcessing
     */
    public boolean needsProcessing() {
        boolean retVal = this.hasChanged;

        this.hasChanged = false;
        myLogger.info("Pending Send List needs processing: " + retVal);

        return (retVal);
    }
}
