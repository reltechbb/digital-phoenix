/*
 * PendingSensorResponseList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/16
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.concurrent.ConcurrentHashMap;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a list of pending responses from the sensor.
 * 
 * @author kmiller
 */
public class PendingSensorResponseList
{
  /** The map of entries */
  private final ConcurrentHashMap<String , PendingSensorResponse>	theMap	= new ConcurrentHashMap<>( 6 );

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs the class
   */
  public PendingSensorResponseList()
  {
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Add a new entry to the table
   *
   * @param resp the pending response to be added
   *
   * @return flag indicating success of inserting this key
   */
  public boolean addPendingResponse( PendingSensorResponse resp )
  {
    boolean								retVal	= false;
    PendingSensorResponse	temp		= theMap.putIfAbsent( resp.getRouting() , resp );

    if ( temp.getTimeQueued() == resp.getTimeQueued() )
    {
      retVal	= true;
    }

    return ( retVal );
  }

  /**
   * Remove a routing key from the pending list
   *
   * @param routing the key to be removed
   *
   * @return flag indicating success or failure
   */
  public boolean removeResponse( String routing )
  {
    boolean								retVal	= false;
    PendingSensorResponse	temp		= this.theMap.remove( routing );

    if ( temp != null )
    {
      retVal	= true;
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns a flag indicating if this key is in the table
   *
   * @param routing the routing key to check
   *
   * @return flag indicating the key was found or not
   */
  public boolean hasRouting( String routing )
  {
    boolean	retVal	= false;

    retVal	= this.theMap.containsKey( routing );

    return ( retVal );
  }
}
