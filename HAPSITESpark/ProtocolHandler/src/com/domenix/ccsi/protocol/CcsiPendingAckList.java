/*
 * CcsiPendingAckList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of a list of pending acknowledgements for the sensor.
 *
 * Version: V1.0  15/09/10
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.common.utils.TimerPurposeEnum;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

//~--- classes ----------------------------------------------------------------

/**
 * This class encapsulates a list of pending host acknowledgement of sensor transmitted messages.
 *
 * @author kmiller
 */
public class CcsiPendingAckList
{
  /** The saved state of the sensor */
  private CcsiSavedState	ctx	= null;

  /** The list of pending ACKs */
  private final HashMap<Long , CcsiPendingAck>	theList	= new HashMap<>();
  
  /** The list of pending ACKs */
  private final HashMap<CcsiChannelEnum , CcsiPendingAck>	theListChan	= new HashMap<>();

  /** A list of pending ACKs that need an MSN */
  private final LinkedHashMap<CcsiChannelEnum , CcsiPendingAck>	theEnumList	= new LinkedHashMap<>();

  /** The error / debug logger */
  private final Logger	myLogger	= Logger.getLogger( CcsiPendingAckList.class );

  /** The sensor configuration */
  private final CcsiConfigSensor	theConfig;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance with a reference to the saved state.
   *
   * @param theConfig
   * @param theSavedState
   */
  public CcsiPendingAckList( CcsiConfigSensor theConfig , CcsiSavedState theSavedState )
  {
    this.theConfig	= theConfig;
    this.ctx				= theSavedState;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Erase the list contents
   */
  public void clearList()
  {
    Iterator	iter	= this.theList.keySet().iterator();

    while ( iter.hasNext() )
    {
      CcsiPendingAck	x	= this.theList.get( (Long) iter.next() );

      if ( x.getTimer() >= 0L )
      {
        this.ctx.getTimerBase().cancelTimer( x.getTimer() );
      }
    }

    this.theList.clear();
    this.theListChan.clear();
    this.theEnumList.clear();
  }

  /**
   * Adds a new pending acknowledgement to the list.
   *
   * @param ack the pending acknowledgement to be added
   *
   * @return flag indicating success or failure
   */
  public boolean addPendingAck( CcsiPendingAck ack )
  {
    boolean	retValue	= false;

    if ( ack.getMsn() == -1L )
    {
      if ( !this.theEnumList.containsKey( ack.getChannels()[0] ) )
      {
        this.theEnumList.put( ack.getChannels()[0] , ack );
      }
    }

    if ( this.theList.put( ack.getMsn() , ack ) == null )
    {
      retValue	= true;
    }

    return ( retValue );
  }

  /**
   * Mark the ack as received and remove the pending entry
   *
   * @param msn the MSN to be marked and removed
   *
   * @return flag indicating success or failure
   */
  public boolean ackReceived( long msn )
  {
    boolean					retValue	= false;
    CcsiPendingAck	x					= null;

    x	= this.theList.get( msn );

    if ( x != null )
    {
      if ( x.getTimer() >= 0L )
      {
        this.ctx.getTimerBase().cancelTimer( x.getTimer() );
      }

      if ( this.theList.remove( msn ) == null )
      {
        myLogger.error( "Error removing pending ACK entry for MSN " + msn );
      }
      else
      {
        myLogger.debug( "Removed pending ack for MSN " + msn );
        retValue	= true;
      }
    }

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns an indication of whether a pending acknowledgement for the specified MSN
   * is in the list.
   *
   * @param msn the MSN to test
   *
   * @return flag indicating it is (true) or is not (false) in the list
   */
  public boolean hasPendingAck( long msn )
  {
    boolean	retValue	= false;

    retValue	= this.theList.containsKey( msn );

    return ( retValue );
  }

  /**
   * Returns an indication of whether a pending acknowledgement for the specified channel
   * is in the list.
   *
   * @param chan the channel to test
   *
   * @return flag indicating it is (true) or is not (false) in the list
   */
  public CcsiPendingAck hasPendingAck( CcsiChannelEnum chan )
  {
    CcsiPendingAck	retValue	= null;

    if ( this.theEnumList.containsKey( chan ) )
    {
      retValue	= this.theEnumList.get( chan );
    }

    return ( retValue );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Used to activate a pending ACK when a sent event indicates that it was sent.  It pulls the
   * entry from the waiting list, starts the ACK timer, and puts it in the pending list.
   *
   * @param chan the channel enumeration
   * @param msn the sent MSN
   * @param timer the ID of the ACK timer
   *
   * @return success/failure
   */
  public boolean setupUpPendingAck( CcsiChannelEnum chan , long msn , TimerEventHandler timer )
  {
    boolean	retVal	= false;

    if ( this.theEnumList.containsKey( chan ) )
    {
      CcsiPendingAck	ent	= this.theEnumList.remove( chan );

      ent.setMsn( msn );
      ent.setTimer( timer.startNewTimer( this.theConfig.getAckTimeout() , false , chan , "PendingACK" ,
                                         TimerPurposeEnum.ACK_TIMER ) );
      this.theList.put( msn , ent );
      this.theListChan.put( chan , ent );
      retVal	= true;
    }

    return ( retVal );
  }

  /**
   * Removes a waiting ACK because the send failed.
   *
   * @param chan the channel to be removed
   * @return the pending ACK that was removed or null if none were found
   */
  public CcsiPendingAck removeWaitingAck( CcsiChannelEnum chan )
  {
    if ( this.theEnumList.containsKey( chan ) )
    {
      CcsiPendingAck ack = this.theEnumList.remove( chan );
      return( ack );
    }
    if ( this.theListChan.containsKey( chan ) )
    {
      CcsiPendingAck ack = this.theListChan.get( chan );
      long msn = ack.getMsn();
      if ( ack.getTimer() >= 0L )
      {
        this.ctx.getTimerBase().cancelTimer( ack.getTimer() );
        this.theListChan.remove( chan );
        this.theList.remove( msn );
      }
      return( ack );
    }
    return( null );
  }
}
