/*
 * CommandExecuter.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This class contains methods for executing command messages.
 *
 */
package com.domenix.ccsi.protocol;

import com.domenix.ccsi.AnnunciatorStateEnum;
import com.domenix.commonha.ccsi.CcsiAckNakEnum;
import com.domenix.ccsi.CcsiBitStatusEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.CcsiChannelRegistrationEnum;
import com.domenix.ccsi.CcsiComponentStateEnum;
import com.domenix.ccsi.CcsiConfigBlockEnum;
import com.domenix.ccsi.CcsiEmconModeEnum;
import com.domenix.ccsi.CcsiLinkNameType;
import com.domenix.ccsi.CcsiLinkTypeEnum;
import com.domenix.ccsi.CcsiLocationReportOptionEnum;
import com.domenix.ccsi.CcsiLocationSourceEnum;
import com.domenix.commonha.ccsi.CcsiModeEnum;
import com.domenix.ccsi.CcsiSensorMountingEnum;
import com.domenix.commonha.ccsi.ChannelRegistrationList;
import com.domenix.commonha.ccsi.ChannelRegistrationType;
import com.domenix.ccsi.CommandHandlingType;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;
import com.domenix.common.ccsi.NameValuePair;
import com.domenix.ccsi.CcsiTimeSourceEnum;
import com.domenix.common.config.SensorUniqueConfigItem;
import com.domenix.ccsi.config.SensorUniqueDataItem;
import com.domenix.common.enums.NetworkProtocolTypeEnum;
import com.domenix.commonha.intfc.NSDSConnection;
import com.domenix.commonha.intfc.NSDSConnectionTypeEnum;
import com.domenix.common.spark.IPCWrapper;
import com.domenix.common.spark.data.AllString;
import com.domenix.common.spark.data.AnnunciatorType;
import com.domenix.common.spark.data.CcsiCommandedModeEnum;
import com.domenix.common.spark.data.ConfigItemBlockType;
import com.domenix.common.spark.data.EnableOrDisable;
import com.domenix.common.spark.data.EraseSecLogArg;
import com.domenix.common.spark.data.FunctionNameType;
import com.domenix.common.spark.data.GetAlertArg;
import com.domenix.common.spark.data.GetCfgArg;
import com.domenix.common.spark.data.GetConfigArgs;
import com.domenix.common.spark.data.GetReadingArg;
import com.domenix.common.spark.data.GetSecLogArg;
import com.domenix.common.spark.data.LocalAlertModeArgs;
import com.domenix.common.spark.data.LocationSourceType;
import com.domenix.common.spark.data.MsgContent;
import com.domenix.common.spark.data.ObjectFactory;
import com.domenix.common.spark.data.PowerOffArg;
import com.domenix.common.spark.data.RebootArgs;
import com.domenix.common.spark.data.RenderUselessArg;
import com.domenix.common.spark.data.SensorUniqueArg;
import com.domenix.common.spark.data.SensorUniqueArgList;
import com.domenix.common.spark.data.SetCfgArg;
import com.domenix.common.spark.data.SetConfigArgs;
import com.domenix.common.spark.data.SetLocArgs;
import com.domenix.common.spark.data.SetLocEnableArg;
import com.domenix.common.spark.data.SetLocEnableArgs;
import com.domenix.common.spark.data.SilenceArgs;
import com.domenix.common.spark.data.SilenceType;
import com.domenix.common.spark.data.StartSelfTestArgs;
import com.domenix.common.spark.data.StartSelfTestComponentEnum;
import com.domenix.common.spark.data.StartStopArgs;
import com.domenix.common.spark.data.StartStopEnum;
import com.domenix.common.spark.data.TimeSourceEnum;
import com.domenix.common.utils.SystemDateTime;
import com.domenix.common.utils.TimerPurposeEnum;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * This class contains methods for executing command messages.
 *
 * @author jmerritt
 */
public class CommandExecuter
{

    /**
     * The PH Context
     */
    private final PHContext theContext;

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger; 

    /**
     * Constructor
     *
     * @param theContext
     */
    public CommandExecuter(PHContext theContext)
    {
        this.theContext = theContext;
        myLogger = Logger.getLogger(CommandExecuter.class);
    }

    /**
     * Executes a Power_Off command
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param argList
     * @return 
     */
    public boolean executePower_Off(CommandHandlingType h,
            long recvdConnId, long recvdMsn, CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, 
                        "Unsupported Command: Power_Off");
                break;
            case passtosensor:
                sendAck(h, recvdConnId, recvdMsn);

                ObjectFactory objFact2 = new ObjectFactory();

                MsgContent sscMsg = objFact2.createMsgContent();
                sscMsg.setCmd(objFact2.createCommandMsg());
                PowerOffArg powerOffArg = new PowerOffArg();
                if (argList.theList.size() == 1)
                {
                    powerOffArg.setDelay(Integer.valueOf(argList.theList.get(0).getTheValue()));
                }
                sscMsg.getCmd().setPowerOff(powerOffArg);
                sscMsg.getCmd().setCmd("Power_Off");
                IPCWrapper sscCmd = new IPCWrapper("ccsi.cmd.Power_Off", sscMsg);

                theContext.getSensorOutQueue().insertLast(sscCmd);
                break;
        }
        return retValue;
    }

    /**
     * Executes a Render_Useless command.
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @param argList
     * @return 
     */
    public boolean executeRender_Useless(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn, CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
                // TODO: implement something here when we figure it out
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, 
                        "Unsupported Command: Render_Useless (internal)");
                break;
            case passtosensor:
                sendAck(h, receivedConnectionID, receivedMsn);

                ObjectFactory objFact2 = new ObjectFactory();

                MsgContent sscMsg = objFact2.createMsgContent();
                sscMsg.setCmd(objFact2.createCommandMsg());
                RenderUselessArg uselessArg = objFact2.createRenderUselessArg();
                // No parameters means "Do It Now"
                if (argList.theList.size() > 0)
                {
                    uselessArg.setTamperCount(Integer.valueOf(
                            argList.theList.get(0).getTheValue()));
                }
                sscMsg.getCmd().setRenderUseless(uselessArg);
                        
                sscMsg.getCmd().setCmd("Set_Render_Useless");
                IPCWrapper sscCmd = new IPCWrapper("ccsi.cmd.Render_Useless", sscMsg);

                theContext.getSensorOutQueue().insertLast(sscCmd);
                break;
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, 
                        "Unsupported Command: Render_Useless");
                break;
        }
        return retValue;

    }

    /**
     * Executes a Set_Tamper command
     *
     * This command is not supported by any known sensor and will always return
     * false
     *
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @return
     */
    public boolean executeSet_Tamper(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;

    }

    /**
     * Executes a Set_Emcon_Mode command
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @param argList
     * @return 
     */
    public boolean executeSet_Emcon_Mode(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn, CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
                CcsiEmconModeEnum emcon = CcsiEmconModeEnum.
                        valueOf(argList.theList.get(0).getTheValue());
                theContext.getTheSavedState().setEmcon(emcon);
                {
                    try
                    {
                        theContext.getTheSavedState().writeSavedState();
                    } catch (Exception ex)
                    {
                        myLogger.error("Exception writing saved state");
                    }
                }

                sendAck(h, receivedConnectionID, receivedMsn);
                break;
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, 
                        "Unsupported Command: Set_Emcon_Mode");
                break;
        }
        return retValue;

    }

    /**
     * Executes a Set_Encryption command
     * 
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @param argList
     * @return 
     */
    public boolean executeSet_Encryption(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn, CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
                for (NameValuePair pair : argList.theList)
                {
                    if (pair.getTheName().equalsIgnoreCase("EnableOrDisable"))
                    {
                        boolean encrypt = false;
                        if (pair.getTheValue().equalsIgnoreCase("enable"))
                        {
                            encrypt = true;
                        }
                        theContext.getTheSavedState().setStateLinkEncrypt(encrypt);
                        retValue = true;
                    }
                }
                sendAck(h, receivedConnectionID, receivedMsn);
                break;
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;

    }

    /**
     * Executes an Erase_Sec_Log command
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @param argList
     * @return 
     */
    public boolean executeErase_Sec_Log(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn,
            CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, 
                        "Unsupported Command: Erase_Sec_Log (internal)");
                break;
            case passtosensor:
                sendAck(h, receivedConnectionID, receivedMsn);

                ObjectFactory objFact2 = new ObjectFactory();

                MsgContent sscMsg = objFact2.createMsgContent();
                sscMsg.setCmd(objFact2.createCommandMsg());
                EraseSecLogArg secLogArg = objFact2.createEraseSecLogArg();
                if (argList.theList.size() > 0)
                {
                    secLogArg.setEraseAll(AllString.ALL);
                }
                sscMsg.getCmd().setEraseSecLog(secLogArg);
                sscMsg.getCmd().setCmd("Erase_Sec_Log");

                IPCWrapper sscCmd = new IPCWrapper("ccsi.cmd.Erase_Sec_Log", sscMsg);

                theContext.getSensorOutQueue().insertLast(sscCmd);
                retValue = true;
                break;
        }
        return retValue;

    }

    /**
     * Executes a Get_Sec_Log command
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @param argList
     * @return 
     */
    public boolean executeGet_Sec_Log(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn,
            CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case passtosensor:
                sendAck(h, receivedConnectionID, receivedMsn);

                ObjectFactory objFact2 = new ObjectFactory();

                MsgContent sscMsg = objFact2.createMsgContent();
                sscMsg.setCmd(objFact2.createCommandMsg());
                GetSecLogArg secLogArg = objFact2.createGetSecLogArg();
                if (argList.theList.size() > 0)
                {
                    secLogArg.setSince(argList.theList.get(0).getTheValue());
                }
                sscMsg.getCmd().setGetSecLog(secLogArg);
                sscMsg.getCmd().setCmd("Get_Sec_Log");

                IPCWrapper sscCmd = new IPCWrapper("ccsi.cmd.Get_Sec_Log", sscMsg);

                theContext.getSensorOutQueue().insertLast(sscCmd);
                retValue = true;
                break;
            case internal:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, 
                        "Unsupported Command: Get_Sec_Log (internal)");
                break;
        }
        return retValue;

    }

    /**
     * Executes the Sanitize command
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @return 
     */
    public boolean executeSanitize(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
                sendAck(h, receivedConnectionID, receivedMsn);
                sanitize ();
                retValue = true;
                break;
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, 
                        "Unsupported Command: Sanitize");
                break;
        }
        return retValue;

    }

    /**
     * Executes the Zeroize command
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @return 
     */
    public boolean executeZeroize(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
                sendAck(h, receivedConnectionID, receivedMsn);
                sanitize ();
                retValue = true;
                break;
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, 
                        "Unsupported Command: Zeroize");
                break;
        }
        return retValue;

    }

    /**
     * Executes a Set_Cmd_Privilege command
     *
     * This command is not supported by any known sensor and will always return
     * false
     *
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @return
     */
    public boolean executeSet_Cmd_Privilege(CommandHandlingType h,
            long recvdConnId, long recvdMsn)
    {
        boolean retVal = false;

        switch (h.getHandle())
        {
            case internal:
            case passtosensor:
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retVal;

    }

    /**
     * Executes a Set_Config_Privilege command
     *
     * This command is not supported by any known sensor and will always return
     * false
     *
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @return
     */
    public boolean executeSet_Config_Privilege(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;

    }

    /**
     * Executes a Reset_Msg_Sequence command
     *
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param conn
     * @return - true = OK
     */
    public boolean executeReset_Msg_Seq(CommandHandlingType h,
            long recvdConnId, long recvdMsn, NSDSConnection conn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
                conn.getConnectionMsns().setNextReceiveMsn(1L);
                conn.getConnectionMsns().setNextTransmitMsn(1L);

                sendAck(h, recvdConnId, recvdMsn);
                myLogger.info("Reset Message Sequence to 1");
                break;
            case passtosensor:
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;

    }

    /**
     * Executes a Start_Stop command
     *
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param argList
     * @return - true = OK
     */
    public boolean executeStart_Stop(CommandHandlingType h,
            long recvdConnId, long recvdMsn, CommandArgumentList argList)
    {
        boolean retValue = false;
                        StartStopEnum sse = StartStopEnum.valueOf(argList.theList.get(0).getTheValue());

        switch (h.getHandle())
        {
            case internal:
                theContext.getTheSavedState().setSensingStopped((sse == StartStopEnum.END));

                if (theContext.getTheSavedState().getConnList().getItem(recvdConnId).getConnType()
                        == NSDSConnectionTypeEnum.PERSISTENT_CONNECTION)
                {
                    ChannelRegistrationList regs
                            = theContext.getTheSavedState().getConnList().
                                    getItem(recvdConnId).getRegisteredChannels();

                    if (regs.hasChannel(CcsiChannelEnum.READGS))
                    {
                        if (sse == StartStopEnum.END)
                        {
                            // Remove all pending entries and cancel the timer if there is one
                            theContext.getPendingReadings().getAllEntries();

                            long timerId = regs.getRegistration(CcsiChannelEnum.READGS).getPeriodTimer();

                            if (timerId >= 0L)
                            {
                                theContext.getTimerEventHandler().cancelTimer(timerId);
                            }
                        } else
                        {
                            ChannelRegistrationType rReg = regs.getRegistration(CcsiChannelEnum.READGS);

                            if (rReg.getRegType() == CcsiChannelRegistrationEnum.PERIOD)
                            {
                                long timerId = theContext.getTimerEventHandler().
                                        startNewTimer(rReg.getPeriod(), true, rReg,
                                        "PERIOD", TimerPurposeEnum.PERIODIC_TIMER);

                                rReg.setPeriodTimer(timerId);
                            }
                        }
                    }
                }
                break;
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
            case passtosensor:
                sendAck(h, recvdConnId, recvdMsn);

                ObjectFactory objFact2 = new ObjectFactory();

                MsgContent sscMsg = objFact2.createMsgContent();
                sscMsg.setCmd(objFact2.createCommandMsg());
                StartStopArgs startStopArgs = objFact2.createStartStopArgs();
                startStopArgs.setStartOrStop(StartStopEnum.valueOf(argList.theList.get(0).getTheValue()));
                sscMsg.getCmd().setStartStop(startStopArgs);
                sscMsg.getCmd().setCmd("Start_Stop");

                sscMsg.getCmd().getStartStop().setStartOrStop(sse);

                IPCWrapper sscCmd = new IPCWrapper("ccsi.cmd.Start_Stop", sscMsg);

                theContext.getSensorOutQueue().insertLast(sscCmd);
                theContext.getTheSavedState().setSensingStopped((sse == StartStopEnum.END));

                if (theContext.getTheSavedState().getConnList().getItem(recvdConnId).getConnType()
                        == NSDSConnectionTypeEnum.PERSISTENT_CONNECTION)
                {
                    ChannelRegistrationList regs
                            = theContext.getTheSavedState().getConnList().
                                    getItem(recvdConnId).getRegisteredChannels();

                    if (regs.hasChannel(CcsiChannelEnum.READGS))
                    {
                        if (sse == StartStopEnum.END)
                        {
                            // Remove all pending entries and cancel the timer if there is one
                            theContext.getPendingReadings().getAllEntries();

                            long timerId = regs.getRegistration(CcsiChannelEnum.READGS).getPeriodTimer();

                            if (timerId >= 0L)
                            {
                                theContext.getTimerEventHandler().cancelTimer(timerId);
                            }
                        } else
                        {
                            ChannelRegistrationType rReg = regs.getRegistration(CcsiChannelEnum.READGS);

                            if (rReg.getRegType() == CcsiChannelRegistrationEnum.PERIOD)
                            {
                                long timerId = theContext.getTimerEventHandler().
                                        startNewTimer(rReg.getPeriod(), true, rReg,
                                        "PERIOD", TimerPurposeEnum.PERIODIC_TIMER);

                                rReg.setPeriodTimer(timerId);
                            }
                        }
                    }
                }
                break;
        }
        return retValue;

    }

    /**
     * Executes a Reboot command
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @param argList
     * @return 
     */
    public boolean executeReboot(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn,
            CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
                 sendAck(h, receivedConnectionID, receivedMsn);
                 reboot ();
                 break;
           case passtosensor:
                sendAck(h, receivedConnectionID, receivedMsn);
                 
                ObjectFactory objFact2 = new ObjectFactory();

                MsgContent sscMsg = objFact2.createMsgContent();
                sscMsg.setCmd(objFact2.createCommandMsg());
                RebootArgs rebootArgs = new RebootArgs ();
                if (argList.theList.size() > 0)
                {
                    for (NameValuePair arg : argList.theList)
                    {
                        switch (arg.getTheName())
                        {
                            case "Delay":
                                rebootArgs.setDelay
                                    (Integer.valueOf(arg.getTheValue()));
                                break;
                            case "Mode":
                                rebootArgs.setMode(CcsiCommandedModeEnum.
                                        valueOf(arg.getTheValue()));
                                break;
                            default:
                                myLogger.error ("Unknown name: " + arg.getTheName());
                                break;
                        }
                    }
                }
                sscMsg.getCmd().setReboot(rebootArgs);
                        
                sscMsg.getCmd().setCmd("Reboot");
                IPCWrapper sscCmd = new IPCWrapper("ccsi.cmd.Reboot", sscMsg);

                theContext.getSensorOutQueue().insertLast(sscCmd);
                break;
               
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    /**
     * Executes a Set_Comm_Permission command
     *
     * This command is not supported by any known sensor and will always return
     * false
     *
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @return
     */
    public boolean executeSet_Comm_Permission(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;

    }

    /**
     * Executes a Set_Local_Alert_Mode command
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param argList
     * @return 
     */
    public boolean executeSet_Local_Alert_Mode(CommandHandlingType h,
            long recvdConnId, long recvdMsn, CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
            case passtosensor:
                sendAck(h, recvdConnId, recvdMsn);

                ObjectFactory objFact2 = new ObjectFactory();

                MsgContent sscMsg = objFact2.createMsgContent();
                sscMsg.setCmd(objFact2.createCommandMsg());
                LocalAlertModeArgs alertArgs = objFact2.createLocalAlertModeArgs();
                alertArgs.setEnable(Boolean.valueOf(argList.theList.get(0).getTheValue()));
                sscMsg.getCmd().setSetLocalAlertMode(alertArgs);
                        
                sscMsg.getCmd().setCmd("Set_Local_Alert_Mode");
                IPCWrapper sscCmd = new IPCWrapper("ccsi.cmd.Set_Local_Alert_Mode", sscMsg);

                theContext.getSensorOutQueue().insertLast(sscCmd);
                break;
        }
        return retValue;

    }

    /**
     * Executes a Start_Self_Test command
     *
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param regList
     * @param sstArgs
     * @param commandHandler
     * @return true = OK
     */
    public boolean executeStart_Self_Test(CommandHandlingType h,
            long recvdConnId, long recvdMsn, ChannelRegistrationList regList,
            CcsiStartSelfTestArgs sstArgs,
            CcsiCommandHandler commandHandler)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, 
                        "Unsupported Command: Start_Self_Test");
                break;
            case passtosensor:
                sendAck(h, recvdConnId, recvdMsn);

                MsgContent sensorMsg;
                ObjectFactory objFact = new ObjectFactory();

                StartSelfTestArgs sstList = objFact.createStartSelfTestArgs();
                String msg;
                boolean noSensorCmd = true;
                CcsiChannelRegistrationEnum statusReg = null;

                if (regList.hasChannel(CcsiChannelEnum.STATUS))
                {
                    statusReg = regList.getRegistration(CcsiChannelEnum.STATUS).getRegType();
                }

                for (NameValuePair x : sstArgs.getArgs())
                {
                    if (!x.getTheName().contentEquals("ALL"))
                    {
                        StartSelfTestComponentEnum ce = StartSelfTestComponentEnum.fromValue(x.getTheName());

                        if (ce == StartSelfTestComponentEnum.CC)
                        {
                            theContext.getTheSavedState().setComponentCCBitStatus(CcsiBitStatusEnum.PASS);
                            theContext.getTheSavedState().setComponentCCBitTime(new Date());
                            theContext.getTheSavedState().setComponentCCStatus(CcsiComponentStateEnum.N);
                            msg = theContext.getReportGenerator().genOneLRUBitReport(StartSelfTestComponentEnum.CC);

                            PendingSendEntry pse = new PendingSendEntry();

                            pse.setMessage(msg);
                            pse.addChannel(CcsiChannelEnum.MAINT);
                            pse.setConnectionId(recvdConnId);
                            pse.setAckRequired(h.getAckRequired());
                            pse.setSendReport(true);
                            theContext.getPendingSends().addPendingSend(pse);

                            if ((statusReg != null) && (statusReg == CcsiChannelRegistrationEnum.EVENT))
                            {
                                theContext.getPendingStatusList().addPendingStatus(new PendingStatusEntry("LRU"));
                                theContext.getPendingEventGen().addPendingEvent(
                                        new PendingEventGeneration(CcsiChannelEnum.STATUS));
                            } else if ((statusReg != null) && (statusReg == CcsiChannelRegistrationEnum.PERIOD))
                            {
                                theContext.getPendingStatusList().addPendingStatus(new PendingStatusEntry("LRU"));
                            }
                        } else
                        {
                            noSensorCmd = false;
                            sstList.getComponentAndLevel().add(ce);
                            sstList.getComponentAndLevel().add(x.getTheValue());
                        }
                    } else
                    {
                        theContext.getTheSavedState().setComponentCCBitStatus(CcsiBitStatusEnum.PASS);
                        theContext.getTheSavedState().setComponentCCBitTime(new Date());
                        theContext.getTheSavedState().setComponentCCStatus(CcsiComponentStateEnum.N);
                        msg = theContext.getReportGenerator().genOneLRUBitReport(StartSelfTestComponentEnum.CC);

                        PendingSendEntry pse = new PendingSendEntry();

                        pse.setMessage(msg);
                        pse.addChannel(CcsiChannelEnum.MAINT);
                        pse.setConnectionId(recvdConnId);
                        pse.setAckRequired(h.getAckRequired());
                        pse.setSendReport(true);
                        theContext.getPendingSends().addPendingSend(pse);
                        noSensorCmd = false;

                        if ((statusReg != null) && (statusReg == CcsiChannelRegistrationEnum.EVENT))
                        {
                            theContext.getPendingStatusList().addPendingStatus(new PendingStatusEntry("LRU"));
                            theContext.getPendingEventGen().addPendingEvent(
                                    new PendingEventGeneration(CcsiChannelEnum.STATUS));
                        } else if ((statusReg != null) && (statusReg == CcsiChannelRegistrationEnum.PERIOD))
                        {
                            theContext.getPendingStatusList().addPendingStatus(new PendingStatusEntry("LRU"));
                        }

                        sstList.getComponentAndLevel().add(StartSelfTestComponentEnum.PDIL);
                        sstList.getComponentAndLevel().add(x.getTheValue());
                        sstList.getComponentAndLevel().add(StartSelfTestComponentEnum.SC);
                        sstList.getComponentAndLevel().add(x.getTheValue());
                    }
                }

                if (!noSensorCmd)
                {
                    sensorMsg = objFact.createMsgContent();
                    sensorMsg.setCmd(objFact.createCommandMsg());
                    sensorMsg.getCmd().setStartSelfTest(sstList);
                    sensorMsg.getCmd().setCmd("Start_Self_Test");
                    sensorMsg.getCmd().getStartSelfTest();

                    IPCWrapper newCmd = new IPCWrapper("ccsi.cmd.Start_Self_Test", sensorMsg);

                    theContext.getSensorOutQueue().insertLast(newCmd);
                }
                break;
        }
        return retValue;

    }

    /**
     * Executes a Set_Security_Timeout command
     *
     * This command is not supported by any known sensor and will always return
     * false
     *
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @return
     */
    public boolean executeSet_Security_Timeout(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    /**
     * Executes a Clear_Alert command
     *
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @param argList
     * @return true = OK
     */
    public boolean executeClear_Alert(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn,
            CommandArgumentList argList)
    {
        boolean retValue = false;
        NameValuePair x;
        switch (h.getHandle())
        {
            case internal:
                x = argList.theList.get(0);

                if (x.getTheValue().contentEquals("ALL"))
                {
                    theContext.getActiveAlerts().clear();
                } else
                {
                    theContext.getActiveAlerts().removeActiveAlert(x.getTheValue());
                }

                sendAck(h, receivedConnectionID, receivedMsn);

                break;
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
            case passtosensor:
                sendAck(h, receivedConnectionID, receivedMsn);

                if ((argList != null) && (argList.theList.size() == 1))
                {
                    x = argList.theList.get(0);
/*                  Don't clear the list here, let the sensor send a DEWARN or DEALERT   

                    if (x.getTheValue().contentEquals("ALL"))
                    {
                        theContext.getActiveAlerts().clear();
                    } else
                    {
                        theContext.getActiveAlerts().removeActiveAlert(x.getTheValue());
                    }
*/
                    MsgContent sensorMsg;
                    ObjectFactory objFact = new ObjectFactory();

                    sensorMsg = objFact.createMsgContent();
                    sensorMsg.setCmd(objFact.createCommandMsg());
                    sensorMsg.getCmd().setClearAlert(objFact.createClearAlertArgs());
                    sensorMsg.getCmd().getClearAlert().setAlertId(x.getTheValue());
                    sensorMsg.getCmd().setCmd("Clear_Alert");

                    IPCWrapper newCmd = new IPCWrapper("ccsi.cmd.Clear_Alert", sensorMsg);

                    theContext.getSensorOutQueue().insertLast(newCmd);
                }
                break;
        }
        return retValue;
    }

    /**
     * Executes a Silence command
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @param argList
     * @return 
     */
    public boolean executeSilence(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn,
            CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case passtosensor:
                sendAck(h, receivedConnectionID, receivedMsn);
                
                ObjectFactory objFact = new ObjectFactory();
                MsgContent sensorMsg = objFact.createMsgContent();
                SilenceArgs silenceArgs = new SilenceArgs ();
                for (NameValuePair pair : argList.theList)
                {
                    switch (pair.getTheName())
                    {
                        case "Type":
                            silenceArgs.setType(SilenceType.
                                    valueOf(pair.getTheValue().toUpperCase()));
                            break;
                        case "Annun":
                            silenceArgs.setAnnun(AnnunciatorType.
                                    valueOf(pair.getTheValue()));
                            break;
                        default:
                            myLogger.error ("Invalid name: " +pair.getTheName());
                            break;
                    }
                }
                sensorMsg.setCmd(objFact.createCommandMsg());
                sensorMsg.getCmd().setSilence(silenceArgs);
                sensorMsg.getCmd().setCmd("Silence");

                IPCWrapper newCmd = new IPCWrapper("ccsi.cmd.Silence", sensorMsg);

                theContext.getSensorOutQueue().insertLast(newCmd);
                break;
            case internal:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    /**
     * Executes a Set_Heartbeat command
     *
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param setHeartbeatArg
     * @return true = OK
     */
    public boolean executeSet_Heartbeat(CommandHandlingType h,
            long recvdConnId, long recvdMsn, NameValuePair setHeartbeatArg)
    {
        NSDSConnection conn = theContext.getTheSavedState().getConnList().getItem(recvdConnId);
        boolean retVal = false;
        switch (h.getHandle())
        {
            case internal:
                boolean sendNak = false;
                NakDetailCodeEnum nakDetail = null;

                if ((setHeartbeatArg != null) && (setHeartbeatArg.getTheValue() != null))
                {
                    String rateVal = setHeartbeatArg.getTheValue().trim();
                    int rate = Integer.parseInt(rateVal);

                    if (conn != null)
                    {
                        ChannelRegistrationList regs = conn.getRegisteredChannels();

                        if ((regs != null) && (regs.hasChannel(CcsiChannelEnum.HRTBT)))
                        {
                            for (ChannelRegistrationType item : regs.getList())
                            {
                                if (item.getMsgChannel() == CcsiChannelEnum.HRTBT)
                                {
                                    if (item.getPeriodTimer() >= 0L)
                                    {
                                        if (rate == 0)
                                        {
                                            theContext.getTimerEventHandler().cancelHeartbeatTimer();
                                        } else
                                        {
                                            if (!theContext.getTimerEventHandler().
                                                    setNewTimePeriod(item.getPeriodTimer(), rate))
                                            {
                                                sendNak = true;
                                                nakDetail = NakDetailCodeEnum.COMMAND_FAILED;
                                            }
                                        }
                                    } else
                                    {
                                        long newId = theContext.getTimerEventHandler().
                                                startNewTimer(rate - 2, true, "HRTBT",
                                                CcsiChannelRegistrationEnum.PERIOD.name(), 
                                                TimerPurposeEnum.HEARTBEAT_TIMER);

                                        item.setPeriodTimer(newId);
                                    }
                                }    // If registered for heartbeats
                            }      // end for channels
                        } // and if registered for heartbeat
                        else
                        {
                            sendNak = true;
                            nakDetail = NakDetailCodeEnum.INVALID_STATE;
                        }
                    }          // end if connection is not null
                } // end if the right args
                else
                {
                    sendNak = true;
                    nakDetail = NakDetailCodeEnum.INVALID_ARGUMENT;
                }

                if (sendNak)
                {
                    sendNack(recvdConnId, recvdMsn, nakDetail, null);
                } else
                {
                    sendAck(h, recvdConnId, recvdMsn);
                }
                break;
                
            case passtosensor:
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retVal;
    }

    /**
     * Executes a Set_Location command
     *
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param argList
     * @return true = OK
     */
    public boolean executeSet_Location(CommandHandlingType h,
            long recvdConnId, long recvdMsn,
            CommandArgumentList argList)
    {
        boolean retValue = true;

        double latitude = 0.0;
        double longitude = 0.0;
        int altitude = 0;

        LocationSourceType locSrcEnum = LocationSourceType.INTERNAL;
        CcsiLocationSourceEnum ccsiLocSrcEnum = CcsiLocationSourceEnum.internal;
        
        try
        {
            for (NameValuePair arg : argList.theList)
            {
                switch (arg.getTheName())
                {
                    case "Latitude":
                        latitude = Double.parseDouble(arg.getTheValue());
                        break;
                    case "Longitude":
                        longitude = Double.parseDouble(arg.getTheValue());
                        break;
                    case "Altitude":
                        altitude = Integer.parseInt(arg.getTheValue());
                        break;
                    case "Source":
                        // converting both
                        locSrcEnum = LocationSourceType.valueOf(arg.getTheValue().toUpperCase());
                        ccsiLocSrcEnum
                                = CcsiLocationSourceEnum.valueOf(arg.getTheValue());
                        break;
                    default:
                        retValue = false;
                        sendNack(recvdConnId, recvdMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT,
                                "Set_Location: Invalid argument: " + arg.getTheName());
                        myLogger.error("Set_Location: Invalid argument" + arg.getTheName());
                        break;
                }
            }
            myLogger.info ("Set_Location altitude: " + altitude + " Long: " + longitude + " Lat: " 
                    + latitude + " Altitude: " + altitude + " Source: " + locSrcEnum);
        }
        catch (NumberFormatException ex) {
            myLogger.error ("Exception parsing Set_Location:" + ex);
        }
        if (retValue == true)
        {

            switch (h.getHandle())
            {
                case passtosensor:
                    ObjectFactory objFact2 = new ObjectFactory();
                    MsgContent cmdMsg = objFact2.createMsgContent();
                    cmdMsg.setCmd(objFact2.createCommandMsg());
                    cmdMsg.getCmd().setCmd("Set_Location");

                    SetLocArgs setLocArgs = new SetLocArgs();
                    setLocArgs.setAlt(altitude);
                    setLocArgs.setLat(latitude);
                    setLocArgs.setLon(longitude);
                    setLocArgs.setSource(locSrcEnum);

                    cmdMsg.getCmd().setSetLocation(setLocArgs);

                    if (retValue == true)
                    {
                        theContext.getTheSavedState().setSensorAlt(altitude);
                        theContext.getTheSavedState().setSensorLon(longitude);
                        theContext.getTheSavedState().setSensorLat(latitude);
                        theContext.getTheSavedState().setLocationsource(ccsiLocSrcEnum);
                        IPCWrapper setTimeDateCmd
                                = new IPCWrapper("ccsi.cmd.Set_Location", cmdMsg);
                        theContext.getSensorOutQueue().insertLast(setTimeDateCmd);
                        try {
                            theContext.getTheSavedState().writeSavedState();
                        } catch (Exception ex) {
                            myLogger.error("Error writing to saved state", ex);
                        }
                    }
                    break;
                case internal:
                    theContext.getTheSavedState().setSensorAlt(altitude);
                    theContext.getTheSavedState().setSensorLon(longitude);
                    theContext.getTheSavedState().setSensorLat(latitude);
                    theContext.getTheSavedState().setLocationsource(ccsiLocSrcEnum);
                    try {
                        theContext.getTheSavedState().writeSavedState();
                    } catch (Exception ex) {
                        myLogger.error("Error writing to saved state", ex);
                    }
                    break;
                    
                case unsupported:
                    sendNack(recvdConnId, recvdMsn,
                            NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                    retValue = false;
                    break;
            }
        }

        if (retValue == true)
        {
            sendAck(h, recvdConnId, recvdMsn);
        }
        return retValue;
    }

    public boolean executeSet_Data_Compression(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    /**
     * Executes a Set_Local_Enable command
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @param argList
     * @return 
     */
    public boolean executeSet_Local_Enable(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn,
            CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case passtosensor:
                sendAck(h, receivedConnectionID, receivedMsn);
                
                ObjectFactory objFact = new ObjectFactory();
                MsgContent sensorMsg = objFact.createMsgContent();
                SetLocEnableArgs args = objFact.createSetLocEnableArgs();
                List<SetLocEnableArg> locEnableArgList = args.getArg();
                SetLocEnableArg locEnableArg = objFact.createSetLocEnableArg ();
                for (NameValuePair pair : argList.theList)
                {
                    if (pair.getTheName().equals("Function"))
                    {
                        locEnableArg.setFunction(FunctionNameType.
                                valueOf(pair.getTheValue()));
                    } else if (pair.getTheName().equals("EnableOrDisable"))
                    {
                        locEnableArg.setEnableDisable(EnableOrDisable.
                                valueOf(pair.getTheValue().toUpperCase()));
                        locEnableArgList.add(locEnableArg);
                        locEnableArg = objFact.createSetLocEnableArg ();
                    }
                }
                sensorMsg.setCmd(objFact.createCommandMsg());
                sensorMsg.getCmd().setSetLocalEnable(args);
                sensorMsg.getCmd().setCmd("Set_Local_Enable");

                IPCWrapper newCmd = new IPCWrapper("ccsi.cmd.Set_Local_Enable", sensorMsg);

                theContext.getSensorOutQueue().insertLast(newCmd);
                break;
                
            case internal:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    /**
     * Executes a Set_BW_Mode command
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @param argList
     * @return 
     */
    public boolean executeSet_Bw_Mode(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn,
            CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
                sendAck(h, receivedConnectionID, receivedMsn);

                for (NameValuePair pair : argList.theList)
                {
                    if (pair.getTheName().equals("Mode"))
                    {
                        boolean linkBWHigh = false;
                        if (pair.getTheValue().equalsIgnoreCase("high"))
                        {
                            linkBWHigh = true;
                        }
                        theContext.getTheSavedState().setStateLinkBWHigh(linkBWHigh);
                    }
                }
                break;
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    /**
     * Executes a Get_Status command
     *
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param argList
     * @return true = OK
     */
    public boolean executeGet_Status(CommandHandlingType h,
            long recvdConnId, long recvdMsn, CommandArgumentList argList)
    {
        boolean retVal = false;
        NSDSConnection conn = theContext.getTheSavedState().getConnList().getItem(recvdConnId);

        switch (h.getHandle())
        {
            case internal:
                boolean nakGsc = false;
                NakReasonCodeEnum nakGscReason = null;
                NakDetailCodeEnum nakGscDetail = null;

                String gscReport = null;
                PendingSendEntry gscPse = null;
                
                if ((argList != null) && (argList.theList.size() > 1))
                {
                    PendingStatusList psl = new PendingStatusList ();
                    for (NameValuePair arg : argList.theList)
                    {
                        if (arg.getTheName().equalsIgnoreCase("Item"))
                        {
                            psl.addPendingStatus(new PendingStatusEntry (arg.getTheValue()));
                        }
                    }
                    gscReport = theContext.getReportGenerator().genEventStatusReport(null,
                                        conn.getConnectionId(), theContext.getPendingNakDetails(),
                                        psl);
                } else if ((argList != null) && (argList.theList.size() > 0))
                {
                    NameValuePair arg = argList.theList.get(0);

                    if (arg.getTheName().trim().equalsIgnoreCase("Item"))
                    {
                        switch (arg.getTheValue())
                        {
                            case "ALL":
                                gscReport = theContext.getReportGenerator()
                                        .genFullStatusReport(theContext.getPendingNakDetails(),
                                                recvdConnId, null, theContext.getPendingStatusList());
                                break;

                            case "LRU":
                                gscReport = theContext.getReportGenerator().genEventStatusReport(arg.getTheValue(),
                                        conn.getConnectionId(), theContext.getPendingNakDetails(),
                                        theContext.getPendingStatusList());
                                break;

                            case "State":
                                gscReport = theContext.getReportGenerator().genEventStatusReport(arg.getTheValue(),
                                        conn.getConnectionId(), theContext.getPendingNakDetails(),
                                        theContext.getPendingStatusList());
                                break;

                            case "Link":
                                gscReport = theContext.getReportGenerator().genEventStatusReport(arg.getTheValue(),
                                        conn.getConnectionId(), theContext.getPendingNakDetails(),
                                        theContext.getPendingStatusList());
                                break;

                            case "Connect":
                                gscReport = theContext.getReportGenerator().genEventStatusReport(arg.getTheValue(),
                                        conn.getConnectionId(), theContext.getPendingNakDetails(),
                                        theContext.getPendingStatusList());
                                break;

                            case "Power":
                                gscReport = theContext.getReportGenerator().genEventStatusReport(arg.getTheValue(),
                                        conn.getConnectionId(), theContext.getPendingNakDetails(),
                                        theContext.getPendingStatusList());
                                break;

                            case "NAK":
                                gscReport = theContext.getReportGenerator().genEventStatusReport(arg.getTheValue(),
                                        conn.getConnectionId(), theContext.getPendingNakDetails(),
                                        theContext.getPendingStatusList());
                                break;

                            default:
                                nakGsc = true;
                                nakGscReason = NakReasonCodeEnum.MSGCON;
                                nakGscDetail = NakDetailCodeEnum.INVALID_ARGUMENT;
                                break;
                        }    // end switch
                    } // end if arg is "Item"
                    else
                    {
                        nakGsc = true;
                        nakGscReason = NakReasonCodeEnum.MSGCON;
                        nakGscDetail = NakDetailCodeEnum.INVALID_ARGUMENT;
                    }
                } else
                {
                    nakGsc = true;
                    nakGscDetail = NakDetailCodeEnum.MISSING_ARGUMENT;
                }

                if (nakGsc)
                {
                    sendNack(recvdConnId, recvdMsn, nakGscDetail, null);
                    retVal = false;
                } else
                {
                    gscPse = new PendingSendEntry();
                    gscPse.setConnectionId(recvdConnId);
                    gscPse.addChannel(CcsiChannelEnum.STATUS);
                    gscPse.setMessage(gscReport);
                    gscPse.setSendReport(true);
                    retVal = true;
                }

                theContext.getPendingSends().addPendingSend(gscPse);
                break;
            case passtosensor:
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retVal;
    }

    /**
     * Executes a Get_Configuration command
     *
     * @param h
     * @param recvdConnId
     * @param receivedMsn
     * @param getCfgArgList
     * @return true = OK
     */
    public boolean executeGet_Configuration(CommandHandlingType h,
            long recvdConnId, long receivedMsn,
            CcsiGetConfigArgumentList getCfgArgList)
    {
        boolean retValue = false;

        switch (h.getHandle())
        {
            case internal:
                getConfigInternal (h, recvdConnId, receivedMsn, getCfgArgList);
                break;
                
            case passtosensor:
                getConfigInternal (h, recvdConnId, receivedMsn, getCfgArgList);
                getConfigPassToSensor (h, recvdConnId, receivedMsn, getCfgArgList);
                break;
                
            case unsupported:
                sendNack(recvdConnId, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    public boolean executeInstall_Start(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    public boolean executeInstall(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    public boolean executeInstall_Complete(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    /**
     * Executes a Set_Ccsi_Mode command
     *
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param argList
     * @return true = OK
     */
    public boolean executeSet_Ccsi_Mode(CommandHandlingType h,
            long recvdConnId, long recvdMsn, CommandArgumentList argList)
    {
        boolean nakScm = false;
        NakDetailCodeEnum scmDetailCode = null;

        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
                if ((argList != null) && (argList.theList.size() == 1))
                {
                    NameValuePair x = argList.theList.get(0);

                    try
                    {
                        CcsiModeEnum mode = CcsiModeEnum.value(x.getTheValue());

                        theContext.getTheSavedState().setMode(mode);
                    } catch (IllegalArgumentException modeEx)
                    {
                        nakScm = true;
                        scmDetailCode = NakDetailCodeEnum.INVALID_ARGUMENT;
                    }
                } else
                {
                    nakScm = true;
                    scmDetailCode = NakDetailCodeEnum.INSUFFICIENT_ARGUMENTS;
                }

                if (nakScm)
                {
                    sendNack(recvdConnId, recvdMsn, scmDetailCode, null);
                } else
                {
                    sendAck(h, recvdConnId, recvdMsn);
                }
                break;
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
            case passtosensor:
                if ((argList != null) && (argList.theList.size() == 1))
                {
                    NameValuePair x = argList.theList.get(0);

                    try
                    {
                        CcsiModeEnum mode = CcsiModeEnum.value(x.getTheValue());

                        theContext.getTheSavedState().setMode(mode);
                    } catch (IllegalArgumentException modeEx)
                    {
                        nakScm = true;
                        scmDetailCode = NakDetailCodeEnum.UNSUPPORTED_SET_ITEM;
                    }

                    if (!nakScm)
                    {
                        CcsiCommandedModeEnum cme = CcsiCommandedModeEnum.valueOf(x.getTheValue());
                        ObjectFactory objFact = new ObjectFactory();

                        MsgContent sensorMsg = objFact.createMsgContent();
                        sensorMsg.setCmd(objFact.createCommandMsg());
                        sensorMsg.getCmd().setSetCcsiMode(objFact.createSetCcsiModeArgs());
                        sensorMsg.getCmd().getSetCcsiMode().setMode(cme);
                        sensorMsg.getCmd().setCmd("Set_Ccsi_Mode");

                        IPCWrapper newCmd = new IPCWrapper("ccsi.cmd.Set_Ccsi_Mode", sensorMsg);

                        theContext.getSensorOutQueue().insertLast(newCmd);
                    }
                } else
                {
                    nakScm = true;
                    scmDetailCode = NakDetailCodeEnum.INSUFFICIENT_ARGUMENTS;
                }

                if (nakScm)
                {
                    sendNack(recvdConnId, recvdMsn, scmDetailCode, null);
                } else
                {
                    sendAck(h, recvdConnId, recvdMsn);
                }
                break;
        }
        return retValue;
    }

    /**
     * Executes a User_Change command
     *
     * This command is not supported by any known sensor and will always return
     * false
     *
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @return
     */
    public boolean executeUser_Change(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    /**
     * Executes a Get_Users command
     *
     * This command is not supported by any known sensor and will always return
     * false
     *
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @return
     */
    public boolean executeGet_Users(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
            case passtosensor:
            case unsupported:
                sendNack(receivedConnectionID, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    /**
     * Executes a Register command
     *
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param work
     * @param regList
     * @param workList
     * @return true = OK
     */
    public boolean executeRegister(CommandHandlingType h,
            long recvdConnId, long recvdMsn, ChannelRegistrationList work,
            ChannelRegistrationList regList, ChannelRegistrationList workList)
    {
        NSDSConnection conn = theContext.getTheSavedState().getConnList().
                getItem(recvdConnId);
        boolean retVal = false;
        switch (h.getHandle())
        {
            case internal:
                if ((work != null) && (!work.isEmpty()))
                {
                    StringBuilder regReport = new StringBuilder();
                    ArrayList<CcsiChannelEnum> channelWork = new ArrayList<>();
                    ChannelRegistrationList result = new ChannelRegistrationList();

                    // First process the work list against the current list
                    for (ChannelRegistrationType x : work.getList())
                    {
                        if (!regList.hasChannel(x.getMsgChannel()))
                        {
                            // Work channel is not in curent
                            result.addChannelRegistration(x);
                        } else      // work channel is in current
                        {
                            ChannelRegistrationType temp = regList.getRegistration(x.getMsgChannel());

                            if (x.getRegType() == temp.getRegType())
                            {
                                if (x.getRegType() == CcsiChannelRegistrationEnum.PERIOD)
                                {
                                    theContext.getTimerEventHandler().cancelTimer(temp.getPeriodTimer());
                                    temp.setPeriodTimer(-1L);
                                    result.addChannelRegistration(x);
                                } else
                                {
                                    result.addChannelRegistration(x);
                                }
                            } else    // They're not the same kind of registration
                            {
                                if (x.getRegType() == CcsiChannelRegistrationEnum.CANCEL)
                                {
                                    if (temp.getRegType() == CcsiChannelRegistrationEnum.PERIOD)
                                    {
                                        // Kill the current timer
                                        theContext.getTimerEventHandler().cancelTimer(temp.getPeriodTimer());
                                        temp.setPeriodTimer(-1L);
                                    }
                                } else if (temp.getRegType() == CcsiChannelRegistrationEnum.PERIOD)
                                {
                                    // Kill the current timer and add the new registration
                                    theContext.getTimerEventHandler().cancelTimer(temp.getPeriodTimer());
                                    temp.setPeriodTimer(-1L);
                                    result.addChannelRegistration(x);
                                } else
                                {
                                    result.addChannelRegistration(x);
                                }
                            }
                        }    // end if current has work list channel
                    }      // end or processing the work list
                    
                    for (ChannelRegistrationType temp : regList.getList()) {
                        if (!workList.hasChannel(temp.getMsgChannel()))                       {
                            // Work list doesn't have the current channel put it into the result
                            result.addChannelRegistration(temp);
                        }
                    }

                    if (!result.isEmpty())
                    {
                        // For each channel in the result produce a full report except for alerts and heartbeat
                        channelWork.add(CcsiChannelEnum.CMD);

                        for (ChannelRegistrationType x : result.getList())
                        {
                            if (!(x.getRegType() == CcsiChannelRegistrationEnum.CANCEL))
                            {
                                // Not a cancelled channel, get a full report
                                if (x.getMsgChannel() != CcsiChannelEnum.ALERTS)
                                {
                                    channelWork.add(x.getMsgChannel());
                                }

                                if (x.getMsgChannel() != CcsiChannelEnum.HRTBT)
                                {
                                    switch (x.getMsgChannel())
                                    {
                                        case IDENT:
                                            String tempI = theContext.getReportGenerator().genIdentReport();

                                            regReport.append(tempI);
                                            break;

                                        case CONFIG:
                                            String tempC = theContext.getReportGenerator().genFullConfigReport();

                                            regReport.append(tempC);
                                            break;

                                        case STATUS:
                                            String tempS = theContext.getReportGenerator().genFullStatusReport(
                                                    theContext.getPendingNakDetails(), recvdConnId, null,
                                                    theContext.getPendingStatusList());

                                            regReport.append(tempS);
                                            break;

                                        case READGS:
                                            String tempR = theContext.
                                                    getReportGenerator().
                                                    genReadingReport(null);

                                            regReport.append(tempR);
                                            break;

                                        case MAINT:
                                            String tempM = theContext.
                                                    getReportGenerator().
                                                    genFullMaintReport(this.theContext.getPendingBits(),
                                                        this.theContext.getPendingConsumables(), 
                                                        this.theContext.getPendingFailures());

                                            regReport.append(tempM);
                                            break;

                                        case ALERTS:
                                            myLogger.debug("Found alerts.");
                                            break;

                                        case TMDT:
                                            String tempDT = theContext.
                                                    getReportGenerator().genTDReport();
                                            regReport.append(tempDT);
                                            break;
                                            
                                        case LOC:
                                            String tempLoc = theContext.
                                                    getReportGenerator().genLocReport();
                                            regReport.append(tempLoc);
                                            break;
                                            
                                        case SEC:
                                            String tempSec = theContext.
                                                    getReportGenerator().genSecurityConnectReport();
                                            regReport.append(tempSec);
                                            break;
                                            
                                        default:
                                        {
                                            StringBuilder temp = new StringBuilder("<Msg><");

                                            temp.append(x.getMsgChannel().
                                                    getChanTag()).append("></Msg>");
                                            regReport.append(temp.toString());

                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        if (regReport.length() > 0)
                        {
                            PendingSendEntry pse = new PendingSendEntry();

                            pse.setConnectionId(recvdConnId);
                            pse.setMessage(regReport.toString());
                            pse.setAckType(CcsiAckNakEnum.ACK);
                            pse.setAckMsn(recvdMsn);
                            pse.setSendReport(true);
                            for (CcsiChannelEnum ce : channelWork)
                            {
                                pse.addChannel(ce);
                            }

                            retVal = theContext.getPendingSends().addPendingSend(pse);
                        } else
                        {
                            sendAck(h, recvdConnId, recvdMsn);
                        }

                        regList.clearList(theContext.getTheSavedState().getTimerBase());

                        for (ChannelRegistrationType x : result.getList())
                        {
                            if ((x.getRegType() != CcsiChannelRegistrationEnum.CANCEL)
                                    && (x.getRegType() != CcsiChannelRegistrationEnum.ONCE))
                            {
                                if (x.getRegType() == CcsiChannelRegistrationEnum.PERIOD)
                                {
                                    // Subtract two seconds for our safety in responding in 
                                    // case we have to reconnect, are busy, etc.
                                    int timerPeriod = x.getPeriod() - 2;
                                    long timerId = -1L;

                                    if (x.getPeriodTimer() == -1L)
                                    {
                                        if (x.getMsgChannel() == CcsiChannelEnum.HRTBT)
                                        {
                                            timerId = theContext.getTimerEventHandler().
                                                    startNewTimer(timerPeriod, true, x,
                                                    x.getRegType().name(), TimerPurposeEnum.HEARTBEAT_TIMER);
                                            x.setPeriodTimer(timerId);
                                        } else
                                        {
                                            timerId = theContext.getTimerEventHandler().
                                                    startNewTimer(timerPeriod, true, x,
                                                    x.getRegType().name(), TimerPurposeEnum.PERIODIC_TIMER);
                                            x.setPeriodTimer(timerId);
                                        }
                                    }

                                    regList.addChannelRegistration(x);
                                } else
                                {
                                    x.setPeriodTimer(-1L);
                                    regList.addChannelRegistration(x);
                                }
                            }

                            theContext.getTheOpenInterfaceHost().setPersistent(conn.getConnectionId());
                        }

                        if (work.hasChannel(CcsiChannelEnum.ALERTS))
                        {
                            if (work.getRegistration(CcsiChannelEnum.ALERTS).
                                    getRegType() != CcsiChannelRegistrationEnum.CANCEL)
                            {
                                String altRpt
                                        = theContext.getReportGenerator().
                                                genFullAlertReport(theContext.getPendingAlerts());
                                PendingSendEntry aPse = new PendingSendEntry();

                                aPse.addChannel(CcsiChannelEnum.ALERTS);
                                aPse.setMessage(altRpt);
                                aPse.setSendReport(retVal);
                                aPse.setConnectionId(recvdConnId);
                                aPse.setAckRequired(h.getAckRequired());
                                retVal = theContext.getPendingSends().addPendingSend(aPse);
                            }
                        }
                    } else
                    {
                        sendNack(recvdConnId, recvdMsn,
                                NakDetailCodeEnum.COMMAND_FAILED,
                                "Only unsupported channels in the command");
                    }
                } else
                {
                    myLogger.error("No argments passed to register command.");
                }
                break;
            case passtosensor:
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retVal;
    }

    /**
     * Executes a Deregister command
     *
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @return true = OK
     */
    public boolean executeDeregister(CommandHandlingType h,
            long recvdConnId, long recvdMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
                NSDSConnection myConn = theContext.getTheSavedState().getConnList().getItem(recvdConnId);

                if (myConn != null)
                {
                    myConn.getRegisteredChannels().clearList(theContext.getTheSavedState().getTimerBase());
                    theContext.getPendingEventGen().clear();
                    theContext.getPendingTimedGen().clear();
                    theContext.getPendingSends().clearList();
                    theContext.getTheOpenInterfaceHost().setTransient(recvdConnId);

                    sendAck(h, recvdConnId, recvdMsn);

                    PendingSendEntry drcdPse = new PendingSendEntry();
                    drcdPse.addChannel(CcsiChannelEnum.CMD);
                    drcdPse.setSendDisconnect(true);
                    drcdPse.setConnectionId(recvdConnId);
                    drcdPse.setAckRequired(h.getAckRequired());
                    theContext.getPendingSends().addPendingSend(drcdPse);
                }
                break;
            case passtosensor:
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    /**
     * Executes a Set_Config command
     *
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param setCfgList
     * @return true = OK
     */
    public boolean executeSet_Config(CommandHandlingType h,
            long recvdConnId, long recvdMsn, CcsiSetConfigArgumentList setCfgList)
    {
        boolean retVal = false;
        switch (h.getHandle())
        {
            case internal:
                retVal = setConfigInternal(h, recvdConnId, recvdMsn, setCfgList);
                // if a NACK it was already sent
                if (retVal) {
                    sendAck(h, recvdConnId, recvdMsn);
                }
                break;

            case passtosensor:
                retVal = setConfigInternal(h, recvdConnId, recvdMsn, setCfgList);
                // if a NACK it was already sent
                if (retVal != false)
                {
                    retVal = setConfigPassToSensor(h, recvdConnId, recvdMsn, setCfgList);
                    // if a NACK it was already sent
                    if (retVal)
                    {
                        sendAck(h, recvdConnId, recvdMsn);
                    }
                }
                break;
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retVal;
    }

    /**
     * Executes a Get_Identification command
     *
     * @param h
     * @param recvdConnId
     * @param receivedMsn
     * @return true = OK
     */
    public boolean executeGet_Identification(CommandHandlingType h,
            long recvdConnId, long receivedMsn)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
                String identMsg = theContext.getReportGenerator().genIdentReport();
                PendingSendEntry send = new PendingSendEntry();

                send.addChannel(CcsiChannelEnum.IDENT);
                send.setMessage(identMsg);
                send.setAckRequired(h.getAckRequired());
                send.setConnectionId(recvdConnId);
                send.setSendReport(true);
                retValue = theContext.getPendingSends().addPendingSend(send);
                myLogger.info ("Processed Get_Identification command: ACK: " + h.getAckRequired());
                break;
            case passtosensor:
            case unsupported:
                sendNack(recvdConnId, receivedMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                break;
        }
        return retValue;
    }

    /**
     * Executes a Set_Date_Time command
     *
     * @param h
     * @param receivedConnectionID
     * @param receivedMsn
     * @param argList
     * @return true = OK
     */
    public boolean executeSet_Date_Time(CommandHandlingType h,
            long receivedConnectionID, long receivedMsn,
            CommandArgumentList argList)
    {
        boolean retValue = true;

        String dtString = null;
        TimeSourceEnum tsEnum = TimeSourceEnum.INTERNAL;

        for (NameValuePair arg : argList.theList)
        {
            switch (arg.getTheName())
            {
                case "Date_Time":
                    dtString = arg.getTheValue();
                    break;
                case "Source":
                    tsEnum = TimeSourceEnum.valueOf(arg.getTheValue().toUpperCase());
                    break;
                default:
                    sendNack(receivedConnectionID, receivedMsn,
                            NakDetailCodeEnum.INVALID_ARGUMENT, "Unknown Arguement");
                    myLogger.error("Set_Date_Time: Unknown Arguement");
                    retValue = false;
                    break;
            }
        }

        if (retValue == true)
        {
            switch (h.getHandle())
            {
                case internal:
                {
                    try
                    {
                        setSystemDateTime(dtString);
                    } catch (ParseException | IOException ex)
                    {
                        sendNack(receivedConnectionID, receivedMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT, null);
                        myLogger.error("Error converting Date_Time");
                        retValue = false;
                    }
                }
                break;
                case passtosensor:
                    ObjectFactory objFact2 = new ObjectFactory();
                    MsgContent cmdMsg = objFact2.createMsgContent();
                    cmdMsg.setCmd(objFact2.createCommandMsg());
                    cmdMsg.getCmd().setSetDateTime(objFact2.createSetDTArgs());
                    cmdMsg.getCmd().setCmd("Set_Date_Time");

                    try
                    {
                        setSystemDateTime(dtString);

                        cmdMsg.getCmd().getSetDateTime().setDateTime(dtString);
                    } catch (ParseException | IOException ex)
                    {
                        sendNack(receivedConnectionID, receivedMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT, null);
                        myLogger.error("Error converting Date_Time");
                        retValue = false;
                    }
                    try
                    {
                        setSystemDateTime(dtString);
                    } catch (IOException | ParseException ex)
                    {
                        sendNack(receivedConnectionID, receivedMsn,
                                NakDetailCodeEnum.INVALID_ARGUMENT, null);
                        myLogger.error("Error setting Date_Time");
                        retValue = false;
                    }
                    cmdMsg.getCmd().getSetDateTime().setSource(tsEnum);

                    if (retValue == true)
                    {
                        IPCWrapper setTimeDateCmd = new IPCWrapper("ccsi.cmd.Set_Date_Time", cmdMsg);
                        theContext.getSensorOutQueue().insertLast(setTimeDateCmd);
                    }

                    break;
                case unsupported:
                    sendNack(receivedConnectionID, receivedMsn,
                            NakDetailCodeEnum.UNSUPPORTED_COMMAND, "Unsupported Command");
                    retValue = false;
                    break;
            }
        }
        if (retValue == true)
        {
            sendAck(h, receivedConnectionID, receivedMsn);
        }

        return retValue;
    }

    /**
     * Executes a Get_Alert_Details command
     *
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param argList
     * @return true = OK
     */
    public boolean executeGet_Alert_Details(CommandHandlingType h,
            long recvdConnId, long recvdMsn,
            CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
                PendingSendEntry gadPse = new PendingSendEntry();
                String msgBody;

                if ((argList != null) && (argList.theList.size() == 1))
                {
                    NameValuePair x = argList.theList.get(0);
                    ActiveAlertListEntry entry
                            = theContext.getActiveAlerts().getAlert(x.getTheValue());

                    if (entry != null)
                    {
                        if (entry.getEntry().getMsgBody().isSetBIT())
                        {
                            msgBody = theContext.getReportGenerator().genSingleBitAlertReport(
                                    entry.getEntry().getMsgBody().getBIT().get(0));

                            if (msgBody != null)
                            {
                                gadPse.setConnectionId(recvdConnId);
                                gadPse.setSendReport(true);
                                gadPse.setMessage(msgBody);
                                gadPse.addChannel(CcsiChannelEnum.ALERTS);
                                gadPse.setAckRequired(h.getAckRequired());
                                theContext.getPendingSends().addPendingSend(gadPse);
                            }
                        } else
                        {
                            msgBody = theContext.getReportGenerator().genSingleReadingAlertReport(
                                    entry); //.getEntry().getMsgBody().getReading());

                            if (msgBody != null)
                            {
                                gadPse.setConnectionId(recvdConnId);
                                gadPse.setSendReport(true);
                                gadPse.setMessage(msgBody);
                                gadPse.addChannel(CcsiChannelEnum.ALERTS);
                                gadPse.setAckRequired(h.getAckRequired());
                                theContext.getPendingSends().addPendingSend(gadPse);
                            }
                        }
                    } else
                    {
                        msgBody = theContext.getReportGenerator().genFullAlertReport(null);
                        gadPse.setConnectionId(recvdConnId);
                        gadPse.setSendReport(true);
                        gadPse.setMessage(msgBody);
                        gadPse.addChannel(CcsiChannelEnum.ALERTS);
                        gadPse.setAckRequired(h.getAckRequired());
                        theContext.getPendingSends().addPendingSend(gadPse);
                    }
                } else
                {
                    sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.MISSING_ARGUMENT, null);
                }
                break;
            case passtosensor:
                NameValuePair pair = argList.theList.get(0);

                String alertID = pair.getTheValue();
                ObjectFactory objFact = new ObjectFactory();

                MsgContent sensorMsg = objFact.createMsgContent();
                sensorMsg.setCmd(objFact.createCommandMsg());
                GetAlertArg alertArg = objFact.createGetAlertArg();
                alertArg.setId(alertID);
                sensorMsg.getCmd().setGetAlertDetails(alertArg);
                sensorMsg.getCmd().setCmd("Get_Alert_Details");

                IPCWrapper newCmd = new IPCWrapper("ccsi.cmd.Get_Alert_Details", sensorMsg);

                theContext.getSensorOutQueue().insertLast(newCmd);

                sendAck(h, recvdConnId, recvdMsn);

                break;
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, 
                        "Unsupported Command: Get_Alert_Details");
                break;
        }
        return retValue;
    }

    /**
     * Executes a Get_Readings_Details command
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param argList
     * @return 
     */
    public boolean executeGet_Reading_Details(CommandHandlingType h,
            long recvdConnId, long recvdMsn,
            CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case internal:
                PendingSendEntry gadPse = new PendingSendEntry();
                String msgBody;

                if ((argList != null) && (argList.theList.size() == 1))
                {
                    NameValuePair x = argList.theList.get(0);
                    ActiveAlertListEntry entry
                            = theContext.getActiveAlerts().getAlert(x.getTheValue());

                    if (entry != null)
                    {
                        if (entry.getEntry().getMsgBody().isSetBIT())
                        {
                            msgBody = theContext.getReportGenerator().genSingleBitAlertReport(
                                    entry.getEntry().getMsgBody().getBIT().get(0));

                            if (msgBody != null)
                            {
                                gadPse.setConnectionId(recvdConnId);
                                gadPse.setSendReport(true);
                                gadPse.setMessage(msgBody);
                                gadPse.addChannel(CcsiChannelEnum.READGS);
                                gadPse.setAckRequired(h.getAckRequired());
                                theContext.getPendingSends().addPendingSend(gadPse);
                            }
                        } else
                        {
                            msgBody = theContext.getReportGenerator().genSingleReadingAlertReport(
                                    entry);
//                                    entry.getEntry().getMsgBody().getReading());

                            if (msgBody != null)
                            {
                                gadPse.setConnectionId(recvdConnId);
                                gadPse.setSendReport(true);
                                gadPse.setMessage(msgBody);
                                gadPse.addChannel(CcsiChannelEnum.READGS);
                                gadPse.setAckRequired(h.getAckRequired());
                                theContext.getPendingSends().addPendingSend(gadPse);
                            }
                        }
                    } else
                    {
                        msgBody = theContext.getReportGenerator().genFullAlertReport(null);
                        gadPse.setConnectionId(recvdConnId);
                        gadPse.setSendReport(true);
                        gadPse.setMessage(msgBody);
                        gadPse.addChannel(CcsiChannelEnum.ALERTS);
                        gadPse.setAckRequired(h.getAckRequired());
                        theContext.getPendingSends().addPendingSend(gadPse);
                    }
                } else
                {
                    sendNack(recvdConnId, recvdMsn, NakDetailCodeEnum.MISSING_ARGUMENT, null);
                }
                break;
            case passtosensor:
                NameValuePair pair = argList.theList.get(0);

                String alertID = pair.getTheValue();
                ObjectFactory objFact = new ObjectFactory();

                MsgContent sensorMsg = objFact.createMsgContent();
                sensorMsg.setCmd(objFact.createCommandMsg());
                GetReadingArg readingArg = objFact.createGetReadingArg();
                readingArg.setId(alertID);
                sensorMsg.getCmd().setGetReadingDetails(readingArg);
                sensorMsg.getCmd().setCmd("Get_Reading_Details");

                IPCWrapper newCmd = new IPCWrapper("ccsi.cmd.Get_Reading_Details", sensorMsg);

                theContext.getSensorOutQueue().insertLast(newCmd);

                sendAck(h, recvdConnId, recvdMsn);

                break;
            case unsupported:
                sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, 
                        "Unsupported Command: Get_Reading_Details");
                break;
        }
        return retValue;
    }

    /**
     * Executes a Sensor Unique command
     * 
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param argList
     * @return
     */
    public boolean executeSensorUniqueCommand (CommandHandlingType h,
            long recvdConnId, long recvdMsn,
            CommandArgumentList argList)
    {
        boolean retValue = false;
        switch (h.getHandle())
        {
            case passtosensor:
                ObjectFactory objFact = new ObjectFactory();

                MsgContent sensorMsg = objFact.createMsgContent();
                sensorMsg.setCmd(objFact.createCommandMsg());
                sensorMsg.getCmd().setCmd(h.getCommand());

                SensorUniqueArgList sensorUniqueArgList = objFact.createSensorUniqueArgList();
                if (argList.theList.size() > 0)
                {
                    for (NameValuePair arg : argList.theList)
                    {
                        SensorUniqueArg uniqueArg = new SensorUniqueArg ();
                        uniqueArg.setArg(arg.getTheName());
                        uniqueArg.setValue(arg.getTheValue());
                        sensorUniqueArgList.getArg().add(uniqueArg);
                    }
                }
                sensorMsg.getCmd().setSensorUnique(sensorUniqueArgList);
                
                IPCWrapper newCmd = new IPCWrapper("ccsi.cmd.SensorUniqueCommand", sensorMsg);

                theContext.getSensorOutQueue().insertLast(newCmd);

                sendAck(h, recvdConnId, recvdMsn);
                break;
                
            case internal:
            case unsupported:
                        sendNack(recvdConnId, recvdMsn,
                        NakDetailCodeEnum.UNSUPPORTED_COMMAND, 
                        "Unsupported Sensor Unique Command: " + h.getCommand());
                break;
}
        return retValue;
    }
    /**
     * Sets the system time on the OS On non-Unix systems, does nothing
     *
     * @param dateString - format yyyyMMddHHmmss
     * @throws IOException
     * @throws ParseException
     */
    private void setSystemDateTime(String dateString) throws IOException, ParseException
    {
        SystemDateTime systemDateTime = new SystemDateTime ();
        systemDateTime.setSystemDateTime(dateString);
    }

    /**
     * Sets the internal configuration
     * @param h
     * @param recvdConnId
     * @param recvdMsn
     * @param setCfgList
     * @return 
     */
    private boolean setConfigInternal(CommandHandlingType h,
            long recvdConnId, long recvdMsn, CcsiSetConfigArgumentList setCfgList)
    {
        boolean retVal;
        boolean nakScc = false;
        boolean updateCfg = false;
        boolean updateState = false;
        NakDetailCodeEnum sccDetailCode = null;
        String sccDetailText = null;
        ArrayList<CcsiConfigBlockEnum> blocks = new ArrayList<>();

        ArrayList<CcsiSetConfigArgument> argList = setCfgList.getArgList();
        for (CcsiSetConfigArgument configArg : argList)
        {
            switch (configArg.getItemName())
            {
                // BASE block
                case "EncryptionType":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        theContext.getTheConfig().setEncryptType(configArg.getItemValue());
                    } else
                    {
                        theContext.getTheConfig().setEncryptType(" ");
                    }
                    blocks.add(CcsiConfigBlockEnum.BASE);
                    updateCfg = true;
                    break;
                // COMM block
                case "HeartbeatRate":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        theContext.getTheSavedState().setHeartbeat(Integer.parseInt(configArg.getItemValue()));
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.COMM);
                    }
                    break;

                case "HostNetworkType":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        NetworkProtocolTypeEnum net
                                = NetworkProtocolTypeEnum.valueOf(configArg.getItemValue());
                        theContext.getTheConfig().getHostNetworkSettings().setNetworkProtocolType(net);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.COMM);
                    }
                    break;
                case "OpenInterfacePort":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        int oiPort = Integer.parseInt(configArg.getItemValue());

                        theContext.getTheConfig().getHostNetworkSettings().setPort(oiPort);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.COMM);
                    }
                    break;

                case "OpenInterfaceServer":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        boolean oiServer = Boolean.parseBoolean(configArg.getItemValue());

                        theContext.getTheConfig().getHostNetworkSettings().setOIServer(oiServer);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.COMM);
                    }

                    break;

                case "AckTimeout":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        int ackTimeout = Integer.parseInt(configArg.getItemValue());

                        theContext.getTheConfig().setAckTimeout(ackTimeout);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.COMM);
                    }

                    break;

                case "ConnectionLimit":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        int connLimit = Integer.parseInt(configArg.getItemValue());
                        theContext.getTheConfig().setConnLimit(connLimit);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.COMM);
                    }
                    break;

                case "ConnRetryTimeout":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        int crT = Integer.parseInt(configArg.getItemValue());

                        theContext.getTheConfig().getHostNetworkSettings().setConnRetryTime(crT);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.COMM);
                    }
                    break;

                case "TimeSource":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        theContext.getTheSavedState().
                                setTimesource(CcsiTimeSourceEnum.
                                        valueOf(configArg.getItemValue()));
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.COMM);
                    }
                    break;

                case "LocationSource":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        theContext.getTheSavedState().
                                setLocationsource(CcsiLocationSourceEnum.
                                        valueOf(configArg.getItemValue()));
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.COMM);
                    }
                    break;

                case "SelectedTimeSource":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        theContext.getTheSavedState().
                                setSelTimesource(CcsiTimeSourceEnum.
                                        valueOf(configArg.getItemValue()));
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.COMM);
                    }
                    break;

                case "SelectedLocationSource":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        theContext.getTheSavedState().
                                setSelLocationsource(CcsiLocationSourceEnum.
                                        valueOf(configArg.getItemValue()));
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.COMM);
                    }
                    break;

                case "UnitMode":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        CcsiModeEnum mode
                                = CcsiModeEnum.value(configArg.getItemValue());
                        theContext.getTheSavedState().setMode(mode);
                        updateState = true;
                        blocks.add(CcsiConfigBlockEnum.GENERAL);
                    }
                    break;

                case "LocationReportOption":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        theContext.getTheSavedState().
                                setLocationreportoption(CcsiLocationReportOptionEnum.
                                        valueOf(configArg.getItemValue()));
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.COMM);
                    }
                    break;

                case "DismountedPowerWarningLevel":
                case "DismountedPowerShutdownLevel":
                    // unsupported 
                    nakScc = true;
                    sccDetailCode = NakDetailCodeEnum.UNSUPPORTED_SET_ITEM;
                    sccDetailText = "Unsupported Set_Config item: "
                            + configArg.getItemName();
                    break;

                case "HostSensorId":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        theContext.getTheSavedState().setHostSensorID(configArg.getItemValue());
                    } else
                    {
                        theContext.getTheSavedState().setHostSensorID(" ");
                    }

                    updateState = true;
                    blocks.add(CcsiConfigBlockEnum.LOCAL);
                    break;

                case "HostSensorName":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        theContext.getTheSavedState().setHostSensorName(configArg.getItemValue());
                    } else
                    {
                        theContext.getTheSavedState().setHostSensorName(" ");
                    }

                    updateState = true;
                    blocks.add(CcsiConfigBlockEnum.LOCAL);
                    break;

                case "HostSensorMount":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        CcsiSensorMountingEnum sme
                                = CcsiSensorMountingEnum.valueOf(configArg.getItemValue());

                        theContext.getTheSavedState().setHostSensorMounting(sme);
                    } else
                    {
                        theContext.getTheSavedState().
                                setHostSensorMounting(CcsiSensorMountingEnum.DISMTD);
                    }

                    updateState = true;
                    blocks.add(CcsiConfigBlockEnum.LOCAL);
                    break;

                case "HostSensorDescription":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        theContext.getTheSavedState().
                                setHostSensorDescription(configArg.getItemValue());
                    } else
                    {
                        theContext.getTheSavedState().setHostSensorDescription(" ");
                    }

                    updateState = true;
                    blocks.add(CcsiConfigBlockEnum.LOCAL);
                    break;

                case "ComponentType":
                case "Present":
                case "HardwareVersion":
                case "SoftwareVersion":
                case "BitPass":
                    // Unsupported Set_Config item
                    nakScc = true;
                    sccDetailCode = NakDetailCodeEnum.UNSUPPORTED_SET_ITEM;
                    sccDetailText = "Unsupported Set_Config item: "
                            + configArg.getItemName() + " Can't set this item";
                    break;

                case "SerialNumber":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        theContext.getTheConfig().setSerial(configArg.getItemValue());
                    }

                    updateCfg = true;
                    blocks.add(CcsiConfigBlockEnum.SENSDESC);
                    break;
                case "AccessRole":
                case "AnnunciationType":
                    // Unsupported Set_Config item
                    nakScc = true;
                    sccDetailCode = NakDetailCodeEnum.UNSUPPORTED_SET_ITEM;
                    sccDetailText = "Unsupported Set_Config item: "
                            + configArg.getItemName() + " Can't set this item";
                    break;
                case "LinkName":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        CcsiLinkNameType linkName
                                = new CcsiLinkNameType(configArg.getItemValue());
                        theContext.getTheSavedState().setStateLinkName(linkName);
                    }
                    updateState = true;
                    blocks.add(CcsiConfigBlockEnum.LINKS);
                    break;

                case "LinkNetType":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        CcsiLinkTypeEnum netType
                                = CcsiLinkTypeEnum.valueOf(configArg.getItemValue());
                        theContext.getTheSavedState().setStateLinkType(netType);
                    }
                    updateState = true;
                    blocks.add(CcsiConfigBlockEnum.LINKS);
                    break;

                case "LinkBandwidthHigh":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        boolean high = Boolean.valueOf(configArg.getItemValue());
                        theContext.getTheSavedState().setStateLinkBWHigh(high);
                    }
                    updateState = true;
                    blocks.add(CcsiConfigBlockEnum.LINKS);
                    break;

                case "LinkEncryption":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        boolean encrypt = Boolean.valueOf(configArg.getItemValue());
                        theContext.getTheSavedState().setStateLinkEncrypt(encrypt);
                    }
                    updateState = true;
                    blocks.add(CcsiConfigBlockEnum.LINKS);
                    break;

                case "LinkCompression":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        boolean compress = Boolean.valueOf(configArg.getItemValue());
                        theContext.getTheSavedState().setStateLinkCompress(compress);
                    }
                    updateState = true;
                    blocks.add(CcsiConfigBlockEnum.LINKS);
                    break;

                case "LinkCompressionThreshold":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        int threshold = Integer.parseInt(configArg.getItemValue());
                        theContext.getTheSavedState().setCompressThreshold(threshold);
                    }
                    updateState = true;
                    blocks.add(CcsiConfigBlockEnum.LINKS);
                    break;

                case "ModifyRole":
                    // unsupported Set_Config item
                    nakScc = true;
                    sccDetailCode = NakDetailCodeEnum.UNSUPPORTED_SET_ITEM;
                    sccDetailText = "Unsupported Set_Config item: "
                            + configArg.getItemName();
                    break;

                case "IpAddress":
                    // Note: there is a host ip and port in both state and config
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        try
                        {
                            InetAddress sensorIp
                                    = InetAddress.getByName(configArg.getItemValue());

                            theContext.getTheConfig().getHostNetworkSettings().setIP(sensorIp);
                            theContext.getTheSavedState().setHostIp(sensorIp);
                            updateCfg = true;
                            blocks.add(CcsiConfigBlockEnum.LINKS);
                        } catch (UnknownHostException ipEx)
                        {
                            nakScc = true;
                            sccDetailCode = NakDetailCodeEnum.INVALID_DATA;
                            sccDetailText = "IP address ["
                                    + configArg.getItemValue() + "] is not valid.";
                        }
                    }
                    break;

                case "NetMask":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        try
                        {
                            InetAddress mask
                                    = InetAddress.getByName(configArg.getItemValue());

                            theContext.getTheConfig().getHostNetworkSettings().setNetmask(mask);
                            updateCfg = true;
                            blocks.add(CcsiConfigBlockEnum.LINKS);
                        } catch (UnknownHostException ipEx)
                        {
                            nakScc = true;
                            sccDetailCode = NakDetailCodeEnum.INVALID_DATA;
                            sccDetailText = "Net Mask ["
                                    + configArg.getItemValue() + "] is not valid.";
                        }
                    }
                    break;

                case "GatewayAddress":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        try
                        {
                            InetAddress mask
                                    = InetAddress.getByName(configArg.getItemValue());

                            theContext.getTheConfig().getHostNetworkSettings().setGatewayAddr(mask);
                            updateCfg = true;
                            blocks.add(CcsiConfigBlockEnum.LINKS);
                        } catch (UnknownHostException ipEx)
                        {
                            nakScc = true;
                            sccDetailCode = NakDetailCodeEnum.INVALID_DATA;
                            sccDetailText = "IP address ["
                                    + configArg.getItemValue() + "] is not valid.";
                        }
                    }
                    break;

                case "IpAddressFixed":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        theContext.getTheConfig().setIPAddressFixed(Boolean.
                                valueOf(configArg.getItemValue()));
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.LINKS);
                    }
                    break;

                case "MaxTransmitSize":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        int mts = Integer.parseInt(configArg.getItemValue());

                        theContext.getTheConfig().getHostNetworkSettings().setMaxTransmit(mts);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.LINKS);
                    }
                    break;

                case "LinkPortNbr":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        int lct = Integer.parseInt(configArg.getItemValue());

                        theContext.getTheConfig().getHostNetworkSettings().setPort(lct);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.LINKS);
                    }
                    break;

                case "BroadcastIp":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        try
                        {
                            InetAddress bdcst
                                    = InetAddress.getByName(configArg.getItemValue());

                            theContext.getTheConfig().getHostNetworkSettings().setBroadcastAddr(bdcst);
                            updateCfg = true;
                            blocks.add(CcsiConfigBlockEnum.LINKS);
                        } catch (UnknownHostException bcEx)
                        {
                            nakScc = true;
                            sccDetailCode = NakDetailCodeEnum.INVALID_DATA;
                            sccDetailText = "Broadcast IP ["
                                    + configArg.getItemValue() + "] is invalid.";
                        }
                    }
                    break;

                case "EmconMode":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        CcsiEmconModeEnum emcon = CcsiEmconModeEnum.valueOf(configArg.getItemValue());
                        theContext.getTheSavedState().setEmcon(emcon);
                    }
                    updateState = true;
                    blocks.add(CcsiConfigBlockEnum.LINKS);
                    break;

                case "LinkSpeed":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        int linkSpd = Integer.parseInt(configArg.getItemValue());

                        theContext.getTheConfig().setIpLinkSpeed(linkSpd);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.LINKS);
                    }
                    break;

                // ANNUN block
                case "AudibleAnnunState":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        AnnunciatorStateEnum annunState = AnnunciatorStateEnum.valueOf(configArg.getItemValue());

                        theContext.getTheConfig().setAudibleAnnunState(annunState);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.LINKS);
                    }
                    break;

                case "VisualAnnunState":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        AnnunciatorStateEnum annunState = AnnunciatorStateEnum.valueOf(configArg.getItemValue());

                        theContext.getTheConfig().setVisualAnnunState(annunState);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.LINKS);
                    }
                    break;

                case "TactileAnnunState":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        AnnunciatorStateEnum annunState = AnnunciatorStateEnum.valueOf(configArg.getItemValue());

                        theContext.getTheConfig().setTactileAnnunState(annunState);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.LINKS);
                    }
                    break;

                case "LedAnnunState":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        AnnunciatorStateEnum annunState = AnnunciatorStateEnum.valueOf(configArg.getItemValue());

                        theContext.getTheConfig().setLedAnnunState(annunState);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.LINKS);
                    }
                    break;

                case "ExternalAnnunState":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        AnnunciatorStateEnum annunState = AnnunciatorStateEnum.valueOf(configArg.getItemValue());

                        theContext.getTheConfig().setExternalAnnunState(annunState);
                        updateCfg = true;
                        blocks.add(CcsiConfigBlockEnum.LINKS);
                    }
                    break;

                // UNIQUE block - handled under default case
                // OPERATORS block
                case "OperatorName":
                case "OperatorOperatorRoles":
                case "OperatorLastLogin":
                case "OperatorFailureCount":
                case "OperatorLastPwChange":
                    // unsupported Set_Config item
                    // unsupported Set_Config item
                    nakScc = true;
                    sccDetailCode = NakDetailCodeEnum.UNSUPPORTED_SET_ITEM;
                    sccDetailText = "Unsupported Set_Config item: "
                            + configArg.getItemName();
                    break;

                // SECURITY block
                case "EncryptComm":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        boolean encrypt = Boolean.valueOf(configArg.getItemValue());
                        theContext.getTheConfig().getHostNetworkSettings().setEncryptComm(encrypt);
                    }
                    updateCfg = true;
                    blocks.add(CcsiConfigBlockEnum.SECURITY);
                    break;

                case "EncryptData":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        boolean encrypt = Boolean.valueOf(configArg.getItemValue());
                        theContext.getTheConfig().setEncryptData(encrypt);
                    }
                    updateCfg = true;
                    blocks.add(CcsiConfigBlockEnum.SECURITY);
                    break;

                case "HostAuthentication":
                case "OperatorAuthentication":
                case "UserPwdTimeout":
                case "PwReuseLength":
                case "LoginFailTimeout":
                    // unsupported Set_Config item
                    nakScc = true;
                    sccDetailCode = NakDetailCodeEnum.UNSUPPORTED_SET_ITEM;
                    sccDetailText = "Unsupported Set_Config item: "
                            + configArg.getItemName();
                    break;

                case "ConnectFailTimeout":
                    if ((configArg.getItemValue() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        int timeout = Integer.parseInt(configArg.getItemValue());
                        theContext.getTheConfig().setConnFailureTimeout(timeout);
                    }
                    updateCfg = true;
                    blocks.add(CcsiConfigBlockEnum.SECURITY);
                    break;

                case "Count":
                case "Window":
                case "Action":
                case "Enable":
                    // can't set this item
                    nakScc = true;
                    sccDetailCode = NakDetailCodeEnum.INVALID_DATA;
                    sccDetailText = "Set_Config item: "
                            + configArg.getItemName() + " cannot be set";
                    break;

                default:
                    // handles sensor unique commands
                    if ((configArg.getItemBlock() != null)
                            && (!configArg.getItemValue().trim().isEmpty()))
                    {
                        SensorUniqueConfigItem theConfigItem
                                = theContext.getTheConfig().
                                        getSensorUniqueConfig(configArg.getItemName());
                        SensorUniqueDataItem theDataItem
                                = theContext.getTheConfig().
                                        getSensorUniqueData(configArg.getItemName());
                        if ((theConfigItem == null) && (theDataItem == null))
                        {
                            // this one is invalid
                            nakScc = true;
                            sccDetailCode = NakDetailCodeEnum.INVALID_DATA;
                            sccDetailText = "Invalid name for sensor unique item: "
                                    + configArg.getItemName();
                            break;
                        }
                        if (theConfigItem != null)
                        {
                            theConfigItem.setValue(configArg.getItemValue());
                        }

                        if (theDataItem != null)
                        {
                            theDataItem.setValue(configArg.getItemValue());
                        }
                        blocks.add(CcsiConfigBlockEnum.UNIQUE);
                    } else
                    {
                        // this one is invalid
                        nakScc = true;
                        sccDetailCode = NakDetailCodeEnum.INVALID_DATA;
                        sccDetailText = "Invalid item block for sensor unique item: "
                                + configArg.getItemBlock();
                    }
            }
        }
        if (nakScc)
        {
            retVal = false;
            sendNack(recvdConnId, recvdMsn, sccDetailCode, sccDetailText);
        } else
        {
            retVal = true;
            try
            {
                if (updateCfg)
                {
                    theContext.getTheConfig().writeSensorConfig();
                }

                if (updateState)
                {
                    theContext.getTheSavedState().writeSavedState();
                }
            } catch (Exception ex)
            {
                myLogger.error("Error writing config or saved state.", ex);
                retVal = false;
            }
        }

        if (retVal == true)
        {
//            sendAck(h, recvdConnId, recvdMsn);

            if (!blocks.isEmpty())
            {
                boolean baseSet = false;
                boolean genSet = false;
                boolean linkSet = false;
                boolean commSet = false;
                boolean locSet = false;
                boolean sensDescSet = false;
                boolean annunSet = false;
                boolean uniqueSet = false;
                boolean operSet = false;
                boolean secSet = false;

                for (CcsiConfigBlockEnum x : blocks)
                {
                    switch (x)
                    {
                        case BASE:
                            if (!baseSet)
                            {
                                theContext.getPendingConfigList().addConfigBlock(x);
                                baseSet = true;
                            }
                            break;

                        case COMM:
                            if (!commSet)
                            {
                                theContext.getPendingConfigList().addConfigBlock(x);
                                commSet = true;
                            }
                            break;

                        case GENERAL:
                            if (!genSet)
                            {
                                theContext.getPendingConfigList().addConfigBlock(x);
                                genSet = true;
                            }
                            break;

                        case LOCAL:
                            if (!locSet)
                            {
                                theContext.getPendingConfigList().addConfigBlock(x);
                                locSet = true;
                            }
                            break;

                        case SENSDESC:
                            if (!sensDescSet)
                            {
                                theContext.getPendingConfigList().addConfigBlock(x);
                                sensDescSet = true;
                            }
                            break;

                        case LINKS:
                            if (!linkSet)
                            {
                                theContext.getPendingConfigList().addConfigBlock(x);
                                linkSet = true;
                            }
                            break;

                        case ANNUN:
                            if (!annunSet)
                            {
                                theContext.getPendingConfigList().addConfigBlock(x);
                                annunSet = true;
                            }
                            break;

                        case UNIQUE:
                            if (!uniqueSet)
                            {
                                theContext.getPendingConfigList().addConfigBlock(x);
                                uniqueSet = true;
                            }
                            break;

                        case OPERATORS:
                            if (!operSet)
                            {
                                theContext.getPendingConfigList().addConfigBlock(x);
                                operSet = true;
                            }
                            break;

                        case SECURITY:
                            if (!secSet)
                            {
                                theContext.getPendingConfigList().addConfigBlock(x);
                                secSet = true;
                            }
                            break;

                        case TAMPER:
                            // no settable items
                            break;

                        default:
                            break;
                    }  // end switch
                }  // end for each block set
            }  // end if we set blocks
            if ((theContext.getTheSavedState().getConnList().getItem(recvdConnId)
                    .getRegisteredChannels().hasChannel(CcsiChannelEnum.CONFIG)))
            {
                ChannelRegistrationType scRegs = theContext.getTheSavedState().getConnList()
                        .getItem(recvdConnId).getRegisteredChannels().getRegistration(CcsiChannelEnum.CONFIG);
                if (scRegs.getRegType() == CcsiChannelRegistrationEnum.PERIOD)
                {
                    PendingTimedGeneration ptg = new PendingTimedGeneration(CcsiChannelEnum.CONFIG);
                    theContext.getPendingTimedGen().addPendingTimed(ptg);
                } else
                {
                    PendingEventGeneration peg = new PendingEventGeneration(CcsiChannelEnum.CONFIG);
                    theContext.getPendingEventGen().addPendingEvent(peg);
                }
            }
        }
        return retVal;
    }

    private boolean setConfigPassToSensor(CommandHandlingType h,
            long recvdConnId, long recvdMsn, CcsiSetConfigArgumentList setCfgList)
    {
        ArrayList<CcsiSetConfigArgument> argList = setCfgList.getArgList();
        SetConfigArgs setConfigArgs = new SetConfigArgs ();
        for (CcsiSetConfigArgument configArg : argList)
        {
            // Note: any case commented out is not sent to the sensor
            switch (configArg.getItemName())
            {
                case "EncryptionType":
                case "HeartbeatRate":
                case "HostNetworkType":
                case "OpenInterfacePort":
                case "OpenInterfaceServer":
                case "AckTimeout":
                case "ConnectionLimit":
                case "ConnRetryTimeout":
                    break;
                case "TimeSource":
                case "LocationSource":
                case "SelectedTimeSource":
                case "SelectedLocationSource":
                case "UnitMode":
                case "LocationReportOption":
                    addToSetConfigArgsList (setConfigArgs, configArg);
                    break;
                case "DismountedPowerWarningLevel":
                case "DismountedPowerShutdownLevel":
                    break;
                case "HostSensorId":
                case "HostSensorName":
                case "HostSensorMount":
                case "HostSensorDescription":
//                    addToSetConfigArgsList (setConfigArgs, configArg);
//                    break;
                case "ComponentType":
                case "Present":
                case "HardwareVersion":
                case "SoftwareVersion":
                case "BitPass":
                    break;
                case "SerialNumber":
                    addToSetConfigArgsList (setConfigArgs, configArg);
                    break;
                case "AccessRole":
                case "AnnunciationType":
                case "LinkName":
                case "LinkNetType":
                case "LinkBandwidthHigh":
                case "LinkEncryption":
                case "LinkCompression":
                case "LinkCompressionThreshold":
                // duplicate case - case "AccessRole":
                case "ModifyRole":
                case "IpAddress":
                case "NetMask":
                case "GatewayAddress":
                case "IpAddressFixed":
                case "MaxTransmitSize":
                case "LinkPortNbr":
                case "BroadcastIp":
                case "EmconMode":
                case "LinkSpeed":
                    break;
                case "AudibleAnnunState":
                case "VisualAnnunState":
                case "TactileAnnunState":
                case "LedAnnunState":
                case "ExternalAnnunState":
                    addToSetConfigArgsList (setConfigArgs, configArg);
                    break;
                // handled by default case Unique Name:
                case "OperatorName":
                case "OperatorOperatorRoles":
                case "OperatorLastLogin":
                case "OperatorFailureCount":
                case "OperatorLastPwChange":
                case "EncryptComm":
                case "EncryptData":
                case "HostAuthentication":
                case "OperatorAuthentication":
                case "UserPwdTimeout":
                case "PwReuseLength":
                case "LoginFailTimeout":
                case "ConnectFailTimeout":
                case "Count":
                case "Window":
                case "Action":
                case "Enable":
                    break;
                default:
                    if ((configArg.getItemBlock() != null)
                            && (configArg.getItemBlock() == CcsiConfigBlockEnum.UNIQUE))
                    {
                        addToSetConfigArgsList (setConfigArgs, configArg);
                    }
                    break;
            }
        }
        if (!setConfigArgs.getArg().isEmpty())
        {
            sendToSensorSetConfig (setConfigArgs);
        }
//        sendAck(h, recvdConnId, recvdMsn);

        return true;
    }

    private boolean getConfigInternal (CommandHandlingType h,
            long recvdConnId, long receivedMsn,
            CcsiGetConfigArgumentList getCfgArgList)
    {
        if ((getCfgArgList != null) && (!getCfgArgList.isEmpty())) {
            StringBuilder msg
                    = new StringBuilder("<Msg><").append(CcsiChannelEnum.CONFIG.getChanTag()).append('>');
            ArrayList<CcsiConfigBlockEnum> blocks = new ArrayList<>();
            String theKey = "ALL";

            //
            // Coalesce the blocks so that all gets for a block are a single request
            //
            for (CcsiGetConfigArgument x : getCfgArgList.getArgList()) {
                if (!blocks.contains(x.getItemBlock())) {
                    blocks.add(x.getItemBlock());
                }

                if (x.getItemBlock() == CcsiConfigBlockEnum.SENSDESC) {
                    if ((x.getItemKey() != null) && (!x.getItemKey().trim().isEmpty())) {
                        theKey = x.getItemKey().trim();
                    }
                }
            }

            //
            // Now, process each block and wrap the whole in a single message
            //
            for (CcsiConfigBlockEnum x : blocks) {
                if (x == CcsiConfigBlockEnum.SENSDESC) {
                    msg.append(theContext.getReportGenerator().getPartialConfigReport(x, theKey));
                } else {
                    msg.append(theContext.getReportGenerator().getPartialConfigReport(x, null));
                }
            }

            msg.append("</").append(CcsiChannelEnum.CONFIG.getChanTag()).append("></Msg>");
            PendingSendEntry shcPse = new PendingSendEntry();
            shcPse.addChannel(CcsiChannelEnum.CONFIG);
            shcPse.setConnectionId(recvdConnId);
            shcPse.setSendReport(true);
            shcPse.setMessage(msg.toString());
            shcPse.setAckRequired(h.getAckRequired());
            theContext.getPendingSends().addPendingSend(shcPse);

        }
        return true;
    }
    /**
     * Executes a Get_Config (passtosensor) command
     * @param h
     * @param recvdConnId
     * @param receivedMsn
     * @param getCfgArgList
     * @return 
     */
    private boolean getConfigPassToSensor (CommandHandlingType h,
            long recvdConnId, long receivedMsn,
            CcsiGetConfigArgumentList getCfgArgList)
    {
        ArrayList<CcsiGetConfigArgument> argList = getCfgArgList.getArgList();
        GetConfigArgs getConfigArgs = new GetConfigArgs ();
        for (CcsiGetConfigArgument configArg : argList)
        {
            // Note: any case with just a break is not sent to the sensor
            switch (configArg.getItemName())
            {
                case "EncryptionType":
                case "HeartbeatRate":
                case "HostNetworkType":
                case "OpenInterfacePort":
                case "OpenInterfaceServer":
                case "AckTimeout":
                case "ConnectionLimit":
                case "ConnRetryTimeout":
                    break;
                case "TimeSource":
                case "LocationSource":
                case "SelectedTimeSource":
                case "SelectedLocationSource":
                case "UnitMode":
                case "LocationReportOption":
                    addToGetConfigArgsList (getConfigArgs, configArg);
                    break;
                case "DismountedPowerWarningLevel":
                case "DismountedPowerShutdownLevel":
                    break;
                case "HostSensorId":
                case "HostSensorName":
                case "HostSensorMount":
                case "HostSensorDescription":
                    addToGetConfigArgsList (getConfigArgs, configArg);
                    break;
                case "ComponentType":
                case "Present":
                case "HardwareVersion":
                case "SoftwareVersion":
                case "BitPass":
                    break;
                case "SerialNumber":
                    addToGetConfigArgsList (getConfigArgs, configArg);
                    break;
                case "AccessRole":
                case "AnnunciationType":
                case "LinkName":
                case "LinkNetType":
                case "LinkBandwidthHigh":
                case "LinkEncryption":
                case "LinkCompression":
                case "LinkCompressionThreshold":
                case "ModifyRole":
                case "IpAddress":
                case "NetMask":
                case "GatewayAddress":
                case "IpAddressFixed":
                case "MaxTransmitSize":
                case "LinkPortNbr":
                case "BroadcastIp":
                case "EmconMode":
                case "LinkSpeed":
                    break;
                case "AudibleAnnunState":
                case "VisualAnnunState":
                case "TactileAnnunState":
                case "LedAnnunState":
                case "ExternalAnnunState":
                    addToGetConfigArgsList (getConfigArgs, configArg);
                    break;
                // handled by default case: Unique Name:
                case "OperatorName":
                case "OperatorOperatorRoles":
                case "OperatorLastLogin":
                case "OperatorFailureCount":
                case "OperatorLastPwChange":
                case "EncryptComm":
                case "EncryptData":
                case "HostAuthentication":
                case "OperatorAuthentication":
                case "UserPwdTimeout":
                case "PwReuseLength":
                case "LoginFailTimeout":
                case "ConnectFailTimeout":
                case "Count":
                case "Window":
                case "Action":
                case "Enable":
                    break;
                // default handles sensor unique data
                default:
                    addToGetConfigArgsList (getConfigArgs, configArg);
                    break;
            }
        }
        sendToSensorGetConfig (getConfigArgs);
        sendAck(h, recvdConnId, receivedMsn);
        return true;
    }
    
    /**
     * Deletes all configuration files and reboots (Non-Windows only)
     * 
     * Note: There's no way to actually erase the files
     */
    private void sanitize()
    {
        if (System.getProperty("os.name").startsWith("Windows"))
        {
            myLogger.info("OS == Windows: Not actually deleting files");
        } else
        {
            String baseDir = System.getProperty("ccsi.baseDir");
            String paReader = System.getProperty("ccsi.pa.reader");
            String paWriter = System.getProperty("ccsi.pa.writer");
            String iaReader = System.getProperty("ccsi.ia.reader");
            String iaWriter = System.getProperty("ccsi.ia.writer");
            
            // delete configuration files
            if (!theContext.getTheConfig().deleteSensorConfig())
            {
                myLogger.error("Error deleting Sensor Config");
            }
            if (!theContext.getTheSavedState().deleteSavedState())
            {
                myLogger.error("Error deleting Saved State");
            }
            File config = new File (baseDir + File.separator + paReader);
            if (!config.delete())
            {
                myLogger.error("Error deleting Protocol Adapter reader");
            }
            
            config = new File (baseDir + File.separator + paWriter);
            if (!config.delete())
            {
                myLogger.error("Error deleting Protocol Adapter reader");
            }
            
            config = new File (baseDir + File.separator + iaReader);
            if (!config.delete())
            {
                myLogger.error("Error deleting Interface Adapter reader");
            }
            
            config = new File (baseDir + File.separator + iaWriter);
            if (!config.delete())
            {
                myLogger.error("Error deleting Interface Adapter writer");
            }
            
            reboot ();
        }
    }
    
    /**
     * Reboots Non-windows based instances after 1 minute
     */
    private void reboot ()
    {
        if (System.getProperty("os.name").startsWith("Windows"))
        {
            myLogger.info("OS == Windows: not actually rebooting");
        } else
        {
            Runtime runtime = Runtime.getRuntime();
            try
            {
                myLogger.info ("Received reboot command - will reboot in 1 minute");
                runtime.exec("shutdown -r +1");
            } catch (IOException ex)
            {
                myLogger.error("IOException rebooting" + ex);
            }
        }
    }
    
    /**
     * Adds a configuration item to SetConfigArgs
     * @param setConfigArgs
     * @param configArg 
     */
    private void addToSetConfigArgsList (SetConfigArgs setConfigArgs, 
            CcsiSetConfigArgument configArg)
    {
        SetCfgArg setCfgArg = new SetCfgArg ();
        setCfgArg.setItem(configArg.getItemName());
        setCfgArg.setValue(configArg.getItemValue());
        if (configArg.getItemBlock() != null)
        {
            setCfgArg.setBlock(ConfigItemBlockType.
                    valueOf(configArg.getItemBlock().toString()));
        }
        if (configArg.getItemKey() != null)
        {
            setCfgArg.setKey(configArg.getItemKey());            
        }
        setConfigArgs.getArg().add(setCfgArg);
    }
    
    /**
     * Adds a configuration item to SetConfigArgs
     * @param getConfigArgs
     * @param configArg 
     */
    private void addToGetConfigArgsList (GetConfigArgs getConfigArgs, 
            CcsiGetConfigArgument configArg)
    {
        GetCfgArg getCfgArg = new GetCfgArg ();
        getCfgArg.setItem(configArg.getItemName());
        if (configArg.getItemBlock() != null)
        {
            getCfgArg.setBlock(ConfigItemBlockType.
                    valueOf(configArg.getItemBlock().toString()));            
        }
        if (configArg.getItemKey() != null)
        {
            getCfgArg.setKey(configArg.getItemKey());            
        }
        getConfigArgs.getArg().add(getCfgArg);
    }
    
    /**
     * Builds a Set_Config message and posts it to the sensor queue
     *
     * @param configArgs - configuration arguments
     */
    private void sendToSensorSetConfig(SetConfigArgs configArgs)
    {
        ObjectFactory objFact2 = new ObjectFactory();

        MsgContent sscMsg = objFact2.createMsgContent();
        sscMsg.setCmd(objFact2.createCommandMsg());
        sscMsg.getCmd().setSetConfig(configArgs);
        sscMsg.getCmd().setCmd("Set_Config");

        IPCWrapper newCmd = new IPCWrapper("ccsi.cmd.Set_Config", sscMsg);

        theContext.getSensorOutQueue().insertLast(newCmd);
    }
    
    /**
     * Builds a Get_Config message and posts it to the sensor queue
     *
     * @param configArgs - configuration arguments
     */
    private void sendToSensorGetConfig(GetConfigArgs configArgs)
    {
        ObjectFactory objFact2 = new ObjectFactory();

        MsgContent sscMsg = objFact2.createMsgContent();
        sscMsg.setCmd(objFact2.createCommandMsg());
        sscMsg.getCmd().setGetConfiguration(configArgs);
        sscMsg.getCmd().setCmd("Get_Config");

        IPCWrapper newCmd = new IPCWrapper("ccsi.cmd.Get_Config", sscMsg);

        theContext.getSensorOutQueue().insertLast(newCmd);
    }

    /**
     * Sends an ACK message back to the host if configured to do so for this
     * message
     *
     * @param h - CommandHandlingType
     * @param receivedConnectionID - connection ID
     * @param recvdMsn - message serial number
     */
    private void sendAck(CommandHandlingType h, long receivedConnectionID,
            long recvdMsn)
    {
        if (h.getAckRequired() == true)
        {
            PendingSendEntry grdPse = new PendingSendEntry();
            grdPse.setConnectionId(receivedConnectionID);
            grdPse.setSendAck(true);
            grdPse.setAckType(CcsiAckNakEnum.ACK);
            grdPse.setAckMsn(recvdMsn);
            grdPse.addChannel(CcsiChannelEnum.CMD);
            grdPse.setAckRequired(false);
            theContext.getPendingSends().addPendingSend(grdPse);
        }
    }

    /**
     * Sends a NACK message back to the host
     *
     * @param receivedConnectionID
     * @param recvdMsn
     * @param nakDetailCode
     */
    private void sendNack(long receivedConnectionID, long recvdMsn,
            NakDetailCodeEnum nakDetailCode, String detailText)
    {
        PendingSendEntry grdPse = new PendingSendEntry();
        grdPse.setConnectionId(receivedConnectionID);
        grdPse.setSendNak(true);
        grdPse.setAckType(CcsiAckNakEnum.NACK);
        grdPse.setNakCode(NakReasonCodeEnum.MSGCON);
        grdPse.setNakDetailCode(nakDetailCode);
        grdPse.setAckMsn(recvdMsn);
        grdPse.addChannel(CcsiChannelEnum.CMD);
        if (detailText != null)
        {
            grdPse.setNakDetailMessage(detailText);
        }
        theContext.getPendingSends().addPendingSend(grdPse);
        myLogger.error("Sending Nack to Host " + nakDetailCode.toString() + " " + detailText);
    }
}
