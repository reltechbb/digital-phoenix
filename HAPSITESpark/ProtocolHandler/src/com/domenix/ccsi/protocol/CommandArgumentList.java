/*
 * CommandArgumentList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/07
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.common.ccsi.NameValuePair;
import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;

import org.apache.log4j.Logger;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

//~--- classes ----------------------------------------------------------------

/**
 * This class encapsulates a list of command arguments for a received CCSI or sensor unique command.
 *
 * @author kmiller
 */
public class CommandArgumentList
{
  /** The actual list */
  ArrayList<NameValuePair>	theList	= new ArrayList<>();

  /** Error/Debug logger */
  Logger	myLogger;

  /** The sensor configuration */
  CcsiConfigSensor	theConfig;

  /** The sensor state */
  CcsiSavedState	theState;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance with the configuration and state.
   *
   * @param cfg the sensor configuration
   * @param state the sensor state
   */
  public CommandArgumentList( CcsiConfigSensor cfg , CcsiSavedState state )
  {
    myLogger				= Logger.getLogger( CommandArgumentList.class );
    this.theConfig	= cfg;
    this.theState		= state;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method process the document tree to gather command arguments as a set of
   * NameValuePair instances.
   *
   * @param base the DOM node of the document command
   *
   * @return a list of command argument which may be empty
   */
  public ArrayList<NameValuePair> getCommandArguments( Node base )
  {
    ArrayList<NameValuePair>	argList		= new ArrayList<>();
    NodeList									argNodes	= base.getChildNodes();
    int												argLen		= ( ( argNodes != null )
            ? argNodes.getLength()
            : 0 );

    for ( int i = 0 ; i < argLen ; i++ )
    {
      if ( ( argNodes.item( i ) != null ) && ( argNodes.item( i ).getNodeType() == Node.ELEMENT_NODE ) &&
           ( argNodes.item( i ).getLocalName().equalsIgnoreCase( "Arg" ) ) )
      {
        String		tempName	= null ,
									tempValue	= null;
        NodeList	valNodes	= argNodes.item( i ).getChildNodes();
        int				valLen		= ( ( valNodes != null )
                                ? valNodes.getLength()
                                : 0 );

        for ( int j = 0 ; j < valLen ; j++ )
        {
          if ( ( valNodes.item( j ) != null ) && ( valNodes.item( j ).getNodeType() == Node.ELEMENT_NODE ) )
          {
            if ( valNodes.item( j ).getLocalName().equalsIgnoreCase( "ArgName" ) )
            {
              tempName	= valNodes.item( j ).getTextContent().trim();
            }
            else if ( valNodes.item( j ).getLocalName().equalsIgnoreCase( "ArgValue" ) )
            {
              tempValue	= valNodes.item( j ).getTextContent();
            }
          }
        }

        if ( ( tempName != null ) && ( tempValue != null ) )
        {
          argList.add( new NameValuePair( tempName , tempValue ) );
        }
      }
    }

    this.theList	= argList;

    return ( argList );
  }
}
