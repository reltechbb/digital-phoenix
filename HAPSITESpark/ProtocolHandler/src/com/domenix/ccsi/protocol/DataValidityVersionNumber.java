/*
 * DataValidityVersionNumber.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements the data validity check for a version number composed of from 2 to 4
 * integers of up to 3 digits separated by a period.
 *
 * @author kmiller
 */
public class DataValidityVersionNumber implements DataValidityInterface
{
  /** The validity check type */
  private final DataValidityType	theType	= DataValidityType.VERSIONNBR;

  /** Version number pattern */
  private final Pattern	versionPattern	= Pattern.compile( "([0-9]{1,3})\\.([0-9]{1,3})(\\.[0-9]{1,3}){0,2}" );

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance
   */
  public DataValidityVersionNumber()
  {
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the data validity check type
   *
   * @return DataValidityType
   */
  @Override
  public DataValidityType getTheType()
  {
    return ( this.theType );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Validates the supplied value and returns a valid/invalid indication.
   *
   * @param value String containing the value to be validated
   *
   * @return valid (true) or invalid (false)
   */
  @Override
  public boolean validateValue( String value )
  {
    Matcher	m1	= versionPattern.matcher( value );

    if ( m1.matches() )
    {
      return ( true );
    }

    return ( false );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns a description of a valid value for this checker
   *
   * @return String containing valid value description
   */
  @Override
  public String getValidityString()
  {
    return ( " must be a sequence of from 2 to 4 integers of up to 3 digits separated by a period" );
  }
}
