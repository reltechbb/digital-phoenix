/*
 * CcsiStartSelfTestArgs.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/8/02
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.common.ccsi.NameValuePair;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

//~--- classes ----------------------------------------------------------------

/**
 * This class holds a list of Start_Self_Test command arguments.
 *
 * @author kmiller
 */
public class CcsiStartSelfTestArgs
{
  /** Field description */
  private final ArrayList<NameValuePair>	args;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an empty list
   */
  public CcsiStartSelfTestArgs()
  {
    args	= new ArrayList<>();
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Adds a component name with no level argument (full test)
   *
   * @param component the component to be added
   */
  public void addArgument( String component )
  {
    NameValuePair	x	= new NameValuePair();

    x.setTheName( component );
    x.setTheValue( "" );
    this.args.add( x );
  }

  /**
   * Adds a component name and a test level
   *
   * @param component the component to be added
   * @param level the test level for the component
   */
  public void addArgument( String component , String level )
  {
    NameValuePair	x	= new NameValuePair();

    x.setTheName( component );
    x.setTheValue( level );
    this.args.add( x );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the args
   */
  public ArrayList<NameValuePair> getArgs()
  {
    return args;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Clear the list
   */
  public void clear()
  {
    this.args.clear();
  }
}
