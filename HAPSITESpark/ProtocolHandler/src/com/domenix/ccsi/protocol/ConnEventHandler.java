/*
 * ConnEventHandler.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This class handles connection events received from the NSDS interface.
 *
 * Version: V1.0  15/10/16
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.commonha.ccsi.ChannelRegistrationList;
import com.domenix.commonha.intfc.NSDSConnEvent;
import com.domenix.commonha.intfc.NSDSConnection;
import com.domenix.commonha.intfc.NSDSConnectionTypeEnum;
import com.domenix.common.utils.TimerPurposeEnum;

import org.apache.log4j.Logger;

//~--- classes ----------------------------------------------------------------

/**
 * This class handles connection events received from the NSDS interface.
 * 
 * @author kmiller
 */
public class ConnEventHandler
{
  /** Error/Debug logger */
  private final Logger	myLogger	= Logger.getLogger( ConnEventHandler.class );
  
  /** The PH Context */
  private final PHContext theContext;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance
   *
   * @param ctx the PH Context
   */
  public ConnEventHandler( PHContext ctx )
  {
    this.theContext = ctx;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Routes events to specific handling functions
   *
   * @param evt the connection event
   *
   * @return success or failure
   */
  public boolean handleConnEvent( NSDSConnEvent evt )
  {
    boolean	retVal	= false;

    switch ( evt.getConnEventType() )
    {
      case CONN_TRANSIENT :     // We're in a transient connection
        retVal	= handleConnTransientEvent( evt );

        break;

      case CONN_PERSISTENT :    // We're in a persistent connection
        retVal	= handleConnPersistentEvent( evt );

        break;

      case CONN_TERMINATED :    // Not persistent and the socket closed
        retVal	= handleConnTerminatedEvent( evt );

        break;

      case CONN_FAILED :        // We tried to connect and couldn't
        retVal	= handleConnFailedEvent( evt );
        this.theContext.getTimerEventHandler().startNewTimer( this.theContext.getTheConfig().getConnFailureTimeout() , false , null , "Conn_Fail_Timer", TimerPurposeEnum.CONNFAILURE_TIMER );
        
        break;

      default :
        myLogger.error( "Unknown CONN event type." );

        break;
    }

    return ( retVal );
  }

  /**
   * Method description
   *
   *
   * @param evt
   *
   * @return
   */
  private boolean handleConnTransientEvent( NSDSConnEvent evt )
  {
    boolean	retVal	= false;

    return ( retVal );
  }

  /**
   * Method description
   *
   *
   * @param evt
   *
   * @return
   */
  private boolean handleConnPersistentEvent( NSDSConnEvent evt )
  {
    boolean	retVal	= false;

    return ( retVal );
  }

  /**
   * Method description
   *
   *
   * @param evt
   *
   * @return
   */
  private boolean handleConnTerminatedEvent( NSDSConnEvent evt )
  {
    boolean									retVal	= false;
    NSDSConnection					conn		= this.getConnection( evt.getConnId() );
    ChannelRegistrationList	list		= conn.getRegisteredChannels();

    if ( ( list != null ) && ( !list.isEmpty() ) )
    {
      this.theContext.getPendingSends().clearList();
      this.theContext.getPendingAcks().clearList();
      this.theContext.getPendingNakDetails().clear();
      this.theContext.getPendingEventGen().clear();
      this.theContext.getPendingTimedGen().clear();
      list.clearList( this.theContext.getBaseTimer() );
      conn.setConnType( NSDSConnectionTypeEnum.NO_CONNECTION );
    }

    return ( retVal );
  }

  /**
   * Method description
   *
   *
   * @param evt
   *
   * @return
   */
  private boolean handleConnFailedEvent( NSDSConnEvent evt )
  {
    boolean	retVal	= false;

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Method description
   *
   *
   * @param connId
   *
   * @return
   */
  private NSDSConnection getConnection( long connId )
  {
    NSDSConnection	temp	= this.theContext.getTheSavedState().getConnList().getItem( connId );

    return ( temp );
  }
}
