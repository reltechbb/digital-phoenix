/*
 * CcsiGetConfigArgumentList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/12/11
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a list of arguments for the Get_Configuration command.
 *
 * @author kmiller
 */
public class CcsiGetConfigArgumentList
{
  /** The list of command arguments */
  private final ArrayList<CcsiGetConfigArgument>	argList;

  /** Error/Debug logger */
  private final Logger	myLogger;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an argument list.
   */
  public CcsiGetConfigArgumentList()
  {
    argList		= new ArrayList<>();
    myLogger	= Logger.getLogger( CcsiGetConfigArgumentList.class );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * The method adds a new argument to the list and, if the item key is not empty, validates the
   * key for that type of item.
   *
   * @param arg the argument to be added
   * @return flag indicating success (true) or failure (false)
   */
  public boolean addArgument( CcsiGetConfigArgument arg )
  {
    boolean	retVal	= false;

    if ( arg != null )
    {
      if ( arg.isValid() )
      {
        this.argList.add( arg );
        retVal	= true;
      }
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the argList
   */
  public ArrayList<CcsiGetConfigArgument> getArgList()
  {
    return argList;
  }

  /**
   * Return indication if the argument list is empty.
   *
   * @return empty or not empty
   */
  public boolean isEmpty()
  {
    return ( argList.isEmpty() );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Erase the contents of the list
   */
  public void clear()
  {
    this.argList.clear();
  }
}
