/*
 * CcsiSetConfigArgumentList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/10
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

//~--- classes ----------------------------------------------------------------

/**
 * This class wraps a list of set configuration arguments.
 *
 * @author kmiller
 */
public class CcsiSetConfigArgumentList
{
  /** The list of command arguments */
  private final ArrayList<CcsiSetConfigArgument>	argList;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs the list
   */
  public CcsiSetConfigArgumentList()
  {
    this.argList	= new ArrayList<>();
  }

  //~--- methods --------------------------------------------------------------

  /**
   * The method adds a new argument to the list and, if the item key is not empty, validates the
   * key for that type of item.
   *
   * @param arg the argument to be added
   * @return flag indicating success (true) or failure (false)
   */
  public boolean addArgument( CcsiSetConfigArgument arg )
  {
    boolean	retVal	= false;

    if ( arg != null )
    {
      if ( arg.isValid() )
      {
        this.argList.add( arg );
        retVal	= true;
      }
    }

    return ( retVal );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the argList
   */
  public ArrayList<CcsiSetConfigArgument> getArgList()
  {
    return argList;
  }
}
