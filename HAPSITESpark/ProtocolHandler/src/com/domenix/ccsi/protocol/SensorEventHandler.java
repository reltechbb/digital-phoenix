/*
 * SensorEventHandler.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/16
 */
package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.ccsi.CcsiBitStatusEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.CcsiChannelRegistrationEnum;
import com.domenix.ccsi.CcsiComponentStateEnum;
import com.domenix.common.config.SensorUniqueConfigItem;
import com.domenix.commonha.ccsi.ChannelRegistrationType;
import com.domenix.commonha.intfc.NSDSConnection;
import com.domenix.commonha.intfc.NSDSConnectionList;
import com.domenix.commonha.intfc.NSDSConnectionTypeEnum;
import com.domenix.commonha.intfc.NSDSInterface;
import com.domenix.common.spark.IPCWrapper;
import com.domenix.common.spark.data.AlertTypeEnum;
import com.domenix.common.spark.data.BITEventMsg;
import com.domenix.common.spark.data.BitResultEnum;
import com.domenix.common.spark.data.CcsiComponentTypeEnum;
import com.domenix.common.spark.data.ConsumableEventMsg;
import com.domenix.common.spark.data.GetCfgRspArg;
import com.domenix.common.spark.data.ReadingEventMsg;
import com.domenix.common.spark.data.StatusEventMsg;
import com.domenix.common.utils.NumberFormatter;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------
import java.util.ArrayList;
import java.util.List;

//~--- classes ----------------------------------------------------------------
/**
 * This class handles events from the sensor
 *
 * @author kmiller
 */
public class SensorEventHandler {

    /**
     * Error/Debug logger
     */
    private final Logger myLogger = Logger.getLogger(SensorEventHandler.class);

    /**
     * PH context
     */
    private final PHContext theContext;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs a class
     *
     * @param ctx the PH Context
     */
    public SensorEventHandler(PHContext ctx) {
        this.theContext = ctx;
    }

    //~--- methods --------------------------------------------------------------
    /**
     * This method receives and processes a sensor reading event
     *
     * @param rdgs the reading
     * @param intfc the Open Interface
     *
     * @return success or failure
     */
    public boolean receiveSensorReadings(IPCWrapper rdgs, NSDSInterface intfc) {
        boolean retVal = false;
        CcsiChannelRegistrationEnum readingReg = this.getRegistration(CcsiChannelEnum.READGS, intfc);
        CcsiChannelRegistrationEnum alertsReg = this.getRegistration(CcsiChannelEnum.ALERTS, intfc);
        int rdgNbr;

        if ((rdgs != null) && (intfc != null)) {
            if (rdgs.getMsgBody().getReading().isSetId()) {
                rdgNbr = rdgs.getMsgBody().getReading().getId();
            } else {
                rdgNbr = this.theContext.getTheSavedState().getNextReadingID();
                rdgs.getMsgBody().getReading().setId(rdgNbr);
            }

            //this.theContext.getTheSavedState().getTheCacheMgr().storeCachedMessage(rdgs);
        }

        if (rdgs != null && rdgs.getMsgBody().isSetReading()) {
            ReadingEventMsg rem = rdgs.getMsgBody().getReading();

            if (readingReg == CcsiChannelRegistrationEnum.PERIOD) {
                this.theContext.getPendingReadings().addReading(rem);
            } else if (readingReg == CcsiChannelRegistrationEnum.EVENT) {
                this.theContext.getPendingReadings().addReading(rem);
                this.theContext.getPendingEventGen().addPendingEvent(new PendingEventGeneration(CcsiChannelEnum.READGS));
            }

            if (rem.isSetAlert()) {
                boolean needPendingGen = false;
                AlertTypeEnum ate = rem.getAlert();
                String detected = rem.getItem();
                ActiveAlertListEntry entry = this.findActiveReadingAlert(detected);

                if (entry != null) {
                    myLogger.info("Found active reading alert for " + detected + 
                            " AlertId: " + entry.getAlertId() +
                            " Type: " + ate.toString());

                    String alertId = entry.getAlertId();
                    long idNbr = Long.parseLong(alertId.substring(2));

                    if ((ate == AlertTypeEnum.DEALERT) || (ate == AlertTypeEnum.NONE)
                            || (ate == AlertTypeEnum.DEWARN)) {
                        // Remove the active alert entry and generate a new pending alert entry
                        PendingAlertEntry newAlert = new PendingAlertEntry(idNbr, false, rdgs.getMsgBody());
                        String clearAlertId = entry.getAlertId().replace('T', 'U');

                        newAlert.setAlertId(clearAlertId);
                        newAlert.setIdNumber(idNbr);
                        this.theContext.getPendingAlerts().addAlert(newAlert);
                        myLogger.info("Added pending alert clear for: " + clearAlertId
                            + " AlertType: " + ate.toString());
                        this.theContext.getActiveAlerts().removeActiveAlert(entry.getAlertId());
                        this.theContext.getTheSavedState().getTheCacheMgr().removeCachedMessage(entry.getCacheKey());
                        needPendingGen = true;
                    } else if (ate != entry.getEntry().getMsgBody().getReading().getAlert()) {
                        StringBuilder idBldr = new StringBuilder();

                        if (ate == AlertTypeEnum.ALERT) {
                            idBldr.append('A');
                        } else if (ate == AlertTypeEnum.WARN) {
                            idBldr.append('W');
                        }

                        if (ate == AlertTypeEnum.DEWARN) {
                            idBldr.append('U');
                        } else {
                            idBldr.append('T');
                        }

                        String idNbrStr = NumberFormatter.getLong(idNbr, 10, true, 9, '0');

                        idBldr.append(idNbrStr);
                        entry.setAlertId(idBldr.toString());
                        // update the alert type
                        entry.getEntry().getMsgBody().getReading().setAlert(ate);
                        rdgs.setCacheKey("alert-" + idNbrStr);
                        this.theContext.getTheSavedState().getTheCacheMgr().storeCachedMessage(rdgs);

                        PendingAlertEntry newAlert = new PendingAlertEntry(idNbr, true, rdgs.getMsgBody());

                        newAlert.setAlertId(idBldr.toString());
                        newAlert.setIdNumber(idNbr);

                        this.theContext.getPendingAlerts().addAlert(newAlert);
                        needPendingGen = true;
                        myLogger.info ("Modifying existing alert for: " + idBldr.toString()
                            + " AlertType: " + ate.toString());
                    }
                } else if ((ate != AlertTypeEnum.NONE) && (ate != AlertTypeEnum.DEWARN) 
                        && (ate != AlertTypeEnum.DEALERT)){
                    // new alert
                    long idNbr = this.theContext.getTheSavedState().getNextAlertID();

                    PendingAlertEntry newAlert = new PendingAlertEntry(idNbr, true, rdgs.getMsgBody());

                    newAlert.setReading(true);
                    newAlert.setIdNumber(idNbr);

                    StringBuilder idBldr = new StringBuilder();

                    if (ate == AlertTypeEnum.ALERT) {
                        idBldr.append('A');
                    } else if (ate == AlertTypeEnum.WARN) {
                        idBldr.append('W');
                    }

                    idBldr.append('T');

                    String idStr = NumberFormatter.getLong(idNbr, 10, true, 9, '0');

                    idBldr.append(idStr);
                    newAlert.setAlertId(idBldr.toString());

                    this.theContext.getPendingAlerts().addAlert(newAlert);

                    ActiveAlertListEntry active = new ActiveAlertListEntry();

                    active.setAlertId(idBldr.toString());
                    active.setEntry(rdgs);
                    active.setCacheKey("alert-" + idStr);
                    rdgs.setCacheKey("alert-" + idStr);
                    this.theContext.getTheSavedState().getTheCacheMgr().storeCachedMessage(rdgs);
                    this.theContext.getActiveAlerts().addAlert(active);
                    needPendingGen = true;
                    myLogger.info ("Adding new alert for: " + idBldr.toString() + " AlertType: " + ate.toString());
                }

                if ((needPendingGen) && (alertsReg == CcsiChannelRegistrationEnum.EVENT)) {
                    this.theContext.getPendingEventGen().addPendingEvent(new PendingEventGeneration(CcsiChannelEnum.ALERTS));
                }
            }
        }

        return (retVal);
    }

    /**
     * This method receives and processes a sensor BIT event
     *
     * @param bit the BIT event
     * @param intfc the Open Interface
     *
     * @return success or failure
     */
    public boolean receiveSensorBIT(IPCWrapper bit, NSDSInterface intfc) {
        boolean retVal = false;
        CcsiChannelRegistrationEnum maintReg = this.getRegistration(CcsiChannelEnum.MAINT, intfc);
        CcsiChannelRegistrationEnum statusReg = this.getRegistration(CcsiChannelEnum.STATUS, intfc);
        CcsiChannelRegistrationEnum alertsReg = this.getRegistration(CcsiChannelEnum.ALERTS, intfc);
        bit.setCacheKey("bit-" + Long.toString(bit.getStamp()));

        if ((bit != null) && (intfc != null)) {
            this.theContext.getTheSavedState().getTheCacheMgr().storeCachedMessage(bit);
        }

        ArrayList<BITEventMsg> bits = (ArrayList<BITEventMsg>) bit.getMsgBody().getBIT();

        for (BITEventMsg x : bits) {
            switch (x.getComponentType()) {
                case PDIL:
                    this.theContext.getTheSavedState().setComponentCCBitStatus(CcsiBitStatusEnum.valueOf(x.getResult().name()));
                    this.theContext.getTheSavedState().setComponentCCBitTime(x.getBitTime().toGregorianCalendar().getTime());
                    this.theContext.getTheSavedState().setComponentCCStatus((x.getResult() == BitResultEnum.PASS)
                            ? CcsiComponentStateEnum.N
                            : CcsiComponentStateEnum.D);

                    break;

                case SC:
                    this.theContext.getTheSavedState().setComponentSCBitStatus
                        (CcsiBitStatusEnum.valueOf(x.getResult().name()), x.getComponentId());
                    this.theContext.getTheSavedState().setComponentSCBitTime
                        (x.getBitTime().toGregorianCalendar().getTime(), x.getComponentId());
                    this.theContext.getTheSavedState().setComponentSCStatus((x.getResult() == BitResultEnum.PASS)
                            ? CcsiComponentStateEnum.N
                            : CcsiComponentStateEnum.D, x.getComponentId());

                    break;

                default:
                    break;
            }

            if (x.getResult() != BitResultEnum.PASS) {
                this.theContext.getTheSavedState().setStateDegraded(true);

                if (x.getComponentType() == CcsiComponentTypeEnum.SC) {
                    this.theContext.getTheSavedState().setStateReadingAffected(true);
                }
            }

            if (maintReg == CcsiChannelRegistrationEnum.PERIOD) {
                this.theContext.getPendingBits().addBit(x);
            } else if (maintReg == CcsiChannelRegistrationEnum.EVENT) {
                this.theContext.getPendingBits().addBit(x);
                this.theContext.getPendingEventGen().addPendingEvent(new PendingEventGeneration(CcsiChannelEnum.MAINT));
            }

            if (x.isSetResult()) {
                boolean needPendingGen = false;
                CcsiComponentTypeEnum which = x.getComponentType();
                ActiveAlertListEntry entry = this.findActiveBitAlert(which);

                if (entry != null) {
                    if (x.getResult() == BitResultEnum.PASS) {
                        // Remove the active alert and add a clear to the pending alerts
                        long idNbr = Long.parseLong(entry.getAlertId().substring(2));

                        this.theContext.getActiveAlerts().removeActiveAlert(entry.getAlertId());
                        needPendingGen = true;
                        this.theContext.getTheSavedState().getTheCacheMgr().removeCachedMessage(entry.getCacheKey());

                        PendingAlertEntry newAlert = new PendingAlertEntry(idNbr, false, bit.getMsgBody());

                        newAlert.setReading(false);
                        newAlert.setIdNumber(idNbr);

                        StringBuilder idBldr = new StringBuilder("AU");
                        String idStr = NumberFormatter.getLong(idNbr, 10, true, 9, '0');

                        idBldr.append(idStr);
                        newAlert.setAlertId(idBldr.toString());
                        this.theContext.getPendingAlerts().addAlert(newAlert);
                    }
                } else {
                    if (x.getResult() == BitResultEnum.FAIL) {
                        long idNbr = this.theContext.getTheSavedState().getNextAlertID();
                        PendingAlertEntry newAlert = new PendingAlertEntry(idNbr, true, bit.getMsgBody());

                        newAlert.setReading(false);
                        newAlert.setIdNumber(idNbr);

                        StringBuilder idBldr = new StringBuilder("AT");
                        String idStr = NumberFormatter.getLong(idNbr, 10, true, 9, '0');

                        idBldr.append(idStr);
                        newAlert.setAlertId(idBldr.toString());
                        this.theContext.getPendingAlerts().addAlert(newAlert);
                        entry = new ActiveAlertListEntry();
                        entry.setCacheKey("alert-" + idStr);
                        bit.setCacheKey("alert-" + idStr);
                        this.theContext.getTheSavedState().getTheCacheMgr().storeCachedMessage(bit);
                        entry.setEntry(bit);
                        entry.setAlertId(idBldr.toString());
                        this.theContext.getActiveAlerts().addAlert(entry);
                        needPendingGen = true;
                    }
                }

                if ((needPendingGen) && (alertsReg == CcsiChannelRegistrationEnum.EVENT)) {
                    this.theContext.getPendingEventGen().addPendingEvent(new PendingEventGeneration(CcsiChannelEnum.ALERTS));
                }

                if (statusReg == CcsiChannelRegistrationEnum.EVENT) {
                    PendingEventGeneration pendStatus = new PendingEventGeneration(CcsiChannelEnum.STATUS);

                    pendStatus.addItem("LRU");
                    this.theContext.getPendingEventGen().addPendingEvent(pendStatus);
                } else if (statusReg == CcsiChannelRegistrationEnum.PERIOD) {
                    PendingStatusEntry pse = new PendingStatusEntry("LRU");
                    this.theContext.getPendingStatusList().addPendingStatus(pse);
                }
            }

            try {
                this.theContext.getTheSavedState().writeSavedState();
            } catch (Exception ex) {
                myLogger.error("Exception writing saved state", ex);
            }

            retVal = true;
        }

        return (retVal);
    }

    /**
     * This method receives and processes a sensor consumables event
     *
     * @param consume the consumables event
     * @param intfc the Open Interface
     *
     * @return success or failure
     */
    public boolean receiveSensorConsumable(IPCWrapper consume, NSDSInterface intfc) {
        boolean retVal = false;

        if ((consume != null) && (intfc != null)) {
            boolean needPendingGen = false;
            CcsiChannelRegistrationEnum maintReg = this.getRegistration(CcsiChannelEnum.MAINT, intfc);
            CcsiChannelRegistrationEnum statusReg = this.getRegistration(CcsiChannelEnum.STATUS, intfc);
            consume.setCacheKey("consume-" + Long.toString(consume.getStamp()));

            if (consume.getMsgBody().isSetConsumable()) {
                for (ConsumableEventMsg x : consume.getMsgBody().getConsumable()) {
                    this.theContext.getPendingConsumables().addConsumable(x);
                    needPendingGen = true;

                    if (x.isNeedsAction()) {
                        this.theContext.getTheSavedState().setStateMaintReq(true);
                    } else {
                        this.theContext.getTheSavedState().setStateMaintReq(false);
                    }
                }

                if ((needPendingGen) && (maintReg == CcsiChannelRegistrationEnum.EVENT)) {
                    this.theContext.getPendingEventGen().addPendingEvent(new PendingEventGeneration(CcsiChannelEnum.MAINT));
                }

                if (statusReg == CcsiChannelRegistrationEnum.EVENT) {
                    PendingEventGeneration pendStatus = new PendingEventGeneration(CcsiChannelEnum.STATUS);

                    pendStatus.addItem("State");
                    this.theContext.getPendingEventGen().addPendingEvent(pendStatus);
                } else if (statusReg == CcsiChannelRegistrationEnum.PERIOD) {
                    PendingStatusEntry pse = new PendingStatusEntry("State");
                    this.theContext.getPendingStatusList().addPendingStatus(pse);
                }
                try {
                    this.theContext.getTheSavedState().writeSavedState();
                } catch (Exception ex) {
                    myLogger.error("Exception writing saved state", ex);
                }
            }
        }

        return (retVal);
    }

    /**
     * This method receives and processes a sensor status event
     *
     * @param status the status event
     * @param intfc the Open Interface
     *
     * @return success or failure
     */
    public boolean receiveSensorStatus(IPCWrapper status, NSDSInterface intfc) {
        boolean retVal = false;

        if ((status != null) && (intfc != null)) {
            boolean needStatusGen = false;
            CcsiChannelRegistrationEnum statusReg = this.getRegistration(CcsiChannelEnum.STATUS, intfc);
            status.setCacheKey("status-" + Long.toString(status.getStamp()));

            if (status.getMsgBody().isSetStatus()) {
                StatusEventMsg msg = status.getMsgBody().getStatus();

                if (msg.isSetDegraded()) {
                    if (this.theContext.getTheSavedState().isStateDegraded() != msg.isDegraded()) {
                        this.theContext.getTheSavedState().setStateDegraded(msg.isDegraded());
                        needStatusGen = true;

                        PendingStatusEntry pse = new PendingStatusEntry("State");

                        this.theContext.getPendingStatusList().addPendingStatus(pse);
                    }
                }

                if (msg.isSetInoperable()) {
                    if (this.theContext.getTheSavedState().isStateSensorInop() != msg.isInoperable()) {
                        this.theContext.getTheSavedState().setStateSensorInop(msg.isInoperable());
                        needStatusGen = true;

                        PendingStatusEntry pse = new PendingStatusEntry("State");

                        this.theContext.getPendingStatusList().addPendingStatus(pse);
                    }
                }

                if (msg.isSetMaintReq()) {
                    if (this.theContext.getTheSavedState().isStateMaintReq() != msg.isMaintReq()) {
                        this.theContext.getTheSavedState().setStateMaintReq(msg.isMaintReq());
                        needStatusGen = true;

                        PendingStatusEntry pse = new PendingStatusEntry("State");

                        this.theContext.getPendingStatusList().addPendingStatus(pse);
                    }
                }

                if (msg.isSetReadingAffected()) {
                    if (this.theContext.getTheSavedState().isStateReadingAffected() != msg.isReadingAffected()) {
                        this.theContext.getTheSavedState().setStateReadingAffected(msg.isReadingAffected());
                        needStatusGen = true;

                        PendingStatusEntry pse = new PendingStatusEntry("State");

                        this.theContext.getPendingStatusList().addPendingStatus(pse);
                    }
                }

                if (msg.isSetTypeReq()) {
                    needStatusGen = true;
                }

                if (msg.isSetPowerMode()) {
                    if (this.theContext.getTheSavedState().getStatePwrStatus() != msg.getPowerMode()) {
                        this.theContext.getTheSavedState().setStatePwrStatus(msg.getPowerMode());
                        needStatusGen = true;

                        PendingStatusEntry pse = new PendingStatusEntry("Power");

                        this.theContext.getPendingStatusList().addPendingStatus(pse);
                    }
                }

                if ((needStatusGen) && (statusReg == CcsiChannelRegistrationEnum.EVENT)) {
                    PendingEventGeneration pendStatus = new PendingEventGeneration(CcsiChannelEnum.STATUS);

                    pendStatus.addItem("State");
                    this.theContext.getPendingEventGen().addPendingEvent(pendStatus);
                }
            }
        }

        return (retVal);
    }

    /**
     * This method receives and processes a command response event
     *
     * @param cmdResponse the command response event
     * @param intfc the Open Interface
     *
     * @return success or failure
     */
    public boolean receiveCmdResponse(IPCWrapper cmdResponse, NSDSInterface intfc) {
        boolean retVal = false;

        switch (cmdResponse.getRoutingKey()) {
            case "ccsi.evt.getcfgresponse":
                retVal = handleGetCfgResponse(cmdResponse, intfc);
                break;
            case "ccsi.evt.sensorcfgchange":
                // sensor configuration has changed 
                retVal = handleSensorCfgChange (cmdResponse);
                if (retVal == true)
                {
                    // notify the host of the change
                    retVal = handleGetCfgResponse(cmdResponse, intfc);
                }
                break;
            case "ccsi.evt.location":
            case "ccsi.evt.security":
                myLogger.error("Unsupported routing key: " + cmdResponse.getRoutingKey());
                break;
            default:
                myLogger.error("Unknown routing key: " + cmdResponse.getRoutingKey());
                break;
        }
        return (retVal);
    }

    /**
     * Sensor is reporting a configuration change - process the change
     * @param cmdResponse
     * @return true = successful config change
     */
    private boolean handleSensorCfgChange (IPCWrapper cmdResponse)
    {
        boolean retVal = true;
        List <GetCfgRspArg> args = cmdResponse.getMsgBody().getGetCfgResponse().getArg();
        if (args != null)
        {
            for (GetCfgRspArg arg : args)
            {
               switch (arg.getBlock()) 
               {
                   case BASE: 
                   case GENERAL:
                   case ANNUN:
                   case TAMPER:
                   case COMM:
                   case SECURITY:
                   case LINKS:
                   case SENSDESC:
                   case OPERATORS:
                   case LOCAL:
                   default:
                       // nothing settable by sensor for this case
                       retVal = false;
                       myLogger.error("Unsupported, Invalid or missing block: \"" + arg.getBlock() + "\"");
                       break;
                   case UNIQUE:
                       SensorUniqueConfigItem theConfigItem
                               = theContext.getTheConfig().
                                       getSensorUniqueConfig(arg.getItem());
                       if (theConfigItem != null) {
                           theConfigItem.setValue(arg.getValue());
                       } else
                       {
                           retVal = false;
                       }
                       break;
               }
            }
        } else
        {
            retVal = false;
        }
        return retVal;
    }
    
    /**
     * Process the sensor response to a Get_Config message
     *
     * @param cmdResponse
     * @param intfc
     * @return
     */
    private boolean handleGetCfgResponse(IPCWrapper cmdResponse, NSDSInterface intfc) {
        long myConn = -1L;
        for (NSDSConnection x : intfc.getActiveConnections().getConnectionList()) {
            if (x.getConnType() == NSDSConnectionTypeEnum.PERSISTENT_CONNECTION) {
                myConn = x.getConnectionId();
                break;
            }
        }
        boolean retValue = true;

        if (myConn != -1) {
            String iMsg = this.theContext.getReportGenerator().
                    genSensorGetCfgResponse(cmdResponse, intfc);
            PendingSendEntry ipse = new PendingSendEntry();

            ipse.addChannel(CcsiChannelEnum.CONFIG);
            ipse.setMessage(iMsg);
            ipse.setAckRequired(true);
            ipse.setConnectionId(myConn);
            ipse.setSendReport(true);
            this.theContext.getPendingSends().addPendingSend(ipse);
        }
        return retValue;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Check for a registered channel and return the type of registration.
     *
     * @param channel the channel to be found
     * @param intfc
     *
     * @return registration type
     */
    private CcsiChannelRegistrationEnum getRegistration(CcsiChannelEnum channel, NSDSInterface intfc) {
        CcsiChannelRegistrationEnum retVal = CcsiChannelRegistrationEnum.NONE;

        if ((intfc != null) && (channel != null)) {
            NSDSConnectionList conns = intfc.getActiveConnections();

            for (NSDSConnection x : conns.getConnectionList()) {
                if (x.getConnType() == NSDSConnectionTypeEnum.PERSISTENT_CONNECTION) {
                    if (x.getRegisteredChannels().hasChannel(channel)) {
                        ChannelRegistrationType regType = x.getRegisteredChannels().getRegistration(channel);

                        retVal = regType.getRegType();

                        break;
                    }
                }
            }
        }

        return (retVal);
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Find an active alert for a reading and return it.
     *
     * @param itemType the type of detected item
     *
     * @return the active alert
     */
    private ActiveAlertListEntry findActiveReadingAlert(String itemType) {
        ActiveAlertListEntry retVal = null;

        myLogger.debug ("Searching for match to alert: " + itemType + " List size: " +
                this.theContext.getActiveAlerts().getAllEntries().length);
        for (Object x : this.theContext.getActiveAlerts().getAllEntries()) {
            ActiveAlertListEntry x1 = (ActiveAlertListEntry) x;
            myLogger.debug ("IsSetReading: " + x1.getEntry().getMsgBody().isSetReading()
                + " IsSetItem: " + x1.getEntry().getMsgBody().getReading().isSetItem());
            if (x1.getEntry().getMsgBody().isSetReading()) {
                if (x1.getEntry().getMsgBody().getReading().isSetItem()) {
                    myLogger.debug ("Comparing: " + x1.getEntry().getMsgBody().getReading().getItem());
                    if (x1.getEntry().getMsgBody().getReading().getItem().equalsIgnoreCase(itemType)) {
                        myLogger.info("Found active alert for reading " + x1.getAlertId());
                        retVal = x1;

                        break;
                    }
                }
            }
        }

        return (retVal);
    }

    /**
     * Find an active alert for a BIT and return it.
     *
     * @param comp the type of component
     *
     * @return the active alert
     */
    private ActiveAlertListEntry findActiveBitAlert(CcsiComponentTypeEnum comp) {
        ActiveAlertListEntry retVal = null;

        for (Object x : this.theContext.getActiveAlerts().getAllEntries()) {
            ActiveAlertListEntry x1 = (ActiveAlertListEntry) x;

            if (x1.getEntry().getMsgBody().isSetBIT()) {
                if (x1.getEntry().getMsgBody().getBIT().get(0).isSetComponentType()) {
                    if (x1.getEntry().getMsgBody().getBIT().get(0).getComponentType() == comp) {
                        retVal = x1;

                        break;
                    }
                }
            }
        }

        return (retVal);
    }
}
