/*
 * PendingStatusEntry.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/02
 */


package com.domenix.ccsi.protocol;

/**
 * This class is a single status entry that must be sent to the host.
 * 
 * @author kmiller
 */
public class PendingStatusEntry
{
  /** The block of status information to be produced */
  private String	statusBlock	= "State";

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance with a block name
   *
   * @param block the status block to be produced
   */
  public PendingStatusEntry( String block )
  {
    if ( ( block != null ) && ( !block.trim().isEmpty() ) )
    {
      this.statusBlock	= block.trim();
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the statusBlock
   */
  public String getStatusBlock()
  {
    return statusBlock;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param statusBlock the statusBlock to set
   */
  public void setStatusBlock( String statusBlock )
  {
    this.statusBlock	= statusBlock;
  }
}
