/*
 * CcsiGetConfigArgument.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/11/27
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.CcsiConfigBlockEnum;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//~--- classes ----------------------------------------------------------------

/**
 * This class encapsulates an argument for the Get_Configuration command.
 *
 * @author kmiller
 */
public class CcsiGetConfigArgument
{
  /** Is the entry valid */
  private boolean	valid	= true;

  /** Sensing component name pattern */
  private final Pattern	scNamePattern	= Pattern.compile( "SC([0-9]{3})" );

  /** Pattern for a link name type */
  private final Pattern	linkNamePattern	= Pattern.compile( "[UWRF]([0-9]{3})" );

  /** The item's block */
  private CcsiConfigBlockEnum	itemBlock;

  /** The block key */
  private String	itemKey;

  /** The item name */
  private String	itemName;

  /** Error/Debug logger */
  private final Logger	myLogger;

  /** NAK details due to argument validation */
  private final PendingNakDetailList	nakDetails;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   */
  public CcsiGetConfigArgument()
  {
    myLogger				= Logger.getLogger( CcsiGetConfigArgument.class );
    nakDetails			= new PendingNakDetailList();
    this.itemBlock	= null;
    this.itemKey		= null;
    this.itemName		= null;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Add a NAK detail entry to the argument
   *
   * @param pnd NAK detail to be added
   */
  public void addNakDetail( PendingNakDetailEntry pnd )
  {
    this.nakDetails.addEntry( pnd );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the itemBlock
   */
  public CcsiConfigBlockEnum getItemBlock()
  {
    return itemBlock;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param itemBlock the itemBlock to set
   */
  public void setItemBlock( CcsiConfigBlockEnum itemBlock )
  {
    if ( itemBlock != null )
    {
      this.itemBlock	= itemBlock;
      this.valid			= true;
    }
    else
    {
      this.valid	= false;
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the itemKey
   */
  public String getItemKey()
  {
    return itemKey;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param itemKey the itemKey to set
   */
  public void setItemKey( String itemKey )
  {
    Matcher	scM	= scNamePattern.matcher( itemKey.trim() );
    Matcher	lnM	= linkNamePattern.matcher( itemKey.trim() );

    if ( itemKey.contentEquals( "ALL" ) )
    {
      this.itemKey	= "ALL";
      this.valid		= true;
    }
    else if ( ( scM.matches() ) || ( lnM.matches() ) )
    {
      this.itemKey	= itemKey;
      this.valid		= true;
    }
    else
    {
      this.valid	= false;
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the itemName
   */
  public String getItemName()
  {
    return itemName;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param itemName the itemName to set
   */
  public void setItemName( String itemName )
  {
    this.itemName	= itemName;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the nakDetails
   */
  public PendingNakDetailList getNakDetails()
  {
    return nakDetails;
  }

  /**
   * Validate the argument and return check status
   *
   * @return flag indicating valid or not valid
   */
  public boolean isValid()
  {
    return ( this.valid );
  }
}
