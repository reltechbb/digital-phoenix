/*
 * DataValidityLengthRange.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/11
 */


package com.domenix.ccsi.protocol;

/**
 * This class implements a maximum string length data validity type.  The specified
 * length must be greater than 0.
 *
 * @author kmiller
 */
public class DataValidityLengthRange implements DataValidityInterface
{
  /** Minimum length */
  private int	minLength	= 0;

  /** The type of validity check. */
  private final DataValidityType	theType	= DataValidityType.STRINGLENGTHRANGE;

  /** The minimum valid string length. */
  private int	maxLength	= Short.MAX_VALUE;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an empty instance.
   */
  public DataValidityLengthRange()
  {
  }

  /**
   * Constructs an instance of the class with a minimum and maximum value.
   *
   * @param min int containing the minimum valid string length
   * @param max int containing the maximum valid string length
   *
   * @throws IllegalArgumentException
   */
  public DataValidityLengthRange( int min , int max ) throws IllegalArgumentException
  {
    if ( ( max > 0 ) && ( max <= Short.MAX_VALUE ) )
    {
      this.maxLength	= max;
    }
    else
    {
      throw new IllegalArgumentException( "Maximum string length must be > 0" );
    }

    if ( ( min >= 0 ) && ( min < Short.MAX_VALUE ) )
    {
      this.minLength	= min;
    }
    else
    {
      throw new IllegalArgumentException( "Minimum string length must be >= 0" );
    }

    if ( max < min )
    {
      throw new IllegalArgumentException( "Minimum string length must be <= maximum" );
    }
  }

  /**
   * Constructs an instance of the class with a maximum value.
   *
   * @param min String containing the maximum valid string length
   * @param max
   *
   * @throws IllegalArgumentException
   * @throws NumberFormatException
   */
  public DataValidityLengthRange( String min , String max ) throws IllegalArgumentException , NumberFormatException
  {
    if ( ( max != null ) && ( max.trim().length() > 0 ) )
    {
      this.maxLength	= Short.parseShort( max.trim() );
    }
    else
    {
      throw new IllegalArgumentException( "Maximum string length must be > 0" );
    }

    if ( ( min != null ) && ( min.trim().length() > 0 ) )
    {
      this.minLength	= Short.parseShort( min.trim() );
    }
    else
    {
      throw new IllegalArgumentException( "Minimum string length must be >= 0" );
    }

    if ( maxLength < minLength )
    {
      throw new IllegalArgumentException( "Minimum string length must be <= maximum" );
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the validity check type of this class.
   *
   * @return DataValidityType which will be MAXSTRINGLENGTH
   */
  @Override
  public DataValidityType getTheType()
  {
    return ( this.theType );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Validates an input string and returns an indication if it meets the maximum
   * length requirement.  The value is considered valid if it is not null, and if
   * the length, including any spaces, is less than or equal to the maximum required length.  If the
   * value is not set the validation will also be true.
   *
   * @param value String whose length is to be validated
   *
   * @return boolean indication of valid (true) or invalid (false)
   */
  @Override
  public boolean validateValue( String value )
  {
    boolean	retValue	= true;

    if ( this.getMaxLength() > 0 )
    {
      if ( ( value == null ) )
      {
        retValue	= false;
      } else if ( ( value.length() > (int) this.getMaxLength() ) )
      {
        retValue	= false;
      }
      else if ( ( value.length() < (int) this.getMinLength() ) )
      {
        retValue	= false;
      }
    }

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the maxLength
   */
  public int getMaxLength()
  {
    return maxLength;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param maxLength the maxLength to set
   */
  public void setMaxLength( int maxLength )
  {
    this.maxLength	= maxLength;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the minLength
   */
  public int getMinLength()
  {
    return minLength;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param minLength the minLength to set
   */
  public void setMinLength( int minLength )
  {
    this.minLength	= minLength;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the description of what is valid using this validator.
   *
   * @return String containing the description
   */
  @Override
  public String getValidityString()
  {
    StringBuilder	msg	= new StringBuilder( " length must be >= " );

    msg.append( this.minLength );
    msg.append( " and <= " );
    msg.append( this.maxLength );

    return ( msg.toString() );
  }
}
