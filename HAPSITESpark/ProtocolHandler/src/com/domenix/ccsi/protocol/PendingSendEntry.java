/*
 * PendingSendEntry.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/16
 */


package com.domenix.ccsi.protocol;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.commonha.ccsi.CcsiAckNakEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a single entry in list of things to be sent to the host.
 * 
 * @author kmiller
 */
public class PendingSendEntry
{
  /** ACK/NAK MSN */
  private long	ackMsn	= -1L;

  /** The cache keys */
  private String[]	cacheKeys	= null;

  /** The channel that was sent */
  private CcsiChannelEnum[]	channel	= null;

  /** The connection */
  private long	connectionId	= -1L;

  /** The message that was sent */
  private String	message	= null;

  /** Was an ACK required */
  private boolean	ackRequired	= false;

  /** NAK Reason Code */
  private NakReasonCodeEnum	nakCode	= null;

  /** NAK Detail Code */
  private NakDetailCodeEnum	nakDetailCode	= null;

  /** NAK detail text */
  private String	nakDetailMessage	= null;

  /** Is this a send heartbeat */
  private boolean	sendHrtbt	= false;

  /** Is this a send ACK */
  private boolean	sendAck	= false;

  /** Is this a send NAK */
  private boolean	sendNak	= false;

  /** Is this a report send */
  private boolean	sendReport	= false;
  
  /** Send a disconnect on the connection */
  private boolean sendDisconnect = false;

  /** The ACK/NAK type */
  private CcsiAckNakEnum	ackType	= CcsiAckNakEnum.NONE;

  /** The channels for transmit */
  private final ArrayList<CcsiChannelEnum>	theChannels	= new ArrayList<>();

  /** the message has been sent */
  private boolean messageSent = false;
  
  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an empty entry
   */
  public PendingSendEntry()
  {
  }

  /**
   * Construct an entry with the channel and message
   *
   * @param channel
   * @param message
   */
  public PendingSendEntry( CcsiChannelEnum channel , String message )
  {
    theChannels.add( channel );
    this.message	= message;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the message
   */
  public String getMessage()
  {
    return message;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param message the message to set
   */
  public void setMessage( String message )
  {
    this.message	= message;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the channel array
   */
  public CcsiChannelEnum[] getChannel()
  {
    this.channel	= new CcsiChannelEnum[this.theChannels.size()];
    this.channel	= this.theChannels.toArray( channel );

    return (CcsiChannelEnum[]) channel.clone();
  }

  //~--- methods --------------------------------------------------------------

  /**
   * @param channel the channel to set
   */
  public void addChannel( CcsiChannelEnum channel )
  {
    this.theChannels.add( channel );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the ackRequired
   */
  public boolean isAckRequired()
  {
    return ackRequired;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param ackRequired the ackRequired to set
   */
  public void setAckRequired( boolean ackRequired )
  {
    this.ackRequired	= ackRequired;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the connectionId
   */
  public long getConnectionId()
  {
    return connectionId;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param connectionId the connectionId to set
   */
  public void setConnectionId( long connectionId )
  {
    this.connectionId	= connectionId;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the cacheKeys
   */
  public String[] getCacheKeys()
  {
    String [] retVal = null;
    if (cacheKeys != null)
    {
        retVal = (String[]) cacheKeys.clone();
    }
    return retVal;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param cacheKeys the cacheKeys to set
   */
  public void setCacheKeys( String[] cacheKeys )
  {
    if (cacheKeys != null)
    {
        this.cacheKeys	= (String[]) cacheKeys.clone();
    } else
    {
        this.cacheKeys = null;
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the sendHrtbt
   */
  public boolean isSendHrtbt()
  {
    return sendHrtbt;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param sendHrtbt the sendHrtbt to set
   */
  public void setSendHrtbt( boolean sendHrtbt )
  {
    this.sendHrtbt	= sendHrtbt;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the sendAck
   */
  public boolean isSendAck()
  {
    return sendAck;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param sendAck the sendAck to set
   */
  public void setSendAck( boolean sendAck )
  {
    this.sendAck	= sendAck;
    this.ackType  = CcsiAckNakEnum.ACK;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the sendNak
   */
  public boolean isSendNak()
  {
    return sendNak;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param sendNak the sendNak to set
   */
  public void setSendNak( boolean sendNak )
  {
    this.sendNak	= sendNak;
    this.ackType  = CcsiAckNakEnum.NACK;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the sendReport
   */
  public boolean isSendReport()
  {
    return sendReport;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param sendReport the sendReport to set
   */
  public void setSendReport( boolean sendReport )
  {
    this.sendReport	= sendReport;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the ackMsn
   */
  public long getAckMsn()
  {
    return ackMsn;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param ackMsn the ackMsn to set
   */
  public void setAckMsn( long ackMsn )
  {
    this.ackMsn	= ackMsn;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the nakCode
   */
  public NakReasonCodeEnum getNakCode()
  {
    return nakCode;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param nakCode the nakCode to set
   */
  public void setNakCode( NakReasonCodeEnum nakCode )
  {
    this.nakCode	= nakCode;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the nakDetailCode
   */
  public NakDetailCodeEnum getNakDetailCode()
  {
    return nakDetailCode;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param nakDetailCode the nakDetailCode to set
   */
  public void setNakDetailCode( NakDetailCodeEnum nakDetailCode )
  {
    this.nakDetailCode	= nakDetailCode;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the nakDetailMessage
   */
  public String getNakDetailMessage()
  {
    return nakDetailMessage;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param nakDetailMessage the nakDetailMessage to set
   */
  public void setNakDetailMessage( String nakDetailMessage )
  {
    this.nakDetailMessage	= nakDetailMessage;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the ackType
   */
  public CcsiAckNakEnum getAckType()
  {
    return ackType;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param ackType the ackType to set
   */
  public void setAckType( CcsiAckNakEnum ackType )
  {
    this.ackType	= ackType;
  }

  /**
   * @return the sendDisconnect
   */
  public boolean isSendDisconnect()
  {
    return sendDisconnect;
  }

  /**
   * @param sendDisconnect the sendDisconnect to set
   */
  public void setSendDisconnect(boolean sendDisconnect)
  {
    this.sendDisconnect = sendDisconnect;
  }
  
  /**
   * This message has been sent - this is in order to avoid resends
   */
  public void setMessageSent ()
  {
      messageSent = true;
  }
  
  public boolean wasMessageSent ()
  {
      return messageSent;
  }
}
