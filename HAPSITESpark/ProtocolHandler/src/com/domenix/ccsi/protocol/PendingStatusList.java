/*
 * PendingStatusList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/12/02
 */


package com.domenix.ccsi.protocol;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a list of status events to be sent to the host.
 * 
 * @author kmiller
 */
public class PendingStatusList
{
  /** The list */
  private final LinkedBlockingQueue<PendingStatusEntry>	theList	= new LinkedBlockingQueue<>();

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs the list.
   */
  public PendingStatusList()
  {
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Method description
   *
   *
   * @param pse
   *
   * @return
   */
  public boolean addPendingStatus( PendingStatusEntry pse )
  {
    boolean	retVal	= false;

    if ( pse != null )
    {
      // Avoid duplicate entries
      for ( PendingStatusEntry x : this.theList )
      {
        if ( x.getStatusBlock().equalsIgnoreCase( pse.getStatusBlock() ) )
        {
          return( true );
        }
      }
      this.theList.add( pse );
      retVal	= true;
    }

    return ( retVal );
  }

  /**
   * Method description
   *
   */
  public void clear()
  {
    this.theList.clear();
  }

  /**
   * Method description
   *
   *
   * @return
   */
  public int size()
  {
    return ( this.theList.size() );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Method description
   *
   *
   * @return
   */
  public boolean isEmpty()
  {
    return ( this.theList.isEmpty() );
  }

  /**
   * Method description
   *
   *
   * @return
   */
  public PendingStatusEntry getPendingStatusEntry()
  {
    PendingStatusEntry	pse	= null;

    if ( !this.theList.isEmpty() )
    {
      pse	= this.theList.poll();
    }

    return ( pse );
  }

  /**
   * Method description
   *
   *
   * @return
   */
  public ArrayList<PendingStatusEntry> getAllEntries()
  {
    ArrayList<PendingStatusEntry>	aList	= new ArrayList<>();

    while ( !this.theList.isEmpty() )
    {
      aList.add( this.theList.poll() );
    }

    return ( aList );
  }
}
