/*
 * CcsiComponentStateEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the enumeration of CCSI component states.
 *
 * Version: V1.0  15/08/30
 */


package com.domenix.ccsi;

/**
 * This enumerates the current state of a CCSI component.
 *
 * @author kmiller
 */
public enum CcsiComponentStateEnum
{
  /** Normal */
  N ,

  /** Degraded */
  D ,

  /** Failed */
  F;
}
