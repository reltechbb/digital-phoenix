/*
 * CcsiLocationReportOptionEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the enumeration of how a sensor will behave when no location source
 * is available.
 *
 * Version: V1.0  15/08/30
 */


package com.domenix.ccsi;

/**
 * This enumerates the desired behavior of the sensor when no location
 * source is available.
 * 
 * @author kmiller
 */
public enum CcsiLocationReportOptionEnum
{
  /** Report no location */
  NONE ,

  /** Report a location error */
  ERROR ,

  /** Report the last known location */
  LAST;
}
