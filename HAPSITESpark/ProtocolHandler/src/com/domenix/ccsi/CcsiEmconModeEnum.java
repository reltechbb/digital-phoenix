/*
 * CcsiEmconModeEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of emissions control mode enumerated values.
 *
 * Version: V1.0  15/08/30
 */


package com.domenix.ccsi;

/**
 * This enumerates the emission control mode (EMCON) of the sensor.
 *
 * @author kmiller
 */
public enum CcsiEmconModeEnum
{
  /** No EMCON in effect */
  NONE ,

  /** Alerts only are sent */
  ALERT ,

  /** Receive commands only */
  RECEIVE ,

  /** Turn all transmissions and radio off */
  OFF ,

  /** Radio turned off until commanded on */
  SLEEP;
}
