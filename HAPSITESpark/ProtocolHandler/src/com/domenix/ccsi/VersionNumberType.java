/*
 * VersionNumberType.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of a comparable CCSI version number composed of up to
 * four integers separated by periods.
 *
 * Version: V1.0  15/08/06
 */


package com.domenix.ccsi;

/**
 * VersionNumberType that maintains the four part CCSI version number
 * and provides means to compare them including with wildcards for the
 * version elements.
 *
 * @author kmiller
 */
public class VersionNumberType implements Comparable<VersionNumberType>
{
  /** Wildcard value for comparisons. */
  public final static short WILDCARD_SEARCH = -1;

  //~--- fields ---------------------------------------------------------------

  /** Major version number */
  private short	major	= 0;

  /** Minor version number */
  private short	minor	= 0;

  /** Moderate (middle) version number */
  private short	moderate	= 0;

  /** Patch number */
  private short	patch	= 0;

  //~--- constructors ---------------------------------------------------------

  /**
   * Creates a new empty instance.
   */
  public VersionNumberType()
  {
  }

  /**
   * Creates a new instance of VersionNumberType using the first two digits
   *
   * @param maj - major version number
   * @param mod - minor version number
   */
  public VersionNumberType( int maj , int mod )
  {
    major = (short) maj;
    moderate = (short) mod;
  }

  /**
   * Creates a new instance of VersionNumberType using the first two digits
   *
   * @param maj - major version
   * @param mod - moderate version number
   */
  public VersionNumberType( short maj , short mod )
  {
    major = maj;
    moderate = mod;
  }

  /**
   * Constructs a new instance using the first three digits
   *
   * @param maj - major version
   * @param mod - moderate version number
   * @param min - minor version number
   */
  public VersionNumberType( int maj , int mod , int min )
  {
    major = (short) maj;
    moderate = (short) mod;
    minor = (short) min;
  }

  /**
   * Constructs a new instance using the first three digits
   *
   * @param maj - major version
   * @param mod - moderate version number
   * @param min - minor version number
   */
  public VersionNumberType( short maj , short mod , short min )
  {
    major = maj;
    moderate = mod;
    minor = min;
  }

  /**
   * Constructs a new instance using all four digits
   *
   * @param maj - major version
   * @param mod - moderate version number
   * @param min - minor version number
   * @param pat - patch number
   */
  public VersionNumberType( int maj , int mod , int min , int pat )
  {
    major = (short) maj;
    moderate = (short) mod;
    minor = (short) min;
    patch = (short) pat;
  }

  /**
   * Constructs a new instance using all four digits
   *
   * @param maj - major version
   * @param mod - moderate version number
   * @param min - minor version number
   * @param pat - patch number
   */
  public VersionNumberType( short maj , short mod , short min , short pat )
  {
    major = maj;
    moderate = mod;
    minor = min;
    patch = pat;
  }
  
  public VersionNumberType( String nbr )
  {
    int partCounter = 0;
    String[] parts = nbr.split( "\\." );
    for ( String x : parts )
    {
      if ( x != null )
      {
        switch( partCounter )
        {
          case 0:
            this.major = Short.parseShort( x );
            partCounter++;
            break;
            
          case 1:
            this.moderate = Short.parseShort( x );
            partCounter++;
            break;
            
          case 2:
            this.minor = Short.parseShort( x );
            partCounter++;
            break;
            
          case 3:
            this.patch = Short.parseShort( x );
            partCounter++;
            break;
            
          default:
              // can't ever get here
              break;
        }
      }
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Increments the major version number.
   */
  public void incrementMajor()
  {
    setMajor( (short) ( getMajor() + 1 ) );
  }

  /**
   * Increments the moderate version number.
   */
  public void incrementModerate()
  {
    setModerate( (short) ( getModerate() + 1 ) );
  }

  /**
   * Increments the minor version number
   */
  public void incrementMinor()
  {
    setMinor( (short) ( getMinor() + 1 ) );
  }

  /**
   * Increments the patch number
   */
  public void incrementPatch()
  {
    setPatch( (short) ( getPatch() + 1 ) );
  }

  /**
   * Returns the current object state as a string of the form Vd.d.d.d
   *
   * @return String containing the object state
   */
  @Override
  public String toString()
  {
    StringBuilder	msg	= new StringBuilder();

    msg.append( Short.toString( getMajor() ) );
    msg.append( '.' );
    msg.append( Short.toString( getModerate() ) );
    msg.append( '.' );
    msg.append( Short.toString( getMinor() ) );
    msg.append( '.' );
    msg.append( Short.toString( getPatch() ) );

    return ( msg.toString() );
  }

  /**
   * Compares two version numbers and returns an integer indication of whether
   * the input parameter is less than (-1), equal to (0), or greater than (+1)
   * this object.  The moderate, minor, and patch fields of the input can be set the to
   * wildcard search value WILDCARD_SEARCH to force a match on those fields.
   *
   * @param obj VersionNumberType to compare
   *
   * @return integer containing -1, 0, or +1 indicating the parameter is less than, equal, or greater than this object
   */
  @Override
  public int compareTo( VersionNumberType obj )
  {
    int	retValue	= 0;

    if ( obj.getMajor() != getMajor() )
    {
      if ( obj.getMajor() < getMajor() )
      {
        retValue	= -1;
      }
      else
      {
        retValue	= 1;
      }
    }
    else if ( ( obj.getModerate() != getModerate() ) && ( obj.getModerate() != WILDCARD_SEARCH ) )
    {
      if ( obj.getModerate() < getModerate() )
      {
        retValue	= -1;
      }
      else
      {
        retValue	= 1;
      }
    }
    else if ( ( obj.getMinor() != getMinor() ) && ( obj.getMinor() != WILDCARD_SEARCH ) )
    {
      if ( obj.getMinor() < getMinor() )
      {
        retValue	= -1;
      }
      else
      {
        retValue	= 1;
      }
    }
    else if ( ( obj.getPatch() != getPatch() ) && ( obj.getPatch() != WILDCARD_SEARCH ) )
    {
      if ( obj.getPatch() < getPatch() )
      {
        retValue	= -1;
      }
      else
      {
        retValue	= 1;
      }
    }

    return ( retValue );
  }

  /**
   * Compares two version number types and returns an indication of equality.
   *
   * @param obj VersionNumberType to be compared to this
   *
   * @return boolean indicating equality (true) or not (false)
   */
  @Override
  public boolean equals( Object obj )
  {
    if ( obj instanceof VersionNumberType )
    {
      if ( this.compareTo( (VersionNumberType) obj ) != 0 )
      {
        return ( false );
      }

      return ( true );
    }
    else
    {
      return ( false );
    }
  }

  /**
   * Computes the hash code for the this object.
   *
   * @return int containing the hash code
   */
  @Override
  public int hashCode()
  {
    return ( getPatch() + ( getMinor() * 100 ) + ( getModerate() * 1000 ) + ( getMajor() * 10000 ) );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the major
   */
  public short getMajor()
  {
    return major;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param major the major to set
   */
  public void setMajor( short major )
  {
    this.major	= major;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the minor
   */
  public short getMinor()
  {
    return minor;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param minor the minor to set
   */
  public void setMinor( short minor )
  {
    this.minor	= minor;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the moderate
   */
  public short getModerate()
  {
    return moderate;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param moderate the moderate to set
   */
  public void setModerate( short moderate )
  {
    this.moderate	= moderate;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the patch
   */
  public short getPatch()
  {
    return patch;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param patch the patch to set
   */
  public void setPatch( short patch )
  {
    this.patch	= patch;
  }
}
