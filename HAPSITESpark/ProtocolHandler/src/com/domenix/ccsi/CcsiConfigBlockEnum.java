/*
 * CcsiConfigBlockEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains CCSI Configuration Block definations
 *
 * Version: V1.0  15/10/12
 */


package com.domenix.ccsi;

/**
 * This file contains CCSI Configuration Block definitions
 * 
 * @author kmiller
 */
public enum CcsiConfigBlockEnum
{
  BASE , LOCAL , ANNUN , COMM , GENERAL , SECURITY , TAMPER , UNIQUE , OPERATORS , LINKS , SENSDESC;
}
