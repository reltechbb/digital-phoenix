/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * This package provides capabilities for reading, updating and writing the CCSI configuration information and the
 * sensor saved state information for a CCSI compliant sensor.
 *
 */
package com.domenix.ccsi.config;