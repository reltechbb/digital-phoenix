/*
 * ConfigComponentInfo.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This class encapsulates presence and BIT status of each of the sensor's CCSI components.
 *
 * Version: V1.0  15/10/06
 */


package com.domenix.ccsi.config;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.ccsi.CcsiBitStatusEnum;
import com.domenix.ccsi.CcsiComponentEnum;
import com.domenix.ccsi.CcsiComponentStateEnum;
import com.domenix.ccsi.CcsiScNameType;
import com.domenix.ccsi.CcsiWccNameType;
import java.util.Date;

//~--- classes ----------------------------------------------------------------

/**
 * This class encapsulates presence and BIT status of each of the sensor's CCSI components.
 * @author kmiller
 */
public class ConfigComponentInfo
{
  /** Last BIT time */
  private Date	compBitTime	= null;

  /** Type of component */
  private CcsiComponentEnum	compKind	= null;

  /** Result of last BIT */
  private CcsiBitStatusEnum	compResult	= null;

  /** If SC component, the component name */
  private CcsiScNameType	compSCName	= null;

  /** Component state */
  private CcsiComponentStateEnum	compStatus	= null;

  /** If WCC component, the component name */
  private CcsiWccNameType	compWCCName	= null;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an empty class instance
   */
  public ConfigComponentInfo()
  {
  }

  /**
   * Constructs .a class instance with a type
   *
   * @param comp the type of component
   */
  public ConfigComponentInfo( CcsiComponentEnum comp )
  {
    this.compKind	= comp;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the compKind
   */
  public CcsiComponentEnum getCompKind()
  {
    return compKind;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param compKind the compKind to set
   */
  public void setCompKind( CcsiComponentEnum compKind )
  {
    this.compKind	= compKind;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the compSCName
   */
  public CcsiScNameType getCompSCName()
  {
    return compSCName;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param compSCName the compSCName to set
   */
  public void setCompSCName( CcsiScNameType compSCName )
  {
    this.compSCName	= compSCName;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the compWCCName
   */
  public CcsiWccNameType getCompWCCName()
  {
    return compWCCName;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param compWCCName the compWCCName to set
   */
  public void setCompWCCName( CcsiWccNameType compWCCName )
  {
    this.compWCCName	= compWCCName;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the compStatus
   */
  public CcsiComponentStateEnum getCompStatus()
  {
    return compStatus;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param compStatus the compStatus to set
   */
  public void setCompStatus( CcsiComponentStateEnum compStatus )
  {
    this.compStatus	= compStatus;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the compBitTime
   */
  public Date getCompBitTime()
  {
    Date retVal = null;
    if (compBitTime != null)
    {
        retVal = (Date) compBitTime.clone ();
    }
    return retVal;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param compBitTime the compBitTime to set
   */
  public void setCompBitTime( Date compBitTime )
  {
    if (compBitTime != null)
    {
        this.compBitTime = (Date) compBitTime.clone();
    } else
    {
        this.compBitTime = null;
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the compResult
   */
  public CcsiBitStatusEnum getCompResult()
  {
    return compResult;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param compResult the compResult to set
   */
  public void setCompResult( CcsiBitStatusEnum compResult )
  {
    this.compResult	= compResult;
  }
}
