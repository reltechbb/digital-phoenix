/*
 * CcsiSavedState.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the saved state for a CCSI sensor.
 *
 * Version: V1.0  15/08/20
 */
package com.domenix.ccsi.config;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.ccsi.CcsiBitStatusEnum;
import com.domenix.ccsi.CcsiComponentEnum;
import com.domenix.ccsi.CcsiComponentStateEnum;
import com.domenix.ccsi.CcsiEmconModeEnum;
import com.domenix.ccsi.CcsiLinkNameType;
import com.domenix.ccsi.CcsiLinkTypeEnum;
import com.domenix.ccsi.CcsiLocationReportOptionEnum;
import com.domenix.ccsi.CcsiLocationSourceEnum;
import com.domenix.commonha.ccsi.CcsiModeEnum;
import com.domenix.ccsi.CcsiScNameType;
import com.domenix.ccsi.CcsiSensorMountingEnum;
import com.domenix.ccsi.CcsiTimeSourceEnum;
import com.domenix.ccsi.CcsiWccNameType;
import com.domenix.common.spark.CtlWrapperQueue;
import com.domenix.ccsi.cache.CcsiCacheManager;
import com.domenix.ccsi.protocol.CcsiPendingAckList;
import com.domenix.ccsi.protocol.PendingNakDetailList;
import com.domenix.commonha.queues.LinkEventQueue;
import com.domenix.commonha.queues.NSDSXferEventQueue;
import com.domenix.commonha.queues.ConnEventQueue;
import com.domenix.commonha.intfc.NSDSConnectionList;
import com.domenix.commonha.intfc.NSDSInterface;
import com.domenix.commonha.intfc.HostProtocolAdapterInterface;
import com.domenix.common.spark.IPCWrapperQueue;
import com.domenix.common.spark.data.PowerModeEnum;
import com.domenix.common.utils.NumberFormatter;
import com.domenix.common.utils.UtilTimer;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.commonsa.interfaces.SIAConnectionList;
import com.domenix.commonsa.interfaces.SIAXferEventQueue;
import com.domenix.commonsa.interfaces.SensorNetworkSettings;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.net.InetAddress;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

//~--- classes ----------------------------------------------------------------
/**
 * This class is the container for the contents of a CCSI saved sensor state file. It provides
 * methods for reading and writing the file, accessing the saved state parameters, and for updating
 * the parameters.
 *
 * @author kmiller
 */
public class CcsiSavedState {

    /**
     * The base CCSI sensor saved state file path
     */
    private String baseFile = null;

    //~--- fields ---------------------------------------------------------------
    /**
     * The alert ID generator
     */
    private final AtomicLong alertIDGenerator = new AtomicLong(1L);

    /**
     * The heartbeat rate.
     */
    private int heartbeat = 60;

    /**
     * Sensing is or is not reported (Start_Stop command)
     */
    private boolean sensingStopped = false;

    /**
     * The assigned host sensor description.
     */
    private String hostSensorDescription = "";

    /**
     * The assigned host sensor identification.
     */
//    private String hostSensorID = "";

    /**
     * The assigned host sensor name.
     */
    private String hostSensorName = "UnknownSensor";

    /**
     * The read ID generator
     */
    private final AtomicInteger readingIDGenerator = new AtomicInteger(999000000);

    /**
     * The sensor location altitude
     */
    private double sensorAlt = 0.0;

    /**
     * The sensor location latitude
     */
    private double sensorLat = 0.0;

    /**
     * The sensor location longitude
     */
    private double sensorLon = 0.0;

    /**
     * The compression threshold value
     */
    private int stateLinkCompressThreshold = 4096;

    /**
     * The type of link in use.
     */
    private CcsiLinkTypeEnum stateLinkType = CcsiLinkTypeEnum.ETHRNT;

    /**
     * Our Open Interface
     */
    private NSDSInterface theOpenInterface = null;

    private HostProtocolAdapterInterface theHPAInterface = null;

    /**
     * The properties.
     */
    private final Properties theProperties = new Properties();

    /**
     * The name of the link in use.
     */
    private CcsiLinkNameType stateLinkName = new CcsiLinkNameType("W001");

    /**
     * Encryption state of the link.
     */
//    private boolean stateLinkEncrypt = false;

    /**
     * Compression state of the link.
     */
    private boolean stateLinkCompress = false;

    /**
     * Is this link high bandwidth.
     */
    private boolean stateLinkBWHigh = true;

    /**
     * Error/Debug logger.
     */
    private final Logger myLogger = Logger.getLogger(CcsiSavedState.class);

    /**
     * Current sensor mode.
     */
//    private CcsiModeEnum mode = CcsiModeEnum.I;

    /**
     * Current sensor mounting.
     */
    private CcsiSensorMountingEnum hostSensorMounting = CcsiSensorMountingEnum.REMOTE;

    /**
     * Current sensor emissions control mode.
     */
    private CcsiEmconModeEnum emcon = CcsiEmconModeEnum.NONE;

    /**
     * WCC component state.
     */
    private CcsiComponentStateEnum componentWCCStatus = CcsiComponentStateEnum.N;

    /**
     * WCC component name
     */
    private CcsiWccNameType componentWCCName = new CcsiWccNameType("W001");

    /**
     * Time of last WCC BIT.
     */
    private Date componentWCCBitTime = new Date();

    /**
     * Result of last WCC BIT.
     */
    private CcsiBitStatusEnum componentWCCBitStatus = CcsiBitStatusEnum.PASS;

    /**
     * The WCC component.
     */
    private CcsiComponentEnum componentWCC = CcsiComponentEnum.WCC;

    /**
     * UIC component state.
     */
    private CcsiComponentStateEnum componentUICStatus = CcsiComponentStateEnum.N;

    /**
     * Time of last UIC BIT.
     */
    private Date componentUICBitTime = new Date();

    /**
     * Result of last UIC BIT.
     */
    private CcsiBitStatusEnum componentUICBitStatus = CcsiBitStatusEnum.PASS;

    /**
     * The UIC component.
     */
    private CcsiComponentEnum componentUIC = CcsiComponentEnum.UIC;

    /** list of sensing components */
    private final List <ComponentSC> componentSCList = new ArrayList <>();
    
    /**
     * PDIL component state.
     */
    private CcsiComponentStateEnum componentPDILStatus = CcsiComponentStateEnum.N;

    /**
     * Time of last PDIL BIT.
     */
    private Date componentPDILBitTime = new Date();

    /**
     * Result of last PDIL BIT.
     */
    private CcsiBitStatusEnum componentPDILBitStatus = CcsiBitStatusEnum.PASS;

    /**
     * The PDIL component.
     */
    private CcsiComponentEnum componentPDIL = CcsiComponentEnum.PDIL;

    /**
     * DPC component state.
     */
    private CcsiComponentStateEnum componentDPCStatus = CcsiComponentStateEnum.N;

    /**
     * Time of last DPC BIT.
     */
    private Date componentDPCBitTime = new Date();

    /**
     * Result of last DPC BIT.
     */
    private CcsiBitStatusEnum componentDPCBitStatus = CcsiBitStatusEnum.PASS;

    /**
     * The DPC component.
     */
    private CcsiComponentEnum componentDPC = CcsiComponentEnum.DPC;

    /**
     * The CC component state.
     */
    private CcsiComponentStateEnum componentCCStatus = CcsiComponentStateEnum.N;

    /**
     * Time of the last CC BIT.
     */
    private Date componentCCBitTime = new Date();

    /**
     * Result of the last CC BIT.
     */
    private CcsiBitStatusEnum componentCCBitStatus = CcsiBitStatusEnum.PASS;

    /**
     * The CC component.
     */
    private CcsiComponentEnum componentCC = CcsiComponentEnum.CC;

    /**
     * The current time source.
     */
    private CcsiTimeSourceEnum timesource = CcsiTimeSourceEnum.internal;

    /**
     * The current selected time source.
     */
    private CcsiTimeSourceEnum seltimesource = CcsiTimeSourceEnum.internal;

    /**
     * Sensor state inoperable
     */
    private boolean stateSensorInop = false;

    /**
     * Sensor state reading affected
     */
    private boolean stateReadingAffected = false;

    /**
     * Sensor state power status
     */
    private PowerModeEnum statePwrStatus = PowerModeEnum.AC;

    /**
     * Sensor state needs maintenance
     */
    private boolean stateMaintReq = false;

    /**
     * Sensor state in degraded mode
     */
    private boolean stateDegraded = false;

    /**
     * The current location source.
     */
    private CcsiLocationSourceEnum locationsource = CcsiLocationSourceEnum.none;

    /**
     * The current selected location source.
     */
    private CcsiLocationSourceEnum sellocationsource = CcsiLocationSourceEnum.none;

    /**
     * The reporting option for locations.
     */
    private CcsiLocationReportOptionEnum locationreportoption = CcsiLocationReportOptionEnum.NONE;

    /**
     * The sensor configuration
     */
    private CcsiConfigSensor cfg;

    /**
     * Component list
     */
    private final ArrayList<ConfigComponentInfo> componentList;

    /**
     * The Control queue for the sensor adapter
     */
    private ConnEventQueue connEventQueue;

    /**
     * Control Queue for the Host Protocol Adapter
     */
    private ConnEventQueue hpaConnEventQueue;

    /**
     * The list of active logical connections
     */
    private final NSDSConnectionList connList;
    
    private final SIAConnectionList sensorConnectionList;

    
    
    /**
     * The current controlling host IP address.
     */
//    private InetAddress hostIp = InetAddress.getByName("127.0.0.1");

    /**
     * The current controlling host port number.
     */
//    private int hostPort = 5647;

    /**
     * The Control queue for main (start and shutdown from the sensor go here.
     */
    private CtlWrapperQueue mainCtlQ;

    /**
     * List of pending NAK details
     */
    private final PendingNakDetailList nakDetails;

    /**
     * The Control queue for NSDS
     */
    private CtlWrapperQueue nsdsCtlQ;
    private CtlWrapperQueue hpaCtlQ;

    /**
     * Link events
     */
    private LinkEventQueue nsdsLinkEventQ;

    /**
     * List of parse errors for CCSI messages.
     */
    private final ArrayList<String> parseErrors;

    /**
     * The list of pending acknowledgment
     */
    private CcsiPendingAckList pendingAcks;

    /**
     * The Control queue for the protocol handler
     */
    private CtlWrapperQueue phCtlQ;

    /**
     * The queue for messages from the protocol handler to the sensor adapter
     */
    private IPCWrapperQueue phToSAMsgQ;

    /**
     * The Control queue for the sensor adapter
     */
    private CtlWrapperQueue saCtlQ;

    /**
     * The queue for messages from the protocol handler to the sensor protocol adapter
     */
    private IPCWrapperQueue saReaderDataQ;

    /**
     * The queue for messages from the sensor adapter to the protocol handler
     */
    private IPCWrapperQueue saToPhMsgQ;

    /**
     * The queue for messages from the protocol handler to the sensor protocol adapter
     */
    private IPCWrapperQueue saWriterDataQ;

    /**
     * A File reference to the schema file for RabbitMQ operations
     */
    private File schemaFileInternal;

    /**
     * The Cache Manager
     */
    private CcsiCacheManager theCacheMgr;

    /**
     * The timer utility reference
     */
    private UtilTimer timerBase;

    /**
     * The transfer event queue
     */
    private NSDSXferEventQueue hiaTransferEventQueue;

    /**
     * Queue for transfer messages from the HPA to the PH
     */
    private NSDSXferEventQueue hpaXferEventQueue;

        /**
     * The transfer event queue
     */
    private SIAXferEventQueue siaXferEventQueue;

    /**
     * Queue for transfer messages from the HPA to the PH
     */
    private SIAXferEventQueue spaXferEventQueue;
    
    
    /**
     * Settings of the host network
     */
    private HostNetworkSettings hostNetworkSettings;
    private SensorNetworkSettings sensorNetworkSettings;
    
    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs the class instance by reading the CCSI saved state file
     * (${user.home}/saved-state.xml
     *
     * @param config the sensor configuration
     * @param baseFile
     *
     * @throws Exception - any exception thrown by the reader/processor is propagated
     */
    public CcsiSavedState(CcsiConfigSensor config, String baseFile) throws Exception {
        this.componentList = new ArrayList<>();
        this.baseFile = baseFile;
        this.cfg = config;
        hostNetworkSettings = config.getHostNetworkSettings();
        sensorNetworkSettings = config.getSensorNetworkSettings();
        this.readSavedState();
        this.pendingAcks = new CcsiPendingAckList(cfg, this);
        this.parseErrors = new ArrayList<>();
        this.nakDetails = new PendingNakDetailList();
        this.connList = new NSDSConnectionList();
        this.sensorConnectionList = new SIAConnectionList();
    }

    //~--- methods --------------------------------------------------------------
    /**
     * This method reads the saved state file
     *
     * @throws Exception
     */
    private void readSavedState() throws Exception {
        String fName = "";

        try {
            fName = baseFile + "/saved-state.xml";

            File temp = new File(fName);

            if (temp.exists()) {
                FileInputStream fis = new FileInputStream(fName);

                theProperties.loadFromXML(fis);

                ConfigComponentInfo ccInfo = null;
                ConfigComponentInfo wccInfo = null;
                ConfigComponentInfo dpcInfo = null;
                ConfigComponentInfo uicInfo = null;
                ConfigComponentInfo pdilInfo = null;

                for (String x : theProperties.stringPropertyNames()) {
                    String value = theProperties.getProperty(x);

                    myLogger.debug("Saved State Key: " + x);
                    myLogger.debug("Saved State Value: " + value);

                    switch (x) {
                        case "ccsi.state.reading.id":
                            try {
                                int tempRdgId = Integer.parseInt(value);

                                this.readingIDGenerator.set(tempRdgId);
                            } catch (NumberFormatException nfx) {
                                myLogger.error("Invalid reading ID value, using '999000000");
                                this.readingIDGenerator.set(999000000);
                            }

                            break;

                        case "ccsi.state.sensing.stopped":
                            try {
                                boolean stopped = Boolean.parseBoolean(value);
                                this.sensingStopped = stopped;
                            } catch (Exception ssEx) {
                                myLogger.error("Exception parsing sensing stopped.", ssEx);
                                this.sensingStopped = false;
                                this.theProperties.put("ccsi.state.sensing.stopped", "false");
                            }
                            break;

                        case "ccsi.state.link.compress.threshold":
                            try {
                                this.stateLinkCompressThreshold = Integer.parseInt(value);

                                if ((this.stateLinkCompressThreshold < 0) || (this.stateLinkCompressThreshold > 32768)) {
                                    myLogger.error("Invalid compression threshold, must be >=0 and <= 32768");
                                    this.stateLinkCompressThreshold = 4096;
                                    this.theProperties.put("ccsi.state.link.compress.threshold", "4096");
                                }
                            } catch (NumberFormatException compTholdEx) {
                                myLogger.error("Exception parsing compression threshold, defaulting to 4K bytes",
                                        compTholdEx);
                                this.stateLinkCompressThreshold = 4096;
                                this.theProperties.put("ccsi.state.link.compress.threshold", "4096");
                            }

                            break;

                        case "ccsi.state.mode":
                            try {
                                hostNetworkSettings.setMode(CcsiModeEnum.valueOf(value));
//TODO:   Do I have to do something with the Sensor Settings, here????????????                             
//                                sensorNetworkSettings.setMode(CcsiModeEnum.valueOf(value));
//TODO END
                            } catch (Exception modeEx) {
                                myLogger.error("Exception parsing sensor mode, defaulting to NORMAL", modeEx);
                                hostNetworkSettings.setMode(CcsiModeEnum.N);
//TODO:   Do I have to do something with the Sensor Settings, here???????????? 
//                                sensorNetworkSettings.setMode(CcsiModeEnum.N);
//TODO END
                                this.theProperties.put("sensor.state.mode", "N");
                            }

                            break;

                        case "ccsi.state.alertId":
                            try {
                                long tempID = Long.parseLong(value);

                                this.alertIDGenerator.set(tempID);
                            } catch (NumberFormatException nfx) {
                                myLogger.error("Invalid alert ID in saved state, using 1", nfx);
                                this.alertIDGenerator.set(1L);
                                this.theProperties.put("ccsi.state.alertId", "1");
                            }

                            break;

                        case "ccsi.state.emcon":
                            try {
                                this.emcon = CcsiEmconModeEnum.valueOf(value);
                            } catch (Exception emconEx) {
                                myLogger.error("Exception parsing EMCON mode, defaulting to NONE", emconEx);
                                this.emcon = CcsiEmconModeEnum.NONE;
                                this.theProperties.put("ccsi.state.emcon", "NONE");
                            }

                            break;

                        case "ccsi.state.component.cc":
                            try {
                                this.componentCC = CcsiComponentEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid type for CC, assuming CC", iax);
                                this.componentCC = CcsiComponentEnum.CC;
                            }

                            if (this.componentCCBitStatus != null) {
                                if (ccInfo == null) {
                                    ccInfo = new ConfigComponentInfo(CcsiComponentEnum.CC);
                                }

                                ccInfo.setCompResult(this.componentCCBitStatus);
                            }

                            break;

    

                        case "ccsi.state.component.cc.status":
                            try {
                                this.componentCCStatus = CcsiComponentStateEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid type for CC status, assuming N", iax);
                                this.componentCCStatus = CcsiComponentStateEnum.N;
                            }

                            if (ccInfo == null) {
                                ccInfo = new ConfigComponentInfo(CcsiComponentEnum.CC);
                            }

                            ccInfo.setCompStatus(this.componentCCStatus);

                            break;

                        case "ccsi.state.component.cc.lastbit":
                            this.componentCCBitTime = NumberFormatter.parseDateTime(value);

                            if (ccInfo == null) {
                                ccInfo = new ConfigComponentInfo(CcsiComponentEnum.CC);
                            }

                            ccInfo.setCompBitTime(this.componentCCBitTime);

                            break;

                        case "ccsi.state.component.cc.result":
                            try {
                                this.componentCCBitStatus = CcsiBitStatusEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid type for CC result, assuming PASS", iax);
                                this.componentCCBitStatus = CcsiBitStatusEnum.PASS;
                            }

                            if (ccInfo == null) {
                                ccInfo = new ConfigComponentInfo(CcsiComponentEnum.CC);
                            }

                            ccInfo.setCompResult(this.componentCCBitStatus);

                            break;

                        case "ccsi.state.component.pdil":
                            try {
                                this.componentPDIL = CcsiComponentEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid type for PDIL, assuming PDIL", iax);
                                this.componentPDIL = CcsiComponentEnum.PDIL;
                            }

                            if (pdilInfo == null) {
                                pdilInfo = new ConfigComponentInfo(CcsiComponentEnum.PDIL);
                            }

                            break;

                        case "ccsi.state.component.pdil.status":
                            try {
                                this.componentPDILStatus = CcsiComponentStateEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid type for PDIL status, assuming N", iax);
                                this.componentPDILStatus = CcsiComponentStateEnum.N;
                            }

                            if (pdilInfo == null) {
                                pdilInfo = new ConfigComponentInfo(CcsiComponentEnum.PDIL);
                            }

                            pdilInfo.setCompStatus(this.componentPDILStatus);

                            break;

                        case "ccsi.state.component.pdil.lastbit":
                            this.componentPDILBitTime = NumberFormatter.parseDateTime(value);

                            if (pdilInfo == null) {
                                pdilInfo = new ConfigComponentInfo(CcsiComponentEnum.PDIL);
                            }

                            pdilInfo.setCompBitTime(this.componentPDILBitTime);

                            break;

                        case "ccsi.state.component.pdil.result":
                            try {
                                this.componentPDILBitStatus = CcsiBitStatusEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid type for PDIL bit status, assuming PASS", iax);
                                this.componentPDILBitStatus = CcsiBitStatusEnum.PASS;
                            }

                            if (pdilInfo == null) {
                                pdilInfo = new ConfigComponentInfo(CcsiComponentEnum.PDIL);
                            }

                            pdilInfo.setCompResult(this.componentPDILBitStatus);

                            break;

                        case "ccsi.state.component.dpc":
                            try {
                                this.componentDPC = CcsiComponentEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid type for DPC, assuming DPC", iax);
                                this.componentDPC = CcsiComponentEnum.DPC;
                            }

                            if (dpcInfo == null) {
                                dpcInfo = new ConfigComponentInfo(CcsiComponentEnum.DPC);
                            }

                            dpcInfo.setCompKind(this.componentDPC);

                            break;

                        case "ccsi.state.component.dpc.status":
                            try {
                                this.componentDPCStatus = CcsiComponentStateEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid type for DPC status, assuming N", iax);
                                this.componentDPCStatus = CcsiComponentStateEnum.N;
                            }

                            if (dpcInfo == null) {
                                dpcInfo = new ConfigComponentInfo(CcsiComponentEnum.DPC);
                            }

                            dpcInfo.setCompKind(this.componentDPC);
                            this.componentDPCStatus = CcsiComponentStateEnum.valueOf(value);

                            break;

                        case "ccsi.state.component.dpc.lastbit":
                            this.componentDPCBitTime = NumberFormatter.parseDateTime(value);

                            if (dpcInfo == null) {
                                dpcInfo = new ConfigComponentInfo(CcsiComponentEnum.DPC);
                            }

                            dpcInfo.setCompBitTime(this.componentDPCBitTime);

                            break;

                        case "ccsi.state.component.dpc.result":
                            try {
                                this.componentDPCBitStatus = CcsiBitStatusEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid type for DPC bit result, assuming PASS", iax);
                                this.componentDPCBitStatus = CcsiBitStatusEnum.PASS;
                            }

                            if (dpcInfo == null) {
                                dpcInfo = new ConfigComponentInfo(CcsiComponentEnum.DPC);
                            }

                            dpcInfo.setCompResult(this.componentDPCBitStatus);

                            break;


                        case "ccsi.state.component.wcc":
                            try {
                                this.componentWCC = CcsiComponentEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid type for WCC, assuming WCC", iax);
                                this.componentWCC = CcsiComponentEnum.WCC;
                            }

                            if (this.componentWCC != null) {
                                if (wccInfo == null) {
                                    wccInfo = new ConfigComponentInfo(CcsiComponentEnum.WCC);
                                }

                                wccInfo.setCompKind(this.componentWCC);
                            }

                            break;

                        case "ccsi.state.component.wcc.status":
                            try {
                                this.componentWCCStatus = CcsiComponentStateEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid type for WCC status, assuming N", iax);
                                this.componentWCCStatus = CcsiComponentStateEnum.N;
                            }

                            if (this.componentWCCStatus != null) {
                                if (wccInfo == null) {
                                    wccInfo = new ConfigComponentInfo(CcsiComponentEnum.WCC);
                                }

                                wccInfo.setCompStatus(this.componentWCCStatus);
                            }

                            break;

                        case "ccsi.state.component.wcc.lastbit":
                            this.componentWCCBitTime = NumberFormatter.parseDateTime(value);

                            if (this.componentWCCBitTime != null) {
                                if (wccInfo == null) {
                                    wccInfo = new ConfigComponentInfo(CcsiComponentEnum.WCC);
                                }

                                wccInfo.setCompBitTime(this.componentWCCBitTime);
                            }

                            break;

                        case "ccsi.state.component.wcc.result":
                            try {
                                this.componentWCCBitStatus = CcsiBitStatusEnum.valueOf(value);
                            } catch (Exception iax) {
                                myLogger.error("Invalid bit result for WCC, assuming PASS", iax);
                                this.componentWCCBitStatus = CcsiBitStatusEnum.PASS;
                            }

                            if (this.componentWCCBitStatus != null) {
                                if (wccInfo == null) {
                                    wccInfo = new ConfigComponentInfo(CcsiComponentEnum.WCC);
                                }

                                wccInfo.setCompResult(this.componentWCCBitStatus);
                            }

                            break;

                        case "ccsi.state.component.wcc.name":
                            this.componentWCCName = new CcsiWccNameType(value);

                            if (this.componentWCCName != null) {
                                if (wccInfo == null) {
                                    wccInfo = new ConfigComponentInfo(CcsiComponentEnum.WCC);
                                }

                                wccInfo.setCompWCCName(this.componentWCCName);
                            }

                            break;

                        case "ccsi.state.component.uic":
                            try {
                                this.componentUIC = CcsiComponentEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid type for UIC, assuming UIC", iax);
                                this.componentUIC = CcsiComponentEnum.UIC;
                            }

                            if (this.componentUIC != null) {
                                if (uicInfo == null) {
                                    uicInfo = new ConfigComponentInfo(CcsiComponentEnum.UIC);
                                }

                                uicInfo.setCompKind(this.componentUIC);
                            }

                            break;

                        case "ccsi.state.component.uic.status":
                            try {
                                this.componentUICStatus = CcsiComponentStateEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid type for UIC status, assuming N", iax);
                                this.componentUICStatus = CcsiComponentStateEnum.N;
                            }

                            if (this.componentUICStatus != null) {
                                if (uicInfo == null) {
                                    uicInfo = new ConfigComponentInfo(CcsiComponentEnum.UIC);
                                }

                                uicInfo.setCompStatus(this.componentUICStatus);
                            }

                            break;

                        case "ccsi.state.component.uic.lastbit":
                            this.componentUICBitTime = NumberFormatter.parseDateTime(value);

                            if (this.componentUICBitTime != null) {
                                if (uicInfo == null) {
                                    uicInfo = new ConfigComponentInfo(CcsiComponentEnum.UIC);
                                }

                                uicInfo.setCompBitTime(this.componentUICBitTime);
                            }

                            break;

                        case "ccsi.state.component.uic.result":
                            try {
                                this.componentUICBitStatus = CcsiBitStatusEnum.valueOf(value);
                            } catch (IllegalArgumentException iax) {
                                myLogger.error("Invalid bit result for UIC, assuming PASS", iax);
                                this.componentUICBitStatus = CcsiBitStatusEnum.PASS;
                            }

                            if (this.componentUICBitStatus != null) {
                                if (uicInfo == null) {
                                    uicInfo = new ConfigComponentInfo(CcsiComponentEnum.UIC);
                                }

                                uicInfo.setCompResult(this.componentUICBitStatus);
                            }

                            break;

                        case "ccsi.state.host.sensor.id":
                            String id = value.trim();
                            hostNetworkSettings.setHostSensorID (id);
//                            this.hostSensorID = value.trim();

                            if ((id.length() < 0) || (id.length() > 63)) {
                                myLogger.error("Invalid host sensor ID length");
                            }

                            break;

                        case "ccsi.state.host.sensor.name":
                            this.hostSensorName = value.trim();

                            break;

                        case "ccsi.state.host.sensor.mounting":
                            try {
                                this.hostSensorMounting = CcsiSensorMountingEnum.valueOf(value);
                            } catch (Exception mntEx) {
                                myLogger.error("Exception parsing sensor mounting, assuming REMOTE", mntEx);
                                this.hostSensorMounting = CcsiSensorMountingEnum.REMOTE;
                                this.theProperties.put("ccsi.state.host.sensor.mounting", "REMOTE");
                            }

                            break;

                        case "ccsi.state.host.sensor.desc":
                            this.hostSensorDescription = value.trim();

                            break;

                        case "ccsi.state.host.heartbeat":
                            try {
                                this.heartbeat = Integer.parseInt(value);
                                if ((this.heartbeat < 5) && (this.heartbeat > 3600)) {
                                    myLogger.error("Invalid heartbeat rate, must be between 5 and 3600 inclusive, assuming 300");
                                    this.heartbeat = 300;
                                }
                            } catch (NumberFormatException hbEx) {
                                myLogger.error("Exception parsing sensor heartbeat, assuming 300", hbEx);
                                this.heartbeat = 300;
                                this.theProperties.put("ccsi.state.host.heartbeat", "300");
                            }

                            break;

                        case "ccsi.state.host.ip":
                            hostNetworkSettings.setIP(InetAddress.getByName(value));

                            break;

                        case "ccsi.state.host.port":
                            hostNetworkSettings.setPort (Integer.parseInt(value));
//TODO:   Do I need to save Sensor Network Settings, here???????????
//TODO END                            
                            break;

                        case "ccsi.state.timesource":
                            this.timesource = CcsiTimeSourceEnum.valueOf(value);

                            break;

                        case "ccsi.state.seltimesource":
                            this.seltimesource = CcsiTimeSourceEnum.valueOf(value);

                            break;

                        case "ccsi.state.locationsource":
                            this.locationsource = CcsiLocationSourceEnum.valueOf(value);

                            break;

                        case "ccsi.state.sellocationsource":
                            this.sellocationsource = CcsiLocationSourceEnum.valueOf(value);

                            break;

                        case "ccsi.state.locationreportoption":
                            this.locationreportoption = CcsiLocationReportOptionEnum.valueOf(value);

                            break;

                        case "ccsi.state.link.name":
                            try {
                                this.stateLinkName = new CcsiLinkNameType(value);
                            } catch (Exception lnkNamEx) {
                                myLogger.error("Invalid link name [" + value + "] defaulting to W001", lnkNamEx);
                                this.stateLinkName = new CcsiLinkNameType("W001");
                                this.theProperties.put("ccsi.state.link.name", "W001");
                            }

                            break;

                        case "ccsi.state.link.type":
                            try {
                                this.stateLinkType = CcsiLinkTypeEnum.valueOf(value);
                            } catch (Exception lnkTypEx) {
                                myLogger.error("Invalid link type [" + value + "] defaulting to XXX", lnkTypEx);
                                this.stateLinkType = CcsiLinkTypeEnum.ETHRNT;
                                this.theProperties.put("ccsi.state.link.type", "ETHRNT");
                            }

                            break;

                        case "ccsi.state.link.bwhigh":
                            this.stateLinkBWHigh = Boolean.parseBoolean(value);

                            break;

                        case "ccsi.state.link.encrypt":
                            hostNetworkSettings.setEncryptComm(Boolean.parseBoolean(value));

                            break;

                        case "ccsi.state.link.compress":
                            this.stateLinkCompress = Boolean.parseBoolean(value);

                            break;

                        case "sensor.state.degraded":
                            this.stateDegraded = Boolean.parseBoolean(value);

                            break;

                        case "sensor.state.maintreq":
                            this.stateMaintReq = Boolean.parseBoolean(value);

                            break;

                        case "sensor.state.readingaffected":
                            this.stateReadingAffected = Boolean.parseBoolean(value);

                            break;

                        case "sensor.state.inop":
                            this.stateSensorInop = Boolean.parseBoolean(value);

                            break;

                        case "sensor.state.pwrstatus":
                            try {
                                this.statePwrStatus = PowerModeEnum.valueOf(value);
                            } catch (Exception pwrEx) {
                                myLogger.error("Invalid power status [" + value + "] defaulting to AC", pwrEx);
                                this.statePwrStatus = PowerModeEnum.AC;
                                this.theProperties.put("ccsi.state.pwrstatus", "AC");
                            }

                            break;

                        case "ccsi.state.loc.lat":
                            try {
                                this.sensorLat = Double.parseDouble(value);

                                if ((this.sensorLat < -90.0) && (this.sensorLat > 90.0)) {
                                    myLogger.error("Invalid sensor latitude " + value);
                                    this.sensorLat = 0.0;
                                }
                            } catch (NumberFormatException nfe) {
                                myLogger.error("Invalid sensor latitude " + value);
                                this.sensorLat = 0.0;
                            }

                            break;

                        case "ccsi.state.loc.lon":
                            try {
                                this.sensorLon = Double.parseDouble(value);

                                if ((this.sensorLon < -180.0) && (this.sensorLon > 180.0)) {
                                    myLogger.error("Invalid sensor longitude " + value);
                                    this.sensorLon = 0.0;
                                }
                            } catch (NumberFormatException nfe) {
                                myLogger.error("Invalid sensor longitude " + value);
                                this.sensorLon = 0.0;
                            }

                            break;

                        case "ccsi.state.loc.alt":
                            try {
                                this.sensorAlt = Double.parseDouble(value);

                                if ((this.sensorAlt < -10000.0) || (this.sensorAlt > 65535.0)) {
                                    myLogger.error("Invalid sensor altitude " + value);
                                    this.sensorAlt = 0.0;
                                }
                            } catch (NumberFormatException nfe) {
                                myLogger.error("Invalid sensor altitude " + value);
                                this.sensorAlt = 0.0;
                            }

                            break;

                        default:
                            if (x.startsWith("ccsi.state.component.sc.result"))
                            {
                                ComponentSC component = getComponentID(x);
                                try {
                                    component.setBitStatus(CcsiBitStatusEnum.valueOf(value));
                                } catch (IllegalArgumentException iax) {
                                    myLogger.error("Invalid type for SC bit result, assuming PASS", iax);
                                    component.setBitStatus(CcsiBitStatusEnum.PASS);
                                }
                            } else if (x.startsWith("ccsi.state.component.sc.name"))
                            {
                                ComponentSC component = getComponentID (x);
                                try {
                                    component.setSCName(new CcsiScNameType(value));
                                } catch (IllegalArgumentException badSC) {
                                    myLogger.error("Invalid sensing component name, defaulting to SC001", badSC);
                                    component.setSCName(new CcsiScNameType("SC001"));
                                }
                            } else if (x.startsWith("ccsi.state.component.sc.status"))
                            {
                                ComponentSC component = getComponentID(x);
                                try {
                                    component.setComponentState(CcsiComponentStateEnum.valueOf(value));
                                } catch (IllegalArgumentException iax) {
                                    myLogger.error("Invalid type for SC status, assuming N", iax);
                                    component.setComponentState(CcsiComponentStateEnum.N);
                                }
                            } else if (x.startsWith("ccsi.state.component.sc.lastbit")) {
                                ComponentSC component = getComponentID(x);
                                component.setBitDate(NumberFormatter.parseDateTime(value));
                            } else if (x.startsWith("ccsi.state.component.sc")) {
                                ComponentSC component = getComponentID(x);
                                try {
                                    component.setComponentType(CcsiComponentEnum.valueOf(value));
                                } catch (IllegalArgumentException iax) {
                                    myLogger.error("Invalid type for SC, assuming SC", iax);
                                    component.setComponentType(CcsiComponentEnum.SC);
                                }
                            } else {
                                myLogger.error("Unknown saved state file entry [" + x + "] found.");
                            }
                            break;
                    }

                    if (ccInfo != null) {
                        this.componentList.add(ccInfo);
                    } else if (pdilInfo != null) {
                        this.componentList.add(pdilInfo);
                    }

                    if (dpcInfo != null) {
                        this.componentList.add(dpcInfo);
                    }

                    if (wccInfo != null) {
                        this.componentList.add(wccInfo);
                    }

                    if (uicInfo != null) {
                        this.componentList.add(uicInfo);
                    }

                }
                for (ComponentSC item : componentSCList) {
                    ConfigComponentInfo info = new ConfigComponentInfo(CcsiComponentEnum.SC);
                    info.setCompSCName(item.getSCName());
                    info.setCompBitTime(item.getBitDate());
                    info.setCompResult(item.getBitStatus());
                    componentList.add(info);
                }
            } else
            {
                // saved state didn't exist. create it
                createSavedState ();
            }
        } catch (IOException | NumberFormatException ex) {
            myLogger.fatal("Cannot read saved state file: " + fName, ex);

            throw (ex);
        }
    }

    /**
     * Deletes the Saved State file
     *
     * @return
     */
    public boolean deleteSavedState() {
        String fName = baseFile + "/saved-state.xml";

        File temp = new File(fName);
        boolean result = temp.delete();
        return result;
    }

    /**
     * Sets all of the default values and saves then to a file
     */
    private void createSavedState ()
    {
        myLogger.warn("Saved State does not exist - creating one");
        ConfigComponentInfo ccInfo;
        hostNetworkSettings = cfg.getHostNetworkSettings();
        sensorNetworkSettings = cfg.getSensorNetworkSettings();
        
        for (SensorComponentItem x : cfg.getSensorComponentItemList())
        {
            ccInfo = new ConfigComponentInfo (x.getComponent());
            switch (ccInfo.getCompKind())
            {
                case CC:
                    this.setComponentCC(CcsiComponentEnum.CC);
                    this.setComponentCCStatus(CcsiComponentStateEnum.N);
                    ccInfo.setCompStatus(componentCCStatus);
                    this.setComponentCCBitTime(new Date ());
                    ccInfo.setCompBitTime(componentCCBitTime);
                    this.setComponentCCBitStatus(CcsiBitStatusEnum.PASS);
                    ccInfo.setCompResult(componentCCBitStatus);
                    componentList.add(ccInfo);
                    break;
                case PDIL:
                    this.setComponentPDIL(CcsiComponentEnum.PDIL);
                    this.setComponentPDILStatus(CcsiComponentStateEnum.F);
                    ccInfo.setCompStatus(componentPDILStatus);
                    this.setComponentPDILBitTime(new Date());
                    ccInfo.setCompBitTime(componentPDILBitTime);
                    this.setComponentPDILBitStatus(CcsiBitStatusEnum.FAIL);
                    ccInfo.setCompResult(componentPDILBitStatus);
                    componentList.add(ccInfo);
                    break;
                case SC:
                    ComponentSC component = new ComponentSC (x);
//                    this.setComponentSCName(new CcsiScNameType(x.getName()));
//                    this.setComponentSCStatus(component.getComponentState());
                    ccInfo.setCompStatus(component.getComponentState());
//                    this.setComponentSCBitTime(new Date());
                    ccInfo.setCompBitTime(component.getBitDate());
//                    this.setComponentPDILBitStatus(CcsiBitStatusEnum.FAIL);
                    ccInfo.setCompResult(component.getBitStatus());
                    componentList.add(ccInfo);
                    component.setProperties(theProperties);
                    componentSCList.add(component);
                    break;
                case WCC: // wireless communication component
                    this.setComponentWCC(CcsiComponentEnum.WCC);
                    this.setComponentWCCStatus(CcsiComponentStateEnum.F);
                    ccInfo.setCompStatus(componentWCCStatus);
                    this.setComponentWCCBitTime(new Date ());
                    ccInfo.setCompBitTime(componentWCCBitTime);
                    this.setComponentWCCBitStatus(CcsiBitStatusEnum.FAIL);
                    ccInfo.setCompResult(componentWCCBitStatus);
                    componentList.add(ccInfo);
                    break;
                case UIC: // user interface component
                    this.setComponentUIC(CcsiComponentEnum.UIC);
                    this.setComponentUICStatus(CcsiComponentStateEnum.F);
                    ccInfo.setCompStatus(componentUICStatus);
                    this.setComponentUICBitTime(new Date ());
                    ccInfo.setCompBitTime(componentUICBitTime);
                    this.setComponentUICBitStatus(CcsiBitStatusEnum.FAIL);
                    ccInfo.setCompResult(componentUICBitStatus);
                    componentList.add(ccInfo);
                    break;
                case DPC: // dismounted power component
                    this.setComponentDPC(CcsiComponentEnum.DPC);
                    this.setComponentDPCStatus(CcsiComponentStateEnum.F);
                    ccInfo.setCompStatus(componentDPCStatus);
                    this.setComponentDPCBitTime(new Date ());
                    ccInfo.setCompBitTime(componentDPCBitTime);
                    this.setComponentDPCBitStatus(CcsiBitStatusEnum.FAIL);
                    ccInfo.setCompResult(componentDPCBitStatus);
                    componentList.add(ccInfo);
                    break;
                default:
                    break;
            }
        }
        theProperties.put("sensor.state.degraded", Boolean.toString(stateDegraded));
        theProperties.put("ccsi.state.locationreportoption", locationreportoption.toString());
        theProperties.put("ccsi.state.host.heartbeat", Integer.toString(heartbeat));
//        stateLinkEncrypt = cfg.getHostNetworkSettings().getEncryptComm();
        theProperties.put("ccsi.state.link.encrypt", Boolean.toString(hostNetworkSettings.getEncryptComm()));
        String id = cfg.getSensorHostID();
        if (id == null)
        {
            id = cfg.getUuid();
            if (id == null)
            {
                id = hostNetworkSettings.getDiscoverySID();
                myLogger.warn("HostSensorID and UUID not set using: " + id);
            }
        }
//        hostNetworkSettings.setHostSensorID (id);
        theProperties.put("ccsi.state.host.sensor.id", hostNetworkSettings.getHostSensorIDOrUUID());
//TODO:    Do I have to do this for the Sensor?????????????????????????????????????
//TODO END
        theProperties.put("ccsi.state.link.compress", Boolean.toString(stateLinkCompress));
        theProperties.put("ccsi.state.loc.lon", Double.toString(sensorLon));
        theProperties.put("ccsi.state.loc.lat", Double.toString(sensorLat));
        theProperties.put("ccsi.state.loc.alt", Double.toString(sensorAlt));
        theProperties.put("sensor.state.readingaffected", Boolean.toString(stateReadingAffected));
        hostSensorMounting = cfg.getSensorMountingType ();
        theProperties.put("ccsi.state.host.sensor.mounting", hostSensorMounting.toString());
        theProperties.put("ccsi.state.seltimesource", seltimesource.toString());
        theProperties.put("ccsi.state.timesource", timesource.toString());
        theProperties.put("ccsi.state.sensing.stopped", Boolean.toString(sensingStopped));
        theProperties.put("ccsi.state.locationsource", locationsource.toString());
        theProperties.put("ccsi.state.link.bwhigh", Boolean.toString(stateLinkBWHigh));
        theProperties.put("ccsi.state.link.compress.threshold", Integer.toString(stateLinkCompressThreshold));
        theProperties.put("ccsi.state.alertId", Long.toString(this.alertIDGenerator.get()));
        theProperties.put("ccsi.state.reading.id", Integer.toString(this.readingIDGenerator.get()));
//        hostIp = cfg.getHostNetworkSettings().getIP();
        theProperties.put("ccsi.state.host.ip", hostNetworkSettings.getIP().getHostName());
//        hostPort = cfg.getHostNetworkSettings().getPort();
        theProperties.put("ccsi.state.host.port", Integer.toString(hostNetworkSettings.getPort()));
//TODO:   Do I have to do similar for the Sensor???????????????????????????????????????????????????????????????????????????
//TODO END
        theProperties.put("sensor.state.inop", Boolean.toString(stateSensorInop));
        theProperties.put("sensor.state.pwrstatus", statePwrStatus.toString());
        theProperties.put("sensor.state.maintreq", Boolean.toString(stateMaintReq));
        theProperties.put("ccsi.state.emcon", emcon.toString());
        hostSensorName = cfg.getSensorHostName();
        theProperties.put("ccsi.state.host.sensor.name", hostSensorName);
        theProperties.put("ccsi.state.mode", hostNetworkSettings.getMode().toString());
        theProperties.put("ccsi.state.link.name", stateLinkName.getLinkName());
        hostSensorDescription = cfg.getSensorHostDescription();
        theProperties.put("ccsi.state.host.sensor.desc", hostSensorDescription);
        try {
            writeSavedState ();
        } catch (Exception ex) {
            myLogger.error("Exception writing saved state", ex);
        }
    }
    
    /**
     * This writes to the sensor saved state file
     *
     * @throws Exception
     */
    public void writeSavedState() throws Exception {
        String fName = "";
        FileOutputStream fos = null;

        try {
            fName = baseFile + "/saved-state.xml";

            fos = new FileOutputStream(fName);
            theProperties.storeToXML(fos, "CCSI V1.2 Saved State - " + hostSensorName, "UTF-8");
            fos.close();
            fos = null;
        } catch (IOException foEx) {
            myLogger.fatal("Fatal exception trying to open, write or close saved state file: " + fName, foEx);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException fex) {
                    myLogger.fatal("Fatal exception trying to close saved state file: " + fName, fex);
                }
            }
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the mode
     */
    public CcsiModeEnum getMode() {
        return hostNetworkSettings.getMode();
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param mode the mode to set
     */
    public void setMode(CcsiModeEnum mode) {
        hostNetworkSettings.setMode(mode);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the emcon
     */
    public CcsiEmconModeEnum getEmcon() {
        return emcon;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param emcon the emcon to set
     */
    public void setEmcon(CcsiEmconModeEnum emcon) {
        this.emcon = emcon;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentCC
     */
    public CcsiComponentEnum getComponentCC() {
        return componentCC;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentCC the componentCC to set
     */
    public void setComponentCC(CcsiComponentEnum componentCC) {
        this.componentCC = componentCC;
        this.theProperties.setProperty("ccsi.state.component.cc", componentCC.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentCCStatus
     */
    public CcsiComponentStateEnum getComponentCCStatus() {
        return componentCCStatus;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentCCStatus the componentCCStatus to set
     */
    public void setComponentCCStatus(CcsiComponentStateEnum componentCCStatus) {
        this.componentCCStatus = componentCCStatus;
        this.theProperties.setProperty("ccsi.state.component.cc.status", componentCCStatus.name());
//        writeSavedState ();
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentCCBitTime
     */
    public Date getComponentCCBitTime() {
        Date retVal = null;
        if (componentCCBitTime != null)
        {
            retVal = (Date)componentCCBitTime.clone();
        }
        return retVal;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentCCBitTime the componentCCBitTime to set
     */
    public void setComponentCCBitTime(Date componentCCBitTime) {
        if (componentCCBitTime != null)
        {
            this.componentCCBitTime = (Date) componentCCBitTime.clone();
        } else
        {
            componentCCBitTime = null;
        }
        this.theProperties.setProperty("ccsi.state.component.cc.lastbit",
                NumberFormatter.getDate(componentCCBitTime, "yyyyMMddHHmmss"));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentCCBitStatus
     */
    public CcsiBitStatusEnum getComponentCCBitStatus() {
        return componentCCBitStatus;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentCCBitStatus the componentCCBitStatus to set
     */
    public void setComponentCCBitStatus(CcsiBitStatusEnum componentCCBitStatus) {
        this.componentCCBitStatus = componentCCBitStatus;
        this.theProperties.setProperty("ccsi.state.component.cc.result", componentCCBitStatus.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentPDIL
     */
    public CcsiComponentEnum getComponentPDIL() {
        return componentPDIL;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentPDIL the componentPDIL to set
     */
    public void setComponentPDIL(CcsiComponentEnum componentPDIL) {
        this.componentPDIL = componentPDIL;
        this.theProperties.setProperty("ccsi.state.component.pdil", componentPDIL.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentPDILStatus
     */
    public CcsiComponentStateEnum getComponentPDILStatus() {
        return componentPDILStatus;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentPDILStatus the componentPDILStatus to set
     */
    public void setComponentPDILStatus(CcsiComponentStateEnum componentPDILStatus) {
        this.componentPDILStatus = componentPDILStatus;
        this.theProperties.setProperty("ccsi.state.component.pdil.status", componentPDILStatus.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentPDILBitTime
     */
    public Date getComponentPDILBitTime() {
        Date retVal = null;
        if (componentPDILBitTime != null)
        {
            retVal = (Date) componentPDILBitTime.clone();
        }
        return retVal;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentPDILBitTime the componentPDILBitTime to set
     */
    public void setComponentPDILBitTime(Date componentPDILBitTime) {
        if (componentPDILBitTime != null)
        {
            this.componentPDILBitTime = (Date) componentPDILBitTime.clone();
        } else
        {
            componentPDILBitTime = null;
        }
        this.theProperties.setProperty("ccsi.state.component.pdil.lastbit",
                NumberFormatter.getDate(componentPDILBitTime, "yyyyMMddHHmmss"));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentPDILBitStatus
     */
    public CcsiBitStatusEnum getComponentPDILBitStatus() {
        return componentPDILBitStatus;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentPDILBitStatus the componentPDILBitStatus to set
     */
    public void setComponentPDILBitStatus(CcsiBitStatusEnum componentPDILBitStatus) {
        this.componentPDILBitStatus = componentPDILBitStatus;
        this.theProperties.setProperty("ccsi.state.component.pdil.result", componentPDILBitStatus.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentSCStatus
     */
//    public CcsiComponentStateEnum getComponentSCStatus() {
//        return componentSCStatus;
//    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentSCStatus the componentSCStatus
     * @param component to set
     */
    public void setComponentSCStatus(CcsiComponentStateEnum componentSCStatus, String component) {
        ComponentSC item = getComponentSC(component);
        item.setComponentState(componentSCStatus);
//        this.componentSCStatus = componentSCStatus;
        item.setProperties(theProperties);
//        this.theProperties.setProperty("ccsi.state.component.sc.status", componentSCStatus.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @param id - ID of SC component (i.e. SC001)
     * @return the componentSCBitTime
     */
    public Date getComponentSCBitTime(String id) {
        ComponentSC component = getComponentSC (id);
        return component.getBitDate();
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentSCBitTime the componentSCBitTime to set
     * @param component
     */
    public void setComponentSCBitTime(Date componentSCBitTime, String component) {
        ComponentSC item = getComponentSC(component);
//        this.componentSCBitTime = componentSCBitTime;
        item.setBitDate(componentSCBitTime);
        item.setProperties(theProperties);
//        this.theProperties.setProperty("ccsi.state.component.sc.lastbit",
//                NumberFormatter.getDate(componentSCBitTime, "yyyyMMddHHmmss"));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @param component i.e. SC001
     * @return the componentSCBitStatus
     */
    public CcsiBitStatusEnum getComponentSCBitStatus(String component) {
        ComponentSC item = getComponentSC(component);
        
        return item.getBitStatus();
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentSCBitStatus the componentSCBitStatus to set
     * @param component
     */
    public void setComponentSCBitStatus(CcsiBitStatusEnum componentSCBitStatus, String component) {
        ComponentSC item = getComponentSC (component);
        item.setBitStatus(componentSCBitStatus);
//        this.componentSCBitStatus = componentSCBitStatus;
//        this.theProperties.setProperty("ccsi.state.component.sc.result", componentSCBitStatus.name());
        item.setProperties (theProperties);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the hostSensorMounting
     */
    public CcsiSensorMountingEnum getHostSensorMounting() {
        return hostSensorMounting;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param hostSensorMounting the hostSensorMounting to set
     */
    public void setHostSensorMounting(CcsiSensorMountingEnum hostSensorMounting) {
        this.hostSensorMounting = hostSensorMounting;
        this.theProperties.setProperty("ccsi.state.host.sensor.mounting", hostSensorMounting.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the hostSensorID
     */
    public String getHostSensorID() {
        return hostNetworkSettings.getHostSensorID();
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param hostSensorID the hostSensorID to set
     */
    public void setHostSensorID(String hostSensorID) {
        hostNetworkSettings.setHostSensorID (hostSensorID);
        this.theProperties.setProperty("ccsi.state.host.sensor.id", hostSensorID);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the hostSensorName
     */
    public String getHostSensorName() {
        return hostSensorName;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param hostSensorName the hostSensorName to set
     */
    public void setHostSensorName(String hostSensorName) {
        this.hostSensorName = hostSensorName;
        this.theProperties.setProperty("ccsi.state.host.sensor.name", hostSensorName);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the hostSensorDescription
     */
    public String getHostSensorDescription() {
        return hostSensorDescription;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param hostSensorDescription the hostSensorDescription to set
     */
    public void setHostSensorDescription(String hostSensorDescription) {
        this.hostSensorDescription = hostSensorDescription;
        this.theProperties.setProperty("ccsi.state.host.sensor.desc", hostSensorDescription);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the heartbeat
     */
    public int getHeartbeat() {
        return heartbeat;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param heartbeat the heartbeat to set
     */
    public void setHeartbeat(int heartbeat) {
        if ((heartbeat >= 5) && (heartbeat <= 3600)) {
            this.heartbeat = heartbeat;
            this.theProperties.setProperty("ccsi.state.host.heartbeat", Integer.toString(heartbeat));
        } else {
            myLogger.error("Invalid heartbeat rate, must be >= 5 and <= 3600 seconds.");
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the stateLinkName
     */
    public CcsiLinkNameType getStateLinkName() {
        return stateLinkName;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param stateLinkName the stateLinkName to set
     */
    public void setStateLinkName(CcsiLinkNameType stateLinkName) {
        this.stateLinkName = stateLinkName;
        this.theProperties.setProperty("ccsi.state.link.name", stateLinkName.getLinkName());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the stateLinkType
     */
    public CcsiLinkTypeEnum getStateLinkType() {
        return stateLinkType;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param stateLinkType the stateLinkType to set
     */
    public void setStateLinkType(CcsiLinkTypeEnum stateLinkType) {
        this.stateLinkType = stateLinkType;
        this.theProperties.setProperty("ccsi.state.link.type", stateLinkType.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the stateLinkBWHigh
     */
    public boolean isStateLinkBWHigh() {
        return stateLinkBWHigh;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param stateLinkBWHigh the stateLinkBWHigh to set
     */
    public void setStateLinkBWHigh(boolean stateLinkBWHigh) {
        this.stateLinkBWHigh = stateLinkBWHigh;
        this.theProperties.setProperty("ccsi.state.link.bwhigh", Boolean.toString(stateLinkBWHigh));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the stateLinkEncrypt
     */
    public boolean isStateLinkEncrypt() {
        return hostNetworkSettings.getEncryptComm();
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param stateLinkEncrypt the stateLinkEncrypt to set
     */
    public void setStateLinkEncrypt(boolean stateLinkEncrypt) {
        hostNetworkSettings.setEncryptComm(stateLinkEncrypt);
        this.theProperties.setProperty("ccsi.state.link.encrypt", Boolean.toString(stateLinkEncrypt));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the stateLinkCompress
     */
    public boolean isStateLinkCompress() {
        return stateLinkCompress;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param stateLinkCompress the stateLinkCompress to set
     */
    public void setStateLinkCompress(boolean stateLinkCompress) {
        this.stateLinkCompress = stateLinkCompress;
        this.theProperties.setProperty("ccsi.state.host.link.compress", Boolean.toString(stateLinkCompress));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the timesource
     */
    public CcsiTimeSourceEnum getTimesource() {
        return timesource;
    }

    /**
     * @return the timesource
     */
    public CcsiTimeSourceEnum getSelTimesource() {
        return seltimesource;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param timesource the timesource to set
     */
    public void setTimesource(CcsiTimeSourceEnum timesource) {
        this.timesource = timesource;
        this.theProperties.setProperty("ccsi.state.timesource", timesource.name());
    }

    /**
     * @param timesource the timesource to set
     */
    public void setSelTimesource(CcsiTimeSourceEnum timesource) {
        this.seltimesource = timesource;
        this.theProperties.setProperty("ccsi.state.seltimesource", timesource.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the locationsource
     */
    public CcsiLocationSourceEnum getLocationsource() {
        return locationsource;
    }

    /**
     * @return the locationsource
     */
    public CcsiLocationSourceEnum getSelLocationsource() {
        return sellocationsource;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param locationsource the location source to set
     */
    public void setLocationsource(CcsiLocationSourceEnum locationsource) {
        this.locationsource = locationsource;
        this.theProperties.setProperty("ccsi.state.locationsource", locationsource.name());
    }

    /**
     * @param locationsource the location source to set
     */
    public void setSelLocationsource(CcsiLocationSourceEnum locationsource) {
        this.sellocationsource = locationsource;
        this.theProperties.setProperty("ccsi.state.sellocationsource", locationsource.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the location report option
     */
    public CcsiLocationReportOptionEnum getLocationreportoption() {
        return locationreportoption;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param locationreportoption the location report option to set
     */
    public void setLocationreportoption(CcsiLocationReportOptionEnum locationreportoption) {
        this.locationreportoption = locationreportoption;
        this.theProperties.setProperty("ccsi.state.locationreportoption", locationreportoption.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the hostIp
     */
    public InetAddress getHostIp() {
        return hostNetworkSettings.getIP ();
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param hostIp the hostIp to set
     */
    public void setHostIp(InetAddress hostIp) {
        hostNetworkSettings.setIP (hostIp);
        this.theProperties.setProperty("ccsi.state.host.ip", hostIp.getHostName());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the hostPort
     */
    public int getHostPort() {
        return hostNetworkSettings.getPort();
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param hostPort the hostPort to set
     */
    public void setHostPort(int hostPort) {
        hostNetworkSettings.setPort (hostPort);
        this.theProperties.setProperty("ccsi.state.host.port", Integer.toString(hostPort));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the compressThreshold
     */
    public int getCompressThreshold() {
        return stateLinkCompressThreshold;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param compressThreshold the compressThreshold to set
     */
    public void setCompressThreshold(int compressThreshold) {
        this.stateLinkCompressThreshold = compressThreshold;
        this.theProperties.setProperty("ccsi.state.link.compress.threshold",
                Integer.toBinaryString(compressThreshold));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the nsdsCtlQ
     */
    public CtlWrapperQueue getNsdsCtlQ() {
        return nsdsCtlQ;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param nsdsCtlQ the nsdsCtlQ to set
     */
    public void setNsdsCtlQ(CtlWrapperQueue nsdsCtlQ) {
        this.nsdsCtlQ = nsdsCtlQ;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the nsdsCtlQ
     */
    public CtlWrapperQueue getHPACtlQ() {
        return hpaCtlQ;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param hpaCtlQ
     */
    public void setHPACtlQ(CtlWrapperQueue hpaCtlQ) {
        this.hpaCtlQ = hpaCtlQ;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the phCtlQ
     */
    public CtlWrapperQueue getPhCtlQ() {
        return phCtlQ;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param phCtlQ the phCtlQ to set
     */
    public void setPhCtlQ(CtlWrapperQueue phCtlQ) {
        this.phCtlQ = phCtlQ;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the saCtlQ
     */
    public CtlWrapperQueue getSaCtlQ() {
        return saCtlQ;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param saCtlQ the saCtlQ to set
     */
    public void setSaCtlQ(CtlWrapperQueue saCtlQ) {
        this.saCtlQ = saCtlQ;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the saToPhMsgQ
     */
    public IPCWrapperQueue getSaToPhMsgQ() {
        return saToPhMsgQ;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param saToPhMsgQ the saToPhMsgQ to set
     */
    public void setSaToPhMsgQ(IPCWrapperQueue saToPhMsgQ) {
        this.saToPhMsgQ = saToPhMsgQ;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the phToSAMsgQ
     */
    public IPCWrapperQueue getPhToSAMsgQ() {
        return phToSAMsgQ;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param phToSAMsgQ the phToSAMsgQ to set
     */
    public void setPhToSAMsgQ(IPCWrapperQueue phToSAMsgQ) {
        this.phToSAMsgQ = phToSAMsgQ;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the timerBase
     */
    public UtilTimer getTimerBase() {
        return timerBase;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param timerBase the timerBase to set
     */
    public void setTimerBase(UtilTimer timerBase) {
        this.timerBase = timerBase;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the stateLinkCompressThreshold
     */
    public int getStateLinkCompressThreshold() {
        return stateLinkCompressThreshold;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param stateLinkCompressThreshold the stateLinkCompressThreshold to set
     */
    public void setStateLinkCompressThreshold(int stateLinkCompressThreshold) {
        this.stateLinkCompressThreshold = stateLinkCompressThreshold;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the schemaFileInternal
     */
    public File getSchemaFileInternal() {
        return schemaFileInternal;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param schemaFileInternal the schemaFileInternal to set
     */
    public void setSchemaFileInternal(File schemaFileInternal) {
        this.schemaFileInternal = schemaFileInternal;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Gets a single valid SCName.  No particular one.
     * @return the componentSCName
     */
    public CcsiScNameType getComponentSCName() {
        CcsiScNameType componentSCName = null;
        if (!componentSCList.isEmpty())
        {
            componentSCName = componentSCList.get(0).getSCName();
        }
        return componentSCName;
    }
    
    /**
     * Gets a list of all of the components
     * @return 
     */
    public List <String> getComponentSCList ()
    {
        List <String> retVal = new ArrayList <>();
        for (ComponentSC x : componentSCList)
        {
            retVal.add (x.getSCName().getScName());
        }
        return retVal;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentSCName the componentSCName to set
     */
//    public void setComponentSCName(CcsiScNameType componentSCName) {
//        this.componentSCName = componentSCName;
//    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the theCacheMgr
     */
    public CcsiCacheManager getTheCacheMgr() {
        return theCacheMgr;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param theCacheMgr the theCacheMgr to set
     */
    public void setTheCacheMgr(CcsiCacheManager theCacheMgr) {
        this.theCacheMgr = theCacheMgr;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pendingAcks
     */
    public CcsiPendingAckList getPendingAcks() {
        return this.pendingAcks;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pendingAcks the list to set
     */
    public void setPendingAcks(CcsiPendingAckList pendingAcks) {
        this.pendingAcks = pendingAcks;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the parseErrors
     */
    public ArrayList<String> getParseErrors() {
        return parseErrors;
    }

    /**
     * @return the nakDetails
     */
    public PendingNakDetailList getNakDetails() {
        return nakDetails;
    }

    /**
     * @return the connList
     */
    public NSDSConnectionList getConnList() {
        return connList;
    }

    
        /**
     * @return the connList
     */
    public SIAConnectionList getSensorConnectionList() {
        return sensorConnectionList;
    }
    
    
    
    /**
     * @return the mainCtlQ
     */
    public CtlWrapperQueue getMainCtlQ() {
        return mainCtlQ;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param mainCtlQ the mainCtlQ to set
     */
    public void setMainCtlQ(CtlWrapperQueue mainCtlQ) {
        this.mainCtlQ = mainCtlQ;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the sensorAlt
     */
    public double getSensorAlt() {
        return sensorAlt;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param sensorAlt the sensorAlt to set
     */
    public void setSensorAlt(double sensorAlt) {
        if ((sensorAlt >= -10000.0) && (sensorAlt <= 65535.0)) {
            this.sensorAlt = sensorAlt;
            this.theProperties.put("ccsi.state.loc.alt", Double.toString(sensorAlt));
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the sensorLat
     */
    public double getSensorLat() {
        return sensorLat;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param sensorLat the sensorLat to set
     */
    public void setSensorLat(double sensorLat) {
        if ((sensorLat >= -90.0) && (sensorLat <= 90.0)) {
            this.sensorLat = sensorLat;
            this.theProperties.put("ccsi.state.loc.lat", Double.toString(sensorLat));
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the sensorLon
     */
    public double getSensorLon() {
        return sensorLon;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param sensorLon the sensorLon to set
     */
    public void setSensorLon(double sensorLon) {
        if ((sensorLon >= -180.0) && (sensorLon <= 180.0)) {
            this.sensorLon = sensorLon;
            this.theProperties.put("ccsi.state.loc.lon", Double.toString(sensorLon));
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @param key
     * @return the componentSC
     */
    public CcsiComponentEnum getComponentSCEnum (String key) {
        ComponentSC componentSC = getComponentSC (key);
        return componentSC.getType();
    }

    /**
     * @return the componentDPCStatus
     */
    public CcsiComponentStateEnum getComponentDPCStatus() {
        return componentDPCStatus;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentDPCStatus the componentDPCStatus to set
     */
    public void setComponentDPCStatus(CcsiComponentStateEnum componentDPCStatus) {
        this.componentDPCStatus = componentDPCStatus;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentDPCBitTime
     */
    public Date getComponentDPCBitTime() {
        Date retVal = null;
        if (componentDPCBitTime != null)
        {
            retVal = (Date) componentDPCBitTime.clone();
        }
        return retVal;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentDPCBitTime the componentDPCBitTime to set
     */
    public void setComponentDPCBitTime(Date componentDPCBitTime) {
        if (componentDPCBitTime != null)
        {
            this.componentDPCBitTime = (Date) componentDPCBitTime.clone();
        } else
        {
            this.componentDPCBitTime = null;
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentDPCBitStatus
     */
    public CcsiBitStatusEnum getComponentDPCBitStatus() {
        return componentDPCBitStatus;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentDPCBitStatus the componentDPCBitStatus to set
     */
    public void setComponentDPCBitStatus(CcsiBitStatusEnum componentDPCBitStatus) {
        this.componentDPCBitStatus = componentDPCBitStatus;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentDPC
     */
    public CcsiComponentEnum getComponentDPC() {
        return componentDPC;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentDPC the componentDPC to set
     */
    public void setComponentDPC(CcsiComponentEnum componentDPC) {
        this.componentDPC = componentDPC;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentUICStatus
     */
    public CcsiComponentStateEnum getComponentUICStatus() {
        return componentUICStatus;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentUICStatus the componentUICStatus to set
     */
    public void setComponentUICStatus(CcsiComponentStateEnum componentUICStatus) {
        this.componentUICStatus = componentUICStatus;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentUICBitTime
     */
    public Date getComponentUICBitTime() {
        Date retVal = null;
        if (componentUICBitTime != null)
        {
            retVal = (Date) componentUICBitTime.clone();
        }
        return retVal;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentUICBitTime the componentUICBitTime to set
     */
    public void setComponentUICBitTime(Date componentUICBitTime) {
        if (componentUICBitTime != null)
        {
            this.componentUICBitTime = (Date) componentUICBitTime.clone();
        } else
        {
            this.componentUICBitTime = null;
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentUICBitStatus
     */
    public CcsiBitStatusEnum getComponentUICBitStatus() {
        return componentUICBitStatus;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentUICBitStatus the componentUICBitStatus to set
     */
    public void setComponentUICBitStatus(CcsiBitStatusEnum componentUICBitStatus) {
        this.componentUICBitStatus = componentUICBitStatus;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentUIC
     */
    public CcsiComponentEnum getComponentUIC() {
        return componentUIC;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentUIC the componentUIC to set
     */
    public void setComponentUIC(CcsiComponentEnum componentUIC) {
        this.componentUIC = componentUIC;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentWCCStatus
     */
    public CcsiComponentStateEnum getComponentWCCStatus() {
        return componentWCCStatus;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentWCCStatus the componentWCCStatus to set
     */
    public void setComponentWCCStatus(CcsiComponentStateEnum componentWCCStatus) {
        this.componentWCCStatus = componentWCCStatus;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentWCCBitTime
     */
    public Date getComponentWCCBitTime() {
        Date retVal = null;
        if (componentWCCBitTime != null)
        {
            retVal = (Date) componentWCCBitTime.clone();
        }
        return retVal;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentWCCBitTime the componentWCCBitTime to set
     */
    public void setComponentWCCBitTime(Date componentWCCBitTime) {
        if (componentWCCBitTime != null)
        {
            this.componentWCCBitTime = (Date) componentWCCBitTime.clone();
        } else
        {
            this.componentWCCBitTime = null;
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentWCCBitStatus
     */
    public CcsiBitStatusEnum getComponentWCCBitStatus() {
        return componentWCCBitStatus;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentWCCBitStatus the componentWCCBitStatus to set
     */
    public void setComponentWCCBitStatus(CcsiBitStatusEnum componentWCCBitStatus) {
        this.componentWCCBitStatus = componentWCCBitStatus;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentWCC
     */
    public CcsiComponentEnum getComponentWCC() {
        return componentWCC;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentWCC the componentWCC to set
     */
    public void setComponentWCC(CcsiComponentEnum componentWCC) {
        this.componentWCC = componentWCC;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentWCCName
     */
    public CcsiWccNameType getComponentWCCName() {
        return componentWCCName;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param componentWCCName the componentWCCName to set
     */
    public void setComponentWCCName(CcsiWccNameType componentWCCName) {
        this.componentWCCName = componentWCCName;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the componentList
     */
    public ArrayList<ConfigComponentInfo> getComponentList() {
        return componentList;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the saWriterDataQ
     */
    public IPCWrapperQueue getSaWriterDataQ() {
        return saWriterDataQ;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param saWriterDataQ the saWriterDataQ to set
     */
    public void setSaWriterDataQ(IPCWrapperQueue saWriterDataQ) {
        this.saWriterDataQ = saWriterDataQ;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the connEventQueue
     */
    public ConnEventQueue getConnEventQueue() {
        return connEventQueue;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param connEventQueue the connEventQueue to set
     */
    public void setConnEventQueue(ConnEventQueue connEventQueue) {
        this.connEventQueue = connEventQueue;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the hiaTransferEventQueue
     */
    public NSDSXferEventQueue getHiaTransferEventQueue() {
        return this.hiaTransferEventQueue;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param transferEventQueue the hiaTransferEventQueue to set
     */
    public void setHiaTransferEventQueue(NSDSXferEventQueue hiaTransferEventQueue) {
        this.hiaTransferEventQueue = hiaTransferEventQueue;
    }

    public SIAXferEventQueue getSiaXferEventQueue() {
        return siaXferEventQueue;
    }

    public void setSiaXferEventQueue(SIAXferEventQueue siaXferEventQueue) {
        this.siaXferEventQueue = siaXferEventQueue;
    }

    public SIAXferEventQueue getSpaXferEventQueue() {
        return spaXferEventQueue;
    }

    public void setSpaXferEventQueue(SIAXferEventQueue spaXferEventQueue) {
        this.spaXferEventQueue = spaXferEventQueue;
    }

    
    
    
    //~--- get methods ----------------------------------------------------------
    /**
     * @return the nsdsLinkEventQ
     */
    public LinkEventQueue getNsdsLinkEventQ() {
        return nsdsLinkEventQ;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param nsdsLinkEventQ the nsdsLinkEventQ to set
     */
    public void setNsdsLinkEventQ(LinkEventQueue nsdsLinkEventQ) {
        this.nsdsLinkEventQ = nsdsLinkEventQ;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Returns the next alert ID value, updates the properties table and wraps the value back to 1
     * when it goes past 999999999
     *
     * @return long containing the alert ID
     */
    public long getNextAlertID() {
        long retVal = this.alertIDGenerator.getAndIncrement();

        if (retVal > 999999999) {
            this.alertIDGenerator.set(1L);
            retVal = 1L;
            this.theProperties.setProperty("ccsi.state.alertId", Long.toString(this.alertIDGenerator.get()));
        } else {
            this.theProperties.setProperty("ccsi.state.alertId", Long.toString(this.alertIDGenerator.get()));
        }

        return (retVal);
    }

    /**
     * Returns the next reading ID value, updates the properties table and wraps the value back to
     * 999000000 when it goes past 999999999
     *
     * @return int containing the reading ID
     */
    public int getNextReadingID() {
        int retVal = this.readingIDGenerator.getAndIncrement();

        if (retVal > 999999999) {
            this.readingIDGenerator.set(999000000);
            retVal = 999000000;
            this.theProperties.setProperty("ccsi.state.reading.id", Integer.toString(retVal));
        } else {
            this.theProperties.setProperty("ccsi.state.reading.id", Integer.toString(this.readingIDGenerator.get()));
        }

        return (retVal);
    }

    /**
     * @return the stateDegraded
     */
    public boolean isStateDegraded() {
        return stateDegraded;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param stateDegraded the stateDegraded to set
     */
    public void setStateDegraded(boolean stateDegraded) {
        this.stateDegraded = stateDegraded;
        this.theProperties.setProperty("sensor.state.degraded", Boolean.toString(stateDegraded));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the stateMainReq
     */
    public boolean isStateMaintReq() {
        return stateMaintReq;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param stateMaintReq the stateMainReq to set
     */
    public void setStateMaintReq(boolean stateMaintReq) {
        this.stateMaintReq = stateMaintReq;
        this.theProperties.setProperty("sensor.state.maintreq", Boolean.toString(stateMaintReq));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the stateReadingAffected
     */
    public boolean isStateReadingAffected() {
        return stateReadingAffected;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param stateReadingAffected the stateReadingAffected to set
     */
    public void setStateReadingAffected(boolean stateReadingAffected) {
        this.stateReadingAffected = stateReadingAffected;
        this.theProperties.setProperty("sensor.state.readingaffected", Boolean.toString(stateReadingAffected));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the stateSensorInop
     */
    public boolean isStateSensorInop() {
        return stateSensorInop;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param stateSensorInop the stateSensorInop to set
     */
    public void setStateSensorInop(boolean stateSensorInop) {
        this.stateSensorInop = stateSensorInop;
        this.theProperties.setProperty("sensor.state.inop", Boolean.toString(stateSensorInop));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the statePwrStatus
     */
    public PowerModeEnum getStatePwrStatus() {
        return statePwrStatus;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param statePwrStatus the statePwrStatus to set
     */
    public void setStatePwrStatus(PowerModeEnum statePwrStatus) {
        this.statePwrStatus = statePwrStatus;
        this.theProperties.setProperty("sensor.state.pwrstatus", statePwrStatus.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the theOpenInterface
     */
    public NSDSInterface getTheOpenInterface() {
        return theOpenInterface;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param theOpenInterface the theOpenInterface to set
     */
    public void setTheOpenInterface(NSDSInterface theOpenInterface) {
        this.theOpenInterface = theOpenInterface;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the theHPAInterface
     */
    public HostProtocolAdapterInterface getTheHPAInterface() {
        return theHPAInterface;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param theHPAInterface
     */
    public void setTheHPAInterface(HostProtocolAdapterInterface theHPAInterface) {
        this.theHPAInterface = theHPAInterface;
    }

    /**
     * @return the sensingStopped
     */
    public boolean isSensingStopped() {
        return sensingStopped;
    }

    /**
     * @param sensingStopped the sensingStopped to set
     */
    public void setSensingStopped(boolean sensingStopped) {
        this.sensingStopped = sensingStopped;
        this.theProperties.put("ccsi.state.sensing.stopped", Boolean.toString(sensingStopped));
    }

    /**
     * Sets the baseFile which is the path that all configurations, etc are located
     *
     * @param base
     */
    public void setBasefile(String base) {
        baseFile = base + File.separator;
    }

    /**
     * Gets the baseFile which is the path that all configurations, etc are located
     *
     * @return
     */
    public String getBasefile() {
        return baseFile;
    }

    public void setHPAConnEventQueue(ConnEventQueue queue) {
        hpaConnEventQueue = queue;
    }

    public ConnEventQueue getHPAConnEventQueue() {
        return hpaConnEventQueue;
    }

    public void setHPAXferEventQueue(NSDSXferEventQueue queue) {
        hpaXferEventQueue = queue;
    }

    public NSDSXferEventQueue getHPAXferEventQueue() {
        return hpaXferEventQueue;
    }
    
    /**
     * Gets the component associated with the ID.  
     * If not found, a new one is returned with that ID
     * @param componentId
     * @return 
     */
    private ComponentSC getComponentID (String componentId)
    {
        ComponentSC component = null;
        String [] id = componentId.split("\\.");
        
        for (ComponentSC x : componentSCList)
        {
            if (id[id.length - 1].equals(x.getId())) {
                component = x;
                break;
            }
        }
        if (component == null)
        {
            component = new ComponentSC (id[id.length - 1]);
            componentSCList.add(component);
        }
        
        return component;
    }
        /**
     * Gets the component associated with the name.  
     * @param componentName - the component
     * @return 
     */
    private ComponentSC getComponentSC (String componentName)
    {
        ComponentSC component = null;
    
        for (ComponentSC x : componentSCList)
        {
            if (x.getSCName().getScName().equals(componentName)) {
                component = x;
                break;
            }
        }
        
        return component;
    }
    
    public String getComponentSCName (String componentName)
    {
        String name = null;
        for (ComponentSC x : componentSCList)
        {
            if (x.getSCName().getScName().equals(componentName)) {
                name = x.getSCName().getScName();
                break;
            }
        }
        
        return name;
    }
    
    public HostNetworkSettings getHostNetworkSettings ()
    {
        return hostNetworkSettings;
    }
    
    
    public SensorNetworkSettings getSensorNetworkSettings ()
    {
        return sensorNetworkSettings;
    }    
    
    
    private class ComponentSC 
    {
        private final String id;
        private CcsiComponentEnum type;
        private CcsiScNameType scName;
        private Date bitTime;
        private CcsiBitStatusEnum bitStatus;
        private CcsiComponentStateEnum state;
        
        public ComponentSC (String id)
        {
            this.id = id.toLowerCase();
        }
        
        public ComponentSC (SensorComponentItem item)
        {
            this.id = item.getName().toLowerCase();
            type = item.getComponent();
            scName = new CcsiScNameType (item.getName());
            bitTime = new Date ();
            bitStatus = CcsiBitStatusEnum.FAIL;
            state = CcsiComponentStateEnum.F;
        }
        
        public String getId ()
        {
            return id;
        }
   
        public CcsiComponentEnum getType() {
            return type;
        }

        public CcsiScNameType getSCName() {
            return scName;
        }
        
        public Date getBitDate ()
        {
            return bitTime;
        }
        
        public CcsiBitStatusEnum getBitStatus ()
        {
            return bitStatus;
        }

        public CcsiComponentStateEnum getComponentState() {
            return state;
        }
        public void setBitDate (Date time)
        {
            bitTime = time;
        }
        
        public void setComponentType (CcsiComponentEnum type)
        {
            this.type = type;
        }
        
        public void setSCName (CcsiScNameType name)
        {
            scName = name;
        }
        
        public void setBitStatus (CcsiBitStatusEnum status)
        {
            bitStatus = status;
        }

        public void setComponentState(CcsiComponentStateEnum state) {
            this.state = state;
        }
        
        public void setProperties (Properties theProperties)
        {
            theProperties.put("ccsi.state.component.sc." + id, type.toString());
            theProperties.put("ccsi.state.component.sc.name." + id, scName.getScName());
            theProperties.put("ccsi.state.component.sc.lastbit." + id, NumberFormatter.getDate(bitTime, "yyyyMMddHHmmss"));
            theProperties.put("ccsi.state.component.sc.result." + id, bitStatus.toString());
            theProperties.put("ccsi.state.component.sc.status." + id, state.toString());
        }
    }
}
