/*
 * CcsiSensorConfig.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the configured CCSI properties and operations to read/write the file and to
 * get and set each item.
 *
 * Version: V1.0  15/08/20
 */
package com.domenix.ccsi.config;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.common.config.SensorUniqueConfigItem;
import com.domenix.common.config.SerialSettings;
import com.domenix.ccsi.AnnunciatorStateEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.ccsi.CcsiSensorMountingEnum;
import com.domenix.ccsi.CcsiSensorTypeEnum;
import com.domenix.ccsi.CcsiSensorVariantEnum;
import com.domenix.ccsi.CommandHandlingListType;
import com.domenix.ccsi.CommandHandlingType;
import com.domenix.common.ccsi.CommandHandlingTypeEnum;
import com.domenix.common.ccsi.ConnSensorNetEnum;
import com.domenix.ccsi.SupportedChannelType;
import com.domenix.ccsi.VersionNumberType;
import com.domenix.common.enums.NetworkAddressTypeEnum;
import com.domenix.common.enums.NetworkProtocolTypeEnum;
import com.domenix.common.enums.NetworkTypeEnum;
import com.domenix.ccsi.protocol.ChannelSupport;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.commonsa.interfaces.SensorNetworkSettings;

//~--- JDK imports ------------------------------------------------------------
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

//~--- classes ----------------------------------------------------------------
/**
 * This class is the container for the contents of a CCSI sensor configuration file. It provides
 * methods for reading and writing the file, accessing the configuration parameters, and for
 * updating the parameters.
 *
 * @author kmiller
 */
public class CcsiConfigSensor {

    /**
     * default configuration file name override with -Dccsi.config.file=&lt;filename&gt;
     */
    private static final String DEFAULT_CONFIG_FILE = "ccsi-config.xml";

    /**
     * property key for the configuration file
     */
    private static final String DEFAULT_CONFIG_FILE_KEY = "ccsi.config.file";

    /**
     * the configuration file used for this instance
     */
    private final String configFileName;

    /**
     * The base CCSI sensor configuration file path
     */
    private final String baseFile;

    /**
     * The ACK timeout value in seconds.
     */
    private int ackTimeout = 5;

    /**
     * The number of non-controlling app connections supported.
     */
    private int connLimit = 0;

    /**
     * The UUID of this sensor.
     */
    private String uuid = "12345678-1234-1234-1234-1234567890ab";

    /**
     * Is the sensor unidirectional
     */
    private boolean unidirectional = false;

    /**
     * The properties
     */
    private final Properties theProperties = new Properties();

    private String sensorHostDesc;

    /**
     * The error/debug logger
     */
    private final Logger myLogger = Logger.getLogger(CcsiConfigSensor.class);

    /**
     * The CCSI version supported by this sensor.
     */
    private VersionNumberType ccsiVersion;

    /**
     * Is sensor internal data encrypted by default. SECURITY block
     */
    private boolean encryptData = false;

    /**
     * The algorithm used for encryption. BASE block
     */
    private String encryptType = "AES-512";

    /**
     * The sensor hardware version.
     */
    private VersionNumberType hwVersion;

    /**
     * The sensor name
     */
    private String sensorName;
    private String sensorHostName;

    /**
     * The SDF defined sensor version implemented
     */
    private String sensorVersion;

    /**
     * The sensor's serial number.
     */
    private String serial;

    /**
     * The sensor's internal software version number.
     */
    private VersionNumberType swVersion;

    /**
     * The sensor's type.
     */
    private CcsiSensorTypeEnum sensorType;

    /**
     * The sensor's variant
     */
    private CcsiSensorVariantEnum variant;

    /**
     * Annunciator states
     */
    private AnnunciatorStateEnum audibleAnnunState = AnnunciatorStateEnum.OFF;
    private AnnunciatorStateEnum visualAnnunState = AnnunciatorStateEnum.OFF;
    private AnnunciatorStateEnum tactileAnnunState = AnnunciatorStateEnum.OFF;
    private AnnunciatorStateEnum ledAnnunState = AnnunciatorStateEnum.OFF;
    private AnnunciatorStateEnum externalAnnunState = AnnunciatorStateEnum.OFF;

    /**
     * List of supported CCSI commands
     */
    private final CommandHandlingListType cmdHandling = new CommandHandlingListType();

    /**
     * List of supported channels
     */
    private final SupportedChannelType theChannels = new SupportedChannelType();

    /**
     * The timer for waiting a long period for connection failures
     */
    private int connFailureTimeout = 300;

    /**
     * The detection items list
     */
    private final ArrayList<String> detectableItems = new ArrayList<>();

    /**
     * list of SensorUnique Configuration Items
     */
    private final ArrayList<SensorUniqueConfigItem> sensorUniqueConfigList = new ArrayList<>();

    /**
     * list of SensorUnique Data Items
     */
    private final ArrayList<SensorUniqueDataItem> sensorUniqueDataList = new ArrayList<>();

    /**
     * the IPAddress is fixed *
     */
    private boolean ipAddressFixed = true;

    /**
     * Classification of the Sensor Data
     */
    private String sensorDataClassification = "UNCLASS";

    /**
     * Classification of the Sensor Comms
     */
    private String sensorCommClassification = "UNCLASS";

    /**
     * SetConfig item that requires a reboot
     */
    private List<String> rebootList = new ArrayList<>();

    /**
     * Sensor Network
     */
    private NetworkAddressTypeEnum sensorNetworkAddressType;
    private NetworkProtocolTypeEnum sensorNetworkProtocolType;
    private InetAddress sensorNetworkRegistryAddr;
    private NetworkTypeEnum sensorNetworkType;

    private InetAddress sensorIPBroadcast;
    private InetAddress sensorIPGateway;
    private InetAddress sensorIPNetmask;
    private InetAddress sensorIP;
    private InetAddress sensorNetworkTimeAddr;
    private int sensorPort = 5674;
    private InetAddress sensorPHIP;
    private int sensorPHPort = 5674;
    private ConnSensorNetEnum sensorInterfaceType = ConnSensorNetEnum.SERIAL;
    private int sensorMaxTransmit = 32768;

    /**
     * Sensor Bit Item
     */
    private List<SensorBitItem> sensorBitItemList;

    /**
     * Sensor Component Item
     */
    private List<SensorComponentItem> sensorComponentItemList;

    /**
     * Sensor Host ID
     */
//    private String sensorHostID = null;

    /**
     * Sensor Manufacturer
     */
    private String sensorManufacturer = null;

    /**
     * Sensor Model
     */
    private String sensorModel = null;

    private int ipLinkSpeed;

    /**
     * Sensor SerialSettings serial interface parameters
     */
    private final SerialSettings serialInterface = new SerialSettings("SENSOR");

    /**
     * Host SerialSettings network interface parameters
     */
    private final SerialSettings hostSerialInterface = new SerialSettings("HOST");
    
    /** Host Network Settings */
    private final HostNetworkSettings hostNetworkSettings = new HostNetworkSettings ();

    /** Sensor Network Settings */
    private final SensorNetworkSettings sensorNetworkSettings = new SensorNetworkSettings ("SENSOR");

    /** sensor mounting type */
    private CcsiSensorMountingEnum sensorMountingType = CcsiSensorMountingEnum.DISMTD;
    
    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs the class instance by reading the CCSI configuration file baseDir/configFileName
     *
     * @param baseFile
     * @throws Exception - any exception thrown by the reader/processor is propagated
     */
    public CcsiConfigSensor(String baseFile) throws Exception {
        sensorBitItemList = new ArrayList<>();
        sensorComponentItemList = new ArrayList<>();
        this.baseFile = baseFile;
        // get the configuration file name from the property
        String tempConfigFile = System.getProperty(DEFAULT_CONFIG_FILE_KEY);
        if ((tempConfigFile == null) || (tempConfigFile.isEmpty())) {
            // wasn't set, use the default
            configFileName = DEFAULT_CONFIG_FILE;
        } else {
            // use the supplied file name
            configFileName = tempConfigFile;
        }

        readSensorConfig();

        myLogger.info("Done reading sensor config: " + configFileName);
    }

    //~--- methods --------------------------------------------------------------
    /**
     * This method reads the configuration file
     *
     * @throws Exception
     */
    private void readSensorConfig() throws Exception {
        String fName = "";

        FileInputStream fis = null;
        
        try {
            fName = baseFile + File.separator + configFileName;

            fis = new FileInputStream(fName);

            theProperties.loadFromXML(fis);

            for (String x : theProperties.stringPropertyNames()) {
                if (x == null) {
                    myLogger.info("Null key returned.");
                } else if (x.startsWith("project")) {
                    // skip this property
                } else {
                    String value = theProperties.getProperty(x);
//KBM:  7/9/2020 -- removed to cut down on Logging length
//                    myLogger.info("Config key = " + x + " val = " + value);
                    if (x.startsWith("ccsi")) {
                        parseCCSI(x, value);
                    } else if (x.startsWith("sensor")) {
//                        System.out.println("SENSOR value:  item:  " + x + "    value is " + value);
                        parseSensor(x, value);
                    } else if (x.startsWith("host")) {
                        parseHost(x, value);
                    } else {
                        myLogger.error("Unknown configuration file entry [" + x + "] found.");
                    }
                }
            }
        } catch (IOException | NumberFormatException ex) {
            System.out.println("Cannot read configuration file: " + fName);
            myLogger.fatal("Cannot read configuration file: " + fName, ex);

            throw (ex);
        } finally
        {
            if (fis != null)
            {
                fis.close();
            }
        }
    }

    /**
     * Deletes the sensor Configuration file
     *
     * @return
     */
    public boolean deleteSensorConfig() {
        String fName = baseFile + File.separator + configFileName;
        File config = new File(fName);
        boolean result = config.delete();
        return result;
    }

    /**
     * This writes to the sensor configuration file
     *
     * @throws Exception
     */
    public void writeSensorConfig() throws Exception {
        String fName = "";
        FileOutputStream fos = null;

        try {
            fName = baseFile + File.separator + configFileName;

            try {
                fos = new FileOutputStream(fName);
                theProperties.storeToXML(fos, "CCSI V1.1.1 Configuration", "UTF-8");
                fos.close();
                fos = null;
            } catch (IOException foEx) {
                myLogger.fatal("Fatal exception trying to open the config file: " + fName, foEx);
                throw (foEx);
            }
        } catch (IOException ex) {
            myLogger.fatal("Fatal exception trying to write/update config file: " + fName, ex);

            throw (ex);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException fex) {
                    myLogger.error("Fatal exception trying to close config file: " + fName, fex);
                }
            }
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
        this.theProperties.setProperty("sensor.uuid", uuid);
    }

    /**
     * @return the interfaceType to the sensor
     */
    public ConnSensorNetEnum getSensorInterfaceType() {
        return sensorInterfaceType;
    }
    
    /**
     * Returns the sensor mounting type
     * @return 
     */
    public CcsiSensorMountingEnum getSensorMountingType ()
    {
        return sensorMountingType;
    }
    
    //~--- set methods ----------------------------------------------------------
    /**
     * @param interfaceType the interfaceType to set
     */
    public void setSensorInterfaceType(ConnSensorNetEnum interfaceType) {
        this.sensorInterfaceType = interfaceType;
        this.theProperties.setProperty("sensor.network.interface", interfaceType.name());
    }

    /**
     * @return the ip address of the Sensor
     */
    public InetAddress getSensorIp() {
        return sensorIP;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param ip the ip to set for the Sensor
     */
    public void setSensorIp(InetAddress ip) {
        this.sensorIP = ip;
        this.theProperties.setProperty("sensor.network.ip", ip.getHostName());
    }

    /**
     * @return the ip address of the Sensor
     */
    public InetAddress getSensorPHIp() {
        return sensorPHIP;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param ip the ip to set for the Sensor
     */
    public void setSensorPHIp(InetAddress ip) {
        this.sensorIP = ip;
        this.theProperties.setProperty("sensor.network.phip", ip.getHostName());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the sensor serial number
     */
    public String getSerial() {
        return serial;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param serial the unit serial number to set
     */
    public void setSerial(String serial) {
        this.serial = serial;
        this.theProperties.setProperty("sensor.serial", serial);
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param ipGateway the ipGateway to set for the Host - PH connection
     */
    public void setSensorIpGateway(InetAddress ipGateway) {
        this.sensorIPGateway = ipGateway;
        this.theProperties.setProperty("sensor.ip.gateway", ipGateway.getHostName());
    }

    /**
     * @return the ipBroadcast for the Sensor - PH connection
     */
    public InetAddress getSensorIpBroadcast() {
        return sensorIPBroadcast;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param ipBroadcast the ipBroadcast to set for the Host - PH connection
     */
    public void setSensorIpBroadcast(InetAddress ipBroadcast) {
        this.sensorIPBroadcast = ipBroadcast;
        this.theProperties.setProperty("sensor.network.bdcst", ipBroadcast.getHostName());
    }

    /**
     * @return the ipNetmask for the Host
     */
    public InetAddress getSensorIpNetmask() {
        return sensorIPNetmask;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param ipNetmask the ipNetmask to set for the Host
     */
    public void setSensorIpNetmask(InetAddress ipNetmask) {
        this.sensorIPNetmask = ipNetmask;
        this.theProperties.setProperty("sensor.network.netmask", ipNetmask.getHostName());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the swVersion
     */
    public VersionNumberType getSwVersion() {
        return swVersion;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param swVersion the swVersion to set
     */
    public void setSwVersion(VersionNumberType swVersion) {
        this.swVersion = swVersion;
        this.theProperties.setProperty("sensor.swVersion", swVersion.toString());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the hwVersion
     */
    public VersionNumberType getHwVersion() {
        return hwVersion;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param hwVersion the hwVersion to set
     */
    public void setHwVersion(VersionNumberType hwVersion) {
        this.hwVersion = hwVersion;
        this.theProperties.setProperty("sensor.hwVersion", hwVersion.toString());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the ccsiVersion
     */
    public VersionNumberType getCcsiVersion() {
        return ccsiVersion;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param ccsiVersion the ccsiVersion to set
     */
    public void setCcsiVersion(VersionNumberType ccsiVersion) {
        this.ccsiVersion = ccsiVersion;
        this.theProperties.setProperty("sensor.ccsiVersion", ccsiVersion.toString());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the Sensor Type
     */
    public CcsiSensorTypeEnum getType() {
        return sensorType;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param type the Sensor Type to set
     */
    public void setType(CcsiSensorTypeEnum type) {
        sensorType = type;
        theProperties.setProperty("sensor.type", type.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the variant
     */
    public CcsiSensorVariantEnum getVariant() {
        return variant;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param variant the variant to set
     */
    public void setVariant(CcsiSensorVariantEnum variant) {
        this.variant = variant;
        this.theProperties.setProperty("sensor.variant", variant.name());
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the encryptType (BASE block)
     */
    public String getEncryptType() {
        return encryptType;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param encryptType the encryptType to set (BASE block)
     */
    public void setEncryptType(String encryptType) {
        this.encryptType = encryptType;
        theProperties.setProperty("ccsi.encrypt.type", encryptType);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the encryptData
     */
    public boolean isEncryptData() {
        return encryptData;
    }

    /**
     * @return the encryptData
     */
    public boolean getEncryptData() {
        return encryptData;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param encryptData the encryptData to set
     */
    public void setEncryptData(boolean encryptData) {
        this.encryptData = encryptData;
        this.theProperties.setProperty("sensor.encrypt.data", ((encryptData) ? "true" : "false"));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the ackTimeout
     */
    public int getAckTimeout() {
        return ackTimeout;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param ackTimeout the ackTimeout to set
     */
    public void setAckTimeout(int ackTimeout) {
        if ((ackTimeout >= 5) && (ackTimeout <= 60)) {
            this.ackTimeout = ackTimeout;
            this.theProperties.setProperty("sensor.ack.timeout", Integer.toBinaryString(ackTimeout));
        } else {
            myLogger.error("Invalid ack timeout of [" + ackTimeout + "] ignored.");
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the connLimit
     */
    public int getConnLimit() {
        return connLimit;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param connLimit the connLimit to set
     */
    public void setConnLimit(int connLimit) {
        this.connLimit = connLimit;
        this.theProperties.setProperty("sensor.conn.limit", Integer.toBinaryString(connLimit));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the sensorVersion
     */
    public String getSensorVersion() {
        return sensorVersion;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param sensorVersion the sensorVersion to set
     */
    public void setSensorVersion(String sensorVersion) {
        this.sensorVersion = sensorVersion.trim();
        this.theProperties.setProperty("sensor.version", sensorVersion.trim());
    }

    /**
     * @return the port of the PH
     */
    public int getSensorPHPort() {
        return sensorPHPort;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param port the port to set for the PH
     */
    public void setSensorPHPort(int port) {
        this.sensorPHPort = port;
        this.theProperties.setProperty("sensor.network.phport", Integer.toBinaryString(port));
    }

    /**
     * @return the port of the PH
     */
    public int getSensorPort() {
        return sensorPort;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param port the port to set for the PH
     */
    public void setSensorPort(int port) {
        this.sensorPort = port;
        this.theProperties.setProperty("sensor.network.port", Integer.toBinaryString(port));
    }

    /**
     * @return the maxTransmit to the Sensor
     */
    public int getSensorMaxTransmit() {
        return sensorMaxTransmit;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param maxTransmit the maxTransmit to set to the Host
     */
    public void setSensorMaxTransmit(int maxTransmit) {
        this.sensorMaxTransmit = maxTransmit;
        this.theProperties.setProperty("sensor.network.maxtransmit", Integer.toBinaryString(maxTransmit));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the unidirectional
     */
    public boolean isUnidirectional() {
        return unidirectional;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param unidirectional the unidirectional to set
     */
    public void setUnidirectional(boolean unidirectional) {
        this.unidirectional = unidirectional;
        this.theProperties.setProperty("sensor.unidirectional",
                ((unidirectional) ? "true" : "false"));
    }

    /**
     * @return the cmdHandling
     */
    public CommandHandlingListType getCmdHandling() {
        return cmdHandling;
    }

    /**
     * @return the theChannels
     */
    public SupportedChannelType getTheChannels() {
        return theChannels;
    }

    /**
     * @return the sensorName
     */
    public String getSensorName() {
        return sensorName;
    }

    /**
     * @param sensorName the sensorName to set
     */
    public void setSensorName(String sensorName) {
        if ((sensorName != null) && (!sensorName.trim().isEmpty())) {
            this.sensorName = sensorName;
            this.theProperties.setProperty("ccsi.sensor.name", sensorName);
        }
    }

    /**
     * @return the sensorName
     */
    public String getSensorHostName() {
        return sensorHostName;
    }

    /**
     * @param sensorHostName the sensorHostName to set
     */
    public void setSensorHostName(String sensorHostName) {
        if ((sensorHostName != null) && (!sensorName.trim().isEmpty())) {
            this.sensorHostName = sensorHostName;
            this.theProperties.setProperty("sensor.hostname", sensorHostName);
        }
    }

    /**
     * @return the connFailureTimeout
     */
    public int getConnFailureTimeout() {
        return connFailureTimeout;
    }

    /**
     * @param connFailureTimeout the connFailureTimeout to set
     */
    public void setConnFailureTimeout(int connFailureTimeout) {
        this.connFailureTimeout = connFailureTimeout;
        theProperties.put("sensor.conn.failure.timeout",
                Integer.toString(connFailureTimeout));
    }

    /**
     * @return the detectableItems
     */
    public ArrayList<String> getDetectableItems() {
        return detectableItems;
    }

    /**
     * gets the sensor unique configuration
     *
     * @return
     */
    public ArrayList<SensorUniqueConfigItem> getSensorUniqueConfig() {
        return sensorUniqueConfigList;
    }

    /**
     * clears the Sensor Unique Configuration
     */
    public void clearSensorUniqueConfig() {
        sensorUniqueConfigList.clear();
    }

    /**
     * gets the sensor unique configuration
     *
     * @return
     */
    public ArrayList<SensorUniqueDataItem> getSensorUniqueData() {
        return sensorUniqueDataList;
    }

    /**
     * clears the Sensor Unique Configuration
     */
    public void clearSensorUniqueData() {
        sensorUniqueDataList.clear();
    }

    /**
     * Gets the sensor unique configuration item that matches the theName
     *
     * @param configItemName
     * @return null if not found
     */
    public SensorUniqueConfigItem getSensorUniqueConfig(String configItemName) {
        SensorUniqueConfigItem config = null;
        for (SensorUniqueConfigItem item : sensorUniqueConfigList) {
            if (item.getItemName().equalsIgnoreCase(configItemName)) {
                config = item;
                break;
            }
        }
        return config;
    }

    /**
     * Gets the sensor unique data item that matches the theName
     *
     * @param configItemName
     * @return null if not found
     */
    public SensorUniqueDataItem getSensorUniqueData(String configItemName) {
        SensorUniqueDataItem config = null;
        for (SensorUniqueDataItem item : sensorUniqueDataList) {
            if (item.getItemName().equalsIgnoreCase(configItemName)) {
                config = item;
                break;
            }
        }
        return config;
    }

    /**
     * gets the state of the Audible Annunciator
     *
     * @return the state
     */
    public AnnunciatorStateEnum getAudibleAnnunState() {
        return audibleAnnunState;
    }

    /**
     * Sets the state of the Audible Annunciator
     *
     * @param theState
     */
    public void setAudibleAnnunState(AnnunciatorStateEnum theState) {
        audibleAnnunState = theState;
    }

    /**
     * gets the state of the Visible Annunciator
     *
     * @return the state
     */
    public AnnunciatorStateEnum getVisualAnnunState() {
        return visualAnnunState;
    }

    /**
     * Sets the state of the Visible Annunciator
     *
     * @param theState
     */
    public void setVisualAnnunState(AnnunciatorStateEnum theState) {
        visualAnnunState = theState;
    }

    /**
     * gets the state of the Tactile Annunciator
     *
     * @return the state
     */
    public AnnunciatorStateEnum getTactileAnnunState() {
        return tactileAnnunState;
    }

    /**
     * Sets the state of the Tactile Annunciator
     *
     * @param theState
     */
    public void setTactileAnnunState(AnnunciatorStateEnum theState) {
        tactileAnnunState = theState;
    }

    /**
     * gets the state of the LED Annunciator
     *
     * @return the state
     */
    public AnnunciatorStateEnum getLedAnnunState() {
        return ledAnnunState;
    }

    /**
     * Sets the state of the Led Annunciator
     *
     * @param theState
     */
    public void setLedAnnunState(AnnunciatorStateEnum theState) {
        ledAnnunState = theState;
    }

    /**
     * gets the state of the External Annunciator
     *
     * @return the state
     */
    public AnnunciatorStateEnum getExternalAnnunState() {
        return externalAnnunState;
    }

    /**
     * Sets the state of the External Annunciator
     *
     * @param theState
     */
    public void setExternalAnnunState(AnnunciatorStateEnum theState) {
        externalAnnunState = theState;
    }

    public String getSensorHostDescription() {
        return sensorHostDesc;
    }

    /**
     * Gets IPAddreddFixed (as opposed to dynamic (DHCP, etc.))
     *
     * @return
     */
    public boolean getIPAddressFixed() {
        return ipAddressFixed;
    }

    /**
     * Sets IPAddressFixed (as opposed to dynamic (DHCP, etc.))
     *
     * @param ipAddressFixed
     */
    public void setIPAddressFixed(boolean ipAddressFixed) {
        this.ipAddressFixed = ipAddressFixed;
    }

    /**
     * Gets the ChannelSupport for the given channel
     *
     * @param channel
     * @return null if not found
     */
    public ChannelSupport getChannelSupport(CcsiChannelEnum channel) {
        ChannelSupport result = null;
        for (ChannelSupport dataComp : theChannels.getTheChannels()) {
            if (dataComp.getChannel().equals(channel)) {
                result = dataComp;
                break;
            }
        }
        return result;
    }

    /**
     * Updates or adds an item to the ChannelSupportList
     *
     * @param channelSupport
     */
    public void updateChannelSupport(ChannelSupport channelSupport) {
        for (ChannelSupport dataComp : theChannels.getTheChannels()) {
            if (dataComp.getChannel().equals(channelSupport.getChannel())) {
                theChannels.getTheChannels().remove(dataComp);
                break;
            }
        }
        theChannels.getTheChannels().add(channelSupport);
    }

    /**
     * Sets the classification of Sensor Data
     *
     * @param classification
     */
    public void setSensorDataClassification(String classification) {
        this.sensorDataClassification = " " + classification + " ";
    }

    /**
     * Gets the sensor data classification
     *
     * @return
     */
    public String getSensorDataClassification() {
        return sensorDataClassification;
    }

    /**
     * Gets the classification of Sensor Communications
     *
     * @param classification
     */
    public void setSensorCommClassification(String classification) {
        this.sensorCommClassification = " " + sensorCommClassification + " ";
    }

    /**
     * Gets the sensor communications classification
     *
     * @return
     */
    public String getSensorCommClassification() {
        return sensorDataClassification;
    }

    public List<String> getRebootList() {
        return rebootList;
    }

    public void setRebootList(List<String> rebootList) {
        this.rebootList = rebootList;
    }

    public void setSensorNetworkAdressType(NetworkAddressTypeEnum addressType) {
        sensorNetworkAddressType = addressType;
    }

    public NetworkAddressTypeEnum getSensorNetworkAdressType() {
        return sensorNetworkAddressType;
    }

    public void setSensorNetworkProtocolType(NetworkProtocolTypeEnum hostNetworkProtocolType) {
        this.sensorNetworkProtocolType = hostNetworkProtocolType;
    }

    public NetworkProtocolTypeEnum getSensorNetworkProtocolType() {
        return sensorNetworkProtocolType;
    }

    /**
     * Address of Sensor Network NTP Server
     *
     * @param sensorNetworkTimeAddr
     */
    public void setSensorNetworkTimeAddr(InetAddress sensorNetworkTimeAddr) {
        this.sensorNetworkTimeAddr = sensorNetworkTimeAddr;
    }

    /**
     * Address of Sensor Network NTP Server
     *
     * @return
     */
    public InetAddress getSensorNetworkTimeAddr() {
        return sensorNetworkTimeAddr;
    }

    /**
     * Address of Sensor Network Registry Server
     *
     * @param sensorNetworkRegistryAddr
     */
    public void setSesnorNetworkRegistryAddr(InetAddress sensorNetworkRegistryAddr) {
        this.sensorNetworkRegistryAddr = sensorNetworkRegistryAddr;
    }

    /**
     * Address of Sensor Network Registry Server
     *
     * @return sensorNetworkRegistryAddr
     */
    public InetAddress getSensorNetworkRegistryAddr() {
        return sensorNetworkRegistryAddr;
    }

    public void setSensorNetworkType(NetworkTypeEnum sensorNetworkType) {
        this.sensorNetworkType = sensorNetworkType;
    }

    public NetworkTypeEnum getSensorNetworkType() {
        return sensorNetworkType;
    }

    public void setSensorBitItemList(List<SensorBitItem> itemList) {
        sensorBitItemList = itemList;
    }

    public List<SensorBitItem> getSensorBitItemList() {
        return sensorBitItemList;
    }

    public void setSensorComponentItemList(List<SensorComponentItem> itemList) {
        sensorComponentItemList = itemList;
    }

    public List<SensorComponentItem> getSensorComponentItemList() {
        return sensorComponentItemList;
    }

    public void setSensorHostID(String id) {
        hostNetworkSettings.setHostSensorID (id);
        this.theProperties.setProperty("sensor.hostid", id);
    }

    public String getSensorHostID() {
        return hostNetworkSettings.getHostSensorID();
    }

    public void setSensorManufacturer(String value) {
        sensorManufacturer = value;
        hostNetworkSettings.setSensorManufacturer(sensorManufacturer);
    }

    public String getSensorManufacturer() {
        return sensorManufacturer;
    }

    public void setSensorModel(String model) {
        sensorModel = model;
        hostNetworkSettings.setSensorModel(model);
    }

    public String getSensorModel() {
        return sensorModel;
    }

    public void setIpLinkSpeed(int speed) {
        ipLinkSpeed = speed;
    }

    public int getIpLinkSpeed() {
        return ipLinkSpeed;
    }

    public SerialSettings getSerialInterface() {
        return serialInterface;
    }

    public SerialSettings getHostSerialInterface() {
        return hostSerialInterface;
    }

    public HostNetworkSettings getHostNetworkSettings() {
        return hostNetworkSettings;
    }

    public SensorNetworkSettings getSensorNetworkSettings() {
        return sensorNetworkSettings;
    }

    /**
     * Parses configuration items starting with "host"
     *
     * @param x - the key
     * @param value
     */
    private void parseHost(String x, String value) throws UnknownHostException {
        switch (x) {
            case "host.network.addresstype":
                // DHCP, FIXED, NA
                hostNetworkSettings.setNetworkAddressType (NetworkAddressTypeEnum.valueOf(value.toUpperCase()));
                break;

            case "host.network.protocol":
                //Open, ISA, VICTORY, OGC
                hostNetworkSettings.setNetworkProtocolType (NetworkProtocolTypeEnum.valueOf(value));
                if (hostNetworkSettings.getNetworkProtocolType () == null) {
                    myLogger.warn("host.network.protocol is null, Setting to Open");
                    hostNetworkSettings.setNetworkProtocolType (NetworkProtocolTypeEnum.Open);
                }
                break;

            case "host.network.registryaddr":
                // address of the registry
                try {
                    hostNetworkSettings.setNetworkRegistryAddr (InetAddress.getByName(value));
                } catch (UnknownHostException ipEx) {
                    myLogger.error("Exception parsing host IP, defaulting to 127.0.0.2", ipEx);
                    hostNetworkSettings.setNetworkRegistryAddr (InetAddress.getByName("127.0.0.2"));
                }
                break;

            case "host.network.time":
                // is there a time server
                hostNetworkSettings.setNetworkTime (Boolean.valueOf(value));
                break;

            case "host.network.timeaddr":
                try {
                    hostNetworkSettings.setNetworkTimeAddr (InetAddress.getByName(value));
                } catch (UnknownHostException ipEx) {
                    myLogger.error("Exception parsing Host Time Address, defaulting to 127.0.0.2", ipEx);
                    hostNetworkSettings.setNetworkTimeAddr (InetAddress.getByName("127.0.0.2"));
                }
                break;

            case "host.network.type":
                // TCP, UDP, etc
                hostNetworkSettings.setNetworkType (NetworkTypeEnum.valueOf(value));
                break;

            case "host.network.iptype":
                // IPv4 or IPv6
                hostNetworkSettings.setNetworkIPType (value);
                break;

            case "host.network.phip":
                //case "sensor.ip":
                try {
                    hostNetworkSettings.setPHIP(InetAddress.getByName(value));
                } catch (UnknownHostException ipEx) {
                    myLogger.error("Exception parsing sensor IP, defaulting to 127.0.0.2", ipEx);
                    hostNetworkSettings.setPHIP(InetAddress.getByName("127.0.0.2"));
                    theProperties.put("host.network.phip", "127.0.0.2");
                }
                break;

            case "host.network.phport":
                //case "sensor.port":
                try {
                    hostNetworkSettings.setPHPort(Integer.parseInt(value));
                    if (hostNetworkSettings.getPHPort() <= 1024) {
                        myLogger.warn("Using privileged port numbers for CCSI is NOT recommended.");
                    }
                    if (hostNetworkSettings.getPHPort() > 65535) {
                        myLogger.error("Port numbers > 65535 are invalid ["
                                + hostNetworkSettings.getPHPort() + "], assuming 5674");
                        hostNetworkSettings.setPHPort(5674);
                        theProperties.put("host.network.phport", "5674");
                    }
                } catch (NumberFormatException portEx) {
                    myLogger.error("Exception parsing sensor port number, assuming 5674", portEx);
                    hostNetworkSettings.setPHPort(5674);
                    theProperties.put("host.network.phport", "5674");
                }
                break;

            case "host.network.gateway":
                //case "sensor.ip.gateway":
                try {
                    hostNetworkSettings.setGatewayAddr(InetAddress.getByName(value));
                } catch (UnknownHostException gwEx) {
                    myLogger.error("Error parsing IP Gateway address, no default", gwEx);
                    hostNetworkSettings.setGatewayAddr(null);
                    theProperties.put("host.network.gateway", " ");
                }
                break;

            case "host.network.netmask":
                //case "sensor.ip.netmask":
                try {
                    hostNetworkSettings.setNetmask(InetAddress.getByName(value));
                } catch (UnknownHostException netMskEx) {
                    myLogger.error("Error parsing IP net mask, assuming 255.255.255.0", netMskEx);
                    hostNetworkSettings.setNetmask(InetAddress.getByName("255.255.255.0"));
                    theProperties.put("host.network.netmask", "255.255.255.0");
                }
                break;

            case "host.network.maxtransmit":
                //case "sensor.ip.maxtransmit":
                try {
                    hostNetworkSettings.setMaxTransmit(Integer.parseInt(value));
                } catch (NumberFormatException maxXmtEx) {
                    myLogger.error("Exception parsing Max Transmit size, assuming 32K", maxXmtEx);
                    hostNetworkSettings.setMaxTransmit(32768);
                    theProperties.put("host.network.maxtransmit", "32768");
                }
                break;

            case "host.network.bdcst":
                //case "sensor.ip.broadcast":
                try {
                    hostNetworkSettings.setBroadcastAddr(InetAddress.getByName(value));
                } catch (UnknownHostException ipBdcstEx) {
                    myLogger.error("Exception parsing IP broadcast address, no default", ipBdcstEx);
                    hostNetworkSettings.setBroadcastAddr(null);
                    theProperties.put("host.network.bdcst", " ");
                }
                break;

            //case "ccsi.host.ip":
            case "host.network.ip":
                try {
                    hostNetworkSettings.setIP (InetAddress.getByName(value));
                } catch (UnknownHostException hostIpEx) {
                    myLogger.error("Exception parsing host IP "
                            + "address, defaulting to 127.0.0.1", hostIpEx);
                    hostNetworkSettings.setIP (InetAddress.getByName("127.0.0.1"));
                    theProperties.put("host.network.ip", "127.0.0.1");
                }
                break;

            case "host.network.port":
                //case "ccsi.host.port":
                try {
                    hostNetworkSettings.setPort(Integer.parseInt(value));
                } catch (NumberFormatException hostPortEx) {
                    myLogger.error("Exception parsing host IP port, "
                            + "defaulting to 5674", hostPortEx);
                    hostNetworkSettings.setPort(5674);
                    theProperties.put("host.network.port", "5674");
                }
                break;
                
            case "host.serial.baud":
                hostSerialInterface.setBaudRate(value);
                break;

            case "host.serial.dev":
                hostSerialInterface.setDevice(value);
                break;

            case "host.serial.databits":
                hostSerialInterface.setDataBits(value);
                break;

            case "host.serial.flowcontrol":
                hostSerialInterface.setFlowControl(value);
                break;

            case "host.serial.parity":
                hostSerialInterface.setParity(value);
                break;

            case "host.serial.startbits":
                hostSerialInterface.setStartBits(value);
                break;

            case "host.serial.stopbits":
                hostSerialInterface.setStopBits(value);
                break;

            case "host.isa.name":
                hostNetworkSettings.setHostName(value);
                break;
                
            case "host.isa.phname":
                hostNetworkSettings.setPHHostName(value);
                break;
                
            case "host.isa.statusinterval":
                hostNetworkSettings.setISAStatusInterval(Integer.valueOf(value));
                break;
                
            default:
                myLogger.error("Unknown configuration file entry [" + x + "] found.");
                break;
        }
    }

    /**
     * Parses configuration items starting with "sensor"
     *
     * @param x - the key
     * @param value
     */
    private void parseSensor(String x, String value) throws UnknownHostException {
        switch (x) {
            case "sensor.model":
                setSensorModel (value);
                break;

            case "sensor.manufacturer":
                setSensorManufacturer (value);
                break;

            case "sensor.hostid":
                hostNetworkSettings.setHostSensorID(value.trim());
                break;

            case "sensor.hostdesc":
                sensorHostDesc = value;
                break;

            case "sensor.network.addresstype":
                // DHCP, FIXED, NA
                sensorNetworkAddressType = NetworkAddressTypeEnum.valueOf(value.toUpperCase());
                break;

            case "sensor.network.protocol":
                //Open, ISA, VICTORY, OGC
                sensorNetworkProtocolType = NetworkProtocolTypeEnum.valueOf(value);
                break;

            case "sensor.network.registryaddr":
                // address of the registry
                try {
                    sensorNetworkRegistryAddr = InetAddress.getByName(value);
                } catch (UnknownHostException ipEx) {
                    myLogger.error("Exception parsing host IP, defaulting to 127.0.0.2", ipEx);
                    sensorNetworkRegistryAddr = InetAddress.getByName("127.0.0.2");
                }
                break;

            case "sensor.network.type":
                // TCP, UDP, etc
                sensorNetworkType = NetworkTypeEnum.valueOf(value);
                break;

            case "sensor.uuid":
                uuid = value;
                hostNetworkSettings.setUuid(value);
                sensorNetworkSettings.setUuid(value);
                break;

            case "sensor.name":
                sensorName = value;
                hostNetworkSettings.setSensorName(value);
                sensorNetworkSettings.setSensorName(value);
                break;

            case "sensor.hostname":
                sensorHostName = value;
                break;

            case "sensor.network.interface":
                //case "sensor.interface":
                try {
                    sensorInterfaceType = ConnSensorNetEnum.valueOf(value);
                } catch (Exception siEx) {
                    myLogger.error("Exception parsing sensor "
                            + "interface type, assuming Serial interface", siEx);
                    sensorInterfaceType = ConnSensorNetEnum.SERIAL;
                    theProperties.put("sensor.network.interface", "SERIAL");
                }
                break;

            case "sensor.network.phip":
                //case "sensor.ip":
                try {
                    sensorIP = InetAddress.getByName(value);
                } catch (UnknownHostException ipEx) {
                    myLogger.error("Exception parsing sensor IP, defaulting to 127.0.0.2", ipEx);
                    sensorIP = InetAddress.getByName("127.0.0.2");
                    theProperties.put("sensor.network.phip", "127.0.0.2");
                }
                break;

            case "sensor.network.phport":
                //case "sensor.port":
                try {
                    sensorPHPort = Integer.parseInt(value);
                    if (sensorPHPort <= 1024) {
                        myLogger.warn("Using privileged port numbers for CCSI is NOT recommended.");
                    }
                    if (sensorPHPort > 65535) {
                        myLogger.error("Port numbers > 65535 are invalid ["
                                + this.sensorPHPort + "], assuming 5674");
                        sensorPHPort = 5674;
                        theProperties.put("sensor.network.phport", "5674");
                    }
                } catch (NumberFormatException portEx) {
                    myLogger.error("Exception parsing sensor port number, assuming 5674", portEx);
                    sensorPHPort = 5674;
                    theProperties.put("sensor.network.phport", "5674");
                }
                break;

            case "sensor.serial":
                serial = value;
                hostNetworkSettings.setSensorSerialNumber(serial);
                break;

            case "sensor.serial.baud":
                serialInterface.setBaudRate(value);
                break;

            case "sensor.serial.dev":
                serialInterface.setDevice(value);
                break;

            case "sensor.serial.databits":
                serialInterface.setDataBits(value);
                break;

            case "sensor.serial.flowcontrol":
                serialInterface.setFlowControl(value);
                break;

            case "sensor.serial.parity":
                serialInterface.setParity(value);
                break;

            case "sensor.serial.startbits":
                serialInterface.setStartBits(value);
                break;

            case "sensor.serial.stopbits":
                serialInterface.setStopBits(value);
                break;

            case "sensor.network.gateway":
                //case "sensor.ip.gateway":
                try {
                    sensorIPGateway = InetAddress.getByName(value);
                } catch (UnknownHostException gwEx) {
                    myLogger.error("Error parsing IP Gateway address, no default", gwEx);
                    sensorIPGateway = null;
                    theProperties.put("sensor.network.gateway", " ");
                }
                break;

            case "sensor.network.netmask": 
                //case "sensor.ip.netmask":
                try {
                    sensorIPNetmask = InetAddress.getByName(value);
                } catch (UnknownHostException netMskEx) {
                    myLogger.error("Error parsing IP net mask, assuming 255.255.255.0", netMskEx);
                    sensorIPNetmask = InetAddress.getByName("255.255.255.0");
                    theProperties.put("sensor.network.netmask", "255.255.255.0");
                }
                break;

            case "sensor.network.maxtransmit":
                //case "sensor.ip.maxtransmit":
                try {
                    sensorMaxTransmit = Integer.parseInt(value);
                } catch (NumberFormatException maxXmtEx) {
                    myLogger.error("Exception parsing Max Transmit size, assuming 32K", maxXmtEx);
                    sensorMaxTransmit = (32 * 1024);
                    theProperties.put("sensor.network.maxtransmit", "32768");
                }
                break;

            case "sensor.network.bdcst":
                //case "sensor.ip.broadcast":
                try {
                    sensorIPBroadcast = InetAddress.getByName(value);
                } catch (UnknownHostException ipBdcstEx) {
                    myLogger.error("Exception parsing IP broadcast address, no default", ipBdcstEx);
                    sensorIPBroadcast = null;
                    theProperties.put("sensor.network.bdcst", " ");
                }
                break;

            case "sensor.swVersion":
                try {
                    this.swVersion = new VersionNumberType(value);
                } catch (Exception swVersEx) {
                    myLogger.error("Exception parsing sensor software"
                            + " version, defaulting to 999.999.999.999",
                            swVersEx);
                    swVersion = new VersionNumberType("999.999.999.999");
                    theProperties.put("sensor.swVersion", "999.999.999.999");
                }
                break;

            case "sensor.hwVersion":
                try {
                    hwVersion = new VersionNumberType(value);
                } catch (Exception hwVersEx) {
                    myLogger.error("Exception parsing sensor hardware "
                            + "version, defaulting to 999.999.999.999", hwVersEx);
                    hwVersion = new VersionNumberType("999.999.999.999");
                    theProperties.put("sensor.hwVersion", "999.999.999.999");
                }
                break;

            case "sensor.ccsiVersion":
                try {
                    ccsiVersion = new VersionNumberType(value);
                } catch (Exception ccsiVersEx) {
                    myLogger.error("Exception parsing sensor CCSI "
                            + "version, defaulting to 1.1.1.0", ccsiVersEx);
                    swVersion = new VersionNumberType("1.1.1.0");
                    theProperties.put("sensor.ccsiVersion", "1.1.1.0");
                }
                break;

            case "sensor.type":
                try {
                    sensorType = CcsiSensorTypeEnum.valueOf(value);
                } catch (Exception sensTypeEx) {
                    myLogger.error("Exception parsing sensor type, "
                            + "defaulting to chemical sensor", sensTypeEx);
                    sensorType = CcsiSensorTypeEnum.CHM;
                    theProperties.put("sensor.type", "CHM");
                }
                break;

            case "sensor.variant":
                try {
                    variant = CcsiSensorVariantEnum.valueOf(value);
                } catch (Exception sensVarEx) {
                    myLogger.error("Exception parsing sensor variant, "
                            + "defaulting to PNT", sensVarEx);
                    variant = CcsiSensorVariantEnum.PNT;
                    theProperties.put("sensor.variant", "PNT");
                }
                break;

            case "sensor.oi.server":
                try {
//                    hostNetworkSettings.setOIServer (Boolean.parseBoolean(value));
                    sensorNetworkSettings.setOiServer (Boolean.parseBoolean(value));
                } catch (Exception oiSvrEx) {
                    myLogger.error("Exception parsing open interface"
                            + " server flag, defaulting to true", oiSvrEx);
                    hostNetworkSettings.setOIServer (true);
                    theProperties.put("sensor.oi.server", "true");
                }
                break;

            case "sensor.version":
                sensorVersion = value;
                break;

            case "sensor.network.ip":
                try {
                    sensorIP = InetAddress.getByName(value);
                    sensorNetworkSettings.setIP(sensorIP);
                } catch (UnknownHostException hostIpEx) {
                    myLogger.error("Exception parsing host IP "
                            + "address, defaulting to 127.0.0.1", hostIpEx);
                    sensorIP = InetAddress.getByName("127.0.0.1");
                    sensorNetworkSettings.setIP(sensorIP);
                    theProperties.put("sensor.network.ip", "127.0.0.1");
                }
                break;

            case "sensor.network.port":
                try {
                    sensorPort = Integer.parseInt(value);
                    sensorNetworkSettings.setPort(sensorPort);
                } catch (NumberFormatException hostPortEx) {
                    myLogger.error("Exception parsing sensor IP port, "
                            + "defaulting to 5674", hostPortEx);
                    sensorPort = 5674;
                    sensorNetworkSettings.setPort(sensorPort);
                    theProperties.put("sensor.port", "5674");
                }
                break;

            case "sensor.mounting":
                sensorMountingType = CcsiSensorMountingEnum.valueOf(value);
                break;
                
            default:
                if (x.startsWith("sensor.bit")) {
                    sensorBitItemList.add(new SensorBitItem(value));
                } else if (x.startsWith("sensor.detect.")) {
                    detectableItems.add(value);
                } else if (x.startsWith("sensor.component")) {
                    sensorComponentItemList.add(new SensorComponentItem(value));
                } else if (x.startsWith("sensor.cmd.handling")) {
                    String[] flds = value.split("\\,");
                    CommandHandlingType cmd = new CommandHandlingType();
//                    int index = -1;
                    for (Integer index = 0; index < flds.length; index++) {
//                        index++;
                        switch (index) {
                            case 0:
                                cmd.setCommand(flds[0].trim());
                                break;
                            case 1:
                                try {
                                    cmd.setHandle(CommandHandlingTypeEnum.valueOf(flds[1].trim()));
                                } catch (Exception chtEx) {
                                    myLogger.error("Exception parsing command handling type, no default",
                                            chtEx);
                                }
                                break;
                            case 2:
                                try {
                                    cmd.setAckRequired(Boolean.parseBoolean(flds[2].trim()));
                                } catch (Exception charEx) {
                                    myLogger.error("Exception parsing command "
                                            + "ACK required, no default", charEx);
                                }
                                break;
                            case 3:
                                if (flds[3].trim().length() == 1) {
                                    char v = flds[3].trim().charAt(0);
                                    cmd.setResponseChannel(CcsiChannelEnum.value(v));
                                }
                                break;
                            default:
                                myLogger.error("Invalid command handling value, too many values");
                                break;
                        }
                    }
                    cmdHandling.addCommand(cmd);
                } else {
                    myLogger.error("Unknown configuration file entry [" + x + "] found.");
                }
                break;
        }
    }

    /**
     * Parses configuration items starting with "ccsi"
     *
     * @param x - the key
     * @param value
     */
    private void parseCCSI(String x, String value) throws UnknownHostException {
        switch (x) {
            case "ccsi.enforce.time":
                hostNetworkSettings.setEnforceTime (Boolean.valueOf(value));
                break;

            case "ccsi.enforce.timeValue":
                hostNetworkSettings.setEnforceTimeValue (Integer.parseInt(value));
                break;

            case "ccsi.unidirectional":
                unidirectional = Boolean.valueOf(value);
                break;

             case "ccsi.sensor.classification":
                if (value != null)
                {
                    hostNetworkSettings.setClassification (" " + value + " ");
                }
                break;

            case "ccsi.sensor.dataclassification":
                sensorDataClassification = " " + value + " ";
                break;

            case "ccsi.sensor.commclassification":
                sensorCommClassification = " " + value + " ";
                break;

            case "ccsi.enforce.strikes":
                try {
//                    hostNetworkSettings.setEnforceStrikes (Boolean.parseBoolean(value));
                    sensorNetworkSettings.setEnforceStrikes (Boolean.parseBoolean(value));
                } catch (Exception strikesEx) {
                    myLogger.error("Invalid 3 strikes enforce flag, default to true", strikesEx);
//                    hostNetworkSettings.setEnforceStrikes (true);
                    sensorNetworkSettings.setEnforceStrikes (true);
                    theProperties.put("ccsi.enforce.strikes", "true");
                }
                break;

            case "ccsi.conn.limit":
                try {
                    connLimit = Integer.parseInt(value);
                } catch (NumberFormatException connLimEx) {
                    myLogger.error("Exception parsing connection limit, assuming one", connLimEx);
                    connLimit = 0;
                    theProperties.put("ccsi.conn.limit", "0");
                }
                break;

            case "ccsi.conn.failure.timeout":
                try {
                    connFailureTimeout = Integer.parseInt(value);
                } catch (NumberFormatException scftEx) {
                    myLogger.error("Exception parsing conn failure timeout, "
                            + "defaulting to 5 minutes", scftEx);
                    connFailureTimeout = 300;  // Default of 5 minutes
                    theProperties.put("ccsi.conn.failure.timeout", "300");
                }
                break;

            case "ccsi.encrypt.data":
                try {
                    encryptData = Boolean.parseBoolean(value);
                } catch (Exception encryptDataEx) {
                    myLogger.error("Exception parsing sensor encrypt "
                            + "data flag, defaulting to false", encryptDataEx);
                    encryptData = false;
                    theProperties.put("sensor.encrypt.data", "false");
                }
                break;

            case "ccsi.encrypt.comm":
                try {
                    hostNetworkSettings.setEncryptComm (Boolean.parseBoolean(value));
                } catch (Exception encryptCommEx) {
                    myLogger.error("Exception parsing sensor encrypt "
                            + "comms, no default", encryptCommEx);
                    hostNetworkSettings.setEncryptComm (false);
                    theProperties.put("sensor.encrypt.comm", "false");
                }
                break;

            case "ccsi.encrypt.type":
                encryptType = value;
                break;

            case "ccsi.ack.timeout":
                try {
                    ackTimeout = Integer.parseInt(value);
                    if ((ackTimeout < 5) || (ackTimeout > 300)) {
                        myLogger.error("Invalid ack timeout [" + value
                                + "], must be >= 5 and <= 300");
                        ackTimeout = 5;
                        theProperties.put("sensor.ack.timeout", "5");
                    }
                } catch (NumberFormatException ackTmoEx) {
                    myLogger.error("Exception parsing sensor ack "
                            + "timeout, defaulting to 5 seconds", ackTmoEx);
                    ackTimeout = 5;
                    theProperties.put("sensor.ack.timeout", "5");
                }
                break;

            case "ccsi.conn.retry.time":
                try {
//                    hostNetworkSettings.setConnRetryTime (Integer.parseInt(value));
                    sensorNetworkSettings.setConnRetryTime (Integer.parseInt(value));
                } catch (NumberFormatException connRetryTmoEx) {
                    myLogger.error("Exception parsing sensor conn "
                            + "retry timeout, defaulting to 5 seconds",
                            connRetryTmoEx);
//                    hostNetworkSettings.setConnRetryTime (5);
                    sensorNetworkSettings.setConnRetryTime (5);
                    theProperties.put("sensor.conn.retry.time", "5");
                }
                break;

            case "ccsi.conn.retry.count":
                try {
//                    hostNetworkSettings.setConnRetryCount (Integer.parseInt(value));
                    sensorNetworkSettings.setConnRetryCount (Integer.parseInt(value));
                } catch (NumberFormatException connRetryCntEx) {
                    myLogger.error("Exception parsing sensor conn "
                            + "retry count, defaulting to 3", connRetryCntEx);
//                    hostNetworkSettings.setConnRetryCount (3);
                    sensorNetworkSettings.setConnRetryCount (3);
                    theProperties.put("sensor.conn.retry.count", "3");
                }

                break;

            default:
                if (x.startsWith("ccsi.channel.supported")) {
                    String[] keyFields = x.split("\\.");
                    ChannelSupport channelSupport
                            = theChannels.getChannelByKeyNumber(keyFields[3]);
                    switch (keyFields.length) {
                        case 4:
                            // Provides the channel name
                            CcsiChannelEnum channel = CcsiChannelEnum.valueOfEnglish(value);
                            channelSupport.setChannel(channel);
                            channelSupport.setSupported(true);
                            break;
                        case 5:
                            // this is a sub element
                            switch (keyFields[4]) {
                                case "meta":
                                    channelSupport.setMetadata(value);
                                    break;
                                case "sud":
                                    channelSupport.setSUD(value);
                                    break;
                                case "compress":
                                    channelSupport.setCompressionThreshold(
                                            Integer.parseInt(value));
                                    break;
                                default:
                                    myLogger.error("Invalid configuration key: " + x);
                                    break;
                            }
                            break;
                        default:
                            myLogger.error("Error in config file key= " + x);
                            break;
                    }
                } else if (x.startsWith("ccsi.cmd.handling")) {
                    String[] flds = value.split("\\,");
                    CommandHandlingType cmd = new CommandHandlingType();
//                    int index = -1;
                    for (Integer index = 0; index < flds.length; index++) {
//                        index++;
                        switch (index) {
                            case 0:
                                cmd.setCommand(flds[0].trim());
                                break;
                            case 1:
                                try {
                                    cmd.setHandle(CommandHandlingTypeEnum.valueOf(flds[1].trim()));
                                } catch (Exception chtEx) {
                                    myLogger.error("Exception parsing command handling type, no default",
                                            chtEx);
                                }
                                break;
                            case 2:
                                try {
                                    cmd.setAckRequired(Boolean.parseBoolean(flds[2].trim()));
                                } catch (Exception charEx) {
                                    myLogger.error("Exception parsing command "
                                            + "ACK required, no default", charEx);
                                }
                                break;
                            case 3:
                                if (flds[3].trim().length() == 1) {
                                    char v = flds[3].trim().charAt(0);
                                    cmd.setResponseChannel(CcsiChannelEnum.value(v));
                                }
                                break;
                            default:
                                myLogger.error("Invalid command handling value, too many values");
                                break;
                        }
                    }
                    cmdHandling.addCommand(cmd);
                } else if (x.startsWith("ccsi.setcfg.reboot")) {
                    rebootList.add(value);
                } else if (x.startsWith("ccsi.suconfig")) {
                    sensorUniqueConfigList.add(new SensorUniqueConfigItem(value));
                } else if (x.startsWith("ccsi.sudata")) {
                    sensorUniqueDataList.add(new SensorUniqueDataItem(value));
                } else {
                    myLogger.error("Unknown configuration file entry [" + x + "] found.");
                }
                break; 
        }
    }
}
