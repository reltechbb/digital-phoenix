/*
 * SensorUniqueDataItem.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Encapsulates a Sensor Unique Data Item
 *
 */
package com.domenix.ccsi.config;

import com.domenix.common.config.SensorUniqueConfigItem;
import org.apache.log4j.Logger;

/**
 * Encapsulates a Sensor Unique Data Item
 *
 * @author jmerritt
 */
public class SensorUniqueDataItem
{
    private String itemName;
    private String itemType;
    private boolean editable;
    private boolean usedInVersion;
    private String value;

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger = Logger.getLogger(SensorUniqueConfigItem.class);

    /**
     * Constructor
     * @param item - comma separated list
     */
    public SensorUniqueDataItem(String item)
    {
        String[] itemList = item.split("\\,");
        if (itemList.length == 4)
        {
            itemName = itemList[0];
            itemType = itemList[1];
            editable = Boolean.getBoolean(itemList[2]);
            usedInVersion = Boolean.getBoolean(itemList[3]);
        } else
        {
            myLogger.error("incorrect number arguments - ccsi.sudata: " + item);
        }
    }

    public boolean isEditable ()
    {
        return editable;
    }
    
    public boolean isUsed ()
    {
        return usedInVersion;
    }
    
    public void setItemName(String name)
    {
        itemName = name;
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemType(String type)
    {
        itemType = type;
    }

    public String getItemType()
    {
        return itemType;
    }
    
    public String getValue ()
    {
        return value;
    }
    
    public void setValue (String value)
    {
        this.value = value;
    }
}
