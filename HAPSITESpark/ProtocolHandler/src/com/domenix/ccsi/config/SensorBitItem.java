/*
 * SensorBitItem.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Encapsulates a SensorBitItem
 *
 */
package com.domenix.ccsi.config;

import org.apache.log4j.Logger;

/**
 * Encapsulates a SensorBitItem
 * @author jmerritt
 */
public class SensorBitItem
{
    String command;
    String level;
    String args;
    
    private final Logger logger  = Logger.getLogger(SensorBitItem.class);
    
    /**
     * Constructor
     * 
     * @param value comma separated list of 3 items
     */
    public SensorBitItem (String value)
    {
        String [] valueList = value.split("\\,");
        if (valueList.length == 3)
        {
            command = valueList [0];
            level = valueList [1];
            args = valueList [2];
        } else
        {
            logger.error ("Incorrect number arguments: " + value);
        }
    }
    
    public void setCommand (String command)
    {
        this.command = command;
    }
    
    public String getCommand ()
    {
        return command;
    }
    
    public void setLevel (String level)
    {
        this.level = level;
    }
    
    public String getLevel ()
    {
        return level;
    }
    
    public void setArgs (String args)
    {
        this.args = args;
    }
    
    public String getArgs ()
    {
        return args;
    }
}
