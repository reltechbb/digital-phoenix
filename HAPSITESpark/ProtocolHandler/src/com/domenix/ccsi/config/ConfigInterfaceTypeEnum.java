/*
 * ConfigInterfaceTypeEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of enumerated interface types that can be used by a sensor.
 *
 * Version: V1.0  15/08/29
 */


package com.domenix.ccsi.config;

/**
 * This enumerates the types of interfaces that can be used by the sensor, each type will have
 * an implementation of the NSDS module to support registration and communications on that type.
 *
 * @author kmiller
 */
public enum ConfigInterfaceTypeEnum
{
  /** The CCSI Open Interface */
  Open ,

  /** Other interface */
  Other;
}
