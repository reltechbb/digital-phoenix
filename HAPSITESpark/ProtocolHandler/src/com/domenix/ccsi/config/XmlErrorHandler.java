/*
 * XmlErrorHandler.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This class provides the XmlErrorHandler for parsing and reading schemas and
 * XML files.
 *
 */
package com.domenix.ccsi.config;

//~--- non-JDK imports --------------------------------------------------------

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

//~--- classes ----------------------------------------------------------------

/**
 * This class provides the XmlErrorHandler for parsing and reading schemas and
 * XML files.
 * 
 * @author kmiller
 */
public class XmlErrorHandler implements org.xml.sax.ErrorHandler
{
  /** The saved state */
  private final CcsiSavedState theState;
  
  /** Creates a new instance of XmlErrorHandler
   * 
   * @param state the saved state
   */
  public XmlErrorHandler( CcsiSavedState state )
  {
    this.theState = state;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Method to handle fatal parse errors
   *
   * @param exception
   *
   * @throws SAXException
   */
  @Override
  public void fatalError( SAXParseException exception ) throws SAXException
  {
    StringBuilder err = new StringBuilder( "Fatal XML parse error:\n  Line: " );
    err.append( exception.getLineNumber() ).append( " Col: " ).append( exception.getColumnNumber() );
    err.append( "\n  Error=" ).append( exception.getLocalizedMessage() ).append( "\n  " );
    err.append( "\n  PublicID: " ).append( exception.getPublicId() ).append( "  SystemID: " ).append( exception.getSystemId() );
    err.append( "\n  Cause: " ).append( exception.getCause().getLocalizedMessage() );

    this.theState.getParseErrors().add( err.toString() );
  }

  /**
   * Method to handle parse errors
   *
   * @param exception
   *
   * @throws SAXException
   */
  @Override
  public void error( SAXParseException exception ) throws SAXException
  {
    StringBuilder err = new StringBuilder( "XML parse error:\n  Line: " );
    err.append( exception.getLineNumber() ).append( " Col: " ).append( exception.getColumnNumber() );
    err.append( "\n  Error=" ).append( exception.getLocalizedMessage() ).append( "\n  " );
    err.append( "\n  PublicID: " ).append( exception.getPublicId() ).append( "  SystemID: " ).append( exception.getSystemId() );
    err.append( "\n  Cause: " ).append( exception.getCause().getLocalizedMessage() );

    this.theState.getParseErrors().add( err.toString() );
  }

  /**
   * Method to handle parse warnings
   *
   * @param exception
   *
   * @throws SAXException
   */
  @Override
  public void warning( SAXParseException exception ) throws SAXException
  {
    StringBuilder err = new StringBuilder( "XML parse warning:\n  Line: " );
    err.append( exception.getLineNumber() ).append( " Col: " ).append( exception.getColumnNumber() );
    err.append( "\n  Error=" ).append( exception.getLocalizedMessage() ).append( "\n  " );
    err.append( "\n  PublicID: " ).append( exception.getPublicId() ).append( "  SystemID: " ).append( exception.getSystemId() );
    err.append( "\n  Cause: " ).append( exception.getCause().getLocalizedMessage() );

    this.theState.getParseErrors().add( err.toString() );
  }
}
