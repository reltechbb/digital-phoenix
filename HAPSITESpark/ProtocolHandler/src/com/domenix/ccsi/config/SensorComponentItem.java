/*
 * SensorComponentItem.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Encapsulates a Sensor Component
 *
 */
package com.domenix.ccsi.config;

import com.domenix.ccsi.CcsiComponentEnum;
import com.domenix.common.spark.data.AnnunciatorType;

/**
 * Encapsulates a Sensor Component
 * @author jmerritt
 */
public class SensorComponentItem
{
    private CcsiComponentEnum component;
    private AnnunciatorType annunciatorType;
    private String name;
    
    /**
     * Constructor
     * @param value - comma separated list of 3 items
     */
    public SensorComponentItem (String value)
    {
        String [] valueList = value.split("\\,");
        if (valueList.length == 3)
        {
            component = CcsiComponentEnum.valueOf(valueList [0]);
            annunciatorType = AnnunciatorType.valueOf(valueList [1]);
            name = valueList [2];
        }
    }
    
    public void setComponent (CcsiComponentEnum value)
    {
        component = value;
    }
    
    public CcsiComponentEnum getComponent ()
    {
        return component;
    }
    
    public void setAnnunciatorType (AnnunciatorType value)
    {
        annunciatorType = value;
    }
    
    public AnnunciatorType getAnnunciatorType ()
    {
        return annunciatorType;
    }
    
    public void setName (String name)
    {
        this.name = name;
    }
    
    public String getName ()
    {
        return name;
    }
}
