/*
 * NameValueTypeTriplet.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This class maintains a triplet of strings that act as a named item, 
 * it's assigned value and the type of value.
 *
 */
package com.domenix.ccsi;

import com.domenix.ccsi.protocol.DataValidityType;

/**
 * This class maintains a triplet of strings that act as a named item, 
 * it's assigned value and the type of value.
 *
 * @author jmerritt
 */
public class NameValueTypeTriplet
{
  /** The name */
  private String	theName	= null;

  /** The value */
  private String	theValue	= null;

  /** The Type - used for validation**/
  private DataValidityType theValidityType = null;
  
  //~--- constructors ---------------------------------------------------------

  /**
   * Default constructor.
   */
  public NameValueTypeTriplet()
  {
  }

  /**
   * Construct the triplet.
   *
   * @param n String containing the name
   * @param v String containing the value
   * @param t String containing the type of the value
   */
  public NameValueTypeTriplet( String n , String v, String t )
  {
    theName		= n;
    theValue	= apostropheHandler( v );
    theValidityType = DataValidityType.getEnum(t);
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Get the pair name value
   *
   * @return String containing the name
   */
  public String getTheName()
  {
    return theName;
  }

  /**
   * Get the pair value.
   *
   * @return String containing the value
   */
  public String getTheValue()
  {
    return theValue;
  }
  
  /**
   * Gets the validity type
   * @return 
   */
  public DataValidityType getTheValidationType ()
  {
      return theValidityType;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * Set the pair name to the provided value.
   *
   * @param theName String containing the name
   */
  public void setTheName( String theName )
  {
    this.theName	= theName;
  }

  /**
   * Set the value portion of the pair to the provided string.
   *
   * @param theValue String containing the value
   */
  public void setTheValue( String theValue )
  {
    this.theValue	= apostropheHandler( theValue );
  }
  
  /**
   * Sets the data validity type
   * @param v 
   */
  public void setDataValidityType (DataValidityType v)
  {
      this.theValidityType = v;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Returns the current object state.
   *
   * @return String containing the state
   */
  @Override
  public String toString()
  {
    StringBuilder	temp	= new StringBuilder( "NV Pair: " );

    temp.append( theName );
    temp.append( "=>" );
    temp.append( theValue );
    temp.append (" Type: ");
    temp.append (theValidityType.toString());
    temp.append( '\n' );

    return ( temp.toString() );
  }

  /**
   * This method replaces all apostrophe characters within the string with
   * a correct Java form of the apostrophe.  This is applied to the value
   * portion only.
   *
   * @param inp String containing the NV pair value being set.
   *
   * @return String with all apostrophe characters replaced
   */
  private String apostropheHandler( String inp )
  {
    String retValue;

    if ( inp.indexOf( '\'' ) >= 0 )
    {
      retValue	= inp.replaceAll( "'" , "''" );
    }
    else
    {
      retValue	= inp;
    }

    return ( retValue );
  }
}