/*
 * CcsiSensorMountingEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the enumeration of CCSI sensor mounting locations.
 *
 * Version: V1.0  15/08/30
 */


package com.domenix.ccsi;

/**
 * This enumerates the mounting location of a CCSI sensor.
 *
 * @author kmiller
 */
public enum CcsiSensorMountingEnum
{
  /** Dismounted */
  DISMTD ,

  /** Externally mounted */
  EXTERNL ,

  /** Internally mounted */
  INTRNL ,

  /** Remotely set up/mounted */
  REMOTE;
}
