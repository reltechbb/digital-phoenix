/*
 * SparkMain.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the main function for the Spark CCSI
 *
 */
package com.domenix.ccsi.main;

import com.domenix.ccsi.CcsiBitStatusEnum;
import com.domenix.ccsi.CcsiComponentStateEnum;
import com.domenix.commonha.ccsi.CcsiModeEnum;
import com.domenix.common.spark.CtlWrapper;
import com.domenix.common.spark.CtlWrapperQueue;
import com.domenix.ccsi.VersionNumberType;
import com.domenix.ccsi.cache.CcsiCacheManager;
import com.domenix.ccsi.protocol.CcsiProtocolHandler;
import com.domenix.ccsi.sensoradapter.SensorAdapter;
import com.domenix.ccsi.config.CcsiConfigSensor;
import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.common.config.InterfaceSettingsBase;
import com.domenix.commonha.queues.LinkEventQueue;
import com.domenix.commonha.queues.NSDSXferEventQueue;
import com.domenix.commonha.queues.ConnEventQueue;
import com.domenix.commonha.intfc.NSDSInterface;
import com.domenix.commonha.intfc.HostProtocolAdapterInterface;
import com.domenix.common.spark.IPCWrapper;
import com.domenix.common.spark.IPCWrapperQueue;
import com.domenix.common.spark.data.MsgContent;
import com.domenix.common.spark.data.ObjectFactory;
import com.domenix.common.spark.data.ResponseMsg;
import com.domenix.common.spark.data.RoutingKeyEnum;
import com.domenix.common.spark.IPCQueueEvent;
import com.domenix.common.spark.IPCQueueListenerInterface;
import com.domenix.common.spark.data.CommandMsg;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.commonha.queues.HAQueues;
import com.domenix.common.utils.SparkThreadFactory;
import com.domenix.common.utils.TimerChangeEvent;
import com.domenix.common.utils.TimerPurposeEnum;
import com.domenix.common.utils.UtilTimer;
import com.domenix.common.utils.UtilTimerListener;
import com.domenix.commonsa.interfaces.SAQueues;
import com.domenix.commonsa.interfaces.SIAConnection;
import com.domenix.commonsa.interfaces.SIAXferEventQueue;
import com.domenix.commonsa.interfaces.SaConnEventQueue;
import com.domenix.commonsa.interfaces.SaLinkEventQueue;
import com.domenix.commonsa.interfaces.SensorNetworkSettings;
import com.domenix.commonsa.interfaces.SensorProtocolAdapterReaderInterface;
import com.domenix.commonsa.interfaces.SensorProtocolAdapterWriterInterface;
import com.domenix.sensorinterfaceadapter.SIAInterface;
import com.domenix.sensorinterfaceadapter.SIAInterfaceEthernetImpl;
import com.domenix.sensorinterfaceadapter.SIALinkEthernetImpl;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * The main Protocol Handler function
 * 
 * @author jmerritt
 */
public class DigitalPhoenixSparkMain implements Runnable, UtilTimerListener,
        IPCQueueListenerInterface
{

    // *** constants for reading environment variables ***
    private static final String CCSI_BASE_DIR = "ccsi.baseDir";
    private static final String SENSOR_PA_READER_CLASS = "ccsi.spa.reader";
    private static final String SENSOR_PA_WRITER_CLASS = "ccsi.spa.writer";
    private static final String SENSOR_IA_READER_CLASS = "ccsi.sia.reader";
    private static final String SENSOR_IA_WRITER_CLASS = "ccsi.sia.writer";
    private static final String SENSOR_IA_CONFIG_FILE_NAME = "ccsi.sia.configFile";
    private static final String SENSOR_PA_CONFIG_FILE_NAME = "ccsi.spa.configFile";
    private static final String HOST_IA_CONFIG_FILE_NAME = "ccsi.hia.configFile";
    private static final String HOST_PA_CONFIG_FILE_NAME = "ccsi.hpa.configFile";
    private static final String HOST_PA_IMPLEMENTATION = "ccsi.hpa.impl";
    private static final String HOST_IA_IMPLEMENTATION = "ccsi.hia.impl";

    /* where SparkCCSI is installed */
    private static String ccsiBaseDir;

    /* Sensor Interface Adapter Reader */
    private static String siaReaderName;
    /* Sensor Interface Adapter Writer */
    private static String siaWriterName;
    /* Sensor Protocol Adapter Reader */
    private static String spaReaderName;
    /* Sensor Protocol Adapter Writer */
    private static String spaWriterName;
    /* Sensor Interface Adapter configuration file name */
    private static String siaConfigFileName;
    /* Sensor Protocol Adapter configuration file name */
    private static String spaConfigFileName;
    /* Sensor Protocol Adapter Reader */
    private static String hpaImpl;
    /* Sensor Protocol Adapter Writer */
    private static String hiaImpl;
    /* Sensor Interface Adapter configuration file name */
    private static String hiaConfigFileName;
    /* Sensor Protocol Adapter configuration file name */
    private static String hpaConfigFileName;
    /**
     * The executor for service threads
     */
    private static ExecutorService executor;

    /**
     * The cache manager
     */
    private CcsiCacheManager theCacheManager;

    /**
     * Process a timer entry
     */
    private boolean processTimerQueue = false;

    /**
     * Final shutdown flag
     */
    private static final AtomicBoolean FINALSHUTDOWNFLAG = new AtomicBoolean(false);

    /**
     * Unprocessed timer events
     */
    private LinkedBlockingQueue<TimerChangeEvent> timerQueue;

    /**
     * Need to send a status response
     */
    private boolean sendStatusResponse = false;

    /**
     * The status value to send back to the sensor
     */
    private boolean statusResponse = false;

    /**
     * A reason message to send back to the sensor.
     */
    private String sendStatusReason = null;

    /**
     * Process a control command
     */
    private boolean processControlCmd = false;

    /**
     * Unprocessed control commands
     */
    private LinkedBlockingQueue<CtlWrapper> ctlCommands;

    /**
     * Startup received and successfully processed flag
     */
    static boolean startedUp = false;

    /**
     * Synchronization variable
     */
    private final Object syncObject = new Object ();

    /**
     * The timer base.
     */
    private UtilTimer baseTimer;

    /**
     * The error/debug logger.
     */
    private Logger myLogger;

    /**
     * The NSDS Open Interface
     */
    private NSDSInterface nsdsHostOpenInterface;
    private HostProtocolAdapterInterface theHPAImpl;

    
    private SIAInterfaceEthernetImpl sensorOpenInterface;     //SIAInterface sensorOpenInterface;
    
    private SaLinkEventQueue siaLinkEventQ;          
    private CtlWrapperQueue siaCtlQueue;    
    private SIAXferEventQueue siaTransferQueue;
    private SIAXferEventQueue spaTransferQueue;
    private SIAXferEventQueue phTransferQueue;
    private SaConnEventQueue siaConnEventQueue;
    
    /**
     * The NSDS control IPC Queue
     */
    private CtlWrapperQueue nsdsCtlQueue; // to HPA
    private CtlWrapperQueue hpaCtlQueue;  // tp HIA

    /**
     * The CCSI Protocol Handler control IPC Queue
     */
    private CtlWrapperQueue phCtlQueue;

    /**
     * The CCSI Protocol Handler to Sensor Adapter IPC
     */
    //private IPCWrapperQueue spWriterDataQueue;
    private IPCWrapperQueue spWriterDataQueue;

    /**
     * The sensor configuration information.
     */
    private CcsiConfigSensor sensorConfig;

    /** The Hoist Network Information */
    private HostNetworkSettings hostNetworkSettings;
    private SensorNetworkSettings sensorNetworkSettings;
    /**
     * The saved sensor state information.
     */
    private CcsiSavedState sensorState;

    /**
     * The CCSI Protocol Handler
     */
    private CcsiProtocolHandler theProtocolHandler;

    /**
     * The Sensor Adapter control IPC Queue
     */
    private CtlWrapperQueue sensorAdapterQueue;

    /**
     * Sensor Protocol Adapter to CCSI Protocol Handler IPC
     */
    private IPCWrapperQueue spAdapterToPh;

    /**
     * Sensor Interface Adapter to CCSI Protocol Handler IPC
     */
    private IPCWrapperQueue siAdapterToSp;

    /**
     * Sensor Interface Adapter to CCSI Protocol Handler IPC
     */
    //private IPCWrapperQueue spAdapterToSi;
    private IPCWrapperQueue spAdapterToSi;

    /**
     * The Sensor Protocol Adapter control IPC Queue
     */
    //private IPCWrapperQueue readerDataQueue;

    /**
     * The Main control IPC Queue
     */
    private CtlWrapperQueue mainCtlQueue;

    /**
     * A link event queue
     */
    private LinkEventQueue nsdsLinkEventQ;

    /**
     * The transfer event queue
     */
    private NSDSXferEventQueue hiaTransferQueue;
    private NSDSXferEventQueue hpaXferEventQueue;

      /** The Sensor Adapter control IPC Queue */
    private ConnEventQueue connEventQueue;
    private ConnEventQueue hpaConnEventQueue;

      /** The timer for tasks. */
    private Timer ccvTimer = null;

      /** The Sensor Adapter component. */
    private SensorAdapter theSensorProtocolAdapter;

    private static SparkThreadFactory sparkPoolThreadFactory;
    
    private SAQueues saQueues = new SAQueues();
    private HAQueues haQueues = new HAQueues ();
    
    private SIALinkEthernetImpl mySIALink;
    private SIAConnection mySIAConnection;
    
    
    
    /**
     * Constructs the class instance providing the base directory from the
     * command line.
     *
     * 
     */
    public DigitalPhoenixSparkMain()
    {
//        System.out.println("SparkMain.constructor -> start.");
                    
        try
        {
            this.myLogger = Logger.getLogger(DigitalPhoenixSparkMain.class);
            this.myLogger.setLevel(Level.INFO);
            this.ctlCommands = new LinkedBlockingQueue<>(3);
            this.timerQueue = new LinkedBlockingQueue<>(3);
        } catch (Exception logEx)
        {
            myLogger.error(logEx);
        }

        try
        {
            sensorConfig = new CcsiConfigSensor(ccsiBaseDir);
        } catch (Exception scEx)
        {
            myLogger.fatal("Cannot find the CCSI configuration file", scEx);
            throw new RuntimeException ("Cannot find the CCSI configuration file", scEx);
        }

//        System.out.println("SparkMain.constructor -> after Sensor Config.");

        try
        {
            sensorState = new CcsiSavedState(sensorConfig, ccsiBaseDir);
        } catch (Exception ssEx)
        {
            myLogger.error("Exception CCSISavedState: " + Arrays.toString(ssEx.getStackTrace()));
        }
        hostNetworkSettings = sensorState.getHostNetworkSettings();
        sensorNetworkSettings = sensorState.getSensorNetworkSettings();

//        System.out.println("SparkMain.constructor -> after Saved State.");
        
        try
        {
//            System.out.println("Property: " + SENSOR_IA_READER_CLASS + " Value: " + siaReaderName);
//            System.out.println("Property: " + SENSOR_IA_WRITER_CLASS + " Value: " + siaWriterName);
//            System.out.println("Property: " + SENSOR_PA_READER_CLASS + " Value: " + spaReaderName);
//            System.out.println("Property: " + SENSOR_PA_WRITER_CLASS + " Value: " + spaWriterName);
//            System.out.println("Property: " + HOST_PA_IMPLEMENTATION + " Value: " + hpaImpl);
//            System.out.println("Property: " + HOST_IA_IMPLEMENTATION + " Value: " + hiaImpl);
//            System.out.println();
            myLogger.info("Property: " + SENSOR_IA_READER_CLASS + " Value: " + siaReaderName);
            myLogger.info("Property: " + SENSOR_IA_WRITER_CLASS + " Value: " + siaWriterName);
            myLogger.info("Property: " + SENSOR_PA_READER_CLASS + " Value: " + spaReaderName);
            myLogger.info("Property: " + SENSOR_PA_WRITER_CLASS + " Value: " + spaWriterName);
            myLogger.info("Property: " + HOST_PA_IMPLEMENTATION + " Value: " + hpaImpl);
            myLogger.info("Property: " + HOST_IA_IMPLEMENTATION + " Value: " + hiaImpl);

            //
            // Set up the base timer thread and the IPC queues.
            //
            baseTimer = new UtilTimer("BrukerNGCD");

            
            // sensor adapter queues
            spAdapterToPh = new IPCWrapperQueue();
            siAdapterToSp = new IPCWrapperQueue();
            spAdapterToSi = new IPCWrapperQueue();
            sensorAdapterQueue = new CtlWrapperQueue();
            spWriterDataQueue = new IPCWrapperQueue();

            siaCtlQueue = new CtlWrapperQueue();
            siaLinkEventQ = new SaLinkEventQueue();
            siaTransferQueue = new SIAXferEventQueue();
            spaTransferQueue = new SIAXferEventQueue();
            siaConnEventQueue = new SaConnEventQueue();       
            
            phTransferQueue = new SIAXferEventQueue();
            
            saQueues.setSensorAdapterQueue(sensorAdapterQueue);
            saQueues.setSiAdapterToSp(siAdapterToSp);
            saQueues.setSpAdapterToPh(spAdapterToPh);
            saQueues.setSpAdapterToSi(spAdapterToSi);
            saQueues.setSpWriterDataQueue(spWriterDataQueue);
            
            saQueues.setSiaCtlQueue(siaCtlQueue);
            saQueues.setSiaLinkEventQueue(siaLinkEventQ);
            saQueues.setSiaXferEventQueue (siaTransferQueue);
            saQueues.setSpaXferEventQueue (spaTransferQueue);
            saQueues.setSiaConnEventQueue (siaConnEventQueue);
                 
            saQueues.setPhXferEventQueue(phTransferQueue);
            
//            System.out.println("SparkMain.constructor -> after 'new' of our IPC Wrapper Queues.");
            
            // host adapter queues
            nsdsCtlQueue = new CtlWrapperQueue();
            haQueues.setNsdsCtlQueue(nsdsCtlQueue);
            hpaCtlQueue = new CtlWrapperQueue();
            haQueues.setHpaCtlQueue(hpaCtlQueue);
            phCtlQueue = new CtlWrapperQueue();
            mainCtlQueue = new CtlWrapperQueue();
            nsdsLinkEventQ = new LinkEventQueue();
            haQueues.setNSDSLinkEventQueue(nsdsLinkEventQ);
            hiaTransferQueue = new NSDSXferEventQueue();
            haQueues.setHiaXferEventQueue (hiaTransferQueue);
            hpaXferEventQueue = new NSDSXferEventQueue();
            haQueues.setHpaXferEventQueue (hpaXferEventQueue);
            connEventQueue = new ConnEventQueue();
            haQueues.setHiaConnEventQueue (connEventQueue);
            hpaConnEventQueue = new ConnEventQueue();
            haQueues.setHpaConnEventQueue (hpaConnEventQueue);
            


//TODO:   seems the follow block should be changed to SIA, rather than HPA            
//TODO:   seems the follow block should be changed to SIA, rather than HPA            
//TODO:   seems the follow block should be changed to SIA, rather than HPA            
//TODO:   seems the follow block should be changed to SIA, rather than HPA            
            sensorState.setSpaXferEventQueue(spaTransferQueue);
            sensorState.setHPAXferEventQueue(hpaXferEventQueue);
            sensorState.setNsdsCtlQ(nsdsCtlQueue);
            sensorState.setHPACtlQ(hpaCtlQueue);
            sensorState.setPhCtlQ(phCtlQueue);
            sensorState.setSaCtlQ(sensorAdapterQueue);
            sensorState.setSaWriterDataQ(spWriterDataQueue);
            sensorState.setNsdsLinkEventQ(nsdsLinkEventQ);
            sensorState.setMainCtlQ(mainCtlQueue);
            mainCtlQueue.addIPCQueueEventListener(this);
            sensorState.setConnEventQueue(connEventQueue);
            sensorState.setHPAConnEventQueue(hpaConnEventQueue);
            sensorState.setTimerBase(baseTimer);
            mainCtlQueue.flushQueue();
            ccvTimer = new Timer("TaskTimer", true);

//            System.out.println("SparkMain.constructor -> about to create NSDS Interface.");     
            
            //
            // Create the NSDS Interface
            //
           
            NSDSInterface theNSDSImpl = (NSDSInterface) Class.forName (hiaImpl).newInstance ();
            
            theNSDSImpl.initialize(haQueues, hostNetworkSettings, sensorState.getConnList(), baseTimer);

            nsdsHostOpenInterface = theNSDSImpl;
            sensorState.setTheOpenInterface(theNSDSImpl);
            sparkPoolThreadFactory.setThreadName("NSDSThread");
            theNSDSImpl.doRun(executor); 
//            System.out.println("SparkMain.constructor -> after HIA.");
                    
            //
            // Create the Host Protocol Adapter Interface
            //
            theHPAImpl = (HostProtocolAdapterInterface)Class.forName (hpaImpl).newInstance ();
            theHPAImpl.initialize(haQueues, hostNetworkSettings, nsdsHostOpenInterface, sensorState.getConnList());
            sensorState.setTheHPAInterface(theHPAImpl);
            sparkPoolThreadFactory.setThreadName("HostProtocolAdapterThread");
            theHPAImpl.doRun(executor);
//            System.out.println("SparkMain.constructor -> after HPA.");
            
            //
            // Open the cache and prep it for use.
            //
            theCacheManager = new CcsiCacheManager(sensorConfig);
            sensorState.setTheCacheMgr(theCacheManager);
         
             
            
//==================================            
            SIAInterface theSIAImpl = (SIAInterface) Class.forName ("com.domenix.sensorinterfaceadapter.SIAInterfaceEthernetImpl").newInstance ();
            
            this.sensorOpenInterface = (SIAInterfaceEthernetImpl)theSIAImpl;
            
            //After this next line, I will have a socket connection with the TestSensor.
            this.sensorOpenInterface.initialize(saQueues, sensorNetworkSettings, sensorState.getSensorConnectionList(), baseTimer);
            //Socket Connection with TestSensor, successful.
            

//            sensorOpenInterface = (SIAInterfaceEthernetImpl)theSIAImpl;
//==================================            
          
 

            //
            // Create the CCSI Protocol Handler
            //           
//            System.out.println("SparkMain.constructor -> BEFORE creating the CCSI PH.");
            theProtocolHandler = new CcsiProtocolHandler(sensorConfig,
                    sensorState, theCacheManager, spAdapterToPh,
                    spWriterDataQueue, phCtlQueue, baseTimer,
                    nsdsHostOpenInterface, sensorOpenInterface, theHPAImpl, ccsiBaseDir);
//            System.out.println("SparkMain.constructor -> AFTER creating the CCSI PH.");
                        
            // Set up the sensor protocol adaptor's receiver and writer classes.
            SensorProtocolAdapterWriterInterface theSPAdapterWriter = 
                    (SensorProtocolAdapterWriterInterface) 
                    Class.forName(spaWriterName).newInstance();
            
            InterfaceSettingsBase interfaceBase = null;
            switch (sensorConfig.getSensorInterfaceType())
            {
                case TCP:
                    interfaceBase = sensorConfig.getSensorNetworkSettings();
                    break;
                case UDP:
                case USB:
                default:
                    myLogger.fatal("Unsupported Sensor Interface Type: " + sensorConfig.getSensorInterfaceType());
//                    System.exit (-1);
                    break;
                case SERIAL:
                    interfaceBase = sensorConfig.getSerialInterface ();
                    break;
            }            
 
            
//                        
//            //deep inside here is where the SIA socket connection is created and connected -> EthernetAdapter
            theSPAdapterWriter.initialize(spAdapterToSi, spWriterDataQueue, 
                    siAdapterToSp, sensorConfig.getSensorUniqueConfig(), 
                    ccsiBaseDir, spaConfigFileName, interfaceBase);
            
            SensorProtocolAdapterReaderInterface theSPAdapterReader = 
                    (SensorProtocolAdapterReaderInterface) 
                    Class.forName(spaReaderName).newInstance();
            theSPAdapterReader.initialize(saQueues,             //siAdapterToSp, spAdapterToPh, 
//                    spAdapterToSi, 
                    mainCtlQueue, interfaceBase,
                    sensorConfig.getSensorUniqueConfig(),
                    ccsiBaseDir, spaConfigFileName);
//            System.out.println("SparkMain.constructor -> AFTER creating the SPA Reader and Writer.");
       

//===============================
//KBM START:  trying to remove current Writer and Reader and replace with ONE combined Writer/Reader.
//            SensorInterfaceAdapterWriterInterface theSIAdapterWriter = 
//                    (SensorInterfaceAdapterWriterInterface) 
//                    Class.forName(siaWriterName).newInstance();
//            theSIAdapterWriter.initialize(spAdapterToSi, ccsiBaseDir, interfaceBase, siaConfigFileName);
//            Object interfaceAdapter = theSIAdapterWriter.getInterfaceAdapter();
//            
//            SensorInterfaceAdapterReaderInterface theSIAdapterReader = 
//                    (SensorInterfaceAdapterReaderInterface) Class.
//                            forName(siaReaderName).newInstance();
//            theSIAdapterReader.initialize(siAdapterToSp, spWriterDataQueue, 
//                    ccsiBaseDir, interfaceBase, siaConfigFileName);
//            theSIAdapterReader.setInterfaceAdapter(interfaceAdapter);
//            System.out.println("SparkMain.constructor -> AFTER creating the SIA Reader and Writer.");            
//KBM END:  trying to remove current Writer and Reader and replace with ONE combined Writer/Reader.
//===============================



            // Create the Sensor Adapter
            theSensorProtocolAdapter = new SensorAdapter(sensorAdapterQueue, this.sensorOpenInterface,    //theSensorInterfaceAdapter,  
//                    theSIAdapterWriter, theSIAdapterReader, 
                    theSPAdapterWriter, theSPAdapterReader,
                    sensorConfig, sensorState);

            //
            // Start the Sensor Adapter to allow us to wait for a Start Message from the sensor.
            //
            ccvTimer.scheduleAtFixedRate(baseTimer, 3L, 1000L);
            sparkPoolThreadFactory.setThreadName("SensorProtocolAdapter");
            executor.execute(theSensorProtocolAdapter);

            boolean retval = sensorOpenInterface.startInterface();
//            theSIAdapterReader.start();

//            mySIALink = new SIALinkEthernetImpl(sensorNetworkSettings, sensorState.getSensorConnectionList(),
//                                                saQueues, baseTimer);

            SIAConnection newConnection = this.sensorOpenInterface.connect(0);
        
        

            

//            System.out.println("SparkMain.constructor -> AFTER creating the SensorAdapter.  Started? " + retval);
            
        } catch (IOException | ClassNotFoundException | IllegalAccessException | InstantiationException genEx)
        {
            myLogger.error("Exception: " + Arrays.toString(genEx.getStackTrace()));
            System.out.println("Exception: " + Arrays.toString(genEx.getStackTrace()));
        }
        
//      System.out.println("SparkMain.constructor -> END OF CONSTRUCTOR.");
    }

    /**
     * The main control thread
     */
    @Override
    public void run()
    {
        boolean termStatus = false;

        while (!FINALSHUTDOWNFLAG.get())
        {
            if (processTimerQueue)
            {
                TimerChangeEvent tEvt = timerQueue.poll();

                if (tEvt.getTimerPurpose() == TimerPurposeEnum.REBOOTDELAY_TIMER)
                {
                    cleanup(0);
                } else if (tEvt.getTimerPurpose() == TimerPurposeEnum.SHUTDOWN_TIMER)
                {
                    theCacheManager.shutDownCache();
                    FINALSHUTDOWNFLAG.set(true);
                }

                if (timerQueue.isEmpty())
                {
                    processTimerQueue = false;
                }
            }

            if (sendStatusResponse)
            {
                sendCtlResponse(this.statusResponse, this.sendStatusReason);
                statusResponse = false;
                sendStatusResponse = false;
                sendStatusReason = null;
            }

            if (this.processControlCmd)
            {
                CtlWrapper cmd = ctlCommands.poll();

                if (cmd != null)
                {
                    if (cmd.getCtlCommand() == CtlWrapper.SENSOR_START_CMD)
                    {
                        if (!startedUp)
                        {
                            boolean retVal = startup(cmd);

                            this.sendStatusResponse = true;
                            this.statusResponse = retVal;

//=================================================================                            
//OpenInterfaceMacroProcessor oimp = new OpenInterfaceMacroProcessor(sensorConfig);    
//String oImsg = oimp.getTranslation("sid");
//System.out.println("OI Msg:  " + oImsg);


                            String xmlOIConnect = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n" +
                            "<ccsi:CCSIConnection xmlns:ccsi=\"file:/C:/ccsi/1.1/CCSI\"\n" +
                            " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" type=\"start\" sid=\"b4d81dec-616b-4f2c-a8b2-f787ad24826e\"/>";

                            CommandMsg cmdOIConnect = new CommandMsg();
                            cmdOIConnect.setCmd(xmlOIConnect);

                            MsgContent msgBody = new MsgContent();
                            msgBody.setCmd(cmdOIConnect);

                            IPCWrapper ipcWrapper = new IPCWrapper();
                            ipcWrapper.setRoutingKey("ccsi.cmd");
                            ipcWrapper.setMsgBody(msgBody); 

//                            System.out.println("DIGITAL PHOENIX:  " + xmlOIConnect);
//                            System.out.println();
                            
                            saQueues.getSpAdapterToSi().insertFirst(ipcWrapper);
//                            spAdapterToSi.insertFirst(ipcWrapper);       //Writes to the SIA.
//                            System.out.println("DIGITAL PHOENIX:  after insertFirst to spAdapterToSi.");
//==================================================================
                            
                        } else
                        {
                            this.sendStatusResponse = true;
                            this.statusResponse = false;
                            this.sendStatusReason = "CCSI already started up successfully.";
                        }                     
                        
                    } else if (cmd.getCtlCommand() == CtlWrapper.SENSOR_SHUTDOWN)
                    {
                        boolean retVal = this.cleanup(cmd.getShutdownTime());

                        this.sendStatusResponse = true;
                        this.statusResponse = retVal;
                    }
                } else
                {
                    this.processControlCmd = false;
                }
            }

            synchronized (this.syncObject)
            {
                try
                {
                    this.syncObject.wait(2000L);
                } catch (InterruptedException interEx)
                {
                }
            }
        }

        baseTimer.cancelAllTimers();
        executor.shutdown();

        try
        {
            termStatus = executor.awaitTermination(5, TimeUnit.SECONDS);

            if (!termStatus)
            {
                executor.shutdownNow();
            }
        } catch (Exception ex)
        {
            myLogger.error("While terminating during shutdown." + ex.getMessage());
        }

        System.out.println("Exiting main thead");

    }

    /**
     * Shut down and clean up the threads after some delay if specified
     *
     * @param shutdownDelay
     * @return
     */
    private boolean cleanup(int shutdownDelay)
    {
        boolean retVal = true;

        if (shutdownDelay > 0)
        {
            long timerId = baseTimer.setNewTimer(shutdownDelay, false,
                    this, null, "ShutdowmTimer",
                    TimerPurposeEnum.REBOOTDELAY_TIMER);

            baseTimer.addUtilTimerListener(this, timerId, TimerChangeEvent.TEST_TIMER_EXPIRED);

            return (retVal);
        } else
        {
            nsdsHostOpenInterface.shutDownInterface();
//            nsdsCtlQueue.insertFirst(new CtlWrapper(CtlWrapper.STOP_THREAD));
            hpaCtlQueue.insertFirst(new CtlWrapper(CtlWrapper.STOP_THREAD));
            phCtlQueue.insertFirst(new CtlWrapper(CtlWrapper.STOP_THREAD));

            long timerId = baseTimer.setNewTimer(2, false, this, null, "ShutdowmTimer",
                    TimerPurposeEnum.SHUTDOWN_TIMER);

            baseTimer.addUtilTimerListener(this, timerId, TimerChangeEvent.TEST_TIMER_EXPIRED);
        }

        return (retVal);
    }

    /**
     * Send control message response
     *
     * @param type success/failure flag
     * @param errorText control error(s)
     */
    private void sendCtlResponse(boolean type, String errorText)
    {
        ObjectFactory oFact = new ObjectFactory();
        MsgContent responseMsgWrapper = oFact.createMsgContent();
        String outRoute = "ccsi.response.ctl";

        responseMsgWrapper.setResponse(new ResponseMsg());
        responseMsgWrapper.getResponse().setSuccess(type);

        if ((errorText != null) && (!errorText.isEmpty()) && (!type))
        {
            responseMsgWrapper.getResponse().setMessage(errorText);
        }

        responseMsgWrapper.getResponse().setRouting(RoutingKeyEnum.fromValue(outRoute));

        IPCWrapper temp = new IPCWrapper(outRoute, responseMsgWrapper);

        spWriterDataQueue.insertLast(temp);
    }

    /**
     * Initiate startup of the threads. This is invoked when the main receives a
     * start message from the sensor. Until then, only the sensor adapter is
     * executing.
     *
     * @param entry
     *
     * @return flag indicating success or failure
     */
    private boolean startup(CtlWrapper entry)
    {
        boolean retVal = true;
        boolean needCfgUpdate = false;
        boolean needStateUpdate = false;
        VersionNumberType temp;

        try
        {
            if ((entry.getUuid() != null) && (!entry.getUuid().trim().isEmpty()))
            {
                if (!entry.getUuid().trim().equalsIgnoreCase(sensorConfig.getUuid().trim()))
                {
                    sensorConfig.setUuid(entry.getUuid().trim());
                    needCfgUpdate = true;
                }
            }

            if ((entry.getHwVers() != null) && (entry.getHwVers().trim().length() > 0))
            {
                temp = new VersionNumberType(entry.getHwVers().trim());

                if (sensorConfig.getHwVersion().compareTo(temp) != 0)
                {
                    sensorConfig.setHwVersion(temp);
                    needCfgUpdate = true;
                }
            }

            if ((entry.getSwVers() != null) && (entry.getSwVers().trim().length() > 0))
            {
                temp = new VersionNumberType(entry.getSwVers().trim());

                if (sensorConfig.getSwVersion().compareTo(temp) != 0)
                {
                    sensorConfig.setSwVersion(temp);
                    needCfgUpdate = true;
                }
            }

            if ((entry.getSerial() != null) && (entry.getSerial().trim().length() > 0))
            {
                if (!entry.getSerial().trim().equalsIgnoreCase(sensorConfig.getSerial().trim()))
                {
                    sensorConfig.setSerial(entry.getSerial().trim());
                    needCfgUpdate = true;
                }
            }

            if (entry.getState() != null)
            {
                if (sensorState.getMode().name().charAt(0) != entry.getState().name().charAt(0))
                {
                    CcsiModeEnum mode = null;
                    CcsiComponentStateEnum state = null;
                    CcsiBitStatusEnum bitStatus = null;
                    
                    switch (entry.getState())
                    {
                        case NORMAL:
                            mode = CcsiModeEnum.N;
                            state = CcsiComponentStateEnum.N;
                            bitStatus = CcsiBitStatusEnum.PASS;
                            break;

                        case DEGRADED:
                            mode = CcsiModeEnum.D;
                            state = CcsiComponentStateEnum.D;
                            bitStatus = CcsiBitStatusEnum.FAIL;
                            break;
                            
                        case FAILED:
                            mode = CcsiModeEnum.D;
                            state = CcsiComponentStateEnum.F;
                            bitStatus = CcsiBitStatusEnum.FAIL;
                            break;

                        default:
                            myLogger.error("Invalid mode received from the sensor: " + entry.getState());
                            break;
                    }

                    if (mode != null)
                    {
                        sensorState.setMode(mode);
                        List <String> scList = sensorState.getComponentSCList();
                        for (String x : scList)
                        {
                            sensorState.setComponentSCStatus(state, x);
                            sensorState.setComponentSCBitStatus(bitStatus, x);
                            sensorState.setComponentSCBitTime(new Date(), x);
                        }
                        sensorState.setComponentPDILStatus(state);
                        sensorState.setComponentPDILBitStatus(bitStatus);
                        sensorState.setComponentPDILBitTime(new Date());
                        needStateUpdate = true;
                    }
                }
            }

            if (needCfgUpdate)
            {
                try
                {
                    sensorConfig.writeSensorConfig();
                } catch (Exception cfgEx)
                {
                    myLogger.error("Error updating the sensor configuration file: ", cfgEx);
                    retVal = false;
                }
            }

            if (needStateUpdate)
            {
                try
                {
                    sensorState.writeSavedState();
                } catch (Exception cfgEx)
                {
                    myLogger.error("Error updating the sensor state file: ", cfgEx);
                    retVal = false;
                }
            }

            if (!retVal)
            {
                return (retVal);
            }

            //
            // Start the NSDS processing
            //
            if (!nsdsHostOpenInterface.startInterface())
            {
                retVal = false;
                myLogger.error("Error starting the NSDS interface.");
            }

            //
            // Start Protocol Handler
            //
            sparkPoolThreadFactory.setThreadName("ProtocolHandlerThread");
            executor.execute(theProtocolHandler);

            //
            // Start the full SensorAdapter by sending a control queue entry
            //
            sensorAdapterQueue.insertLast(new CtlWrapper(CtlWrapper.START_ALL_READERS));
            retVal = true;
        } catch (Exception ex)
        {
            myLogger.error("Exception starting threads", ex);
            retVal = false;
        }

        if (retVal)
        {
            startedUp = true;
        }

        return (retVal);
    }

    /**
     * Method description
     *
     *
     * @param e
     */
    @Override
    public void timerExpired(TimerChangeEvent e)
    {
        if (e.getTimerPurpose() == TimerPurposeEnum.REBOOTDELAY_TIMER)
        {
            this.timerQueue.add(e);
            this.processTimerQueue = true;
        } else if (e.getTimerPurpose() == TimerPurposeEnum.SHUTDOWN_TIMER)
        {
            this.timerQueue.add(e);
            this.processTimerQueue = true;
        }
    }

    /**
     * Handle a queue change event for the main
     *
     * @param evt the IPCQueue event
     */
    @Override
    public void queueEventFired(IPCQueueEvent evt)
    {
//        System.out.println("SparkMain.queueEventFired -> top of routine.");
                
        if (evt.getEventSource() instanceof CtlWrapperQueue)
        {
            CtlWrapper rcvd = mainCtlQueue.readFirst(0);
            StringBuilder msg = new StringBuilder("Received entry on: ");

            if (rcvd != null)
            {
                msg.append(rcvd.getCtlCommand());

//                System.out.println("SparkMain.queueEventFired -> msg:  " + msg.toString());
//                System.out.println();

                if (rcvd.getCtlCommand() == CtlWrapper.SENSOR_START_CMD)
                {
                    myLogger.debug(" is start command.");
                    this.ctlCommands.add(rcvd);
                    this.processControlCmd = true;
                } else if (rcvd.getCtlCommand() == CtlWrapper.SENSOR_SHUTDOWN)
                {
                    myLogger.debug(" is shutdown command.");
                    this.ctlCommands.add(rcvd);
                    this.processControlCmd = true;
                }
            }
        }
    }

    /**
     * Main program of the CCSI interface for the Spark CCSI. 
     * -D ccsi.baseDir=&lt;base directory&gt;
     * -D ccsi.pa.reader=&lt;FQN of class&gt;
     * -D ccsi.pa.writer=&lt;FQN of class&gt;
     * -D ccsi.ia.reader=&lt;FQN of class&gt;
     * -D ccsi.ia.writer=&lt;FQN of class&gt;
     * -D ccsi.ia.configFile=&lt;config file name&gt;
     * -D ccsi.pa.configFile=&lt;config file name&gt;
     *
     * @param args the command line arguments
     */
    
 
    public static void main(String[] args)
    {
        Logger myLogger = Logger.getLogger(DigitalPhoenixSparkMain.class);
        int exitStatus = 0;

        System.out.println((char)27 + "[30;43m" + "Main:   KEY" + (char)27 + "[0m" + "\n");
        System.out.println((char)27 + "[32m" + "PH" + (char)27 + "[0m");
        System.out.println((char)27 + "[35m" + "SPA" + (char)27 + "[0m");
        System.out.println((char)27 + "[34m" + "SIAInterfaceEthernetImpl.run" + (char)27 + "[0m");
        System.out.println((char)27 + "[31m" + "SIAInterfaceEthernetImpl.queueEventFired" + (char)27 + "[0m");
        System.out.println((char)27 + "[36m" + "SIAInterfaceEthernetImpl.validateAndRoute" + (char)27 + "[0m");
        System.out.println((char)27 + "[33m" + "SIALink" + (char)27 + "[0m" + "\n");
        System.out.println((char)27 + "[30;43m" + "Main:   END KEY:" + (char)27 + "[0m" + "\n\n");

        // read the environment variables
        ccsiBaseDir = System.getProperty(CCSI_BASE_DIR);
        siaReaderName = System.getProperty(SENSOR_IA_READER_CLASS);
        siaWriterName = System.getProperty(SENSOR_IA_WRITER_CLASS);
        spaReaderName = System.getProperty(SENSOR_PA_READER_CLASS);
        spaWriterName = System.getProperty(SENSOR_PA_WRITER_CLASS);
        siaConfigFileName = System.getProperty(SENSOR_IA_CONFIG_FILE_NAME);
        spaConfigFileName = System.getProperty(SENSOR_PA_CONFIG_FILE_NAME);
        hpaImpl = System.getProperty(HOST_PA_IMPLEMENTATION);
        hiaImpl = System.getProperty(HOST_IA_IMPLEMENTATION);
        hiaConfigFileName = System.getProperty(HOST_IA_CONFIG_FILE_NAME);
        hpaConfigFileName = System.getProperty(HOST_PA_CONFIG_FILE_NAME);
       
        String errorMessage = null;
        if (ccsiBaseDir == null || siaReaderName == null || siaWriterName == null 
                || spaReaderName == null || spaWriterName == null || 
                siaConfigFileName == null || spaConfigFileName == null ||
                hiaImpl == null)
        {
            errorMessage = "Not all required environment variables set, exiting";
            if (ccsiBaseDir == null)
            {
                System.out.println(errorMessage);
                System.exit(-1);
            }
        }
        String os = System.getProperty("os.name").toLowerCase();
        String log4jXmlFile;
        // check if this is a unix based system
        if (os.contains("nux"))
        {
            log4jXmlFile = ccsiBaseDir + "/log4j.xml";
        } else
        {
            log4jXmlFile = ccsiBaseDir + "/log4jwin.xml";
        }
        
        ExecutorService myExecutor = Executors.newSingleThreadExecutor(new SparkThreadFactory ("MainThread"));
        
        // First, set up log4j
        DOMConfigurator myConfig = new DOMConfigurator();
        myConfig.doConfigure(log4jXmlFile, Logger.getRootLogger().getLoggerRepository());
        Logger.getRootLogger().setLevel(Level.INFO);

        if (errorMessage != null && ccsiBaseDir != null)
        {
            myLogger.error(errorMessage);
            System.exit(-1);
        }
        
        //
        // Create our thread runner/manager
        //
        sparkPoolThreadFactory = new SparkThreadFactory ();
        executor = Executors.newFixedThreadPool(8, sparkPoolThreadFactory);

        //
        // Start the processing
        //
        try
        {
            DigitalPhoenixSparkMain theMain = new DigitalPhoenixSparkMain();
//            System.out.println("SparkMain -> constructor done.  Entering main loop.");

            myExecutor.execute(theMain);

//             System.out.println("SparkMain -> constructor done.  AFTER Execute theMain.");           
//==================================


   
//        String xmlOIConnect = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n" +
//        "<ccsi:CCSIConnection xmlns:ccsi=\"file:/C:/ccsi/1.1/CCSI\"\n" +
//        " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" type=\"start\" sid=\"b4d81dec-616b-4f2c-a8b2-f787ad24826e\"/>";
//        
//        String xmlOIConnect2 = oimp.headerStart + oimp.getTranslation(oimp.openIfcInfo);
//        
////        if (xmlOIConnect.equals(xmlOIConnect2)) {
////            System.out.println("YAY -- EQUALS!!");
//            System.out.println("DIGITAL PHOENIX Main:  " + xmlOIConnect);
//            System.out.println();
//            System.out.println("DIGITAL PHOENIX Main2:  " + xmlOIConnect2);
//            System.out.println();
////        }
//        
//        CommandMsg cmdOIConnect = new CommandMsg();
//        cmdOIConnect.setCmd(xmlOIConnect2);
//
//        MsgContent msgBody = new MsgContent();
//        msgBody.setCmd(cmdOIConnect);
//
//        IPCWrapper ipcWrapper = new IPCWrapper();
//        ipcWrapper.setRoutingKey("ccsi.cmd");
//        ipcWrapper.setMsgBody(msgBody); 
//
////        System.out.println("DIGITAL PHOENIX Main:  " + xmlOIConnect);
////        System.out.println();
//        spAdapterToSi.insertFirst(ipcWrapper);       //Writes to the SIA.
//////        spWriterDataQueue.insertFirst(ipcWrapper);       //Writes to the SPA.
//////        System.out.println("DIGITAL PHOENIX Main:  after insertFirst to spAdapterToSi.");
//////        System.out.println();
//////        System.out.println("END:  Initiate Sensor Comms!");



//            //Used to send a START_SENSOR_CMD to CcsiProtocolHandler to force start of  the .run method.
//            System.out.println("SparkMain:  about to send SENSOR_START_CMD.");
//            CtlWrapper ctlEntry;
//            ctlEntry =  new CtlWrapper();
//            ctlEntry.setCtlCommand(CtlWrapper.SENSOR_START_CMD);
//            mainCtlQueue.insertFirst(ctlEntry);
//            //END send START_SENSOR_CMD to force PH.run method.

            
            synchronized (FINALSHUTDOWNFLAG)
            {
                while (!FINALSHUTDOWNFLAG.get())
                {
                    try
                    {
//                        System.out.println("SparkMain.main -> in main loop.......");
                        FINALSHUTDOWNFLAG.wait(6000L);
                    } catch (InterruptedException ex2)
                    {
                    }
                }

                myExecutor.shutdownNow();
            }
        } catch (Exception ex)
        {
            System.out.println("Exception during execution: " + Arrays.toString(ex.getStackTrace()));
            myLogger.error("Exception during execution: " + Arrays.toString(ex.getStackTrace()), ex);
            exitStatus = 1;
        }

        System.exit(exitStatus);
    }
    
//    public static boolean initiateGetInformation() {
//        
////        System.out.println("START:  Initiate Sensor Comms!");
//        
////        String xmlGetInformation = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ccsi:Hdr xmlns:ccsi=\"file:/C:/ccsi/1.1/CCSI\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" sid=\"10000000-1000-1000-1000-100000000000\" msn=\"1\" dtg=\"1595515399\" chn=\"c\" mod=\"N\" len=\"80\"><CCSIDoc>\n" +
////"<Msg>\n" +
////"<CmdChn Cmd=\"Get_Identification\"/>\n" +
////"</Msg>\n" +
////"</CCSIDoc>\n" +
////"</ccsi:Hdr>";
//      
//        String header1 = hmp.headerStart + hmp.getTranslation(hmp.headerMiddle);       
//        hmp.setLength(String.valueOf(gip.getIdentificationMessage.length()));
//        String header2 = hmp.getTranslation(hmp.headerMiddle);
//        String xmlGetInformation = header1 + header2 + gip.getIdentificationMessage + hmp.headerEnd;
//        CommandMsg cmdGetInformation = new CommandMsg();
//        cmdGetInformation.setCmd(xmlGetInformation);
//
//        MsgContent msgBody = new MsgContent();
//        msgBody.setCmd(cmdGetInformation);
//
//        IPCWrapper ipcWrapper = new IPCWrapper();
//        ipcWrapper.setMsgBody(msgBody);
//        ipcWrapper.setRoutingKey("ccsi.cmd.Get_Identification");   
//
////        System.out.println("DIGITAL PHOENIX Main:  " + xmlGetInformation);
////        System.out.println();
//        spAdapterToSi.insertFirst(ipcWrapper);
////        spWriterDataQueue.insertFirst(ipcWrapper);
////        System.out.println("DIGITAL PHOENIX Main:  after insertFirst to spAdapterToSi.");
////        System.out.println();
//        
//        return true;
//    }
//    
//    public static void register(IPCWrapper wrapper) {
//         spWriterDataQueue.insertFirst(wrapper);
////        spAdapterToSi.insertFirst(wrapper);
//    }

}
