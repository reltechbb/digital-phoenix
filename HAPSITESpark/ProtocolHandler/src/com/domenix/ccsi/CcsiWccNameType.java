/*
 * CcsiWccNameType.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * 
 *
 * Version: V1.0  15/08/01
 */
package com.domenix.ccsi;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author kmiller
 */
public class CcsiWccNameType
{
  /** The pattern of a valid name */
  private final static String WCC_NAME_REGEX = "[UWORF][0-9]{3}";

  /** The Regex matching pattern */
  private final Pattern	wccNamePattern	= Pattern.compile( WCC_NAME_REGEX );

  /** The SC name */
  private String	wccName;
  
  public CcsiWccNameType()
  {
    this.wccName = "";
  }

  /**
   * Constructs an SC name with an input string and throws an IllegalArgumentException if
   * the name is not valid.
   *
   * @param wccName
   */
  public CcsiWccNameType( String wccName )
  {
    Matcher	m	= wccNamePattern.matcher( wccName );

    if ( m.matches() )
    {
      this.wccName	= wccName;
    }
    else
    {
      throw new IllegalArgumentException( "Invalid WCC name in constructor" );
    }
  }

  /**
   * @return the wccName
   */
  public String getWccName()
  {
    return wccName;
  }

  /**
   * @param wccName the wccName to set
   */
  public void setWccName(String wccName)
  {
    this.wccName = wccName;
  }

  /**
   * @return the wccNamePattern
   */
  public Pattern getWccNamePattern()
  {
    return wccNamePattern;
  }
}
