/*
 * CcsiComponentEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This enumerates the types of CCSI sensor components that can be reported.
 *
 * Version: V1.0  15/09/10
 */
package com.domenix.ccsi;

/**
 * This enumerates the types of CCSI sensor components that can be reported.
 * 
 * @author kmiller
 */
public enum CcsiComponentEnum
{
  /** Mandatory Control component */
  CC ,

  /** Mandatory Power distribution and interface logic component */
  PDIL ,

  /** Mandatory Sensing component */
  SC ,

  /** Wireless communications component */
  WCC ,

  /** Dismounted power component */
  DPC ,
  
  /** User interface component */
  UIC;
}
