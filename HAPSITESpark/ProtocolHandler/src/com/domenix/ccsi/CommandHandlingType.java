/*
 * CommandHandlingType.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition for how to handle a single CCSI standard command.
 *
 * Version: V1.0  15/09/11
 */


package com.domenix.ccsi;

import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.common.ccsi.CommandHandlingTypeEnum;

/**
 * This class encapsulates a single CCSI standard command and defines how it is handled by the
 * Protocol Handler.
 *
 * @author kmiller
 */
public class CommandHandlingType
{
  /** Is acknowledgement required. */
  private Boolean	ackRequired;

  /** The command */
  private String	command;

  /** How to handle the command. */
  private CommandHandlingTypeEnum	handle;

  /** The channel for a response to the command */
  private CcsiChannelEnum	responseChannel;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a default class instance
   */
  public CommandHandlingType()
  {
    this.command					= null;
    this.handle						= CommandHandlingTypeEnum.unsupported;
    this.ackRequired			= false;
    this.responseChannel	= null;
  }

  /**
   * Constructs a class instance will all parameters.
   *
   * @param cmd the command name
   * @param hndl how to handle the command
   * @param ack whether an ACK is required
   * @param resp the channel for a response to the command or null if none
   */
  public CommandHandlingType( String cmd , CommandHandlingTypeEnum hndl , boolean ack , CcsiChannelEnum resp )
  {
    this.command					= cmd;
    this.handle						= hndl;
    this.ackRequired			= ack;
    this.responseChannel	= resp;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the command
   */
  public String getCommand()
  {
    return command;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param command the command to set
   */
  public void setCommand( String command )
  {
    this.command	= command;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the handle
   */
  public CommandHandlingTypeEnum getHandle()
  {
    return handle;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param handle the handle to set
   */
  public void setHandle( CommandHandlingTypeEnum handle )
  {
    this.handle	= handle;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the ackRequired
   */
  public Boolean getAckRequired()
  {
    return ackRequired;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param ackRequired the ackRequired to set
   */
  public void setAckRequired( Boolean ackRequired )
  {
    this.ackRequired	= ackRequired;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the responseChannel
   */
  public CcsiChannelEnum getResponseChannel()
  {
    return responseChannel;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param responseChannel the responseChannel to set
   */
  public void setResponseChannel( CcsiChannelEnum responseChannel )
  {
    this.responseChannel	= responseChannel;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Returns the current object state as a string.
   *
   * @return String containing the object state
   */
  @Override
  public String toString()
  {
    StringBuilder	msg	= new StringBuilder( "Command " );

    msg.append( this.command ).append( ' ' ).append( this.handle.name() );

    if ( this.ackRequired )
    {
      msg.append( " requires ACK" );
    }
    else
    {
      msg.append( " no ACK " );
    }

    if ( this.responseChannel != null )
    {
      msg.append( " response on " ).append( this.responseChannel.name() );
    }
    else
    {
      msg.append( " no response" );
    }

    return ( msg.toString() );
  }
}
