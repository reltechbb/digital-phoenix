/*
 * CcsiScNameType.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/02
 */


package com.domenix.ccsi;

//~--- JDK imports ------------------------------------------------------------

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//~--- classes ----------------------------------------------------------------

/**
 *
 * @author kmiller
 */
public class CcsiScNameType
{
  /** The pattern of a valid name */
  private final static String SC_NAME_REGEX = "SC[0-9]{3}";

  /** The Regex matching pattern */
  private final Pattern	scNamePattern	= Pattern.compile( SC_NAME_REGEX );

  /** The SC name */
  private String	scName;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a default instance without a name
   */
  public CcsiScNameType()
  {
    this.scName	= "";
  }

  /**
   * Constructs an SC name with an input string and throws an IllegalArgumentException if
   * the name is not valid.
   *
   * @param scName
   */
  public CcsiScNameType( String scName )
  {
    Matcher	m	= scNamePattern.matcher( scName );

    if ( m.matches() )
    {
      this.scName	= scName;
    }
    else
    {
      throw new IllegalArgumentException( "Invalid SC name in constructor" );
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the scName
   */
  public String getScName()
  {
    return scName;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param scName the scName to set
   */
  public void setScName( String scName )
  {
    Matcher	m	= scNamePattern.matcher( scName );

    if ( m.matches() )
    {
      this.scName	= scName;
    }
    else
    {
      throw new IllegalArgumentException( "Invalid SC name in setScName" );
    }
  }
}
