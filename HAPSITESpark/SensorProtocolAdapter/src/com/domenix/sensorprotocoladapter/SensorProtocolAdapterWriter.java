/*
 * SensorProtocolAdapterWriter.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <put description here>
 *
 */
package com.domenix.sensorprotocoladapter;

import com.domenix.common.config.InterfaceSettingsBase;
import com.domenix.common.config.SensorUniqueConfigItem;
import com.domenix.commonsa.interfaces.SensorProtocolAdapterWriterInterface;
import com.domenix.common.spark.IPCWrapper;
import com.domenix.common.spark.IPCWrapperQueue;
import com.domenix.common.spark.data.MsgContent;
import com.domenix.common.spark.data.ObjectFactory;
import com.domenix.common.spark.IPCQueueEvent;
import com.domenix.common.spark.IPCQueueListenerInterface;
import com.domenix.commonsa.interfaces.HeaderMacroProcessor;
import java.io.File;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventLocator;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

/**
 * Abstract class implementing the SensorProtocolAdapterWriterInterface
 *
 * @author jmerritt
 */
public abstract class SensorProtocolAdapterWriter implements Runnable,
        IPCQueueListenerInterface, SensorProtocolAdapterWriterInterface
{

    // the Protocol Adapter should be shutdown
    private boolean shutdown = false;
    // queue for sending to the Sensor Interface Adapter
    protected IPCWrapperQueue toSensorInterface = null;
    // queue for sending to the Sensor Protocol Adapter reader
    protected IPCWrapperQueue toSensorProtocolAdapterReader = null;
    // queue for reading from the Protocol Handler and Protocol Adapter Reader
    private IPCWrapperQueue fromProtocolHandler = null;
    // FQN of the vendor specific configuration file
    private String spaConfigFile = null;
    
    // Schema for validating incoming messages
    private Schema schema = null;
    /**
     * The context for JAXB operations (marshal/unmarshal)
     */
    private JAXBContext jaxbContext = null;
    // Schema Factory
    private SchemaFactory schemaFactory = null;

    private File schemaFile = null;

    /** Sensor configuration file */
    protected InterfaceSettingsBase configSensor = null;
    
    /**
     * XML Un-marshaler
     */
    private Marshaller jaxbMarshaller = null;

    /**
     * The object factory
     */
    private final ObjectFactory oFact = new ObjectFactory();

    // logger
    private final Logger myLogger = Logger.getLogger(SensorProtocolAdapterWriter.class);

    // internal queue
    LinkedBlockingQueue<IPCWrapper> ipcWrapperQueue = null;

    /** List of sensor unique configuration item defaults used for initialization of sensor */
    List <SensorUniqueConfigItem> uniqueConfig;
            
    /**
     * constructor
     */
    public SensorProtocolAdapterWriter()
    {
        
    }

    /**
     * the main thread
     */
    @Override
    public void run()
    {

        
        myLogger.info("SP Adapter Writer running...");
//        System.out.println("Sensor Protocol Adapter (SPA) Writer running...");
//        System.out.println();
        
        try
        {
            IPCWrapper entry;
            while (!shutdown)
            {
                try
                {
                    entry = this.ipcWrapperQueue.poll(10, TimeUnit.SECONDS);
                    List <IPCWrapper> translatedMessages;                 
                            
                    if (entry != null)
                    {
                        
//                        System.out.println("SPA Writer.run:  just READ a MSG and ABOUT to xfer to SIA:  " +  entry.getMsgBody().getCmd().getCmd());
//                        System.out.println();
                    
//                        unmarshalMessage (entry);
//                        translatedMessages = translateMessage(entry);
                        
//THIS CREATES AND APPLIES THE HEADER, COMPUTE LENGTH OF DOC.  NEEDS UUID.    SaCcsiChannelEnum type.
                        
//                        for (IPCWrapper wrapper : translatedMessages)
//                        {
//                            System.out.println("SPA Writer.run:  about to insertLast toSensorInterface.");
//                            System.out.println();
                                                    
                            
                        IPCWrapper wrapper = new IPCWrapper();
                        wrapper.setMsgBody(entry.getMsgBody());
                            //SEND TO SIA:
                            toSensorInterface.insertLast(wrapper);
                            myLogger.info("Sending command to SIA: " + wrapper.getMsgBody().getCmd().getCmd());   //getRawMsg());
//                        }
                    }
                } catch (InterruptedException ignored)
                {
                    // this exception is ignored
                } catch (Exception ex)
                {
                    myLogger.error("Exception: " + ex);
                }

            }
            myLogger.info("Shutting Down SP Adapter Writer");

        } catch (Exception iex)
        {
            myLogger.error("Exception writing message to the Sensor Interface Adapter", iex);
        }
    }

    /**
     * Fires when a message is posted to the external queue. The message is
     * moved to the internal queue.
     *
     * @param evt
     */
    @Override
    public void queueEventFired(IPCQueueEvent evt)
    {

        IPCWrapper iw = fromProtocolHandler.readFirst(0);
//        System.out.println("SPA Writer: queueEventFired");   //:  message is => " + iw.getMsgBody().getCmd().getCmd());
//        System.out.println();
        ipcWrapperQueue.add(iw);
    }

    /**
     * Starts the main thread
     *
     * @param x
     */
    @Override
    public void doRun(Executor x)
    {
        shutdown = false;
        x.execute(this);
    }

    /**
     * Initializes the Protocol Adapter Writer
     *
     * @param toSensorInterface
     * @param fromProtocolHandler
     * @param toSensorProtocolAdapterReader - to Sensor Protocol Adapter Reader
     * @param configFileBase
     * @param configFileName - protocol adapter specific configuration file
     * @param configSensor - Ccsi Sensor Configuration
     * @return - true = OK or false = an error occurred
     */
    @Override
    public boolean initialize(IPCWrapperQueue toSensorInterface,
            IPCWrapperQueue fromProtocolHandler, IPCWrapperQueue toSensorProtocolAdapterReader,
            List <SensorUniqueConfigItem> uniqueConfig,
            String configFileBase,
            String configFileName,
            InterfaceSettingsBase configSensor)
    {
        boolean returnValue = true;
        this.fromProtocolHandler = fromProtocolHandler;
        this.toSensorInterface = toSensorInterface;
        this.toSensorProtocolAdapterReader = toSensorProtocolAdapterReader;
        this.uniqueConfig = uniqueConfig;

        spaConfigFile = configFileBase + File.separator + configFileName;
        ipcWrapperQueue = new LinkedBlockingQueue<>(10);
        fromProtocolHandler.addIPCQueueEventListener(this);
        schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        schemaFile = new File(configFileBase + File.separator + "Spark_Queue_Entries.xsd");
        this.configSensor = configSensor;
        
        try
        {
            schema = schemaFactory.newSchema(schemaFile);
            
            jaxbContext = JAXBContext.newInstance(com.domenix.common.spark.data.ObjectFactory.class );

        } catch (SAXException ex)
        {
            myLogger.error("SAXException reading schema", ex);
        } catch (JAXBException ex)
        {
            myLogger.error("JAXBException reading schema", ex);
        }

        if (!initializeTranslator(configFileBase))
        {
            returnValue = false;
        }

        return returnValue;
    }

    /**
     * Triggers a shutdown of the Protocol Adapter Writer
     *
     * @return
     */
    @Override
    public boolean shutdown()
    {
        shutdown = true;
        return true;
    }

    /**
     * Un-marshals and validates the incoming message
     * @param message
     * @return 
     */
    private String unmarshalMessage(IPCWrapper message)
    {
        String postMessage = null;
        MsgContent body = message.getMsgBody();
        StringWriter writer = new StringWriter();
        JAXBElement packet = oFact.createMessage(body);
        String routingKey = message.getRoutingKey();
        ArrayList<String> errors = new ArrayList<>();

        System.out.println("SPA Writer.unmarshalMessage -> the message:  " + message.getMsgBody().getCmd().getCmd());
        System.out.println("SPA Writer.unmarshalMessage:  -> Routing Key:  " + message.getRoutingKey());
        System.out.println();
        
        try
        {
            jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setSchema(schema);
            jaxbMarshaller.setEventHandler((ValidationEvent ve) ->
            {
                // ignore warnings
                if (ve.getSeverity() != ValidationEvent.WARNING)
                {
                    ValidationEventLocator vel = ve.getLocator();
                    StringBuilder msg = new StringBuilder("Line:Col[");
                    msg.append(vel.getLineNumber());
                    msg.append(':');
                    msg.append(vel.getColumnNumber());
                    msg.append("]:");
                    msg.append(ve.getMessage());
                    myLogger.error(msg.toString());
                    errors.add(msg.toString());
                }
                return (true);
            } // allow unmarshalling to continue even if there are errors
            );

            jaxbMarshaller.marshal(packet, writer);
            if (errors.isEmpty())
            {
                postMessage = writer.toString();
                myLogger.debug ("Sent msg on " + routingKey + ":" + postMessage);
            }

            for (String z : errors)
            {
                myLogger.error("Error marshalling: " + z);
            }
        } catch (JAXBException e)
        {
            myLogger.error("JAXBException", e);
        }

        return postMessage;
    }

    /**
     * method to translate the message from internal format to that
     * utilized by the sensor. May simply be a pass trough if the sensor speaks
     * CCSI.
     *
     * @param ipcWrapper
     * @return list of IPCWrappers - the list may be empty
     */
    private List<IPCWrapper> translateMessage(IPCWrapper ipcWrapper)
    {
        System.out.println("SPA Writer.translateMessage top.");
        System.out.println();
        String routingKey = ipcWrapper.getRoutingKey();
        List<IPCWrapper> returnList = new ArrayList<>();

        switch (routingKey)
        {
            case "ccsi.cmd.Power_Off":
                returnList = translatePower_Off (ipcWrapper);
                break;
            case "ccsi.cmd.Render_Useless":
                returnList = translateRender_Useless (ipcWrapper);
                break;
            case "ccsi.cmd.Set_Tamper":
                returnList = translateSet_Tamper (ipcWrapper);
                break;
            case "ccsi.cmd.Erase_Sec_Log":
                returnList = translateErase_Sec_Log (ipcWrapper);
                break;
            case "ccsi.cmd.Get_Sec_Log":
                returnList = translateGet_Sec_Log (ipcWrapper);
                break;
            case "ccsi.cmd.Set_Cmd_Privilege":
                returnList = translateSet_Cmd_Privilege (ipcWrapper);
                break;
            case "ccsi.cmd.Set_Config_Privilege":
                returnList = translateSet_Config_Privilege (ipcWrapper);
                break;
            case "ccsi.cmd.Start_Stop":
                returnList = translateStart_Stop (ipcWrapper);
                break;
            case "ccsi.cmd.Reboot":
                returnList = translateReboot (ipcWrapper);
                break;
            case "ccsi.cmd.Set_Comm_Permission":
                returnList = translateSet_Comm_Permission (ipcWrapper);
                break;
            case "ccsi.cmd.Set_Local_Alert_Mode":
                returnList = translateSet_Local_Alert_Mode (ipcWrapper);
                break;
            case "ccsi.cmd.Start_Self_Test":
                returnList = translateStart_Self_Test (ipcWrapper);
                break;
            case "ccsi.cmd.Set_Security_Timeout":
                returnList = translateSet_Security_Timeout (ipcWrapper);
                break;
            case "ccsi.cmd.Clear_Alert":
                returnList = translateClear_Alert (ipcWrapper);
                break;
            case "ccsi.cmd.Silence":
                returnList = translateSilence (ipcWrapper);
                break;
            case "ccsi.cmd.Set_Location":
                returnList = translateSet_Location (ipcWrapper);
                break;
            case "ccsi.cmd.Set_Local_Enable":
                returnList = translateSet_Local_Enable (ipcWrapper);
                break;
            case "ccsi.cmd.Get_Config":
                returnList = translateGet_Config (ipcWrapper);
                break;
            case "ccsi.cmd.Install_Start":
                returnList = translateInstall_Start (ipcWrapper);
                break;
            case "ccsi.cmd.Install":
                returnList = translateInstall (ipcWrapper);
                break;
            case "ccsi.cmd.Install_Complete":
                returnList = translateInstall_Complete (ipcWrapper);
                break;
            case "ccsi.cmd.Set_Ccsi_Mode":
                returnList = translateSet_Ccsi_Mode (ipcWrapper);
                break;
            case "ccsi.cmd.Set_Config":
                returnList = translateSet_Config (ipcWrapper);
                break;
            case "ccsi.cmd.Set_Date_Time":
                returnList = translateSet_Date_Time (ipcWrapper);
                break;
            case "ccsi.cmd.Get_Alert_Details":
                returnList = translateGet_Alert_Details (ipcWrapper);
                break;
            case "ccsi.cmd.Get_Reading_Details":
                returnList = translateGet_Reading_Details (ipcWrapper);
                break;
            case "ccsi.cmd.SensorUniqueCommand":
                returnList = translateSensorUniqueCommand (ipcWrapper);
                break;

            case "ccsi.response.ctl":
                // don't forward this one
                break;
                
            //KBM:   START of new code added for DigitalPhoenix
            case "ccsi.cmd.Get_Identification":
                System.out.println("SPA Writer.translateMessage -> GET IDENTIFICATION case branch!");
                System.out.println();
                translateGet_Identification (ipcWrapper);
                //returnList = translateGet_Identification (ipcWrapper);
                break;
            //KBM:   END of new code for DigitalPhoenix    

             default:
                myLogger.error("Unknown routing key: " + routingKey);
                break;
        }
        return returnList;
    }
    /**
     * Abstract method to initialize the sensor specific translation
     *
     * @param configFile - FQN of the configuration file
     * @return - true = OK or false = an error occurred
     */
    abstract boolean initializeTranslator(String configFile);

    /**
     * Abstract methods to translate commands.  Should simply be a stub if
     * the command is not supported by the sensor or Sensor Interface Adapter
     * @param List <IPCWrapper>
     * @return 
     */
    abstract List <IPCWrapper> translatePower_Off (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateRender_Useless (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateSet_Tamper (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateErase_Sec_Log (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateGet_Sec_Log (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateSet_Cmd_Privilege (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateSet_Config_Privilege (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateStart_Stop (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateReboot (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateSet_Comm_Permission (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateSet_Local_Alert_Mode (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateStart_Self_Test (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateSet_Security_Timeout (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateClear_Alert (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateSilence (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateSet_Location (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateSet_Local_Enable (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateGet_Config (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateInstall_Start (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateInstall (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateInstall_Complete (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateSet_Ccsi_Mode (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateSet_Config (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateSet_Date_Time (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateGet_Alert_Details (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateGet_Reading_Details (IPCWrapper ipcWrapper);
    abstract List <IPCWrapper> translateSensorUniqueCommand (IPCWrapper ipcWrapper);
    
    //KBM:   START of new code added for DigitalPhoenix
    //abstract List <IPCWrapper> translateGet_Identification (IPCWrapper ipcWrapper);
    abstract boolean translateGet_Identification (IPCWrapper ipcWrapper);
    //KBM:   END of new code for DigitalPhoenix
        
        
}
