/**
 * SPAWriterImplTemplate.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 

* Template for a Sensor Protocol Adapter Writer Impl
 */
package com.domenix.sensorprotocoladapter;

import com.domenix.common.config.SensorInterfaceConfig;
import com.domenix.common.spark.IPCWrapper;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Template for a Sensor Protocol Adapter Writer Impl
 *
 * Contains a translateXXX method for every possible message to be sent to the
 * sensor.
 *
 * You only need to populate the messages actually supported by this sensor.
 *
 * @author jmerritt
 */
public class SPAWriterImpl extends SensorProtocolAdapterWriter
{

    /** Error/Debug logger */
    private final Logger myLogger = Logger.getLogger(SPAWriterImpl.class);

    /** configuration for this sensor */
    private SensorInterfaceConfig sensorInterfaceConfig;

    /**
     * Initializes the translation function.
     *
     * @param configFileBase - configuration file location
     * @return
     */
    @Override
    public boolean initializeTranslator(String configFileBase)
    {
        try
        {
            sensorInterfaceConfig = new SensorInterfaceConfig(configFileBase);
        } catch (Exception ex)
        {
            myLogger.error("Error Initializing PA Writer - file: " + configFileBase);
            return false;
        }
        myLogger.info("Initializing PA Writer - file: " + configFileBase);
        return true;
    }

    //KBM:  START of new code for DigitalPhoenix
    @Override
    //public List<IPCWrapper> translateGet_Identification(IPCWrapper ipcWrapper)
    public boolean translateGet_Identification(IPCWrapper ipcWrapper)
    {
//        System.out.println("Translate Get_Identification.");
//        System.out.println();
//        
//        toSensorInterface.insertFirst(ipcWrapper);
//        //throw new UnsupportedOperationException("GET_IDENTIFICATION");
        return true;
    }
    //KBM:   END of new code for DigitalPhoenix
    
    @Override
    public List<IPCWrapper> translateStart_Stop(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translatePower_Off(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateRender_Useless(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateSet_Tamper(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateErase_Sec_Log(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateGet_Sec_Log(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateSet_Cmd_Privilege(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateSet_Config_Privilege(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateReboot(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateSet_Comm_Permission(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateSet_Local_Alert_Mode(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateStart_Self_Test(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateSet_Security_Timeout(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateClear_Alert(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateSilence(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateSet_Location(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateSet_Local_Enable(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateGet_Config(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateInstall_Start(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateInstall(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateInstall_Complete(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateSet_Ccsi_Mode(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateSet_Config(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateSet_Date_Time(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateGet_Alert_Details(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateGet_Reading_Details(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    List<IPCWrapper> translateSensorUniqueCommand(IPCWrapper ipcWrapper)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    

}
