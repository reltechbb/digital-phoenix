/*
 * SensorProtocolAdapterReader
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Abstract implementation of an SensorProtocolAdapterReaderInterface
 *
 */
package com.domenix.sensorprotocoladapter;

import com.domenix.common.spark.CtlWrapper;
import com.domenix.common.spark.CtlWrapperQueue;
import com.domenix.common.config.InterfaceSettingsBase;
import com.domenix.common.config.SensorInterfaceConfig;
import com.domenix.common.config.SensorUniqueConfigItem;
import com.domenix.commonsa.interfaces.SensorProtocolAdapterReaderInterface;
import com.domenix.common.spark.IPCWrapper;
import com.domenix.common.spark.IPCWrapperQueue;
import com.domenix.common.spark.data.SensorStateEnum;
import com.domenix.common.spark.IPCQueueEvent;
import com.domenix.common.spark.IPCQueueListenerInterface;
import com.domenix.commonsa.interfaces.SAQueues;
import com.domenix.commonsa.interfaces.SIATransferEvent;
import com.domenix.commonsa.interfaces.SIAXferEventQueue;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

/**
 * Abstract implementation of an SensorProtocolAdapterReaderInterface
 *
 * @author jmerritt
 */
public abstract class SensorProtocolAdapterReader implements Runnable,
        IPCQueueListenerInterface, SensorProtocolAdapterReaderInterface
{

    /**
     * Queue to send to the Protocol Handler
     */
    protected IPCWrapperQueue toProtocolHandler = null;
    /**
     * Queue to receive from the Interface Adapter Reader
     */
    private IPCWrapperQueue fromInterfaceAdapter = null;
    /**
     * Queue for sending to the Interface Adapter Writer
     */
    protected IPCWrapperQueue toInterfaceAdapterWriter = null;

    /**
     * Destination for control messages
     */
    private CtlWrapperQueue ctlWrapperQueue = null;
    /**
     * Fully qualified location of the configuration file
     */
    private String configFile = null;
    /**
     * The adapter should be shutdown
     */
    protected boolean shutdown = false;

    /**
     * Spark CCSI configuration information
     */
    InterfaceSettingsBase sensorConfig = null;

    /**
     * configuration file for sensor interface
     */
    SensorInterfaceConfig sensorInterfaceConfig = null;

    /**
     * Item ID
     */
    private int itemId = 0;

    /**
     * the logger
     */
    private final Logger logger = Logger.getLogger(SensorProtocolAdapterReader.class);

    /**
     * Internal queue of pending messages to be processed
     */
    protected LinkedBlockingQueue<IPCWrapper> pendingProcessingQueue = null;

    /**
     * size of the pendingProcessingQueue
     */
    private static final int PENDING_PROCESSING_QUEUE_SIZE = 10;

    /**
     * initialization is complete, ready to receive messages
     */
    protected boolean initComplete = false;

    /** List of sensor unique configuration item defaults used for initialization of sensor */
    protected List <SensorUniqueConfigItem> uniqueConfig;
    
    
    private SIAXferEventQueue phTransferEventQueue;
    
    private SIAXferEventQueue spaTransferEventQueue;
        
        
        
    /*
     * constructor
     */
    SensorProtocolAdapterReader()
    {
    }

    /**
     * the main thread.
     */
    @Override
    public void run()
    {
        ArrayList<String> errors = new ArrayList<>();
//        System.out.println("Sensor Protocol Adapter (SPA) Reader running.");
//        System.out.println();
        logger.info("Protocol Adapter Reader running");
        try
        {
            boolean sensorStarted = false;

            IPCWrapper entry;

            while (!shutdown)
            {
                errors.clear();
                try
                {
                    entry = this.pendingProcessingQueue.poll(6000, TimeUnit.MILLISECONDS);
                    if (entry != null)
                    {
                        if (initComplete)
                        {
                            try {
                                System.out.println((char)27 + "[35m" + "SPA Reader.run:  Received message:  " +  entry.getMsgBody().getCmd().getCmd());
                                System.out.println((char)27 + "[35m" + "SPA Reader.run:  ...more about message - Routing Key:  " +  entry.getRoutingKey());
                                System.out.println();
                                logger.debug ("Received message: " + entry.getRoutingKey());

//DO I NEED THIS NEXT BLOCK?!?!?!?!    IT PREVENTS EXCEPTION IN SWITCH STATEMENT.                               
//DO I NEED THIS NEXT BLOCK?!?!?!?!    IT PREVENTS EXCEPTION IN SWITCH STATEMENT.                               
//DO I NEED THIS NEXT BLOCK?!?!?!?!    IT PREVENTS EXCEPTION IN SWITCH STATEMENT.                               
                            if (entry.getRoutingKey()==null) {
                                entry.setRoutingKey("ccsi.cmd");
                            }
    
                            } catch(Exception e) {
                                System.out.println((char)27 + "[35m" + "SPAReader.run:  CAUGHT EXCEPTION WHEN PRINTING MSG BODY.");
                            }
                            
                            
                            if (!sensorStarted)
                            {
                                // got a message from the sensor, so its started
                                // build and sent a sensor start message
                                // to the protocol handler
                                buildSensorStartCmdMsg();
                                sensorStarted = true;

                                // send any initialization commands to the sensor
                                initializeSensor(sensorInterfaceConfig, sensorConfig);
                            }

                            switch (entry.getRoutingKey())
                            {
                                case "ccsi.event":
                                    IPCWrapper translatedMessage = translate(entry, errors);
                                    if (translatedMessage != null)
                                    {
                                        toProtocolHandler.insertLast(entry);    //translatedMessage);
                                        IPCWrapper successMessage = buildAckMessage();
                                        if (successMessage != null)
                                        {
                                            toInterfaceAdapterWriter.insertLast(successMessage);
                                        }
                                        logger.debug("Sending event message to PH");
                                        System.out.println("SPA Reader.run -> Sending event message to PH");
                                    } else
                                    {
                                        IPCWrapper errorMessage = buildNakMessage();
                                        if (errorMessage != null)
                                        {
                                            toInterfaceAdapterWriter.insertLast(entry);
                                        }
                                    }
                                    break;

                                case "ccsi.cmd.Get_Config":
                                    IPCWrapper message = processGet_Config(entry);
                                    if (!message.getMsgBody().getGetCfgResponse().getArg().isEmpty())
                                    {
                                        toProtocolHandler.insertLast(message);
                                    } else
                                    {
                                        logger.warn("Get_Config no items found, nothing returned");
                                    }
                                    break;
                                    
                                //KBM:   START of new code added for DigitalPhoenix
                                case "ccsi.cmd.Get_Identification":
//                                    System.out.println("SPA Reader.run -> GET IDENTIFICATION case branch!");
//                                    System.out.println();
                                    toProtocolHandler.insertLast(entry);
//                                    translateGet_Identification(ipcWrapper);
                                    //returnList = translateGet_Identification (ipcWrapper);
                                    break;
                                //KBM:   END of new code for DigitalPhoenix    

                                //KBM   START:   added to see if I need to return MSG to PH.    7/22/20    
                                case "ccsi.cmd":
                                    if (!entry.getMsgBody().getCmd().getCmd().isEmpty())
                                    {
                                        System.out.println((char)27 + "[35m" + "SPA Reader.run:  FORWARDING TO PH -- possibly an ACK!!!!");
                                        System.out.println();
                                        toProtocolHandler.insertLast(entry);
                                    } else
                                    {
                                        logger.warn("Get_Config no items found, nothing returned");
                                    }
                                    break;
                                //KBM   END:   added to see if I need to return MSG to PH.    7/22/20  
                                    
                                default:
                                    System.out.println((char)27 + "[35m" + "SPA Reader.run -> DEFAULT case branch!    no routing key, nothing happening....");
                                    System.out.println();
                                    logger.error("Routing key not set");
                                    break;
                            }

                        } else
                        {
                            logger.warn("Message received before init complete, discarding");
                            System.out.println((char)27 + "[35m" + "SPA Reader.run:  Message received before init complete, discarding");
                        }
                    } else
                    {
                        
                        
//TOM -- Build ACK????                        
//TOM -- Build ACK????                        
//TOM -- Build ACK????                        
//TOM -- Build ACK????                        
//TOM -- Build ACK????                        
//TOM -- Build ACK????                        
//TOM -- Build ACK????                        
//TOM -- Build ACK????                                              
                        // some sensors and simulators require sending something to them
                        // prior to returning a reading , this supports this
                        IPCWrapper successMessage = buildAckMessage();
                        if (successMessage != null)
                        {
                            System.out.println((char)27 + "[35m" + "SPA Reader.run:  about to send to SIA Writer: " + successMessage.getMsgBody().getCmd().getCmd());
                            System.out.println();
                            toInterfaceAdapterWriter.insertLast(successMessage);
                        }

                    }
                } catch (Exception ex)
                {
                    logger.error("Exception: ", ex);
                    System.out.println((char)27 + "[35m" + "SPAReader.run:  CAUGHT EXCEPTION 1.");
                }
            }
            logger.warn("Shutting Down SP Adapter Reader");
            System.out.println((char)27 + "[35m" + "SPA Reader.run:   Shutting Down SP Adapter Reader");
        } catch (Exception iex)
        {
            logger.error("Exception, thread exiting: ", iex);
            System.out.println((char)27 + "[35m" + "SPAReader.run:  CAUGHT EXCEPTION 2.");
        }
    }

    /**
     * Handle a queue event
     *
     * @param evt - the event
     */
    @Override
    public void queueEventFired(IPCQueueEvent evt)
    {
//        IPCWrapper iw = this.fromInterfaceAdapter.readFirst(0);
//
//        System.out.println((char)27 + "[35m" + "SPA Reader.queueEventFired, after read from Queue:  " + iw.getMsgBody().getCmd().getCmd());
        System.out.println((char)27 + "[35m" + "SPA Reader.queueEventFired, after read from Queue:  " + "  evt type = " + evt.getEventType() + "   evt source:  " + evt.getEventSource());
        System.out.println();
        
        if (evt.getEventSource() instanceof SIAXferEventQueue)
        {
            SIATransferEvent xfrEvt = this.spaTransferEventQueue.readFirst(0);

            
            
//TOM -- the received HEARTBEAT from SIA is null!!!???!!            
//TOM -- the received HEARTBEAT from SIA is null!!!???!!            
//TOM -- the received HEARTBEAT from SIA is null!!!???!!            
//TOM -- the received HEARTBEAT from SIA is null!!!???!!            
//TOM -- the received HEARTBEAT from SIA is null!!!???!!            
            if (xfrEvt != null)
            {
                if (evt.getEventType() == IPCQueueEvent.IPCQueueEventType.IPC_NEW) {
                    System.out.println((char)27 + "[35m" + "SPA Reader.queueEventFired:  IPC_NEW:  " + (char)27 + "[31m" + xfrEvt.getXferType());
                    System.out.println(); 
                     IPCWrapper iw = this.fromInterfaceAdapter.readFirst(0);
                     
                    System.out.println((char)27 + "[35m" + "SPA Reader.queueEventFired:  IPC_NEW:  " + iw.getMsgBody().getCmd().getCmd());
                    System.out.println((char)27 + "[35m" + "SPA Reader.queueEventFired:  IPC_NEW:  " + "  evt type = " + evt.getEventType() + "   evt source:  " + evt.getEventSource());
                    System.out.println();   
                }
                
                System.out.println((char)27 + "[35m" + "SPA Reader.queueEventFired:  TRANSFER EVENT:  " + (char)27 + "[31m" + xfrEvt.getXferType());
                System.out.println();  
               
                //Propagate up to the PH.
                this.phTransferEventQueue.insertLast(xfrEvt);
            }
        }
        else if (pendingProcessingQueue.size() >= PENDING_PROCESSING_QUEUE_SIZE)
        {
            // the queue is full so discard the oldest one
            // should only happed during testing
            pendingProcessingQueue.peek();
            logger.warn("Queue is full, discarding 1 message");
        } else {
            
            IPCWrapper iw = this.fromInterfaceAdapter.readFirst(0);

            System.out.println((char)27 + "[35m" + "SPA Reader.queueEventFired, ....else:  " + iw.getMsgBody().getCmd().getCmd());
            System.out.println((char)27 + "[35m" + "SPA Reader.queueEventFired, ....else:  " + "  evt type = " + evt.getEventType() + "   evt source:  " + evt.getEventSource());
            System.out.println();
        
            // post to the internal queue
            this.pendingProcessingQueue.add(iw);
        }
    }

    /**
     * Initialize the Protocol Adapter Reader
     *
     * @param fromInterfaceAdapter - queue to receive on
     * @param toProtocolHandler - queue to send to
     * @param toInterfaceAdapterWriter
     * @param ctlWrapperQueue
     * @param uniqueConfig
     * @param sensorConfig
     * @param configFileBase - location of the configuration file
     * @param configFileName - name of the configuration file
     * @return
     */
    @Override
    public boolean initialize(SAQueues saQueues,              //IPCWrapperQueue fromInterfaceAdapter,
//            IPCWrapperQueue toProtocolHandler, IPCWrapperQueue toInterfaceAdapterWriter,
            CtlWrapperQueue ctlWrapperQueue,
            InterfaceSettingsBase sensorConfig,
            List <SensorUniqueConfigItem> uniqueConfig,
            String configFileBase, String configFileName)
    {
        this.toProtocolHandler = saQueues.getSpAdapterToPh();          //toProtocolHandler;   <- OLD
        this.toInterfaceAdapterWriter = saQueues.getSpWriterDataQueue();   //toInterfaceAdapterWriter;    <- OLD
        this.ctlWrapperQueue = ctlWrapperQueue;
        this.uniqueConfig = uniqueConfig;
        this.sensorConfig = sensorConfig;
        configFile = configFileBase + File.separator + configFileName;

        try
        {
            this.sensorInterfaceConfig = new SensorInterfaceConfig(configFileBase);
        } catch (Exception ex)
        {
            logger.error("Error initializing Sensor Interface Config: " + ex);
        }

        if (!initializeTranslator(configFile))
        {
            return false;
        }

        // internal queue
        pendingProcessingQueue = new LinkedBlockingQueue<>(PENDING_PROCESSING_QUEUE_SIZE);

        this.fromInterfaceAdapter = saQueues.getSiAdapterToSp();      //fromInterfaceAdapter;     <- OLD
        this.fromInterfaceAdapter.addIPCQueueEventListener(this);
        this.phTransferEventQueue = saQueues.getPhXferEventQueue();    //added PH XFER EVT Q on 8/17/20
//        this.phTransferEventQueue.addIPCQueueEventListener(this);
        this.spaTransferEventQueue = saQueues.getSpaXferEventQueue();
        this.spaTransferEventQueue.addIPCQueueEventListener(this);
        
        // init is complete, can now process messages
        initComplete = true;

        return true;
    }

    /**
     * Starts the main thread
     *
     * @param x - instance of SensorProtocolAdapterReader
     */
    @Override
    public void doRun(Executor x)
    {
        shutdown = false;
        x.execute(this);
    }

    /**
     * Shuts down the main thread
     *
     * @return - true = OK or false = an error occurred
     */
    @Override
    public boolean shutdown()
    {
        shutdown = true;

        return true;
    }

    /**
     * Abstract method to initialize the translator
     *
     * @param configFile - configuration file to read
     * @return - true = OK or false = an error occurred
     */
    abstract boolean initializeTranslator(String configFile);

    /**
     * Abstract method to initialize the sensor
     *
     * Implementation may simply call defaultInitializeSensor ()
     */
    abstract void initializeSensor(SensorInterfaceConfig configFile,
            InterfaceSettingsBase sensorConfig);

    /**
     * Abstract method to build and send a Sensor Start Command message
     *
     * the implementation may simply call defaultBuildSensorStartCmdMsg
     *
     */
    abstract void buildSensorStartCmdMsg();

    /**
     * Abstract method to translate the message to internal format The concrete
     * class should log any errors.
     *
     * @param message - the message to translate
     * @return - the translated message or null
     */
    abstract IPCWrapper translate(IPCWrapper message, ArrayList<String> errors);

    /**
     * Abstract method to build an ack message to be sent to the Sensor
     *
     * @return - IPCWrapper containing the message or null if unsupported
     */
    abstract IPCWrapper buildAckMessage();

    /**
     * Abstract method to build an nak message to be sent to the Sensor
     *
     * @return - IPCWrapper containing the message or null if unsupported
     */
    abstract IPCWrapper buildNakMessage();

    /**
     * Abstract method to process a Get_Config message. May simply be a stub.
     *
     * @param entry
     * @return
     */
    abstract IPCWrapper processGet_Config(IPCWrapper entry);

    /**
     * If the ack message defined in SensorInterfaceConfig is sufficient for
     * this sensor then have buildAckMessage just call this method and pass the
     * results back
     *
     * @return IPCWrapper or null if the property is not defined or empty
     */
    protected IPCWrapper defaultBuildAckMessage()
    {
        IPCWrapper ipc = null;
        if (sensorInterfaceConfig.getSensorAck() != null)
        {
            ipc = new IPCWrapper();
            ipc.setRawMsg(sensorInterfaceConfig.getSensorAck());
        }
        return ipc;
    }

    /**
     * If the Nack message defined in SensorInterfaceConfig is sufficient for
     * this sensor then have buildNakMessage just call this method and pass the
     * results back
     *
     * @return IPCWrapper or null if the property is not defined or empty
     */
    protected IPCWrapper defaultBuildNakMessage()
    {
        IPCWrapper ipc = null;
        if (sensorInterfaceConfig.getSensorNack() != null)
        {
            ipc = new IPCWrapper();
            ipc.setRawMsg(sensorInterfaceConfig.getSensorNack());
        }
        return ipc;
    }

    /**
     * Default method to send sensor initialization commands to the sensor
     *
     * @param configFile
     * @return
     */
    protected boolean defaultInitializeSensor(SensorInterfaceConfig configFile)
    {
        boolean retVal = true;
        try
        {
            for (String command : configFile.getSensorInitList())
            {
                logger.info("Init sensor: " + command);
                IPCWrapper ipcWrapper = new IPCWrapper();
                ipcWrapper.setRawMsg(command);
                toInterfaceAdapterWriter.insertLast(ipcWrapper);
            }
        } catch (Exception ex)
        {
            logger.error("Exception initializing sensor" + ex);
            retVal = false;
        }
        return retVal;
    }

    /**
     * Default method to build and send a sensor start command message This
     * notifies the Protocol Handler that the sensor is operational.
     */
    protected void defaultBuildSensorStartCmdMsg()
    {
        CtlWrapper ctlWrapper = new CtlWrapper();
        ctlWrapper.setCtlCommand(CtlWrapper.SENSOR_START_CMD);
//        ctlWrapper.setHwVers(sensorConfig.getHwVersion().toString());
//        ctlWrapper.setSwVers(sensorConfig.getSwVersion().toString());
        ctlWrapper.setState(SensorStateEnum.NORMAL);
//        ctlWrapper.setUuid(sensorConfig.getUuid());
//        ctlWrapper.setSerial(sensorConfig.getSerial());
        ctlWrapperQueue.insertFirst(ctlWrapper);
        logger.info("Sent SENSOR_START_CMD");
    }

    /**
     * Converts a value to milli grays
     *
     * @param value
     * @return
     */
    public double stringToMilliGray(String value) {
        double result = 0.0;
        if (value.length() >= 5) {
            String baseString = value.substring(0, 3);
            String exponentSign = value.substring(3, 4);
            String exponentString = value.substring(4);

            double base = Double.parseDouble(baseString) / 10.0;
            double exponent = Double.parseDouble(exponentString);
            if (exponentSign.equals("-")) {
                exponent = exponent * -1.0;
            }
            result = base * Math.pow(10.0, exponent);
        }

        return result;
    }

    /**
     * Gets and increments the Id. Wraps back to 0 at 999999999
     *
     * @return itemId
     */
    protected int getId()
    {
        itemId++;

        if (itemId > 999999999)
        {
            itemId = 0;
        }

        return itemId;
    }
}
