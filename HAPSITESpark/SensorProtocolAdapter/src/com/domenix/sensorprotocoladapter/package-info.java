/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * This package provides classes to translate between sensor and internal formats
 *
 *
 */
package com.domenix.sensorprotocoladapter;