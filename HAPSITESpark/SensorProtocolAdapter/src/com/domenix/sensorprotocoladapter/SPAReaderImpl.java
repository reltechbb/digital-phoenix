/**
 * SPAReaderImplTemplate.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 

* Template for a Sensor Protocol Adapter Reader Impl
 */
package com.domenix.sensorprotocoladapter;

import com.domenix.common.config.InterfaceSettingsBase;
import com.domenix.common.config.SensorInterfaceConfig;
import com.domenix.common.spark.IPCWrapper;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 * Template for a Sensor Protocol Adapter Reader Impl
 * @author jmerritt
 */
public class SPAReaderImpl extends SensorProtocolAdapterReader
{
    /** Error/Debug logger */
    private final Logger myLogger = Logger.getLogger(SPAReaderImpl.class);

    /** The name of the configuration file */
    private String configFile = null;

    /**
     * Initialization for this class
     *
     * @param configFile
     * @return boolean - true = OK or false = an error occurred
     */
    @Override
    public boolean initializeTranslator(String configFile)
    {
        this.configFile = configFile;
        myLogger.info("Initializing PA Adapter Reader - file: " + configFile);
        return true;
    }

    /**
     * Builds an error message
     *
     * @return IPCWrapper containing the message
     */
    @Override
    public IPCWrapper buildNakMessage()
    {
        IPCWrapper ipc = defaultBuildNakMessage ();
        return ipc;
    }

    /**
     * Builds an Acknowledge message
     *
     * @return IPCWrapper containing the message
     */
    @Override
    public IPCWrapper buildAckMessage()
    {
        IPCWrapper ipc = defaultBuildAckMessage ();
        return ipc;
    }

    /**
     * Translates the incoming message into internal format
     *
     * @param message
     * @param errors
     * @return IPCWrapper containing the translated message
     */
    @Override
    public IPCWrapper translate(IPCWrapper message, ArrayList<String> errors)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    IPCWrapper processGet_Config(IPCWrapper entry)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    void initializeSensor(SensorInterfaceConfig configFile, InterfaceSettingsBase sensorConfig)
    {
//        System.out.println("SPAReader.initializeSensor -> NOT NEEDED and remains UNIMPLEMENTED.");
        //throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    protected void buildSensorStartCmdMsg()
    {
        defaultBuildSensorStartCmdMsg ();
    }
}
