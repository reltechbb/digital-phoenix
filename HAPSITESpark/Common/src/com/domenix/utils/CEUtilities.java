/*
 * CEUtilities.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains functions to support CCSI combined compression and encryption.
 *
 * Version: V1.0  15/07/24
 */


package com.domenix.utils;

//~--- non-JDK imports --------------------------------------------------------

import java.nio.charset.Charset;
import javax.crypto.spec.SecretKeySpec;
import org.apache.log4j.Logger;

//~--- classes ----------------------------------------------------------------

/**
 * This class contains functions to perform combined compression and encryption for
 * CCSI communications using RFC 3173 specified operation ordering when both are selected.
 *
 * @author kmiller
 */
public class CEUtilities
{
  /** UTF-8 character set */
  private static final Charset	utf8	= Charset.forName( "UTF-8" );

  //~--- fields ---------------------------------------------------------------

  /** Compression utilities for use. */
  private final CompressionUtilities	compUtil	= new CompressionUtilities();

  /** encryption utilities for use. */
  private final EncryptionUtilities	encryptUtil	= new EncryptionUtilities();

  /** Debug/Error logger. */
  private final Logger	myLogger;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance.
   */
  public CEUtilities()
  {
    this.myLogger	= Logger.getLogger( CEUtilities.class );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Compress and/or encrypt an outbound message or transmission performing 
   *
   * @param msg string to be operated on
   * @param compress flag indicating compression is desired
   * @param encrypt flag indicating encryption is desired
   * @param key secret key for encryption
   *
   * @return the processed input
   */
  public String processOutputCommunication( String msg , boolean compress , boolean encrypt , SecretKeySpec key )
  {
    byte[]	working	= null;
    byte[]	temp		= null;

    if ( ( msg != null ) && ( msg.length() > 0 ) )
    {
      working	= msg.getBytes( utf8 );

      //
      // First, compress the block if that is selected
      //
      if ( compress )
      {
        temp	= compUtil.compressData( working );
        myLogger.info( "Compressed byte length is " + temp.length );
        working	= temp;
        temp		= null;
      }

      //
      // Next, encrypt the block if that is selected
      //
      if ( ( encrypt ) && ( key != null ) )
      {
        temp	= encryptUtil.encryptBytes( key , working );
        myLogger.info( "Encrypted byte length is " + temp.length );
        working	= temp;
        temp		= null;
      }
      else if ( encrypt )
      {
        myLogger.error( "Encryption specified without key" );

        return ( null );
      }

      //
      // If we did either compression or encryption or both, convert to Base64
      //
      if ( ( encrypt ) || ( compress ) )
      {
        String	ret	= Base64.encodeBytes( working );

        myLogger.info( "Base64 length is " + ret.length() );

        return ( ret );
      }
    }

    return ( msg );
  }

  /**
   * Process a received communication to decrypt and/or decompress it.
   *
   * @param msg string containing the input communication
   * @param compress flag indicating the input is compressed
   * @param encrypt flag indicating the input is encrypted
   * @param key secret key for decryption
   *
   * @return the processed message or null if a decryption error occurs
   */
  public String processInputCommunication( String msg , boolean compress , boolean encrypt , SecretKeySpec key )
  {
    byte[]	working	= null;
    byte[]	temp		= null;

    if ( ( msg != null ) && ( msg.length() > 0 ) )
    {
      if ( ( compress ) || ( encrypt ) )
      {
        working	= Base64.decode( msg );

        //
        // If the block is encrypted, decrypt it
        //
        if ( ( encrypt ) && ( key != null ) )
        {
          temp		= encryptUtil.decryptBytes( key , working );
          working	= temp;
          temp		= null;
        }
        else if ( encrypt )
        {
          myLogger.error( "Encryption specified without key" );

          return ( null );
        }

        //
        // If the block is compressed, decompress it
        //
        if ( compress )
        {
          temp		= compUtil.uncompressString( working );
          working	= temp;
          temp		= null;
        }

        //
        // If we did either decompression or decryption convert back to a string
        //
        if ( ( compress ) || ( encrypt ) )
        {
          return ( new String( working , utf8 ) );
        }
      }
    }

    return ( msg );
  }
}
