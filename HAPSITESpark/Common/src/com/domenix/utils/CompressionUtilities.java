/*
 * CompressionUtilities.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains utilities for handling CCSI compression operations using BZip2
 *
 * Version: V1.0  15/07/24
 */


package com.domenix.utils;

//~--- non-JDK imports --------------------------------------------------------

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.itadaki.bzip2.BZip2InputStream;
import org.itadaki.bzip2.BZip2OutputStream;

//~--- classes ----------------------------------------------------------------

/**
 * This class provides for BZip2 compression and decompression of strings for use
 * in constructing and parsing CCSI messages.
 *
 * @author kmiller
 */
public class CompressionUtilities
{
  /** Debug/Error Logger */
  private final Logger	myLogger;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   */
  public CompressionUtilities()
  {
    myLogger	= Logger.getLogger( CompressionUtilities.class );
    myLogger.info( "Constructed..." );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Compresses the provided input string and returns the compressed string as Base64
   *
   * @param inp byte array to be compressed
   *
   * @return byte array containing the compressed version converted or null if error
   */
  public byte[] compressData( byte[] inp )
  {
    ByteArrayOutputStream	baStrm			= null;
    BZip2OutputStream			tempStream	= null;
    byte[]								compTemp		= null;

    try
    {
      baStrm			= new java.io.ByteArrayOutputStream();
      tempStream	= new BZip2OutputStream( baStrm );
      tempStream.write( inp , 0 , inp.length );
      tempStream.finish();
      compTemp	= baStrm.toByteArray();
      baStrm.close();
    }
    catch ( Exception ex )
    {
      myLogger.error( "Exception compressing string" , ex );
      compTemp	= null;
    }

    return ( compTemp );
  }

  /**
   * Uncompress a string
   *
   * @param inp a compressed string
   *
   * @return String containing uncompressed version or null if error
   */
  public byte[] uncompressString( byte[] inp )
  {
    ByteArrayInputStream	baStrm			= null;
    BZip2InputStream			tempStream	= null;
    byte[]								decompTemp	= null;
    int										which				= 0;
    int										index				= 0;

    try
    {
      baStrm			= new java.io.ByteArrayInputStream( inp );
      tempStream	= new BZip2InputStream( baStrm , false );
      decompTemp	= new byte[32768];

      while ( ( ( which = tempStream.read() ) != -1 ) )
      {
        decompTemp[index++]	= ( (byte) ( which & 0x000000FF ) );
      }

      tempStream.close();
      baStrm.close();
    }
    catch ( Exception ex )
    {
      myLogger.error( "Exception Decompressing string" , ex );

      return ( null );
    }

    return ( java.util.Arrays.copyOfRange( decompTemp , 0 , index ) );
  }
}
