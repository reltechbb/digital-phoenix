/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * This package provides functions for capabilities required to support CCSI sensor operations.  These include
 * compression, encryption, inter-process communications, number formatting, the CCSI Open Interface
 * socket operations, and timers.
 *
 */
package com.domenix.utils;