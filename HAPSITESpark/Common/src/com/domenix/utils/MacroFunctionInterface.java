/*
 * MacroFunctionInterface.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the interface definition to be implemented for macro text replacement values.
 *
 * Version: V1.0  15/08/14
 */


package com.domenix.utils;

/**
 * This interface must be implemented by classes that provide values for text replacement of Macros.
 *
 * @author kmiller
 */
public interface MacroFunctionInterface
{
  /**
   * This function provides replacement text for all macros in an input string
   *
   * @param inString String containing the name of the macro being requested
   *
   * @return String containing the replacement text
   */
  public String getTranslation( String inString );
  
  /**
   * This function translates a specific macro to a String, which may be null.
   * 
   * @param macro
   * 
   * @return 
   */
  public String translateMacro( String macro );
}
