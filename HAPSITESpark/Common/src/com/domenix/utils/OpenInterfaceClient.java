/*
 * OpenInterfaceClient.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of a client socket used to connect to a host system when
 * it is acting as the server.
 *
 * Version: V1.0  15/07/31
 */


package com.domenix.utils;

//~--- non-JDK imports --------------------------------------------------------

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import org.apache.log4j.Logger;

//~--- classes ----------------------------------------------------------------

/**
 * This class is the CCSI Open Interface client type connection for the
 * communications validator.  In this case the sensor must be acting as
 * a TCP/IP server.
 *
 * @author kmiller
 */
public class OpenInterfaceClient
{
  /** The buffer size for the sockets. */
  private static final int	SOCKET_BUFFER_SIZE	= ( 1024 * 32 );

  //~--- fields ---------------------------------------------------------------

  /** The host's IP address */
  private InetAddress	hostIp	= null;

  /** The host's server port to connect with. */
  private int	hostPort	= -1;

  /** The socket connected to the host. */
  private Socket	theSocket	= null;

  /** Log4j logger. */
  private final Logger	myLogger;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance of a client connection to a host being tested.
   *
   * @param addr InetAddress of the host acting as a server
   * @param port int containing the server port number of the host which, by default, is 5674
   *
   * @throws IOException
   */
  public OpenInterfaceClient( InetAddress addr , int port ) throws IOException
  {
    myLogger	= Logger.getLogger( "ccsi_comm_validator" );

    if ( port <= 0 )
    {
      hostPort	= 5674;
    }
    else
    {
      hostPort	= port;
    }

    hostIp	= addr;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Initiate a connection to the sensor.  If success the Socket is returned,
   * else null.  A wait time of from 0 to 120000 milli-seconds may be specified.
   *
   * @param timeout int containing the number of milli-seconds each connect attempt should wait
   *
   * @return the socket connected to the sensor or null if the connection failed.
   *
   * @throws IllegalArgumentException
   */
  public Socket connectToServer( int timeout ) throws IllegalArgumentException
  {
    int			connRetryCount	= 5;
    boolean	connected				= false;

    try
    {
      if ( ( timeout < 0 ) || ( timeout > 120000 ) )
      {
        throw new IllegalArgumentException( "Invalid timeout value specified, must be >= 0 and <= 120000 ms" );
      }

      InetSocketAddress	sAddr	= new InetSocketAddress( hostIp , hostPort );

      while ( ( connRetryCount > 0 ) && ( !connected ) )
      {
        try
        {
          theSocket	= new Socket();
          theSocket.setReuseAddress( true );
          theSocket.setReceiveBufferSize( SOCKET_BUFFER_SIZE );
          theSocket.setSendBufferSize( SOCKET_BUFFER_SIZE );
          theSocket.setTrafficClass( 0x04 );
          theSocket.setKeepAlive( true );
          theSocket.setSoTimeout( 500 );
          myLogger.warn( "Initiating connection to sensor." );
          theSocket.connect( sAddr , timeout );
          myLogger.warn( "Connection to sensor established." );
          connected	= true;
        }
        catch ( SocketTimeoutException tox )
        {
          myLogger.error( tox );
          theSocket	= null;
        }
        catch ( IOException iox )
        {
          myLogger.error( iox );
          theSocket	= null;
        }

        connRetryCount--;
      }

      if ( !connected )
      {
        theSocket	= null;
      }
    }
    catch ( Exception ex )
    {
      myLogger.error( "Error establishing connection to a host." , ex );
      theSocket	= null;
    }

    return ( theSocket );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the socket that is connected to the host or null
   * if not connected.
   *
   * @return Socket that is connected to the host or null
   */
  public Socket getTheSocket()
  {
    return ( theSocket );
  }

  /**
   * Returns the InputStream side of the socket that is connected
   * to the host.
   *
   * @return InputStream that is connected to the host
   */
  public InputStream getTheRecvStream()
  {
    try
    {
      InputStream	is	= theSocket.getInputStream();

      return ( is );
    }
    catch ( Exception ex )
    {
      return ( null );
    }
  }

  /**
   * Returns the OutputStream side of the socket that is connected
   * to the host
   *
   * @return OutputStream that is connected to the host
   */
  public OutputStream getTheSendStream()
  {
    try
    {
      OutputStream	os	= theSocket.getOutputStream();

      return ( os );
    }
    catch ( Exception ex )
    {
      return ( null );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This method closes the socket connection to the server.
   */
  public void close()
  {
    try
    {
      if ( theSocket != null )
      {
        theSocket.getOutputStream().close();
        theSocket.getInputStream().close();
        theSocket.close();
      }
    }
    catch ( Exception ex )
    {
      myLogger.error( ex );
    }
  }
}
