/*
 * OpenInterfaceServer.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the implementation of creating and using a TCP/IP ServerSocket.
 *
 * Version: V1.0  15/07/31
 */


package com.domenix.utils;

//~--- non-JDK imports --------------------------------------------------------

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import org.apache.log4j.Logger;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a TCP/IP server socket interface that a CCSI host can
 * connect to for communications.
 *
 * @author kmiller
 */
public class OpenInterfaceServer
{
  /** The buffer size for the sockets. */
  private static final int	SOCKET_BUFFER_SIZE	= ( 1024 * 32 );

  //~--- fields ---------------------------------------------------------------

  /** The server socket. */
  private ServerSocket	theSocket	= null;

  /** Error/Debug logger */
  private Logger	myLogger;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a server interface with a 32K receive buffer, address re-use,
   * and no socket timeout.
   *
   * @param port int containing the port number for the server socket, the default is 5674 which will 
   * be provided if the input port number is &le; 0.
   */
  public OpenInterfaceServer( int port )
  {
    myLogger	= Logger.getLogger( "ccsi_comm_validator" );
    myLogger.info( "Enter" );

    int	thePort	= port;

    if ( port == 0 )
    {
      thePort	= 5674;
    }

    try
    {
      theSocket	= new ServerSocket( thePort , 1 );
      theSocket.setReceiveBufferSize( SOCKET_BUFFER_SIZE );
      theSocket.setReuseAddress( true );
      theSocket.setSoTimeout( 0 );
    }
    catch ( Exception ex )
    {
      myLogger.error( ex );
    }

    myLogger.info( "Exit" );
  }

  /**
   * Constructs a server interface with a 32K receive buffer, address re-use,
   * and a specified socket timeout.
   *
   * @param port int containing the port number for the server socket, the default is 5674 which will 
   * be provided if the input port number is &le; 0.
   * @param timeout int containing the number of milli-seconds to wait for a connection, must be &le; 0 and &ge; 120000
   *
   * @throws IllegalArgumentException
   */
  public OpenInterfaceServer( int port , int timeout ) throws IllegalArgumentException
  {
    myLogger	= Logger.getLogger( "ccsi_comm_validator" );
    myLogger.info( "Enter" );

    int	thePort	= port;

    if ( port <= 0 )
    {
      thePort	= 5674;
    }

    if ( ( timeout < 0 ) || ( timeout > 120000 ) )
    {
      throw new IllegalArgumentException( "Invalid timeout value specified, must be >= 0 and <= 120000" );
    }

    try
    {
      theSocket	= new ServerSocket( thePort , 1 );
      theSocket.setReceiveBufferSize( SOCKET_BUFFER_SIZE );
      theSocket.setReuseAddress( true );
      theSocket.setSoTimeout( timeout );
    }
    catch ( Exception ex )
    {
      myLogger.error( ex );
    }

    myLogger.info( "Exit" );
  }

  /**
   * Constructs a server interface with a 32K receive buffer, address re-use,
   * and a specified socket timeout.
   *
   * @param addr String containing the host name or IP address
   * @param port int containing the port number for the server socket the default is 5674 which will 
   * be provided if the input port number is &le; 0.
   * @param timeout int containing the number of milli-seconds to wait for a connection, must be 
   * &ge; 0 and &le; 120000
   *
   * @throws IllegalArgumentException
   */
  public OpenInterfaceServer( String addr , int port , int timeout ) throws IllegalArgumentException
  {
    myLogger	= Logger.getLogger( "ccsi_comm_validator" );
    myLogger.info( "Enter" );

    int	thePort	= port;

    if ( port == 0 )
    {
      thePort	= 5674;
    }

    if ( ( timeout < 0 ) || ( timeout > 120000 ) )
    {
      throw new IllegalArgumentException( "Invalid timeout value specified, must be >= 0 and <= 120000" );
    }

    try
    {
      theSocket	= new ServerSocket( thePort , 1 , InetAddress.getByName( addr ) );
      theSocket.setReceiveBufferSize( SOCKET_BUFFER_SIZE );
      theSocket.setReuseAddress( true );
      theSocket.setSoTimeout( timeout );
    }
    catch ( Exception ex )
    {
      myLogger.error( ex );
    }

    myLogger.info( "Exit" );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This method listens for and accepts an incoming host connection.  The
   * method will listen without timeout until either a connection is accepted
   * or the server socket is closed.
   *
   * @return Socket connection to a host or null if timeout or exception thrown
   *
   * @throws SocketException
   */
  public Socket getConnection() throws SocketException
  {
    Socket	connSocket	= null;
    boolean	doMore			= true;

    myLogger.info( "Enter" );

    while ( doMore )
    {
      try
      {
        connSocket	= theSocket.accept();
        connSocket.setSoTimeout( 500 );

        break;
      }
      catch ( SocketTimeoutException sockEx )
      {
        myLogger.info( sockEx );
        connSocket	= null;
        doMore			= false;
      }
      catch ( Exception ex )
      {
        myLogger.error( ex );
        doMore			= false;
        connSocket	= null;

        throw new SocketException( "Fatal server socket error" );
      }
    }

    myLogger.info( "Exit" );

    return ( connSocket );
  }

  /**
   * This method listens for and accepts an incoming host connection.  The
   * method will listen without timeout until either a connection is accepted
   * or the server socket is closed.
   *
   * @param timeout number of milliseconds to wait for a connection, , must be &ge; 0 and &le; 120000
   *
   * @return Socket connection to a sensor or null if timeout or exception thrown
   *
   *
   * @throws IllegalArgumentException
   * @throws SocketException
   */
  public Socket getConnection( int timeout ) throws IllegalArgumentException , SocketException
  {
    Socket	connSocket	= null;
    boolean	doMore			= true;

    myLogger.info( "Enter" );

    if ( ( timeout < 0 ) || ( timeout > 120000 ) )
    {
      throw new IllegalArgumentException( "Invalid timeout value, must be 0 <= timeout <= 120000" );
    }

    while ( doMore )
    {
      try
      {
        connSocket	= theSocket.accept();
        connSocket.setSoTimeout( timeout );

        break;
      }
      catch ( SocketTimeoutException sockEx )
      {
        myLogger.info( sockEx );
        connSocket	= null;
        doMore			= false;
      }
      catch ( Exception ex )
      {
        myLogger.error( ex );
        doMore			= false;
        connSocket	= null;

        throw new SocketException( "Fatal server socket error" );
      }
    }

    myLogger.info( "Exit" );

    return ( connSocket );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This method closes the server socket and terminates listening.
   */
  public void closeServer()
  {
    try
    {
      myLogger.info( "Closing the listener server socket." );

      if ( theSocket != null )
      {
        theSocket.close();
        theSocket	= null;
      }
    }
    catch ( Exception ex )
    {
      myLogger.error( ex );
    }
  }
}
