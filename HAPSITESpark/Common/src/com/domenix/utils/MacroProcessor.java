/*
 * MacroProcessor.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains an abstract base class for performing macro replacement in strings. Implementations
 * must
 *
 * Version: V1.0  15/08/14
 */


package com.domenix.utils;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//~--- classes ----------------------------------------------------------------

/**
 * This abstract class processes a set of 'Macros' to perform text substitution within a text string.  Macros are character sequences
 * with a known text string enclosed in braces (e.g., {msgreceived}, {linkname}, etc.) for which an actual
 * value appropriate to the current state of the sensor, its environment, and it connections status.
 *
 * @author kmiller
 */
public abstract class MacroProcessor implements MacroFunctionInterface
{
  /** Pattern for finding macros */
  protected String	textMacros	= "";

//Example setup of the text macro pattern string. All macro strings should be lower case letters:  textMacros  =
//"\\{((new)?(alertid|readingid|dtgid|identhostsensorid|hostsensorid|hostip|sensorip|portNumber|netmassk|linkport|broadcastip|gatewayip|sensoruuid|createdate))\\}";

  /**
   * HashMap that that maps macro names without braces to the class that implements the translation for the macro,
   * in most cases this will be a reference to 'this'.
   */
  protected final HashMap<String , Object>	macroMap;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance
   */
  public MacroProcessor()
  {
    macroMap	= new HashMap<>();
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This method processes any known macros in a text string and provides the replacement text.  If
   * the macro is not known to this processor it will ignore the macro and leave it in place to be
   * processed by a different macro processor.
   *
   * @param text the text string to be processed and have macros replaced
   *
   * @return String containing the replaced macros
   */
  private String processMacros( String text )
  {
    String	newText	= text;
    Pattern	aPat		= Pattern.compile( this.textMacros );
    Matcher	mPat		= aPat.matcher( text );

    if ( mPat.find() )
    {
      do
      {
        String	repl			= null;
        String	macro			= mPat.group( 1 ).toLowerCase();
        Object	replFunc	= macroMap.get( macro );

        if ( replFunc != null )
        {
          repl	= ( (MacroFunctionInterface) replFunc ).translateMacro( macro );

          if ( repl != null )
          {
            newText	= newText.replaceFirst( this.textMacros , repl );
          }
        }
      }
      while ( mPat.find( mPat.end() ) );
    }

    return ( newText );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This is the public interface for macro processing.  It accepts an input string and processes
   * the macros that it recognizes returning the resultant string or a null if any failure occurs
   *
   * @param inString the string to be processed
   *
   * @return String containing the translated results or null if an error occurred
   */
  @Override
  public String getTranslation( String inString )
  {
    if ( ( inString != null ) && ( !inString.trim().isEmpty() ) )
    {
      return ( this.processMacros( inString ) );
    }
    else
    {
      return ( null );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This is the internal interface used to translate a macro to its result
   *
   * @param macro String containing the macro
   * 
   * @return String containing the value or null if no value found
   */
  @Override
  public abstract String translateMacro( String macro );
}
