/*
 * EncryptionUtilities.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains functions that provide operations for encrypting/decrypting.
 *
 * Version: V1.0  15/07/24
 */


package com.domenix.utils;

//~--- JDK imports ------------------------------------------------------------

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.interfaces.PBEKey;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

//~--- classes ----------------------------------------------------------------

/**
 * This class provides utilities for testing encryption behavior between a sensor and a host
 * computer when encrypting messages and communications beyond SSL/TLS.
 *
 * @author kmiller
 */
public class EncryptionUtilities
{
  /** Field description */
  private static final String	ALLOWED_PWD_SPECIAL	= "!@#$%&*_-?<> ";

  /** Field description */
  private static final int	MAX_PWD_LENGTH	= 256;

  /** Field description */
  private static final int	MIN_PWD_DIGITS	= 2;

  /** Field description */
  private static final int	MIN_PWD_LENGTH	= 8;

  /** Field description */
  private static final int	MIN_PWD_LOWER	= 2;

  /** Field description */
  private static final int	MIN_PWD_SPECIAL	= 2;

  /** Field description */
  private static final int	MIN_PWD_UPPER	= 2;

  /** Field description */
  private static final String	PWD_KEY_ALGORITHM	= "PBKDF2WithHmacSHA1";

  /** Field description */
  private static final int	PWD_KEY_BIT_LENGTH	= 128;

  /** Field description */
  private static final int	PWD_KEY_ITERATION_COUNT	= 1023;

  /** Field description */
  private static final byte[]	keySalt;

  //~--- static initializers --------------------------------------------------

  static
  {
    keySalt	= new byte[]
    {
      -115 , -115 , -103 , -76 , -90 , -50 , 72 , 38 , -119 , 31 , -100 , -105 , -6 , -47 , -14 , -97
    };
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Create a key specification for the provided password if it is valid.
   *
   * @param pwd String containing the password to be used
   *
   * @return the secret key spec
   *
   * @throws Exception
   */
  public static SecretKeySpec buildKeySpec( final String pwd ) throws Exception
  {
    if ( validatePassword( pwd ) )
    {
      final PBEKeySpec	password	= new PBEKeySpec( pwd.toCharArray() , keySalt , PWD_KEY_ITERATION_COUNT ,
                                      PWD_KEY_BIT_LENGTH );
      final SecretKeyFactory	factory	= SecretKeyFactory.getInstance( PWD_KEY_ALGORITHM );
      final PBEKey						key			= (PBEKey) factory.generateSecret( password );
      final SecretKeySpec			sKey		= new SecretKeySpec( key.getEncoded() , "AES" );

      return sKey;
    }

    throw new IllegalArgumentException( "Password is not valid for defined constraints." );
  }

  /**
   * Validate the password against the criteria for validity.
   *
   * @param val the password to be validated
   *
   * @return flag indicating valid (true) or not valid (false)
   */
  public static boolean validatePassword( final String val )
  {
    boolean	retValue	= false;
    int			nUpper		= 0;
    int			nLower		= 0;
    int			nDigit		= 0;
    int			nSpec			= 0;

    if ( ( val.length() >= MIN_PWD_LENGTH ) && ( val.length() <= MAX_PWD_LENGTH ) )
    {
      for ( int i = 0 ; i < val.length() ; ++i )
      {
        final char	cur	= val.charAt( i );

        if ( Character.isDigit( cur ) )
        {
          ++nDigit;
        }
        else if ( Character.isUpperCase( cur ) )
        {
          ++nUpper;
        }
        else if ( Character.isLowerCase( cur ) )
        {
          ++nLower;
        }
        else
        {
          if ( ALLOWED_PWD_SPECIAL.indexOf( cur ) < 0 )
          {
            retValue	= false;
            nSpec			= -1;

            break;
          }

          ++nSpec;
        }
      }

      if ( ( nUpper >= MIN_PWD_UPPER ) && ( nLower >= MIN_PWD_LOWER ) && ( nDigit >= MIN_PWD_DIGITS ) &&
           ( nSpec >= MIN_PWD_SPECIAL ) )
      {
        retValue	= true;
      }
    }

    return retValue;
  }

  /**
   * Encrypt the provided byte array using the provided key specification.
   *
   * @param key the input key specification
   * @param data the data to be encrypted
   *
   * @return byte array containing the encrypted data
   */
  public byte[] encryptBytes( SecretKeySpec key , byte[] data )
  {
    if ( ( key != null ) && ( data != null ) )
    {
      try
      {
        Cipher	encryptor	= Cipher.getInstance( "AES" );

        encryptor.init( Cipher.ENCRYPT_MODE , key );

        byte[]	result	= encryptor.doFinal( data );

        return ( result );
      }
      catch ( NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException
              | BadPaddingException ex )
      {
      }
    }

    return ( null );
  }

  /**
   * Decrypt the provided byte array using the provided key specification.
   *
   * @param key the key specification
   * @param data the data to be decrypted
   *
   * @return byte array containing the decrypted data
   */
  public byte[] decryptBytes( SecretKeySpec key , byte[] data )
  {
    if ( ( key != null ) && ( data != null ) )
    {
      try
      {
        Cipher	encryptor	= Cipher.getInstance( "AES" );

        encryptor.init( Cipher.DECRYPT_MODE , key );

        byte[]	retValue	= encryptor.doFinal( data );

        return ( retValue );
      }
      catch ( NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException
              | BadPaddingException ex )
      {
      }
    }

    return ( null );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the password policy for display to a user.
   *
   * @return string containing the password policy
   */
  public static String getPasswordPolicy()
  {
    final StringBuilder	msg	= new StringBuilder( "Password Policy:\n - Min Length: " );

    msg.append( MIN_PWD_LENGTH );
    msg.append( "\n - Max Length: " );
    msg.append( MAX_PWD_LENGTH );
    msg.append( "\n - Min Digits: " );
    msg.append( MIN_PWD_DIGITS );
    msg.append( "\n - Min Upper Case: " );
    msg.append( MIN_PWD_UPPER );
    msg.append( "\n - Min Lower Case: " );
    msg.append( MIN_PWD_LOWER );
    msg.append( "\n - Min Special: " );
    msg.append( MIN_PWD_SPECIAL );
    msg.append( "\n - Allowed Special Chars: ||" );
    msg.append( ALLOWED_PWD_SPECIAL );
    msg.append( "||" );

    return msg.toString();
  }
}
