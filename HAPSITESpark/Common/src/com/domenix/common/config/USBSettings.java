/*
 * USBSettings.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Placeholder for future capability
 * Container for USB interface parameters
 */
package com.domenix.common.config;

/**
 * Placeholder for future capability
 * Container for USB interface parameters
 * @author jmerritt
 */
public class USBSettings extends InterfaceSettingsBase
{
    public USBSettings (String type)
    {
        super (type);
    }
}
