/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * This package contains the definition of the actions that can be taken by the sensor
 * protocol handler to operate on a command from the host.
 * Classes containing configuration data for the Sensor Protocol Adapter (SPA)
 * and Sensor Interface Adapter (SIA)
 * 
 * Also used by the Protocol Handler (PH)
 *
 */
package com.domenix.common.config;