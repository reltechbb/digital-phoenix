/*
 * SerialSettings.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the settings for a serial interface.
 *
 */
package com.domenix.common.config;

/**
 * Settings for a serial interface
 * @author jmerritt
 */
public class SerialSettings extends InterfaceSettingsBase
{
    // the device name
    private String device;
    // baud rate
    private String baudRate;
    // data bits 5, 6, 7 or 8
    private String dataBits;
    // parity none, odd or even
    private String parity;
    // Start bits
    private String startBits;
    // stop bits - 1 or 2
    private String stopBits;
    // flow control none, hardware or software
    private String flowControl;
    
    /**
     * Constructor - sets all values to defaults
     * @param type
     */
    public SerialSettings (String type)
    {
        super (type);
        device = "/dev/ttyAMA0";
        baudRate = "38400";
        dataBits = "8";
        parity = "none";
        startBits = "1";
        stopBits = "1";
        flowControl = "none";
    }
    
    public void setDevice (String device)
    {
        this.device = device;
    }
    
    public String getDevice ()
    {
        return device;
    }
    
    /**
     * Sets the baud rate
     * @param baud - the baud rate
     */
    public void setBaudRate (String baud)
    {
        this.baudRate = baud;
    }
    
    public String getBaudRate ()
    {
        return baudRate;
    }
    
    public int getBaudRateInteger ()
    {
        return Integer.parseInt(baudRate);
    }
    
    public void setDataBits (String bits)
    {
        this.dataBits = bits;
    }
    
    public String getDataBits ()
    {
        return dataBits;
    }
    
    public int getDataBitsInteger ()
    {
        return Integer.parseInt(dataBits);
    }
    
    public void setParity (String parity)
    {
        this.parity = parity;
    }
    
    public String getParity ()
    {
        return parity;
    }

    public void setStartBits (String bits)
    {
        this.startBits = bits;
    }
    
    public String getStartBits ()
    {
        return startBits;
    }
        
    public void setStopBits (String bits)
    {
        this.stopBits = bits;
    }
    
    public String getStopBits ()
    {
        return stopBits;
    }
    
    public int getStopBitsInteger ()
    {
        return Integer.parseInt(stopBits);
    }
    
    public void setFlowControl (String flow)
    {
        this.flowControl = flow;
    }
    
    public String getFlowControl ()
    {
        return flowControl;
    }
}
