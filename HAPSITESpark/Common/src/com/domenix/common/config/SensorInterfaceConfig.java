/*
 * SensorInterfaceConfig
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Reads a configuration file defining commands to send to the sensor
 *
 */
package com.domenix.common.config;

import com.domenix.common.ccsi.NameValuePair;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 * Reads a configuration file defining commands to send to the sensor
 *
 * @author jmerritt
 */
public class SensorInterfaceConfig {

    /**
     * default configuration file name override with -Dccsi.sia.configFile=<filename>
     */
    private static final String DEFAULT_CONFIG_FILE = "SensorInterfaceConfig.xml";

    /**
     * property key for the configuration file
     */
    private static final String DEFAULT_CONFIG_FILE_KEY = "ccsi.sia.configFile";

    /**
     * The properties
     */
    private final Properties theProperties = new Properties();

    /**
     * base directory
     */
    private final String baseFile;

    /**
     * configuration file name
     */
    private final String configFileName;

    /**
     * The error/debug logger
     */
    private final Logger myLogger = Logger.getLogger(SensorInterfaceConfig.class);

    /**
     * list of sensor initialization commands
     */
    private final ArrayList<NameValuePair> sensorInitList = new ArrayList<>();

    /**
     * list of sensor unique commands
     */
    private final ArrayList <NameValuePair> sensorUniqueCommandList = new ArrayList <>();
    
    /**
     * list of sensor unique configuration items
     */
    private final ArrayList <NameValuePair> sensorUniqueConfigList = new ArrayList <>();
    
    /**
     * sensor ack/nack commands
     */
    private String sensorAck = null;
    private String sensorNack = null;

    /**
     * sensor start/stop commands
     */
    private String sensorStart = null;
    private String sensorStop = null;

    /** clear alert */
    private String clearAlert = null;
     
    /** type of interface - true = ascii, false = binary */
    private boolean ascii = false;
    
    /** reboot sensor command */
    private String reboot = null;
    
    /** start and stop  of frame pattern */
    private byte [] frameStart = null;
    private byte [] frameStop = null;
    
    /** where in the frameStart and frameStop the frame starts/stops */
    private int frameStartOffset = 0;

    /** frame size in bytes */
    private final List<Integer> frameSizeList = new ArrayList<>();
    private int maxFrameSize = 0;
    
    /** the endian-ness of the byte stream */
    private ByteOrder endian = ByteOrder.LITTLE_ENDIAN;
    
    /** size of the message header, if any */
    private int headerSize = 0;
    
    /** sync pattern, if any */
    private String syncPattern = null;
    
    /** offset into the header to the message size */
    private int messageSizeOffset = 0;
    
    /** size of the message size field, in bytes */
    private int messageSizeSize = 0;
    
    /**
     * Constructs the class instance by reading the CCSI configuration file baseDir/configFileName
     *
     * @param baseFile
     * @throws Exception - any exception thrown by the reader/processor is propagated
     */
    public SensorInterfaceConfig(String baseFile) throws Exception {
        this.baseFile = baseFile;
        // get the configuration file name from the property
        String tempConfigFile = System.getProperty(DEFAULT_CONFIG_FILE_KEY);
        if ((tempConfigFile == null) || (tempConfigFile.isEmpty())) {
            // wasn't set, use the default
            configFileName = DEFAULT_CONFIG_FILE;
        } else {
            // use the supplied file name
            configFileName = tempConfigFile;
        }

        readSensorInterfaceConfig();

        myLogger.info("Done reading sensor interface config: " + configFileName);
    }

    /**
     * This method reads the sensor interface configuration file
     *
     * @throws Exception
     */
    private void readSensorInterfaceConfig() throws Exception {
        String fName = "";
        FileInputStream fis = null;
        try {
            fName = baseFile + File.separator + configFileName;

            fis = new FileInputStream(fName);

            theProperties.loadFromXML(fis);

            for (String x : theProperties.stringPropertyNames()) {
                if (x == null) {
                    myLogger.info("Null key returned.");
                } else {
                    String value = theProperties.getProperty(x);
//KBM:  7/9/2020 - removed to cut down on Logging.                    
//                    myLogger.info("Sensor Interface Config key = " + x + " val = " + value);
                    if ((value != null) && (value.length() > 0)) {
                        switch (x) {
                            case "sensor.ack":
                                sensorAck = value;
                                break;

                            case "sensor.nack":
                                sensorNack = value;
                                break;

                            case "sensor.start":
                                sensorStart = value;
                                break;

                            case "sensor.stop":
                                sensorStop = value;
                                break;

                            case "sensor.clearalert":
                                clearAlert = value;
                                break;
                                
                            case "sensor.asciiinterfacetype":
                                if (value.equalsIgnoreCase("true"))
                                {
                                    ascii = true;
                                } else
                                {
                                    ascii = false;
                                }
                                break;
                                
                            case "sensor.startframe":
                                String [] frameStartArray = value.split(",");
                                frameStart = new byte [frameStartArray.length];
                                for (int i = 0; i < frameStartArray.length; i++)
                                {
                                    frameStart [i] = (byte)Integer.parseInt(frameStartArray [i],16);
                                }
                                break;
                                
                            case "sensor.startframeoffset":
                                frameStartOffset = Integer.parseInt(value);
                                break;
                                
                            case "sensor.stopframe":
                                String [] frameStopArray = value.split(",");
                                frameStop = new byte [frameStopArray.length];
                                for (int i = 0; i < frameStopArray.length; i++)
                                {
                                    frameStop [i] = (byte)Integer.parseInt(frameStopArray [i],16);
                                }
                                break;
                                
                            case "sensor.framesize":
                                String [] stringList = value.split(",");
                                for (String entry : stringList)
                                {
                                    frameSizeList.add(Integer.parseInt(entry));
                                }
                                for (int i : frameSizeList)
                                {
                                    if (i > maxFrameSize)
                                    {
                                        maxFrameSize = i;
                                    }
                                }
                                break;
                                
                            case "sensor.endian":
                                if (value.equalsIgnoreCase("little"))
                                {
                                    endian = ByteOrder.LITTLE_ENDIAN;
                                }else
                                {
                                    endian = ByteOrder.BIG_ENDIAN;
                                }
                                break;
                                
                            case "sensor.reboot":
                                reboot = value;
                                break;
                                
                            case "sensor.headersize":
                                headerSize = Integer.parseInt(value);
                                break;
                            case "sensor.syncpattern":
                                syncPattern = value;
                                break;
                            case "sensor.messagesizeoffset":
                                messageSizeOffset = Integer.parseInt(value);
                                break;
                            case "sensor.messagesizesize":
                                messageSizeSize = Integer.parseInt(value);
                                break;
                                
                            default:
                                if (x.startsWith("sensor.init")) {
                                    // sensor initialization commands
                                    sensorInitList.add(new NameValuePair(x, value));
                                } else if (x.startsWith("sensor.unique.command") )
                                {
                                    // sensor unique commands
                                    String [] unique = value.split(",");
                                    sensorUniqueCommandList.add(new NameValuePair (unique[0], unique[1]));
                                } else if (x.startsWith("sensor.unique.config")) {
                                    // sensor unique config
                                    String [] unique = value.split(",");
                                    sensorUniqueConfigList.add(new NameValuePair (unique[0], unique[1]));
                                } else {
                                    myLogger.error("Unknown/invalid name/value pair: " + x
                                            + " " + value);
                                }
                        }
                    }
                }
            }
            // need to sort the initSensorList because otherwise its unordered
            Collections.sort(sensorInitList, (a, b) -> a.getTheName().compareTo(b.getTheName()));
        } catch (IOException ex) {
            myLogger.fatal("Cannot read configuration file: " + fName, ex);

            throw (ex);
        } catch (NumberFormatException ex) {
            myLogger.fatal("Number format exception reading configuration file: " + fName, ex);

            throw (ex);
        } 
        finally {
            if (fis != null) {
                fis.close();
            }
        }
    }

    /**
     * Gets a sensor start command
     *
     * @return command or null if undefined
     */
    public String getSensorStart() {
        return sensorStart;
    }

    /**
     * Gets a sensor stop command
     *
     * @return command or null if undefined
     */
    public String getSensorStop() {
        return sensorStop;
    }

    /**
     * Gets a sensor start command
     *
     * @return command or null if undefined
     */
    public String getSensorAck() {
        return sensorAck;
    }

    /**
     * Gets a sensor start command
     *
     * @return command or null if undefined
     */
    public String getSensorNack() {
        return sensorNack;
    }

    /**
     * Gets a command to clear alert
     * 
     * @return 
     */
    public String getClearAlert ()
    {
        return clearAlert;
    }
       
    /**
     * Gets a sensor initialization list
     *
     * @return list of commands to send
     */
    public List<String> getSensorInitList() {
        List<String> returnList = new ArrayList<>();
        for (NameValuePair pair : sensorInitList) {
            returnList.add(pair.getTheValue());
        }
        return returnList;
    }
    
    /** 
     * Indicates the type of interface: ascii (with carriage returns and line feeds) or binary
     * where the user has to accumulate data until a complete frame is received
     * 
     * @return the interface type: ascii (true) or binary (false)
     */
    public boolean getAscii ()
    {
        return ascii;
    }
    
    /**
     * returns the sequence of bytes that signifies a start of a frame
     * @return 
     */
    public byte [] getFrameStart ()
    {
        byte [] retVal = null;
        if (frameStart != null)
        {
            retVal = frameStart.clone();
        }
        return retVal;
    }
    
    /**
     * gets the offset in frameStart where the new frame actually starts
     * @return 
     */
    public int getFrameStartOffset ()
    {
        return frameStartOffset;
    }
    
    /**
     * Returns the byte pattern corresponding to the frame stop
     * @return 
     */
    public byte [] getFrameStop ()
    {
        byte [] retVal = null;
        if (frameStop != null)
        {
            retVal = frameStop.clone();
        }
        return retVal;
    }
    
    /**
     * Returns a list of valid frame sizes
     * @return
     */
    public List<Integer> getFrameSizeList ()
    {
        return frameSizeList;
    }
    
    /**
     * Determines if the size is in the list of valid frame sizes.  
     * @param size
     * @return true if valid or the Frame Size List is empty
     */
    public boolean isFrameSizeValid (int size)
    {
        boolean valid = false;
        if (frameSizeList.isEmpty()) {
            valid = true;
        } else {
            for (int entry : frameSizeList) {
                if (entry == size) {
                    valid = true;
                    break;
                }
            }
        }
        return valid;
    }
    /**
     * Scans the frame for the start frame pattern
     * @param frame
     * @return position match started or -1 = not found
     */
    public int findFrameStart (byte [] frame)
    {
        int position = -1;
        boolean found = false;

        if (frameStart != null) {
            for (int index = 0; index < frame.length && !found; index++) {
                if (frame[index] == frameStart[0]) {
                    if ((index + frameStart.length) < frame.length) {
                        for (int i = 1; i < frameStart.length; i++) {
                            if (frame[index + i] != frameStart[i]) {
                                break;
                            } else if (i + 1 == frameStart.length) {
                                found = true;
                                break;
                            }
                        }
                    }
                }
                if (found) {
                    position = index;
                }
            }
        }
        return position;
    }
    
    /**
     * Scans the frame starting at position for the frame end pattern.  If the frame size
     * is NOT one of the valid choices, it keeps scanning.
     *
     * @param frame
     * @param startPosition
     * @return position where the frame end pattern starts or -1 if not found
     */
    public int findFrameEnd (byte [] frame, int startPosition)
    {
        int position = -1;
        boolean found = false;

        if (frameStop != null) {
            for (int index = startPosition; index < frame.length && !found; index++) {
                if (frame[index] == frameStop[0]) {
                    // check if there are enough bytes for a frame stop
                    if ((index + frameStop.length) <= frame.length) {
                        // scan for a frame stop pattern
                        for (int i = 1; i < frameStop.length; i++) {
                            if (frame[index + i] != frameStop[i]) {
                                break;
                            } else if (i + 1 == frameStop.length) {
                                // found a candidate frame stop
                                found = true;
                                break;
                            }
                        }
                    }
                }
                // check if the canidate frame stop is real
                // if not keep looking
                if (found && isFrameSizeValid (index - startPosition + frameStop.length)) {
                    position = index;
                } else
                {
                    found = false;
                }
            }
        } else
        {
            found = true;
        }
        return position;
    }
        
    /**
     * Appends array2 to the end of array 1
     * @param array1
     * @param array2
     * @return 
     */
    public byte [] appendByteArray (byte [] array1, byte [] array2)
    {
        byte [] newByte = new byte [array1.length + array2.length];
        int wIndex = 0;
        for (byte entry : array1)
        {
            newByte [wIndex] = entry;
            wIndex++;
        }
        for (byte entry : array2)
        {
            newByte [wIndex] = entry;
            wIndex++;
        }
        return newByte;
    }
    
    /**
     * Returns a byte array with the first "start" bytes removed
     * @param array
     * @param start
     * @return 
     */
    public byte [] getPartByteArray (byte [] array, int start)
    {
        byte [] newByte = new byte [array.length - start];
        for (int i = start, j = 0; i < array.length; i++, j++)
        {
            newByte [j] = array [i];
        }
        return newByte;
    }
    
    /**
     * Extracts a portion of the byte array.  For through the end of the array set stop to message.length
     * @param message
     * @param start
     * @param stop
     * @return 
     */
    public byte [] getByteSubArray (byte [] message, int start, int stop)
    {
        byte [] newArray = new byte [stop - start];
        for (int i = 0; i < newArray.length; i++)
        {
            newArray [i] = message [start + i];
        }
        return newArray;
    }
    
    /**
     * returns the maximum configured frame size
     * @return 
     */
    public int getMaxFrameSize ()
    {
        return maxFrameSize;
    }
    /**
     * Gets the Little or Big endian byte order of the stream
     * @return 
     */
    public ByteOrder getByteOrder ()
    {
        return endian;
    }
    
    /**
     * Returns the Sensor Unique Command corresponding to commandName
     * @param commandName
     * @return the command or null if not found
     */
    public String getUniqueCommand (String commandName)
    {
        String value = null;
        for (NameValuePair x : sensorUniqueCommandList){
            if (x.getTheName().equalsIgnoreCase(commandName))
            {
                value = x.getTheValue();
                break;
            }
        }
        return value;
    }
    
    /**
     * Returns the Sensor Unique Configuration item corresponding to commandName
     * @param configName
     * @return the configuration item or null if not found
     */
    public String getUniqueConfig (String configName)
    {
        String value = null;
        for (NameValuePair x : sensorUniqueConfigList){
            if (x.getTheName().equalsIgnoreCase(configName))
            {
                value = x.getTheValue();
                break;
            }
        }
        return value;
    }
    
    public Integer getHeaderSize ()
    {
        return headerSize;
    }
    
    public String getSyncPattern ()
    {
        return syncPattern;
    }
    
    public Integer getMessageSizeOffset ()
    {
        return messageSizeOffset;
    }
    
    public Integer getMessageSizeSize ()
    {
        return messageSizeSize;
    }
}
