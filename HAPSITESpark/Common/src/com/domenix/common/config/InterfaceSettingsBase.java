/*
 * InterfaceSettingsBase.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD)
 *
 * Base class for holding attributes of an interface. 
 * Implementations include SerialSettings, NetworkSettings, USBSettings.
 *
 */
package com.domenix.common.config;

/**
 * Abstract base class for holding attributes of an interface. Implementations include SerialSettings,
 * NetworkSettings, USBSettings.
 *
 * @author jmerritt
 */
abstract public class InterfaceSettingsBase {

    /**
     * TCP, UDP, SERIAL or USB
     */
    private final String destination;

    /** max transmit size in bytes */
    private int maxTransmit = 32768;

    /** Interface destination: HOST or SENSOR
     * @return  
     */
    public String getInterfaceDestination() {
        return destination;
    }
    
    /**
     * 
     * @param destination - HOST or SENSOR
     */
    public InterfaceSettingsBase (String destination)
    {
        this.destination = destination;
    }
    /**
     * Gets the max transmit size in bytes
     * @return 
     */
    public int getMaxTransmit ()
    {
        return maxTransmit;
    }    
    
    /**
     * Sets the max transmit size in bytes
     * @param max - size in bytes
     */
    public void setMaxTransmit (int max)
    {
        maxTransmit = max;
    }
}
