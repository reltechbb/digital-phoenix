/*
 * NetworkSettings.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Contains configuration information for network settings: IP Address, netmask, etc.
 */
package com.domenix.common.config;

import com.domenix.common.enums.NetworkAddressTypeEnum;
import com.domenix.common.enums.NetworkProtocolTypeEnum;
import com.domenix.common.enums.NetworkTypeEnum;
import java.net.InetAddress;

/**
 * Contains configuration information for network settings: IP Address, netmask, etc.
 * @author jmerritt
 */
abstract public class NetworkSettings extends InterfaceSettingsBase
{
    private NetworkAddressTypeEnum networkAddressType = NetworkAddressTypeEnum.NA;
    private NetworkProtocolTypeEnum networkProtocolType = NetworkProtocolTypeEnum.Open;
    private InetAddress networkRegistryAddr;
    /** IP address of the time server */
    private InetAddress networkTimeAddr;
    /** true = there is a time server */
    private boolean networkTime = false;
    private NetworkTypeEnum networkType = NetworkTypeEnum.TCP;
    private String networkIPType; // IPv4 or IPv6
    /** Host or Sensor IP address */
    private InetAddress ip;
    /** Host or Sensor port number */
    private int port = 5673;
    private InetAddress broadcastIP;
    private InetAddress gatewayIP;
    private InetAddress netmask;
    /** Protocol Handler IP address */
    private InetAddress phIP;
    /** Protocol Handler port number */
    private int phPort = 5673;
    
    /**
     * Constructor
     * @param type - the destination HOST or SENSOR 
     */
    public NetworkSettings (String type)
    {
        super (type);
    }
    
    /**
     * Sets the NetworkAddressType
     * @param type 
     */
    public void setNetworkAddressType (NetworkAddressTypeEnum type)
    {
        networkAddressType = type;
    }    
    
    /**
     * Gets the NetworkAddressType
     * @return type 
     */
    public NetworkAddressTypeEnum getNetworkAddressType ()
    {
        return networkAddressType;
    }

    /**
     * Sets the NetworkProtocolType
     * @param type 
     */
    public void setNetworkProtocolType (NetworkProtocolTypeEnum type)
    {
        networkProtocolType = type;
    }    
    
    /**
     * Gets the NetworkProtocolType 
     * @return NetworkProtocolType
     */
    public NetworkProtocolTypeEnum getNetworkProtocolType ()
    {
        return networkProtocolType;
    }
        
    /**
     * Sets the registry address 
     * @param addr - the address
     */
    public void setNetworkRegistryAddr (InetAddress addr)
    {
        networkRegistryAddr = addr;
    }    
    
    /**
     * Gets the registry address 
     * @return the address
     */
     public InetAddress getNetworkRegistryAddr ()
    {
        return networkRegistryAddr;
    }
    
    /**
     * Sets the broadcast address 
     * @param addr - the address
     */
    public void setBroadcastAddr (InetAddress addr)
    {
        broadcastIP = addr;
    }    
    
    /**
     * Gets the broadcast address 
     * @return the address
     */
    public InetAddress getBroadcastAddr ()
    {
        return broadcastIP;
    }
    
    /**
     * Gets the gateway address 
     * @param addr - the address
     */
    public void setGatewayAddr (InetAddress addr)
    {
        gatewayIP = addr;
    }    
    /**
     * Gets the gateway address 
     * @return the address
     */
    public InetAddress getGatewayAddr ()
    {
        return gatewayIP;
    }
    
    /**
     * Sets the net mask 
     * @param addr - the mask
     */
    public void setNetmask (InetAddress addr)
    {
        netmask = addr;
    }    
    
    /**
     * Gets the net mask 
     * @return the net mask
     */
    public InetAddress getNetmask ()
    {
        return netmask;
    }
    
    /**
     * Gets the IP address 
     * @param addr - the address
     */
    public void setIP (InetAddress addr)
    {
        ip = addr;
    }    
    
    /**
     * Sets the IP address 
     * @return the address
     */
    public InetAddress getIP ()
    {
        return ip;
    }
    
    /**
     * Sets the port number 
     * @param port - the port
     */
    public void setPort (int port)
    {
        this.port = port;
    }
    
    /**
     * Gets the port number 
     * @return the port number
     */
    public int getPort ()
    {
        return port;
    }
    
    /**
     * Sets the IP address of the Protocol Handler
     * @param addr - the address
     */
    public void setPHIP (InetAddress addr)
    {
        phIP = addr;
    }    
    
    /**
     * Gets the IP address of the Protocol Handler
     * @return the address
     */
    public InetAddress getPHIP ()
    {
        return phIP;
    }
    
    /**
     * Sets the port number of the Protocol Handler
     * @param port - the port number
     */
    public void setPHPort (int port)
    {
        this.phPort = port;
    }
    
    /**
     * Gets the port number of the Protocol Handler
     * @return the port number
     */
    public int getPHPort ()
    {
        return phPort;
    }
    
    /**
     * sets existence of a network time source
     * @param time - the source exists
     */
    public void setNetworkTime (boolean time)
    {
        networkTime = time;
    }
    
    /**
     * Gets existence of a network time source
     * @return the source exists
     */
    public boolean isNetworkTime ()
    {
        return networkTime;
    }
    
    /**
     * Sets the IP address of the time server
     * @param addr - the address
     */
    public void setNetworkTimeAddr (InetAddress addr)
    {
        networkTimeAddr = addr;
    }    
    
    /**
     * Gets the IP address of the time server
     * @return the address
     */
    public InetAddress getNetworkTimeAddr ()
    {
        return networkTimeAddr;
    }

    /**
     * Sets the network type
     * @param type - the type
     */
    public void setNetworkType (NetworkTypeEnum type)
    {
        networkType = type;
    }
    
    /**
     * Gets the network type
     * @return the type
     */
    public NetworkTypeEnum getNetworkType ()
    {
        return networkType;
    }
    
    /**
     * Sets the network IP address type
     * @param type - the type
     */
    public void setNetworkIPType (String type)
    {
        networkIPType = type;
    }
    
    /**
     * Gets the network IP address type
     * @return the type
     */
    public String getNetworkIPType ()
    {
        return networkIPType;
    }
}
