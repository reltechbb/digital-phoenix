/*
 * SensorUniqueConfigItem.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Encapsulates a Sensor Unique Configuration Item
 *
 */
package com.domenix.common.config;

import com.domenix.common.ccsi.CommandHandlingTypeEnum;
import org.apache.log4j.Logger;

/**
 * Encapsulates a Sensor Unique Configuration Item
 * @author jmerritt
 */
public class SensorUniqueConfigItem
{
    /** Error/Debug Logger */
    private final Logger myLogger = Logger.getLogger(SensorUniqueConfigItem.class);
    
    private String itemName;
    private String itemType;
    private String value;
    /** How to handle the command. */
    private CommandHandlingTypeEnum	handling;
            
    public SensorUniqueConfigItem (String item)
    {
        String [] itemList = item.split("\\,");
        if (itemList.length == 4)
        {
            itemName = itemList [0];
            itemType = itemList [1];
            value = itemList [2];
            handling = CommandHandlingTypeEnum.valueOf(itemList [3]);
        } else
        {
            myLogger.error ("incorrect number arguments - ccsi.suconfig: " + item);
        }
    }
    
    public void setItemName (String name)
    {
        itemName = name;
    }
    
    public String getItemName ()
    {
        return itemName;
    }
    
    public void setItemType (String type)
    {
        itemType = type;
    }
    
    public String getItemType ()
    {
        return itemType;
    }
    
    public void setValue (String value)
    {
        this.value = value;
    }
    
    public String getValue ()
    {
        return value;
    }
    
    public void setHandling (CommandHandlingTypeEnum value)
    {
        handling = value;
    }
    
    public CommandHandlingTypeEnum getHandling ()
    {
        return handling;
    }
}
