/*
 * CtlWrapper.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of a queue wrapper for control messages.
 *
 * Version: V1.0  15/08/22
 */


package com.domenix.common.spark;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.common.spark.data.SensorStateEnum;
import com.domenix.common.spark.data.StartupMsg;

//~--- classes ----------------------------------------------------------------

/**
 * This class defines a queue entry wrapper for a control command
 *
 * @author kmiller
 */
public class CtlWrapper
{
  /** Sensor commanded shutdown */
  public static final int	SENSOR_SHUTDOWN	= 4;

  /** Sensor commanded start */
  public static final int	SENSOR_START_CMD	= 3;

  /** The command code for starting all readers */
  public static final int	START_ALL_READERS	= 2;

  /** The command code for stopping the thread */
  public static final int	STOP_THREAD	= 1;

  /** Serialization UID */
  private static final long	serialVersionUID	= 1L;

  //~--- fields ---------------------------------------------------------------

  /** The control command being sent */
  private int	ctlCommand	= 0;

  /** For a shutdown command, how long */
  private int	shutdownTime	= 0;

  /** reason for a failure */
  private String	failReason;

  /** Hardware version */
  private String	hwVers;

  /** serial number */
  private String	serial;

  /** Sensor State */
  private SensorStateEnum	state;

  /** software version of the sensor */
  private String	swVers;

  /** Sensor UUID */
  private String	uuid;

  //~--- constructors ---------------------------------------------------------

  /**
   * Default constructor
   */
  public CtlWrapper()
  {
  }

  /**
   * Constructs an instance of the wrapper with the specified command type
   *
   * @param type the type of control command
   */
  public CtlWrapper( int type )
  {
    this.ctlCommand	= type;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the ctlCommand
   */
  public int getCtlCommand()
  {
    return ctlCommand;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param ctlCommand the ctlCommand to set
   */
  public void setCtlCommand( int ctlCommand )
  {
    this.ctlCommand	= ctlCommand;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the shutdownTime
   */
  public int getShutdownTime()
  {
    return shutdownTime;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param shutdownTime the shutdownTime to set
   */
  public void setShutdownTime( int shutdownTime )
  {
    this.shutdownTime	= shutdownTime;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the state
   */
  public SensorStateEnum getState()
  {
    return state;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param state the state to set
   */
  public void setState( SensorStateEnum state )
  {
    this.state	= state;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the uuid
   */
  public String getUuid()
  {
    return uuid;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param uuid the uuid to set
   */
  public void setUuid( String uuid )
  {
    this.uuid	= uuid;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the serial
   */
  public String getSerial()
  {
    return serial;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param serial the serial to set
   */
  public void setSerial( String serial )
  {
    this.serial	= serial;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the swVers
   */
  public String getSwVers()
  {
    return swVers;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param swVers the swVers to set
   */
  public void setSwVers( String swVers )
  {
    this.swVers	= swVers;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the hwVers
   */
  public String getHwVers()
  {
    return hwVers;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param hwVers the hwVers to set
   */
  public void setHwVers( String hwVers )
  {
    this.hwVers	= hwVers;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the failReason
   */
  public String getFailReason()
  {
    return failReason;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param failReason the failReason to set
   */
  public void setFailReason( String failReason )
  {
    this.failReason	= failReason;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Load a start command's fields
   * @param msg
   */
  public void loadStartCmd( StartupMsg msg )
  {
    if ( msg.isSetFailReason() )
    {
      this.setFailReason( msg.getFailReason() );
    }

    if ( msg.isSetHwVers() )
    {
      this.setHwVers( msg.getHwVers() );
    }

    if ( msg.isSetSwVers() )
    {
      this.setSwVers( msg.getSwVers() );
    }

    if ( msg.isSetState() )
    {
      this.setState( msg.getState() );
    }

    if ( msg.isSetUuid() )
    {
      this.setUuid( msg.getUuid() );
    }
    
    if ( msg.isSetSerial() )
    {
      this.setSerial( msg.getSerial() );
    }
  }
}
