//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.03.29 at 01:35:24 PM EDT 
//


package com.domenix.common.spark.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CcsiStdCmdSupportedEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CcsiStdCmdSupportedEnum">
 *   &lt;restriction base="{urn:com:domenix:spark:sensor:internal}CcsiStdCmdEnum">
 *     &lt;enumeration value="Clear_Alert"/>
 *     &lt;enumeration value="Start_Stop"/>
 *     &lt;enumeration value="Set_Ccsi_Mode"/>
 *     &lt;enumeration value="Start_Self_Test"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CcsiStdCmdSupportedEnum")
@XmlEnum(CcsiStdCmdEnum.class)
public enum CcsiStdCmdSupportedEnum {

    @XmlEnumValue("Clear_Alert")
    CLEAR_ALERT(CcsiStdCmdEnum.CLEAR_ALERT),
    @XmlEnumValue("Start_Stop")
    START_STOP(CcsiStdCmdEnum.START_STOP),
    @XmlEnumValue("Set_Ccsi_Mode")
    SET_CCSI_MODE(CcsiStdCmdEnum.SET_CCSI_MODE),
    @XmlEnumValue("Start_Self_Test")
    START_SELF_TEST(CcsiStdCmdEnum.START_SELF_TEST);
    private final CcsiStdCmdEnum value;

    CcsiStdCmdSupportedEnum(CcsiStdCmdEnum v) {
        value = v;
    }

    public CcsiStdCmdEnum value() {
        return value;
    }

    public static CcsiStdCmdSupportedEnum fromValue(CcsiStdCmdEnum v) {
        for (CcsiStdCmdSupportedEnum c: CcsiStdCmdSupportedEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
