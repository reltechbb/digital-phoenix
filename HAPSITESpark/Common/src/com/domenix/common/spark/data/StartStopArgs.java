//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.03.29 at 01:35:24 PM EDT 
//


package com.domenix.common.spark.data;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import com.sun.xml.internal.bind.Locatable;
import com.sun.xml.internal.bind.annotation.XmlLocation;
import org.xml.sax.Locator;


/**
 * 
 *                 This defines the arguments that are valid for the CCSI Start_Stop command.
 *             
 * 
 * <p>Java class for StartStopArgs complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StartStopArgs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StartOrStop" type="{urn:com:domenix:spark:sensor:internal}StartStopEnum"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StartStopArgs", propOrder = {
    "startOrStop"
})
public class StartStopArgs
    implements Serializable, Locatable
{

    private final static long serialVersionUID = 12345L;
    @XmlElement(name = "StartOrStop", required = true)
    @XmlSchemaType(name = "string")
    protected StartStopEnum startOrStop;
    @XmlLocation
    @XmlTransient
    protected Locator locator;

    /**
     * Gets the value of the startOrStop property.
     * 
     * @return
     *     possible object is
     *     {@link StartStopEnum }
     *     
     */
    public StartStopEnum getStartOrStop() {
        return startOrStop;
    }

    /**
     * Sets the value of the startOrStop property.
     * 
     * @param value
     *     allowed object is
     *     {@link StartStopEnum }
     *     
     */
    public void setStartOrStop(StartStopEnum value) {
        this.startOrStop = value;
    }

    public boolean isSetStartOrStop() {
        return (this.startOrStop!= null);
    }

    public Locator sourceLocation() {
        return locator;
    }

    public void setSourceLocation(Locator newLocator) {
        locator = newLocator;
    }

}
