//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.03.29 at 01:35:24 PM EDT 
//


package com.domenix.common.spark.data;

import java.io.Serializable;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import com.sun.xml.internal.bind.Locatable;
import com.sun.xml.internal.bind.annotation.XmlLocation;
import org.xml.sax.Locator;


/**
 * 
 *         This defines a single row of spectrum information sent from the sensor to the CCSI component.  Each
 *         row has:
 *         polarity -> character [+|-}
 *         m1 -> double
 *         m2 -> double
 *         count -> int
 *       
 * 
 * <p>Java class for ReadingSpectrumRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReadingSpectrumRow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attGroup ref="{urn:com:domenix:spark:sensor:internal}SpectrumGroup"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReadingSpectrumRow")
public class ReadingSpectrumRow
    implements Serializable, Locatable
{

    private final static long serialVersionUID = 12345L;
    @XmlAttribute(name = "polarity", required = true)
    protected String polarity;
    @XmlAttribute(name = "m1", required = true)
    protected double m1;
    @XmlAttribute(name = "m2", required = true)
    protected double m2;
    @XmlAttribute(name = "count", required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger count;
    @XmlLocation
    @XmlTransient
    protected Locator locator;

    /**
     * Gets the value of the polarity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolarity() {
        return polarity;
    }

    /**
     * Sets the value of the polarity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolarity(String value) {
        this.polarity = value;
    }

    public boolean isSetPolarity() {
        return (this.polarity!= null);
    }

    /**
     * Gets the value of the m1 property.
     * 
     */
    public double getM1() {
        return m1;
    }

    /**
     * Sets the value of the m1 property.
     * 
     */
    public void setM1(double value) {
        this.m1 = value;
    }

    public boolean isSetM1() {
        return true;
    }

    /**
     * Gets the value of the m2 property.
     * 
     */
    public double getM2() {
        return m2;
    }

    /**
     * Sets the value of the m2 property.
     * 
     */
    public void setM2(double value) {
        this.m2 = value;
    }

    public boolean isSetM2() {
        return true;
    }

    /**
     * Gets the value of the count property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCount(BigInteger value) {
        this.count = value;
    }

    public boolean isSetCount() {
        return (this.count!= null);
    }

    public Locator sourceLocation() {
        return locator;
    }

    public void setSourceLocation(Locator newLocator) {
        locator = newLocator;
    }

}
