//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.03.29 at 01:35:24 PM EDT 
//


package com.domenix.common.spark.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CcsiCommandedModeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CcsiCommandedModeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NORMAL"/>
 *     &lt;enumeration value="EXERCS"/>
 *     &lt;enumeration value="TEST"/>
 *     &lt;enumeration value="TRAIN"/>
 *     &lt;enumeration value="SETUP"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CcsiCommandedModeEnum")
@XmlEnum
public enum CcsiCommandedModeEnum {

    NORMAL,
    EXERCS,
    TEST,
    TRAIN,
    SETUP;

    public String value() {
        return name();
    }

    public static CcsiCommandedModeEnum fromValue(String v) {
        return valueOf(v);
    }

}
