//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.03.29 at 01:35:24 PM EDT 
//


package com.domenix.common.spark.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoutingKeyEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoutingKeyEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ccsi.ctl.start"/>
 *     &lt;enumeration value="ccsi.ctl.shutdown"/>
 *     &lt;enumeration value="ccsi.cmd.Power_Off"/>
 *     &lt;enumeration value="ccsi.cmd.Render_Useless"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Tamper"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Emcon_Mode"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Encryption"/>
 *     &lt;enumeration value="ccsi.cmd.Erase_Sec_Log"/>
 *     &lt;enumeration value="ccsi.cmd.Get_Sec_Log"/>
 *     &lt;enumeration value="ccsi.cmd.Sanitize"/>
 *     &lt;enumeration value="ccsi.cmd.Zeroize"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Cmd_Privilege"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Config_Privilege"/>
 *     &lt;enumeration value="ccsi.cmd.Reset_Msg_Seq"/>
 *     &lt;enumeration value="ccsi.cmd.Start_Stop"/>
 *     &lt;enumeration value="ccsi.cmd.Reboot"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Comm_Permission"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Local_Alert_Mode"/>
 *     &lt;enumeration value="ccsi.cmd.Start_Self_Test"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Security_Timeout"/>
 *     &lt;enumeration value="ccsi.cmd.Clear_Alert"/>
 *     &lt;enumeration value="ccsi.cmd.Silence"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Heartbeat"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Location"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Data_Compression"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Local_Enable"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Bw_Mode"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Date_Time"/>
 *     &lt;enumeration value="ccsi.cmd.Get_Status"/>
 *     &lt;enumeration value="ccsi.cmd.Get_Configuration"/>
 *     &lt;enumeration value="ccsi.cmd.Install_Start"/>
 *     &lt;enumeration value="ccsi.cmd.Install"/>
 *     &lt;enumeration value="ccsi.cmd.Install_Complete"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Ccsi_Mode"/>
 *     &lt;enumeration value="ccsi.cmd.User_Change"/>
 *     &lt;enumeration value="ccsi.cmd.Get_Users"/>
 *     &lt;enumeration value="ccsi.cmd.Register"/>
 *     &lt;enumeration value="ccsi.cmd.Deregister"/>
 *     &lt;enumeration value="ccsi.cmd.Set_Config"/>
 *     &lt;enumeration value="ccsi.cmd.Get_Identification"/>
 *     &lt;enumeration value="ccsi.cmd.Get_Alert_Details"/>
 *     &lt;enumeration value="ccsi.cmd.Get_Reading_Details"/>
 *     &lt;enumeration value="ccsi.evt.reading"/>
 *     &lt;enumeration value="ccsi.evt.bit"/>
 *     &lt;enumeration value="ccsi.evt.consumable"/>
 *     &lt;enumeration value="ccsi.evt.status"/>
 *     &lt;enumeration value="ccsi.response.event"/>
 *     &lt;enumeration value="ccsi.response.ctl"/>
 *     &lt;enumeration value="ccsi.response.cmd"/>
 *     &lt;enumeration value="ccsi.evt.location"/>
 *     &lt;enumeration value="ccsi.evt.security"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoutingKeyEnum")
@XmlEnum
public enum RoutingKeyEnum {

    @XmlEnumValue("ccsi.ctl.start")
    CCSI_CTL_START("ccsi.ctl.start"),
    @XmlEnumValue("ccsi.ctl.shutdown")
    CCSI_CTL_SHUTDOWN("ccsi.ctl.shutdown"),
    @XmlEnumValue("ccsi.cmd.Power_Off")
    CCSI_CMD_POWER_OFF("ccsi.cmd.Power_Off"),
    @XmlEnumValue("ccsi.cmd.Render_Useless")
    CCSI_CMD_RENDER_USELESS("ccsi.cmd.Render_Useless"),
    @XmlEnumValue("ccsi.cmd.Set_Tamper")
    CCSI_CMD_SET_TAMPER("ccsi.cmd.Set_Tamper"),
    @XmlEnumValue("ccsi.cmd.Set_Emcon_Mode")
    CCSI_CMD_SET_EMCON_MODE("ccsi.cmd.Set_Emcon_Mode"),
    @XmlEnumValue("ccsi.cmd.Set_Encryption")
    CCSI_CMD_SET_ENCRYPTION("ccsi.cmd.Set_Encryption"),
    @XmlEnumValue("ccsi.cmd.Erase_Sec_Log")
    CCSI_CMD_ERASE_SEC_LOG("ccsi.cmd.Erase_Sec_Log"),
    @XmlEnumValue("ccsi.cmd.Get_Sec_Log")
    CCSI_CMD_GET_SEC_LOG("ccsi.cmd.Get_Sec_Log"),
    @XmlEnumValue("ccsi.cmd.Sanitize")
    CCSI_CMD_SANITIZE("ccsi.cmd.Sanitize"),
    @XmlEnumValue("ccsi.cmd.Zeroize")
    CCSI_CMD_ZEROIZE("ccsi.cmd.Zeroize"),
    @XmlEnumValue("ccsi.cmd.Set_Cmd_Privilege")
    CCSI_CMD_SET_CMD_PRIVILEGE("ccsi.cmd.Set_Cmd_Privilege"),
    @XmlEnumValue("ccsi.cmd.Set_Config_Privilege")
    CCSI_CMD_SET_CONFIG_PRIVILEGE("ccsi.cmd.Set_Config_Privilege"),
    @XmlEnumValue("ccsi.cmd.Reset_Msg_Seq")
    CCSI_CMD_RESET_MSG_SEQ("ccsi.cmd.Reset_Msg_Seq"),
    @XmlEnumValue("ccsi.cmd.Start_Stop")
    CCSI_CMD_START_STOP("ccsi.cmd.Start_Stop"),
    @XmlEnumValue("ccsi.cmd.Reboot")
    CCSI_CMD_REBOOT("ccsi.cmd.Reboot"),
    @XmlEnumValue("ccsi.cmd.Set_Comm_Permission")
    CCSI_CMD_SET_COMM_PERMISSION("ccsi.cmd.Set_Comm_Permission"),
    @XmlEnumValue("ccsi.cmd.Set_Local_Alert_Mode")
    CCSI_CMD_SET_LOCAL_ALERT_MODE("ccsi.cmd.Set_Local_Alert_Mode"),
    @XmlEnumValue("ccsi.cmd.Start_Self_Test")
    CCSI_CMD_START_SELF_TEST("ccsi.cmd.Start_Self_Test"),
    @XmlEnumValue("ccsi.cmd.Set_Security_Timeout")
    CCSI_CMD_SET_SECURITY_TIMEOUT("ccsi.cmd.Set_Security_Timeout"),
    @XmlEnumValue("ccsi.cmd.Clear_Alert")
    CCSI_CMD_CLEAR_ALERT("ccsi.cmd.Clear_Alert"),
    @XmlEnumValue("ccsi.cmd.Silence")
    CCSI_CMD_SILENCE("ccsi.cmd.Silence"),
    @XmlEnumValue("ccsi.cmd.Set_Heartbeat")
    CCSI_CMD_SET_HEARTBEAT("ccsi.cmd.Set_Heartbeat"),
    @XmlEnumValue("ccsi.cmd.Set_Location")
    CCSI_CMD_SET_LOCATION("ccsi.cmd.Set_Location"),
    @XmlEnumValue("ccsi.cmd.Set_Data_Compression")
    CCSI_CMD_SET_DATA_COMPRESSION("ccsi.cmd.Set_Data_Compression"),
    @XmlEnumValue("ccsi.cmd.Set_Local_Enable")
    CCSI_CMD_SET_LOCAL_ENABLE("ccsi.cmd.Set_Local_Enable"),
    @XmlEnumValue("ccsi.cmd.Set_Bw_Mode")
    CCSI_CMD_SET_BW_MODE("ccsi.cmd.Set_Bw_Mode"),
    @XmlEnumValue("ccsi.cmd.Set_Date_Time")
    CCSI_CMD_SET_DATE_TIME("ccsi.cmd.Set_Date_Time"),
    @XmlEnumValue("ccsi.cmd.Get_Status")
    CCSI_CMD_GET_STATUS("ccsi.cmd.Get_Status"),
    @XmlEnumValue("ccsi.cmd.Get_Configuration")
    CCSI_CMD_GET_CONFIGURATION("ccsi.cmd.Get_Configuration"),
    @XmlEnumValue("ccsi.cmd.Install_Start")
    CCSI_CMD_INSTALL_START("ccsi.cmd.Install_Start"),
    @XmlEnumValue("ccsi.cmd.Install")
    CCSI_CMD_INSTALL("ccsi.cmd.Install"),
    @XmlEnumValue("ccsi.cmd.Install_Complete")
    CCSI_CMD_INSTALL_COMPLETE("ccsi.cmd.Install_Complete"),
    @XmlEnumValue("ccsi.cmd.Set_Ccsi_Mode")
    CCSI_CMD_SET_CCSI_MODE("ccsi.cmd.Set_Ccsi_Mode"),
    @XmlEnumValue("ccsi.cmd.User_Change")
    CCSI_CMD_USER_CHANGE("ccsi.cmd.User_Change"),
    @XmlEnumValue("ccsi.cmd.Get_Users")
    CCSI_CMD_GET_USERS("ccsi.cmd.Get_Users"),
    @XmlEnumValue("ccsi.cmd.Register")
    CCSI_CMD_REGISTER("ccsi.cmd.Register"),
    @XmlEnumValue("ccsi.cmd.Deregister")
    CCSI_CMD_DEREGISTER("ccsi.cmd.Deregister"),
    @XmlEnumValue("ccsi.cmd.Set_Config")
    CCSI_CMD_SET_CONFIG("ccsi.cmd.Set_Config"),
    @XmlEnumValue("ccsi.cmd.Get_Identification")
    CCSI_CMD_GET_IDENTIFICATION("ccsi.cmd.Get_Identification"),
    @XmlEnumValue("ccsi.cmd.Get_Alert_Details")
    CCSI_CMD_GET_ALERT_DETAILS("ccsi.cmd.Get_Alert_Details"),
    @XmlEnumValue("ccsi.cmd.Get_Reading_Details")
    CCSI_CMD_GET_READING_DETAILS("ccsi.cmd.Get_Reading_Details"),
    @XmlEnumValue("ccsi.evt.reading")
    CCSI_EVT_READING("ccsi.evt.reading"),
    @XmlEnumValue("ccsi.evt.bit")
    CCSI_EVT_BIT("ccsi.evt.bit"),
    @XmlEnumValue("ccsi.evt.consumable")
    CCSI_EVT_CONSUMABLE("ccsi.evt.consumable"),
    @XmlEnumValue("ccsi.evt.status")
    CCSI_EVT_STATUS("ccsi.evt.status"),
    @XmlEnumValue("ccsi.response.event")
    CCSI_RESPONSE_EVENT("ccsi.response.event"),
    @XmlEnumValue("ccsi.response.ctl")
    CCSI_RESPONSE_CTL("ccsi.response.ctl"),
    @XmlEnumValue("ccsi.response.cmd")
    CCSI_RESPONSE_CMD("ccsi.response.cmd"),
    @XmlEnumValue("ccsi.evt.location")
    CCSI_EVT_LOCATION("ccsi.evt.location"),
    @XmlEnumValue("ccsi.evt.security")
    CCSI_EVT_SECURITY("ccsi.evt.security");
    private final String value;

    RoutingKeyEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoutingKeyEnum fromValue(String v) {
        for (RoutingKeyEnum c: RoutingKeyEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
