/*
 * IPCWrapper.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains a wrapper class for RabbitMQ messages that includes the routing key.
 *
 * Version: V1.0  15/08/22
 */


package com.domenix.common.spark;

//~--- non-JDK imports --------------------------------------------------------

import com.domenix.common.spark.data.CommandMsg;
import com.domenix.common.spark.data.MsgContent;
import com.domenix.common.utils.NumberFormatter;
import java.io.File;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.io.StringReader;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.xml.sax.SAXException;

//~--- classes ----------------------------------------------------------------

/**
 * This class wraps a MsgContent instance in another structure that contains the routing key
 * to be written to or the key for the received message.
 *
 * @author kmiller
 */
public class IPCWrapper implements Serializable , Comparable
{
  /** Serialization UID */
  private static final long	serialVersionUID	= -8472510125153790358L;
  
  /** The Schema file for messages */
  private static final String SCHEMA_FILE = System.getProperty("ccsi.baseDir") 
          + File.separator + "Spark_Queue_Entries.xsd";

  //~--- fields ---------------------------------------------------------------
  
  /** The alert ID for entry */
  private long alertId = -1L;

  /** Cache Key */
  private String	cacheKey	= null;

  /** The body of the queue message */
  private transient MsgContent	msgBody	= null;

  /** The message received from the sensor - only one may be populated */
  private String	rawMsg	= null;
  private byte []	rawMsgBinary	= null;

  /** The message routing key */
  private String	routingKey	= null;

  /** Error/Debug Logger */
  private transient Logger	myLogger	= Logger.getLogger( IPCWrapper.class );

  /** Timestamp */
  private long	stamp;
  
  /** Our schema */
  private transient File schema;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an empty class instance.
   */
  public IPCWrapper()
  {
    this.stamp	= System.currentTimeMillis();
    try
    {
      this.schema = new File( SCHEMA_FILE );
      if ( !this.schema.canRead() )
      {
        throw new IllegalArgumentException( "Cannot read schema file: " + this.schema.getAbsolutePath() );
      }
    }
    catch( Exception ex )
    {
      myLogger.error( "Exception creating class instance" , ex );
    }
  }

  /**
   * Constructs a populated class instance and provided by the key and message body.
   *
   * @param key the routing key
   * @param body the message body
   */
  public IPCWrapper( String key , MsgContent body )
  {
    this.routingKey	= key;
    this.msgBody		= body;
    this.stamp			= System.currentTimeMillis();
    makeCacheKey();
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Makes a cache key from the routing key and whether or not the contained event message
   * is an alert.
   */
  private void makeCacheKey()
  {
    String	timeStamp	= NumberFormatter.getLong( stamp , 10 , true , 10 , '0' );

    if ( ( this.routingKey != null ) && ( !this.routingKey.isEmpty() ) )
    {
      if ( this.routingKey.startsWith( "ccsi.event" ) )
      {
        switch( this.routingKey )
        {
          case "ccsi.event.reading" :
            this.cacheKey	= "reading-" + timeStamp;
            break;
            
          case "ccsi.event.status" :
            this.cacheKey	= "status-" + timeStamp;
            break;
            
          case "ccsi.event.bit" :
            this.cacheKey	= "bit-" + timeStamp;
            break;
            
          case "ccsi.event.consumable" :
            this.cacheKey	= "consume-" + timeStamp;
            break;
            
          default:
            myLogger.error( "Invalid event routing key [" + this.routingKey + "] while making cache key." );
            break;
        }
      }
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the routingKey
   */
  public String getRoutingKey()
  {
    return routingKey;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param routingKey the routingKey to set
   */
  public void setRoutingKey( String routingKey )
  {
    this.routingKey	= routingKey;

    if ( this.msgBody != null )
    {
      makeCacheKey();
    }
  }

  //~--- get methods ----------------------------------------------------------

  public void setXMLCmd(String cmd) {
        CommandMsg cmdMsg = new CommandMsg();
        cmdMsg.setCmd(cmd);
            
        MsgContent msgBody = new MsgContent();
        msgBody.setCmd(cmdMsg);
        
        this.setMsgBody(msgBody);
  }
  
 
  public String getTheCcsiCmd() {
  
      if (this.isGoodCmd()) {
          return this.getMsgBody().getCmd().getCmd();
      } else {
          return "";
      }
  }
  
  public boolean isGoodCmd() {
      
      MsgContent msgBody = this.getMsgBody();
      
      if ((msgBody != null) && (msgBody.getCmd() != null) && (msgBody.getCmd().getCmd() != null)) {
          return true;
      } else {
          return false;
      }
  }
  
  
  /**
   * @return the msgBody
   */
  public MsgContent getMsgBody()
  {
    return msgBody;
  }

  /**
   * @return the RAW Ascii Message
   */
  public String getRawMsg()
  {
    return rawMsg;
  }

  /**
   * @return the RAW Binary Msg
   */
  public byte [] getRawMsgBinary()
  {
    byte [] retVal = null;
    if (rawMsgBinary != null)
    {
        retVal = rawMsgBinary.clone();
    }
    return retVal;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param msgBody the msgBody to set
   */
  public void setMsgBody( MsgContent msgBody )
  {
    this.msgBody	= msgBody;

    if ( this.routingKey != null )
    {
      makeCacheKey();
    }
  }

  /**
   * @param rawMsg -- the ascii rawMsg to set
   */
  public void setRawMsg( String rawMsg )
  {
    this.rawMsg	= rawMsg;
  }
  
  /**
   * @param rawMsgBinary -- the binary rawMsg to set
   */
  public void setRawMsgBinary( byte [] rawMsgBinary )
  {
    if (rawMsgBinary != null)
    {
        this.rawMsgBinary = rawMsgBinary.clone ();
    } else
    {
        this.rawMsgBinary = null;
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the cacheKey
   */
  public String getCacheKey()
  {
    return cacheKey;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param cacheKey the cacheKey to set
   */
  public void setCacheKey( String cacheKey )
  {
    this.cacheKey	= cacheKey;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the stamp
   */
  public long getStamp()
  {
    return stamp;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param stamp the stamp to set
   */
  public void setStamp( long stamp )
  {
    this.stamp	= stamp;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Compares timestamps between this and another object
   *
   *
   * @param o the object
   *
   * @return 0 = same, -1 = this is older, 1 = this is newer
   */
  @Override
  public int compareTo( Object o )
  {
    IPCWrapper	obj	= (IPCWrapper) o;

    if ( this.stamp > obj.getStamp() )
    {
      return ( 1 );
    }
    else if ( this.stamp < obj.getStamp() )
    {
      return ( -1 );
    }
    else
    {
      return ( 0 );
    }
  }

  /**
   * De-serialize an object
   *
   * @param in the input object stream
   *
   * @throws ClassNotFoundException
   * @throws IOException
   */
  private void readObject( ObjectInputStream in ) throws ClassNotFoundException , IOException
  {
    JAXBContext		jaxbContext;
    Unmarshaller	jaxbUnmarshaller;
    SchemaFactory	sFact;
    Schema			mySchema;
//    ObjectFactory	oFact							= new ObjectFactory();

    this.cacheKey		= (String) in.readObject();
    this.rawMsg			= (String) in.readObject();
    this.rawMsgBinary			= (byte []) in.readObject();
    this.routingKey	= (String) in.readObject();
    this.stamp			= in.readLong();
    this.alertId    = in.readLong();
    myLogger.info( "Regenerating the message content from: " + this.rawMsg );
    this.schema = new File( IPCWrapper.SCHEMA_FILE );
    sFact	= SchemaFactory.newInstance( XMLConstants.W3C_XML_SCHEMA_NS_URI );
    StringReader	readerXML	= new StringReader( this.getRawMsg() );

    try
    {
      mySchema				  = sFact.newSchema( this.schema );
      jaxbContext				= JAXBContext.newInstance(com.domenix.common.spark.data.ObjectFactory.class );
      jaxbUnmarshaller	= jaxbContext.createUnmarshaller();
      jaxbUnmarshaller.setSchema( mySchema );

      MsgContent	msg	=
        (MsgContent) ( (javax.xml.bind.JAXBElement) jaxbUnmarshaller.unmarshal( readerXML ) ).getValue();

      this.setMsgBody( msg );
    }
    catch ( JAXBException | SAXException ex )
    {
      myLogger.error( "Exception deserializing the IPCWrapper" , ex );
    }
  }

  /**
   * De-serialize without an object.
   *
   * @throws ObjectStreamException
   */
  private void readObjectNoData() throws ObjectStreamException
  {
    this.cacheKey		= "";
    this.rawMsg			= null;
    this.rawMsgBinary = null;
    this.routingKey	= "";
    this.stamp			= -1L;
    this.alertId    = -1L;
    this.schema     = new File( IPCWrapper.SCHEMA_FILE );
  }

  /**
   * Serialize an object
   *
   * @param out the output stream
   *
   * @throws IOException
   */
  private void writeObject( ObjectOutputStream out ) throws IOException
  {
    out.writeObject( this.cacheKey );
    out.writeObject( this.rawMsg );
    out.writeObject( this.rawMsgBinary );
    out.writeObject( this.routingKey );
    out.writeLong( this.stamp );
    out.writeLong( this.alertId );
    out.flush();
  }

  /**
   * @return the alertId
   */
  public long getAlertId()
  {
    return alertId;
  }

  /**
   * @param alertId the alertId to set
   */
  public void setAlertId(long alertId)
  {
    this.alertId = alertId;
  }
}
