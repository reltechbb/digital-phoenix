/*
 * IPCQueue.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the logic for queueing operations of entries of type <T> which is
 * specified when the class instance is instantiated.
 *
 * Version: V1.0  15/07/24
 */


package com.domenix.common.spark;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

//~--- classes ----------------------------------------------------------------

/**
 * This class implements a queue of entries to be received and processed
 * for IPC.
 *
 * @author Kevin Miller
 *
 * @param <T>
 */
public class IPCQueue<T>
{
  /** The list of listeners */
  private ArrayList<IPCQueueListenerInterface>	listeners	= null;

  /** The entries in the queue. */
  private final LinkedBlockingDeque<T>	theQueue	= new LinkedBlockingDeque<>( 64 );
  
  /** Error/Monitoring/Debug logger */
  private final Logger myLogger;
  
  private final String queueName;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance of the class.
   * 
   * @param name the name of the queue
   */
  public IPCQueue( String name )
  {
    super();
    this.queueName = name;
    this.myLogger = Logger.getLogger( IPCQueue.class );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This method flushes the contents of the queue by deleting all
   * entries.
   */
  public synchronized void flushQueue()
  {
    theQueue.clear();
//    deliverEvents( IPCQueueEvent.IPCQueueEventType.IPC_CLEAR , null );
  }
  
  /**
   * This method removes the first entry in the queue
   */
  public synchronized void removeFirst()
  {
    this.theQueue.removeFirst();
  }

  /**
   * This method inserts an entry at the front of the queue.
   *
   * @param entry Object to be inserted.
   */
  public void insertFirst( T entry )
  {
    synchronized ( theQueue )
    {
      theQueue.offerFirst( entry );
    }
    deliverEvents( IPCQueueEvent.IPCQueueEventType.IPC_NEW , entry );
  }

  /**
   * This method adds an entry to the end of the queue.
   *
   * @param entry Object entry to be inserted.
   */
  public void insertLast( T entry )
  {
    synchronized ( theQueue )
    {
      theQueue.add( entry );
    }

    deliverEvents( IPCQueueEvent.IPCQueueEventType.IPC_NEW , entry );
  }

  /**
   * This method reads the first entry in the queue or waits
   * for a caller specified length of time until an entry is
   * placed in the queue.  If the timeout expires with no queue
   * entries a null result will be returned.  The queue read
   * removes the entry from the queue.
   *
   * @param waitTime long containing the number of seconds to wait.
   *
   * @return Object entry from the queue or null if a timeout or error occurs.
   */
  public T readFirst( long waitTime )
  {
    T				cmd;
    boolean	doMore	= true;

    while ( doMore )
    {
      try
      {
        if ( waitTime == 0L )
        {
          cmd = theQueue.poll();
        }
        else
        {
          cmd	= theQueue.pollFirst( waitTime , TimeUnit.SECONDS );
        }
        return ( cmd );
      }
      catch ( InterruptedException ie )
      {
        // Test if we're being cancelled, if not, continue waiting.
        doMore	= true;
      }
      catch ( Exception ex )
      {
        doMore	= false;
      }
    }

    return ( null );
  }

  /**
   * This method returns the current number of entries in the
   * queue to the caller.
   *
   * @return int containing the number of entries in the queue.
   */
  public int size()
  {
    return ( theQueue.size() );
  }

  /**
   * Add a listener to this queue
   *
   * @param listener IPCQueueListenerInterface to be added
   */
  public void addIPCQueueEventListener( IPCQueueListenerInterface listener )
  {
    if ( listeners == null )
    {
      listeners	= new ArrayList<>();
    }

    boolean	found	= false;

    for ( IPCQueueListenerInterface x : listeners )
    {
      if ( x == listener )
      {
        found	= true;

        break;
      }
    }

    if ( !found )
    {
      listeners.add( listener );
  }
  }

  /**
   * Remove a listener from this queue
   *
   * @param listener IPCQueueListenerInterface to be removed
   */
  public void removeIPCQueueEventListener( IPCQueueListenerInterface listener )
  {
    if ( listeners != null )
    {
      for ( IPCQueueListenerInterface x : listeners )
      {
        if ( x == listener )
        {
          listeners.remove( listener );
          break;
        }
      }
    }
  }

  /**
   * Deliver the specified event type to the queue listeners
   *
   * @param event IPCQueueEvent to be delivered
   */
  private void deliverEvents( IPCQueueEvent.IPCQueueEventType event , Object payload )
  {
    if ( ( listeners != null ) && ( listeners.size() > 0 ) )
    {
      for ( IPCQueueListenerInterface x : listeners )
      {
        x.queueEventFired( new IPCQueueEvent( this , event , payload ) );
      }
    }
  }

  /**
   * @return the queueName
   */
  public String getQueueName()
  {
    return queueName;
  }
}
