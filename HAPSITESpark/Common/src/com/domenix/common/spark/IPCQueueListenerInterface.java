/*
 * IPCQueueListenerInterface.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the interface spacification for listeners on IPC Queues.
 *
 * Version: V1.0  15/07/24
 */


package com.domenix.common.spark;

//~--- JDK imports ------------------------------------------------------------

import java.util.EventListener;

//~--- interfaces -------------------------------------------------------------

/**
 * Interface defining listener actions on an IPCQueue
 *
 * @author kmiller
 */
public interface IPCQueueListenerInterface extends EventListener
{
  /**
   * Event action handler for IPCQueue events
   *
   * @param evt the IPCQueueEvent that occurred
   */
  public void queueEventFired( IPCQueueEvent evt );
}
