/*
 * IPCQueueEvent.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of an event used to notify listeners of IPC Queue activity.
 *
 * Version: V1.0  15/07/24
 */


package com.domenix.common.spark;

//~--- JDK imports ------------------------------------------------------------

import java.util.EventObject;


/**
 * This class defines an event used to notify listeners that a new entry has been placed in the
 * queue or that the queue has been cleared.
 *
 * @author kmiller
 */
public class IPCQueueEvent extends EventObject
{
  /** Serialization UUID */
  private static final long	serialVersionUID	= 5519710932648163546L;

  //~--- fields ---------------------------------------------------------------

  /** The event type */
  private IPCQueueEventType	eventType	= IPCQueueEventType.IPC_NEW;
  
  /** The event payload */
  private Object payload;

  /**
   * @return the payload
   */
  public Object getPayload()
  {
    return payload;
  }

  /**
   * @param payload the payload to set
   */
  public void setPayload(Object payload)
  {
    this.payload = payload;
  }
  
  //~--- constant enums -------------------------------------------------------

  /**
   * Enumeration for IPCQueue event types.
   */
  public enum IPCQueueEventType
  {
    /** No event type set */
    IPC_NONE ,

    /** New queue entry inserted */
    IPC_NEW ,

    /** Queue was cleared */
    IPC_CLEAR ,
    
    /** This queue is monitored */
    IPC_MONITOR ,
    
    /** This queue is no longer monitored */
    IPC_NOMONITOR
  }

  //~--- constructors ---------------------------------------------------------
  
  /**
   * Default constructor
   */
  public IPCQueueEvent()
  {
    super( null );
  }

  /**
   * Constructs an instance of the event with the provided source
   *
   * @param source the source object for the event which will be the queue initiating the event.
   */
  public IPCQueueEvent( Object source )
  {
    super( source );
  }

  /**
   * Constructs an instance of the event with the provided source and type
   *
   * @param source the source object for the event which will be the queue initiating the event.
   * @param theType the event type
   */
  public IPCQueueEvent( Object source , IPCQueueEventType theType )
  {
    super(source);
    this.eventType	= theType;
  }
  
  /**
   * Constructs an instance of the event with the provided source and type
   *
   * @param source the source object for the event which will be the queue initiating the event.
   * @param theType the event type
   * @param payload
   */
  public IPCQueueEvent( Object source , IPCQueueEventType theType , Object payload )
  {
    super(source);
    this.eventType	= theType;
    this.payload    = payload;
  }
  
  //~--- set methods ----------------------------------------------------------

  /**
   * @param eventType the eventType to set
   */
  public void setEventType( IPCQueueEventType eventType )
  {
    this.eventType	= eventType;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the eventType
   */
  public IPCQueueEventType getEventType()
  {
    return eventType;
  }

  /**
   * @return the eventSource
   */
  public Object getEventSource()
  {
//    return( this.source );
    return( super.getSource() );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Returns the string describing this event.
   *
   * @return String describing the event
   */
  @Override
  public String toString()
  {
    StringBuilder	msg	= new StringBuilder( "IPCQueueEvent " );

    if ( eventType != null )
    {
      msg.append( eventType.name() );
    }
    else
    {
      msg.append( "UNSPECIFIED" );
    }

    return ( msg.toString() );
  }
}
