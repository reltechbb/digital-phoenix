/*
 * IPCQueueEventWrapperEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This defines the types of wrappers (data types) in an IPCQueue
 *
 * Version: V1.0  15/09/29
 */


package com.domenix.common.spark;

/**
 * This defines the types of wrappers (data types) in an IPCQueue
 * 
 * @author kmiller
 */
public enum IPCQueueEventWrapperEnum
{
  IPC_WRAPPER , CTL_WRAPPER , LINK_EVENT , CONN_EVENT , XFER_EVENT;
}
