/*
 * CommandHandlingTypeEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of the actions that can be taken by the sensor
 * protocol handler to operate on a command from the host.
 *
 * Version: V1.0  15/09/10
 */


package com.domenix.common.ccsi;

/**
 * This enumerates the actions for handling a received command.
 * 
 * @author kmiller
 */
public enum CommandHandlingTypeEnum
{ 
  /** Handle the command within Protocol Handler */
  internal , 
  /** Pass the command to the sensor to handle */
  passtosensor , 
  /** This command is not supported */
  unsupported;
}
