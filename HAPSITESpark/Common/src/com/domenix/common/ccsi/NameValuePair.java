/*
 * NameValuePair.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This class maintains a pair of strings that act as a named item and
 * it's assigned value.
 *
 */
package com.domenix.common.ccsi;

/**
 * This class maintains a pair of strings that act as a named item and
 * it's assigned value.
 *
 */
public class NameValuePair
{
  /** The name */
  private String	theName	= null;

  /** The value */
  private String	theValue	= null;

  //~--- constructors ---------------------------------------------------------

  /**
   * Default constructor.
   */
  public NameValuePair()
  {
  }

  /**
   * Construct the pair.
   *
   * @param n String containing the name
   * @param v String containing the value
   */
  public NameValuePair( String n , String v )
  {
    theName		= n;
    theValue	= apostropheHandler( v );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Get the pair name value
   *
   * @return String containing the name
   */
  public String getTheName()
  {
    return theName;
  }

  /**
   * Get the pair value.
   *
   * @return String containing the value
   */
  public String getTheValue()
  {
    return theValue;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * Set the pair name to the provided value.
   *
   * @param theName String containing the name
   */
  public void setTheName( String theName )
  {
    this.theName	= theName;
  }

  /**
   * Set the value portion of the pair to the provided string.
   *
   * @param theValue String containing the value
   */
  public void setTheValue( String theValue )
  {
    this.theValue	= apostropheHandler( theValue );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Returns the current object state.
   *
   * @return String containing the state
   */
  @Override
  public String toString()
  {
    StringBuilder	temp	= new StringBuilder( "NV Pair: " );

    temp.append( theName );
    temp.append( "=>" );
    temp.append( theValue );
    temp.append( '\n' );

    return ( temp.toString() );
  }

  /**
   * This method replaces all apostrophe characters within the string with
   * a correct Java form of the apostrophe.  This is applied to the value
   * portion only.
   *
   * @param inp String containing the NV pair value being set.
   *
   * @return String with all apostrophe characters replaced
   */
  private String apostropheHandler( String inp )
  {
    String	retValue;

    if ( inp.indexOf( '\'' ) >= 0 )
    {
      retValue	= inp.replaceAll( "'" , "''" );
    }
    else
    {
      retValue	= inp;
    }

    return ( retValue );
  }
}