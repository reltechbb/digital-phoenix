/*
 * ConnSensorNetEnum
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Enums of the types of Sensor connections
 *
 */
package com.domenix.common.ccsi;

/**
 * Enums of the types of Sensor connections
 * @author jmerritt
 */
public enum ConnSensorNetEnum { TCP, UDP, USB, SERIAL }
