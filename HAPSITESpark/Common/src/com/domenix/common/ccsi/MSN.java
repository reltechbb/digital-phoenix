/*
 * MSN
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Singleton class for assigning incoming Message Sequence Numbers (MSNs)
 */
package com.domenix.common.ccsi;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Singleton class for assigning incoming MMessage Sequence Numbers (MSNs)
 * @author jmerritt
 */
public class MSN {
    private static final MSN msnInstance = new MSN ();
    private final AtomicInteger msnValue = new AtomicInteger (0);
    
    private MSN ()
    {
    }
    
    /**
     * returns an instance of MSN
     * @return 
     */
    public static MSN getMSN ()
    {
        return msnInstance;
    }
    
    /**
     * Gets the next MSN
     * @return 
     */
    public Integer getNextMSN ()
    {
        return msnValue.incrementAndGet();
    }
    
    /**
     * Gets the current MSN
     * @return 
     */
    public Integer getCurrentMSN ()
    {
        return msnValue.get();
    }
}
