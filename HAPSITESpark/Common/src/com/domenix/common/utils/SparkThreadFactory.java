/*
 * SparkThreadFactory.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * A thread factory for the purpose of setting thread names, etc.
 *
 */
package com.domenix.common.utils;

import java.util.concurrent.ThreadFactory;

/**
 * A thread factory for the purpose of setting thread names, etc.  
 * Gets passed in when creating a ThreadPool 
 * @author jmerritt
 */
public class SparkThreadFactory implements ThreadFactory
{
    /** the name of the thread 
     * needs to be set before the thread is executed.
     */
    private String threadName = null;
    
    /**
     * Constructor for unnamed thread
     */
    public SparkThreadFactory ()
    {
        
    }

    /**
     * Constructor for named thread
     * @param threadName 
     */
    public SparkThreadFactory (String threadName)
    {
        this.threadName = threadName;
    }

    /**
     * Sets the thread name
     * @param threadName 
     */
    public void setThreadName (String threadName)
    {
        this.threadName = threadName;
    }
    
    @Override
    public Thread newThread(Runnable r)
    {
        return new Thread(r, threadName);
    }
}
