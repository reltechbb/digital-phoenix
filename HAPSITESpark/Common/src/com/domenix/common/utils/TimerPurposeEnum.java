/*
 * TimerPurposeEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file defines an enumeration describing the purpose of a timer.
 *
 * Version: V1.0  15/10/16
 */


package com.domenix.common.utils;

/**
 * Defines an enumeration describing the purpose of a timer.
 * 
 * @author kmiller
 */
public enum TimerPurposeEnum
{
  /** Unspecified purpose - the default */
  UNSPECIFIED("Unspecified") , 
  /** ACK/NAK timer */
  ACK_TIMER("Acknowledgement Wait Timer ") , 
  /** Heartbeat period timer */
  HEARTBEAT_TIMER( "Heartbeat" ) ,
  /** Periodic report timer */
  PERIODIC_TIMER("Periodic Report Timer") ,
  /** Shutdown timer */
  SHUTDOWN_TIMER("Shutdown Delay Timer") , 
  /** Connection retry timer */
  CONNRETRY_TIMER("Connect Retry Timer") ,
  /** Connection failure timer */
  CONNFAILURE_TIMER("Connection Failure Timer") , 
  /** Security event window timer */
  SECURITYEVENT_TIMER("Security Event Timer") ,
  /** Reboot delay timer */
  REBOOTDELAY_TIMER("Reboot Delay Timer") , 
  /** NSDS Timer - general purpose */
  NSDS_TIMER("NSDS Timer") , 
  /** Protocol Handler timer - general purpose */
  PROTOCOLHANDLER_TIMER("PH Timer") ,
  /** Sensor adapter timer - general purpose */
  ADAPTER_TIMER("Adapter Timer");

  /** Human readable name for logging, etc. */
  private final String	humanName;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an enumeration value
   *
   * @param hn String containing the human readable name.
   */
  private TimerPurposeEnum( String hn )
  {
    this.humanName	= hn;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the human readable name for this enumeration value.
   *
   * @return String containing the human readable name.
   */
  public String getHumanName()
  {
    return ( this.humanName );
  }
}
