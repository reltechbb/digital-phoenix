/*
 * TimerChangeEvent.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of a timer event signalling to a listener that an event
 * occurred on a timer of interest.
 *
 * Version: V1.0  15/07/24
 */


package com.domenix.common.utils;

/**
 * This is a test timer event for timeout events, etc.
 *
 * @author kmiller
 */
public class TimerChangeEvent extends java.util.EventObject
{
  /** All events */
  public static final int	TEST_TIMER_ALL	= 0;

  /** Timer cancel event */
  public static final int	TEST_TIMER_CANCELLED	= 2;

  /** Timer expired event */
  public static final int	TEST_TIMER_EXPIRED	= 1;

  /** Timer reset event */
  public static final int	TEST_TIMER_RESET	= 3;

  /** Serialization UUID */
  private static final long	serialVersionUID	= 3256245618164803683L;

  //~--- fields ---------------------------------------------------------------

  /** The timer object payload. */
  private Object	payload	= null;
  
  /** The type of event. */
  private int	timerEvent	= TEST_TIMER_EXPIRED;

  /** The ID of the expired timer. */
  private long	timerId	= 0;
  
  /** The purpose of the timer change event */
  private TimerPurposeEnum timerPurpose = TimerPurposeEnum.UNSPECIFIED;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an event instance
   *
   * @param src Object that is the source
   * @param id int containing the timer identification number.
   * @param pld Object that is the timer payload
   * @param type int containing the event type
   * @param purp purpose of the timer
   */
  public TimerChangeEvent( Object src , long id , Object pld , int type , TimerPurposeEnum purp )
  {
    super( src );
    timerId			= id;
    payload			= pld;
    timerEvent	= type;
    timerPurpose = purp;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the timer ID.
   *
   * @return long timer id
   */
  public long getTimerId()
  {
    return ( timerId );
  }

  /**
   * Returns the payload
   *
   * @return Object the event payload
   */
  public Object getPayload()
  {
    return ( payload );
  }

  /**
   * Returns the timer even type
   *
   * @return int the timer event type
   */
  public int getTimerEvent()
  {
    return ( timerEvent );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Returns the current object state
   *
   * @return String containing the object state
   */
  @Override
  public String toString()
  {
    StringBuilder	msg	= new StringBuilder( "TimerExpiredEvent: id=" );

    msg.append( timerId );
    msg.append( " type=" );
    msg.append( timerEvent );

    if ( payload != null )
    {
      msg.append( "\n  Payload->" );
      msg.append( payload );
    }
    else
    {
      msg.append( "\n  No Payload." );
    }

    return ( msg.toString() );
  }

  /**
   * @return the timerPurpose
   */
  public TimerPurposeEnum getTimerPurpose()
  {
    return timerPurpose;
  }

  /**
   * @param timerPurpose the timerPurpose to set
   */
  public void setTimerPurpose(TimerPurposeEnum timerPurpose)
  {
    this.timerPurpose = timerPurpose;
  }
}
