/*
 * UtilTimerListener.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of a listener interface for timer events.
 *
 * Version: V1.0  15/07/24
 */


package com.domenix.common.utils;

/**
 * This interface defines a listener for TestTimer events.
 *
 * @author kmiller
 */
public interface UtilTimerListener
{
  /**
   * This method is called to deliver a timer expiration, cancellation,
   * or restart event.
   *
   * @param e TimerChangeEvent
   */
  public void timerExpired( TimerChangeEvent e );
}
