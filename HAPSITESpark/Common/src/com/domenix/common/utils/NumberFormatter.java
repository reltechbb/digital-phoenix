/*
 * NumberFormatter.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains a set of utility functions for formatting numbers for output, dates and times,
 * and for formatting strings.
 *
 * Version: V1.0  15/07/24
 */


package com.domenix.common.utils;

//~--- JDK imports ------------------------------------------------------------

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;

//~--- classes ----------------------------------------------------------------

/**
 * This class provides functions for formatting and parsing numbers, dates, and strings.
 *
 * @author kmiller
 */
public class NumberFormatter
{
  /** Binary number radix. */
  private static final int	RADIX_BINARY	= 2;

  /** Decimal number radix. */
  private static final int	RADIX_DECIMAL	= 10;

  /** Hexadecimal number radix. */
  private static final int	RADIX_HEX	= 16;

  /** Octal number radix. */
  private static final int	RADIX_OCTAL	= 8;

  /** The default format string for date-time output */
  private static final String	DEFAULT_FORMAT	= "yyyyMMddHHmmss";

  /**
   * Date/Time output format.
   */
  private static final SimpleDateFormat	DATE_FORMAT	= new SimpleDateFormat( DEFAULT_FORMAT );

  /**
   * The UTC zone.
   */
  private static final SimpleTimeZone	MY_ZONE	= new SimpleTimeZone( 0 , "UTC" );

  //~--- constructors ---------------------------------------------------------

  /**
   * Default constructor.
   */
  public NumberFormatter()
  {
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Format a date time string using the caller provided formatting string.  If the format
   * is null the default format is used.  If the dtg is null the current system time is used.
   * All values are generated as UTC based.
   *
   * @param dtg Date to be formatted
   * @param fmt String containing the format
   *
   * @return String containing the formatted date/time
   */
  public static String getDate( Date dtg , String fmt )
  {
    String workFmt = NumberFormatter.DEFAULT_FORMAT;
    Date   workDate = new Date();
    
    if ( dtg != null )
    {
      workDate = dtg;
    }

    if ( fmt != null )
    {
      workFmt = fmt;
    }

    SimpleDateFormat	specFormat	= new SimpleDateFormat( workFmt );

    specFormat.setTimeZone( MY_ZONE );

    return ( specFormat.format( workDate ).toUpperCase() );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Parse a date string using the default format.
   *
   * @param timeIn String containing the time value to be parsed
   *
   * @return Date containing the value parsed or null if error
   */
  public static Date parseDateTime( String timeIn )
  {
    if ( ( timeIn != null ) && ( timeIn.length() > 0 ) )
    {
      try
      {
        SimpleDateFormat DateFormat	= new SimpleDateFormat( DEFAULT_FORMAT );
        DateFormat.setTimeZone( MY_ZONE );

        Date	temp	= DateFormat.parse( timeIn );

        return ( temp );
      }
      catch ( ParseException exc )
      {
        exc.printStackTrace( System.err );
      }
    }

    return ( null );
  }

  /**
   * Parse a date string using the caller supplied time format.
   *
   * @param timeIn String containing the time value to be parsed
   * @param fmt String containing the time-date format for the string
   *
   * @return Date containing the value parsed or null if error
   */
  public static Date parseDateTime( String timeIn , String fmt )
  {
    if ( ( timeIn != null ) && ( timeIn.length() > 0 ) )
    {
      try
      {
        SimpleDateFormat	specFormat	= new SimpleDateFormat( fmt );

        specFormat.setTimeZone( MY_ZONE );

        Date temp = specFormat.parse( timeIn );

        return ( temp );
      }
      catch ( ParseException exc )
      {
        exc.printStackTrace( System.err );
      }
    }

    return ( null );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   *  Get an int string in a caller specified radix.  This returns a string
   *  containing the integer value formatted in base 2, 8, 10, or 16 with
   *  the caller provided justification and padding.  If the width is specified
   *  as 0 no justification will be performed.
   *
   *  @param intValue the int value to be converted
   *  @param radix int containing the number base to use (2, 8, 10, or 16)
   *  @param right boolean flag for left (false) or right (true) justification
   *  @param width int containing the desire output string width or 0 for default
   *  @param pad char containing the padding to use when formatting or \u0000 for the default
   *
   *  @return String containing the formatted value or null if format failed
   */
  public static String getInt( int intValue , int radix , boolean right , int width , char pad )
  {
    char		myPad	= ( pad == '\u0000' )
                    ? ' '
                    : pad;
    String	temp;
    boolean	negFlag	= false;

    if ( testRadix( radix ) )
    {
      if ( intValue < 0 )
      {
        negFlag		= true;
        intValue	= -intValue;
      }

      temp	= Integer.toString( intValue , radix );

      if ( ( width > 0 ) && ( temp.length() <= width ) )
      {
        if ( right )
        {
          temp	= getStringRight( temp , width , pad );

          if ( negFlag )
          {
            StringBuilder	t2	= new StringBuilder();

            t2.append( '-' );
            t2.append( temp.substring( 1 ) );

            return ( t2.toString() );
          }

          return ( temp );
        }
        else
        {
          return ( getStringLeft( temp , width , pad ) );
        }
      }
      else
      {
        return ( temp.substring( 0 , width ) );
      }
    }
    else
    {
      return ( null );
    }
  }

  /**
   * Get an int string in a caller specified radix.  This returns a string
   * containing the integer value formatted in base 2, 8, 10, or 16 with
   * the caller provided justification and padding.  If the width is specified
   * as 0 no justification will be performed.
   *
   * @param longValue
   * @param radix int containing the number base to use (2, 8, 10, or 16)
   * @param right boolean flag for left (false) or right (true) justification
   * @param width int containing the desire output string width or 0 for default
   * @param pad char containing the padding to use when formatting or \u0000 for the default
   *
   * @return String containing the formatted value or null if format failed
   */
  public static String getLong( long longValue , int radix , boolean right , int width , char pad )
  {
    String	temp;
    boolean	negFlag	= false;

    if ( testRadix( radix ) )
    {
      if ( longValue < 0 )
      {
        negFlag		= true;
        longValue	= -longValue;
      }

      temp	= Long.toString( longValue , radix );

      if ( ( width > 0 ) && ( temp.length() <= width ) )
      {
        if ( right )
        {
          temp	= getStringRight( temp , width , pad );

          if ( negFlag )
          {
            StringBuilder	t2	= new StringBuilder();

            t2.append( '-' );
            t2.append( temp.substring( 1 ) );

            return ( t2.toString() );
          }

          return ( temp );
        }
        else
        {
          return ( getStringLeft( temp , width , pad ) );
        }
      }
      else
      {
        return ( temp.substring( 0 , width ) );
      }
    }
    else
    {
      return ( null );
    }
  }

  /**
   * Get a double value string converted with caller specified formatting.
   * @see java.text.NumberFormat
   *
   * @param dblValue value to be converted
   * @param group boolean indication if grouping is to be used.
   * @param minDigits int minimum number of integer digits to be used
   * @param maxDigits int maximum number of integer digits to be used
   * @param minFract int minimum number of fractional digits to be used
   * @param maxFract int maximum number of fractional digits to be used
   *
   * @return String containing the formatted number
   */
  public static String getDouble( double dblValue , boolean group , int minDigits , int maxDigits , int minFract ,
                                  int maxFract )
  {
    String				outStr;
    NumberFormat	tempNF	= (NumberFormat) NumberFormat.getInstance().clone();

    tempNF.setGroupingUsed( group );
    tempNF.setMaximumIntegerDigits( maxDigits );
    tempNF.setMinimumIntegerDigits( minDigits );
    tempNF.setMaximumFractionDigits( maxFract );
    tempNF.setMinimumFractionDigits( minFract );
    outStr	= tempNF.format( dblValue );

    return ( outStr );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Test the radix value.
   *
   * @param radix int containing the number radix (2, 8, 10, 16 valid)
   *
   * @return boolean valid/not valid indication
   */
  private static boolean testRadix( int radix )
  {
    if ( ( radix == RADIX_BINARY ) || ( radix == RADIX_OCTAL ) || ( radix == RADIX_DECIMAL ) || ( radix == RADIX_HEX ) )
    {
      return ( true );
    }
    else
    {
      return ( false );
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Get a string right justified in a fixed width field.  If the string overflows
   * the field width it will be truncated on the right, if shorter than the field
   * width it will be right justified with the padding character in the left most
   * positions.
   *
   * @param strValue string to be output
   * @param width int width of the fixed field
   * @param pad char containing the padding to be used or \u0000 for the default
   *
   * @return String containing the justified field
   */
  public static String getStringRight( String strValue , int width , char pad )
  {
    char	myPad	= ( pad == '\u0000' )
                  ? ' '
                  : pad;

    if ( ( strValue != null ) && ( width > 0 ) )
    {
      int						strLen	= strValue.length();
      int						nPad		= width - strLen;
      StringBuilder	temp		= new StringBuilder();

      if ( nPad <= 0 )
      {
        temp.append( strValue.substring( 0 , width ) );
      }
      else
      {
        for ( int i = 0 ; i < nPad ; i++ )
        {
          temp.append( myPad );
        }

        temp.append( strValue );
      }

      return ( temp.toString() );
    }
    else
    {
      return ( null );
    }
  }

  /**
   * Get a string left justified in a fixed width field.
   *
   * @param strValue string to be output
   * @param width int width of the fixed field
   * @param pad char containing the padding to be used or \u0000 for the default
   *
   * @return String containing the justified field
   */
  public static String getStringLeft( String strValue , int width , char pad )
  {
    char	myPad	= ( pad == '\u0000' )
                  ? ' '
                  : pad;

    if ( ( strValue != null ) && ( width > 0 ) )
    {
      int						strLen	= strValue.length();
      int						nPad		= width - strLen;
      StringBuilder	temp		= new StringBuilder();

      if ( nPad <= 0 )
      {
        temp.append( strValue.substring( 0 , width ) );
      }
      else
      {
        temp.append( strValue );

        for ( int i = 0 ; i < nPad ; i++ )
        {
          temp.append( myPad );
        }
      }

      return ( temp.toString() );
    }
    else
    {
      return ( null );
    }
  }
}
