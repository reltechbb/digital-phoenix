/*
 * TimerQueueEntry.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definiton of a timer entry holding the state of a single utility timer.
 *
 * Version: V1.0  15/07/24
 */


package com.domenix.common.utils;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

//~--- classes ----------------------------------------------------------------

/**
 * This class holds a timer queue entry to inform a thread that a timer has expired.
 *
 * @author kmiller
 */
public class TimerQueueEntry
{
  /** The timer countdown value. */
  public long	timerCounter	= 0L;

  /** The assigned ID of the timer. */
  public long	timerId	= 0;

  /** The name of the timer. */
  public String	timerName	= null;

  /** A timer payload object provided by the creator to passed when the timer expires. */
  public Object	timerPayload	= null;

  /** The timer source value */
  public Object	timerSource	= null;

  /** The time value of the timer. */
  public long	timerTime	= 0L;

  /** The timer should repeat until canceled flag. */
  public boolean	timerRepeats	= false;

  /** The purpose of the timer */
  public TimerPurposeEnum timerPurpose = TimerPurposeEnum.UNSPECIFIED;

  /** Listeners for timer reset events. */
  public ArrayList<UtilTimerListener>	theResetListeners	= new ArrayList<>();

  /** Listeners for timer expired events. */
  public ArrayList<UtilTimerListener>	theExpiredListeners	= new ArrayList<>();

  /** Listeners for timer canceled events. */
  public ArrayList<UtilTimerListener>	theCancelListeners	= new ArrayList<>();

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance of a timer queue entry.
   *
   * @param source Object that is the source of the event.
   * @param id int containing the ID number assigned by the timer task.
   * @param time long containing the time delay value in seconds.
   * @param payload Object to be returned to thread that started the timer.
   * @param repeats boolean indicating if the timer repeats
   * @param name String containing the timer name
   * @param purpose the purpose of this timer
   */
  public TimerQueueEntry( Object source , long id , long time , Object payload , boolean repeats , String name , TimerPurposeEnum purpose )
  {
    timerId				= id;
    timerTime			= time * 1000L;
    timerPayload	= payload;
    timerSource		= source;
    timerCounter	= timerTime;
    timerRepeats	= repeats;
    timerPurpose  = ((purpose!=null)?purpose:TimerPurposeEnum.UNSPECIFIED);

    if ( ( name != null ) && ( name.length() > 0 ) )
    {
      timerName	= name;
    }
    else
    {
      timerName	= "Timer-" + Long.toString( id );
    }
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Return the current state of the object as a string.
   *
   * @return String containing the current state of the entity.
   */
  @Override
  public String toString()
  {
    StringBuilder	msg	= new StringBuilder( "TimerQueueEntry: name = " );

    msg.append( timerName );
    msg.append( " id=" );
    msg.append( timerId );
    msg.append( " src=" );
    msg.append( timerSource );
    msg.append( " timing " );
    msg.append( ( timerTime / 1000L ) );

    if ( timerRepeats )
    {
      msg.append( " seconds - repeating." );
    }
    else
    {
      msg.append( " seconds - not repeating." );
    }

    if ( timerPayload != null )
    {
      msg.append( "\n  Payload->" );
      msg.append( timerPayload );
    }
    else
    {
      msg.append( "\n  No Payload." );
    }

    msg.append( "\n  Listener " );

    if ( ( ( theExpiredListeners != null ) && ( theExpiredListeners.size() > 0 ) ) ||
         ( ( theCancelListeners != null ) && ( theResetListeners.size() > 0 ) ) ||
         ( ( theResetListeners != null ) && ( theResetListeners.size() > 0 ) ) )
    {
      msg.append( "YES" );
    }
    else
    {
      msg.append( "NO" );
    }

    return ( msg.toString() );
  }
}
