/*
 * SystemDateTime.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Utility for setting the System Date and Time
 *
 */
package com.domenix.common.utils;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

/**
 * Utility for setting the System Date and Time on a Linux based system
 * 
 * @author jmerritt
 */
public class SystemDateTime {

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger = Logger.getLogger(SystemDateTime.class);

    /**
     * Sets the system time on the OS On non-Unix systems, does nothing
     *
     * @param dateString - epoch
     * @throws IOException
     * @throws ParseException
     */
    public void setSystemDateTime(String dateString) throws IOException, ParseException 
    {
        String os = System.getProperty("os.name").toLowerCase();

        // check if this is a unix based system
        if (os.contains("nux")) 
        {
            Long longDate = Long.parseLong(dateString) * 1000;
            Date date = new Date (longDate);

            String [] cmdString = new String [] {"/bin/date", "-s", date.toString()};
            Process p = Runtime.getRuntime().exec(cmdString);
            int result = -1;
            boolean done = false;
            myLogger.info("Setting time to: epoch: " + dateString + " formatted: "
                    + date.toString());
            try 
            {
                done = p.waitFor(2, TimeUnit.SECONDS);
                result = p.exitValue();
            } catch (InterruptedException ex) 
            {
                myLogger.error("Interrupted exception setting DateTime to: " + dateString);
            }
            if (result == 0)
            {
                myLogger.info ("Date as set: " + System.currentTimeMillis() / 1000);
            } else
            {
                myLogger.error("Error setting DateTime to: " + dateString + " Exit value: " + 
                        result + " Done: " + done);
            }
        } else {
            // not unix, dont do it
            myLogger.info("OS Not Linux: " + os + " Simulating setting time to: " + dateString);
        }
    }

    /**
     * Checks the datetime against the system datetime and updates the system time if necessary
     * @param dateString
     * @throws java.io.IOException
     * @throws java.text.ParseException
     */
    public void checkDateTime(String dateString) throws IOException, ParseException {
        long currentTime = System.currentTimeMillis() / 1000;
        if ((dateString != null) && (dateString.trim().length() > 0)) {
            long dtg = Long.parseLong(dateString);

            long temp = currentTime - dtg;

            if (Math.abs(temp) >= 5L) {
                setSystemDateTime(dateString);
            }
        }
    }
    
    /**
     * Checks and possibly sets the system date time from a Long
     * @param datetime 
     */
    public void checkSystemDateTimeLong (long datetime)
    {
        try
        {
            checkDateTime (Long.toString(datetime));
        } catch (IOException | ParseException ex) {
            myLogger.error ("Exception setting date time: " + ex);
        } 
    }
}
