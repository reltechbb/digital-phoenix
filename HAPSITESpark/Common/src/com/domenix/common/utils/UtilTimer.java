/*
 * UtilTimer.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of a utility timer that can service multiple individual timers
 * using the Java TimerTask base class.
 *
 * Version: V1.0  15/07/24
 */


package com.domenix.common.utils;

//~--- non-JDK imports --------------------------------------------------------

import java.util.ArrayList;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.log4j.Logger;

//~--- classes ----------------------------------------------------------------

/**
 * This class is a timing service that triggers an event back to a client when a
 * time expires.
 *
 * @author kmiller
 */
public class UtilTimer extends TimerTask
{
  /** The last time we ran. */
  private long	lastRunTime	= 0;

  /** Synchronization field. */
  private final Object	syncVbl	= new Object();

  /** The ID number for the next timer. */
  private long	timerIdNumber	= 1;

  /** Did the list change */
  private boolean	listChanged	= false;

  /** The list of active timers. */
  private ConcurrentHashMap<Long , TimerQueueEntry>	timerList	= null;

  /** Debug/Error logger */
  private final Logger	myLogger;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs the utility timer.
   *
   * @param loggerName String containing the name of the logger to be used or null to use the root logger
   */
  public UtilTimer( String loggerName )
  {
    super();

    if ( ( loggerName == null ) || ( loggerName.trim().length() <= 0 ) )
    {
      myLogger	= Logger.getRootLogger();
    }
    else
    {
      myLogger	= Logger.getLogger( loggerName );
    }

    timerList	= new ConcurrentHashMap<>();
    myLogger.debug( "Constructed..." );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This method is run once per second to maintain the timers.
   */
  @Override
  public void run()
  {
    ArrayList<String>	tempList	= new ArrayList<>();
    long							timeNow		= System.currentTimeMillis();

    if ( timerList.size() > 0 )
    {
      synchronized ( syncVbl )
      {
        long		elapsed			= timeNow - lastRunTime;
        boolean	needCleanup	= listChanged;

        listChanged	= false;

        for ( TimerQueueEntry x : timerList.values() )
        {
          if ( x.timerCounter > 0 )
          {
            x.timerCounter	-= elapsed;
          }

          if ( ( x.timerCounter <= 0 ) && ( x.timerId == -1L ) )
          {
            x.timerCounter	= 0;
            x.timerRepeats	= false;
            needCleanup			= true;
          }
          else if ( ( x.timerCounter <= 0 ) && ( x.timerId != -1L ) )
          {
            myLogger.debug( "timer " + x.timerName + " triggered." );

            if ( ( x.theExpiredListeners != null ) && ( x.theExpiredListeners.size() > 0 ) )
            {
              deliverEvent( x , TimerChangeEvent.TEST_TIMER_EXPIRED );
            }

            if ( !x.timerRepeats )
            {
              if ( ( x.theCancelListeners != null ) && ( x.theCancelListeners.size() > 0 ) )
              {
                deliverEvent( x , TimerChangeEvent.TEST_TIMER_CANCELLED );
              }

              x.timerId				= -1L;
              x.timerCounter	= 0L;
              x.timerTime			= 0L;
              needCleanup			= true;
              myLogger.debug( "timer " + x.timerName + " being cancelled." );
            }
            else
            {
              x.timerCounter	= x.timerTime;

              if ( ( x.theResetListeners != null ) && ( x.theResetListeners.size() > 0 ) )
              {
                deliverEvent( x , TimerChangeEvent.TEST_TIMER_RESET );
              }

              myLogger.debug( "timer " + x.timerName + " being restarted." );
            }
          }
        }

        if ( needCleanup )
        {
          for ( Object key : timerList.keySet().toArray() )
          {
            TimerQueueEntry	x	= timerList.get( (Long) key );

            if ( x.timerId <= 0 )
            {
              if ( x.theCancelListeners != null )
              {
                x.theCancelListeners.clear();
              }

              if ( x.theExpiredListeners != null )
              {
                x.theExpiredListeners.clear();
              }

              if ( x.theResetListeners != null )
              {
                x.theResetListeners.clear();
              }

              timerList.remove( (Long) key );
            }
          }
        }
      }
    }
    else
    {
      tempList.add( "No active timers." );
    }

    lastRunTime	= timeNow;
  }

  /**
   * Cancel all current timers.
   */
  public void cancelAllTimers()
  {
    myLogger.debug( "Enter" );

    synchronized ( syncVbl )
    {
      if ( timerList.size() > 0 )
      {
        for ( TimerQueueEntry x : timerList.values() )
        {
          if ( ( x.theCancelListeners != null ) && ( x.theCancelListeners.size() > 0 ) )
          {
            deliverEvent( x , TimerChangeEvent.TEST_TIMER_CANCELLED );
          }

          myLogger.debug( "Cancelling timer " + x.timerName );
          x.timerId				= -1L;
          x.timerRepeats	= false;
          x.timerTime			= 0;
          x.timerCounter	= 0;
          if (x.theCancelListeners != null)
          {
            x.theCancelListeners.clear();
          }
          x.theExpiredListeners.clear();
          x.theResetListeners.clear();
        }
      }
    }

    myLogger.debug( "Exit" );
  }

  /**
   * Cancel all current timers except for those whose name is provided
   * in the argument list.  If the list is null or empty the behavior
   * will be the same as cancelAllTimers()
   *
   * @param which list of all timers to retain
   */
  public void cancelAllTimersExcluding( ArrayList<String> which )
  {
    myLogger.debug( "Enter" );

    synchronized ( syncVbl )
    {
      if ( timerList.size() > 0 )
      {
        for ( TimerQueueEntry x : timerList.values() )
        {
          if ( ( which != null ) && ( which.size() > 0 ) )
          {
            if ( which.contains( x.timerName ) )
            {
              myLogger.debug( "Skipping timer " + x.timerName );

              continue;
            }
          }

          if ( ( x.theCancelListeners != null ) && ( x.theCancelListeners.size() > 0 ) )
          {
            deliverEvent( x , TimerChangeEvent.TEST_TIMER_CANCELLED );
          }

          listChanged			= true;
          x.timerId				= -1L;
          x.timerRepeats	= false;
          x.timerTime			= 0;
          x.timerCounter	= 0;
          if (x.theCancelListeners != null)
          {
            x.theCancelListeners.clear();
          }

          x.theExpiredListeners.clear();
          x.theResetListeners.clear();
        }
      }
    }

    myLogger.debug( "Exit" );
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * Set and start a new test timer.
   *
   * @param secs long containing the number of seconds the timer is to count
   * @param repeats boolean indication of whether the timer is a one-shot (false) or repeating (true)
   * @param src Object to be used as the source for the timer
   * @param pld Object to be delivered to the caller when the timer expires.  This can be anything the caller
   * wants to have associated with the timer or can be null.
   * @param name String containing the name to be used for the timer when being logged/displayed
   * @param purpose the timer purpose
   *
   * @return long containing the ID number of timer that was established.
   */
  public long setNewTimer( long secs , boolean repeats , Object src , Object pld , String name , TimerPurposeEnum purpose )
  {
    myLogger.debug( "Enter" );

    if ( src == null )
    {
      throw new IllegalArgumentException( "Timer src is null." );
    }

    long	theId	= timerIdNumber++;

    if ( timerIdNumber == Long.MAX_VALUE - 1L )
    {
      timerIdNumber	= 1L;
      theId					= timerIdNumber;
    }

    TimerQueueEntry	qe	= new TimerQueueEntry( src , theId , secs , ( ( pld == null )
            ? name
            : pld ) , repeats , name , ((purpose!=null)?purpose:TimerPurposeEnum.UNSPECIFIED) );

    synchronized ( syncVbl )
    {
      timerList.put( theId , qe );
      listChanged	= true;
    }

    myLogger.debug( "Entered new timer:\n" + qe.toString() );
    myLogger.debug( "Exit" );

    return ( theId );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Cancel an existing timer.
   *
   * @param id long containing the ID of the timer to be canceled.
   */
  public void cancelTimer( long id )
  {
    myLogger.debug( "Enter" );

    if ( id >= 0L )
    {
      TimerQueueEntry	x	= timerList.get(id);

      if ( x != null )
      {
        synchronized ( syncVbl )
        {
          listChanged	= true;

          StringBuilder	msg	= new StringBuilder( "Cancelled timer " );

          if ( x.theCancelListeners.size() > 0 )
          {
            deliverEvent( x , TimerChangeEvent.TEST_TIMER_CANCELLED );
          }

          msg.append( x.timerName );
          msg.append( " with " );
          msg.append( x.timerCounter );
          msg.append( " ms remaining." );
          x.timerId				= -1L;
          x.timerRepeats	= false;
          x.timerTime			= 0;
          x.timerCounter	= 0;
          x.theCancelListeners.clear();
          x.theExpiredListeners.clear();
          x.theResetListeners.clear();
          myLogger.debug( msg.toString() );
        }
      }
    }

    myLogger.debug( "Exit" );
  }

  /**
   * Deliver the specified type of event to all listeners for the timer.
   *
   * @param ent TimerQueueEntry being processed
   * @param type int containing the type of event being delivered
   */
  private void deliverEvent( TimerQueueEntry ent , int type )
  {
      switch (type)
      {
          case TimerChangeEvent.TEST_TIMER_CANCELLED:
              if ( ent.theCancelListeners.size() > 0 )
              {
                  for ( UtilTimerListener theCancelListener : ent.theCancelListeners )
                  {
                      theCancelListener.timerExpired( new TimerChangeEvent( ent.timerSource , ent.timerId , ent.timerPayload ,
                              type , ent.timerPurpose ) );
                  }
              }       
              break;
          case TimerChangeEvent.TEST_TIMER_EXPIRED:
              if ( ent.theExpiredListeners.size() > 0 )
              {
                  for ( UtilTimerListener theExpiredListener : ent.theExpiredListeners )
                  {
                      theExpiredListener.timerExpired( new TimerChangeEvent( ent.timerSource , ent.timerId , ent.timerPayload ,
                              type , ent.timerPurpose) );
                  }
              }       
              break;
          case TimerChangeEvent.TEST_TIMER_RESET:
              if ( ent.theResetListeners.size() > 0 )
              {
                  for ( UtilTimerListener theResetListener : ent.theResetListeners )
                  {
                      theResetListener.timerExpired( new TimerChangeEvent( ent.timerSource , ent.timerId , ent.timerPayload ,
                              type , ent.timerPurpose ) );
                  }
              }       
              break;
          default:
              break;
      }
  }

  /**
   * Adds a listener to the specified timer queue entry using the ID to match
   * the entry.
   *
   * @param l UtilTimerListener to receive timeout events.
   * @param id long containing the ID number of the timer to attach
   * @param which in containing the specific event type desired or
   */
  public void addUtilTimerListener( UtilTimerListener l , long id , int which )
  {
    myLogger.debug( "Enter" );

    if ( l != null )
    {
      TimerQueueEntry	x	= timerList.get(id);

      if ( x != null )
      {
        synchronized ( syncVbl )
        {
          StringBuilder	msg	= new StringBuilder( "Listener set for timer " );

          msg.append( x.timerName );

          if ( ( which == TimerChangeEvent.TEST_TIMER_ALL ) || ( which == TimerChangeEvent.TEST_TIMER_CANCELLED ) )
          {
            if ( !x.theCancelListeners.contains( l ) )
            {
              x.theCancelListeners.add( l );
            }
          }

          if ( ( which == TimerChangeEvent.TEST_TIMER_ALL ) || ( which == TimerChangeEvent.TEST_TIMER_EXPIRED ) )
          {
            if ( !x.theExpiredListeners.contains( l ) )
            {
              x.theExpiredListeners.add( l );
            }
          }

          if ( ( which == TimerChangeEvent.TEST_TIMER_ALL ) || ( which == TimerChangeEvent.TEST_TIMER_RESET ) )
          {
            if ( !x.theResetListeners.contains( l ) )
            {
              x.theResetListeners.add( l );
            }
          }

          myLogger.debug( msg.toString() );
        }
      }
    }
  }

  /**
   * Removes a listener from the specified timer.
   *
   * @param id long containing the timer ID number whose listener will be removed.
   * @param l UtilTimerListener to be removed
   */
  public void removeUtilTimerListener( long id , UtilTimerListener l )
  {
    myLogger.debug( "Enter" );

    TimerQueueEntry	x	= timerList.get(id);

    if ( x != null )
    {
      synchronized ( syncVbl )
      {
        StringBuilder	msg	= new StringBuilder( "Listener removed for timer " );

        msg.append( x.timerName );
        x.theCancelListeners.remove( l );
        x.theExpiredListeners.remove( l );
        x.theResetListeners.remove( l );
        myLogger.debug( msg.toString() );
      }
    }

    myLogger.debug( "Exit" );
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * Resets the time value for the specified timer.
   *
   * @param id the ID of the timer to be modified
   * @param value long containing the number of seconds for the timer
   *
   * @return flag indicating success (true) or failure (false)
   */
  public boolean setNewTimePeriod( long id , long value )
  {
    boolean	retValue	= false;

    myLogger.debug( "Enter" );

    TimerQueueEntry	x	= timerList.get(id);

    if ( x != null )
    {
      synchronized ( syncVbl )
      {
        x.timerTime	= ( value * 1000L );
        retValue		= true;
      }
    }

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the timer queue entry for the timer whose name matches the input
   *
   * @param name String containing the timer name
   *
   * @return TimerQueueEntry or null if not found
   */
  public TimerQueueEntry getByName( String name )
  {
    TimerQueueEntry	retValue	= null;

    myLogger.debug( "Enter" );

    if ( ( name != null ) && ( name.trim().length() > 0 ) )
    {
      for ( TimerQueueEntry x : timerList.values() )
      {
        if ( x.timerName.equalsIgnoreCase( name.trim() ) )
        {
          retValue	= x;

          break;
        }
      }
    }

    return ( retValue );
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * Resets the time value for the specified timer.
   *
   * @param name the name of the timer to be modified
   * @param value long containing the number of seconds for the timer
   *
   * @return flag indicating success (true) or failure (false)
   */
  public boolean setNewTimePeriod( String name , long value )
  {
    boolean	retValue	= false;

    myLogger.debug( "Enter" );

    TimerQueueEntry	x	= this.getByName( name );

    if ( x != null )
    {
      synchronized ( syncVbl )
      {
        x.timerTime	= ( value * 1000L );
        retValue		= true;
      }
    }

    return ( retValue );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * This method resets the timer to its original value.
   *
   * @param id long containing the ID number of the timer to be reset
   *
   * @return long containing the number of ms remaining when reset
   */
  public long resetUtilTimer( long id )
  {
    myLogger.debug( "Enter" );

    long						retValue	= 0L;
    TimerQueueEntry	x					= timerList.get(id);

    if ( x != null )
    {
      synchronized ( syncVbl )
      {
        listChanged	= true;

        StringBuilder	msg	= new StringBuilder( "Reset timer " );

        if ( x.theResetListeners.size() > 0 )
        {
          deliverEvent( x , TimerChangeEvent.TEST_TIMER_RESET );
        }

        msg.append( x.timerName );
        msg.append( " to " );
        msg.append( ( x.timerTime / 1000 ) );
        msg.append( " seconds." );
        retValue				= x.timerCounter;
        x.timerCounter	= x.timerTime;
        myLogger.debug( msg.toString() );
      }
    }

    myLogger.debug( "Exit" );

    return ( retValue );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * This returns the base timer value in seconds for the ID
   * provided.
   *
   * @param id long ID number of the timer to fetch
   *
   * @return long continuing the base timer value in seconds
   */
  public long getTimerBaseValue( long id )
  {
    myLogger.debug( "Enter" );

    long						retValue	= 0L;
    TimerQueueEntry	x					= timerList.get(id);

    if ( x != null )
    {
      synchronized ( syncVbl )
      {
        retValue	= x.timerTime / 1000;
      }
    }

    myLogger.debug( "Exit" );

    return ( retValue );
  }

  /**
   * Returns the name of the specified timer or null if the timer ID is not found
   *
   * @param t the timer ID to find and obtain the name
   *
   * @return String containing the name or null if the timer ID was not found
   */
  public String getTimerName( long t )
  {
    TimerQueueEntry	x	= timerList.get(t);

    if ( x != null )
    {
      return ( x.timerName );
    }

    return ( null );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Returns the name of the specified timer or null if the timer ID is not found
   *
   * @param t the timer ID to find and obtain the name
   *
   * @return String containing the name or null if the timer ID was not found
   */
  public boolean doesTimerRepeat( long t )
  {
    TimerQueueEntry	x	= timerList.get(t);

    if ( x != null )
    {
      return ( x.timerRepeats );
    }

    return ( false );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the timerList
   */
  public ConcurrentHashMap<Long , TimerQueueEntry> getTimerList()
  {
    return timerList;
  }
}
