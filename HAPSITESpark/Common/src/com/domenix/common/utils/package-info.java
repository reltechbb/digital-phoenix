/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * This package contains the definition of the actions that can be taken by the sensor
 * protocol handler to operate on a command from the host.
 * Utilities used by the Sensor Protocol Adapter (SPA), Sensor Interface Adapter (SIA),
 * Host Interface Adapter (HIA), Host Protocol Adapter and the Protocol Handler (PH)
 *
 */
package com.domenix.common.utils;