/*
 * NetworkProtocolTypeEnum.java
 * Network Protocol Types
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of the interface that must be implemented by all sensor
 * adapter writers.
 *
 */
package com.domenix.common.enums;

/**
 * Network Protocol Types
 * @author jmerritt
 */
public enum NetworkProtocolTypeEnum
{
    Open,
    ISA,
    VICTORY,
    OGC
}
