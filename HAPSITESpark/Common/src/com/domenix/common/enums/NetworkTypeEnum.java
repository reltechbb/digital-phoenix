/*
 * NetworkTypeEnum.java
 * Network Types
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of the interface that must be implemented by all sensor
 * adapter writers.
 *
 */
package com.domenix.common.enums;

/**
 * Network Types
 * @author jmerritt
 */
public enum NetworkTypeEnum
{
    TCP,
    UDP,
    USB,
    Serial
}
