/*
 * NetworkAddressTypeEnum.java
 * Network Address Types
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file defines network addtress types
 *
 */
package com.domenix.common.enums;

/**
 * Network Address Types
 * @author jmerritt
 */
public enum NetworkAddressTypeEnum
{
    DHCP,
    FIXED,
    NA
}
