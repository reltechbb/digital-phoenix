/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * Provides CCSI Open Interface structures and logic for CCSI V1.1.1 
 *
 */
package com.domenix.ccsi.openintfc;