/*
 * NSDSLinkStateCmdEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/01
 */


package com.domenix.nsds.impl;

/**
 * This enumeration defines internal event types for notifying the link that socket or connection
 * events have occurred.
 * 
 * @author kmiller
 */
public enum NSDSLinkStateCmdEnum
{
  /** No command, a default value. */
  NONE("No command") ,
  
  /** Terminate now */
  TERMINATE("Terminate the thread") ,
  
  /** Listen for a host connection */
  LISTEN_HOST("Listening for a host connection") ,

  /** Initiate connection to the host */
  CONNECT_HOST("Connect to host") ,

  /** Close the physical connection (socket channel) */
  CLOSE_SOCKET("Close the physical connection.") ,

  /** Disconnect from the sensor and break the logical connection. */
  DISCONNECT_HOST("Disconnect from the host") ,

  /** Open a socket connection to a logically connected sensor. */
  OPEN_SOCKET("Open socket to connected host.") ,

  /** Connection attempt failed. */
  CONN_FAILED("Connection attempt failed.") ,

  /** Connection successful. */
  CONN_SUCCESS("Connection successful.") ,

  /** Socket was closed event */
  SOCKET_CLOSED("Open socket was closed.") ,

  /** Socket was opened event */
  SOCKET_OPENED("Socket was opened.") ,

  /** Sensor disconnected. */
  SENSOR_DISCONNECTED("The sensor is disconnected.");

  /** Human readable command description */
  private final String	description;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance of the enumeration with the provided description
   *
   * @param desc human readable description of the enumeration
   */
  private NSDSLinkStateCmdEnum( String desc )
  {
    this.description	= desc;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Return the human readable string describing this enumeration
   *
   * @return enumeration value description
   */
  @Override
  public String toString()
  {
    return ( this.description );
  }
}
