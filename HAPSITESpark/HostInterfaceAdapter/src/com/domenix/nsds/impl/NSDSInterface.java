/*
 * NSDSInterface.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This defines the basic interface between a CCSI protocol implementation and Network Security and
 * Discovery Services (NSDS) which is responsible for interactions with the network.
 *
 * Version: V1.0  15/08/02
 */
package com.domenix.nsds.impl;

import com.domenix.common.utils.UtilTimer;
import com.domenix.commonha.ccsi.CcsiAckNakEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.commonha.intfc.NSDSConnection;
import com.domenix.commonha.intfc.NSDSConnectionList;
import com.domenix.commonha.intfc.NSDSConnectionTypeEnum;
import com.domenix.commonha.intfc.NSDSLinkList;
import com.domenix.commonha.queues.HAQueues;
import java.util.concurrent.Executor;

/**
 * This defines the basic interface between a CCSI protocol implementation and Network Security and
 * Discovery Services (NSDS) which is responsible for interactions with the network.
 *
 * @author kmiller
 */
public interface NSDSInterface {
    /**
     * Initializes the interface.   MUST be called prior to any other operation.
     * @param queues
     * @param config 
     * @param theList 
     * @param theTimer 
     */
    public void initialize(HAQueues queues, HostNetworkSettings config, NSDSConnectionList theList, UtilTimer theTimer);

     /**
     * Completes the initialization of the interface and starts the process for receiving messages
     *
     * @param x - instance of implementing class
     */
    public void doRun(Executor x);
    

    /**
     * Start up the interface
     *
     * @return flag indicating success/failure
     */
    public boolean startInterface();

    /**
     * Shut down the interface
     *
     * @return flag indicating success/failure
     */
    public boolean shutDownInterface();

    /**
     * Register this sensor on the network. For non-network type operations this may be a null
     * implementation that simply returns success. If this is not a null implementation the function
     * must register the sensor with the network discovery mechanism and return the status. The
     * specific information required for the registration is unique to the NSDS mechanism on the
     * network and must be defined by the class implementing this interface and provided prior to
     * invoking the function.
     *
     * @param linkId the ID number of the link to be used
     *
     * @return indication of success (true) or failure (false).
     *
     * @throws IllegalArgumentException if the index is out of range or does not identify an
     * available link
     */
    public boolean register(int linkId) throws IllegalArgumentException;

    /**
     * Deregister this sensor from the network. For non-network type operations this may be a null
     * implementation that simply returns success. If this is not a null implementation the function
     * must deregister the sensor from the network discovery mechanism and return the status.
     *
     * @param linkId the ID number of the link to be used
     *
     * @return indication of success (true) or failure (false).
     *
     * @throws IllegalArgumentException if the index is out of range or does not identify an
     * available link
     */
    public boolean deregister(int linkId) throws IllegalArgumentException;

    /**
     * Connect the sensor to a host system. This function initiates the connection process on the
     * connection through its assigned link. The configuration information for the connection is
     * specific to the type of connection being implemented by the class that implements this
     * interface and must be provided prior to invoking this function.
     *
     * @param linkId the ID number of the link to be used
     *
     * @return an NSDSConnection object or null if the connection initiation fails
     *
     * @throws IllegalArgumentException if the index is out of range or does not identify an
     * available link
     */
    public NSDSConnection connect(int linkId) throws IllegalArgumentException;

    /**
     * Disconnect the sensor from a host system. This function initiates the disconnect process on
     * an active connection through its assigned link.
     *
     * @param connectionId the ID number of the connection to be tested
     *
     * @return indication of success (true) or failure (false)
     *
     * @throws IllegalArgumentException if the index is out of range or does not identify an active
     * connection
     */
    public boolean disconnect(long connectionId) throws IllegalArgumentException;

    //~--- get methods ----------------------------------------------------------
    /**
     * Return an indication of whether or not the connection is connected. This indication must be
     * based on the CCSI logical connection concept and return the appropriate value.
     *
     * @param connectionId the ID number of the connection to be tested
     *
     * @return NSDSConnectionTypeEnum indicating the type of connection
     *
     * @throws IllegalArgumentException if the index is out of range or does not identify an active
     * connection
     */
    public NSDSConnectionTypeEnum isConnected(long connectionId) throws IllegalArgumentException;

    /**
     * Returns a flag indicating whether the link underlying this connection is 'open'. Open means
     * that the underlying link is physically connected to the communications media and is available
     * for active communications. For example, on a TCP/IP socket type link it means that there is
     * currently an open socket between the sensor and the host system.
     *
     * @param connectionId the ID number of the connection to be tested
     *
     * @return boolean indicating open (true) or not open (false)
     *
     * @throws IllegalArgumentException if the index is out of range or does not identify an active
     * connection
     */
    public boolean isOpen(long connectionId) throws IllegalArgumentException;

    /**
     * Returns a list of the available links that can be used for communications with a host.
     *
     * @return NSDSLinkList containing a list of the links available on the sensor
     */
    public NSDSLinkList getAvailableLinks();

    /**
     * Returns a list of the available connections that can be used for communications with a host.
     *
     * @return NSDSConnectionList containing a list of the connections available on the sensor
     */
    public NSDSConnectionList getActiveConnections();

    /**
     * Get the host IP address
     *
     * @param connectionId the ID of the connection
     *
     * @return the host address or null if not found
     */
    public String getHostIp(long connectionId);

    /**
     * Returns an indication of whether or not the link is registered on the network.
     *
     * @param linkId the ID number of the link to be tested
     *
     * @return boolean indicating if the link is registered (true) or not (false)
     *
     * @throws IllegalArgumentException if the index is out of range or does not identify an
     * available link
     */
    public boolean isRegistered(int linkId) throws IllegalArgumentException;

    /**
     * Sends an acknowledgement for the MSN on the connection.
     *
     * @param connectionId the ID number of the connection
     * @param channel the channel for the ACK
     * @param msn the MSN of the message being acknowledged
     *
     * @return
     */
    public boolean sendAck(long connectionId, CcsiChannelEnum channel, long msn);

    /**
     * Sends a specified negative acknowledgment for the MSN on the connection with optional
     * parameters to fill in a detailed status report if the host is registered for the STATUS
     * channel and
     *
     * @param connectionId the ID number of the connection
     * @param channel the channel for NAK
     * @param msn the MSN of the message being negatively acknowledged
     * @param nakCode the message header NAK code
     * @param nakDetailCode the detailed NAK code of the error if applicable, else null
     * @param nakDetailMessage the detailed NAK message for the error if applicable, else null
     *
     * @return flag indicating successful queuing (true) or not (false)
     */
    public boolean sendNak(long connectionId, CcsiChannelEnum channel, long msn, NakReasonCodeEnum nakCode, NakDetailCodeEnum nakDetailCode, String nakDetailMessage);

    /**
     * Sends a heartbeat message to the host system.
     *
     * @param connectionId the ID number of the connection
     *
     * @return flag indicating successful queuing (true) or not (false)
     */
    public boolean sendHrtbt(long connectionId);

    /**
     * Sends a report message to the host system. The string provided must contain a complete CCSI
     * message to be transmitted as defined in the CCSI
     * schemas.
     *
     * @param connectionId the ID number of the connection
     * @param channel the channel of the message
     * @param cacheKeys the cache key(s) for the report(s) being sent
     * @param msg the message to be transmitted
     * @param ack is this an ack or nak
     * @param ackMsn mission serial number
     * @param nakCode reason for the Nak
     *
     * @return flag indicating successful queuing (true) or not (false)
     */
    public boolean sendReport(long connectionId, CcsiChannelEnum[] channel, String[] cacheKeys, String msg, CcsiAckNakEnum ack, long ackMsn, NakReasonCodeEnum nakCode);

    /**
     * Set a connection to be persistent
     *
     * @param connId the connection ID
     */
    public void setPersistent(long connId);

    /**
     * Set a connection to be transient
     *
     * @param connId the connection ID
     */
    public void setTransient(long connId);
}
