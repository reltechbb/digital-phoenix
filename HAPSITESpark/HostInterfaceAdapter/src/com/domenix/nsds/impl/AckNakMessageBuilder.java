/*
 * AckNakMessageBuilder.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains utility functions for creating ACK and NACK messages.
 *
 * Version: V1.0  15/10/26
 */
package com.domenix.nsds.impl;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.commonha.ccsi.CcsiAckNakEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.CcsiChannelRegistrationEnum;
import com.domenix.commonha.ccsi.ChannelRegistrationType;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.commonha.intfc.NSDSConnection;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//~--- classes ----------------------------------------------------------------
/**
 * A class to construct CCSI acknowledgment and negative acknowledgment
 * messages including a status report if sufficient information is provided in
 * the negative acknowledgment function.
 *
 * @author kmiller
 */
public class AckNakMessageBuilder
{

    /**
     * Open Interface connection message start
     */
    private static final String CONN_MSG_START = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<ccsi:CCSIConnection xmlns:ccsi=\"file:/C:/ccsi/1.1/CCSI\"\nb"
            + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
            + "xmlns:ism=\"urn:us:gov:ic:ism\" ";

    /**
     * The beginning of a CCSI V1.1.1 message
     */
    private static final String HEADER_START
            = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<ccsi:Hdr "
            + "xmlns:ccsi=\"file:/C:/ccsi/1.1/CCSI\" "
            + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
            + "xmlns:ism=\"urn:us:gov:ic:ism\" "
//            + "ism:resourceElement=\"true\" "
            + "xsi:schemaLocation=\"file:/C:/ccsi/1.1/CCSI file:/C:/ccsi/1.1/CCSI/CCSI_XML_Communications.xsd\" "
            ;

    /**
     * Status channel start
     */
    private static final String STATUS_START
            = "<CCSIDoc><Msg><StatusChn Alert=\"false\" BIT=\"false\" Maint=\"false\"><NakDetails";

    //~--- fields ---------------------------------------------------------------
    /**
     * Message with body header end
     */
    private final String bodyEnd = ">";

    /**
     * Header only end pattern
     */
    private final String hdrOnlyEnd = "/>";

    /**
     * The end of a message with a body.
     */
    private final String msgEnd = "</CCSIDoc></ccsi:Hdr>";

    /**
     * Start of header tag
     */
    private final Pattern hdrStartTag = Pattern.compile("\\s*<(([a-zA-Z0-9\\-\\.\\_]{1,30}):)?Hdr");

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger;

    /**
     * Host network configuration information
     */
    private final HostNetworkSettings theConfig;

    /**
     * Sensor saved state
     */
//    private final CcsiSavedState theState;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs an instance of the class
     *
     * @param cfg the saved configuration
     *
     * @exception IllegalArgumentException thrown if either parameter is null
     */
    public AckNakMessageBuilder(HostNetworkSettings cfg) throws IllegalArgumentException
    {
        this.myLogger = Logger.getLogger(AckNakMessageBuilder.class);

        if (cfg != null)
        {
            this.theConfig = cfg;
        } else
        {
            StringBuilder msg = new StringBuilder("Missing ");

            msg.append("config ");
            msg.append("argument in constructor.");
            myLogger.error("Missing argument configuration or saved state in constructor.");

            throw new IllegalArgumentException("Missing argument configuration or saved state in constructor.");
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * This method returns the sensor's SID using the CCSI rules which prefer
     * the HostID over the UUID.
     *
     * @return String containing the sensor's SID
     */
    private String getSensorSid()
    {
        String theSid = this.theConfig.getHostSensorIDOrUUID();

        return (theSid);
    }

    //~--- methods --------------------------------------------------------------
    /**
     * This method generates an acknowledgment message with macros in place for
     * the dtg and msn (e.g., {msgMsn} for the MSN and {msgDtg} for the date
     * time group.
     *
     * @param conn the logical connection
     * @param chan the channel being acknowledged
     * @param ackMsn the MSN being acknowledged
     *
     * @return a String containing a fully formed ACK message with two macros to
     * be filled in or null if there is an error.
     */
    public String makeAckMessage(NSDSConnection conn, CcsiChannelEnum chan, long ackMsn)
    {
        if ((conn != null) && (chan != null) && (ackMsn >= 0L))
        {
            StringBuilder msg = new StringBuilder(HEADER_START);
            msg.append(theConfig.getClassification());

            String theSid = this.getSensorSid();

            msg.append(" sid=\"").append(theSid.trim()).append("\"");
            msg.append(" msn=\"{msgMsn}\"");
            msg.append(" dtg=\"{msgDtg}\"");
            msg.append(" mod=\"").append(theConfig.getMode().name()).append("\"");
            msg.append(" chn=\"").append(chan.getAbbreviation()).append("\"");
            msg.append(" len=\"0\"").append(" ack=\"ACK\"");
            msg.append(" ackmsn=\"").append(Long.toString(ackMsn)).append("\"");
            msg.append(hdrOnlyEnd);

            return (msg.toString());
        } else
        {
            myLogger.error("Invalid or missing argument(s)");

            return (null);
        }
    }

    /**
     * This method generates a heartbeat message with macros in place for the
     * dtg and msn (e.g., {msgMsn} for the MSN and {msgDtg} for the date time
     * group.
     *
     * @param conn the logical connection
     *
     * @return a String containing a fully formed HRTBT message with two macros
     * to be filled in or null if there is an error.
     */
    public String makeHrtbtMessage(NSDSConnection conn)
    {
        if (conn != null)
        {
            StringBuilder msg = new StringBuilder(HEADER_START);
            msg.append(theConfig.getClassification());

            String theSid = this.getSensorSid();

            msg.append(" sid=\"").append(theSid.trim()).append("\"");
            msg.append(" msn=\"{msgMsn}\"");
            msg.append(" dtg=\"{msgDtg}\"");
            msg.append(" mod=\"").append(theConfig.getMode().name()).append("\"");
            msg.append(" chn=\"h\"").append(" len=\"0\"").append(hdrOnlyEnd);

            return (msg.toString());
        } else
        {
            myLogger.error("Invalid or missing argument(s)");

            return (null);
        }
    }

    /**
     * This method generates a negative acknowledgement message with macros in
     * place for the dtg and msn (e.g., {msgMsn} for the MSN and {msgDtg} for
     * the date time group.
     *
     * @param conn the logical connection
     * @param chan the channel being acknowledged
     * @param ackMsn the MSN being acknowledged
     * @param reason the reason for the NAK
     * @param nakDetail detailed NAK reason, may be null
     * @param nakText descriptive text for a detailed error, may be null
     *
     * @return a String containing a fully formed ACK message with two macros to
     * be filled in or null if there is an error.
     */
    public String makeNakMessage(NSDSConnection conn, CcsiChannelEnum chan, long ackMsn, NakReasonCodeEnum reason,
            NakDetailCodeEnum nakDetail, String nakText)
    {
        if ((conn != null) && (chan != null) && (reason != null))
        {
            StringBuilder msg = new StringBuilder(HEADER_START);
            msg.append(theConfig.getClassification());

            String theSid = this.getSensorSid();
            int msgLen = 0;
            boolean genDetails = false;
            long sendAckMsn = ((ackMsn >= 0L) ? ackMsn : 0L);

            if ((conn.getRegisteredChannels().hasChannel(CcsiChannelEnum.STATUS)))
            {
                ChannelRegistrationType statusReg = conn.getRegisteredChannels().getRegistration(CcsiChannelEnum.STATUS);

                if ((statusReg != null)
                        && ((statusReg.getRegType() == CcsiChannelRegistrationEnum.EVENT)
                        || (statusReg.getRegType() == CcsiChannelRegistrationEnum.PERIOD))
                        && (statusReg.isDetails()))
                {
                    if (nakDetail != null)
                    {
                        genDetails = true;
                    }
                }
            }

            // Build the basic header information with macros for the DTG and MSN
            msg.append(" sid=\"").append(theSid.trim()).append("\"");
            msg.append(" msn=\"{msgMsn}\"");
            msg.append(" dtg=\"{msgDtg}\"");
            msg.append(" mod=\"").append(theConfig.getMode().name()).append("\"");

            StringBuilder channels = new StringBuilder();

            if (genDetails)
            {
                channels.append(chan.getAbbreviation()).append('s');
            } else
            {
                channels.append(chan.getAbbreviation());
            }

            msg.append(" chn=\"").append(channels.toString()).append("\"");
            msg.append(" ack=\"NACK\" ackmsn=\"").append(Long.toString(sendAckMsn)).append("\"");
            msg.append(" nakcod=\"").append(reason.name()).append("\"");

            //
            // If we should provide the details, generate them as a status report
            if (genDetails)
            {
                StringBuilder statusMsg = new StringBuilder(AckNakMessageBuilder.STATUS_START);

                statusMsg.append(" Code=\"").append(nakDetail.getXmlCode()).append("\" MSN=\"").append(
                        Long.toString(ackMsn)).append("\"");

                // If we have text to report put that in
                if ((nakText != null) && (!nakText.trim().isEmpty()))
                {
                    statusMsg.append("><NakDescription>\"").append(nakText).append(
                            "</NakDescription></NakDetails></StatusChn></Msg></CCSIDoc></ccsi:Hdr>");
                } else
                {
                    statusMsg.append("/></StatusChn></Msg></CCSIDoc></ccsi:Hdr>");
                }

                // Now get the message length and finish the header
                msgLen = statusMsg.toString().length();
                msg.append(" len=\"").append(msgLen).append("\">").append(statusMsg.toString());
            } else
            {
                // No details, just end the message
                msg.append(" len=\"0\" />");
            }

            return (msg.toString());
        } else
        {
            myLogger.error("Invalid or missing argument(s)");

            return (null);
        }
    }

    /**
     * Build a CCSI report message and send it.
     *
     * @param body the body of the message
     * @param chan CcsiChannelEnum[]
     * @param ack the type of ACK or null for none
     * @param msn the ack MSN
     * @param code the nak code
     *
     * @return string containing the message
     */
    public String makeReportMessage(String body, CcsiChannelEnum[] chan, CcsiAckNakEnum ack, long msn,
            NakReasonCodeEnum code)
    {
        StringBuilder msg = new StringBuilder(HEADER_START);
        msg.append(theConfig.getClassification());

        // TODO: get the classification markings to validate with Hett
//        StringBuilder bodyBld = new StringBuilder("<CCSIDoc ism:classification=\"U\" ism:ownerProducer=\"USA\">").append(body);
        StringBuilder bodyBld = new StringBuilder("<CCSIDoc>").append(body);
        String theSid = this.getSensorSid();

        msg.append(" sid=\"").append(theSid.trim()).append("\"");
        msg.append(" msn=\"{msgMsn}\"");
        msg.append(" dtg=\"{msgDtg}\"");
        msg.append(" mod=\"").append(theConfig.getMode().name()).append("\"");

        StringBuilder channels = new StringBuilder();

        for (CcsiChannelEnum x : chan)
        {
            channels.append(x.getAbbreviation());
        }

        msg.append(" chn=\"").append(channels.toString()).append("\"");

        if ((ack != null) && (ack != CcsiAckNakEnum.NONE))
        {
            msg.append(" ack=\"").append(ack.name()).append("\"");
            msg.append(" ackmsn=\"").append(msn).append("\"");

            if (ack == CcsiAckNakEnum.NACK)
            {
                msg.append(" nakcod=\"").append(code.name()).append("\"");
            }
        }

        bodyBld.append(msgEnd);

        int msgLen = bodyBld.toString().length();

        msg.append(" len=\"").append(msgLen).append("\"");
        msg.append(bodyEnd).append(bodyBld.toString());

        return (msg.toString());
    }

    /**
     * Extracts the message header from a received message.
     *
     * @param msgBuf the received message
     *
     * @return String containing the extracted message header or null if not
     * found
     */
    public String extractHeader(String msgBuf)
    {
        Matcher hdrStartMatch = hdrStartTag.matcher(msgBuf);
        String theHdr = null;

        if ((msgBuf != null) && (msgBuf.trim().length() > 0))
        {
            if (hdrStartMatch.find())
            {
                int hdrPrefixStart = hdrStartMatch.start();
                int hdrPrefixEnd = hdrStartMatch.end();
                int hdrOnlyIndex = msgBuf.indexOf(hdrOnlyEnd, hdrPrefixEnd);
                int bodyIndex = msgBuf.indexOf(bodyEnd, hdrPrefixEnd);

                if ((hdrOnlyIndex != -1) && (bodyIndex != -1))
                {
                    if (hdrOnlyIndex < bodyIndex)
                    {
                        theHdr = msgBuf.substring(hdrPrefixStart, hdrOnlyIndex + 2);
                    } else
                    {
                        theHdr = msgBuf.substring(hdrPrefixStart, bodyIndex + 1);
                    }
                } else if (hdrOnlyIndex != -1)
                {
                    theHdr = msgBuf.substring(hdrPrefixStart, hdrOnlyIndex + 2);
                } else
                {
                    theHdr = msgBuf.substring(hdrPrefixStart, bodyIndex + 1);
                }
            }
        } else
        {
            // NO HEADER
        }

        return (theHdr);
    }

    /**
     * This method builds a CCSI open interface connection start message.
     *
     * @param theSid the SID to use for connecting
     *
     * @return String containing the message.
     */
    public String makeConnStartMessage(String theSid)
    {
        StringBuilder msg = new StringBuilder(AckNakMessageBuilder.CONN_MSG_START);

        msg.append(" sid=\"").append(theSid).append("\"");
        msg.append(" type=\"start\"/>");

        return (msg.toString());
    }

    /**
     * This method builds a CCSI open interface connection start message.
     *
     * @param theSid the SID to use for disconnecting
     *
     * @return String containing the message.
     */
    public String makeConnEndMessage(String theSid)
    {
        StringBuilder msg = new StringBuilder(AckNakMessageBuilder.CONN_MSG_START);

        msg.append(" sid=\"").append(theSid).append("\"");
        msg.append(" type=\"end\"/>");

        return (msg.toString());
    }
}
