/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * Provides specific implementations of the NSDS interface to support various communications links
 * and connection types.
 *
 */
package com.domenix.nsds.impl;