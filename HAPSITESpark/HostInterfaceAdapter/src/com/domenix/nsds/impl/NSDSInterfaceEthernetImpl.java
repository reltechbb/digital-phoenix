/*
 * NSDSInterfaceEthernetImpl.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains an implementation of the NSDSInterface for TCP/IP Ethernet connections.
 *
 * Version: V1.0  15/08/02
 */
package com.domenix.nsds.impl;

//~--- non-JDK imports --------------------------------------------------------
//import com.domenix.sensorinterfaceadapter.old.SensorReceiveCallbackInterface;
import com.domenix.ccsi.openintfc.MsgParserUtilities;
import com.domenix.common.enums.NetworkProtocolTypeEnum;
import com.domenix.common.spark.CtlWrapper;
import com.domenix.common.spark.CtlWrapperQueue;
import com.domenix.common.spark.IPCQueueEvent;
import com.domenix.common.spark.IPCQueueListenerInterface;
import com.domenix.common.utils.SparkThreadFactory;
import com.domenix.common.utils.SystemDateTime;
import com.domenix.common.utils.UtilTimer;
import com.domenix.commonha.ccsi.CcsiAckNakEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.commonha.enums.NSDSConnEventEnum;
import com.domenix.commonha.enums.NSDSTransferEventEnum;
import com.domenix.commonha.intfc.NSDSConnEvent;
import com.domenix.commonha.intfc.NSDSConnection;
import com.domenix.commonha.intfc.NSDSConnectionList;
import com.domenix.commonha.intfc.NSDSConnectionTypeEnum;
import com.domenix.commonha.intfc.NSDSLink;
import com.domenix.commonha.intfc.NSDSLinkEvent;
import com.domenix.commonha.intfc.NSDSLinkList;
import com.domenix.commonha.intfc.NSDSTransferEvent;
import com.domenix.commonha.queues.ConnEventQueue;
import com.domenix.commonha.queues.HAQueues;
import com.domenix.commonha.queues.LinkEventQueue;
import com.domenix.commonha.queues.NSDSXferEventQueue;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import org.apache.log4j.Logger;
//import com.domenix.ccsi.protocol.PHContext;
import com.domenix.nsds.impl.AckNakMessageBuilder;

//~--- classes ----------------------------------------------------------------
/**
 * This implements the NSDSInterface for Ethernet TCP/IP based communications.
 *
 * @author kmiller
 */
public class NSDSInterfaceEthernetImpl implements NSDSInterface, IPCQueueListenerInterface, Runnable
{
//    PHContext theContext = null;
//    private SensorReceiveCallbackInterface callback;
    private Thread thread;
    private Boolean server;
    
    /**
     * Pending send entry ID generator
     */
    private static final AtomicLong ITEM_ID_GENERATOR = new AtomicLong(0L);

    //~--- fields ---------------------------------------------------------------
    /**
     * Field description
     */
    private long myConnectId = 0L;

    /**
     * List of the available links
     */
    private final NSDSLinkList theLinks = new NSDSLinkList();

    /**
     * Started up flag
     */
    private boolean startedUp = false;

    /**
     * Shut down now
     */
    private boolean shutDown = false;

    /**
     * Field description
     */
    private boolean checkForStartMsg = true;

    /**
     * Field description
     */
    private boolean waitingForConnStart = false;

    /**
     * Waiting for a connection end message.
     */
    private final boolean waitingForConnEnd = false;

    /**
     * Enforce three strikes
     */
    private boolean enforceStrikes = true;

    /**
     * Connection event queue that we write
     */
    private ConnEventQueue connEventQueue;

    /**
     * The control queue
     */
    private CtlWrapperQueue controlQueue;

    /**
     * The executor for service threads
     */
    private ExecutorService executor;

    /**
     * The message builder
     */
    private AckNakMessageBuilder msgBuilder;

    /**
     * Field description
     */
    private MsgParserUtilities msgParser;

    /**
     * Field description
     */
    private LinkEventQueue myLinkEventQ;

    /**
     * Field description
     */
    private LinkedBlockingDeque<NSDSLinkEvent> myLinkEvents;

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger;
    /** is debug log level enabled */
    private final boolean logLevelDebug;
    
    /**
     * Our pending send list
     */
    private PendingSendItemList pendingSends;

    /**
     * The Host Network configuration
     */
    private HostNetworkSettings hostNetworkSettings;

    /**
     * Our transfer event queue - we only write to this
     */
    private NSDSXferEventQueue transferEventQueue;

    private static final SparkThreadFactory sparkPoolThreadFactory = new SparkThreadFactory ();
    
    private NSDSConnectionList connectionList;
    
    //~--- constructors ---------------------------------------------------------

    /**
     * Constructs a class instance.
     *
     */
    public NSDSInterfaceEthernetImpl()
    {
        myLogger = Logger.getLogger(NSDSInterfaceEthernetImpl.class);
        myLogger.isDebugEnabled();
        logLevelDebug = myLogger.isDebugEnabled();
    }

    /**
     * Initializes the interface.
     *
     * @param queues - messaging queues used by the Host Adapter
     * @param config - Host Interface configuration information
     * @param theList - list of connections
     * @param theTimer - timer class instance
     */
    //TODO:   pass in theContext.   Used to setUuid.
    @Override
    public void initialize(HAQueues queues, HostNetworkSettings config, NSDSConnectionList theList, UtilTimer theTimer)
    {
        if ((queues != null) && (config != null) && (theList != null) && (theTimer != null))
        {
//            this.theContext = theContext;
            connectionList = theList;
            hostNetworkSettings = config;
            this.connEventQueue = queues.getHiaConnEventQueue();
            this.transferEventQueue = queues.getHiaXferEventQueue();
            this.controlQueue = queues.getNsdsCtlQueue();
            this.controlQueue.addIPCQueueEventListener(this);
            this.msgBuilder = new AckNakMessageBuilder(hostNetworkSettings);
            this.executor = Executors.newFixedThreadPool(2, sparkPoolThreadFactory);
            this.checkForStartMsg = true;
            msgParser = new MsgParserUtilities(config.getClassification());
            this.myLinkEvents = new LinkedBlockingDeque<>();
            this.pendingSends = new PendingSendItemList();
            this.myLinkEventQ = queues.getNSDSLinkEventQueue();
            this.myLinkEventQ.addIPCQueueEventListener(this);
            this.enforceStrikes = hostNetworkSettings.isEnforceStrikes();

            //
            // Create the links
            //
            if (config.getNetworkProtocolType() == NetworkProtocolTypeEnum.Open)
            {
                NSDSLinkEthernetImpl theLink = new NSDSLinkEthernetImpl(hostNetworkSettings, 
                        connectionList, queues, theTimer);

                theLink.setBroadcastIpAddress(config.getBroadcastAddr());
                theLink.setLocalIpAddress(config.getPHIP());
                theLink.setSecureComms(config.isEncryptComm());

                if (!config.isOIServer())
                {
                    if (config.getIP() != null)
                    {
                        theLink.setHostIpAddress(config.getIP());
                        theLink.setHostIpPort(config.getPort());
                    } else
                    {
                        throw (new IllegalStateException("CONFIGURATION IS MESSED UP!"));
                    }
                }

                theLink.getLinkEventQueue().addIPCQueueEventListener(this);
                this.theLinks.addLink(theLink);
                myLogger.debug("Connect link " + theLink.getLinkId());
            }
        }

        myLogger.info("NSDSInterfaceEthernetImpl constructed");
    }

    
    
    
//KBM    START:    added from EthernetAdapterCcsi
//    /**
//     * Sets the callback for receiving data
//     *
//     * @param callback
//     */
//    public void setListener(SensorReceiveCallbackInterface callback) {
//        this.callback = callback;
//    }
      
//        /**
//     * Starts the receive data thread running
//     */
//    public void start() {
//        thread = new Thread(this);
//        if (server)
//        {
//            thread.setName("Server ReceiveDataThread");
//        } else {
//            thread.setName("Client ReceiveDataThread");
//        }
//        thread.start();
//    }
//KBM    END:    added from EthernetAdapterCcsi    
    
    
    
    
    
    //~--- methods --------------------------------------------------------------
    /**
     * Start up the interface
     *
     * @return flag indicating success/failure
     */
    @Override
    public boolean startInterface()
    {
        boolean retVal = false;

        if (!this.startedUp)
        {
            for (NSDSLink x : this.theLinks.getLinkList())
            {
                NSDSLinkEthernetImpl aLink = (NSDSLinkEthernetImpl) x;

                sparkPoolThreadFactory.setThreadName("NewNSDSLinkEthernetThread");
                this.executor.execute(aLink);
            }

            retVal = true;
            this.startedUp = retVal;
        } else
        {
            myLogger.info("Interface already started up");
        }

        if (retVal)
        {
            myLogger.info("Started the interface");
        } else
        {
            myLogger.error("Error starting the interface");
        }

        return (retVal);
    }

    /**
     * Shut down the interface
     *
     * @return flag indicating success/failure
     */
    @Override
    public boolean shutDownInterface()
    {
        boolean retVal = false;

        if (this.startedUp)
        {
            myLogger.info("Shutting down the interface");

            // SEND END MESSAGE TO TERMINATE if we have a connection
            for (NSDSConnection x : connectionList.getConnectionList())
            {
                x.setConnType(NSDSConnectionTypeEnum.NO_CONNECTION);

                if (x.getConnType() != NSDSConnectionTypeEnum.NO_CONNECTION)
                {
                    myLogger.debug("Sending open interface end on link " + x.getLinkId());

                    String endStr = this.createEndMsg();
                    long linkIndx = connectionList.getItem(this.myConnectId).getLinkId();
                    NSDSLinkEthernetImpl myLink = (NSDSLinkEthernetImpl) this.theLinks.getItem((int) linkIndx);
                    PendingSendItem psi = new PendingSendItem();

                    psi.isOIConnEndMsg = true;
                    this.pendingSends.addEntry(psi);
                    myLink.send(endStr, this.myConnectId, psi.itemId);
                }
            }

            for (NSDSLink x : this.theLinks.getLinkList())
            {
                NSDSLinkEthernetImpl aLink = (NSDSLinkEthernetImpl) x;

                aLink.shutDown();
            }

            boolean doMore = true;

            this.executor.shutdown();

            while (doMore)
            {
                try
                {
                    if (this.executor.awaitTermination(5, TimeUnit.SECONDS))
                    {
                        doMore = false;
                        retVal = true;
                        this.startedUp = false;
                    } else
                    {
                        this.executor.shutdownNow();
                        this.executor.awaitTermination(3, TimeUnit.SECONDS);
                        doMore = false;
                        retVal = true;
                        this.startedUp = false;
                    }
                } catch (InterruptedException ignore)
                {
                }
            }

            this.shutDown = true;
            myLogger.info("We are shut down");
        }

        return (retVal);
    }

    /**
     * Initiates NSDS actions to register the sensor on the network. This is a
     * NOOPa at this time and will always return true.
     *
     * @param linkId the ID number of the link for network registration
     *
     * @return flag indicating success (true) or failure (false)
     *
     * @throws IllegalArgumentException
     */
    @Override
    public boolean register(int linkId) throws IllegalArgumentException
    {
        return (true);
    }

    /**
     * Initiates actions to deregister the sensor from the network. This is a
     * NOOPa at this time and will always return true.
     *
     * @param linkId the ID number of the link for network registration
     *
     * @return flag indicating success (true) or failure (false)
     *
     * @throws IllegalArgumentException
     */
    @Override
    public boolean deregister(int linkId) throws IllegalArgumentException
    {
        return (true);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Get the host IP address
     *
     * @param connectionId the ID of the connection
     *
     * @return the host address or null if not found
     */
    @Override
    public String getHostIp(long connectionId)
    {
        String host;
        int link ;

        link = connectionList.getItem(connectionId).getLinkId();

        NSDSLink myLink = this.theLinks.getItem(link);

        host = myLink.getConnectHostAddress(connectionId);

        return (host);
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Connect the sensor to a host system. This function initiates the
     * connection process on the connection through its assigned link. The
     * configuration information for the connection is specific to the type of
     * connection being implemented by the class that implements this interface
     * and must be provided prior to invoking this function.
     *
     * @param linkId the ID number of the link to be used
     *
     * @return an NSDSConnection object or null if the connection initiation
     * fails
     *
     * @throws IllegalArgumentException if the index is out of range or does not
     * identify an available link
     */
    @Override
    public NSDSConnection connect(int linkId) throws IllegalArgumentException
    {
        NSDSConnection retVal = null;
        boolean gotLink = false;
        NSDSConnection c;

        myLogger.info("Connected to linkID: " + linkId);
        if (connectionList.isEmpty())
        {
            c = new NSDSConnection(linkId);
            c.setHostIpString(hostNetworkSettings.getIP().getHostAddress());
            connectionList.addConnection(c);
            gotLink = theLinks.getItem((int) linkId).connect(c.getConnectionId());

            if (gotLink)
            {
                retVal = c;
            }
        } else
        {
            for (NSDSConnection x : connectionList.getConnectionList())
            {
                if (x.getLinkId() == linkId)
                {
                    theLinks.getItem((int) linkId).connect(linkId);
                    retVal = x;
                }
            }
        }

        if (retVal != null)
        {
            this.myConnectId = retVal.getConnectionId();
        }

        return retVal;
    }

    /**
     * Disconnect the sensor from a host system. This function initiates the
     * disconnect process on an active connection through its assigned link.
     *
     * @param connectionId the ID number of the connection to be tested
     *
     * @return indication of success (true) or failure (false)
     *
     * @throws IllegalArgumentException if the index is out of range or does not
     * identify an active connection
     */
    @Override
    public boolean disconnect(long connectionId) throws IllegalArgumentException
    {
        boolean retVal = false;

        if (connectionId >= 0L)
        {
            long linkIndx = connectionList.getItem(connectionId).getLinkId();
            NSDSLinkEthernetImpl myLink = (NSDSLinkEthernetImpl) this.theLinks.getItem((int) linkIndx);

            if (myLink != null)
            {
                if (connectionList.getItem(connectionId).getConnType()
                        != NSDSConnectionTypeEnum.NO_CONNECTION)
                {
                    // send End Msg.
                    String endMsg = this.createEndMsg();
                    PendingSendItem psi = new PendingSendItem();

                    psi.isOIConnEndMsg = true;
                    this.pendingSends.addEntry(psi);
                    myLink.send(endMsg, connectionId, psi.itemId);
                    myLogger.debug ("Sending END MSG as part of Discconnect.");
                }

                retVal = myLink.disconnect(connectionId);
            }
        }

        return retVal;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Return an indication of whether or not the connection is connected. This
     * indication must be based on the CCSI logical connection concept and
     * return the appropriate value.
     *
     * @param connectionId the ID number of the connection to be tested
     *
     * @return NSDSConnectionTypeEnum indicating the type of connection
     *
     * @throws IllegalArgumentException if the index is out of range or does not
     * identify an active connection
     */
    @Override
    public NSDSConnectionTypeEnum isConnected(long connectionId) throws IllegalArgumentException
    {
        NSDSConnectionTypeEnum retVal = NSDSConnectionTypeEnum.NO_CONNECTION;

        if (connectionId >= 1L)
        {
            for (NSDSConnection x : connectionList.getConnectionList())
            {
                if (x.getConnectionId() == connectionId)
                {
                    retVal = x.getConnType();

                    return (retVal);
                }
            }
        }

        return (retVal);
    }

    /**
     * Returns a flag indicating whether the link underlying this connection is
     * 'open'. Open means that the underlying link is physically connected to
     * the communications media and is available for active communications. For
     * example, on a TCP/IP socket type link it means that there is currently an
     * open socket between the sensor and the host system.
     *
     * @param connectionId the ID number of the connection to be tested
     *
     * @return boolean indicating open (true) or not open (false)
     *
     * @throws IllegalArgumentException if the index is out of range or does not
     * identify an active connection
     */
    @Override
    public boolean isOpen(long connectionId) throws IllegalArgumentException
    {
        boolean retVal = false;

        if (connectionId >= 1L)
        {
            for (NSDSConnection x : connectionList.getConnectionList())
            {
                if (x.getConnectionId() == connectionId)
                {
                    NSDSLinkEthernetImpl y = (NSDSLinkEthernetImpl) this.theLinks.getItem((int) x.getLinkId());

                    return (y.isConnected());
                }
            }
        }

        return (retVal);
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * Used to tell NSDS to go to PERSISTENT_CONNECTION state.
     *
     * @param connectionId the ID number of the connection
     */
    @Override
    public void setPersistent(long connectionId)
    {
        connectionList.getItem(connectionId).setConnType(
                NSDSConnectionTypeEnum.PERSISTENT_CONNECTION);
    }

    /**
     * Used to tell NSDS to go to TRANSIENT_CONNECTION state.
     *
     * @param connectionId the ID number of the connection
     */
    @Override
    public void setTransient(long connectionId)
    {
        connectionList.getItem(connectionId).setConnType(NSDSConnectionTypeEnum.TRANSIENT_CONNECTION);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Returns a list of the available links that can be used for communications
     * with a host.
     *
     * @return NSDSLinkList containing a list of the links available on the
     * sensor
     */
    @Override
    public NSDSLinkList getAvailableLinks()
    {
        return (this.theLinks);
    }

    /**
     * Returns a list of the available connections that can be used for
     * communications with a host.
     *
     * @return NSDSConnectionList containing a list of the connections available
     * on the sensor
     */
    @Override
    public NSDSConnectionList getActiveConnections()
    {
        return (connectionList);
    }

    /**
     * Returns an indication of whether or not the link is registered on the
     * network.
     *
     * @param linkId the ID number of the link to be tested
     *
     * @return boolean indicating if the link is registered (true) or not
     * (false)
     *
     * @throws IllegalArgumentException if the index is out of range or does not
     * identify an available link
     */
    @Override
    public boolean isRegistered(int linkId) throws IllegalArgumentException
    {
        return (true);
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Sends an acknowledgement for the MSN on the connection.
     *
     * @param connectionId the ID number of the connection
     * @param channel the channel for the ACK
     * @param msn the MSN of the message being acknowledged
     *
     * @return - the message was sent successfully
     */
    @Override
    public boolean sendAck(long connectionId, CcsiChannelEnum channel, long msn)
    {
        boolean retVal = false;

        if (connectionId >= 1L)
        {
            for (NSDSConnection x : connectionList.getConnectionList())
            {
                if (x.getConnectionId() == connectionId)
                {
                    String msg = msgBuilder.makeAckMessage(x, channel, msn);
                    NSDSLinkEthernetImpl y = (NSDSLinkEthernetImpl) this.theLinks.getItem(x.getLinkId());
                    PendingSendItem psi = new PendingSendItem();

                    psi.sentAckId = msn;

                    CcsiChannelEnum[] channs = new CcsiChannelEnum[1];

                    channs[0] = channel;
                    psi.sentChannels = channs;
                    this.pendingSends.addEntry(psi);
                    retVal = y.send(msg, connectionId, psi.itemId);
                }
            }
        }

        return (retVal);
    }

    /**
     * Sends a specified negative acknowledgment for the MSN on the connection
     * with optional parameters to fill in a detailed status report if the host
     * is registered for the STATUS channel and
     *
     * @param connectionId the ID number of the connection
     * @param channel the channel for NAK
     * @param msn the MSN of the message being negatively acknowledged
     * @param nakCode the message header NAK code
     * @param nakDetailCode the detailed NAK code of the error if applicable,
     * else null
     * @param nakDetailMessage the detailed NAK message for the error if
     * applicable, else null
     *
     * @return flag indicating successful queuing (true) or not (false)
     */
    @Override
    public boolean sendNak(long connectionId, CcsiChannelEnum channel, long msn, NakReasonCodeEnum nakCode,
            NakDetailCodeEnum nakDetailCode, String nakDetailMessage)
    {
        boolean retVal = false;

        if (connectionId >= 1L)
        {
            for (NSDSConnection x : connectionList.getConnectionList())
            {
                if (x.getConnectionId() == connectionId)
                {
                    String msg = msgBuilder.makeNakMessage(x, channel, msn, nakCode, nakDetailCode,
                            nakDetailMessage);
                    NSDSLinkEthernetImpl y = (NSDSLinkEthernetImpl) this.theLinks.getItem((int) x.getLinkId());
                    PendingSendItem psi = new PendingSendItem();

                    psi.sentAckId = msn;
                    this.pendingSends.addEntry(psi);
                    retVal = y.send(msg, connectionId, psi.itemId);

                    if (this.enforceStrikes)
                    {
                        if (connectionList.getItem(myConnectId).incrementThreeStrikes())
                        {
                            this.disconnect(connectionId);
                        }
                    }
                }
            }
        }

        return (retVal);
    }

    /**
     * Sends a heartbeat message to the host system.
     *
     * @param connectionId the ID number of the connection
     *
     * @return flag indicating successful queuing (true) or not (false)
     */
    @Override
    public boolean sendHrtbt(long connectionId)
    {
        boolean retVal = false;

        if (connectionId >= 1L)
        {
            for (NSDSConnection x : connectionList.getConnectionList())
            {
                if (x.getConnectionId() == connectionId)
                {
                    String msg = msgBuilder.makeHrtbtMessage(x);
                    NSDSLinkEthernetImpl y = (NSDSLinkEthernetImpl) this.theLinks.getItem((int) x.getLinkId());
                    PendingSendItem psi = new PendingSendItem();
                    CcsiChannelEnum[] chans = new CcsiChannelEnum[1];

                    chans[0] = CcsiChannelEnum.HRTBT;
                    psi.sentChannels = chans;

                    this.pendingSends.addEntry(psi);
                    retVal = y.send(msg, connectionId, psi.itemId);
                }
            }
        }

        return (retVal);
    }

    /**
     * Sends a report message to the host system. The string provided must
     * contain a complete CCSI
     * message to be transmitted as
     * defined in the CCSI schemas.
     *
     * @param connectionId the ID number of the connection
     * @param channel the channels of the message
     * @param cacheKeys the cache key(s) for the report(s) being sent
     * @param msg the message to be transmitted
     * @param ack the type of ack/nak or null if not used
     * @param ackMsn the MSN to be acknowledged
     * @param nakCode the NAK code
     *
     * @return flag indicating successful queuing (true) or not (false)
     */
    @Override
    public boolean sendReport(long connectionId, CcsiChannelEnum[] channel, String[] cacheKeys, String msg,
            CcsiAckNakEnum ack, long ackMsn, NakReasonCodeEnum nakCode)
    {
        boolean retVal = false;

        if (connectionId >= 1L)
        {
            for (NSDSConnection x : connectionList.getConnectionList())
            {
                if (x.getConnectionId() == connectionId)
                {
                    String fullMsg = msgBuilder.makeReportMessage(msg, channel, ack, ackMsn, nakCode);
                    NSDSLinkEthernetImpl y = (NSDSLinkEthernetImpl) this.theLinks.getItem((int) x.getLinkId());
                    PendingSendItem psi = new PendingSendItem();

                    psi.cacheKeys = cacheKeys;
                    psi.sentChannels = channel;

                    if (ack != null)
                    {
                        psi.sentAckId = ackMsn;
                    }

                    this.pendingSends.addEntry(psi);
                    retVal = y.send(fullMsg, connectionId, psi.itemId);
                }
            }
        }

        return (retVal);
    }

    /**
     * Handler for IPCQueue interface events.
     *
     * @param evt the IPCQueue event
     */
    @Override
    public void queueEventFired(IPCQueueEvent evt)
    {
        if (evt.getEventSource() instanceof LinkEventQueue)
        {
            long theLinkId = 0L;
            NSDSLinkEvent linkEvt = this.myLinkEventQ.readFirst(0);

            if (linkEvt != null)
            {
                theLinkId = linkEvt.getLinkId();
                this.myLinkEvents.addLast(linkEvt);
            }
        } else if (evt.getEventSource() instanceof CtlWrapperQueue)
        {
            CtlWrapper ctlEvt = this.controlQueue.readFirst(0);

            if (ctlEvt != null)
            {
                if (ctlEvt.getCtlCommand() == CtlWrapper.STOP_THREAD)
                {
                    this.shutDownInterface();
                    this.shutDown = true;
                } else
                {
                }
            }
        } else
        {
            myLogger.error("Unexpected queue event received.");
        }
    }

    /**
     * Thread execution point
     */
    @Override
    public void run()
    {
        boolean processing = true;

        myLogger.info("Initiated NSDS IF Ethernet thread");

        while ((processing) && (!this.shutDown))
        {
            try
            {
                NSDSLinkEvent e = null;

                e = myLinkEvents.pollFirst(10, TimeUnit.SECONDS);

                if (e != null)
                {
                    NSDSConnectionTypeEnum connType = connectionList.getItem(myConnectId).getConnType();
                    PendingSendItem psi = null;

                    switch (e.getEventType())
                    {
                        case LINK_SEND_OK:
                            long item = e.getItemId();

                            if (item >= 0L)
                            {
                                psi = this.pendingSends.find(item);
                            }

                            if (psi != null)
                            {
                                if (connType == NSDSConnectionTypeEnum.NO_CONNECTION)
                                {
                                    if (psi.isOIConnStartMsg)
                                    {
                                        if (hostNetworkSettings.isOIServer())
                                        {
                                            connectionList.getItem(myConnectId).setConnType(
                                                    NSDSConnectionTypeEnum.TRANSIENT_CONNECTION);
                                        }
                                    }
                                } else
                                {
                                    if (psi.sentAckId >= 0L)
                                    {
                                        NSDSTransferEvent sentAck = new NSDSTransferEvent(NSDSTransferEventEnum.XFER_SENT_ACK);

                                        sentAck.setEventStatus(true);
                                        sentAck.setAckMsn(psi.sentAckId);
                                        sentAck.setChannels(psi.sentChannels);
                                        sentAck.setConnectionId(myConnectId);
                                        sentAck.setSentMsn(e.getSentMsn());
                                        this.transferEventQueue.insertLast(sentAck);

                                        // RESET THREE XXX's
                                        if (this.enforceStrikes)
                                        {
                                            connectionList.getItem(myConnectId).resetStrikes();
                                        }
                                    }

                                    if ((psi.sentChannels != null) && (psi.sentChannels.length == 1)
                                            && (psi.sentChannels[0] == CcsiChannelEnum.HRTBT))
                                    {
                                        // SEND HEARTBEAT EVENT.
                                        NSDSTransferEvent sentHrtbt = new NSDSTransferEvent(NSDSTransferEventEnum.XFER_SENT_HRTBT);

                                        sentHrtbt.setEventStatus(true);
                                        sentHrtbt.setAckFlag(false);
                                        sentHrtbt.setSentMsn(e.getSentMsn());
                                        sentHrtbt.setConnectionId(myConnectId);
                                        this.transferEventQueue.insertLast(sentHrtbt);
                                    } else if ((psi.sentChannels != null) && (psi.sentChannels.length > 1))
                                    {
                                        // SEND REPORT EVENT.
                                        NSDSTransferEvent sentRpt = new NSDSTransferEvent(NSDSTransferEventEnum.XFER_SENT_REPORT);

                                        sentRpt.setEventStatus(true);
                                        sentRpt.setAckFlag(false);
                                        sentRpt.setSentMsn(e.getSentMsn());
                                        sentRpt.setConnectionId(myConnectId);
                                        sentRpt.setCacheKeys(psi.cacheKeys);
                                        sentRpt.setChannels(psi.sentChannels);
                                        this.transferEventQueue.insertLast(sentRpt);
                                    } else if ((psi.sentChannels != null) && (psi.sentChannels.length == 1))
                                    {
                                        if ((psi.sentChannels.length == 1)
                                                && ((psi.sentChannels[0] != CcsiChannelEnum.HRTBT)
                                                && (psi.sentChannels[0] != CcsiChannelEnum.CMD)))
                                        {
                                            // SEND REPORT EVENT.
                                            NSDSTransferEvent sentRpt = 
                                                    new NSDSTransferEvent(NSDSTransferEventEnum.XFER_SENT_REPORT);

                                            sentRpt.setEventStatus(true);
                                            sentRpt.setSentMsn(e.getSentMsn());
                                            sentRpt.setCacheKeys(psi.cacheKeys);
                                            sentRpt.setChannels(psi.sentChannels);
                                            sentRpt.setConnectionId(myConnectId);
                                            this.transferEventQueue.insertLast(sentRpt);
                                        }
                                    }
                                }    // end if have a connection - next test we have no connection
                            }      // end if we found a pending send entry

                            break;

                        case LINK_CONNECTED:
                            myLogger.info("Got LINK_CONNECTED message");
                            if (connectionList.getItem(myConnectId).getConnType()
                                    == NSDSConnectionTypeEnum.NO_CONNECTION)
                            {
                                if (hostNetworkSettings.isOIServer())
                                {
                                    this.waitingForConnStart = true;
                                } else
                                {
                                    this.waitingForConnStart = true;

                                    NSDSLink aLink
                                            = this.theLinks.getItem(connectionList.getItem(myConnectId).getLinkId());
                                    String connStartMsg = this.createReturnStartMsg();

                                    psi = new PendingSendItem();
                                    psi.isOIConnStartMsg = true;
                                    this.pendingSends.addEntry(psi);
                                    aLink.send(connStartMsg, myConnectId, psi.itemId);
                                }
                            }

                            break;

                        case LINK_DISCONNECTED:
                            if (connectionList.getItem(myConnectId).getConnType()
                                    != NSDSConnectionTypeEnum.PERSISTENT_CONNECTION)
                            {
                                connectionList.getItem(myConnectId).setConnType(
                                        NSDSConnectionTypeEnum.NO_CONNECTION);

                                NSDSConnEvent connLostEvt = new NSDSConnEvent(NSDSConnEventEnum.CONN_TERMINATED);

                                connLostEvt.setConnId(this.myConnectId);
                                this.connEventQueue.insertLast(connLostEvt);
                            }

                            break;

                        case LINK_RECEIVED:    // MSG REC'D....
                            String msg = e.getLinkBuffer();
                            myLogger.info("Got LINK_RECEIVED message: " + msg);

                            if (!msg.isEmpty())
                            {
                                this.validateAndRoute(msg);
                            }

                            break;

                        default:
                            break;
                    }
                }
            } catch (Exception ex)
            {
                myLogger.error("Thread exiting due to exception.", ex);
            }
        }

        myLogger.info("Thread exit");
    }

    /**
     * Validate a received SID
     *
     *
     * @param sid
     * @param conn flag indicating this is a connection message
     *
     * @return valid or not valid in this context
     */
    private boolean validateSid(String sid, boolean conn)
    {
        boolean retVal = false;

        if ((hostNetworkSettings.getHostSensorID() != null)
                && (hostNetworkSettings.getHostSensorID().trim().length() > 0)
                && (sid.equalsIgnoreCase(hostNetworkSettings.getHostSensorID())))
        {
            if (logLevelDebug)
            {
                myLogger.debug ("SID " + sid + " IS GOOD!");
            }
            retVal = true;
        } else if (sid.equalsIgnoreCase(hostNetworkSettings.getUuid()))
        {
            if (logLevelDebug)
            {
                myLogger.debug ("SID is set to UUID: " + sid);
            }
            retVal = true;
        } else if ((sid.equalsIgnoreCase(hostNetworkSettings.getDiscoverySID())) && conn)
        {
            if (logLevelDebug)
            {
                myLogger.debug ("SID is set to DISCOVERY: " + sid);
            }
            retVal = true;
        } else
        {
            myLogger.error ("SID \"" + sid + "\" IS NOT VALID!!!" +
                    " Must be one of Host ID, Sensor UUID or Discovery UUID");
        }

        return retVal;
    }

    /**
     * Creates a start message to reply to the host
     *
     * @return the message
     */
    private String createReturnStartMsg()
    {
        String temp;

        temp = hostNetworkSettings.getHostSensorIDOrUUID();
        
        String retStr = msgParser.getMsgConnection() + "sid=\"" + temp + "\" type=\"start\"/>";

        myLogger.info("start msg = " + retStr);

        return retStr;
    }

    /**
     * Creates an open interface end message
     *
     * @return the message
     */
    private String createEndMsg()
    {
        String temp = hostNetworkSettings.getHostSensorIDOrUUID();
        String sendStr = msgParser.getMsgConnection() + "sid=\"" + temp + "\" type=\"end\"/>";

        return sendStr;
    }

    /**
     * Validates the message length field
     *
     * @param str the message
     *
     * @return valid or not valid
     */
    private boolean validMsgLen(String str)
    {
        boolean retVal = false;
        Matcher lenPattern = msgParser.getLenPattern().matcher(str);
        int indx = str.indexOf("len=");
        int slashBracketIndx = str.indexOf("/>", indx);
        int slashIndx = str.indexOf(">", indx);
        int i = 0;
        int lengthStr = str.length();

        if (lenPattern.find())
        {
            String len = lenPattern.group(1);
            int lenInt = Integer.parseInt(len);

            if ((lenInt == 0) && (slashBracketIndx > indx))
            {
                return true;
            }

            if (slashIndx > 0)
            {
                i = slashIndx + 1;

                int msgLen = lengthStr - i;

                if (msgLen == lenInt)
                {
                    return true;
                }
            }
        }

        return retVal;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Gets the message length field
     *
     * @param str the message
     *
     * @return the length value
     */
    private int getMsgLen(String str)
    {
        int lenInt = 0;
        Matcher lenPattern = msgParser.getLenPattern().matcher(str);

        if (lenPattern.find())
        {
            String len = lenPattern.group(1);

            lenInt = Integer.parseInt(len);
        }

        return lenInt;
    }

    /**
     * Get the channels in the header or return null if no valid channels were
     * found
     *
     * @param msg the message to be parsed
     *
     * @return channels or null
     */
    private String getValidChannel(String msg)
    {
        String chans = msgParser.getChannels(msg);

        if (chans != null)
        {
            return (chans);
        } else
        {
            myLogger.error("Missing or invalid channels.");
        }

        return null;
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Validates the received DTG
     *
     * @param str the message
     *
     * @return true = valid or false = not valid
     */
    private boolean validDTG(String str)
    {
        boolean retVal = false;
        Matcher dtgPattern = msgParser.getDtgPattern().matcher(str);
        long currentTime = System.currentTimeMillis() / 1000;

        if (hostNetworkSettings.getEnforceTime() == false)
        {
            retVal = true;
        } else
        {
            if (logLevelDebug)
            {
                myLogger.debug ("current time = " + currentTime);
            }
            if ((str != null) && (str.trim().length() > 0))
            {
                if (dtgPattern.find())
                {
                    String dtgStr = dtgPattern.group(1);
                    long dtg = Long.parseLong(dtgStr);

                    if (logLevelDebug)
                    {
                        myLogger.debug ("parsed dtg = " + dtg);
                    }

                    long temp = currentTime - dtg;

                    if (Math.abs(temp) <= hostNetworkSettings.getEnforceTimeValue())
                    {
                        retVal = true;
                    }
                }
            }
        }
        return retVal;
    }

    /**
     * Validates the ACK field
     *
     * @param str the message
     *
     * @return valid or not valid
     */
    private boolean validAck(String str)
    {
        boolean retVal = false;
        Matcher mAck = msgParser.getAckPattern().matcher(str);

        if (mAck.find())
        {
            String ack = mAck.group(1);

            if (ack.matches("ACK"))
            {
                retVal = true;
            }
        }

        return retVal;
    }

    /**
     * Validates the received MSN
     *
     * @param str the message
     *
     * @return valid or not valid
     */
    private boolean validMSN(String str)
    {
        boolean validMsn = false;
        boolean reTx = false;
//        long msnNum = -1L;

        long msnNum = msgParser.getMsn(str);
        if (logLevelDebug)
        {
            myLogger.debug ("MSN of msg: " + msnNum);
        }
        String sFlag = msgParser.getFlags(str);

        if (sFlag != null)
        {
            if (sFlag.contains("R"))
            {
                reTx = true;
            }
        }

        boolean resync = msgParser.isResetMsgSeq(str);
        if (logLevelDebug)
        {
            myLogger.debug ("Validating with: " + msnNum + " reTx: " + reTx + " resync: " + resync);
        }

        validMsn
                = connectionList.getItem(myConnectId).getConnectionMsns().validateReceivedMsn(msnNum, reTx,
                        resync);

        return validMsn;
    }

    /**
     * Returns an array of enumeration values for the channels listed in the
     * header
     *
     * @param x the channel string
     *
     * @return array of CcsiChannelEnum's
     */
    private CcsiChannelEnum[] makeEnumArray(String x)
    {
        CcsiChannelEnum[] retVal;

        if ((x != null) && (!x.isEmpty()))
        {
            retVal = new CcsiChannelEnum[x.length()];

            for (int i = 0; i < x.length(); i++)
            {
                retVal[i] = CcsiChannelEnum.value(x.charAt(i));
            }
        } else
        {
            retVal = new CcsiChannelEnum[1];
            retVal[0] = CcsiChannelEnum.CMD;
        }

        return (retVal);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * Gets the SID from the header
     *
     * @return the SID
     */
    private String getSensorSid()
    {
        String retVal = hostNetworkSettings.getHostSensorIDOrUUID();
        return (retVal);
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Validates a received message and routes queue entries to the PH
     *
     * @param fullMsg the message
     */
    private void validateAndRoute(String fullMsg)
    {
        boolean isDiscovery = false;    // Is this a discovery start connect
        long msgMsn = -1L;      // The MSN
        CcsiChannelEnum channels[] = null;     // The parsed channel result
        NSDSConnection myConnection = connectionList.getItem(this.myConnectId);
        String msgType = null;     // The type of message as conn or msg
        long ackMsn = -1L;      // The ack/nak MSN
        NakReasonCodeEnum hdrNakCode = null;     // The NAK reason
        String hdrSid = null;     // The received SID
        String connType = null;     // The connection type
        String msgDtg = "";
        String sChannels = "";
        String sFlags = "";
        String nakCode = "";
        boolean msnError = false;
        boolean dtgError = false;
        boolean lenError = false;

        // What kind do we have?
        msgType = msgParser.getHdrConnType(fullMsg);

        if (msgType != null)
        {
            hdrSid = msgParser.getSid(fullMsg);

            if (hdrSid != null)
            {
                if (msgType.charAt(0) == 'H')
                {
                    // This is a CCSI message
                    if (this.validateSid(hdrSid, false)) // Valid SID?
                    {
                        // MESSAGE HDR VALIDATION:
                        // msglen
                        // channels
                        // flg
                        // dtg
                        // msn     //need flag reTx
                        // ack
                        // ackmsn
                        // nakcode
                        // and ROUTE    Gen. xfer  recv  Events for Cmd, Ack, Hrtbt
                        // 3 strikes:
                        // SID, MSN, DTG, LEN   ==  NOT VALID = STRIKE
                        // if strike (MSN, DTG, LEN) , NAK message based upon NakCodes (5 in all)
                        if (this.validMsgLen(fullMsg))
                        {                                                // get Msg Len.
                            int msgLen = this.getMsgLen(fullMsg);

                            if (logLevelDebug)
                            {
                                myLogger.debug ("The length is " + msgLen);
                            }
                        } else
                        {
                            lenError = true;
                        }

                        if (this.validDTG(fullMsg))
                        {
                            msgDtg = msgParser.getDtg(fullMsg);
                        } else
                        {
                            dtgError = true;
                        }

                        sChannels = msgParser.getChannels(fullMsg);    // get Channels
                        sFlags = msgParser.getFlags(fullMsg);       // get Flags.

                        if (this.validMSN(fullMsg))
                        {                                                // get MSN.
                            msgMsn = msgParser.getMsn(fullMsg);
                        } else
                        {
                            msnError = true;
                            // Maybe need to increment MSN
                        }

                        if ((lenError) || (dtgError) || (msnError) || (sChannels == null))
                        {
                            // if strike (MSN, DTG, LEN, CHAN) , NAK message based upon NakCodes (5 in all):
                            if (this.enforceStrikes)
                            {
                                connectionList.getItem(myConnectId).incrementThreeStrikes();
                            }

                            ackMsn = msgParser.getAckMsn(fullMsg);

                            if (lenError)
                            {
                                hdrNakCode = NakReasonCodeEnum.LENMSM;
                            } else if (dtgError)
                            {
                                hdrNakCode = NakReasonCodeEnum.TIMOUT;
                            } else if (msnError)
                            {
                                hdrNakCode = NakReasonCodeEnum.MSGMSN;
                            } else
                            {
                                hdrNakCode = NakReasonCodeEnum.FORMAT;
                            }

                            String baseChans = this.getValidChannel(fullMsg);

                            if ((baseChans == null) || (baseChans.isEmpty()))
                            {
                                baseChans = msgParser.getRawChannels(fullMsg);

                                if ((baseChans == null) || (baseChans.isEmpty()))
                                {
                                    baseChans = "c";
                                }
                            }

                            CcsiChannelEnum[] enumChans = new CcsiChannelEnum[baseChans.length()];

                            for (int i = 0; i < enumChans.length; i++)
                            {
                                CcsiChannelEnum val = CcsiChannelEnum.value(baseChans.charAt(i));

                                if (val != null)
                                {
                                    enumChans[i] = val;
                                }
                            }

                            if (enumChans[0] == null)
                            {
                                enumChans[0] = CcsiChannelEnum.CMD;
                            }

                            // SEND NAK:
                            this.sendNak(myConnectId, enumChans[0], msgMsn, hdrNakCode, null, null);
                        } else
                        {
                            // GOOD MESSAGE!!!  RESET THREE XXX's
                            if (this.enforceStrikes)
                            {
                                connectionList.getItem(myConnectId).resetStrikes();
                            }

                            sChannels = this.getValidChannel(fullMsg);

                            CcsiChannelEnum[] eChannels;

                            if ((sChannels == null) || (sChannels.isEmpty()))
                            {
                                sChannels = "c";
                            }

                            eChannels = this.makeEnumArray(sChannels);

                            // GOOD MESSAGE!!!    ROUTE TO CORRECT:  Gen. xfer  recv  Events for Cmd, Ack, Hrtbt
                            if (this.validAck(fullMsg))
                            {
                                NSDSTransferEvent recvAckEvt = new NSDSTransferEvent(NSDSTransferEventEnum.XFER_RCV_ACK);

                                recvAckEvt.setConnectionId(this.myConnectId);
                                recvAckEvt.setSentMsn(msgMsn);
                                recvAckEvt.setChannels(eChannels);

                                CcsiAckNakEnum which = this.msgParser.getAck(fullMsg);

                                recvAckEvt.setAckFlag(true);
                                recvAckEvt.setEventStatus(which == CcsiAckNakEnum.ACK);
                                ackMsn = this.msgParser.getAckMsn(fullMsg);
                                recvAckEvt.setAckMsn(ackMsn);
                                nakCode = this.msgParser.getNakCode(fullMsg);
                                recvAckEvt.setNakCode(nakCode);
                                this.transferEventQueue.insertLast(recvAckEvt);
                            }

                            if (sChannels.contains("h"))
                            {
                                NSDSTransferEvent recvHrtbtEvt = new NSDSTransferEvent(NSDSTransferEventEnum.XFER_RCV_HRTBT);

                                recvHrtbtEvt.setConnectionId(this.myConnectId);
                                recvHrtbtEvt.setSentMsn(msgMsn);
                                long dtg = Long.parseLong(msgDtg);
                                recvHrtbtEvt.setRcvdDtg(dtg);
                                this.transferEventQueue.insertLast(recvHrtbtEvt);
                            } else if (sChannels.contains("c"))
                            {
                                NSDSTransferEvent recvCmdEvt = new NSDSTransferEvent(NSDSTransferEventEnum.XFER_RCV_CMD);

                                recvCmdEvt.setConnectionId(this.myConnectId);
                                recvCmdEvt.setRecvdMsn(msgMsn);
                                recvCmdEvt.setFlags(sFlags);
                                recvCmdEvt.setAckFlag(false);
                                recvCmdEvt.setChannels(eChannels);
                                recvCmdEvt.setEventStatus(true);

                                long dtg = Long.parseLong(msgDtg);

                                recvCmdEvt.setRcvdDtg(dtg / 1000);
                                recvCmdEvt.setRecvdBody(fullMsg);
                                this.transferEventQueue.insertLast(recvCmdEvt);
                                myLogger.debug ("Sent CMD Event to PH.");
                            } else
                            {
                                if (logLevelDebug)
                                {
                                    myLogger.debug ("Received " + sChannels);
                                }
                            }
                        }
                    } else
                    {
                        if (this.enforceStrikes)
                        {
                            connectionList.getItem(myConnectId).incrementThreeStrikes();
                        }

                        myLogger.error("Error:  Drop message, SID not valid.");
                    }
                } else
                {
                    // get the date and time from the connect message and use it to set the system date and time
                    String connectTime = msgParser.getConnectTime(fullMsg);
                    if (connectTime != null)
                    {
                        SystemDateTime systemDateTime = new SystemDateTime();
                        try
                        {
                            systemDateTime.setSystemDateTime(connectTime);
                        } catch (IOException | ParseException ex)
                        {
                            myLogger.error("Exception setting date and time", ex);
                        }
                    }
                    // This is a connection message
                    connType = msgParser.getOIConnType(fullMsg);

                    if (connType != null)
                    {
                        if (connType.charAt(0) == 's')
                        {
                            boolean isSidValid = false;

                            if (this.waitingForConnStart)
                            {
                                isSidValid = this.validateSid(hdrSid, true);
                            } else
                            {
                                isSidValid = this.validateSid(hdrSid, false);
                            }

                            if (isSidValid)
                            {
                                isDiscovery = hdrSid.contentEquals(hostNetworkSettings.getDiscoverySID());

                                if (isDiscovery)
                                {
                                    hdrSid = this.getSensorSid();
                                }

                                if (hdrSid != null)
                                {
                                    if ((myConnection.getConnType() == NSDSConnectionTypeEnum.NO_CONNECTION)
                                            && (this.waitingForConnStart))
                                    {
                                        if (hostNetworkSettings.isOIServer())
                                        {
                                            myLogger.debug ("Sending OI start message reply");
                                            String startStr = this.createReturnStartMsg();
                                            PendingSendItem psi = new PendingSendItem();

                                            psi.isOIConnStartMsg = true;
                                            this.pendingSends.addEntry(psi);

                                            NSDSLink aLink = this.theLinks.getItem(myConnection.getLinkId());
                                            boolean retVal = aLink.send(startStr, myConnectId, psi.itemId);

                                            myLogger.debug ("SEND STATUS: " + retVal);

                                            if (retVal)
                                            {
                                                // Initialize MSNs.
                                                connectionList.getItem(myConnectId).getConnectionMsns().initializeMsns();
                                                connectionList.getItem(myConnectId).setConnType(
                                                        NSDSConnectionTypeEnum.TRANSIENT_CONNECTION);
                                            }

                                            this.waitingForConnStart = false;
                                        } else
                                        {
//TODO                                            
                                            // As Client:   go to Transient and Initialize MSNs.
                                            connectionList.getItem(myConnectId).setConnType(
                                                    NSDSConnectionTypeEnum.TRANSIENT_CONNECTION);
                                            connectionList.getItem(myConnectId).getConnectionMsns().initializeMsns();
                                            //send the SID to theContext.
                                            //initiate send of GetIdentification.

                                        }
                                    } else
                                    {
                                        myLogger.error("Error:  Received 2nd Connect Msg.");
                                    }
                                }
                            } else
                            {
                                // If Start Message and BAD SID, disconnect.
                                if (this.enforceStrikes)
                                {
                                    connectionList.getItem(myConnectId).incrementThreeStrikes();
                                }

                                myLogger.error("Error:  Drop Start Connection message, SID not valid.");
                                this.disconnect(myConnectId);
                            }
                        } else
                        {
                            if (this.validateSid(hdrSid, false))
                            {
                                myLogger.debug ("Sending reply to OI end message");
                                String endStr = this.createEndMsg();
                                PendingSendItem psi = new PendingSendItem();

                                psi.isOIConnEndMsg = true;
                                this.pendingSends.addEntry(psi);

                                NSDSLink aLink = this.theLinks.getItem(myConnection.getLinkId());
                                boolean retVal = aLink.send(endStr, myConnectId, psi.itemId);
                                connEventQueue.insertLast(
                                        new NSDSConnEvent(NSDSConnEventEnum.CONN_TERMINATED, true, myConnectId));
                                myLogger.debug ("SEND STATUS: " + retVal);
                                connectionList.getItem(myConnectId).setConnType(
                                        NSDSConnectionTypeEnum.NO_CONNECTION);
                                this.disconnect(myConnectId);
                            }
                        }
                    } else
                    {
                        myLogger.error("Error:   Drop Msg, connection type is NULL.");
                    }
                }
            } else
            {
                myLogger.error("Error: SID is blank, must be one of Host ID, Sensor UUID or Discovery UUID");
            }
        } else
        {
            myLogger.error("Error:   Ignore Msg, msg type is NULL.");
        }
    }

    /**
     * Starts the execution of this thread
     *
     * @param x
     */
    @Override
    public void doRun(Executor x)
    {
        x.execute(this);
    }

    //~--- inner classes --------------------------------------------------------
    /**
     * Container for message waiting to be sent
     */
    class PendingSendItem
    {

        /**
         * The cache keys
         */
        String[] cacheKeys = null;

        /**
         * The ID of this pending send item
         */
        long itemId = -1L;

        /**
         * The ACK MSN sent
         */
        long sentAckId = -1L;    // TCS needs to be initialized correctly

        /**
         * The channels that were sent
         */
        CcsiChannelEnum[] sentChannels = null;

        /**
         * This is an Open Interface start message
         */
        boolean isOIConnStartMsg = false;

        /**
         * This is an Open Interface end message
         */
        boolean isOIConnEndMsg = false;

        //~--- constructors -------------------------------------------------------
        /**
         * Constructs an item instance
         */
        public PendingSendItem()
        {
            itemId = ITEM_ID_GENERATOR.getAndIncrement();

            if (itemId == Long.MAX_VALUE)
            {
                ITEM_ID_GENERATOR.set(0L);
            }
        }
    }

    /**
     * A list to hold pending send entries
     */
    class PendingSendItemList
    {

        /**
         * The entry map indexed by ID
         */
        private final HashMap<Long, PendingSendItem> pendingList = new HashMap<>();

        //~--- constructors -------------------------------------------------------
        /**
         * Constructs a list instance
         */
        public PendingSendItemList()
        {
            pendingList.clear();
        }

        //~--- methods ------------------------------------------------------------
        /**
         * Adds an entry to the list
         *
         * @param x the item to be added
         *
         * @return added (true) or not added (false)
         */
        public boolean addEntry(PendingSendItem x)
        {
            boolean retVal = false;
            PendingSendItem v = this.pendingList.put(Long.valueOf(x.itemId), x);

            if (v == null)
            {
                retVal = true;
            }

            return (retVal);
        }

        //~--- get methods --------------------------------------------------------
        /**
         * Get the length of the list
         *
         * @return list length
         */
        public int getSize()
        {
            return (this.pendingList.size());
        }

        //~--- methods ------------------------------------------------------------
        /**
         * Get the item by its ID
         *
         * @param itemId the item's ID
         *
         * @return the item or null if not in the list
         */
        public PendingSendItem find(long itemId)
        {
            Long testId = itemId;

            if (this.pendingList.containsKey(testId))
            {
                return (this.pendingList.get(testId));
            }

            return (null);
        }

        /**
         * Removes an entry from the list
         *
         * @param itemId the ID of the item to remove
         */
        public void remove(long itemId)
        {
            Long testId = itemId;

            this.pendingList.remove(testId);
        }
    }
}
