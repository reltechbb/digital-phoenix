/*
 * NSDSLinkEthernetImpl.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains a specialized NSDS link for Ethernet based communications.
 *
 * Version: V1.0  15/08/02
 */
package com.domenix.nsds.impl;

//~--- non-JDK imports --------------------------------------------------------
import com.domenix.commonha.ccsi.CcsiMsnType;
import com.domenix.ccsi.openintfc.MsgParserUtilities;
import com.domenix.commonha.queues.LinkEventQueue;
import com.domenix.commonha.intfc.NSDSConnection;
import com.domenix.commonha.intfc.NSDSConnectionTypeEnum;
import com.domenix.commonha.intfc.NSDSInterfaceTypeEnum;
import com.domenix.commonha.intfc.NSDSLinkEvent;
import com.domenix.commonha.enums.NSDSLinkEventTypeEnum;
import com.domenix.common.spark.IPCQueueEvent;
import com.domenix.common.spark.IPCQueueListenerInterface;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.common.utils.TimerChangeEvent;
import com.domenix.common.utils.TimerPurposeEnum;
import com.domenix.common.utils.UtilTimer;
import com.domenix.common.utils.UtilTimerListener;
import com.domenix.commonha.intfc.NSDSConnectionList;
import com.domenix.commonha.queues.HAQueues;

import org.apache.log4j.Logger;

//~--- JDK imports ------------------------------------------------------------
import java.io.IOException;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.StandardSocketOptions;

import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

import javax.security.cert.X509Certificate;

//~--- classes ----------------------------------------------------------------
/**
 * This class extends the NSDSLink type for Ethernet TCP/IP interfaces.
 *
 * @author kmiller
 */
public class NSDSLinkEthernetImpl extends com.domenix.commonha.intfc.NSDSLink
        implements Runnable, IPCQueueListenerInterface, UtilTimerListener
{

    /**
     * The timeout wait in milli seconds
     */
    private static final int DEFAULT_CONN_RETRY_TIMEOUT = 300;

    /**
     * Field description
     */
    private static final int MAX_CONNECT_ATTEMPTS = 3;

    /**
     * UTF-8 character set
     */
    private static final Charset utf8 = Charset.forName("UTF-8");

    //~--- fields ---------------------------------------------------------------
    /**
     * The R/W buffer size
     */
    private final int BUFFER_SIZE = (32 * 1024);

    /**
     * Field description
     */
    private int connectAttempts = 0;

    /**
     * Field description
     */
    private String connectedHostAddress = "";

    /**
     * The active connection ID number
     */
    private long connectionId = 0L;

    /**
     * Current send entry
     */
    private SendListEntry currentSending = null;

    /**
     * Field description
     */
    private String myHostIpString = "";

    /**
     * Open Interface connect retry timeout (seconds).
     */
    private int oiConnRetryTimeout = NSDSLinkEthernetImpl.DEFAULT_CONN_RETRY_TIMEOUT;

    /**
     * Field description
     */
    private boolean needToWrite = false;

    /**
     * Field description
     */
    private boolean hostConnected = false;

    /**
     * The network broadcast IP address
     */
    private InetAddress broadcastIpAddress;

    /**
     * Field description
     */
    private SocketChannel client;

    /**
     * The network default gateway address
     */
    private InetAddress gatewayAddress;

    /**
     * The host IP address
     */
    private InetAddress hostIpAddress;

    /**
     * The host port number
     */
    private int hostIpPort;

    /**
     * The link internal state queue
     */
    private final LinkStateQueue internalLinkState;

    /**
     * The link event queue
     */
    private LinkEventQueue linkEventQueue;

    /**
     * The sensor's local IP address for this Ethernet link.
     */
    private InetAddress localIpAddress;

    /**
     * Field description
     */
    private MsgParserUtilities msgParser;

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger;
    /* the log level < debug*/
    private boolean logLevelDebug;

    /**
     * The sensor PKI certificates associated
     */
    private X509Certificate pkiCert;

    /**
     * Secure communications enabled on this link
     */
    private boolean secureComms;

    /**
     * The selector for the server socket
     */
    private Selector selector;

    /**
     * A list of messages to be sent
     */
    private final LinkedBlockingQueue<SendListEntry> sendList;

    /**
     * Our socket channel to the host
     */
    private SocketChannel sockChan;

    /**
     * The selection key for the client socket
     */
    private SelectionKey sockKey;

    /**
     * The selection key for the server socket
     */
    private SelectionKey svrKey;

    /**
     * The server socket channel
     */
    private ServerSocketChannel svrSock;

    /**
     * The host's address
     */
    private InetSocketAddress theHost;

    /**
     * The sensor configuration
     */
    private HostNetworkSettings hostNetworkSettings;    // Singleton

    NSDSConnectionList connectionList;
    /**
     * The base timer utility
     */
    private UtilTimer ut;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructor providing an interface type that default to the TCP/IP Open
     * Interface. If the secure communications flag is set later the type will
     * be changed to TCP/IP secure.
     *
     * @param cfg
     * @param theList
     * @param queues
     * @param timerBase
     */
    public NSDSLinkEthernetImpl(HostNetworkSettings cfg, NSDSConnectionList theList,
            HAQueues queues, UtilTimer timerBase)
    {
        super();
        super.setInterfaceType(NSDSInterfaceTypeEnum.INTFC_TCPIP_OI);
        connectionList = theList;
        this.linkEventQueue = queues.getNSDSLinkEventQueue();
        this.hostNetworkSettings = cfg;
        
        myLogger = Logger.getLogger(NSDSLinkEthernetImpl.class);
        logLevelDebug = myLogger.isDebugEnabled();
        
        this.sendList = new LinkedBlockingQueue<>(10);
        this.internalLinkState = new LinkStateQueue();

        this.theHost = new InetSocketAddress(hostNetworkSettings.getIP(), hostNetworkSettings.getPort());
        msgParser = new MsgParserUtilities(cfg.getClassification());
        ut = timerBase;

        try
        {
            if (this.hostNetworkSettings.isOIServer())
            {
                this.createServerSocket();
            }
        } catch (Exception ex)
        {
            myLogger.fatal("Could not create the server socket IP: " + 
                    hostNetworkSettings.getIP() + " Port: " + hostNetworkSettings.getPort(), ex);
        }

        int connRetryTime = this.hostNetworkSettings.getConnRetryTime();

        if (connRetryTime <= 0)
        {
            connRetryTime = 30;
        }

        this.hostConnected = false;
        this.myHostIpString = "";
        myLogger.info("Constructed...");
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Interrupt this thread
     */
    public void shutDown()
    {
        this.internalLinkState.insertFirst(new LinkStateQueueEntry(NSDSLinkStateCmdEnum.TERMINATE));
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the localIpAddress
     */
    public InetAddress getLocalIpAddress()
    {
        return localIpAddress;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param localIpAddress the localIpAddress to set
     */
    public void setLocalIpAddress(InetAddress localIpAddress)
    {
        this.localIpAddress = localIpAddress;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the broadcastIpAddress
     */
    public InetAddress getBroadcastIpAddress()
    {
        return broadcastIpAddress;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param broadcastIpAddress the broadcastIpAddress to set
     */
    public void setBroadcastIpAddress(InetAddress broadcastIpAddress)
    {
        this.broadcastIpAddress = broadcastIpAddress;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the gatewayAddress
     */
    public InetAddress getGatewayAddress()
    {
        return gatewayAddress;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param gatewayAddress the gatewayAddress to set
     */
    public void setGatewayAddress(InetAddress gatewayAddress)
    {
        this.gatewayAddress = gatewayAddress;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the secureComms
     */
    public boolean isSecureComms()
    {
        return secureComms;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param secureComms the secureComms to set
     */
    public void setSecureComms(boolean secureComms)
    {
        this.secureComms = secureComms;

        if (secureComms)
        {
            super.setInterfaceType(NSDSInterfaceTypeEnum.INTFC_TCPIP_SEC);
        } else
        {
            super.setInterfaceType(NSDSInterfaceTypeEnum.INTFC_TCPIP_OI);
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the pkiCert
     */
    public X509Certificate getPkiCert()
    {
        return pkiCert;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param pkiCert the pkiCert to set
     */
    public void setPkiCert(X509Certificate pkiCert)
    {
        this.pkiCert = pkiCert;
    }

    //~--- methods --------------------------------------------------------------
    /**
     * This method initiates the connection on the link.
     *
     * @param connectId the connection ID for the connection
     *
     * @return an indication of success (true) or failure (false)
     */
    @Override
    public boolean connect(long connectId)
    {
        boolean retVal = false;

        if ((connectId >= 0L) && (getConnList().getItem(connectId) != null))
        {
            this.connectionId = connectId;
            this.internalLinkState.insertLast(new LinkStateQueueEntry(NSDSLinkStateCmdEnum.CONNECT_HOST, connectId));
            retVal = true;
        }

        return (retVal);
    }

    /**
     * This method terminates the connection on the link.
     *
     * @param connectId the connection ID for the disconnect
     *
     * @return an indication of success (true) or failure (false)
     */
    @Override
    public boolean disconnect(long connectId)
    {
        boolean retVal = false;

        if ((connectId >= 1L) && (getConnList().getItem(connectId) != null))
        {
            this.internalLinkState.insertLast(new LinkStateQueueEntry(NSDSLinkStateCmdEnum.DISCONNECT_HOST, connectId));
            retVal = true;
        }

        return (retVal);
    }

    /**
     * This method sends a message to the host system on this link.
     *
     * @param msg String containing the message to be sent
     * @param connectId the ID number of this connection
     * @param itemId
     *
     * @return flag indicating success (true) or failure (false)
     */
    @Override
    public synchronized boolean send(String msg, long connectId, long itemId)
    {
        if ((getConnList().getItem(connectId).getConnType()
                == NSDSConnectionTypeEnum.PERSISTENT_CONNECTION) && (!this.hostConnected))
        {
            myLogger.info("PERSISTENT conn and host is NOT Connected......CONNECTING FIRST.....");
            this.openClientSocket();
        }

        if ((msg != null) && (!msg.trim().isEmpty()) && (connectId >= 1L))
        {
            if (getConnList().getItem(connectId) != null)
            {
                SendListEntry sle = new SendListEntry(msg, connectId, itemId);
                boolean ret = this.sendList.add(sle);

                this.needToWrite = true;

                return (ret);
            } else
            {
                return (false);
            }
        } else
        {
            myLogger.info("Cannot send message.");
            return (false);
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the hostIpAddress
     */
    public InetAddress getHostIpAddress()
    {
        return hostIpAddress;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param hostIpAddress the hostIpAddress to set
     */
    public void setHostIpAddress(InetAddress hostIpAddress)
    {
        this.hostIpAddress = hostIpAddress;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the hostIpPort
     */
    public int getHostIpPort()
    {
        return hostIpPort;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param hostIpPort the hostIpPort to set
     */
    public void setHostIpPort(int hostIpPort)
    {
        this.hostIpPort = hostIpPort;
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Method description
     *
     */
    private void readyConnection()
    {
        if (this.hostNetworkSettings.isOIServer())
        {
            myLogger.info("Initiating connection as a Server...");

            this.enableServerSocket();
        } else
        {
            myLogger.info("Initiating connection as a Client...");

            this.openClientSocket();
        }
    }

    /**
     * Thread execution point
     */
    @Override
    public void run()
    {
        Object syncObject = new Object ();
//        String emptyString = "";
        ByteBuffer theBuffer = ByteBuffer.allocate(this.BUFFER_SIZE);
        boolean doMore = true;
        int readByteCount = 0;
        long timeoutWait = this.oiConnRetryTimeout;

        myLogger.info("Running...");

        try
        {
            while ((doMore) && (!Thread.currentThread().isInterrupted()))
            {
                //
                //  Check for a received message
                //
                if ((this.selector != null) && (this.selector.select(timeoutWait) == 0))
                {
                    if (readByteCount > 0)
                    {
                        theBuffer.flip();

                        String theMessage = utf8.newDecoder().decode(theBuffer).toString();

                        theBuffer.clear();
                        this.sockKey.attach(null);
                        readByteCount = 0;

                        NSDSLinkEvent linkEvent = new NSDSLinkEvent(NSDSLinkEventTypeEnum.LINK_RECEIVED, true);

                        linkEvent.setLinkBuffer(theMessage);    // Package the Message (string) into Link Event to send to Logical Layer.
                        linkEvent.setLinkId(this.getLinkId());    // our Link Id, so Logial Layer knows who it's from.
                        this.linkEventQueue.insertLast(linkEvent);    // Tell Logical Layer we rec'd a msg!
                    }
                }

                //
                //  Process internal link events
                //
                if (this.internalLinkState.size() > 0)
                {
                    LinkStateQueueEntry ent = this.internalLinkState.readFirst(1L);

                    if (ent != null)
                    {
                        myLogger.info("Received entry " + ent.getLinkCmdType().name());

                        // Process OUR (Link Layer) Events.....
                        switch (ent.getLinkCmdType())
                        {
                            case TERMINATE:
                                doMore = false;
                                this.closeConnections();
                                getConnList().getItem(connectionId).setConnType(
                                        NSDSConnectionTypeEnum.NO_CONNECTION);

                                break;

                            case CONNECT_HOST:    // we have an order to CONNECT TO HOST.
                                if (getConnList().getItem(connectionId).getConnType()
                                        == NSDSConnectionTypeEnum.NO_CONNECTION)
                                {
                                    this.readyConnection();
                                } else if (getConnList().getItem(connectionId).getConnType()
                                        == NSDSConnectionTypeEnum.TRANSIENT_CONNECTION)
                                {
                                    this.myLogger.error("ERROR!  Can't re-connect to Host in TRANSIENT.");
                                    this.internalLinkState.insertLast(new LinkStateQueueEntry(NSDSLinkStateCmdEnum.CONN_FAILED,
                                            this.connectionId));
                                }

                                break;

                            case CLOSE_SOCKET:
                                this.closeClientSocket();

                                if (getConnList().getItem(connectionId).getConnType()
                                        != NSDSConnectionTypeEnum.PERSISTENT_CONNECTION)
                                {
                                    getConnList().getItem(connectionId).setConnType(
                                            NSDSConnectionTypeEnum.NO_CONNECTION);

                                    if (this.hostNetworkSettings.isOIServer())
                                    {
                                        this.enableServerSocket();
                                    }
                                } else
                                {
                                    this.enableServerSocket();
                                }

                                myLogger.info(" Close Socket.....Disconnect from Host.");

                                break;

                            case DISCONNECT_HOST:
                                this.closeClientSocket();

                                if (getConnList().getItem(connectionId).getConnType()
                                        == NSDSConnectionTypeEnum.PERSISTENT_CONNECTION)
                                {
                                    this.enableServerSocket();
                                } else if (this.hostNetworkSettings.isOIServer())
                                {
                                    this.enableServerSocket();
                                } else
                                {
                                    getConnList().getItem(connectionId).setConnType(
                                            NSDSConnectionTypeEnum.NO_CONNECTION);
                                }

                                myLogger.info(" Disconnect from Host.");

                                break;

                            case OPEN_SOCKET:
                                myLogger.info(" Open Socket Event.");
                                this.openClientSocket();

                                break;

                            case CONN_FAILED:
                                this.closeConnections();
                                getConnList().getItem(connectionId).setConnType(
                                        NSDSConnectionTypeEnum.NO_CONNECTION);
                                myLogger.info(" CONN FAILED Event: Close all Sockets!");

                                break;

                            case CONN_SUCCESS:
                                if (getConnList().getItem(connectionId).getConnType()
                                        == NSDSConnectionTypeEnum.NO_CONNECTION)
                                {
                                    getConnList().getItem(connectionId).setConnType(
                                            NSDSConnectionTypeEnum.TRANSIENT_CONNECTION);
                                }

                                break;

                            case SOCKET_CLOSED:
                                myLogger.info(" Socket Closed Event.");

                                break;

                            case SOCKET_OPENED:
                                myLogger.info(" Socket Opened Event.");

                                break;

                            case SENSOR_DISCONNECTED:
                                if (getConnList().getItem(this.connectionId) != null)
                                {
                                    if (getConnList().getItem(connectionId).getConnType()
                                            != NSDSConnectionTypeEnum.PERSISTENT_CONNECTION)
                                    {
                                        CcsiMsnType msnGenerator = getConnList().getItem(connectionId).getConnectionMsns();
                                        msnGenerator.initializeMsns();
                                    }
                                }

                                myLogger.info(" Sensor Disconnected Event.");
                                this.internalLinkState.insertLast(new LinkStateQueueEntry(NSDSLinkStateCmdEnum.CONNECT_HOST,
                                        connectionId));

                                break;

                            case NONE:
                                myLogger.info(" No Event.");

                                break;

                            default:
                                break;
                        }
                    }
                } else if (selector != null)
                {
                    try
                    {
                    if (needToWrite)
                    {
                        needToWrite = false;
                        this.sockKey.interestOps(this.sockKey.interestOps() | SelectionKey.OP_WRITE);
                    }

                    // Something is ready
                    Iterator<SelectionKey> keyIter = selector.selectedKeys().iterator();

                    while (keyIter.hasNext())
                    {
                        SelectionKey key = keyIter.next();

                        if ((key.isAcceptable()) && (key.isValid()))
                        {
                            client = null;

                            try
                            {
                                client = this.handleAccept(key);
                            } catch (Exception accEx)
                            {
                                myLogger.error("Exception during accept operation.", accEx);
                            }

                            if (client != null)
                            {
                                SocketAddress xxx = client.getRemoteAddress();

                                this.connectedHostAddress = ((InetSocketAddress) xxx).getHostString();
                                this.sockChan = client;

                                if (this.sendList.isEmpty())
                                {
                                    this.sockKey = sockChan.register(this.selector, SelectionKey.OP_READ);
                                } else
                                {
                                    this.sockKey = sockChan.register(this.selector, SelectionKey.OP_WRITE);
                                }
                            } else
                            {
                                myLogger.info("Did not accept the connection.");
                            }
                        } else if ((key.isConnectable()) && (key.isValid()))
                        {
                            try
                            {
                                this.sockChan = this.handleConnect(key);
                            } catch (Exception connEx)
                            {
                                myLogger.error("Exception during connection operations.", connEx);

                                if (this.sockChan != null)
                                {
                                    this.sockChan.close();
                                    this.sockChan = null;
                                }
                            }

                            if (this.sockChan == null)
                            {
                                myLogger.error("Error connecting client socket to sensor.");
                            } else
                            {
                                if (this.sendList.isEmpty())
                                {
                                    this.sockKey = sockChan.register(this.selector, SelectionKey.OP_READ);
                                } else
                                {
                                    this.sockKey = sockChan.register(this.selector, SelectionKey.OP_WRITE);
                                }
                            }
                        } else if ((key.isReadable()) && (key.isValid()))
                        {
                            try
                            {
                                readByteCount = this.handleRead(key, theBuffer);
                            } catch (Exception readEx)
                            {
                                myLogger.error("Exception during read operation.", readEx);
                                readByteCount = -1;
                            }

                            if (readByteCount == -1)
                            {
                                myLogger.info("Socket closed during read.");

                                if (getConnList().getItem(connectionId).getConnType()
                                        == NSDSConnectionTypeEnum.PERSISTENT_CONNECTION)
                                {
                                    this.internalLinkState.insertLast(
                                            new LinkStateQueueEntry(NSDSLinkStateCmdEnum.SENSOR_DISCONNECTED));
                                }
                                this.internalLinkState.insertLast(new LinkStateQueueEntry(NSDSLinkStateCmdEnum.CLOSE_SOCKET));
                                myLogger.info("Closing out the socket.");
                            }
                        } else if ((key.isWritable()) && (key.isValid()))
                        {
                            try
                            {
                                this.handleWrite(key);
                            } catch (Exception writEx)
                            {
                                myLogger.error("Exception during write operation.", writEx);

                                if ((key.isValid()) && (key.isWritable()))
                                {
                                    key.interestOps(0);
                                }
                            }
                        }

                        keyIter.remove();
                    }
                    }
                    catch (CancelledKeyException ex)
                    {
                        // happens when a connection has been closed and an attempt to access the selector occurs
                        myLogger.error("Cancelled Key Exception.", ex);
                    }
                } else
                {
                    try
                    {
                        synchronized (syncObject)
                        {
                            syncObject.wait(2000L);
                        }
                    } catch (InterruptedException intrEx)
                    {
                        myLogger.error("Interrupted Exception", intrEx);
                    }
                }
            }
        } catch (Exception ex)
        {
            myLogger.error("Exception in main loop", ex);
        } finally
        {
            // close out all sockets, selectors, and keys
            try
            {
                myLogger.error("Main loop shutting down.");

                if (selector != null)
                {
                    selector.close();
                }

                if (sockKey != null)
                {
                    sockKey.cancel();
                    sockKey = null;
                }

                if (svrKey != null)
                {
                    svrKey.cancel();
                    svrKey = null;
                }

                if (svrSock != null)
                {
                    svrSock.close();
                    svrSock = null;
                }

                if (sockChan != null)
                {
                    sockChan.close();
                    sockChan = null;
                }

            } catch (Exception e)
            {
                myLogger.error("Error closing sockets in this.closeConnections!", e);
            } finally
            {
                this.hostConnected = false;
                this.myHostIpString = "";
                this.needToWrite = false;
                this.sendList.clear();                
            }
        }
    }

    /**
     * Handle an accept connection selector trigger indicating that a server
     * socket is ready to accept another connection or has an error pending.
     *
     * @param key selection key for the accept operation
     *
     * @return SocketChannel for the client connection that was accepted or null
     * if error
     *
     * @throws IOException
     */
    private synchronized SocketChannel handleAccept(SelectionKey key) throws IOException
    {
        // The client socket channel
        SocketChannel myclient = ((ServerSocketChannel) key.channel()).accept();

        if ((myclient != null) && (myclient.getRemoteAddress() != null))
        {
            SocketAddress xxx = myclient.getRemoteAddress();

            this.connectedHostAddress = ((InetSocketAddress) xxx).getHostString();
            myclient.configureBlocking(false);

            // The connecting client's network address
            InetAddress clientAddr = ((InetSocketAddress) myclient.getRemoteAddress()).getAddress();
            NSDSConnection conn = getConnList().getItem(this.connectionId);

            myLogger.info("Connect from " + clientAddr.getHostAddress());

            switch (conn.getConnType())
            {
                case PERSISTENT_CONNECTION:

                    // See if this is the same sensor
                    if ((clientAddr.getHostAddress().equalsIgnoreCase(hostNetworkSettings.getIP().getHostAddress()))
                            || (clientAddr.getHostAddress().equalsIgnoreCase(this.myHostIpString)))
                    {
                        // It is the same one, are we already connected?
                        if (this.sockChan != null)
                        {
                            // We are, close the connection
                            myclient.close();
                            myclient = null;
                            this.connectedHostAddress = "";
                            myLogger.error("Connection attempted from host with an existing connection");
                        } else
                        {
                            key.interestOps(SelectionKey.OP_READ);
                            this.hostConnected = true;
                            myLogger.info("Connection re-established with " + clientAddr.getHostAddress());
                            this.linkEventQueue.insertLast(new NSDSLinkEvent(true, this.getLinkId(),
                                    NSDSLinkEventTypeEnum.LINK_CONNECTED));
                        }
                    } else if (this.sockChan != null)
                    {
                        myclient.close();
                        myclient = null;
                        this.connectedHostAddress = "";
                        myLogger.error("Too many hosts, closed socket.");
                    }

                    break;

                case TRANSIENT_CONNECTION:

                    // We're already connected to a sensor for a transient connection, close this one
                    myclient.close();
                    myclient = null;
                    this.connectedHostAddress = "";
                    myLogger.error("Connection attempt from a different sensor while transient connection active.");

                    break;

                default:
                    this.disableServerSocket();                          // we need to disable any (additional) ACCEPTS so that
                    myLogger.info("New connection from host " + clientAddr.getHostAddress());
                    this.hostConnected = true;
                    this.myHostIpString = clientAddr.getHostAddress();
                    this.linkEventQueue.insertLast(new NSDSLinkEvent(true, this.getLinkId(),
                            NSDSLinkEventTypeEnum.LINK_CONNECTED));    // tell Logical Layer we are LINK CONNECTED! Yay!

                    break;
            }    // End switch(conn type)
        } else if (myclient != null)
        {
            // The client address was null, close the connection socket
            myclient.close();
            myclient = null;
            this.connectedHostAddress = "";
            myLogger.error("Accepted socket had no client address.");
        }

        return (myclient);
    }

    /**
     * Handle a read ready selector trigger indicating that a channel is ready
     * for reading, has reached the end of stream, or has an error pending.
     *
     * @param key selection key for the read
     * @param buf a byte buffer to use to hold the read bytes
     *
     * @return the number of bytes read or -1 if an exception or socket closure
     * occurred
     * @throws IOException
     */
    private synchronized int handleRead(SelectionKey key, ByteBuffer buf) throws IOException
    {
        int bytesRead = 0;

        if ((key.isValid()) && (key.isReadable()))
        {
            if (key.attachment() == null)
            {
                buf.clear();
                key.attach(buf);
            }

            try
            {
                bytesRead = ((SocketChannel) key.channel()).read(buf);
            } catch (Exception readEx)
            {
                key.interestOps(0);
                key.channel().close();
                myLogger.error("Error during read.", readEx);
                bytesRead = -1;
            }

            if (bytesRead == -1)
            {
                key.attach(null);
                key.interestOps(0);
                key.channel().close();
                this.hostConnected = false;
                this.myHostIpString = "";

                if (getConnList().getItem(this.connectionId).getConnType()
                        != NSDSConnectionTypeEnum.PERSISTENT_CONNECTION)
                {
                    this.sendList.clear();
                    this.needToWrite = false;
                }

                this.linkEventQueue.insertLast(new NSDSLinkEvent(true, this.getLinkId(),
                        NSDSLinkEventTypeEnum.LINK_DISCONNECTED));
            }
        }

        return (bytesRead);
    }

    /**
     * Handle a write ready selector trigger indicating that a channel is ready
     * for writing, has been closed for writing, or has an error pending.
     *
     * @param key selection key for the write
     *
     * @throws IOException
     */
    private void handleWrite(SelectionKey key) throws IOException
    {
        ByteBuffer msg = null;

        if ((key.isValid()) && (key.isWritable()))
        {
            if (key.attachment() == null)
            {
                if (this.sendList.peek() != null)
                {
                    this.currentSending = this.sendList.poll();    // Read and Remove Send Item.

                    String sendStr = this.currentSending.sendBuffer;

                    // fill in the MSN and DTG
                    if (getConnList().getItem(this.connectionId) != null)
                    {
                        CcsiMsnType msnGenerator = getConnList().getItem(connectionId).getConnectionMsns();

                        if (this.currentSending.sendBuffer.contains("{msgMsn}"))
                        {
                            long theMsn = msnGenerator.getNextTransmitMsn();

                            this.currentSending.sentMsn = theMsn;

                            String sMsn = String.valueOf(theMsn);

                            sendStr = sendStr.replace("{msgMsn}", sMsn);
                        }

                        if (this.currentSending.sendBuffer.contains("{msgDtg}"))
                        {
                            long sysTime = System.currentTimeMillis() / 1000;
                            String sTime = String.valueOf(sysTime);

                            sendStr = sendStr.replace("{msgDtg}", sTime);
                        }
                    }

                    myLogger.info("Writing msg: " + sendStr);
                    msg = ByteBuffer.wrap(sendStr.getBytes(utf8));
                    key.attach(msg);
                } else
                {
                    key.interestOps(SelectionKey.OP_READ);

                    return;
                }
            } else
            {
                msg = (ByteBuffer) key.attachment();
            }

            ((SocketChannel) key.channel()).write(msg);

            if (!msg.hasRemaining())
            {
                if (logLevelDebug)
                {
                    myLogger.debug("Complete write of message.");
                }
                if (this.sendList.isEmpty())
                {
                    key.interestOps(SelectionKey.OP_READ);
                } else
                {
                    key.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                }

                key.attach(null);

                NSDSLinkEvent x = new NSDSLinkEvent(true, this.getLinkId(), NSDSLinkEventTypeEnum.LINK_SEND_OK);

                x.setItemId(this.currentSending.itemId);
                x.setSentMsn(this.currentSending.sentMsn);
                this.currentSending = null;
                key.attach(null);
                this.linkEventQueue.insertLast(x);
            } else
            {
                msg.compact();
            }
        }
    }

    /**
     * Handle a connect ready selector trigger indicating that a channel is
     * ready to complete its connection sequence or has an error pending.
     *
     * @param key selection key for the connect
     *
     * @return the socket channel for the connection or null if none
     */
    private synchronized SocketChannel handleConnect(SelectionKey key)
    {
        SocketChannel theChan = null;

        if (key.isConnectable())
        {
            theChan = (SocketChannel) key.channel();

            try
            {
                while (!theChan.finishConnect())
                {
                }

                this.hostConnected = true;

                StringBuilder msg = new StringBuilder("Client socket setup for ");

                msg.append(((InetSocketAddress) theChan.getRemoteAddress()).getHostString());
                myLogger.info(msg.toString());
                this.linkEventQueue.insertLast(new NSDSLinkEvent(true, this.getLinkId(),
                        NSDSLinkEventTypeEnum.LINK_CONNECTED));
            } catch (IOException ioEx)
            {
                myLogger.error("Error making connection to host.", ioEx);
                theChan = null;
                this.hostConnected = false;
                this.myHostIpString = "";

                if (getConnList().getItem(this.connectionId).getConnType()
                        != NSDSConnectionTypeEnum.PERSISTENT_CONNECTION)
                {
                    this.sendList.clear();
                    this.needToWrite = false;
                }

                if (this.connectAttempts < this.MAX_CONNECT_ATTEMPTS)
                {
                    long myTimer = ut.setNewTimer(this.oiConnRetryTimeout, false, this, null, "MyLinkTimer",
                            TimerPurposeEnum.CONNRETRY_TIMER);

                    ut.addUtilTimerListener(this, myTimer, TimerChangeEvent.TEST_TIMER_EXPIRED);
                }

                this.connectAttempts++;
            }
        }

        return (theChan);
    }

    /**
     * Handle connect retry timer expiration
     *
     * @param e the event
     */
    @Override
    public void timerExpired(TimerChangeEvent e)
    {
        myLogger.info("Connect Timer Expired!    Retrying connection attempt # " + this.connectAttempts);
        this.openClientSocket();
    }

    /**
     * Open a client socket connection to the sensor.
     */
    private synchronized void openClientSocket()
    {
        int i = 0;

        try
        {
            if ((this.sockChan != null) && (this.sockKey != null))
            {
                this.sockChan.close();
                this.sockKey.cancel();
                this.sockChan = null;
                this.sockKey = null;
            }

            if (this.selector == null)
            {
                this.selector = Selector.open();
            }

            this.sockChan = SocketChannel.open();
            this.sockChan.configureBlocking(false);
            this.sockChan.setOption(StandardSocketOptions.SO_KEEPALIVE, true);
            this.sockChan.setOption(StandardSocketOptions.SO_SNDBUF, 32768);
            this.sockChan.setOption(StandardSocketOptions.SO_RCVBUF, 32768);
            this.sockKey = this.sockChan.register(this.selector, SelectionKey.OP_CONNECT);

            try
            {
                this.sockChan.connect(this.theHost);
            } catch (Exception e)
            {
                myLogger.error("Exception connecting to host", e);
            }
        } catch (Exception ex)
        {
            myLogger.error("Exception opening client socket.", ex);
        }
    }

    /**
     * Create a server socket
     *
     * @throws IOException
     */
    private void createServerSocket() throws IOException
    {
        myLogger.info("Creating server socket.");
        this.svrSock = ServerSocketChannel.open();
        this.svrSock.configureBlocking(false);
        this.svrSock.setOption(StandardSocketOptions.SO_REUSEADDR, true);
        this.svrSock.bind(new InetSocketAddress(this.hostNetworkSettings.getPHIP(), 
                this.hostNetworkSettings.getPHPort()), 1);
    }

    /**
     * Disable the server socket
     */
    private void disableServerSocket()
    {
        myLogger.info("Disabling server socket.");

        try
        {
            if (this.svrSock != null)
            {
                if (this.svrKey != null)
                {
                    this.svrKey.interestOps(0);
                }
            }
        } catch (Exception ex)
        {
            myLogger.error("Exception disabling server socket.", ex);
        }
    }

    /**
     * Open a server socket to listen for incoming connections.
     */
    private void enableServerSocket()
    {
        myLogger.info("Enabling Server Socket.");

        try
        {
            if (this.svrSock != null)
            {
                if (this.selector == null)
                {
                    this.selector = Selector.open();
                }

                if (this.svrKey == null)
                {
                    this.svrKey = this.svrSock.register(selector, SelectionKey.OP_ACCEPT);
                } else
                {
                    this.svrKey.interestOps(SelectionKey.OP_ACCEPT);
                }
            } else
            {
                if (this.selector == null)
                {
                    this.selector = Selector.open();
                }

                this.createServerSocket();
                this.svrKey = this.svrSock.register(selector, SelectionKey.OP_ACCEPT);
            }

            myLogger.info("Our Server Sock is: " + svrSock.getLocalAddress().toString());
            myLogger.info("Our Server Key is Acceptable?: " + svrKey.isAcceptable());
            myLogger.info("Our Selector is Open?: " + selector.isOpen());
            myLogger.info("Connectable: " + svrKey.isConnectable());
            myLogger.info("Readable: " + svrKey.isReadable());
            myLogger.info("Writable: " + svrKey.isWritable());
            myLogger.info("Key: " + svrKey.toString());
        } catch (Exception ex)
        {
            myLogger.error("Exception enabling server socket.", ex);
        }
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Handler for a new internal link state queue event
     *
     * @param evt the queue event
     */
    @Override
    public void queueEventFired(IPCQueueEvent evt)
    {
        if (evt.getEventType() == IPCQueueEvent.IPCQueueEventType.IPC_NEW)
        {
            if (logLevelDebug)
            {
                myLogger.debug("Got new link state queue entry.");
            }
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the theSensorConfig
     */
    public HostNetworkSettings getTheSensorConfig()
    {
        return hostNetworkSettings;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param hostNetworkSettings
     */
    public void setTheSensorConfig(HostNetworkSettings hostNetworkSettings)
    {
        this.hostNetworkSettings = hostNetworkSettings;
    }

    //~--- methods --------------------------------------------------------------
    /**
     * Close all socket related connection information
     */
    public synchronized void closeConnections()
    {
        try
        {
            myLogger.info("Closing all connections.");

            if (selector != null)
            {
                selector.close();
            }

            if (sockKey != null)
            {
                sockKey.cancel();
                sockKey = null;
            }

            if (svrKey != null)
            {
                svrKey.cancel();
                svrKey = null;
            }

            if (svrSock != null)
            {
                svrSock.close();
            }

            svrSock = null;

            if (sockChan != null)
            {
                sockChan.close();
                sockChan = null;
            }

            this.hostConnected = false;
            this.myHostIpString = "";
            this.sendList.clear();
            this.needToWrite = false;
        } catch (Exception e)
        {
            this.hostConnected = false;
            this.myHostIpString = "";
            this.sendList.clear();
            this.needToWrite = false;
            myLogger.error("Error closing sockets in this.closeConnections!", e);
        }
    }

    /**
     * Close the client socket
     */
    private synchronized void closeClientSocket()
    {
        try
        {
            sockChan.close();
            this.hostConnected = false;
            this.myHostIpString = "";

            if (getConnList().getItem(this.connectionId).getConnType()
                    != NSDSConnectionTypeEnum.PERSISTENT_CONNECTION)
            {
                this.sendList.clear();
                this.needToWrite = false;
            }

            this.linkEventQueue.insertLast(new NSDSLinkEvent(true, this.getLinkId(),
                    NSDSLinkEventTypeEnum.LINK_DISCONNECTED));
        } catch (Exception e)
        {
            this.hostConnected = false;
            this.myHostIpString = "";

            if (getConnList().getItem(this.connectionId).getConnType()
                    != NSDSConnectionTypeEnum.PERSISTENT_CONNECTION)
            {
                this.sendList.clear();
                this.needToWrite = false;
            }

            myLogger.error("Error closing sockets in this.closeSockets!", e);
        }
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the linkEventQueue
     */
    public LinkEventQueue getLinkEventQueue()
    {
        return linkEventQueue;
    }

    /**
     * Get the connected host's address
     *
     * @param connectId which connection
     *
     * @return the address
     */
    @Override
    public String getConnectHostAddress(long connectId)
    {
        return (this.connectedHostAddress);
    }

    private NSDSConnectionList getConnList ()
    {
        return connectionList;
    }
    
    //~--- inner classes --------------------------------------------------------
    /**
     * Container for a send list buffer entry
     */
    class SendListEntry
    {

        /**
         * The connection to send on
         */
        long connId = 0L;

        /**
         * The logical connection item ID, if it is < 0 then it's internally
         * generated
         */
        long itemId = -1L;

        /**
         * The message to be sent
         */
        String sendBuffer = null;

        /**
         * The MSN that was sent
         */
        long sentMsn = -1L;

        //~--- constructors -------------------------------------------------------
        /**
         * Constructs an entry
         *
         * @param buf the message to send
         * @param id the connection ID
         * @param item
         */
        public SendListEntry(String buf, long id, long item)
        {
            this.connId = id;
            this.sendBuffer = buf;
            this.itemId = item;
        }
    }
}
