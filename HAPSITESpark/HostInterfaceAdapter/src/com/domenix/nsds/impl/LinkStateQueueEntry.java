/*
 * LinkStateQueueEntry.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of a class for queuing state change commands/results.
 *
 * Version: V1.0  15/08/17
 */


package com.domenix.nsds.impl;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Encapsulates a link state change command/result for subsequent processing by the
 * the link implementation.
 * 
 * @author kmiller
 */
public class LinkStateQueueEntry
{
  /** Serialization UID */
  private static final long	serialVersionUID	= 1L;

  //~--- fields ---------------------------------------------------------------

  /** The connection ID to operate on */
  private long	connectionId	= -1L;

  /** The command/result */
  private NSDSLinkStateCmdEnum	linkCmdType	= NSDSLinkStateCmdEnum.NONE;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an entry instance with a command type
   *
   * @param cmdType the state command/result
   */
  public LinkStateQueueEntry( NSDSLinkStateCmdEnum cmdType )
  {
    this.linkCmdType = cmdType;
    
  }
  
  /**
   * Constructs an entry instance with a command type
   *
   * @param cmdType the state command/result
   * @param connId the connection ID
   */
  public LinkStateQueueEntry( NSDSLinkStateCmdEnum cmdType , long connId )
  {
    this.linkCmdType = cmdType;
    this.connectionId = connId;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the linkCmdType
   */
  public NSDSLinkStateCmdEnum getLinkCmdType()
  {
    return linkCmdType;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param linkCmdType the linkCmdType to set
   */
  public void setLinkCmdType( NSDSLinkStateCmdEnum linkCmdType )
  {
    this.linkCmdType	= linkCmdType;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the connectionId
   */
  public long getConnectionId()
  {
    return connectionId;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param connectionId the connectionId to set
   */
  public void setConnectionId( long connectionId )
  {
    this.connectionId	= connectionId;
  }
}
