/*
 * NSDSXferSentReport.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/29
 */


package com.domenix.nsds.intfc;

import com.domenix.commonha.enums.NSDSLinkEventTypeEnum;
import com.domenix.commonha.intfc.NSDSLinkEvent;
import com.domenix.commonha.ccsi.CcsiChannelEnum;

/**
 * This class is the container for an NSDS transfer sent report event.
 *
 * @author kmiller
 */
public class NSDSXferRECVReport extends NSDSLinkEvent
{
  /** The cache keys of the sent messages */
  private String[]	cacheKeys;

  /** The channels that were sent */
  private CcsiChannelEnum[]	channels;
  
  /** The MSN of the NAK or ACK */
  private long eventMsn = -1L;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance for dispatch to listeners
   *
   */
  public NSDSXferRECVReport()
  {
    super( NSDSLinkEventTypeEnum.LINK_SEND_OK , true );
    this.cacheKeys	= new String[12];
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the sentMsn
   */
  public long getEventMsn()
  {
    return( super.getSentMsn() );
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param eventMsn the MSN of to set
   */
  public void setEventMsn( long eventMsn )
  {
    this.eventMsn	= eventMsn;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the channels
   */
  public CcsiChannelEnum[] getChannels()
  {
    if (channels != null)
    {
        return (CcsiChannelEnum[]) channels.clone();
    } else
    {
        return new CcsiChannelEnum [0];
    }
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param channels the channels to set
   */
  public void setChannels( CcsiChannelEnum[] channels )
  {
    if (channels != null)
    {
        this.channels = (CcsiChannelEnum[]) channels.clone();
    } else
    {
        this.channels = null;
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the cacheKeys
   */
  public String[] getCacheKeys()
  {
    String [] retVal = null;
    if (cacheKeys != null)
    {
        retVal = (String[])cacheKeys.clone ();
    } 
    return retVal;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param cacheKeys the cacheKeys to set
   */
  public void setCacheKeys( String[] cacheKeys )
  {
    if (cacheKeys != null)
    {
        this.cacheKeys = (String[])cacheKeys.clone ();
    } else
    {
        this.cacheKeys = null;
    }
  }
}
