/*
 * NSDSRegistrationEvent.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of an NSDS registration event that is dispatched to registered
 * listeners.
 *
 * Version: V1.0  15/08/01
 */


package com.domenix.nsds.intfc;

import com.domenix.commonha.enums.NSDSLinkEventTypeEnum;
import com.domenix.commonha.intfc.NSDSLinkEvent;

/**
 * This defines an NSDS registration event that can be dispatched to notify listeners.
 * 
 * @author kmiller
 */
public class NSDSRegistrationEvent extends NSDSLinkEvent
{
  /** The type of event. */
  private NSDSRegistrationTypeEnum	regEventType	= NSDSRegistrationTypeEnum.REG_NOT_REGISTERED;

  /** The ID of the link that was the source of the event. */
  private long	regNetworkLinkId	= -1L;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an event with a source.
   *
   * @param type the event type
   */
  public NSDSRegistrationEvent( NSDSLinkEventTypeEnum type )
  {
    super( type , true );
  }

  /**
   * Constructs an event with a source and status.
   *
   * @param type the event type
   * @param status the event status
   */
  public NSDSRegistrationEvent( NSDSLinkEventTypeEnum type , boolean status )
  {
    super( type , status );
  }

  /**
   * Constructs an event with a source, status, and link identification number.
   *
   * @param type the event type
   * @param status the event status
   * @param linkId the ID number of the link for this event
   */
  public NSDSRegistrationEvent( NSDSLinkEventTypeEnum type , boolean status , long linkId )
  {
    super( type , status );
    this.regNetworkLinkId	= linkId;
  }

  /**
   * Constructs an event with a source, status, link ID number, and event type.
   *
   * @param status the event status
   * @param linkId the ID number of the link for this event
   * @param regType - registered, de-registered or not registered
   */
  public NSDSRegistrationEvent( boolean status , long linkId , NSDSRegistrationTypeEnum regType )
  {
    super( NSDSLinkEventTypeEnum.LINK_REGISTERED , status );
    this.regNetworkLinkId	= linkId;
    this.regEventType			= regType;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the regEventType
   */
  public NSDSRegistrationTypeEnum getRegEventType()
  {
    return regEventType;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param regEventType the regEventType to set
   */
  public void setRegEventType( NSDSRegistrationTypeEnum regEventType )
  {
    this.regEventType	= regEventType;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the regNetworkLinkId
   */
  public long getRegNetworkLinkId()
  {
    return regNetworkLinkId;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param regNetworkLinkId the regNetworkLinkId to set
   */
  public void setRegNetworkLinkId( long regNetworkLinkId )
  {
    this.regNetworkLinkId	= regNetworkLinkId;
  }
}
