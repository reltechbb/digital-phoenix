/*
 * NSDSXferRecvHrtbt.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/29
 */


package com.domenix.nsds.intfc;

import com.domenix.commonha.enums.NSDSLinkEventTypeEnum;
import com.domenix.commonha.intfc.NSDSLinkEvent;

/**
 * This class is the container for an NSDS transfer received heartbeat event.
 *
 * @author kmiller
 */
public class NSDSXferRecvHrtbt extends NSDSLinkEvent
{
  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance for dispatch to listeners
   *
   */
  public NSDSXferRecvHrtbt()
  {
    super( NSDSLinkEventTypeEnum.LINK_RECEIVED );
    super.setEventStatus( true );
  }
}
