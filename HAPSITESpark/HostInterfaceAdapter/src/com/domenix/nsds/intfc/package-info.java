/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * Provides NSDS related types for interface elements involving connections to host systems,
 *  communication links, and events,
 *
 */
package com.domenix.nsds.intfc;