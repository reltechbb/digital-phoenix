/*
 * NSDSXferRecvCmd.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/29
 */


package com.domenix.nsds.intfc;

import com.domenix.commonha.enums.NSDSLinkEventTypeEnum;
import com.domenix.commonha.intfc.NSDSLinkEvent;

/**
 * This class is the container for an NSDS transfer received command event.
 *
 * @author kmiller
 */
public class NSDSXferSENDCmd extends NSDSLinkEvent
{
  /** The flags that were received */
  private String	rcvdFlags	= "";

  /** The body of the message */
  private String	recvdBody	= null;

  /** The DTG of the received command */
  private long	recvdDtg	= -1L;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance for dispatch to listeners
   *
   */
  public NSDSXferSENDCmd()
  {
    super( NSDSLinkEventTypeEnum.LINK_RECEIVED , true );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the eventMsn
   */
  public long getRecvdMsn()
  {
    return super.getSentMsn();
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param eventMsn the eventMsn to set
   */
  public void setRecvdMsn( long eventMsn )
  {
    super.setSentMsn( eventMsn );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the rcvdFlags
   */
  public String getRcvdFlags()
  {
    return rcvdFlags;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param rcvdFlags the rcvdFlags to set
   */
  public void setRcvdFlags( String rcvdFlags )
  {
    this.rcvdFlags	= rcvdFlags;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the recvdDtg
   */
  public long getRecvdDtg()
  {
    return recvdDtg;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param recvdDtg the recvdDtg to set
   */
  public void setRecvdDtg( long recvdDtg )
  {
    this.recvdDtg	= recvdDtg;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the recvdBody
   */
  public String getRecvdBody()
  {
    return recvdBody;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param recvdBody the recvdBody to set
   */
  public void setRecvdBody( String recvdBody )
  {
    this.recvdBody	= recvdBody;
  }
}
