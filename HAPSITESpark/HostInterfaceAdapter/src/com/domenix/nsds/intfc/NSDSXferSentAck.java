/*
 * NSDSXferSentAck.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/09/29
 */
package com.domenix.nsds.intfc;

import com.domenix.commonha.enums.NSDSLinkEventTypeEnum;
import com.domenix.commonha.intfc.NSDSLinkEvent;
import com.domenix.commonha.ccsi.CcsiChannelEnum;

/**
 * This class is the container for an NSDS transfer sent ACK event.
 *
 * @author kmiller
 */
public class NSDSXferSentAck extends NSDSLinkEvent
{

    /**
     * The channel that was ACKed/NAKed
     */
    private CcsiChannelEnum ackChn[];

    /**
     * The MSN that was ACKed/NAKed
     */
    private long ackMsn = -1L;

    //~--- constructors ---------------------------------------------------------
    /**
     * Constructs a class instance for dispatch to listeners
     *
     * @param source the source object (usually the 'this' of the class
     * constructing the class).
     */
    public NSDSXferSentAck(Object source)
    {
        super(NSDSLinkEventTypeEnum.LINK_SEND_OK, true);
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the ackMsn
     */
    public long getAckMsn()
    {
        return ackMsn;
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param ackMsn the ackMsn to set
     */
    public void setAckMsn(long ackMsn)
    {
        this.ackMsn = ackMsn;
    }

    //~--- get methods ----------------------------------------------------------
    /**
     * @return the ackChn
     */
    public CcsiChannelEnum[] getAckChn()
    {
        if (ackChn != null)
        {
            return (CcsiChannelEnum[]) ackChn.clone();
        } else
        {
            return new CcsiChannelEnum [0];
        }
    }

    //~--- set methods ----------------------------------------------------------
    /**
     * @param ackChn the ackChn to set
     */
    public void setAckChn(CcsiChannelEnum[] ackChn)
    {
        if (ackChn != null)
        {
            this.ackChn = (CcsiChannelEnum[]) ackChn.clone();
        } else
        {
           this.ackChn = null; 
        }
    }
}
