/*
 * NSDSXferRecvAck.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/29
 */


package com.domenix.nsds.intfc;

import com.domenix.commonha.enums.NSDSLinkEventTypeEnum;
import com.domenix.commonha.intfc.NSDSLinkEvent;
import com.domenix.commonha.ccsi.CcsiChannelEnum;

/**
 * This class is the container for an NSDS transfer received ack event.
 *
 * @author kmiller
 */
public class NSDSXferRecvAck extends NSDSLinkEvent
{
  /** The channels in the ACK message */
  private CcsiChannelEnum[]	ackChannels = null;

  /** The MSN that was ACKed/NAKed */
  private long	ackMsn	= -1L;

  /** The NAK code if present */
  private String	nakCode	= null;

  /** The type of message is an ACK */
  private boolean	ackFlag	= true;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a class instance for dispatch to listeners
   *
   */
  public NSDSXferRecvAck()
  {
    super( NSDSLinkEventTypeEnum.LINK_RECEIVED , true );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the ackMsn
   */
  public long getAckMsn()
  {
    return ackMsn;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param ackMsn the ackMsn to set
   */
  public void setAckMsn( long ackMsn )
  {
    this.ackMsn	= ackMsn;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the ackChannels
   */
    public CcsiChannelEnum[] getAckChannels()
    {
        if (ackChannels != null)
        {
            return (CcsiChannelEnum[]) ackChannels.clone();
        } else
        {
            return new CcsiChannelEnum [0];
        }
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param ackChannels the ackChannels to set
   */
  public void setAckChannels( CcsiChannelEnum[] ackChannels )
  {
    if (ackChannels != null)
    {
        this.ackChannels = (CcsiChannelEnum[])ackChannels.clone();
    } else
    {
        this.ackChannels = null;
    }
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the nakCode
   */
  public String getNakCode()
  {
    return nakCode;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param nakCode the nakCode to set
   */
  public void setNakCode( String nakCode )
  {
    this.nakCode	= nakCode;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the ackFlag
   */
  public boolean isAckFlag()
  {
    return ackFlag;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param ackFlag the ackFlag to set
   */
  public void setAckFlag( boolean ackFlag )
  {
    this.ackFlag	= ackFlag;
  }
}
