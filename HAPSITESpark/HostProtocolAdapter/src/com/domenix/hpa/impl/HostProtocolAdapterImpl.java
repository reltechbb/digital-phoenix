/*
 * HostProtocolAdapterImpl.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains an implementation of the HostProtocolAdapterInterface
 * for the Host Protocol Adapter.
 *
 * 
 */
package com.domenix.hpa.impl;

//import com.domenix.ccsi.config.CcsiSavedState;
import com.domenix.commonha.intfc.NSDSInterface;
import com.domenix.common.spark.CtlWrapper;
import com.domenix.common.spark.CtlWrapperQueue;
import com.domenix.common.spark.IPCQueueEvent;
import com.domenix.common.spark.IPCQueueListenerInterface;
import com.domenix.commonha.ccsi.CcsiAckNakEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.commonha.intfc.NSDSConnEvent;
import com.domenix.commonha.queues.ConnEventQueue;
import com.domenix.commonha.intfc.NSDSTransferEvent;
import com.domenix.commonha.queues.HAQueues;
import com.domenix.commonha.queues.NSDSXferEventQueue;
import com.domenix.commonha.intfc.HostProtocolAdapterInterface;
import com.domenix.commonha.intfc.NSDSConnectionList;

import java.util.concurrent.Executor;

import org.apache.log4j.Logger;

/**
 * This file contains an implementation of the HostProtocolAdapterInterface for
 * the Host Protocol Adapter.
 *
 * @author jmerritt
 */
public class HostProtocolAdapterImpl implements HostProtocolAdapterInterface,
        IPCQueueListenerInterface, Runnable
{

    /**
     * Receive control messages from the Protocol Handler
     */
    private CtlWrapperQueue hpaCtlQueue;

    /**
     * Send control messages to the Host Interface Adapter
     */
    private CtlWrapperQueue nsdsCtlQueue;
    
    /**
     * Send received messages to the Protocol Handler
     */
    private NSDSXferEventQueue hpaXferEventQueue;

    /**
     * Receive messages from the Host Interface
     */
    private NSDSXferEventQueue hiaXferEventQueue;

    /**
     * Send connection events to the Protocol Handler
     */
    private ConnEventQueue hpaConnEventQueue;

    /**
     * Receive connection events from the Host Interface Adapter
     */
    private ConnEventQueue hiaConnEventQueue;

    /**
     * The configuration
     */
    private HostNetworkSettings theConfig;

    /**
     * The Host Interface Adapter
     */
    private NSDSInterface hostInterfaceAdapter;

    /**
     * the saved state
     */
//    private CcsiSavedState theState;

    /**
     * The Host Protocol Adapter should be shutdown
     */
    private boolean shutDown = false;

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger;

    public HostProtocolAdapterImpl()
    {
        myLogger = Logger.getLogger(HostProtocolAdapterImpl.class);
    }

    @Override
    public void initialize(HAQueues queues, HostNetworkSettings config,
            NSDSInterface nsdsInterface, NSDSConnectionList connectionList)
    {
        // connectionList is not used by this implementation
        hpaCtlQueue = queues.getHpaCtlQueue();
        nsdsCtlQueue = queues.getNsdsCtlQueue();
        hpaXferEventQueue = queues.getHpaXferEventQueue();
        hpaConnEventQueue = queues.getHpaConnEventQueue();
        hiaConnEventQueue = queues.getHiaConnEventQueue();
        hiaXferEventQueue = queues.getHiaXferEventQueue();
        hostInterfaceAdapter = nsdsInterface;
        theConfig = config;

        hpaCtlQueue.addIPCQueueEventListener(this);
        hiaConnEventQueue.addIPCQueueEventListener(this);
        hiaXferEventQueue.addIPCQueueEventListener(this);
    }

    @Override
    public void doRun(Executor x)
    {
        x.execute(this);
    }

    @Override
    public boolean sendAck(long connectionId, CcsiChannelEnum channel, long msn)
    {
        boolean result = hostInterfaceAdapter.sendAck(connectionId, channel, msn);
        return result;
    }

    @Override
    public boolean sendNak(long connectionId, CcsiChannelEnum channel, long msn,
            NakReasonCodeEnum nakCode, NakDetailCodeEnum nakDetailCode, String nakDetailMessage)
    {
        boolean result = hostInterfaceAdapter.sendNak(connectionId, channel, msn, 
                nakCode, nakDetailCode, nakDetailMessage);
        return result;
    }

    @Override
    public boolean sendHrtbt(long connectionId)
    {
        boolean result = hostInterfaceAdapter.sendHrtbt(connectionId);
        return result;
    }

    @Override
    public boolean sendReport(long connectionId, CcsiChannelEnum[] channel,
            String[] cacheKeys, String msg, CcsiAckNakEnum ack, long ackMsn, 
            NakReasonCodeEnum nakCode, boolean ackRequired)
    {
        boolean result = hostInterfaceAdapter.sendReport(connectionId, channel, 
                cacheKeys, msg, ack, ackMsn, nakCode);
        return result;
    }

    @Override
    public void queueEventFired(IPCQueueEvent evt)
    {
        if (evt.getEventSource() instanceof NSDSXferEventQueue)
        {
            NSDSTransferEvent xferEvt = hiaXferEventQueue.readFirst(0);

            if (xferEvt != null)
            {
                hpaXferEventQueue.insertLast(xferEvt);
            }
        } else if (evt.getEventSource() instanceof CtlWrapperQueue)
        {
            CtlWrapper ctlEvt = hpaCtlQueue.readFirst(0);

            if (ctlEvt != null)
            {
                if (ctlEvt.getCtlCommand() == CtlWrapper.STOP_THREAD)
                {
                    shutDown = true;
                    nsdsCtlQueue.insertLast(ctlEvt);
                }
            }
        } else if (evt.getEventSource() instanceof ConnEventQueue)
        {
            NSDSConnEvent connEvt = hiaConnEventQueue.readFirst(0);

            if (connEvt != null)
            {
                hpaConnEventQueue.insertLast(connEvt);
            }
        } else
        {
            myLogger.error("Unexpected queue event received.");
        }
    }

    @Override
    public void run()
    {
        myLogger.info("Initiated Host Protocol Adapter thread");

        while (!this.shutDown)
        {
            try
            {
                // does nothing
                Thread.sleep(2000);
            } catch (InterruptedException ex)
            {
                myLogger.error("Interrupted Exception", ex);
            }
        }
    }
}
