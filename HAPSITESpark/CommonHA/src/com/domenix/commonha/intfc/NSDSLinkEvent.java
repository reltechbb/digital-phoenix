/*
 * NSDSLinkEvent.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of an NSDS Link event that can be dispatched to listeners.
 *
 * Version: V1.0  15/08/01
 */


package com.domenix.commonha.intfc;

import com.domenix.commonha.enums.NSDSLinkEventTypeEnum;

/**
 * This defines a link event that can be dispatched to listeners.
 *
 * @author kmiller
 */
public class NSDSLinkEvent
{
  /** The event type. */
  private NSDSLinkEventTypeEnum	eventType	= NSDSLinkEventTypeEnum.LINK_CONFIGURED;

  /** The ID number of the link for the event. */
  private int	linkId	= -1;

  /** Send/Received MSN */
  private long	sentMsn	= -1L;
  
  /** The logical connection item being sent */
  private long itemId = -1L;
  
  /** The connection ID */
  private long connectionId = -1L;
  
  /** A received message buffer */
  private String	linkBuffer = null;
  
  /**
   * The timestamp (milliseconds) of the event.  This value is assigned when an event
   * is created and cannot be modified.
   */
  private long	eventTime	= System.currentTimeMillis();

  /** The status of the event indicating successful (true) or failed (false) with a default value of failed. */
  private boolean	eventStatus	= false;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an event with a source.
   *
   * @param type the type of the event
   */
  public NSDSLinkEvent( NSDSLinkEventTypeEnum type )
  {
    this.eventType = type;
  }

  /**
   * Constructs an event with a source and status.
   *
   * @param type the event type
   * @param status the event status
   */
  public NSDSLinkEvent( NSDSLinkEventTypeEnum type , boolean status )
  {
    this.eventType = type;
    this.eventStatus = status;
  }

  /**
   * Constructs an event with a source, status, and link ID number
   *
   *
   * @param type the event type
   * @param status the event status
   * @param linkId the ID number of the associated link
   */
  public NSDSLinkEvent( NSDSLinkEventTypeEnum type , boolean status , int linkId )
  {
    this.eventType = type;
    this.eventStatus = status;
    this.linkId	= linkId;
  }

  /**
   * Constructs an event with a source, status, link ID, and event type.
   *
   * @param status the event status
   * @param linkId the ID number of the associated link
   * @param type the type of the event
   */
  public NSDSLinkEvent( boolean status , int linkId , NSDSLinkEventTypeEnum type )
  {
    this.eventStatus = status;
    this.linkId			= linkId;
    this.eventType	= type;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the eventType
   */
  public NSDSLinkEventTypeEnum getEventType()
  {
    return eventType;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param eventType the eventType to set
   */
  public void setEventType( NSDSLinkEventTypeEnum eventType )
  {
    this.eventType	= eventType;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the linkId
   */
  public long getLinkId()
  {
    return linkId;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param linkId the linkId to set
   */
  public void setLinkId( int linkId )
  {
    this.linkId	= linkId;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the linkBuffer
   */
  public String getLinkBuffer()
  {
    return linkBuffer;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param linkBuffer the linkBuffer to set
   */
  public void setLinkBuffer( String linkBuffer )
  {
    this.linkBuffer	= linkBuffer;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Method description
   *
   *
   * @return
   */
  public long getSentMsn()
  {
    return sentMsn;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * Method description
   *
   *
   * @param sentMsn
   */
  public void setSentMsn( long sentMsn )
  {
    this.sentMsn	= sentMsn;
  }

  /**
   * @return the eventTime
   */
  public long getEventTime()
  {
    return eventTime;
  }

  /**
   * @param eventTime the eventTime to set
   */
  public void setEventTime(long eventTime)
  {
    this.eventTime = eventTime;
  }

  /**
   * @return the eventStatus
   */
  public boolean isEventStatus()
  {
    return eventStatus;
  }

  /**
   * @param eventStatus the eventStatus to set
   */
  public void setEventStatus(boolean eventStatus)
  {
    this.eventStatus = eventStatus;
  }

  /**
   * @return the itemId
   */
  public long getItemId()
  {
    return itemId;
  }

  /**
   * @param itemId the itemId to set
   */
  public void setItemId(long itemId)
  {
    this.itemId = itemId;
  }

  /**
   * @return the connectionId
   */
  public long getConnectionId()
  {
    return connectionId;
  }

  /**
   * @param connectionId the connectionId to set
   */
  public void setConnectionId(long connectionId)
  {
    this.connectionId = connectionId;
  }
}
