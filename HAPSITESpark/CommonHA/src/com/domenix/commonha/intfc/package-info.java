/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 *<br><br>
 * This package contains Enums shared by the Host Interface Adapter, 
 * Host Protocol Adapter and the Protocol Handler (PH)
 * Provides Host related types for sensors to maintain connections, links, and generate events.
 *
 */
package com.domenix.commonha.intfc;