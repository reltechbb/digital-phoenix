/*
 * NSDSConnEvent.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of an NSDS Connection Event
 *
 * Version: V1.0  15/08/01
 */


package com.domenix.commonha.intfc;

import com.domenix.commonha.enums.NSDSConnEventEnum;

/**
 * This class defines an NSDS connection event which is dispatched to registered listeners when
 * the connection state changes.
 *
 * @author kmiller
 */
public class NSDSConnEvent
{
  /** The type of the event */
  private NSDSConnEventEnum	connEventType	= NSDSConnEventEnum.CONN_TERMINATED;

  /** The connection ID */
  private long	connId	= -1L;

  /** Event status */
  private boolean	status	= false;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a basic connection event.
   *
   * @param type the type of connection event (terminated, transient, persistent)
   */
  public NSDSConnEvent( NSDSConnEventEnum type )
  {
    this.connEventType	= type;
  }

  /**
   * Constructs an event with a source and a status.
   *
   * @param type the type of connection event (terminated, transient, persistent)
   * @param status the status of the event
   */
  public NSDSConnEvent( NSDSConnEventEnum type , boolean status )
  {
    this.status					= status;
    this.connEventType	= type;
  }

  /**
   * Constructs an event with a source, status, and connection ID number.
   *
   * @param type the type of connection event (terminated, transient, persistent)
   * @param status the status of the event
   * @param connId the identification number of the connection for the event
   */
  public NSDSConnEvent( NSDSConnEventEnum type , boolean status , long connId )
  {
    this.connEventType	= type;
    this.status					= status;
    this.connId					= connId;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the connEventType
   */
  public NSDSConnEventEnum getConnEventType()
  {
    return connEventType;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param connEventType the connEventType to set
   */
  public void setConnEventType( NSDSConnEventEnum connEventType )
  {
    this.connEventType	= connEventType;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the status
   */
  public boolean isStatus()
  {
    return status;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param status the status to set
   */
  public void setStatus( boolean status )
  {
    this.status	= status;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the connId
   */
  public long getConnId()
  {
    return connId;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param connId the connId to set
   */
  public void setConnId( long connId )
  {
    this.connId	= connId;
  }
}
