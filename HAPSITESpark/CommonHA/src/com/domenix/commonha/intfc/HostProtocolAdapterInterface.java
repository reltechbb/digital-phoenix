/*
 * HostProtocolAdapterInterface.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of the interface to the Host Protocol Adapter
 *
 */
package com.domenix.commonha.intfc;

import com.domenix.commonha.ccsi.CcsiAckNakEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.commonha.queues.HAQueues;

import java.util.concurrent.Executor;

/**
 * This file contains the definition of the interface to the Host Protocol Adapter
 *
 * @author jmerritt
 */
public interface HostProtocolAdapterInterface {

    /**
     * Initializes the interface. MUST be called prior to any other operation.
     *
     * @param haQueues - Queues utilized by the Host Adapter and Protocol Handler
     * @param config - configuration of the Host network
     * @param nsdsInterface - implementation of NSDSInterface
     * @param connectionList - connection list - not used by all implementations
     */
    public void initialize(HAQueues haQueues, HostNetworkSettings config, 
            NSDSInterface nsdsInterface, NSDSConnectionList connectionList);

    /**
     * Completes the initialization of the interface and starts the process for receiving messages
     *
     * @param x - instance of implementing class
     */
    public void doRun(Executor x);

    /**
     * Sends an acknowledgment for the MSN on the connection.
     *
     * @param connectionId the ID number of the connection
     * @param channel the channel for the ACK
     * @param msn the MSN of the message being acknowledged
     *
     * @return
     */
    public boolean sendAck(long connectionId, CcsiChannelEnum channel, long msn);

    /**
     * Sends a specified negative acknowledgment for the MSN on the connection with optional
     * parameters to fill in a detailed status report if the host is registered for the STATUS
     * channel and
     *
     * @param connectionId the ID number of the connection
     * @param channel the channel for NAK
     * @param msn the MSN of the message being negatively acknowledged
     * @param nakCode the message header NAK code
     * @param nakDetailCode the detailed NAK code of the error if applicable, else null
     * @param nakDetailMessage the detailed NAK message for the error if applicable, else null
     *
     * @return flag indicating successful queuing (true) or not (false)
     */
    public boolean sendNak(long connectionId, CcsiChannelEnum channel, long msn, NakReasonCodeEnum nakCode,
            NakDetailCodeEnum nakDetailCode, String nakDetailMessage);

    /**
     * Sends a heartbeat message to the host system.
     *
     * @param connectionId the ID number of the connection
     *
     * @return flag indicating successful queuing (true) or not (false)
     */
    public boolean sendHrtbt(long connectionId);

    /**
     * Sends a report message to the host system. The string provided must contain a complete CCSI
     * message to be transmitted as defined in the CCSI
     * schemas.
     *
     * @param connectionId the ID number of the connection
     * @param channel the channel of the message
     * @param cacheKeys the cache key(s) for the report(s) being sent
     * @param msg the message to be transmitted
     * @param ack
     * @param ackMsn
     * @param nakCode
     *
     * @return flag indicating successful queuing (true) or not (false)
     */
    public boolean sendReport(long connectionId, CcsiChannelEnum[] channel, String[] cacheKeys,
            String msg, CcsiAckNakEnum ack, long ackMsn, NakReasonCodeEnum nakCode, boolean ackRequired);
}
