/*
 * NSDSLink.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of a link which will be extended by specific link type
 * classes.
 *
 * Version: V1.0  15/08/01
 */


package com.domenix.commonha.intfc;

//~--- JDK imports ------------------------------------------------------------

import java.util.concurrent.atomic.AtomicInteger;

//~--- classes ----------------------------------------------------------------

/**
 * This defines a base class for an NSDS/CCSI physical link.  Actual link implementations must
 * extend this class and provide the specific functionality
 *
 * @author kmiller
 */
public abstract class NSDSLink
{
  /** The static generator of Link ID numbers */
  private static final AtomicInteger	idGenerator;

  //~--- static initializers --------------------------------------------------

  static
  {
    idGenerator	= new AtomicInteger( -1 );
  }

  /**
   * @return the idGenerator
   */
  public static AtomicInteger getIdGenerator()
  {
    return idGenerator;
  }

  //~--- fields ---------------------------------------------------------------
  
  /** Is this link physically connected (not a logical connection) */
  private boolean	connected	= false;

  /** Is this link registered on the network. */
  private boolean	registered	= false;

  /** The type of the interface */
  private NSDSInterfaceTypeEnum	interfaceType = NSDSInterfaceTypeEnum.INTFC_NONE;

  /** The ID number of this link. */
  private final int	linkId;
  
  /** The IPC queue for writing to the host system */
//  private IPCQueue sendQueue;
  
  /** The IPC queue for reading from the host system */
//  private IPCQueue receiveQueue;
  
  /** The Thread group for link readers and writers */
//  private ThreadGroup linkIoThreads;
  
  /** The link send thread */
//  private ArrayList<Thread> linkSendThread;
  
  /** The link receive thread */
//  private ArrayList<Thread> linkRecvThread;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a default instance with the Open Interface type and assigns it an interface ID number.
   */
  public NSDSLink()
  {
    this.linkId	= NSDSLink.idGenerator.incrementAndGet();
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the linkId
   */
  public int getLinkId()
  {
    return linkId;
  }

  /**
   * @return the connected
   */
  public boolean isConnected()
  {
    return connected;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param connected the connected to set
   */
  public void setConnected( boolean connected )
  {
    this.connected	= connected;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the registered
   */
  public boolean isRegistered()
  {
    return registered;
  }

  //~--- set methods ----------------------------------------------------------

  /**
   * @param registered the registered to set
   */
  public void setRegistered( boolean registered )
  {
    this.registered	= registered;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * @return the interfaceType
   */
  public NSDSInterfaceTypeEnum getInterfaceType()
  {
    return interfaceType;
  }

  /**
   * @param interfaceType the interfaceType to set
   */
  public void setInterfaceType(NSDSInterfaceTypeEnum interfaceType)
  {
    this.interfaceType = interfaceType;
  }
  
  /**
   * This method starts the reading/writing/connecting/disconnecting threads for the link.
   * 
   * @param connectId the ID of the connection associated with the connect
   * 
   * @return flag indicating success (true) or failure (false)
   */
  public abstract boolean connect( long connectId );
  
  /**
   * This method stops the reading/writing/connecting/disconnecting threads for the link.
   * 
   * @param connectId the ID of the connection
   * 
   * @return flag indicating success (true) or failure (false)
   */
  public abstract boolean disconnect( long connectId );
  
  /**
   * This method initiates sending a message to the host system.
   * 
   * @param msg String containing the message to be sent
   * @param connectId the ID number of the connection this is being sent for.
   * @param itemId the ID of the logical connection's item being sent
   * 
   * @return flag indicating successful queuing of the message for transmission
   */
  public abstract boolean send( String msg , long connectId , long itemId );
  
  public abstract String getConnectHostAddress( long connectId );
}
