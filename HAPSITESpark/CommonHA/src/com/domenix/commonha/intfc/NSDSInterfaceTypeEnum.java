/*
 * NSDSInterfaceTypeEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of NSDS interface type codes for Open Interface implementations.
 *
 * Version: V1.0  15/08/01
 */


package com.domenix.commonha.intfc;

import static com.domenix.commonha.intfc.NSDSInterfaceTypeEnum.valueOf;

/**
 * This defines the enumeration of NSDS package interface specialization types.
 *
 * @author kmiller
 */
public enum NSDSInterfaceTypeEnum
{
  /** No interface specified */
  INTFC_NONE ,
  /** An open interface connection over TCP/IP */
  INTFC_TCPIP_OI ,
  /** A secure connection over TCP/IP */
  INTFC_TCPIP_SEC ,
  /** A connection over a serial connection. */
  INTFC_SERIAL;

  /**
   * Returns the String form of the enumeration values name.
   *
   * @return String containing the enumeration value name
   */
  public String value()
  {
    return name();
  }

  /**
   * Returns the enumeration value for the supplied name String.
   *
   * @param v String containing the name of the enumeration value
   *
   * @return the enumeration value or null.
   */
  public static NSDSInterfaceTypeEnum fromValue( String v )
  {
    return valueOf( v );
  }
}
