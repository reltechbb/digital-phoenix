/*
 * NSDSLinkList.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition for a List of NSDSLink items
 *
 * Version: V1.0  15/08/02
 */


package com.domenix.commonha.intfc;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

//~--- classes ----------------------------------------------------------------

/**
 * This defines a list of NSDSLink objects which identify the communications links available for
 * host communications that are available on the sensor.
 *
 * @author kmiller
 */
public class NSDSLinkList
{
  /** The list of NSDSLink items. */
  private final ArrayList<NSDSLink>	linkList = new ArrayList<>();

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a instance of the class.
   */
  public NSDSLinkList()
  {
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the underlying list object
   *
   * @return List of NSDS link objects
   */
  protected List<NSDSLink> getList()
  {
    return ( this.linkList );
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Add an NSDSLink item to the list.
   *
   * @param item NSDSLink to be added
   * 
   * @throws IllegalArgumentException if the item is null
   */
  public void addLink( NSDSLink item )
  {
    if ( item != null )
    {
      this.linkList.add( item );
    }
    else
    {
      throw new IllegalArgumentException( "Null item cannot be added to the list" );
    }
  }
  
  /**
   * Get the number of items in the list.
   * 
   * @return int the number of items in the list
   */
  public int size()
  {
    return( this.linkList.size() );
  }
  
  /**
   * Clear all items from the list
   */
  public void clear()
  {
    this.linkList.clear();
  }
  
  /**
   * Returns the NSDSLink item located at the index.
   * 
   * @param index the list index of the item to be returned
   * 
   * @return NSDSLink located in the index slot in the list or null if the index is invalid
   */
  public NSDSLink getItem( int index )
  {
    if ( ( index >= 0 ) && ( index < this.linkList.size() ) )
    {
      return( this.linkList.get( index ) );
    }
    else
    {
      return( null );
    }
  }
  
  /**
   * Returns a flag indicating if the list is empty.
   * 
   * @return boolean indicating if the list is empty (true) or has at least on element (false)
   */
  public boolean isEmpty()
  {
    return( this.linkList.isEmpty() );
  }

  /**
   * @return the linkList
   */
  public ArrayList<NSDSLink> getLinkList()
  {
    return linkList;
  }
}
