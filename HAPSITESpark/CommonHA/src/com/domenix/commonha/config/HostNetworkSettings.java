/*
 * HostNetworkSettings.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * contains network settings for the host interface
 */
package com.domenix.commonha.config;

import com.domenix.common.config.NetworkSettings;
import com.domenix.commonha.ccsi.CcsiModeEnum;

/**
 * contains additional network settings for the host interface
 * @author jmerritt
 */
public class HostNetworkSettings extends NetworkSettings
{
    /** 3 strikes should be enforced */
    private boolean enforceStrikes = false;
    /** Protocol Handler is an Open Interface server */
    private boolean oiServer = true;
    /** Time window should be enforced */
    private boolean enforceTime = true;
    /** time window is + or - this value in seconds */
    private long enforceTimeValue = 15L;
    /** time delay between retires in seconds */
    private int connRetryTime = 30;
    /** number of times to retry a connection before giving up */
    private int connRetryCount = 3;
    /** classification of communications */
    private String classification = "UNCLASS";
    /** The UUID of this sensor. */
    private String uuid = "12345678-1234-1234-1234-1234567890ab";
    /** Communications are encrypted */
    private boolean encryptComm = false;
    /** ID of the sensor as assigned by the host */
    private String hostSensorID = null;
    /** The mode of the PH */
    private CcsiModeEnum mode = CcsiModeEnum.I;
    /** the name of the host (used for ISA) must match the name in the cert */
    private String hostName;
    /** the name of the Protocol Handler (used for ISA) must match the name in the cert */
    private String hostPHName;
    /** the ISA status interval */
    private Integer isaStatusInterval = 30;
    /** sensor manufacturer */
    private String sensorManufacturer = "Unspecified";
    /** sensor serial number */
    private String sensorSerialNumber = "Unspecified";
    /** sensor model */
    private String sensorModel = "Unspecified";
    /** sensor name */
    private String sensorName = "Unspecified";
    
    /**
     * The discovery SID
     */
    private final static String DISCOVERY_SID = "b4d81dec-616b-4f2c-a8b2-f787ad24826e";

    /**
     * Constructor for a HostNetworkSettings instance
     */
    public HostNetworkSettings ()
    {
        super ("HOST");
    }
    
    /**
     * 3 strikes should be enforced
     * @param enforce
     */
    public void setEnforceStrikes (boolean enforce)
    {
        enforceStrikes = enforce;
    }
    
    /**
     * Gets Enforce Strikes
     * @return true = 3 strikes should be enforced
     */
    public boolean isEnforceStrikes ()
    {
        return enforceStrikes;
    }
    
    public void setOIServer (boolean isOIServer)
    {
        this.oiServer = isOIServer;
    }
    
    public boolean isOIServer ()
    {
        return oiServer;
    }
    
    /**
     * time window should be enforced
     * @param enforce 
     */
    public void setEnforceTime (boolean enforce)
    {
        enforceTime = enforce;
    }
    
    public boolean getEnforceTime ()
    {
        return enforceTime;
    }
    
    public void setEnforceTimeValue (long time)
    {
        enforceTimeValue = time;
    }
    
    public long getEnforceTimeValue ()
    {
        return enforceTimeValue;
    }
    
    public void setConnRetryTime (int time)
    {
        connRetryTime = time;
    }
    
    public int getConnRetryTime ()
    {
        return connRetryTime;
    }
    
    public void setConnRetryCount (int count)
    {
        connRetryCount = count;
    }
    
    public int getConnRetryCount ()
    {
        return connRetryCount;
    }
    
    public String getClassification ()
    {
        return classification;
    }
    
    public void setClassification (String classification)
    {
        this.classification = classification;
    }    
    
    /**
     * UUID of the sensor
     * If not null, use HostSensorID instead
     * @return the UUID
     */
    public String getUuid ()
    {
        return uuid;
    }
    
    public void setUuid (String uuid)
    {
        if (uuid != null)
        {
            this.uuid = uuid.trim ();
        }
    }
    
    public boolean getEncryptComm ()
    {
        return encryptComm;
    }
    
    public void setEncryptComm (boolean encryptComm)
    {
        this.encryptComm = encryptComm;
    }
    
    public boolean isEncryptComm ()
    {
        return encryptComm;
    }
    
    /**
     * Get ID assigned by the host
     * If null use UUID instead
     * @return Host Sensor ID
     */
    public String getHostSensorID ()
    {
        return hostSensorID;
    }
    
    public void setHostSensorID (String id)
    {
        if (id != null)
        {
            hostSensorID = id.trim();
        } else
        {
            hostSensorID = "";
        }
    }
    
    public CcsiModeEnum getMode ()
    {
        return mode;
    }
    
    public void setMode (CcsiModeEnum mode)
    {
        this.mode = mode;
    }
    
    /**
     * Returns the Host Sensor ID that was assigned by the host.  If the ID is null
     * or empty the UUID of the sensor is returned.
     * If neither are valid the discovery SID is returned
     * @return sensor ID
     */
    public String getHostSensorIDOrUUID ()
    {
        String retVal;
        if ((hostSensorID != null) && (!hostSensorID.trim().isEmpty()))
        {
            retVal = hostSensorID;
        }
        else if((uuid != null) && (!uuid.isEmpty()))
        {
            retVal = uuid;
        }
        else
        {
            retVal =  DISCOVERY_SID;
        }
        return retVal;
    }
    
    public String getDiscoverySID()
    {
        return DISCOVERY_SID;
    }
    
    public String getHostName ()
    {
        return hostName;
    }
    
    public void setHostName (String name)
    {
        hostName = name;
    }    
    
    public String getPHHostName ()
    {
        return hostPHName;
    }
    
    public void setPHHostName (String name)
    {
        hostPHName = name;
    }
    
    public Integer getISAStatusInterval ()
    {
        return isaStatusInterval;
    }
    
    public void setISAStatusInterval (Integer interval)
    {
        isaStatusInterval = interval;
    }
    
    public String getSensorManufacturer ()
    {
        return sensorManufacturer;
    }
    
    public void setSensorManufacturer (String manufacturer)
    {
        sensorManufacturer = manufacturer;
    }
    
    public String getSensorSerialNumber ()
    {
        return sensorSerialNumber;
    }
    
    public void setSensorSerialNumber (String serial)
    {
        sensorSerialNumber = serial;
    }
    
    public String getSensorModel ()
    {
        return sensorModel;
    }
    
    public void setSensorModel (String model)
    {
        sensorModel = model;
    }
    
    public String getSensorName ()
    {
        return sensorName;
    }
    
    public void setSensorName (String name)
    {
        sensorName = name;
    }
}
