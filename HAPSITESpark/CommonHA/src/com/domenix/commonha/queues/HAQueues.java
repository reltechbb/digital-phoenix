/*
 * HAQueues.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file is a container for Queue references
 *
 */
package com.domenix.commonha.queues;

import com.domenix.common.spark.CtlWrapperQueue;

/**
 * This file is a container for Queue references
 * 
 * @author jmerritt
 */
public class HAQueues
{
    /**
     * Control messages to the Host Protocol Adapter
     */
    private CtlWrapperQueue hpaCtlQueue;

    /**
     * Control messages to the Host Interface Adapter
     */
    private CtlWrapperQueue nsdsCtlQueue;
    
    /**
     * Transfer messages to the Protocol Handler
     */
    private NSDSXferEventQueue hpaXferEventQueue;

    /**
     * Transfer messages from the Host Interface Adapter
     */
    private NSDSXferEventQueue hiaXferEventQueue;

    /**
     * Connection events to the Protocol Handler
     */
    private ConnEventQueue hpaConnEventQueue;

    /**
     * Connection events from the Host Interface Adapter
     */
    private ConnEventQueue hiaConnEventQueue;
    
    /**
     * Link events
     */
    private LinkEventQueue nsdsLinkEventQueue;
    
    /**
     * Control messages to the Host Protocol Adapter
     * @return 
     */
    public CtlWrapperQueue getHpaCtlQueue ()
    {
        return hpaCtlQueue;
    }

    /**
     * Sets the HostProtocolAdapter control queue
     * @param queue 
     */
    public void setHpaCtlQueue (CtlWrapperQueue queue)
    {
        hpaCtlQueue = queue;
    }
    
    /**
     * Control messages to the Host Interface Adapter
     * @return 
     */
    public CtlWrapperQueue getNsdsCtlQueue ()
    {
        return nsdsCtlQueue;
    }
    
    /**
     * Sets the NSDS control queue
     * @param queue 
     */
    public void setNsdsCtlQueue (CtlWrapperQueue queue)
    {
        nsdsCtlQueue = queue;
    }
    
    /**
     * Transfer messages to the Protocol Handler
     * @return 
     */
    public NSDSXferEventQueue getHpaXferEventQueue ()
    {
        return hpaXferEventQueue;
    }

    /**
     * Sets the HostProtocolAdapter transfer event queue
     * @param queue 
     */
    public void setHpaXferEventQueue (NSDSXferEventQueue queue)
    {
        hpaXferEventQueue = queue;
    }
    
    /**
     * Transfer messages from the Host Interface Adapter
     * @return 
     */
    public NSDSXferEventQueue getHiaXferEventQueue ()
    {
        return hiaXferEventQueue;
    }

    /**
     * Sets the HostInterfaceAdapter transfer event queue
     * @param queue 
     */
    public void setHiaXferEventQueue (NSDSXferEventQueue queue)
    {
        hiaXferEventQueue = queue;
    }
    
    /**
     * Connection events to the Protocol Handler
     * @return 
     */
    public ConnEventQueue getHpaConnEventQueue ()
    {
        return hpaConnEventQueue;
    }

    /**
     * Sets the HostProtocolAdapter connection event queue
     * @param queue 
     */
    public void setHpaConnEventQueue (ConnEventQueue queue)
    {
        hpaConnEventQueue = queue;
    }
    
    /**
     * Connection events from the Host Interface Adapter
     * @return 
     */
    public ConnEventQueue getHiaConnEventQueue ()
    {
        return hiaConnEventQueue;
    }
    
    /**
     * Sets the HostInterfaceAdapter connection event queue
     * @param queue 
     */
    public void setHiaConnEventQueue (ConnEventQueue queue)
    {
        hiaConnEventQueue = queue;
    }
    
    /**
     * Connection events from the Host Interface Adapter
     * @return 
     */
    public LinkEventQueue getNSDSLinkEventQueue ()
    {
        return nsdsLinkEventQueue;
    }
    
    /**
     * Sets the NSDS Link Event queue
     * @param queue 
     */
    public void setNSDSLinkEventQueue (LinkEventQueue queue)
    {
        nsdsLinkEventQueue = queue;
    }
}
