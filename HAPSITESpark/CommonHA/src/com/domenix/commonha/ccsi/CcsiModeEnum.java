/*
 * CcsiModeEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This enumeration identifies the CCSI modes available for a sensor.
 *
 * Version: V1.0  15/08/30
 */
package com.domenix.commonha.ccsi;

/**
 * This enumeration identifies the CCSI modes available for a sensor.
 * 
 * @author kmiller
 */
public enum CcsiModeEnum
{
  /** Normal mode */
  N("NORMAL",true) ,
  /** Degraded but not inoperable */
  D("DEGRAD",false) ,
  /** Exercise Mode */
  X("EXERCS",true) , 
  /** Test Mode */
  T("TEST",true) , 
  /** User training Mode */
  U("TRAIN",true) ,
  /** Setup mode for programming the sensor */
  S("SETUP",true) ,
  /** Sensor is initializing */
  I("INIT",false);

  /** Long form of the mode name */
  private final String	longName;
  
  /** Is this mode commandable */
  private final boolean commandable;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance with the specified name
   *
   * @param ln the long name of the mode
   * @param commandable can this mode be set by command
   */
  private CcsiModeEnum( String ln , boolean commandable )
  {
    this.longName	= ln;
    this.commandable = commandable;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * Returns the long name
   *
   * @return the long name
   */
  public String getLongName()
  {
    return ( this.longName );
  }
  
  /**
   * Returns the enumeration whose long name matches the input or null if none matches
   * 
   * @param ln the long name to be found
   * 
   * @return the matching enumeration or null
   */
  
  public static CcsiModeEnum value( String ln ) throws IllegalArgumentException
  {
    for ( CcsiModeEnum x : CcsiModeEnum.values() )
    {
      if ( x.longName.equalsIgnoreCase( ln ) )
      {
        return( x );
      }
    }
    throw new IllegalArgumentException( "No matching enumeration with specified name." );
  }

  /**
   * @return whether the is mode is commandable by a host
   */
  public boolean isCommandable()
  {
    return this.commandable;
  }
}
