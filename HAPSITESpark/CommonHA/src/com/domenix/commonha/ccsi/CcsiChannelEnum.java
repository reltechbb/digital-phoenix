/*
 * CcsiChannelEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains a definition of CCSI channels containing their header abbreviation, long name, and message tag.
 *
 * Version: V1.0  15/08/02
 */


package com.domenix.commonha.ccsi;

/**
 * This file contains a definition of CCSI channels containing their header abbreviation, long name, and message tag.
 * 
 * @author kmiller
 */
public enum CcsiChannelEnum
{
  /** Alerts channel */
  ALERTS('a' , "Alerts" , "AlertsChn", "ALERTS") ,

  /** Configuration channel */
  CONFIG('f' , "Configuration" , "ConfigChn", "CONFIG") ,

  /** Heartbeat channel, has no matching channel tag since it is a header only channel */
  HRTBT('h' , "Heartbeat" , "None", "HRTBT") ,

  /** Identification channel */
  IDENT('i' , "Identification" , "IdentChn", "IDENT") ,

  /** Location channel */
  LOC('l' , "Location" , "LocationChn", "LOC") ,

  /** Maintenance channel */
  MAINT('m' , "Maintenance" , "MaintChn", "MAINT") ,

  /** Readings channel */
  READGS('r' , "Readings" , "ReadingsChn", "READGS") ,

  /** Status channel */
  STATUS('s' , "Status" , "StatusChn", "STATUS") ,

  /** Time-Date channel */
  TMDT('t' , "TimeDate" , "TimeDateChn", "TMDT") ,

  /** Command channel */
  CMD('c' , "Command" , "CmdChn", "CMD") ,

  /** Security channel */
  SEC('z' , "Security" , "SecurityChn", "SEC");

  /** The message header letter code */
  private final char	abbreviation;

  /** The communications tag for the channel */
  private final String	chanTag;

  /** The English name of the channel */
  private final String	longName;

  /** The ENUM name of the channel */
  private final String enumName;
  
  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an instance with the specified abbreviation and name.
   *
   * @param abbrev message header letter for this channel
   * @param name the English name of the channel
   * @param chan
   */
  private CcsiChannelEnum( char abbrev , String name , String chan, String enumName )
  {
    this.abbreviation	= abbrev;
    this.longName			= name;
    this.chanTag			= chan;
    this.enumName = enumName;
  }

  //~--- methods --------------------------------------------------------------

  /**
   * Returns the enumeration that matches the input string converted to upper case.
   *
   * @param val string containing the channel name
   *
   * @return enumeration value or null if no match
   */
  public static CcsiChannelEnum value( String val )
  {
    return ( CcsiChannelEnum.valueOf( val.toUpperCase() ) );
  }

  /**
   * Returns the enumeration that matches the input character.
   *
   * @param val char containing the channel abbreviation to be matched
   *
   * @return enumeration value or null if no match
   */
  public static CcsiChannelEnum value( char val )
  {
    for ( CcsiChannelEnum x : CcsiChannelEnum.values() )
    {
      if ( x.getAbbreviation() == val )
      {
        return ( x );
      }
    }

    return ( null );
  }
  
  /**
   * Returns the enumeration that matches the input character.
   *
   * @param val String containing the channel name to be matched
   *
   * @return enumeration value or null if no match
   */
  public static CcsiChannelEnum valueOfEnglish( String val )
  {
    for ( CcsiChannelEnum x : CcsiChannelEnum.values() )
    {
      if ( x.longName.equalsIgnoreCase( val.trim() ) ||
              x.enumName.equalsIgnoreCase( val.trim() ))
      {
        return ( x );
      }
    }

    return ( null );
  }

  /**
   * Returns the enumeration that matches the input channel tag for CCSI.
   *
   * @param val String containing the channel tag to be matched
   *
   * @return enumeration value or null if no match
   */
  public static CcsiChannelEnum valueChan( String val )
  {
    for ( CcsiChannelEnum x : CcsiChannelEnum.values() )
    {
      if ( x.getChanTag().equalsIgnoreCase( val.trim() ) )
      {
        return ( x );
      }
    }

    return ( null );
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the abbreviation
   */
  public char getAbbreviation()
  {
    return abbreviation;
  }

  /**
   * @return the longName
   */
  public String getLongName()
  {
    return longName;
  }

  /**
   * @return the chanTag
   */
  public String getChanTag()
  {
    return chanTag;
  }
}
