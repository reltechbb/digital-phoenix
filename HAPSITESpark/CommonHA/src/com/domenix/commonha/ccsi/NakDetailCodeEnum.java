/*
 * NakDetailCodeEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the enumeration for CCSI detailed NAK type codes.
 *
 * Version: V1.0  15/08/14
 */


package com.domenix.commonha.ccsi;

/**
 * This enumeration defines an enumeration for detailed NAK message codes for CCSI.
 *
 * @author kmiller
 */
public enum NakDetailCodeEnum
{
  UNKNOWN_COMMAND( "UnknownCommand" , "Command is unknown for this sensor." ) ,
  UNSUPPORTED_COMMAND( "UnsupportedCommand" , "Command is not supported." ) ,
  COMMAND_FAILED( "CommandFailed" , "Command execution failed." ) ,
  INVALID_ARGUMENT( "InvalidArgument" , "Command argument invalid" ) ,
  UNKNOWN_ARGUMENT( "UnknownArgument" , "Argument is unknown." ) ,
  MISSING_ARGUMENT( "MissingArgument" , "Required argument is missing." ) ,
  TOO_MANY_ARGUMENTS( "TooManyArguments" , "Too many arguments were received." ) ,
  INSUFFICIENT_ARGUMENTS( "InsufficientArguments" , "Not enough argument were received." ) ,
  INVALID_STATE( "InvalidState" , "Sensor state is not valid for execution of the command." ) ,
  INVALID_MESSAGE( "InvalidMessage" , "Received message was not valid" ) ,
  MISSING_DATA( "MissingData" , "Required data was not present in the message." ) ,
  UNKNOWN_DATA( "UnknownData" , "Received data is not known or recognized." ) ,
  INVALID_DATA( "InvalidData" , "Received data is not valid." ) ,
  UNSUPPORTED_SET_ITEM( "UnsupportedSetItem" , "The item to be set is not supported for this operation." ) ,
  INVALID_MARKING( "InvalidMarking" , "Classification markings received are not valid." );

  /** The default text for an error message with embedded macros */
  private final String	defText;

  /** The XML enumeration string for this Java enumeration */
  private final String	xmlCode;

  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs an enumeration instance
   *
   * @param xmlCode the associated XML enumeration name
   * @param defaultText the default text for a detail report
   */
  private NakDetailCodeEnum( String xmlCode , String defaultText )
  {
    this.xmlCode	= xmlCode;
    this.defText	= defaultText;
  }

  //~--- get methods ----------------------------------------------------------

  /**
   * @return the xmlCode
   */
  public String getXmlCode()
  {
    return xmlCode;
  }

  /**
   * @return the defText
   */
  public String getDefText()
  {
    return defText;
  }
}
