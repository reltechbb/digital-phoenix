/*
 * NakReasonCodeEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the enumeration of messag header NAK codes
 *
 * Version: V1.0  15/08/02
 */


package com.domenix.commonha.ccsi;

/**
 * This enumeration specifies the valid message header NAK codes.
 *
 * @author kmiller
 */
public enum NakReasonCodeEnum
{
  /** Message format is invalid */
  FORMAT ,

  /** Message length invalid */
  LENMSM ,

  /** Message content is invalid */
  MSGCON ,

  /** Message sequence number is invalid */
  MSGMSN ,

  /** Timeout waiting for response */
  TIMOUT
}
