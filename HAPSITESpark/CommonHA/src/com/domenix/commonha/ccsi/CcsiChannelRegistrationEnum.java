/*
 * CcsiChannelRegistrationEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the type of registration for a channel.
 *
 * Version: V1.0  15/09/10
 */


package com.domenix.commonha.ccsi;

/**
 * This enumerates the different types of registrations for a channel including no registration.
 * 
 * @author kmiller
 */
public enum CcsiChannelRegistrationEnum
{
  /** Event reporting */
  EVENT , 
  /** Periodic reporting */
  PERIOD , 
  /** Report only once */
  ONCE , 
  /** Cancel reporting */
  CANCEL , 
  /** No reporting */
  NONE;
}
