/*
 * NSDSTransferEventEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains ...
 *
 * Version: V1.0  15/10/10
 */


package com.domenix.commonha.enums;

/**
 * This enumeration defines the different types of Link Transfer events.
 * 
 * @author kmiller
 */
public enum NSDSTransferEventEnum
{
  /** Received an ACK or NAK */
  XFER_RCV_ACK , 
  /** Received a heartbeat message or heartbeat ACK */
  XFER_RCV_HRTBT , 
  /** Received a command message */
  XFER_RCV_CMD , 
  /** Sent an ACK or NAK */
  XFER_SENT_ACK , 
  /** Sent a heartbeat message or heartbeat ACK */
  XFER_SENT_HRTBT , 
  /** Sent a report */
  XFER_SENT_REPORT;
}
