/*
 * NSDSConnEventEnum.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * This file contains the definition of the types of connection events.
 *
 * Version: V1.0  15/08/01
 */


package com.domenix.commonha.enums;

import static com.domenix.commonha.enums.NSDSConnEventEnum.valueOf;

/**
 * This enumeration defines the types of connection events that can be reported by NSDS.
 *
 * @author kmiller
 */
public enum NSDSConnEventEnum
{
  /** Connection is now a CCSI transient logical connection */
  CONN_TRANSIENT , 
  /** Connection is now a CCSI persistent logical connection */
  CONN_PERSISTENT , 
  /** Connection was terminated */
  CONN_TERMINATED ,
  /** Connection attempt failed after three tries */
  CONN_FAILED;
  
  /**
   * Returns the String form of the enumeration values name.
   *
   * @return String containing the enumeration value name
   */
  public String value()
  {
    return name();
  }

  /**
   * Returns the enumeration value for the supplied name String.
   *
   * @param v String containing the name of the enumeration value
   *
   * @return the enumeration value or null.
   */
  public static NSDSConnEventEnum fromValue( String v )
  {
    return valueOf( v );
  }
}
