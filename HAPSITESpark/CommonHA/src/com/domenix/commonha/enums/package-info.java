/**
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * This package contains enums shared by the 
 * Host Interface Adapter, Host Protocol Adapter and the Protocol Handler
 * 
 * Also used by the Protocol Handler (PH)
 *
 */
package com.domenix.commonha.enums;