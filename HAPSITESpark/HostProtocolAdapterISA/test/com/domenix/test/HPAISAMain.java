/*
 * HPAISAMain
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Test Main for the HPAISA
 */
package com.domenix.test;

import com.domenix.hpaisa.radiac.HPAISARadiacImpl;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Test Main for the HPAISA
 * @author jmerritt
 */
public class HPAISAMain
{
    
    public static void main(final String[] args)
    {
        // First, set up log4j
        String log4jXmlFile = "d:/Spark/Code/TRUNK/HostProtocolAdapterISA/resources/log4j.xml";
        DOMConfigurator myConfig = new DOMConfigurator();
        myConfig.doConfigure(log4jXmlFile, Logger.getRootLogger().getLoggerRepository());
        Logger.getRootLogger().setLevel(Level.INFO);

        HPAISARadiacImpl test = new HPAISARadiacImpl();
        test.initializeHPAISA();
    }
}
