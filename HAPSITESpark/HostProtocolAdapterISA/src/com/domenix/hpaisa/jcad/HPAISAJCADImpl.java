/*
 * HPAISAJCADImpl.java
 *
 * Copyright (c) 2001-2020 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * <put description here>
 *
 */
package com.domenix.hpaisa.jcad;

import com.domenix.commonha.ccsi.CcsiAckNakEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.commonha.intfc.NSDSConnectionList;
import com.domenix.commonha.intfc.NSDSInterface;
import com.domenix.commonha.queues.HAQueues;
import com.domenix.hpaisa.HPAISABase;
import gov.isa.capabilities.PropertyHandler;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Implements the methods required by HPAISAInit for JCAD
 *
 * @author dricciardi
 */
public class HPAISAJCADImpl extends HPAISABase implements Runnable {

    private HPAISAJCADInitImpl hpaISAInit;
    private HAQueues haQueues;
    private HostNetworkSettings hostNetworkSettings;
    private NSDSConnectionList connectionList;
    private final Logger myLogger;
    private JCADReadings jcadReadings;

    // send entire message first time
    private Boolean firstReport = true;
    
    public HPAISAJCADImpl() {
        super();
        myLogger = Logger.getLogger(HPAISAJCADImpl.class);
    }

    @Override
    public void initialize(HAQueues queues, HostNetworkSettings config,
            NSDSInterface nsdsInterface, NSDSConnectionList connectionList) {
        super.initialize(queues, config, nsdsInterface, connectionList);
        myLogger.info("Initializing HPAISAJCADImpl");
        this.connectionList = connectionList;
        haQueues = queues;
        hostNetworkSettings = config;
        hpaISAInit = new HPAISAJCADInitImpl(haQueues, hostNetworkSettings, connectionList);
        hpaISAInit.initialize(hostNetworkSettings);
        jcadReadings = new JCADReadings();
    }

    /**
     * Constructor for testing purposes - not to be used operationally
     */
    public void initializeHPAISA() {
        myLogger.info("Initializing HPAISAJCADImpl test");
        hpaISAInit = new HPAISAJCADInitImpl(haQueues, hostNetworkSettings, connectionList);
        hpaISAInit.initialize(hostNetworkSettings);
    }

    private Boolean gHarmful = false;
    private Boolean hHarmful = false;

    private void processReadingReportSUDList(Node data) {
        String gBar = null;
        String gDose = null;
        String gAgentType = null;
        String hBar = null;
        String hDose = null;
        String hAgentType = null;
        NamedNodeMap sudList = null;
        Node sud = null;
        if (data != null) {
            sud = data.getFirstChild();
            if (sud != null) {
                do {
                    sudList = sud.getAttributes();
                    for (Integer attribute = 0; attribute < sudList.getLength(); attribute++) {
                        Node node = sudList.item(attribute);
                        String name = node.getNodeName();
                        if (name.equals("Name")) {
                            String nodeValue = node.getNodeValue();
                            myLogger.debug("Name: " + nodeValue + " Value: " + getValue(sudList));
                            switch (nodeValue) {
                                case "GBar":
                                    gBar = getValue(sudList);
                                    break;
                                case "GDose":
                                    gDose = getValue(sudList);
                                    break;
                                case "GAgentType":
                                    gAgentType = getValue(sudList);
                                    break;
                                case "HBar":
                                    hBar = getValue(sudList);
                                    break;
                                case "HDose":
                                    hDose = getValue(sudList);
                                    break;
                                case "HAgentType":
                                    hAgentType = getValue(sudList);
                                    break;

                                case "drawing_number":
                                    jcadReadings.setDrawingNumber(getValue(sudList));
                                    break;
                                case "issue_number":
                                    jcadReadings.setIssueNumber(getValue(sudList));
                                    break;
                                case "system_id":
                                    jcadReadings.setSystemID(getValue(sudList));
                                    break;
                                case "detection_mode":
                                    jcadReadings.setDetectionMode(getValue(sudList));
                                    break;
                                case "system_alert_status":
                                    jcadReadings.setSystemAlertStatus(getValue(sudList));
                                    break;
                                case "saturation_protection":
                                    jcadReadings.setSaturationProtection(getValue(sudList));
                                    break;
                                case "operating_mode":
                                    jcadReadings.setOperatingMode(getValue(sudList));
                                    break;
                                case "clock":
                                    jcadReadings.setClock(getValue(sudList));
                                    break;
                                case "sieve_life_remaining":
                                    jcadReadings.setSieveLifeRemaining(getValue(sudList));
                                    break;
                                case "atmospheric_pressure":
                                    jcadReadings.setAtmosphericPressure(getValue(sudList));
                                    break;
                                case "sensor_temperature":
                                    jcadReadings.setSensorTemperature(getValue(sudList));
                                    break;
                                case "positive_fg_volts":
                                    jcadReadings.setPositiveFGVolts(getValue(sudList));
                                    break;
                                case "negative_fg_volts":
                                    jcadReadings.setNegativeFGVolts(getValue(sudList));
                                    break;
                                case "pos_noise_g":
                                    jcadReadings.setPosNoiseG(getValue(sudList));
                                    break;
                                case "neg_noise_h":
                                    jcadReadings.setNegNoiseH(getValue(sudList));
                                    break;
                                case "posMobCalibFac":
                                    jcadReadings.setPosMobCalibFac(getValue(sudList));
                                    break;
                                case "negMobCalibFac":
                                    jcadReadings.setNegMobCalibFac(getValue(sudList));
                                    break;
                                case "warning_flags":
                                    jcadReadings.setWarningFlags(getValue(sudList));
                                    break;
                                case "major_fault":
                                    jcadReadings.setMajorFaultFlags(getValue(sudList));
                                    break;
                                case "fault_flags":
                                    jcadReadings.setFaultFlags(getValue(sudList));
                                    break;
                                case "power_status":
                                    jcadReadings.setPowerStatus(getValue(sudList));
                                    break;
                                case "runtime_hrs":
                                    jcadReadings.setRuntimeHrs(getValue(sudList));
                                    break;
                                case "runtime_min":
                                    jcadReadings.setRuntimeMin(getValue(sudList));
                                    break;
                                case "pos_temperature":
                                    jcadReadings.setPosTemperature(getValue(sudList));
                                    break;
                                case "neg_temperature":
                                    jcadReadings.setNegTemperature(getValue(sudList));
                                    break;
                                case "positive_rip_amplitude":
                                    jcadReadings.setPositiveRipAmplitude(getValue(sudList));
                                    break;
                                case "positive_rip_mobility":
                                    jcadReadings.setPositiveRipMobility(getValue(sudList));
                                    break;
                                case "positive_rip_noise":
                                    jcadReadings.setPositiveRipNoise(getValue(sudList));
                                    break;
                                case "negative_rip_amplitude":
                                    jcadReadings.setNegativeRipAmplitude(getValue(sudList));
                                    break;
                                case "negative_rip_mobility":
                                    jcadReadings.setNegativeRipMobility(getValue(sudList));
                                    break;
                                case "negative_rip_noise":
                                    jcadReadings.setNegativeRipNoise(getValue(sudList));
                                    break;
                                case "pos_rip_chk_amp1":
                                    jcadReadings.setPosRipChkAmp1(getValue(sudList));
                                    break;
                                case "pos_rip_chk_mob1":
                                    jcadReadings.setPosRipChkMob1(getValue(sudList));
                                    break;
                                case "pos_rip_chk_amp2":
                                    jcadReadings.setPosRipChkAmp2(getValue(sudList));
                                    break;
                                case "pos_rip_chk_mob2":
                                    jcadReadings.setPosRipChkMob2(getValue(sudList));
                                    break;
                                case "pos_rip_chk_amp3":
                                    jcadReadings.setPosRipChkAmp3(getValue(sudList));
                                    break;
                                case "pos_rip_chk_mob3":
                                    jcadReadings.setPosRipChkMob3(getValue(sudList));
                                    break;
                                case "neg_rip_chk_amp1":
                                    jcadReadings.setNegRipChkAmp1(getValue(sudList));
                                    break;
                                case "neg_rip_chk_mob1":
                                    jcadReadings.setNegRipChkMob1(getValue(sudList));
                                    break;
                                case "neg_rip_chk_amp2":
                                    jcadReadings.setNegRipChkAmp2(getValue(sudList));
                                    break;
                                case "neg_rip_chk_mob2":
                                    jcadReadings.setNegRipChkMob2(getValue(sudList));
                                    break;
                                case "neg_rip_chk_amp3":
                                    jcadReadings.setNegRipChkAmp3(getValue(sudList));
                                    break;
                                case "neg_rip_chk_mob3":
                                    jcadReadings.setNegRipChkMob3(getValue(sudList));
                                    break;
                                case "pos_peak1_amp":
                                    jcadReadings.setPosPeak1Amp(getValue(sudList));
                                    break;
                                case "pos_peak1_mob":
                                    jcadReadings.setPosPeak1Mob(getValue(sudList));
                                    break;
                                case "pos_peak2_amp":
                                    jcadReadings.setPosPeak2Amp(getValue(sudList));
                                    break;
                                case "pos_peak2_mob":
                                    jcadReadings.setPosPeak2Mob(getValue(sudList));
                                    break;
                                case "pos_peak3_amp":
                                    jcadReadings.setPosPeak3Amp(getValue(sudList));
                                    break;
                                case "pos_peak3_mob":
                                    jcadReadings.setPosPeak3Mob(getValue(sudList));
                                    break;
                                case "neg_peak1_amp":
                                    jcadReadings.setNegPeak1Amp(getValue(sudList));
                                    break;
                                case "neg_peak1_mob":
                                    jcadReadings.setNegPeak1Mob(getValue(sudList));
                                    break;
                                case "neg_peak2_amp":
                                    jcadReadings.setNegPeak2Amp(getValue(sudList));
                                    break;
                                case "neg_peak2_mob":
                                    jcadReadings.setNegPeak2Mob(getValue(sudList));
                                    break;
                                case "neg_peak3_amp":
                                    jcadReadings.setNegPeak3Amp(getValue(sudList));
                                    break;
                                case "neg_peak3_mob":
                                    jcadReadings.setNegPeak3Mob(getValue(sudList));
                                    break;
                                case "material_name":
                                    // duplicate value 
                                    break;
                                case "material_class":
                                    // duplicate value 
                                    break;
                                case "cas_number":
                                    jcadReadings.setCASNumber(getValue(sudList));
                                    break;
                                case "concentration":
                                    // duplicate value 
                                    break;
                                case "bars":
                                    // duplicate value 
                                    break;
                                case "peak_bars":
                                    jcadReadings.setPeakBars(getValue(sudList));
                                    break;
                                case "message_1":
                                    jcadReadings.setMessage1(getValue(sudList));
                                    break;
                                case "message_2":
                                    jcadReadings.setMessage2(getValue(sudList));
                                    break;
                                case "message_3":
                                    jcadReadings.setMessage3(getValue(sudList));
                                    break;
                                case "message_4":
                                    jcadReadings.setMessage4(getValue(sudList));
                                    break;
                                case "message_5":
                                    jcadReadings.setMessage5(getValue(sudList));
                                    break;
                                case "message_6":
                                    jcadReadings.setMessage6(getValue(sudList));
                                    break;
                                case "message_7":
                                    jcadReadings.setMessage7(getValue(sudList));
                                    break;
                                case "message_8":
                                    jcadReadings.setMessage8(getValue(sudList));
                                    break;
                                case "hardware_ident":
                                    jcadReadings.setHardwareIdent(getValue(sudList), hpaISAInit.getModelProperty());
                                    break;
                                case "boot_loader":
                                    jcadReadings.setBootLoader(getValue(sudList));
                                    break;
                                case "hw_version_string":
                                    jcadReadings.setHWVersionString(getValue(sudList));
                                    break;
                                case "loader_software_drawing":
                                    jcadReadings.setLoaderSoftwareDrawing(getValue(sudList));
                                    break;
                                case "app_sw_drawing":
                                    jcadReadings.setAppSWDrawing(getValue(sudList));
                                    break;
                                case "serial_number":
                                    jcadReadings.setSerialNumber(getValue(sudList), hpaISAInit.getSerialNumberProperty());
                                    break;
                                case "positive_spectrum":
                                    jcadReadings.setPositiveSpectrum(getValue(sudList));
                                    break;
                                case "negative_spectrum":
                                    jcadReadings.setNegativeSpectrum(getValue(sudList));
                                    break;
                                default:
                                    myLogger.error("Unsupported SUD: " + sudList.item(attribute).getNodeValue());
                                    break;
                            }
                        }
                    }
                } while ((sud = sud.getNextSibling()) != null);
            }
        }

        // publish the event
        if ((gAgentType != null) && (gBar != null) && (gDose != null)) {
            firstReport = false;
            Integer gBarValue = Integer.parseInt(gBar);
            hpaISAInit.processReadingEvent(gAgentType, (gBarValue >= 3), gDose, gBarValue, "gbar", jcadReadings);
        } else if ((hAgentType != null) && (hBar != null) && (hDose != null)) {
            firstReport = false;
            Integer hBarValue = Integer.parseInt(hBar);
            hpaISAInit.processReadingEvent(hAgentType, (hBarValue >= 3), hDose, hBarValue, "hbar", jcadReadings);
        } else if (firstReport && sudList != null) {
            // call this for the first event so AdditionalProperties gets updated
            jcadReadings.setValueChanged(true);
            hpaISAInit.processReadingEvent("None", false, "0.0", 0, "none", jcadReadings);
            firstReport = false;
        } else {
        }
    }
    
    @Override
    public boolean sendReportISA(long connectionId, CcsiChannelEnum[] channel,
            String[] cacheKeys, String msg, CcsiAckNakEnum ack, long ackMsn,
            NakReasonCodeEnum nakCode, boolean ackRequired) {
        // msg may have more than one item in it so wrap it so it can be parsed
        String xmsg = "<x>" + msg + "</x>";

        // notify the PH that the message was sent
        hpaISAInit.sendSentReportMessage(channel, msg, ackMsn, cacheKeys);

        // ack the message
        hpaISAInit.sendRcvAckMessage(channel, msg, ackMsn, cacheKeys);

        try {
            InputSource source = new InputSource(new StringReader(xmsg));
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(source);

            // Always only 1 item in this list
            NodeList rootList = document.getChildNodes();

            // may be more than 1 item in this list
            NodeList childList = rootList.item(0).getChildNodes();
            Integer length = childList.getLength();

            // process every item in the list
            for (Integer item = 0; item < length; item++) {
                Node type = childList.item(item).getFirstChild();
                String nodeName = type.getNodeName();

                switch (nodeName) {
                    case "ReadingsChn": {
                        // get ReadingReport node
                        NodeList list = type.getChildNodes();
                        int listLength = list.getLength();
//                        System.out.println("Msg Length: " + listLength);

                        // may be multiple ReadingReports in this message
                        Node readingReport = type.getFirstChild();
                        do {
                            // get Data
                            Node data = readingReport.getFirstChild();
                            if (data != null) {
                                processReadingReportSUDList(data);
                            }
                            readingReport = readingReport.getNextSibling();
                        } while (readingReport != null);
                    }
                    break;
                    case "ConfigChn":
                        Node block = type.getFirstChild();
                        Node configItem = block.getFirstChild();
                        NamedNodeMap configList = configItem.getAttributes();
                        String name = null;
                        String value = null;
                        for (Integer configIndex = 0; configIndex < configList.getLength(); configIndex++) {
                            String configItemName = configList.item(configIndex).getNodeName();
                            String configItemValue = configList.item(configIndex).getNodeValue();
                            if (configItemName.equals("Name")) {
                                name = configItemValue;
                            } else if (configItemName.equals("Value")) {
                                value = configItemValue;
                            }
                        }
                        List<PropertyHandler> propertyList = hpaISAInit.getCustomTypePropertyHandlerList();
                        for (PropertyHandler property : propertyList) {
                            if (property.getName().equals(name)) {
                                property.setValue(value);
                                break;
                            }
                        }

                        break;
                    case "IdentChn":
                        // TODO: publish this
                        // TODO: do I want to report other things (SW version, 
                        // UUID, HW version, Host ID)?
                        break;

                    case "LocationChn":
                        String latitude = null;
                        String longitude = null;
                        String altitude = null;
                        Node locationReport = type.getFirstChild();
                        NamedNodeMap attributeList = locationReport.getAttributes();
                        for (Integer attributes = 0; attributes < attributeList.getLength(); attributes++) {
                            String locationName = attributeList.item(attributes).getNodeName();
                            switch (locationName) {
                                case "Lat":
                                    latitude = attributeList.item(attributes).getNodeValue();
                                    break;
                                case "Lon":
                                    longitude = attributeList.item(attributes).getNodeValue();
                                    break;
                                case "Alt":
                                    altitude = attributeList.item(attributes).getNodeValue();
                                    break;
                                default:
                                    myLogger.error("Unknown attribute to set: " + locationName);
                                    break;
                            }
                        }
//                        hpaISAInit.updatePosition(latitude, longitude, altitude);
                        break;
                    case "AlertsChn":
                        String alertName = null;
                        String alertType = null;
                        NamedNodeMap alertList = type.getAttributes();
                        for (Integer attributes = 0; attributes < alertList.getLength(); attributes++) {
                            if (alertList.item(attributes).getNodeName().equals("Event")) {
                                // gets ALERT, DEALERT, WARN, DEWARN or (initial case) NONE
                                alertType = alertList.item(attributes).getNodeValue();
                            }

                        }
                        Node readingNode = type.getFirstChild();
                        if (readingNode == null) {
                            // this will happen initially when the alertType == NONE
                            // there are no children and nothing to report
                            myLogger.info("First Child Null: " + xmsg);
                            break;
                        }
                        Node detectNode = readingNode.getFirstChild();
                        if (detectNode == null) {
                            myLogger.error("First Child Null: " + xmsg);
                            break;
                        }
                        Node sudNode = detectNode.getFirstChild();
                        if (sudNode == null) {
                            myLogger.error("First Child Null: " + xmsg);
                            break;
                        }

                        alertList = sudNode.getAttributes();
                        for (Integer attributes = 0; attributes < alertList.getLength(); attributes++) {
                            if (sudNode.getNodeName().equals("SUD")) {
                                switch (alertList.item(attributes).getNodeValue()) {
                                    case "GBar":
                                        alertName = "gbarAlarm";
                                        if (alertType != null) {
                                            gHarmful = ((!alertType.equals("NONE"))
                                                    && (!alertType.equals("DEALERT"))
                                                    && (!alertType.equals("DEWARN")));
                                        }
                                        break;
                                    case "HBar":
                                        alertName = "hbarAlarm";
                                        if (alertType != null) {
                                            hHarmful = ((!alertType.equals("NONE"))
                                                    && (!alertType.equals("DEALERT")));
                                        }
                                        break;
                                    default:
//                                        myLogger.error("Invalid value: " + alertList.item(attributes).getNodeValue());
                                        break;
                                }
                            }
                        }
                        if ((alertType != null) && (alertName != null)) {
                            hpaISAInit.processAlertEvent(alertName, alertType);
                        }
                        // TODO: do I need an alert ID?
                        break;
                    default:
                        myLogger.error("Not supported yet: " + nodeName);
                        break;
                }
            }

        } catch (IOException | ParserConfigurationException | DOMException | SAXException e) {
            myLogger.error("Exception: " + e.getMessage());
        }

        return true;
    }

    /**
     * scans the list of nodes for the one named "Value" and returns the value
     * of that node
     *
     * @param nodeList
     * @return
     */
    private String getValue(NamedNodeMap nodeList) {
        String value = null;
        for (Integer attribute = 0; attribute < nodeList.getLength(); attribute++) {
            if (nodeList.item(attribute).getNodeName().equals("Value")) {
                value = nodeList.item(attribute).getNodeValue();
                break;
            }
        }
        return value;
    }

    @Override
    public boolean sendAckISA(long connectionId, CcsiChannelEnum channel, long msn) {
        hpaISAInit.sendSentAckMessage(connectionId, channel, msn);
        return true;
    }

    @Override
    public boolean sendNakISA(long connectionId, CcsiChannelEnum channel, long msn, NakReasonCodeEnum nakCode, NakDetailCodeEnum nakDetailCode, String nakDetailMessag) {
        hpaISAInit.sendSentNakMessage(connectionId, channel, msn, nakCode, nakDetailCode, nakDetailMessag);
        return true;
    }

}
