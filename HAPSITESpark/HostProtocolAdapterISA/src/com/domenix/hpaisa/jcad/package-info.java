/**
 * Copyright (c) 2001-2020 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * <br><br>
 * Provides device specific functionality for the Host Protocol Adapter for the JCAD
 *
 */
package com.domenix.hpaisa.jcad;
