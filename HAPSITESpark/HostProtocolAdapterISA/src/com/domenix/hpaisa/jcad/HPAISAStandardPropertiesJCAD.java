/*
 * <put file name here>
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * <put description here>
 *
 */
package com.domenix.hpaisa.jcad;

import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.gps.GPSInterface;
import com.domenix.hpaisa.HPAISAInit;
import com.google.common.base.Optional;
import gov.isa.capabilities.CachingPropertyHandler;
import gov.isa.capabilities.PermanentPropertyHandler;
import gov.isa.capabilities.PropertyChangeListener;
import gov.isa.capabilities.PropertyHandler;
import gov.isa.capabilities.ReadyState;
import gov.isa.capabilities.ReportingState;
import gov.isa.capabilities.StandardCapabilities;
import gov.isa.model.BSOID;
import gov.isa.model.CustomTypeName;
import gov.isa.model.Degrees;
import gov.isa.model.GeographicPosition;
import gov.isa.model.Meters;
import gov.isa.model.ModelFactory;
import gov.isa.model.Mutability;
import gov.isa.model.Percent;
import gov.isa.model.PropertyDeclaration;
import gov.isa.model.TypeName;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.log4j.Logger;

/**
 *
 * @author jmerritt
 */
public class HPAISAStandardPropertiesJCAD {
        private final Logger myLogger = Logger.getLogger(HPAISAStandardPropertiesJCAD.class);
    private final HostNetworkSettings hostNetworkSettings;
    private GPSInterface gpsInterface;
    
    public HPAISAStandardPropertiesJCAD (HPAISAInit hpaISAInit, HostNetworkSettings settings)
    {
        hostNetworkSettings = settings;
    }
    
    /**
     * Creates a handler for the standard Manufacturer property. This property
     * value does not change, so a PermanentPropertyHandler is used.
     *
     * @param modelFactory
     * @return A PropertyHandler for the Manufacturer property.
     * @see PermanentPropertyHandler
     */
    public PropertyHandler createManufacturerProperty(ModelFactory modelFactory) {
        return new PermanentPropertyHandler(
                // Model factory
                modelFactory,
                // Property declaration
                StandardCapabilities.manufacturerProperty(
                        // Model factory
                        modelFactory,
                        // Mutability
                        modelFactory.newMutability(Mutability.Predefined.PERMANENT)),
                // Property value
                hostNetworkSettings.getSensorManufacturer()
        );
    }
    
    /**
     * Creates a handler for the standard Serial Number property. This property
     * value does not change, so a PermanentPropertyHandler is used.
     *
     * @param modelFactory
     * @return A PropertyHandler for the Serial Number property.
     * @see CachingPropertyHandler
     */
    public PropertyHandler createSerialNumberProperty(ModelFactory modelFactory) {
        PropertyHandler handler = new CachingPropertyHandler(
                // Model factory
                modelFactory,
                // Property declaration
                StandardCapabilities.serialNumberProperty(
                        // Model factory
                        modelFactory,
                        // Mutability
                        modelFactory.newMutability(Mutability.Predefined.READ_WRITE)),
                ReadyState.NOT_READY,
                ReportingState.NOT_REPORTING
        );
        return handler;
    }
    
    /**
     * Creates a handler for the standard Model Number property. This property
     * value does not change, so a PermanentPropertyHandler is used.
     *
     * @param modelFactory
     * @return A PropertyHandler for the Model Number property.
     * @see CachingPropertyHandler
     */
    public PropertyHandler createModelNumberProperty(ModelFactory modelFactory) {
        PropertyHandler handler = new CachingPropertyHandler (
                // Model factory
                modelFactory,
                // Property declaration
                StandardCapabilities.modelProperty(
                        // Model factory
                        modelFactory,
                        // Mutability
                        modelFactory.newMutability(Mutability.Predefined.READ_WRITE))
        );
        handler.setReady(ReadyState.NOT_READY);
        handler.setReporting(ReportingState.NOT_REPORTING);        

        return handler;
    }
    
    /**
     * Creates a handler for the standard Name property. This property
     * value does not change, so a PermanentPropertyHandler is used.
     *
     * @param modelFactory
     * @return A PropertyHandler for the Name property.
     * @see PermanentPropertyHandler
     */
    public PropertyHandler createNameProperty(ModelFactory modelFactory) {
        return new PermanentPropertyHandler(
                // Model factory
                modelFactory,
                // Property declaration
                StandardCapabilities.nameProperty(
                        // Model factory
                        modelFactory,
                        // Mutability
                        modelFactory.newMutability(Mutability.Predefined.PERMANENT)),
                // Property value
                hostNetworkSettings.getPHHostName()
        );
    }

    /**
     * Creates property handler for position using the custom property handler
     * class
     *
     * @param model_factory model factory to use to create data structures
     * @param latitude
     * @param longitude
     * @param altitude
     * @return property handler
     */
    public PropertyHandler createPositionPropertyHandler(ModelFactory model_factory,
            Double latitude, Double longitude, Double altitude, GPSInterface gpsInterface)
    {
        this.gpsInterface = gpsInterface;

        return new CustomPositionPropertyHandler(
                model_factory,
                StandardCapabilities.positionProperty( // property declaration
                        model_factory,
                        model_factory.newMutability(Mutability.Predefined.READ_WRITE)
                ),
                // TODO: from config if available
                createPosition(model_factory, latitude, longitude, altitude), // initial value
                ReadyState.NOT_READY,
                ReportingState.NOT_REPORTING
        );
    }    

    /**
     * a class that extends PropertyHandler and implements the getValue() and
     * doSetValue() methods
     */
    private class CustomPositionPropertyHandler extends PropertyHandler
    {

        private final AtomicReference<Object> currentPosition;

        public CustomPositionPropertyHandler(
                ModelFactory model_factory,
                PropertyDeclaration declaration,
                Object value,
                ReadyState ready,
                ReportingState reporting
        )
        {
            super(model_factory, declaration, ready, reporting);
            this.currentPosition = new AtomicReference<>(value);
        }

        @Override
        final public Object getValue()
        {
            return currentPosition.get();
        }

        @Override
        final protected boolean doSetValue(Object value)
        {
            if (value instanceof GeographicPosition)
            {
                GeographicPosition newPosition = (GeographicPosition) value;
                gpsInterface.setPosition(newPosition.getLatitude().degrees(), 
                        newPosition.getLongitude().degrees(), 
                        newPosition.getAltitude().get().meters());
                return true;
            }

            return false;
        }
    }

    public GeographicPosition createPosition(ModelFactory model_factory, 
            double lat, double lon, double meters)
    {
        return model_factory.newGeographicPosition(
                model_factory.newDegrees(lat), // latitude
                model_factory.newDegrees(lon), // longitude
                Optional.<Meters>of(model_factory.newMeters(meters)), // altitude (MSL)
                Optional.<Meters>absent(),
                Optional.<Meters>absent(),
                Optional.<Meters>absent(),
                Optional.<Degrees>absent(),
                Optional.<Degrees>absent()
        );
    }
    private BSOID createBSOID (ModelFactory model_factory, String id)
    {
        return model_factory.newBSOID(id);
    }
    /**
     * Creates a property change listener for listening to the changes in the
     * value of a property
     *
     * @return property change listener
     */
    public PropertyChangeListener createPropertyChangeListener()
    {

        return new PropertyChangeListener()
        {
            @Override
            public void onPropertyChanged(PropertyDeclaration declaration, Object newval)
            {

                myLogger.info(declaration.getName() + " property changed: " + newval.toString());

                if (declaration.getType() instanceof TypeName)
                {

                    TypeName standardTypeName = (TypeName) declaration.getType();

                    myLogger.info ("Standard type received: " + standardTypeName.value());

                    switch (standardTypeName.predef())
                    {
                        case GEOGRAPHIC_POSITION:

                            GeographicPosition position = (GeographicPosition) newval;

                            myLogger.info ("Position received: Lat: " + 
                                    position.getLatitude().degrees() + " Lon: " +
                                    position.getLongitude().degrees() + " Alt: " +
                                    position.getAltitude().get().meters()
                            );

                            break;

                        // add cases for other types here, if desired
                        default:
                            break;
                    }

                } else if (declaration.getType() instanceof CustomTypeName)
                {
                    CustomTypeName customTypeName = (CustomTypeName) declaration.getType();

                    myLogger.info ("Custom type received: " + customTypeName.value());
                }
            }
        };
    }

    /**
     * Creates a caching property handler for the Identity property
     *
     * @param model_factory model factory to use to create data structures
     * @param sidc 2525c symbol to use for identity
     * @return a property handler for the Identity Property
     */
    public PropertyHandler createIdentityHandler(
            ModelFactory model_factory,
            String sidc
    )
    {
        return new PermanentPropertyHandler(
                model_factory,
                StandardCapabilities.identityProperty(
                        model_factory,
                        model_factory.newMutability(
                                Mutability.Predefined.PERMANENT
                        )
                ),
                model_factory.newStandardIdentity(
                        model_factory.newSIDC(sidc),
                        Optional.<Percent>absent()
                )
        );
    }
}
