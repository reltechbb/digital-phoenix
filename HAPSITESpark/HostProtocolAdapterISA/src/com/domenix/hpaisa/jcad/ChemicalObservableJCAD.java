/*
 * ChemicalObservableJCAD
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Chemical Observable for JCAD
 *
 */
package com.domenix.hpaisa.jcad;

import com.domenix.hpaisa.BSOObservable;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import gov.isa.behaviors.AbstractComponentBehavior;
import gov.isa.behaviors.BehaviorControls;
import gov.isa.behaviors.ComponentBehavior;
import gov.isa.capabilities.CachingPropertyHandler;
import gov.isa.capabilities.ObservableHandler;
import gov.isa.capabilities.PropertyHandler;
import gov.isa.capabilities.ReadyState;
import gov.isa.capabilities.ReportingState;
import gov.isa.capabilities.StandardCapabilities;
import gov.isa.model.ChemicalMaterialClass;
import gov.isa.model.ChemicalReading;
import gov.isa.model.Count;
import gov.isa.model.CustomType;
import gov.isa.model.CustomTypeDeclaration;
import gov.isa.model.Degrees;
import gov.isa.model.EventType;
import gov.isa.model.FieldDeclaration;
import gov.isa.model.GeographicPosition;
import gov.isa.model.Kgpm3;
import gov.isa.model.MeasuredKgpm2;
import gov.isa.model.MeasuredKgpm3;
import gov.isa.model.MeasuredKgsecpm3;
import gov.isa.model.MeasuredPpm;
import gov.isa.model.MeasuredSecs;
import gov.isa.model.Meters;
import gov.isa.model.ModelFactory;
import gov.isa.model.Mutability;
import gov.isa.model.NameValuePair;
import gov.isa.model.Percent;
import gov.isa.model.RangeDeclaration;
import gov.isa.model.ThresholdDeclaration;
import gov.isa.model.TypeName;
import gov.isa.model.UTC;
import gov.isa.util.TimeUtils;
import org.apache.log4j.Logger;

/**
 * Chemical Observable for JCAD
 *
 * @author jmerritt
 */
public class ChemicalObservableJCAD {

    private ObservableHandler clientChemical;
    public BehaviorControls chemicalBehaviorControls;
    private final Logger myLogger = Logger.getLogger(ChemicalObservableJCAD.class);
    private final ModelFactory modelFactory;
    private ObservableHandler clientBSO;
    private BSOObservable bsoObservable;
    private ObservableHandler clientPosition;
    private ObservableHandler clientIdentity;
    private CustomTypeDeclaration customTypeDeclaration;
    private PropertyHandler customPropertyHandler;
    private Boolean reportBSO = false;
    
    public ChemicalObservableJCAD(ModelFactory modelFactory) {
        this.modelFactory = modelFactory;
        if (System.getProperty("reportBSO") != null) {
            reportBSO = System.getProperty("reportBSO").equalsIgnoreCase("true");
        }
    }

    public ComponentBehavior createChemicalEvent(ModelFactory model_factory, String bsoID) {
        customTypeDeclaration = createCustomTypeDeclaration(model_factory);
        customPropertyHandler = createCustomTypePropertyHandler(model_factory, customTypeDeclaration);
//        customTypeObservableHandler = createCustomObservableHandler(model_factory);

        clientChemical = createChemicalObservable(model_factory);
        clientPosition = createPositionObservable(model_factory);
        bsoObservable = new BSOObservable(model_factory, bsoID);
        clientBSO = bsoObservable.createBSOObservable(model_factory);
        clientIdentity = createIdentityObservable(model_factory);

        if (reportBSO) {
            return new AbstractComponentBehavior() {

                @Override
                public ImmutableSet<ObservableHandler> getObservables() {
                    return ImmutableSet.<ObservableHandler>of(
                            clientChemical,
                            clientBSO,
                            clientPosition,
                            clientIdentity//,
                            //customTypeObservableHandler
                    );
                }

                @Override
                public ImmutableSet<PropertyHandler> getProperties() {
                    return ImmutableSet.<PropertyHandler>of( // leave this code commented out
                            // otherwise these properties get reported in the Status message
                            //                        customPropertyHandler // declare the property handler
                            );
                }

                @Override
                public void initialize(BehaviorControls controls) {
                    chemicalBehaviorControls = controls;
                }

                @Override
                public ImmutableSet<CustomTypeDeclaration> getCustomTypes() {
                    return ImmutableSet.<CustomTypeDeclaration>of(
                            customTypeDeclaration // declare the POC extended type
                    );
                }
            ;
        }  
            ;} else {
            return new AbstractComponentBehavior() {

                @Override
                public ImmutableSet<ObservableHandler> getObservables() {
                    return ImmutableSet.<ObservableHandler>of(
                            clientChemical,
                            //                        clientBSO,
                            clientPosition//,
                            //                        clientIdentity,
                            //customTypeObservableHandler
                    );
                }

                @Override
                public ImmutableSet<PropertyHandler> getProperties() {
                    return ImmutableSet.<PropertyHandler>of( // leave this code commented out
                            // otherwise these properties get reported in the Status message
                            //                        customPropertyHandler // declare the property handler
                            );
                }

                @Override
                public void initialize(BehaviorControls controls) {
                    chemicalBehaviorControls = controls;
                }

                @Override
                public ImmutableSet<CustomTypeDeclaration> getCustomTypes() {
                    return ImmutableSet.<CustomTypeDeclaration>of(
                            customTypeDeclaration // declare the POC extended type
                    );
                }
            ;
        }

    

    ;
}
    }
        
/*    private ObservableHandler createCustomObservableHandler(ModelFactory model_factory) {
        ExtendedCapabilities.MethodObservableHandler customObservableHandler = new ExtendedCapabilities.MethodObservableHandler(
                model_factory,
                ReadyState.READY,
                ReportingState.REPORTING
        );
        return customObservableHandler;
    }*/

    private PropertyHandler createCustomTypePropertyHandler(ModelFactory model_factory, CustomTypeDeclaration declaration) {
        PropertyHandler propertyHandler = new CachingPropertyHandler(
                model_factory, // model factory
                model_factory.newPropertyDeclaration( // property declaration
                        "AdditionalMeasurements",// name
                        "Additional Measurements", // description
                        model_factory.newCustomTypeName(declaration.getName()), // type
                        //                                model_factory.newCustomTypeName(declaration.getName()), // type
                        model_factory.newMutability(Mutability.Predefined.READ_WRITE), // mutability
                        model_factory.newSingleStructure(), // structure
                        Optional.<RangeDeclaration>absent(), // range declaration
                        Optional.<ThresholdDeclaration>absent() // threshold declaration
                ),
                createCustomTypeValue(model_factory), // initial value
                ReadyState.READY, // ready state
                ReportingState.REPORTING // reporting state
        );
        return propertyHandler;
    }

    private CustomTypeDeclaration createCustomTypeDeclaration(ModelFactory model_factory) {
        return model_factory.newCustomTypeDeclaration(
                "AdditionalMeasurements",
                "Additional Measurements",
                ImmutableList.<FieldDeclaration>of(
                        model_factory.newFieldDeclaration(
                                "positive_fg_volts", // name
                                "positive_fg_volts", // description
                                model_factory.newTypeName(TypeName.Predefined.INTEGER), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "negative_fg_volts", // name
                                "negative_fg_volts", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_noise_g", // name
                                "pos_noise_g", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_noise_h", // name
                                "neg_noise_h", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "posMobCalibFac", // name
                                "posMobCalibFac", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "negMobCalibFac", // name
                                "negMobCalibFac", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_temperature", // name
                                "pos_temperature", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_temperature", // name
                                "neg_temperature", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_rip_amp", // name
                                "pos_rip_amp", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_rip_mob", // name
                                "pos_rip_mob", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_rip_noise", // name
                                "pos_rip_noise", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_rip_amp", // name
                                "neg_rip_amp", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_rip_mob", // name
                                "neg_rip_mob", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_rip_noise", // name
                                "neg_rip_noise", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_rip_chk_amp1",
                                "pos_rip_chk_amp1", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_rip_chk_mob1",
                                "pos_rip_chk_mob1", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_rip_chk_amp2",
                                "pos_rip_chk_amp2", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_rip_chk_mob2",
                                "pos_rip_chk_mob2", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_rip_chk_amp3",
                                "pos_rip_chk_amp3", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_rip_chk_mob3",
                                "pos_rip_chk_mob3", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_rip_chk_amp1",
                                "neg_rip_chk_amp1", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_rip_chk_mob1",
                                "neg_rip_chk_mob1", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_rip_chk_amp2",
                                "neg_rip_chk_amp2", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_rip_chk_mob2",
                                "neg_rip_chk_mob2", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_rip_chk_amp3",
                                "neg_rip_chk_amp3", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_rip_chk_mob3",
                                "neg_rip_chk_mob3", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_peak1_amp",
                                "pos_peak1_amp", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_peak1_mob",
                                "pos_peak1_mob", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_peak2_amp",
                                "pos_peak2_amp", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_peak2_mob",
                                "pos_peak2_mob", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_peak3_amp",
                                "pos_peak3_amp", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_peak3_mob",
                                "pos_peak3_mob", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_peak1_amp",
                                "neg_peak1_amp", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_peak1_mob",
                                "neg_peak1_mob", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_peak2_amp",
                                "neg_peak2_amp", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_peak2_mob",
                                "neg_peak2_mob", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_peak3_amp",
                                "neg_peak3_amp", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_peak3_mob",
                                "neg_peak3_mob", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        //                        model_factory.newFieldDeclaration(
                        //                                "material_name",
                        //                                "material_name", // description
                        //                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                        //                                model_factory.newSingleStructure(), // structure
                        //                                false // optional
                        //                        ),
                        //                        model_factory.newFieldDeclaration(
                        //                                "material_class",
                        //                                "material_class", // description
                        //                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                        //                                model_factory.newSingleStructure(), // structure
                        //                                false // optional
                        //                        ),
                        /*                        model_factory.newFieldDeclaration(
                                "cas_number",
                                "cas_number", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),*/
                        //                        model_factory.newFieldDeclaration(
                        //                                "concentration",
                        //                                "concentration", // description
                        //                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                        //                                model_factory.newSingleStructure(), // structure
                        //                                false // optional
                        //                        ),
                        //                        model_factory.newFieldDeclaration(
                        //                                "bars",
                        //                                "bars", // description
                        //                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                        //                                model_factory.newSingleStructure(), // structure
                        //                                false // optional
                        //                        ),
                        model_factory.newFieldDeclaration(
                                "peak_bars",
                                "peak_bars", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "pos_spectrum",
                                "pos_spectrum", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "neg_spectrum",
                                "neg_spectrum", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        )
                ));
    }

    private CustomType createCustomTypeValue(ModelFactory model_factory) {
        return model_factory.newCustomType(
                ImmutableList.<NameValuePair>of(
                        model_factory.newNameValuePair("positive_fg_volts", "0.0"),
                        model_factory.newNameValuePair("negative_fg_volts", "0.0"),
                        model_factory.newNameValuePair("pos_noise_g", "0"),
                        model_factory.newNameValuePair("neg_noise_h", "0"),
                        model_factory.newNameValuePair("posMobCalibFac", "0"),
                        model_factory.newNameValuePair("negMobCalibFac", "0"),
                        model_factory.newNameValuePair("pos_temperature", "0"),
                        model_factory.newNameValuePair("neg_temperature", "0"),
                        model_factory.newNameValuePair("positive_rip_amplitude", "0"),
                        model_factory.newNameValuePair("positive_rip_mobility", "0"),
                        model_factory.newNameValuePair("positive_rip_noise", "0"),
                        model_factory.newNameValuePair("negative_rip_amplitude", "0"),
                        model_factory.newNameValuePair("negative_rip_mobility", "0"),
                        model_factory.newNameValuePair("negative_rip_noise", "0"),
                        model_factory.newNameValuePair("pos_rip_chk_amp1", "0"),
                        model_factory.newNameValuePair("pos_rip_chk_mob1", "0"),
                        model_factory.newNameValuePair("pos_rip_chk_amp2", "0"),
                        model_factory.newNameValuePair("pos_rip_chk_mob2", "0"),
                        model_factory.newNameValuePair("pos_rip_chk_amp3", "0"),
                        model_factory.newNameValuePair("pos_rip_chk_mob3", "0"),
                        model_factory.newNameValuePair("neg_rip_chk_amp1", "0"),
                        model_factory.newNameValuePair("neg_rip_chk_mob1", "0"),
                        model_factory.newNameValuePair("neg_rip_chk_amp2", "0"),
                        model_factory.newNameValuePair("neg_rip_chk_mob2", "0"),
                        model_factory.newNameValuePair("neg_rip_chk_amp3", "0"),
                        model_factory.newNameValuePair("neg_rip_chk_mob3", "0"),
                        model_factory.newNameValuePair("pos_peak1_amp", "0"),
                        model_factory.newNameValuePair("pos_peak1_mob", "0"),
                        model_factory.newNameValuePair("pos_peak2_amp", "0"),
                        model_factory.newNameValuePair("pos_peak2_mob", "0"),
                        model_factory.newNameValuePair("pos_peak3_amp", "0"),
                        model_factory.newNameValuePair("pos_peak3_mob", "0"),
                        model_factory.newNameValuePair("neg_peak1_amp", "0"),
                        model_factory.newNameValuePair("neg_peak1_mob", "0"),
                        model_factory.newNameValuePair("neg_peak2_amp", "0"),
                        model_factory.newNameValuePair("neg_peak2_mob", "0"),
                        model_factory.newNameValuePair("neg_peak3_amp", "0"),
                        model_factory.newNameValuePair("neg_peak3_mob", "0"),
                        //                        model_factory.newNameValuePair("material_name", "NONE"),
                        //                        model_factory.newNameValuePair("material_class", "NONE"),
                        //                        model_factory.newNameValuePair("cas_number", "NONE"),
                        model_factory.newNameValuePair("concentration", "0.0"),
                        //                        model_factory.newNameValuePair("bars", "0"),
                        model_factory.newNameValuePair("peak_bars", "0"),
                        model_factory.newNameValuePair("pos_spectrum", "NONE"),
                        model_factory.newNameValuePair("pos_spectrum", "NONE")
                ));
    }

    private ObservableHandler createChemicalObservable(ModelFactory model_factory) {
        return new ObservableHandler(
                model_factory,
                StandardCapabilities.chemicalReadingObservable(model_factory),
                ReadyState.READY,
                ReportingState.REPORTING
        );
    }

    private ObservableHandler createIdentityObservable(ModelFactory model_factory) {
        return new ObservableHandler(
                model_factory,
                StandardCapabilities.identityObservable(model_factory),
                ReadyState.READY,
                ReportingState.REPORTING
        );
    }

    private ObservableHandler createPositionObservable(ModelFactory model_factory) {
        return new ObservableHandler(
                model_factory,
                StandardCapabilities.positionObservable(model_factory),
                ReadyState.READY,
                ReportingState.REPORTING
        );
    }

    private void updateAdditionalMeasurements(JCADReadings readings) {
        CustomType newValues = readings.getAdditionalMeasurements(modelFactory);
        customPropertyHandler.setValue(newValues);
    }

    public void publishHBarEvent(String materialName, Boolean harmful,
            String density, Integer hbar, Double latitude,
            Double longitude, Double altitude, String identity, JCADReadings readings) {
        EventType eventType;
        if (harmful) {
            eventType = modelFactory.newEventType(EventType.Predefined.ALARM);
        } else {
            eventType = modelFactory.newEventType(EventType.Predefined.MEASUREMENT);
        }

        // convert mgpm3 to Kgpm3
        Double dose = Double.parseDouble(density)/1000.0;
        UTC start = TimeUtils.currentTimeUTC(modelFactory);
        UTC stale = TimeUtils.after(modelFactory, start, 30.0);
        ChemicalMaterialClass.Predefined materialClass;
        
        if (materialName.startsWith("TIC"))
        {
            materialClass = ChemicalMaterialClass.Predefined.TOXIC_INDUSTRIAL_CHEMICAL;
        } else {
            materialClass = ChemicalMaterialClass.Predefined.NERVE_AGENT;
        }
        
        ChemicalReading reading = modelFactory.newChemicalReading(
                Optional.<ChemicalMaterialClass>of(modelFactory.newChemicalMaterialClass(materialClass)),
                Optional.<String>of(materialName),
                Optional.<String>of(readings.getCASNumber()), // service number
                Optional.<Boolean>of(harmful),
                Optional.<MeasuredKgpm3>of(modelFactory.newMeasuredKgpm3(
                        modelFactory.newKgpm3(dose), Optional.<Kgpm3>absent())),
                Optional.<MeasuredSecs>absent(), // duration
                Optional.<MeasuredKgsecpm3>absent(), // concetration time
                Optional.<MeasuredKgpm2>absent(), // deposition
                Optional.<MeasuredPpm>absent(), // mass fraction
                Optional.<Count>absent(), // GBar reading
                Optional.<Count>of(modelFactory.newCount(hbar.longValue(), Optional.<Long>absent())));

        updateAdditionalMeasurements(readings);
        if (reportBSO) {
            chemicalBehaviorControls.getComponent().observe(
                    start,
                    Optional.<UTC>of(stale),
                    eventType,
                    clientChemical.observe(reading),
                    clientPosition.observe(createPosition(modelFactory, latitude, longitude, altitude)),
                    clientBSO.observe(bsoObservable.getBSOID()),
                    clientIdentity.observe(modelFactory.newStandardIdentity(modelFactory.newSIDC(identity), Optional.<Percent>absent())),
                    customPropertyHandler.observe());
        } else {
            chemicalBehaviorControls.getComponent().observe(
                    start,
                    Optional.<UTC>of(stale),
                    eventType,
                    clientChemical.observe(reading),
                    // Tom sez' report position on properties only
                    //                clientPosition.observe(createPosition(modelFactory, latitude, longitude, altitude)), 
                    // Uncomment these lines to put BSO and Identity back
                    //                clientBSO.observe(bsoObservable.getBSOID()), 
                    //                clientIdentity.observe(modelFactory.newStandardIdentity(modelFactory.newSIDC(identity), Optional.<Percent>absent())),
                    customPropertyHandler.observe());

        }
    }

    public void publishGBarEvent(String materialName, Boolean harmful,
            String density, Integer gbar, Double latitude,
            Double longitude, Double altitude, String identity, JCADReadings readings) {
        EventType eventType;
        if (harmful) {
            eventType = modelFactory.newEventType(EventType.Predefined.ALARM);
        } else {
            eventType = modelFactory.newEventType(EventType.Predefined.MEASUREMENT);
        }

        updateAdditionalMeasurements(readings);

        // convert mgpm3 to Kgpm3
        Double dose = Double.parseDouble(density)/1000.0;
        UTC start = TimeUtils.currentTimeUTC(modelFactory);
        UTC stale = TimeUtils.after(modelFactory, start, 30.0);
        ChemicalMaterialClass.Predefined materialClass = ChemicalMaterialClass.Predefined.G_AGENT;
        ChemicalReading reading = modelFactory.newChemicalReading(
                Optional.<ChemicalMaterialClass>of(modelFactory.newChemicalMaterialClass(materialClass)),
                Optional.<String>of(materialName),
                Optional.<String>of(readings.getCASNumber()), // service number
                Optional.<Boolean>of(harmful),
                Optional.<MeasuredKgpm3>of(modelFactory.newMeasuredKgpm3(
                        modelFactory.newKgpm3(dose), Optional.<Kgpm3>absent())),
                Optional.<MeasuredSecs>absent(), // duration
                Optional.<MeasuredKgsecpm3>absent(), // concenration time
                Optional.<MeasuredKgpm2>absent(), // deposition
                Optional.<MeasuredPpm>absent(), // mass fraction
                Optional.<Count>of(modelFactory.newCount(gbar.longValue(), Optional.<Long>absent())), // GBar reading
                Optional.<Count>absent()); // HBar reading

        if (reportBSO) {
            chemicalBehaviorControls.getComponent().observe(
                    start,
                    Optional.<UTC>of(stale),
                    eventType,
                    clientChemical.observe(reading),
                    clientPosition.observe(createPosition(modelFactory, latitude, longitude, altitude)),
                    clientBSO.observe(bsoObservable.getBSOID()),
                    clientIdentity.observe(modelFactory.newStandardIdentity(modelFactory.newSIDC(identity), Optional.<Percent>absent())),
                    customPropertyHandler.observe());
        } else {
            chemicalBehaviorControls.getComponent().observe(
                    start,
                    Optional.<UTC>of(stale),
                    eventType,
                    clientChemical.observe(reading),
                    //                clientPosition.observe(createPosition(modelFactory, latitude, longitude, altitude)), 
                    // Uncomment these lines to put BSO and Identity back
                    //                clientBSO.observe(bsoObservable.getBSOID()), 
                    //                clientIdentity.observe(modelFactory.newStandardIdentity(modelFactory.newSIDC(identity), Optional.<Percent>absent())),
                    customPropertyHandler.observe());
        }
    }
 
    private GeographicPosition createPosition(ModelFactory model_factory,
            double lat, double lon, double meters) {
        return model_factory.newGeographicPosition(
                model_factory.newDegrees(lat), // latitude
                model_factory.newDegrees(lon), // longitude
                Optional.<Meters>of(model_factory.newMeters(meters)), // altitude (MSL)
                Optional.<Meters>absent(),
                Optional.<Meters>absent(),
                Optional.<Meters>absent(),
                Optional.<Degrees>absent(),
                Optional.<Degrees>absent()
        );
    }

    public void updateAlertState(String name, String state, PropertyHandler propertyHandler) {
        /*        Component component = chemicalBehaviorControls.getComponent();
        CustomType type = modelFactory.newCustomType(
                ImmutableList.<NameValuePair>of(
                        modelFactory.newNameValuePair(name, state)));
        Observation observation = Observation.createObservation(propertyHandler, type);
        UTC start = TimeUtils.currentTimeUTC(modelFactory);
        EventReference event = component.observe(start, 
                Optional.<UTC>absent(), 
                modelFactory.newEventType(EventType.Predefined.ALARM), 
                observation, clientBSO.observe(bsoObservable.getBSOID()));

        myLogger.info ("Alert name: " + name + " state: " + state);*/
    }
}
