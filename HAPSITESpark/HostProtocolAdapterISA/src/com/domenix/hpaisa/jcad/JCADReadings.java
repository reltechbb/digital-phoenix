/*
 * JCADReadings.java
 *
 * Copyright (c) 2001-2020 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Container for holding and formatting custom JCAD propertis
 *
 */
package com.domenix.hpaisa.jcad;

import com.google.common.collect.ImmutableList;
import gov.isa.capabilities.PropertyHandler;
import gov.isa.capabilities.ReadyState;
import gov.isa.capabilities.ReportingState;
import gov.isa.model.CustomType;
import gov.isa.model.ModelFactory;
import gov.isa.model.NameValuePair;
import java.util.ArrayList;

/**
 * Container for holding and formatting custom JCAD properties
 * @author jmerritt
 */
public class JCADReadings {
    // bit masks
    // warning flags
    protected static final Integer SIEVE_LOW_WARNING_MASK = 0x0001;
    protected static final Integer CALIBRATION_MODE_MASK = 0x0002;
    protected static final Integer CHANGE_BATTERY_MASK = 0x0004;
    protected static final Integer BATTERY_LOW_MASK = 0x0010;
    // major fault flags
    protected static final Integer PERSISTENT_HEALTH_MASK = 0x0002;
    protected static final Integer EEPROM_CHECKSUM_MASK = 0x0004;
    protected static final Integer HEALTH_CHK_MASK = 0x0008;
    protected static final Integer VIBRATION_DETECTED_MASK = 0x0040;
    protected static final Integer DATALOG_FAULT_MASK = 0x0200;
    protected static final Integer CLOCK_BATTERY_FAULT_MASK = 0x1000;
    // fault flags
    protected static final Integer CHANGE_SIEVE_PACK_MASK = 0x0001;
    protected static final Integer MAJOR_FAULT_MASK = 0x0020;
    protected static final Integer CHANGE_BATTERY_FAULT_MASK = 0x0040;
    
    // additional properties
    private String drawing_number = "0";
    private String issue_number = "0";
    private String system_id = "0";
    private String detection_mode = "UNKNOWN";
    private String system_alert_status = "NONE";
    private String saturation_protection = "false";
    private String operating_mode = "NONE";
    private String clock = "0";
    private String sieve_life_remaining = "0";
    private String atmospheric_pressure = "0";
    private String sensor_temperature = "0";
    protected Integer warningFlags = 0;  //bit field
    protected Integer majorFaultFlags = 0; //bit field
    protected Integer faultFlags = 0;  //bit field
    private String power_status = "0";
    private String runtime_hrs = "0";
    private String runtime_min = "0";
    private String message_1 = "NONE";
    private String message_2 = "NONE";
    private String message_3 = "NONE";
    private String message_4 = "NONE";
    private String message_5 = "NONE";
    private String message_6 = "NONE";
    private String message_7 = "NONE";
    private String message_8 = "NONE";
    private String hardware_ident = "NONE";
    private String boot_loader = "NONE";
    private String hw_version_string = "NONE";
    private String loader_software_drawing = "NONE";
    private String app_sw_drawing = "NONE";
    private String serial_number = "NONE";
    
    private Boolean valueChanged = false;
    // only send the clock once
    // only set the model and serial number once
    private Boolean firstReport = true;

    // additional measurements
    private String positive_fg_volts = "0.0";
    private String negative_fg_volts = "0.0";
    private String pos_noise_g = "0";
    private String neg_noise_h = "0";
    private String posMobCalibFac = "0";
    private String negMobCalibFac = "0";
    private String pos_temperature = "0";
    private String neg_temperature = "0";
    private String pos_rip_amp = "0";
    private String pos_rip_mob = "0";
    private String pos_rip_noise = "0";
    private String neg_rip_amp = "0";
    private String neg_rip_mob = "0";
    private String neg_rip_noise = "0";
    private String pos_rip_chk_amp1 = "0";
    private String pos_rip_chk_mob1 = "0";
    private String pos_rip_chk_amp2 = "0";
    private String pos_rip_chk_mob2 = "0";
    private String pos_rip_chk_amp3 = "0";
    private String pos_rip_chk_mob3 = "0";
    private String neg_rip_chk_amp1 = "0";
    private String neg_rip_chk_mob1 = "0";
    private String neg_rip_chk_amp2 = "0";
    private String neg_rip_chk_mob2 = "0";
    private String neg_rip_chk_amp3 = "0";
    private String neg_rip_chk_mob3 = "0";
    private String pos_peak1_amp = "0";
    private String pos_peak1_mob = "0";
    private String pos_peak2_amp = "0";
    private String pos_peak2_mob = "0";
    private String pos_peak3_amp = "0";
    private String pos_peak3_mob = "0";
    private String neg_peak1_amp = "0";
    private String neg_peak1_mob = "0";
    private String neg_peak2_amp = "0";
    private String neg_peak2_mob = "0";
    private String neg_peak3_amp = "0";
    private String neg_peak3_mob = "0";
//    private String material_name = "NONE";
//    private String material_class = "NONE";
    private String cas_number = "NONE";
//    private String concentration = "0.0";
//    private String bars = "0";
    private String peak_bars = "0";
    private String positive_spectrum = "";
    private String negative_spectrum = "";

    public JCADReadings ()
    {
        
    }

    public void setValueChanged (Boolean value)
    {
        valueChanged = value;
    }
    public Boolean getValueChanged ()
    {
        return valueChanged;
    }
    // Setters for properties
    public void setDrawingNumber (String value)
    {
        drawing_number = value;
    }
    public void setIssueNumber (String value)
    {
        issue_number = value;
    }
    public void setSystemID (String value)
    {
        system_id = value;
    }
    public void setDetectionMode (String value)
    {
        if (!detection_mode.equals(value)) {
            detection_mode = value;
            valueChanged = true;
        }
    }
    public void setSystemAlertStatus(String value) {
        if (!system_alert_status.equals(value)) {
            system_alert_status = value;
            valueChanged = true;
        }
    }
    public void setSaturationProtection(String value) {
        if (!saturation_protection.equals(value)) {
            saturation_protection = value;
            valueChanged = true;
        }
    }
    public void setOperatingMode(String value) {
        if (!operating_mode.equals(value)) {
            operating_mode = value;
            valueChanged = true;
        }
    }
    public void setClock(String value) {
        if (!clock.equals(value)) {
            clock = value;
        }
    }
    public void setSieveLifeRemaining (String value)
    {
        if (!sieve_life_remaining.equals(value))
        {
            sieve_life_remaining = value;
            valueChanged = true;
        }
    }
    public void setAtmosphericPressure (String value)
    {
        // pressure is in mBars
        if (!atmospheric_pressure.equals(value))
        {
            Integer ap = Integer.parseInt(atmospheric_pressure);
            Integer newVal = Integer.parseInt(value);
            // only update if pressue has changed by more than 50 mBars
            if (Math.abs(ap - newVal) > 50) {
                atmospheric_pressure = value;
                valueChanged = true;
            }
        }
    }
    public void setSensorTemperature (String value)
    {
        if (!sensor_temperature.equals(value))
        {
            sensor_temperature = value;
            valueChanged = true;
        }
    }
    public void setWarningFlags (String value)
    {
        Integer intValue = Integer.parseInt(value);
        if (!warningFlags.equals(intValue))
        {
            warningFlags = intValue;
            valueChanged = true;
        }
    }
    public void setMajorFaultFlags (String value)
    {
        Integer intValue = Integer.parseInt(value);
        if (!majorFaultFlags.equals(intValue))
        {
            majorFaultFlags = intValue;
            valueChanged = true;
        }
    }
    public void setFaultFlags (String value)
    {
        Integer intValue = Integer.parseInt(value);
        if (!faultFlags.equals(intValue))
        {
            faultFlags = intValue;
            valueChanged = true;
        }
    }
    public void setPowerStatus (String value)
    {
        if (!power_status.equals(value))
        {
            power_status = value;
            valueChanged = true;
        }
    }
    public void setRuntimeMin (String value)
    {
        if (!runtime_min.equals(value))
        {
            Integer min = Integer.parseInt(runtime_min);
            Integer newVal = Integer.parseInt(value);
            // only update if runtime_min has changed by more than 5 min
            if (Math.abs(min - newVal) > 5)
            {
                runtime_min = value;
                valueChanged = true;
            }
        }
    }
    public void setRuntimeHrs (String value)
    {
        if (!runtime_hrs.equals(value))
        {
            runtime_hrs = value;
            valueChanged = true;
        }
    }
    public void setMessage1 (String value)
    {
        if (!message_1.equals(value))
        {
            message_1 = value;
            valueChanged = true;
        }
    }
    public void setMessage2 (String value)
    {
        if (!message_2.equals(value))
        {
            message_2 = value;
            valueChanged = true;
        }
    }
    public void setMessage3 (String value)
    {
        if (!message_3.equals(value))
        {
            message_3 = value;
            valueChanged = true;
        }
    }
    public void setMessage4 (String value)
    {
        if (!message_4.equals(value))
        {
            message_4 = value;
            valueChanged = true;
        }
    }
    public void setMessage5 (String value)
    {
        if (!message_5.equals(value))
        {
            message_5 = value;
            valueChanged = true;
        }
    }
    public void setMessage6 (String value)
    {
        if (!message_6.equals(value))
        {
            message_6 = value;
            valueChanged = true;
        }
    }
    public void setMessage7 (String value)
    {
        if (!message_7.equals(value))
        {
            message_7 = value;
            valueChanged = true;
        }
    }
    public void setMessage8 (String value)
    {
        if (!message_8.equals(value))
        {
            message_8 = value;
            valueChanged = true;
        }
    }
    public void setHardwareIdent (String value, PropertyHandler propertyHandler)
    {
        if (firstReport)
        {
            {
                if (propertyHandler.getName().equals("Model"))
                {
                    hardware_ident = value;
                    Boolean result = propertyHandler.setValue(value);
                    if (propertyHandler.getReady() == ReadyState.NOT_READY) {
                        propertyHandler.setReady(ReadyState.READY);
                        propertyHandler.setReporting(ReportingState.REPORTING);
                    }
                }
            }
        }
    }
    public void setBootLoader (String value)
    {
        if (!boot_loader.equals(value))
        {
            boot_loader = value;
            valueChanged = true;
        }
    }
    public void setHWVersionString (String value)
    {
        if (!hw_version_string.equals(value))
        {
            hw_version_string = value;
            valueChanged = true;
        }
    }
    public void setLoaderSoftwareDrawing (String value)
    {
        if (!loader_software_drawing.equals(value))
        {
            loader_software_drawing = value;
            valueChanged = true;
        }
    }
    public void setAppSWDrawing (String value)
    {
        if (!app_sw_drawing.equals(value))
        {
            app_sw_drawing = value;
            valueChanged = true;
        }
    }
    public void setSerialNumber (String value, PropertyHandler propertyHandler)
    {
        if (firstReport) {
            {
                if (propertyHandler.getName().equals("Serial Number")) {
                    serial_number = value;
                    Boolean result = propertyHandler.setValue(value);
                    propertyHandler.setReady(ReadyState.READY);
                    propertyHandler.setReporting(ReportingState.REPORTING);
                }
            }
        }
    }
    
    // Setters for measurements
    public void setPositiveFGVolts (String value)
    {
        positive_fg_volts = value;
    }
    public void setNegativeFGVolts (String value)
    {
        negative_fg_volts = value;
    }
    public void setPosNoiseG (String value)
    {
        pos_noise_g = value;
    }
    public void setNegNoiseH (String value)
    {
        neg_noise_h = value;
    }
    public void setPosMobCalibFac (String value)
    {
        posMobCalibFac = value;
    }
    public void setNegMobCalibFac (String value)
    {
        negMobCalibFac = value;
    }
    public void setPosTemperature (String value)
    {
        pos_temperature = value;
    }
    public void setNegTemperature (String value)
    {
        neg_temperature = value;
    }
    public void setPositiveRipAmplitude (String value)
    {
        pos_rip_amp = value;
    }
    public void setPositiveRipMobility (String value)
    {
        pos_rip_mob = value;
    }
    public void setPositiveRipNoise (String value)
    {
        pos_rip_noise = value;
    }
    public void setNegativeRipAmplitude (String value)
    {
        neg_rip_amp = value;
    }
    public void setNegativeRipMobility (String value)
    {
        neg_rip_mob = value;
    }
    public void setNegativeRipNoise (String value)
    {
        neg_rip_noise = value;
    }
    public void setPosRipChkAmp1 (String value)
    {
        pos_rip_chk_amp1 = value;
    }
    public void setPosRipChkMob1 (String value)
    {
        pos_rip_chk_mob1 = value;
    }
    public void setPosRipChkAmp2 (String value)
    {
        pos_rip_chk_amp2 = value;
    }
    public void setPosRipChkMob2 (String value)
    {
        pos_rip_chk_mob2 = value;
    }
    public void setPosRipChkAmp3 (String value)
    {
        pos_rip_chk_amp3 = value;
    }
    public void setPosRipChkMob3 (String value)
    {
        pos_rip_chk_mob3 = value;
    }
    public void setNegRipChkAmp1 (String value)
    {
        neg_rip_chk_amp1 = value;
    }
    public void setNegRipChkMob1 (String value)
    {
        neg_rip_chk_mob1 = value;
    }
    public void setNegRipChkAmp2 (String value)
    {
        neg_rip_chk_amp2 = value;
    }
    public void setNegRipChkMob2 (String value)
    {
        neg_rip_chk_mob2 = value;
    }
    public void setNegRipChkAmp3 (String value)
    {
        neg_rip_chk_amp3 = value;
    }
    public void setNegRipChkMob3 (String value)
    {
        neg_rip_chk_mob3 = value;
    }
    public void setPosPeak1Amp (String value)
    {
        pos_peak1_amp = value;
    }
    public void setPosPeak1Mob (String value)
    {
        pos_peak1_mob = value;
    }
    public void setPosPeak2Amp (String value)
    {
        pos_peak2_amp = value;
    }
    public void setPosPeak2Mob (String value)
    {
        pos_peak2_mob = value;
    }
    public void setPosPeak3Amp (String value)
    {
        pos_peak3_amp = value;
    }
    public void setPosPeak3Mob (String value)
    {
        pos_peak3_mob = value;
    }
    public void setNegPeak1Amp (String value)
    {
        neg_peak1_amp = value;
    }
    public void setNegPeak1Mob (String value)
    {
        neg_peak1_mob = value;
    }
    public void setNegPeak2Amp (String value)
    {
        neg_peak2_amp = value;
    }
    public void setNegPeak2Mob (String value)
    {
        neg_peak2_mob = value;
    }
    public void setNegPeak3Amp (String value)
    {
        neg_peak3_amp = value;
    }
    public void setNegPeak3Mob (String value)
    {
        neg_peak3_mob = value;
    }
//    public void setMaterialName (String value)
//    {
//        material_name = value;
//    }
    public void setCASNumber (String value)
    {
        cas_number = value;
    }
    public String getCASNumber ()
    {
        return cas_number;
    }
//    public void setMaterialClass (String value)
//    {
//        material_class = value;
//    }
//    public void setConcentration (String value)
//    {
//        concentration = value;
//    }
//    public void setBars (String value)
//    {
//        bars = value;
//    }
    public void setPeakBars (String value)
    {
        peak_bars = value;
    }
    public void setPositiveSpectrum (String value)
    {
        positive_spectrum = value;
    }
    public void setNegativeSpectrum (String value)
    {
        negative_spectrum = value;
    }

    public CustomType getAdditionalProperties (ModelFactory model_factory)
    {
        ArrayList <NameValuePair> list = new ArrayList <>();
        if (valueChanged == true)
        {
            valueChanged = false;
            list.add (model_factory.newNameValuePair("drawing_number", drawing_number));
            list.add (model_factory.newNameValuePair("issue_number", issue_number));
            list.add (model_factory.newNameValuePair("system_id", system_id));
            list.add (model_factory.newNameValuePair("detection_mode", detection_mode));
            list.add (model_factory.newNameValuePair("system_alert_status", system_alert_status));
            list.add (model_factory.newNameValuePair("saturation_protection", saturation_protection));
            list.add (model_factory.newNameValuePair("operating_mode", operating_mode));
            if (firstReport)
            {
                // only send clock first time
                list.add (model_factory.newNameValuePair("clock", clock));
                firstReport = false;
            }
            list.add (model_factory.newNameValuePair("sieve_life_remaining", sieve_life_remaining));
            list.add (model_factory.newNameValuePair("atmospheric_pressure", atmospheric_pressure));
            list.add (model_factory.newNameValuePair("sensor_temperature", sensor_temperature));

            list.add (model_factory.newNameValuePair("sieve_low_warning",  (((Boolean)((warningFlags & SIEVE_LOW_WARNING_MASK)!= 0)).toString())));
            list.add (model_factory.newNameValuePair("calibration_mode",   (((Boolean)((warningFlags & CALIBRATION_MODE_MASK)!= 0)).toString())));
            list.add (model_factory.newNameValuePair("change_battery",     (((Boolean)((warningFlags & CHANGE_BATTERY_MASK)!= 0)).toString())));
            list.add (model_factory.newNameValuePair("battery_low_warning",(((Boolean)((warningFlags & BATTERY_LOW_MASK)!= 0)).toString())));
            
            list.add (model_factory.newNameValuePair("persistent_health_fault", (((Boolean)((majorFaultFlags & PERSISTENT_HEALTH_MASK)!= 0)).toString())));
            list.add (model_factory.newNameValuePair("eeprom_checksum_fault",   (((Boolean)((majorFaultFlags & EEPROM_CHECKSUM_MASK)!= 0)).toString())));
            list.add (model_factory.newNameValuePair("health_chk_fault",        (((Boolean)((majorFaultFlags & HEALTH_CHK_MASK)!= 0)).toString())));
            list.add (model_factory.newNameValuePair("vibration_detected",      (((Boolean)((majorFaultFlags & VIBRATION_DETECTED_MASK)!= 0)).toString())));
            list.add (model_factory.newNameValuePair("datalog_fault",           (((Boolean)((majorFaultFlags & DATALOG_FAULT_MASK)!= 0)).toString())));
            list.add (model_factory.newNameValuePair("clock_battery_fault",     (((Boolean)((majorFaultFlags & CLOCK_BATTERY_FAULT_MASK)!= 0)).toString())));
            
            list.add (model_factory.newNameValuePair("change_sieve_pack",    (((Boolean)((faultFlags & CHANGE_SIEVE_PACK_MASK)!= 0)).toString())));
            list.add (model_factory.newNameValuePair("major_fault",          (((Boolean)((faultFlags & MAJOR_FAULT_MASK)!= 0)).toString())));
            list.add (model_factory.newNameValuePair("change_battery_fault", (((Boolean)((faultFlags & CHANGE_BATTERY_FAULT_MASK)!= 0)).toString())));

            list.add (model_factory.newNameValuePair("power_status", power_status));
            list.add (model_factory.newNameValuePair("runtime_hrs", runtime_hrs));
            list.add (model_factory.newNameValuePair("runtime_min", runtime_min));
            
            list.add (model_factory.newNameValuePair("message_1", message_1));
            list.add (model_factory.newNameValuePair("message_2", message_2));
            list.add (model_factory.newNameValuePair("message_3", message_3));
            list.add (model_factory.newNameValuePair("message_4", message_4));
            list.add (model_factory.newNameValuePair("message_5", message_5));
            list.add (model_factory.newNameValuePair("message_6", message_6));
            list.add (model_factory.newNameValuePair("message_7", message_7));
            list.add (model_factory.newNameValuePair("message_8", message_8));
//            list.add (model_factory.newNameValuePair("hardware_ident", hardware_ident));
            list.add (model_factory.newNameValuePair("boot_loader", boot_loader));
            list.add (model_factory.newNameValuePair("hw_version_string", hw_version_string));
            list.add (model_factory.newNameValuePair("loader_software_drawing", loader_software_drawing));
            list.add (model_factory.newNameValuePair("app_sw_drawing", app_sw_drawing));
//            list.add (model_factory.newNameValuePair("serial_number", serial_number));
        }
        return  model_factory.newCustomType(ImmutableList.copyOf(list));
    }
    
    public CustomType getAdditionalMeasurements (ModelFactory model_factory)
    {
        ArrayList <NameValuePair> list = new ArrayList <>();
        list.add (model_factory.newNameValuePair("positive_fg_volts", positive_fg_volts));
        list.add (model_factory.newNameValuePair("negative_fg_volts", negative_fg_volts));
        list.add (model_factory.newNameValuePair("pos_noise_g", pos_noise_g));
        list.add (model_factory.newNameValuePair("neg_noise_h", neg_noise_h));
        list.add (model_factory.newNameValuePair("posMobCalibFac", posMobCalibFac));
        list.add (model_factory.newNameValuePair("negMobCalibFac", negMobCalibFac));

        list.add (model_factory.newNameValuePair("pos_temperature", pos_temperature));
        list.add (model_factory.newNameValuePair("neg_temperature", neg_temperature));
        list.add (model_factory.newNameValuePair("pos_rip_amp", pos_rip_amp));
        list.add (model_factory.newNameValuePair("pos_rip_mob", pos_rip_mob));
        list.add (model_factory.newNameValuePair("pos_rip_noise", pos_rip_noise));
        list.add (model_factory.newNameValuePair("neg_rip_amp", neg_rip_amp));
        list.add (model_factory.newNameValuePair("neg_rip_mob", neg_rip_mob));
        list.add (model_factory.newNameValuePair("neg_rip_noise", neg_rip_noise));
        list.add (model_factory.newNameValuePair("pos_rip_chk_amp1", pos_rip_chk_amp1));
        list.add (model_factory.newNameValuePair("pos_rip_chk_mob1", pos_rip_chk_mob1));
        list.add (model_factory.newNameValuePair("pos_rip_chk_amp2", pos_rip_chk_amp2));
        list.add (model_factory.newNameValuePair("pos_rip_chk_mob2", pos_rip_chk_mob2));
        list.add (model_factory.newNameValuePair("pos_rip_chk_amp3", pos_rip_chk_amp3));
        list.add (model_factory.newNameValuePair("pos_rip_chk_mob3", pos_rip_chk_mob3));
        list.add (model_factory.newNameValuePair("neg_rip_chk_amp1", neg_rip_chk_amp1));
        list.add (model_factory.newNameValuePair("neg_rip_chk_mob1", neg_rip_chk_mob1));
        list.add (model_factory.newNameValuePair("neg_rip_chk_amp2", neg_rip_chk_amp2));
        list.add (model_factory.newNameValuePair("neg_rip_chk_mob2", neg_rip_chk_mob2));
        list.add (model_factory.newNameValuePair("neg_rip_chk_amp3", neg_rip_chk_amp3));
        list.add (model_factory.newNameValuePair("neg_rip_chk_mob3", neg_rip_chk_mob3));
        list.add (model_factory.newNameValuePair("pos_peak1_amp", pos_peak1_amp));
        list.add (model_factory.newNameValuePair("pos_peak1_mob", pos_peak1_mob));
        list.add (model_factory.newNameValuePair("pos_peak2_amp", pos_peak2_amp));
        list.add (model_factory.newNameValuePair("pos_peak2_mob", pos_peak2_mob));
        list.add (model_factory.newNameValuePair("pos_peak3_amp", pos_peak3_amp));
        list.add (model_factory.newNameValuePair("pos_peak3_mob", pos_peak3_mob));
        list.add (model_factory.newNameValuePair("neg_peak1_amp", neg_peak1_amp));
        list.add (model_factory.newNameValuePair("neg_peak1_mob", neg_peak1_mob));
        list.add (model_factory.newNameValuePair("neg_peak2_amp", neg_peak2_amp));
        list.add (model_factory.newNameValuePair("neg_peak2_mob", neg_peak2_mob));
        list.add (model_factory.newNameValuePair("neg_peak3_amp", neg_peak3_amp));
        list.add (model_factory.newNameValuePair("neg_peak3_mob", neg_peak3_mob));
        // duplicate list.add (model_factory.newNameValuePair("material_name", material_name));
//        list.add (model_factory.newNameValuePair("material_class", material_class));
// service_number in Chemical Readings
//        list.add (model_factory.newNameValuePair("cas_number", cas_number));
//        list.add (model_factory.newNameValuePair("concentration", concentration));
//        list.add (model_factory.newNameValuePair("bars", bars));
        list.add (model_factory.newNameValuePair("peak_bars", peak_bars));
        list.add (model_factory.newNameValuePair("positive_spectrum", positive_spectrum));
        list.add (model_factory.newNameValuePair("negative_spectrum", negative_spectrum));

        return model_factory.newCustomType(ImmutableList.copyOf(list));
    }
}
