/*
 * ISACustomTypesJCAD.java
 *
 * Copyright (c) 2001-2020 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Custom Types for the JCAD
 *
 */

package com.domenix.hpaisa.jcad;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import gov.isa.capabilities.CachingPropertyHandler;
import gov.isa.capabilities.CommandHandler;
import gov.isa.capabilities.PropertyChangeListener;
import gov.isa.capabilities.PropertyHandler;
import gov.isa.capabilities.ReadyState;
import gov.isa.capabilities.ReportingState;
import gov.isa.model.CustomType;
import gov.isa.model.CustomTypeDeclaration;
import gov.isa.model.CustomTypeName;
import gov.isa.model.FieldDeclaration;
import gov.isa.model.ModelFactory;
import gov.isa.model.Mutability;
import gov.isa.model.NameValuePair;
import gov.isa.model.PropertyDeclaration;
import gov.isa.model.RangeDeclaration;
import gov.isa.model.ThresholdDeclaration;
import gov.isa.model.TypeName;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.log4j.Logger;

/**
 * Custom type definitions for the JCAD
 * @author dricciardi
 */
public class ISACustomTypesJCAD {
    private final Logger myLogger = Logger.getLogger(ISACustomTypesJCAD.class);

    private final HPAISAJCADInitImpl hpaISAInit;

    /**
     * Constructor
     * @param init 
     */
    public ISACustomTypesJCAD (HPAISAJCADInitImpl init)
    {
        hpaISAInit = init;
    }

    /**
     * Creates custom types: rateThreshold, accumulatedDoseThreshold, rateAlarm and doseAlarm
     *
     * @param model_factory
     * @return custom type declaration
     */
    public List<CustomTypeDeclaration> createCustomTypeDeclaration(ModelFactory model_factory)
    {
        List<CustomTypeDeclaration> customTypeDeclarationList = new ArrayList<>();
      
        customTypeDeclarationList.add(model_factory.newCustomTypeDeclaration(
                "additionalProperties",
                "Additional Properties",
                ImmutableList.<FieldDeclaration>of(
                        model_factory.newFieldDeclaration(
                                "drawing_number", // name
                                "Drawing Number", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "issue_number", // name
                                "Issue Number", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "system_id", // name
                                "SystemID", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "detection_mode", // name
                                "Detection Mode", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "system_alert_status", // name
                                "System Alert Status", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "saturation_protection", // name
                                "saturation protection", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "operating_mode", // name
                                "operating_mode", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "clock", // name
                                "Clock", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "sieve_life_remaining", // name
                                "sieve_life_remaining", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "atmospheric_pressure", // name
                                "atmospheric_pressure", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "sensor_temperature", // name
                                "sensor_temperature", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        
                        model_factory.newFieldDeclaration(
                                "sieve_low_warning", // name
                                "sieve_low_warning", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "calibration_mode", // name
                                "calibration_mode", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "change_battery", // name
                                "change_battery", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "battery_low_warning", // name
                                "battery low warning", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "health_chk_fault", // name
                                "health check fault", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "vibration_detected", // name
                                "vibration_detected", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "datalog_fault", // name
                                "datalog_fault", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "clock_battery_fault", // name
                                "clock_battery_fault", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "persistent_health_ fault", // name
                                "persistent health fault", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "eeprom_checksum_fault", // name
                                "eeprom checksum fault", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "change_sieve_pack", // name
                                "change_sieve_pack", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "major_fault", // name
                                "major_fault", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "change_battery_fault", // name
                                "change_battery_fault", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "power_status", // name
                                "power_status", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "runtime_hrs", // name
                                "runtime_hrs", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "runtime_min", // name
                                "runtime_min", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "message_1", // name
                                "message_1", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "message_2", // name
                                "message_2", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "message_3", // name
                                "message_3", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "message_4", // name
                                "message_4", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "message_5", // name
                                "message_5", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "message_6", // name
                                "message_6", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "message_7", // name
                                "message_7", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "message_8", // name
                                "message_8", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
// standard property - model
//                        model_factory.newFieldDeclaration(
//                                "hardware_ident", // name
//                                "hardware_ident", // description
//                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
//                                model_factory.newSingleStructure(), // structure
//                                false // optional
//                        ),
                        model_factory.newFieldDeclaration(
                                "boot_loader", // name
                                "boot_loader", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "hw_version_string", // name
                                "hw_version_string", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "loader_software_drawing", // name
                                "loader_software_drawing", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "app_sw_drawing", // name
                                "app_sw_drawing", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        )
// standard property - serial number
//                        model_factory.newFieldDeclaration(
//                                "serial_number", // name
//                                "serial_number", // description
//                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
//                                model_factory.newSingleStructure(), // structure
//                                false // optional
//                        )
                )));

        return customTypeDeclarationList;
    }

    /**
     * Creates a property handler for the color property using the declared
     * custom type
     *
     * @param model_factory model factory to use to create data structures
     * @param typeDeclarationList the list of type declarations
     * @return list custom property handlers
     */
    public List<PropertyHandler> createCustomTypePropertyHandler(
            ModelFactory model_factory, List<CustomTypeDeclaration> typeDeclarationList) {
        List<PropertyHandler> customTypePropertyHandlerList = new ArrayList<>();

        for (CustomTypeDeclaration declaration : typeDeclarationList) {
                PropertyHandler propertyHandler = new CachingPropertyHandler(
                        model_factory, // model factory
                        model_factory.newPropertyDeclaration( // property declaration
                                "AdditionalProperties",// name
                                "Additional Properties", // description
                                model_factory.newCustomTypeName(declaration.getName()), // type
                                model_factory.newMutability(Mutability.Predefined.READ_WRITE), // mutability
                                model_factory.newSingleStructure(), // structure
                                Optional.<RangeDeclaration>absent(), // range declaration
                                Optional.<ThresholdDeclaration>absent() // threshold declaration
                        ),
                        createCustomTypeValue (model_factory), // initial value
                        ReadyState.NOT_READY, // ready state
                        ReportingState.REPORTING // reporting state
                );
                customTypePropertyHandlerList.add(propertyHandler);
        }
        return customTypePropertyHandlerList;
    }
    
    /**
     * Creates a property change listener for listening to the changes in the
     * value of a property
     *
     * @return property change listener
     */
    public PropertyChangeListener createPropertyChangeListener()
    {

        return new PropertyChangeListener()
        {
            @Override
            public void onPropertyChanged(PropertyDeclaration declaration, Object newval)
            {

                myLogger.debug("Property changed: " + declaration.getName() + " " + newval.toString());

                if (declaration.getType() instanceof CustomTypeName)
                {

                    CustomTypeName customTypeName = (CustomTypeName) declaration.getType();

                    myLogger.debug("Custom type received: " + customTypeName.value());

                    // handle custom type here, if desired
                }
            }
        };
    }

    private CustomType createCustomTypeValue(ModelFactory model_factory)
    {
            return model_factory.newCustomType(
                ImmutableList.<NameValuePair>of(
                        model_factory.newNameValuePair("drawing_number", "1"),
                        model_factory.newNameValuePair("issue_number", "2"),
                        model_factory.newNameValuePair("system_id", "2"),
                        model_factory.newNameValuePair("detection_mode", "MONITOR"),
                        model_factory.newNameValuePair("system_alert_status", "NONE"),
                        model_factory.newNameValuePair("saturation_protection", "2"),
                        model_factory.newNameValuePair("operating_mode", "2"),
                        model_factory.newNameValuePair("clock", "2"),
                        model_factory.newNameValuePair("sieve_life_remaining", "2"),
                        model_factory.newNameValuePair("atmospheric_pressure", "2"),
                        model_factory.newNameValuePair("sensor_temperature", "2"),
                        
                        model_factory.newNameValuePair("sieve_low_warning", "false"),
                        model_factory.newNameValuePair("calibration_mode", "false"),
                        model_factory.newNameValuePair("change_battery", "false"),
                        model_factory.newNameValuePair("battery_low_warning", "false"),
                        model_factory.newNameValuePair("health_chk_fault", "false"),
                        model_factory.newNameValuePair("vibration_detected", "false"),
                        model_factory.newNameValuePair("datalog_fault", "false"),
                        model_factory.newNameValuePair("clock_battery_fault", "false"),
                        model_factory.newNameValuePair("persistent_health_fault", "false"),
                        model_factory.newNameValuePair("eeprom_checksum_fault", "false"),
                        model_factory.newNameValuePair("change_sieve_pack", "false"),
                        model_factory.newNameValuePair("major_fault", "false"),
                        model_factory.newNameValuePair("change_battery_fault", "false"),

                        model_factory.newNameValuePair("power_status", "0"),
                        model_factory.newNameValuePair("runtime_hrs", "0"),
                        model_factory.newNameValuePair("runtime_min", "0"),
                        model_factory.newNameValuePair("message_1", "NONE"),
                        model_factory.newNameValuePair("message_2", "NONE"),
                        model_factory.newNameValuePair("message_3", "NONE"),
                        model_factory.newNameValuePair("message_4", "NONE"),
                        model_factory.newNameValuePair("message_5", "NONE"),
                        model_factory.newNameValuePair("message_6", "NONE"),
                        model_factory.newNameValuePair("message_7", "NONE"),
                        model_factory.newNameValuePair("message_8", "NONE"),
//                        model_factory.newNameValuePair("hardware_ident", "NONE"),
                        model_factory.newNameValuePair("boot_loader", "NONE"),
                        model_factory.newNameValuePair("hw_version_string", "NONE")
//                        model_factory.newNameValuePair("serial_number", "NONE")
                ));
    }
    
  /**
   * Create a custom command handler for a command ClearCommand
   *
   * @param model_factory
   * @return custom command handler
   */
  public List <CommandHandler> createCustomCommandHandler( ModelFactory model_factory ) {

    List <CommandHandler> customCommandHandlerList = new ArrayList <>();

    return customCommandHandlerList;
  }
    /**
     * a class that extends PropertyHandler and implements the getValue() and
     * doSetValue() methods
     */
    private class CustomPropertyHandler extends PropertyHandler
    {

        private final AtomicReference<Object> current;
        private final PropertyDeclaration declaration;

        public CustomPropertyHandler(
                ModelFactory model_factory,
                PropertyDeclaration declaration,
                Object value,
                ReadyState ready,
                ReportingState reporting
        )
        {
            super(model_factory, declaration, ready, reporting);
            this.declaration = declaration;
            this.current = new AtomicReference<>(value);
        }

        @Override
        final public Object getValue()
        {
            myLogger.info ("getValue on: " + declaration.getName() + " value=" + current.toString());
            return current.get();
        }

        @Override
        final protected boolean doSetValue(Object value) {
            boolean retVal = true;
            myLogger.info("doSetValue on: " + declaration.getName() + " value=" + value.toString());

            current.set(value);

            return retVal;
        }
    }

    /**
     * Create a command handler for a command called "Move"
     *
     * @param model_factory
     * @return command handler
     */
    public List<CommandHandler> createStandardCommandHandler(final ModelFactory model_factory) {
        List<CommandHandler> commandHandlerList = new ArrayList<>();

        return commandHandlerList;
  }
}
