/*
 * ChemicalObservable.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Observable that reports detection of chemicals and alerts
 */
package com.domenix.hpaisa.lcd;

import com.domenix.hpaisa.BSOObservable;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import gov.isa.behaviors.AbstractComponentBehavior;
import gov.isa.behaviors.BehaviorControls;
import gov.isa.behaviors.ComponentBehavior;
import gov.isa.capabilities.ObservableHandler;
import gov.isa.capabilities.PropertyHandler;
import gov.isa.capabilities.ReadyState;
import gov.isa.capabilities.ReportingState;
import gov.isa.capabilities.StandardCapabilities;
import gov.isa.model.ChemicalMaterialClass;
import gov.isa.model.ChemicalReading;
import gov.isa.model.Count;
import gov.isa.model.Degrees;
import gov.isa.model.EventType;
import gov.isa.model.GeographicPosition;
import gov.isa.model.Kgpm3;
import gov.isa.model.MeasuredKgpm2;
import gov.isa.model.MeasuredKgpm3;
import gov.isa.model.MeasuredKgsecpm3;
import gov.isa.model.MeasuredPpm;
import gov.isa.model.MeasuredSecs;
import gov.isa.model.Meters;
import gov.isa.model.ModelFactory;
import gov.isa.model.Percent;
import gov.isa.model.UTC;
import gov.isa.util.TimeUtils;
import org.apache.log4j.Logger;

/**
 * Observable that reports detection of chemicals and alerts
 * @author jmerritt
 */
public class ChemicalObservable {
    private ObservableHandler clientChemical;
    public BehaviorControls chemicalBehaviorControls;
    private final Logger myLogger = Logger.getLogger(ChemicalObservable.class);
    private final ModelFactory modelFactory;
    private ObservableHandler clientBSO;
    private BSOObservable bsoObservable;
    private ObservableHandler clientPosition;
    private ObservableHandler clientIdentity;
    
    public ChemicalObservable (ModelFactory modelFactory)
    {
        this.modelFactory = modelFactory;
    }
    
    public ComponentBehavior createChemicalEvent(ModelFactory model_factory, String bsoID)
    {
        clientChemical = createChemicalObservable(model_factory);
        clientPosition = createPositionObservable(model_factory);
        bsoObservable = new BSOObservable (model_factory, bsoID);
        clientBSO = bsoObservable.createBSOObservable (model_factory);
        clientIdentity = createIdentityObservable (model_factory);

        return new AbstractComponentBehavior()
        {

            @Override
            public ImmutableSet<ObservableHandler> getObservables()
            {
                return ImmutableSet.<ObservableHandler>of(
                        clientChemical,
                        clientBSO,
                        clientPosition,
                        clientIdentity
                );
            }

            @Override
            public void initialize(BehaviorControls controls)
            {
                chemicalBehaviorControls = controls;
            }
        ;
    }

    ;
    }
        
    private ObservableHandler createChemicalObservable (ModelFactory model_factory)
    {
        return new ObservableHandler(
                model_factory,
                StandardCapabilities.chemicalReadingObservable(model_factory),
                ReadyState.READY,
                ReportingState.REPORTING
        );
    }
    
        
    private ObservableHandler createIdentityObservable(ModelFactory model_factory)
    {
        return new ObservableHandler(
                model_factory,
                StandardCapabilities.identityObservable(model_factory),
                ReadyState.READY,
                ReportingState.REPORTING
        );
    }
    
    private ObservableHandler createPositionObservable (ModelFactory model_factory)
    {
        return new ObservableHandler(
                model_factory,
                StandardCapabilities.positionObservable(model_factory),
                ReadyState.READY,
                ReportingState.REPORTING
        );
    }

    public void publishHBarEvent(String materialName, Boolean harmful, 
            String density, Integer hbar, Double latitude,
            Double longitude, Double altitude, String identity)
    {
        EventType eventType;
        if (harmful)
        {
            eventType = modelFactory.newEventType(EventType.Predefined.ALARM);
        } else
        {
            eventType = modelFactory.newEventType(EventType.Predefined.MEASUREMENT);
        }

        Double dose = Double.parseDouble(density);
        UTC start = TimeUtils.currentTimeUTC(modelFactory);
        UTC stale = TimeUtils.after(modelFactory, start, 30.0);
        ChemicalMaterialClass.Predefined materialClass = ChemicalMaterialClass.Predefined.NERVE_AGENT;
        ChemicalReading reading = modelFactory.newChemicalReading(
                Optional.<ChemicalMaterialClass>of(modelFactory.newChemicalMaterialClass(materialClass)), 
                Optional.<String>of(materialName), 
                Optional.<String>absent(), // service number
                Optional.<Boolean>of(harmful), 
                Optional.<MeasuredKgpm3>of(modelFactory.newMeasuredKgpm3(
                        modelFactory.newKgpm3(dose), Optional.<Kgpm3>absent())), 
                Optional.<MeasuredSecs>absent(), // duration
                Optional.<MeasuredKgsecpm3>absent(), // concetration time
                Optional.<MeasuredKgpm2>absent(), // deposition
                Optional.<MeasuredPpm>absent(), // mass fraction
                Optional.<Count>absent(), // GBar reading
                Optional.<Count>of(modelFactory.newCount(hbar.longValue(), Optional.<Long>absent())));

        chemicalBehaviorControls.getComponent().observe(
                start,
                Optional.<UTC>of(stale),
                eventType,
                clientChemical.observe(reading),
                clientPosition.observe(createPosition(modelFactory, latitude, longitude, altitude)), 
                clientBSO.observe(bsoObservable.getBSOID()), 
                clientIdentity.observe(modelFactory.newStandardIdentity(modelFactory.newSIDC(identity), Optional.<Percent>absent())));
    }

    public void publishGBarEvent(String materialName, Boolean harmful, 
        String density, Integer gbar, Double latitude,
        Double longitude, Double altitude, String identity)
    {
        EventType eventType;
        if (harmful)
        {
            eventType = modelFactory.newEventType(EventType.Predefined.ALARM);
        } else
        {
            eventType = modelFactory.newEventType(EventType.Predefined.MEASUREMENT);
        }

        Double dose = Double.parseDouble(density);
        UTC start = TimeUtils.currentTimeUTC(modelFactory);
        UTC stale = TimeUtils.after(modelFactory, start, 30.0);
        ChemicalMaterialClass.Predefined materialClass = ChemicalMaterialClass.Predefined.G_AGENT;
        ChemicalReading reading = modelFactory.newChemicalReading(
                Optional.<ChemicalMaterialClass>of(modelFactory.newChemicalMaterialClass(materialClass)), 
                Optional.<String>of(materialName), 
                Optional.<String>absent(), // service number
                Optional.<Boolean>of(harmful), 
                Optional.<MeasuredKgpm3>of(modelFactory.newMeasuredKgpm3(
                        modelFactory.newKgpm3(dose), Optional.<Kgpm3>absent())), 
                Optional.<MeasuredSecs>absent(), // duration
                Optional.<MeasuredKgsecpm3>absent(), // concenration time
                Optional.<MeasuredKgpm2>absent(), // deposition
                Optional.<MeasuredPpm>absent(), // mass fraction
                Optional.<Count>of(modelFactory.newCount(gbar.longValue(), Optional.<Long>absent())),
                Optional.<Count>absent()); // HBar reading

        chemicalBehaviorControls.getComponent().observe(
                start,
                Optional.<UTC>of(stale),
                eventType,
                clientChemical.observe(reading),
                clientPosition.observe(createPosition(modelFactory, latitude, longitude, altitude)), 
                clientBSO.observe(bsoObservable.getBSOID()), 
                clientIdentity.observe(modelFactory.newStandardIdentity(modelFactory.newSIDC(identity), Optional.<Percent>absent())));
    }
    
    private GeographicPosition createPosition(ModelFactory model_factory, 
            double lat, double lon, double meters)
    {
        return model_factory.newGeographicPosition(
                model_factory.newDegrees(lat), // latitude
                model_factory.newDegrees(lon), // longitude
                Optional.<Meters>of(model_factory.newMeters(meters)), // altitude (MSL)
                Optional.<Meters>absent(),
                Optional.<Meters>absent(),
                Optional.<Meters>absent(),
                Optional.<Degrees>absent(),
                Optional.<Degrees>absent()
        );
    }
    public void updateAlertState(String name, String state, PropertyHandler propertyHandler)
    {
/*        Component component = chemicalBehaviorControls.getComponent();
        CustomType type = modelFactory.newCustomType(
                ImmutableList.<NameValuePair>of(
                        modelFactory.newNameValuePair(name, state)));
        Observation observation = Observation.createObservation(propertyHandler, type);
        UTC start = TimeUtils.currentTimeUTC(modelFactory);
        EventReference event = component.observe(start, 
                Optional.<UTC>absent(), 
                modelFactory.newEventType(EventType.Predefined.ALARM), 
                observation, clientBSO.observe(bsoObservable.getBSOID()));

        myLogger.info ("Alert name: " + name + " state: " + state);*/
    }
}
