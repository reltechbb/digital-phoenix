/*
 * HPAISABase.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Implements the interface with the protocol handler
 */
package com.domenix.hpaisa;

import com.domenix.common.spark.CtlWrapper;
import com.domenix.common.spark.CtlWrapperQueue;
import com.domenix.common.spark.IPCQueueEvent;
import com.domenix.common.spark.IPCQueueListenerInterface;
import com.domenix.commonha.ccsi.CcsiAckNakEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.commonha.intfc.HostProtocolAdapterInterface;
import com.domenix.commonha.intfc.NSDSConnEvent;
import com.domenix.commonha.intfc.NSDSConnectionList;
import com.domenix.commonha.intfc.NSDSInterface;
import com.domenix.commonha.intfc.NSDSTransferEvent;
import com.domenix.commonha.queues.ConnEventQueue;
import com.domenix.commonha.queues.HAQueues;
import com.domenix.commonha.queues.NSDSXferEventQueue;
import java.util.concurrent.Executor;
import org.apache.log4j.Logger;

/**
 * Implements the interface with the protocol handler
 * @author jmerritt
 */
abstract public class HPAISABase implements HostProtocolAdapterInterface,
        IPCQueueListenerInterface, Runnable
{
    /**
     * Receive control messages from the Protocol Handler
     */
    private CtlWrapperQueue hpaCtlQueue;

    /**
     * Send control messages to the Host Interface Adapter
     */
    private CtlWrapperQueue nsdsCtlQueue;
    
    /**
     * Send received messages to the Protocol Handler
     */
    protected NSDSXferEventQueue hpaXferEventQueue;

    /**
     * Receive messages from the Host Interface
     */
    private NSDSXferEventQueue hiaXferEventQueue;

    /**
     * Send connection events to the Protocol Handler
     */
    private ConnEventQueue hpaConnEventQueue;

    /**
     * Receive connection events from the Host Interface Adapter
     */
    private ConnEventQueue hiaConnEventQueue;

    /**
     * The Host Protocol Adapter should be shutdown
     */
    private boolean shutDown = false;

    /**
     * Error/Debug Logger
     */
    private final Logger myLogger = Logger.getLogger(HPAISABase.class);

    // Settings of the host interface
    HostNetworkSettings hostNetworkSettings;
    
    public HPAISABase()
    {
    }

    @Override
    public void initialize(HAQueues queues, HostNetworkSettings config,
            NSDSInterface nsdsInterface, NSDSConnectionList connectionList)
    {
        myLogger.info("Initializing HPAISABase");
        hpaCtlQueue = queues.getHpaCtlQueue();
        nsdsCtlQueue = queues.getNsdsCtlQueue();
        hpaXferEventQueue = queues.getHpaXferEventQueue();
        hpaConnEventQueue = queues.getHpaConnEventQueue();
        hiaConnEventQueue = queues.getHiaConnEventQueue();
        hiaXferEventQueue = queues.getHiaXferEventQueue();

        hpaCtlQueue.addIPCQueueEventListener(this);
        hiaConnEventQueue.addIPCQueueEventListener(this);
        hiaXferEventQueue.addIPCQueueEventListener(this);
        
        hostNetworkSettings = config;
    }

    @Override
    public void doRun(Executor x)
    {
        x.execute(this);
    }

    @Override
    public boolean sendAck(long connectionId, CcsiChannelEnum channel, long msn)
    {
        return true;
    }

    @Override
    public boolean sendNak(long connectionId, CcsiChannelEnum channel, long msn,
            NakReasonCodeEnum nakCode, NakDetailCodeEnum nakDetailCode, String nakDetailMessage)
    {
// TODO: do I need to do something here?
        return true;
    }

    @Override
    public boolean sendHrtbt(long connectionId)
    {
        return true;
    }

    @Override
    public boolean sendReport(long connectionId, CcsiChannelEnum[] channel,
            String[] cacheKeys, String msg, CcsiAckNakEnum ack, long ackMsn, 
            NakReasonCodeEnum nakCode, boolean ackRequired)
    {
        boolean result = sendReportISA(connectionId, channel, 
                cacheKeys, msg, ack, ackMsn, nakCode, ackRequired);
        return result;
    }

    @Override
    public void queueEventFired(IPCQueueEvent evt)
    {
        if (evt.getEventSource() instanceof NSDSXferEventQueue)
        {
            NSDSTransferEvent xferEvt = hiaXferEventQueue.readFirst(0);

            if (xferEvt != null)
            {
                hpaXferEventQueue.insertLast(xferEvt);
            }
        } else if (evt.getEventSource() instanceof CtlWrapperQueue)
        {
            CtlWrapper ctlEvt = hpaCtlQueue.readFirst(0);

            if (ctlEvt != null)
            {
                if (ctlEvt.getCtlCommand() == CtlWrapper.STOP_THREAD)
                {
                    shutDown = true;
                    nsdsCtlQueue.insertLast(ctlEvt);
                }
            }
        } else if (evt.getEventSource() instanceof ConnEventQueue)
        {
            NSDSConnEvent connEvt = hiaConnEventQueue.readFirst(0);

            if (connEvt != null)
            {
                hpaConnEventQueue.insertLast(connEvt);
            }
        } else
        {
            myLogger.error("Unexpected queue event received.");
        }
    }

    @Override
    public void run()
    {
        myLogger.info("Initiated ISA HPA thread");

        while (!this.shutDown)
        {
            try
            {
                // does nothing
                Thread.sleep(2000);
            } catch (InterruptedException ex)
            {
                myLogger.error("Interrupted Exception", ex);
            }
        }
    }
    
    public void receiveXferEvent ()
    {
        
    }
    
    abstract public boolean sendReportISA (long connectionId, CcsiChannelEnum[] channel,
            String[] cacheKeys, String msg, CcsiAckNakEnum ack, long ackMsn, 
            NakReasonCodeEnum nakCode, boolean ackRequired);
    
    abstract public boolean sendAckISA (long connectionId, CcsiChannelEnum channel, long msn);
    abstract public boolean sendNakISA (long connectionId, CcsiChannelEnum channel, long msn,
            NakReasonCodeEnum nakCode, NakDetailCodeEnum nakDetailCode, String nakDetailMessag);
}
