/*
 * BSOObservable.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Observable that reports the BSO (Battle Space Observable) for linking events together
 */
package com.domenix.hpaisa;

import gov.isa.behaviors.BehaviorControls;
import gov.isa.capabilities.ObservableHandler;
import gov.isa.capabilities.ReadyState;
import gov.isa.capabilities.ReportingState;
import gov.isa.capabilities.StandardCapabilities;
import gov.isa.model.BSOID;
import gov.isa.model.ModelFactory;

/**
 * Observable that reports the BSO (Battle Space Observable) for linking events
 * together
 *
 * @author jmerritt
 */
public class BSOObservable {

    private final ModelFactory modelFactory;
    private final BSOID bsoID;
    public BehaviorControls bsoBehaviorControls;
    
    public BSOObservable(ModelFactory modelFactory, String id) {
        this.modelFactory = modelFactory;
        bsoID = modelFactory.newBSOID(id);
    }
          
    public ObservableHandler createBSOObservable (ModelFactory model_factory)
    {
        return new ObservableHandler(
                model_factory,
                StandardCapabilities.bSOObservable(model_factory),
                ReadyState.READY,
                ReportingState.REPORTING
        );
    }
    
    public BSOID getBSOID ()
    {
        return bsoID;
    }
}
