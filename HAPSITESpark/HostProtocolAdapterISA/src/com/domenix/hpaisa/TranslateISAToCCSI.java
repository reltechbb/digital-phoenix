/*
 * TranslateISAToCCSI.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Translates ISA Messages to CCSI
 */
package com.domenix.hpaisa;

import com.domenix.common.ccsi.MSN;
import com.domenix.common.utils.NumberFormatter;
import gov.isa.model.GeographicPosition;

/**
 * Translates ISA Messages to CCSI
 *
 * @author jmerritt
 */
public class TranslateISAToCCSI {

    private final MSN msn = MSN.getMSN();

    protected static String MESSAGE_HEADER
            = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ccsi:Hdr "
            + "xmlns:ccsi=\"file:/C:/ccsi/1.1/CCSI\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
            + "xmlns:ism=\"urn:us:gov:ic:ism\" ism:exemptFrom=\"DOD_DISTRO_STATEMENT\" ism:DESVersion=\"201412\" "
            + "ism:ISMCATCESVersion=\"201502\" ism:classification=\"U\" ism:compliesWith=\"USGov USDOD\" "
            + "ism:createDate=\"2018-10-01\" ism:disseminationControls=\"FOUO\" ism:ownerProducer=\"USA\" "
            + "ism:resourceElement=\"true\" sid=\"{HOSTID}\" msn=\"{MSN}\" dtg=\"{DTG}\" chn=\"{CHANNEL}\" mod=\"N\" "
            + "len=\"1912\"><CCSIDoc ism:classification=\"U\" ism:ownerProducer=\"USA\">"
            + "<Msg>";
    protected static final String MESSAGE_TRAILER
            = "</Msg>" + "</CCSIDoc>" + "</ccsi:Hdr>";
    private static final String DEREGISTER_MESSAGE
            = "<CmdChn Cmd=\"Deregister\"/>";
    private static final String REGISTER_MESSAGE
            = "<CmdChn Cmd=\"Register\">"
            + // ALERTS
            "<Arg>" + "<ArgName>Channel</ArgName>" + "<ArgValue>ALERTS</ArgValue>" + "</Arg>"
            + "<Arg>" + "<ArgName>Type</ArgName>" + "<ArgValue>EVENT</ArgValue>" + "</Arg>"
            + "<Arg>" + "<ArgName>Rate</ArgName>" + "<ArgValue>0</ArgValue>" + "</Arg>"
            + // READGS
            "<Arg>" + "<ArgName>Channel</ArgName>" + "<ArgValue>READGS</ArgValue>" + "</Arg>"
            + "<Arg>" + "<ArgName>Type</ArgName>" + "<ArgValue>EVENT</ArgValue>" + "</Arg>"
            + "<Arg>" + "<ArgName>Rate</ArgName>" + "<ArgValue>0</ArgValue>" + "</Arg>"
            + "<Arg>" + "<ArgName>Details</ArgName>" + "<ArgValue>false</ArgValue>" + "</Arg>"
            + // IDENT
            "<Arg>" + "<ArgName>Channel</ArgName>" + "<ArgValue>IDENT</ArgValue>" + "</Arg>"
            + "<Arg>" + "<ArgName>Type</ArgName>" + "<ArgValue>ONCE</ArgValue>" + "</Arg>"
            + "<Arg>" + "<ArgName>Rate</ArgName>" + "<ArgValue>0</ArgValue>" + "</Arg>"
            + // LOC
            "<Arg>" + "<ArgName>Channel</ArgName>" + "<ArgValue>LOC</ArgValue>" + "</Arg>"
            + "<Arg>" + "<ArgName>Type</ArgName>" + "<ArgValue>ONCE</ArgValue>" + "</Arg>"
            + "<Arg>" + "<ArgName>Rate</ArgName>" + "<ArgValue>0</ArgValue>" + "</Arg>"
            + "</CmdChn>";

    private static final String LOCATION_MESSAGE = "<CmdChn Cmd=\"Set_Location\">"
            + "<Arg><ArgName>Latitude</ArgName><ArgValue>{LATITUDE}</ArgValue></Arg>"
            + "<Arg><ArgName>Longitude</ArgName><ArgValue>{LONGITUDE}</ArgValue></Arg>"
            + "<Arg><ArgName>Altitude</ArgName><ArgValue>{ALTITUDE}</ArgValue></Arg>"
            + "<Arg><ArgName>Source</ArgName><ArgValue>manual</ArgValue></Arg>"
            + "</CmdChn>";

    private static String ACK_MESSAGE
            = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ccsi:Hdr xmlns:ccsi=\"file:/C:/ccsi/1.1/CCSI\" "
            + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" sid=\"{HOSTID}\" msn=\"{MSN}\" "
            + "dtg=\"{DTG}\" chn=\"{CHANNEL}\" mod=\"N\" ack=\"ACK\" ackmsn=\"{ACKMSN}\" len=\"0\"/>";

    private static final String SET_CONFIG_MESSAGE
            = "<CmdChn Cmd=\"Set_Config\"><Arg><ArgName>Name</ArgName><ArgValue>{THRESHOLD}</ArgValue>"
            + "</Arg><Arg><ArgName>Block</ArgName><ArgValue>UNIQUE</ArgValue></Arg><Arg>"
            + "<ArgName>Value</ArgName><ArgValue>{VALUE}</ArgValue></Arg></CmdChn>";


    /**
     * Constructor
     *
     * @param hostID
     */
    public TranslateISAToCCSI(String hostID) {
        MESSAGE_HEADER = MESSAGE_HEADER.replace("{HOSTID}", hostID);
        ACK_MESSAGE = ACK_MESSAGE.replace("{HOSTID}", hostID);
    }

    /**
     * Builds a register message in CCSI format
     *
     * @return
     */
    public String getRegisterMessage() {
        String header = MESSAGE_HEADER;
        header = header.replace("{CHANNEL}", "c");
        header = header.replace("{DTG}", NumberFormatter.getDate(null, null));
        header = header.replace("{MSN}", msn.getNextMSN().toString());
        StringBuilder stringBuilder = new StringBuilder(header);
        stringBuilder.append(REGISTER_MESSAGE).append(MESSAGE_TRAILER);
        return stringBuilder.toString();
    }

    /**
     * Builds a deregister message in CCSI format
     *
     * @return
     */
    public String getDeregisterMessage() {
        String header = MESSAGE_HEADER;
        header = header.replace("{CHANNEL}", "c");
        header = header.replace("{DTG}", NumberFormatter.getDate(null, null));
        header = header.replace("{MSN}", msn.getNextMSN().toString());
        StringBuilder stringBuilder = new StringBuilder(header);
        stringBuilder.append(header).append(DEREGISTER_MESSAGE).append(MESSAGE_TRAILER);
        return stringBuilder.toString();
    }

    public String getSetLocationMessage( String msn, GeographicPosition position) {
        String header = MESSAGE_HEADER;
        header = header.replace("{CHANNEL}", "c");
        header = header.replace("{DTG}", NumberFormatter.getDate(null, null));
        header = header.replace("{MSN}", msn);
        String location = LOCATION_MESSAGE;
        location = location.replace("{LATITUDE}", position.getLatitude().atomicValue().toString());
        location = location.replace("{LONGITUDE}", position.getLongitude().atomicValue().toString());
        if (position.getAltitude().isPresent()) {
            Integer value =  position.getAltitude().get().atomicValue().intValue();
            location = location.replace("{ALTITUDE}", value.toString());
        } else {
            // altitude is optional in both CCSI and ISA
            location = location.replace("{ALTITUDE}", "0");
        }
        StringBuilder stringBuilder = new StringBuilder(header);
        stringBuilder.append(location).append(MESSAGE_TRAILER);
        return stringBuilder.toString();
    }

    public String getAckMessage(String dtg, long ackMsn, String channel, Integer msn) {
        String message = ACK_MESSAGE;
        message = message.replace("{DTG}", dtg);
        message = message.replace("{MSN}", msn.toString());
        message = message.replace("{ACKMSN}", Long.toString(ackMsn)); // MSN being acknowleged
        message = message.replace("{CHANNEL}", channel);
        return message;
    }

    public String getSetRateMessage(String value, String msn) {
        String header = MESSAGE_HEADER;
        header = header.replace("{CHANNEL}", "c");
        header = header.replace("{DTG}", NumberFormatter.getDate(null, null));
        header = header.replace("{MSN}", msn);
        String setConfig = SET_CONFIG_MESSAGE;
        setConfig = setConfig.replace("{THRESHOLD}", "rateThreshold");
        setConfig = setConfig.replace("{VALUE}", value);

        StringBuilder stringBuilder = new StringBuilder(header);
        stringBuilder.append(setConfig).append(MESSAGE_TRAILER);

        return stringBuilder.toString();
    }

    public String getSetDoseMessage(String value, Integer msn) {
        String header = MESSAGE_HEADER;
        header = header.replace("{CHANNEL}", "c");
        header = header.replace("{DTG}", NumberFormatter.getDate(null, null));
        header = header.replace("{MSN}", msn.toString());
        String setConfig = SET_CONFIG_MESSAGE;
        setConfig = setConfig.replace("{THRESHOLD}", "accumulatedDoseThreshold");
        setConfig = setConfig.replace("{VALUE}", value);

        StringBuilder stringBuilder = new StringBuilder(header);
        stringBuilder.append(setConfig).append(MESSAGE_TRAILER);

        return stringBuilder.toString();
    }


}
