/*
 * HPAISASecurity.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Security methods for key stores and passwords
 *
 */
package com.domenix.hpaisa;

import com.google.common.base.Charsets;
import com.google.common.base.Supplier;
import gov.isa.model.UCI;
import gov.isa.spi.StandardComponentFactoryOptions;
import gov.isa.spi.StandardComponentFactoryOptions.KeyPasswordRetriever;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyStore;

/**
 * Security methods for key stores and passwords
 * @author jmerritt
 */
public class HPAISASecurity
{

    /**
     * Returns a KeyPasswordRetriever that assumes private key passwords are
     * stored in the format <code>&lt;key_directory&gt;/&lt;uci&gt;.pw</code>
     *
     * <p>
     * Password retrievers must return <strong>new</strong> char[] arrays upon
     * each call to {@link KeyPasswordRetriever#<UCI>apply(UCI) apply} as the
     * resulting array will be zeroed after each use.
     * </p>
     *
     * @param key_directory Directory in which to find the password files.
     * @return A file-based KeyPasswordRetriever
     */
    public StandardComponentFactoryOptions.KeyPasswordRetriever createPasswordRetriever(final Path key_directory)
    {
        return new StandardComponentFactoryOptions.KeyPasswordRetriever()
        {
            @Override
            public char[] apply(final UCI uci)
            {
                return retrievePassword(
                        key_directory.resolve(uci.uci() + ".pw")
                );
            }
        };

    }

    /**
     * Returns a KeyStoreRetriever that assumes keystores are stored in the
     * format <code>&lt;key_directory&gt;/&lt;uci&gt;.jks</code> and the JKS
     * password is stored in
     * <code>&lt;key_directory&gt;/&lt;uci&gt;.jks.pw</code>
     *
     * @param key_directory Directory in which to find the keystore files.
     * @return A file-based KeyStoreRetriever
     */
    public StandardComponentFactoryOptions.KeyStoreRetriever createKeystoreRetriever(final Path key_directory)
    {
    
        return new StandardComponentFactoryOptions.KeyStoreRetriever()
        {

            @Override
            public KeyStore apply(final UCI uci)
            {
                try
                {
                    return loadJKS(
                            key_directory.resolve(uci.uci() + ".jks"),
                            new Supplier<char[]>()
                    {
                        @Override
                        public char[] get()
                        {
                            return retrievePassword(
                                    key_directory.resolve(uci.uci() + ".jks.pw")
                            );
                        }
                    }
                    );

                } catch (Exception e)
                {
                    throw new IllegalStateException("unable to read keystore", e);
                }
            }
        };
    }

    /**
     * Loads a JKS Keystore from the specified file path and using the specified
     * password retriever.
     *
     * @param jks the file path of the keystore
     * @param passwordRetriever a supplier which returns a <strong>new</strong>
     * char[] when invoked
     * @return the resulting JKS instance
     */
    public KeyStore loadJKS(
            Path jks,
            Supplier<char[]> passwordRetriever
    )
    {

        try (InputStream stream = Files.newInputStream(jks))
        {
            KeyStore ks = KeyStore.getInstance("JKS");
            char[] password = passwordRetriever.get();
            ks.load(stream, password);
            stream.close();
            return ks;

        } catch (Throwable t)
        {

            throw new IllegalStateException("unable to load keystore", t);
        }
    }

    /**
     * Reads a char[] from the contents of a given file path.
     *
     * @param password_path path of the character file
     * @return the character contents of the file.
     */
    public char[] retrievePassword(Path password_path)
    {
   
        try
        {
            byte[] pw_data = Files.readAllBytes(password_path);
            CharBuffer pw_buffer = Charsets.UTF_8.decode(ByteBuffer.wrap(pw_data));
            return pw_buffer.array();

        } catch (IOException e)
        {
            throw new IllegalStateException("unable to read password file", e);
        }
    }
}
