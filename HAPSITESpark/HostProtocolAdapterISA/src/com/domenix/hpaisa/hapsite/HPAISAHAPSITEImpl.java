/*
 * <put file name here>
 *
 * Copyright (c) 2001-2020 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * <put description here>
 *
 */
package com.domenix.hpaisa.hapsite;

import com.domenix.commonha.ccsi.CcsiAckNakEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.commonha.intfc.NSDSConnectionList;
import com.domenix.commonha.intfc.NSDSInterface;
import com.domenix.commonha.queues.HAQueues;
import com.domenix.hpaisa.HPAISABase;
import gov.isa.capabilities.PropertyHandler;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author jmerritt
 */
public class HPAISAHAPSITEImpl extends HPAISABase implements Runnable {

    private HPAISAHAPSITEInitImpl hpaISAInit;
    private HAQueues haQueues;
    private HostNetworkSettings hostNetworkSettings;
    private NSDSConnectionList connectionList;
    private final Logger myLogger;
    private HAPSITEReadings hapsiteReadings;

    // send entire message first time
    private Boolean firstReport = true;
    
    public HPAISAHAPSITEImpl() {
        super();
        myLogger = Logger.getLogger(HPAISAHAPSITEImpl.class);
    }

    @Override
    public void initialize(HAQueues queues, HostNetworkSettings config,
            NSDSInterface nsdsInterface, NSDSConnectionList connectionList) {
        super.initialize(queues, config, nsdsInterface, connectionList);
        myLogger.info("Initializing HPAISAHAPSITEImpl");
        this.connectionList = connectionList;
        haQueues = queues;
        hostNetworkSettings = config;
        hpaISAInit = new HPAISAHAPSITEInitImpl(haQueues, hostNetworkSettings, connectionList);
        hpaISAInit.initialize(hostNetworkSettings);
        hapsiteReadings = new HAPSITEReadings();
    }

    /**
     * Constructor for testing purposes - not to be used operationally
     */
    public void initializeHPAISA() {
        myLogger.info("Initializing HPAISAHAPSITEImpl test");
        hpaISAInit = new HPAISAHAPSITEInitImpl(haQueues, hostNetworkSettings, connectionList);
        hpaISAInit.initialize(hostNetworkSettings);
    }

//    private Boolean gHarmful = false;
//    private Boolean hHarmful = false;

    private void processReadingReportSUDList(Node data) {
//        String gBar = null;
//        String gDose = null;
//        String gAgentType = null;
//        String hBar = null;
//        String hDose = null;
//        String hAgentType = null;
        String materialName = null;
        String density = null;
        NamedNodeMap sudList = null;
        Node sud;
        if (data != null) {
            sud = data.getFirstChild();
            if (sud != null) {
                do {
                    sudList = sud.getAttributes();
                    for (Integer attribute = 0; attribute < sudList.getLength(); attribute++) {
                        Node node = sudList.item(attribute);
                        String name = node.getNodeName();
                        if (name.equals("Name")) {
                            String nodeValue = node.getNodeValue();
                            myLogger.info("Name: " + nodeValue + " Value: " + getValue(sudList));
                            switch (nodeValue) {
                                // measurements
                                case "scanMode":
                                    hapsiteReadings.setScanMode(getValue(sudList));
                                    break;
                                case "methodState":
                                    hapsiteReadings.setMethodState (getValue(sudList));
                                    break;
                                case "methodType":
                                    hapsiteReadings.setMethodType(getValue(sudList));
                                    break;
                                case "methodName":
                                    hapsiteReadings.setMethodName (getValue(sudList));
                                    break;
                                case "casNumber":
                                    hapsiteReadings.setServiceNumber (getValue(sudList));
                                    break;
                                case "startTime":
                                    hapsiteReadings.setStartTime(getValue(sudList));
                                    break;
                                case "compound":
                                    materialName = getValue(sudList);
                                    hapsiteReadings.setMaterialName(materialName);
                                    break;
                                case "concentration":
                                    density = getValue(sudList);
                                    hapsiteReadings.setDensity(Float.parseFloat(density));
                                    break;
                                case "units":
                                    hapsiteReadings.setUnits(getValue(sudList));
                                    break;
                                case "purity":
                                    hapsiteReadings.setPurity(getValue(sudList));
                                    break;
                                case "alarmLevel":
                                    hapsiteReadings.setAlarmLevel (getValue(sudList));
                                    break;
                                case "fit":
                                    hapsiteReadings.setFit (getValue(sudList));
                                    break;
                                case "resultID":
                                    hapsiteReadings.setResultID(getValue(sudList));
                                    break;
                                    
                                // properties
                                case "hostName":
                                    hapsiteReadings.setHostname(getValue(sudList));
                                    break;
                                case "serialNumber":
                                    hapsiteReadings.setSerialNumber(getValue(sudList), hpaISAInit.getSerialNumberProperty());
                                    break;
                                case "softwareVersion":
                                    hapsiteReadings.setSWVersion (getValue(sudList));
                                    break;
                                case "gcVersion":
                                    hapsiteReadings.setGCVersion (getValue(sudList));
                                    break;
                                case "msVersion":
                                    hapsiteReadings.setMSVersion (getValue(sudList));
                                    break;
                                case "probeVersion":
                                    hapsiteReadings.setProbeVersion (getValue(sudList));
                                    break;
                                case "headspaceVersion":
                                    hapsiteReadings.setHeadspaceVersion (getValue(sudList));
                                    break;
                                case "situProbeVersion":
                                    hapsiteReadings.setSITUProbeVersion (getValue(sudList));
                                    break;
                                case "extSoftwareVersion":
                                    hapsiteReadings.setExtSoftwareVersion (getValue(sudList));
                                    break;
                                case "model":
                                    hapsiteReadings.setModel (getValue(sudList), hpaISAInit.getModelProperty()) ;
                                    break;
                                case "dscVersion":
                                    hapsiteReadings.setDscVersion (getValue(sudList));
                                    break;
                                case "batteryState":
                                    hapsiteReadings.setBatteryState(getValue(sudList));
                                    break;
                                case "voltage":
                                    hapsiteReadings.setVoltage(getValue(sudList));
                                    break;
                                case "sysState":
                                    hapsiteReadings.setSysState(getValue(sudList));
                                    break;
                                case "charge":
                                    hapsiteReadings.setCharge (getValue(sudList));
                                    break;
                                

                                default:
                                    myLogger.error("Unsupported SUD: " + sudList.item(attribute).getNodeValue());
                                    break;
                            }
                        }
                    }
                } while ((sud = sud.getNextSibling()) != null);
            }
        }

        // publish the event
        if ((materialName != null) && (density != null)) {
            firstReport = false;
            hpaISAInit.processReadingEvent(materialName, false, density, 0, "gbar", hapsiteReadings);
            materialName = null;
        } else if (firstReport && sudList != null) {
            // call this for the first event so AdditionalProperties gets updated
            hapsiteReadings.setValueChanged(true);
            hpaISAInit.processReadingEvent("None", false, "0.0", 0, "none", hapsiteReadings);
            firstReport = false;
        } else if (hapsiteReadings.getValueChanged ()) {
            hpaISAInit.processReadingEvent("None", false, "0.0", 0, "none", hapsiteReadings);
        } else {
            myLogger.warn ("Received message, not sending anything");
        }
    }
    
    @Override
    public boolean sendReportISA(long connectionId, CcsiChannelEnum[] channel,
            String[] cacheKeys, String msg, CcsiAckNakEnum ack, long ackMsn,
            NakReasonCodeEnum nakCode, boolean ackRequired) {
        // msg may have more than one item in it so wrap it so it can be parsed
        String xmsg = "<x>" + msg + "</x>";

        // notify the PH that the message was sent
        hpaISAInit.sendSentReportMessage(channel, msg, ackMsn, cacheKeys);

        // ack the message
        hpaISAInit.sendRcvAckMessage(channel, msg, ackMsn, cacheKeys);

        try {
            InputSource source = new InputSource(new StringReader(xmsg));
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(source);

            // Always only 1 item in this list
            NodeList rootList = document.getChildNodes();

            // may be more than 1 item in this list
            NodeList childList = rootList.item(0).getChildNodes();
            Integer length = childList.getLength();

            // process every item in the list
            for (Integer item = 0; item < length; item++) {
                Node type = childList.item(item).getFirstChild();
                String nodeName = type.getNodeName();

                switch (nodeName) {
                    case "ReadingsChn": {
                        // get ReadingReport node
                        NodeList list = type.getChildNodes();
                        int listLength = list.getLength();
//                        System.out.println("Msg Length: " + listLength);

                        // may be multiple ReadingReports in this message
                        Node readingReport = type.getFirstChild();
                        do {
                            // get Data
                            Node data = readingReport.getFirstChild();
                            if (data != null) {
                                processReadingReportSUDList(data);
                            }
                            readingReport = readingReport.getNextSibling();
                        } while (readingReport != null);
                    }
                    break;
                    case "ConfigChn":
                        Node block = type.getFirstChild();
                        Node configItem = block.getFirstChild();
                        NamedNodeMap configList = configItem.getAttributes();
                        String name = null;
                        String value = null;
                        for (Integer configIndex = 0; configIndex < configList.getLength(); configIndex++) {
                            String configItemName = configList.item(configIndex).getNodeName();
                            String configItemValue = configList.item(configIndex).getNodeValue();
                            if (configItemName.equals("Name")) {
                                name = configItemValue;
                            } else if (configItemName.equals("Value")) {
                                value = configItemValue;
                            }
                        }
                        List<PropertyHandler> propertyList = hpaISAInit.getCustomTypePropertyHandlerList();
                        for (PropertyHandler property : propertyList) {
                            if (property.getName().equals(name)) {
                                property.setValue(value);
                                break;
                            }
                        }

                        break;
                    case "IdentChn":
                        // TODO: publish this
                        // TODO: do I want to report other things (SW version, 
                        // UUID, HW version, Host ID)?
                        break;

                    case "LocationChn":
                        String latitude = null;
                        String longitude = null;
                        String altitude = null;
                        Node locationReport = type.getFirstChild();
                        NamedNodeMap attributeList = locationReport.getAttributes();
                        for (Integer attributes = 0; attributes < attributeList.getLength(); attributes++) {
                            String locationName = attributeList.item(attributes).getNodeName();
                            switch (locationName) {
                                case "Lat":
                                    latitude = attributeList.item(attributes).getNodeValue();
                                    break;
                                case "Lon":
                                    longitude = attributeList.item(attributes).getNodeValue();
                                    break;
                                case "Alt":
                                    altitude = attributeList.item(attributes).getNodeValue();
                                    break;
                                default:
                                    myLogger.error("Unknown attribute to set: " + locationName);
                                    break;
                            }
                        }
//                        hpaISAInit.updatePosition(latitude, longitude, altitude);
                        break;
                    case "AlertsChn":
                        /*String alertName = null;
                        String alertType = null;
                        NamedNodeMap alertList = type.getAttributes();
                        for (Integer attributes = 0; attributes < alertList.getLength(); attributes++) {
                            if (alertList.item(attributes).getNodeName().equals("Event")) {
                                // gets ALERT, DEALERT, WARN, DEWARN or (initial case) NONE
                                alertType = alertList.item(attributes).getNodeValue();
                            }

                        }
                        Node readingNode = type.getFirstChild();
                        if (readingNode == null) {
                            // this will happen initially when the alertType == NONE
                            // there are no children and nothing to report
                            myLogger.info("First Child Null: " + xmsg);
                            break;
                        }
                        Node detectNode = readingNode.getFirstChild();
                        if (detectNode == null) {
                            myLogger.error("First Child Null: " + xmsg);
                            break;
                        }
                        Node sudNode = detectNode.getFirstChild();
                        if (sudNode == null) {
                            myLogger.error("First Child Null: " + xmsg);
                            break;
                        }

                        alertList = sudNode.getAttributes();
                        for (Integer attributes = 0; attributes < alertList.getLength(); attributes++) {
                            if (sudNode.getNodeName().equals("SUD")) {
                                switch (alertList.item(attributes).getNodeValue()) {
                                    case "GBar":
                                        alertName = "gbarAlarm";
                                        if (alertType != null) {
                                            gHarmful = ((!alertType.equals("NONE"))
                                                    && (!alertType.equals("DEALERT"))
                                                    && (!alertType.equals("DEWARN")));
                                        }
                                        break;
                                    case "HBar":
                                        alertName = "hbarAlarm";
                                        if (alertType != null) {
                                            hHarmful = ((!alertType.equals("NONE"))
                                                    && (!alertType.equals("DEALERT")));
                                        }
                                        break;
                                    default:
//                                        myLogger.error("Invalid value: " + alertList.item(attributes).getNodeValue());
                                        break;
                                }
                            }
                        }
                        if ((alertType != null) && (alertName != null)) {
                            hpaISAInit.processAlertEvent(alertName, alertType);
                        }*/
                        break;
                    default:
                        myLogger.error("Not supported yet: " + nodeName);
                        break;
                }
            }

        } catch (IOException e) {
            myLogger.error("IOException: " + e.getMessage());
        } catch (ParserConfigurationException e) {
            myLogger.error("ParserConfigurationException: " + e.getMessage());
        } catch (DOMException e) {
            myLogger.error("DOMException: " + e.getMessage());
        } catch (SAXException e) {
            myLogger.error("Exception: " + e.getMessage());
        }

        return true;
    }

    /**
     * scans the list of nodes for the one named "Value" and returns the value
     * of that node
     *
     * @param nodeList
     * @return
     */
    private String getValue(NamedNodeMap nodeList) {
        String value = null;
        for (Integer attribute = 0; attribute < nodeList.getLength(); attribute++) {
            if (nodeList.item(attribute).getNodeName().equals("Value")) {
                value = nodeList.item(attribute).getNodeValue();
                break;
            }
        }
        return value;
    }

    @Override
    public boolean sendAckISA(long connectionId, CcsiChannelEnum channel, long msn) {
        hpaISAInit.sendSentAckMessage(connectionId, channel, msn);
        return true;
    }

    @Override
    public boolean sendNakISA(long connectionId, CcsiChannelEnum channel, long msn, NakReasonCodeEnum nakCode, NakDetailCodeEnum nakDetailCode, String nakDetailMessag) {
        hpaISAInit.sendSentNakMessage(connectionId, channel, msn, nakCode, nakDetailCode, nakDetailMessag);
        return true;
    }

    
}
