/*
 * ISACustomTypesHAPSITE.java
 *
 * Copyright (c) 2001-2020 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * Custom types for the HAPSITE Sensor
 *
 */
package com.domenix.hpaisa.hapsite;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import gov.isa.capabilities.CachingPropertyHandler;
import gov.isa.capabilities.CommandHandler;
import gov.isa.capabilities.PropertyChangeListener;
import gov.isa.capabilities.PropertyHandler;
import gov.isa.capabilities.ReadyState;
import gov.isa.capabilities.ReportingState;
import gov.isa.model.CustomType;
import gov.isa.model.CustomTypeDeclaration;
import gov.isa.model.CustomTypeName;
import gov.isa.model.FieldDeclaration;
import gov.isa.model.ModelFactory;
import gov.isa.model.Mutability;
import gov.isa.model.NameValuePair;
import gov.isa.model.PropertyDeclaration;
import gov.isa.model.RangeDeclaration;
import gov.isa.model.ThresholdDeclaration;
import gov.isa.model.TypeName;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.log4j.Logger;

/**
 * Custom types for the HAPSITE Sensor
 * @author jmerritt
 */
public class ISACustomTypesHAPSITE {
        private final Logger myLogger = Logger.getLogger(ISACustomTypesHAPSITE.class);

    private final HPAISAHAPSITEInitImpl hpaISAInit;

    /**
     * Constructor
     * @param init 
     */
    public ISACustomTypesHAPSITE (HPAISAHAPSITEInitImpl init)
    {
        hpaISAInit = init;
    }

    /**
     * Creates custom types: rateThreshold, accumulatedDoseThreshold, rateAlarm and doseAlarm
     *
     * @param model_factory
     * @return custom type declaration
     */
    public List<CustomTypeDeclaration> createCustomTypeDeclaration(ModelFactory model_factory)
    {
        List<CustomTypeDeclaration> customTypeDeclarationList = new ArrayList<>();
      
        customTypeDeclarationList.add(model_factory.newCustomTypeDeclaration(
                "additionalProperties",
                "Additional Properties",
                ImmutableList.<FieldDeclaration>of(
                        model_factory.newFieldDeclaration(
                                "swVersion", // name
                                "SW Version", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "gcVersion", // name
                                "gcVersion", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "msVersion", // name
                                "msVersion", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "probeVersion", // name
                                "probe version", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "headspaceVersion", // name
                                "headspaceVersion", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "situProbeVersion", // name
                                "situProbeVersion", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "extSoftwareVersion", // name
                                "extSoftwareVersion", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "model", // name
                                "model", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "dscVersion", // name
                                "dscVersion", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "batteryState", // name
                                "batteryState", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "voltage", // name
                                "Voltage", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "sysState", // name
                                "sysState", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                         model_factory.newFieldDeclaration(
                                "charge", // name
                                "charge", // description
                                model_factory.newTypeName( TypeName.Predefined.STRING ), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        )
                )));

        return customTypeDeclarationList;
    }

    /**
     * Creates a property handler for the color property using the declared
     * custom type
     *
     * @param model_factory model factory to use to create data structures
     * @param typeDeclarationList the list of type declarations
     * @return list custom property handlers
     */
    public List<PropertyHandler> createCustomTypePropertyHandler(
            ModelFactory model_factory, List<CustomTypeDeclaration> typeDeclarationList) {
        List<PropertyHandler> customTypePropertyHandlerList = new ArrayList<>();

        for (CustomTypeDeclaration declaration : typeDeclarationList) {
                PropertyHandler propertyHandler = new CachingPropertyHandler(
                        model_factory, // model factory
                        model_factory.newPropertyDeclaration( // property declaration
                                "AdditionalProperties",// name
                                "Additional Properties", // description
                                model_factory.newCustomTypeName(declaration.getName()), // type
                                model_factory.newMutability(Mutability.Predefined.READ_WRITE), // mutability
                                model_factory.newSingleStructure(), // structure
                                Optional.<RangeDeclaration>absent(), // range declaration
                                Optional.<ThresholdDeclaration>absent() // threshold declaration
                        ),
                        createCustomTypeValue (model_factory), // initial value
                        ReadyState.NOT_READY, // ready state
                        ReportingState.REPORTING // reporting state
                );
                customTypePropertyHandlerList.add(propertyHandler);
        }
        return customTypePropertyHandlerList;
    }
    
    /**
     * Creates a property change listener for listening to the changes in the
     * value of a property
     *
     * @return property change listener
     */
    public PropertyChangeListener createPropertyChangeListener()
    {

        return new PropertyChangeListener()
        {
            @Override
            public void onPropertyChanged(PropertyDeclaration declaration, Object newval)
            {

                myLogger.debug("Property changed: " + declaration.getName() + " " + newval.toString());

                if (declaration.getType() instanceof CustomTypeName)
                {

                    CustomTypeName customTypeName = (CustomTypeName) declaration.getType();

                    myLogger.debug("Custom type received: " + customTypeName.value());

                    // handle custom type here, if desired
                }
            }
        };
    }

    private CustomType createCustomTypeValue(ModelFactory model_factory)
    {
            return model_factory.newCustomType(
                ImmutableList.<NameValuePair>of(
                        model_factory.newNameValuePair("swVersion", "NONE"),
                        model_factory.newNameValuePair("gcVersion", "NONE"),
                        model_factory.newNameValuePair("msVersion", "NONE"),
                        model_factory.newNameValuePair("probeVersion", "NONE"),
                        model_factory.newNameValuePair("headspaceVersion", "NONE"),
                        model_factory.newNameValuePair("situProbeVersion", "NONE"),
                        model_factory.newNameValuePair("extSoftwareVersion", "0.0"),
                        model_factory.newNameValuePair("model", "0.0"),
                        model_factory.newNameValuePair("dscVersion", "0.0"),
                        model_factory.newNameValuePair("batteryState", "0"),
                        model_factory.newNameValuePair("sysState", "UNKNOWN"),
                        model_factory.newNameValuePair("charge", "0.0")
                ));
    }
    
  /**
   * Create a custom command handler for a command ClearCommand
   *
   * @param model_factory
   * @return custom command handler
   */
  public List <CommandHandler> createCustomCommandHandler( ModelFactory model_factory ) {

    List <CommandHandler> customCommandHandlerList = new ArrayList <>();

    return customCommandHandlerList;
  }
    /**
     * a class that extends PropertyHandler and implements the getValue() and
     * doSetValue() methods
     */
    private class CustomPropertyHandler extends PropertyHandler
    {

        private final AtomicReference<Object> current;
        private final PropertyDeclaration declaration;

        public CustomPropertyHandler(
                ModelFactory model_factory,
                PropertyDeclaration declaration,
                Object value,
                ReadyState ready,
                ReportingState reporting
        )
        {
            super(model_factory, declaration, ready, reporting);
            this.declaration = declaration;
            this.current = new AtomicReference<>(value);
        }

        @Override
        final public Object getValue()
        {
            myLogger.info ("getValue on: " + declaration.getName() + " value=" + current.toString());
            return current.get();
        }

        @Override
        final protected boolean doSetValue(Object value) {
            boolean retVal = true;
            myLogger.info("doSetValue on: " + declaration.getName() + " value=" + value.toString());

            current.set(value);

            return retVal;
        }
    }

    /**
     * Create a command handler for a command called "Move"
     *
     * @param model_factory
     * @return command handler
     */
    public List<CommandHandler> createStandardCommandHandler(final ModelFactory model_factory) {
        List<CommandHandler> commandHandlerList = new ArrayList<>();

        return commandHandlerList;
  }
}
