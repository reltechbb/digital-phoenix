/*
 * HAPSITEReadings.java
 *
 * Copyright (c) 2001-2020 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * <put description here>
 *
 */
package com.domenix.hpaisa.hapsite;

import com.google.common.collect.ImmutableList;
import gov.isa.capabilities.PropertyHandler;
import gov.isa.capabilities.ReadyState;
import gov.isa.capabilities.ReportingState;
import gov.isa.model.CustomType;
import gov.isa.model.ModelFactory;
import gov.isa.model.NameValuePair;
import java.util.ArrayList;

/**
 * Measurement readings and Additional Properties for HAPSITE
 *
 * @author jmerritt
 */
public class HAPSITEReadings {

    // measurements
    //private String scanType; same as scan mode
    private String scanMode;
    private String methodState;
    private String methodType;
    private String methodName;
    private String serviceNumber; // cas number
    private String startTime; // epoch time
    private String materialName;
    private Float density;
    private String units;
    private String purity = "0";
    private String alarmLevel = "0";
    private String fit = "0";
    // properties - these never change once set
    private String hostname = "Unknown";
    private String serialNumber = "Unknown";
    private String swVersion = "Unknown";
    private String gcVersion = "0.0";
    private String msVersion = "0.0";
    private String probeVersion = "0.0";
    private String headspaceVersion = "0.0";
    private String situProbeVersion = "0.0";
    private String extSoftwareVersion = "0.0";
    private String model = "0.0";
    private String dscVersion = "0.0";
    // state - these can change
    private String batteryState = "0";
    private String voltage = "0";
    private String sysState = "Unknown";
    private String charge = "0";
    private String resultID = "1";
    
    private Boolean valueChanged = false;
    private Boolean firstReport = true;

    public HAPSITEReadings() {

    }

    public void setValueChanged (Boolean value)
    {
        valueChanged = value;
    }

    public Boolean getValueChanged ()
    {
        return valueChanged;
    }

    public void setScanMode(String value) {
        scanMode = value;
    }

    public String getScanMode() {
        return scanMode;
    }

    public void setMethodState(String value) {
        methodState = value;
    }

    public String getMethodState() {
        return methodState;
    }

    public void setMethodType(String value) {
        methodType = value;
    }

    public String getMethodType() {
        return methodType;
    }

    public void setMethodName(String value) {
        methodName = value;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setServiceNumber(String value) {
        serviceNumber = value;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setStartTime(String value) {
        startTime = value;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setMaterialName(String value) {
        materialName = value;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setDensity(Float value) {
        density = value;
    }

    public Float getDensity() {
        return density;
    }

    public void setUnits(String value) {
        units = value;
    }

    public String getUnits() {
        return units;
    }

    public void setPurity(String value) {
        purity = value;
    }

    public String getPurity() {
        return purity;
    }

    public void setAlarmLevel(String value) {
        alarmLevel = value;
    }

    public String getAlarmLevel() {
        return alarmLevel;
    }

    public void setFit(String value) {
        fit = value;
    }

    public String getFit() {
        return fit;
    }

    public void setHostname(String value) {
        hostname = value;
    }

    public String getHostname() {
        return hostname;
    }

    public void setResultID (String value) {
        resultID = value;
    }

    public String getResultID() {
        return resultID;
    }

    public void setSerialNumber(String value, PropertyHandler propertyHandler) {
        if (firstReport) {

            if (propertyHandler.getName().equals("Serial Number")) {
                serialNumber = value;
                Boolean result = propertyHandler.setValue(value);
                propertyHandler.setReady(ReadyState.READY);
                propertyHandler.setReporting(ReportingState.REPORTING);
            }
        }
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSWVersion(String value) {
        swVersion = value;
    }

    public String getSWVersion() {
        return swVersion;
    }

    public void setGCVersion(String value) {
        gcVersion = value;
    }

    public String getGCVersion() {
        return gcVersion;
    }

    public void setMSVersion(String value) {
        msVersion = value;
    }

    public String getMSVersion() {
        return msVersion;
    }

    public void setProbeVersion(String value) {
        probeVersion = value;
    }

    public String getProbeVersion() {
        return probeVersion;
    }

    public void setHeadspaceVersion(String value) {
        headspaceVersion = value;
    }

    public String getHeadspaceVersion() {
        return headspaceVersion;
    }

    public void setSITUProbeVersion(String value) {
        situProbeVersion = value;
    }

    public String getSITUProbeVersion() {
        return situProbeVersion;
    }

    public String getExtSoftwareVersion() {
        return extSoftwareVersion;
    }

    public void setExtSoftwareVersion(String extSoftwareVersion) {
        this.extSoftwareVersion = extSoftwareVersion;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model, PropertyHandler propertyHandler) {
        if (firstReport) {
            if (propertyHandler.getName().equals("Model")) {
                this.model = model;
                Boolean result = propertyHandler.setValue(model);
                if (propertyHandler.getReady() == ReadyState.NOT_READY) {
                        propertyHandler.setReady(ReadyState.READY);
                        propertyHandler.setReporting(ReportingState.REPORTING);
                }
            }
        }
    }

    public String getDscVersion() {
        return dscVersion;
    }

    public void setDscVersion(String dscVersion) {
        this.dscVersion = dscVersion;
    }
    
    public void setBatteryState(String value) {
        if (!batteryState.equals(value)) {
            batteryState = value;
            valueChanged = true;
        }
    }

    public String getBatteryState() {
        return batteryState;
    }

    public void setVoltage(String value) {
        if (!voltage.equals(value)) {
            voltage = value;
            valueChanged = true;
        }
    }

    public String getVoltage() {
        return voltage;
    }

    public void setSysState(String value) {
        if (!sysState.equals(value)) {
            sysState = value;
            valueChanged = true;
        }
    }

    public String getSysState() {
        return sysState;
    }

    public void setCharge(String value) {
        if (!charge.equals(value)) {
            charge = value;
            valueChanged = true;
        }
    }

    public String getCharge() {
        return charge;
    }

    public CustomType getAdditionalProperties(ModelFactory model_factory) {
        ArrayList<NameValuePair> list = new ArrayList<>();
        if (valueChanged == true) {
            valueChanged = false;
            
            if (firstReport)
            {
                // any additional properties that are "first-time report" exclusive  set here
                firstReport = false;
            }
            
            list.add (model_factory.newNameValuePair("hostname", hostname));
            list.add (model_factory.newNameValuePair("swVersion", swVersion));
            list.add (model_factory.newNameValuePair("gcVersion", gcVersion));
            list.add (model_factory.newNameValuePair("msVersion", msVersion));
            list.add (model_factory.newNameValuePair("probeVersion", probeVersion));
            list.add (model_factory.newNameValuePair("headspaceVersion", headspaceVersion));
            list.add (model_factory.newNameValuePair("situProbeVersion", situProbeVersion));
            list.add (model_factory.newNameValuePair("extSoftwareVersion", extSoftwareVersion));
            list.add (model_factory.newNameValuePair("dscVersion", dscVersion));
            list.add (model_factory.newNameValuePair("batteryState", batteryState));
            list.add (model_factory.newNameValuePair("voltage", voltage));
            list.add (model_factory.newNameValuePair("sysState", sysState));
            list.add (model_factory.newNameValuePair("charge", charge));
        }
        return model_factory.newCustomType(ImmutableList.copyOf(list));
    }

    public CustomType getAdditionalMeasurements(ModelFactory model_factory) {
        ArrayList<NameValuePair> list = new ArrayList<>();
        list.add (model_factory.newNameValuePair("scanMode", scanMode));
        list.add (model_factory.newNameValuePair("methodState", methodState));
        list.add (model_factory.newNameValuePair("methodType", methodType));
        list.add (model_factory.newNameValuePair("methodName", methodName));
//        list.add (model_factory.newNameValuePair("serviceNumber", serviceNumber));
        list.add (model_factory.newNameValuePair("startTime", startTime));
        list.add (model_factory.newNameValuePair("materialName", materialName));
        list.add (model_factory.newNameValuePair("units", units));
        list.add (model_factory.newNameValuePair("purity", purity));
        list.add (model_factory.newNameValuePair("alarmLevel", alarmLevel));
        list.add (model_factory.newNameValuePair("fit", fit));
        list.add (model_factory.newNameValuePair("resultID", resultID));
        return model_factory.newCustomType(ImmutableList.copyOf(list));
    }

    
}
