/*
 * <put file name here>
 *
 * Copyright (c) 2001-2020 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * <put description here>
 *
 */
package com.domenix.hpaisa.hapsite;

import com.domenix.hpaisa.BSOObservable;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import gov.isa.behaviors.AbstractComponentBehavior;
import gov.isa.behaviors.BehaviorControls;
import gov.isa.behaviors.ComponentBehavior;
import gov.isa.capabilities.CachingPropertyHandler;
import gov.isa.capabilities.ObservableHandler;
import gov.isa.capabilities.PropertyHandler;
import gov.isa.capabilities.ReadyState;
import gov.isa.capabilities.ReportingState;
import gov.isa.capabilities.StandardCapabilities;
import gov.isa.model.ChemicalMaterialClass;
import gov.isa.model.ChemicalReading;
import gov.isa.model.Count;
import gov.isa.model.CustomType;
import gov.isa.model.CustomTypeDeclaration;
import gov.isa.model.Degrees;
import gov.isa.model.EventType;
import gov.isa.model.FieldDeclaration;
import gov.isa.model.GeographicPosition;
import gov.isa.model.Kgpm3;
import gov.isa.model.MeasuredKgpm2;
import gov.isa.model.MeasuredKgpm3;
import gov.isa.model.MeasuredKgsecpm3;
import gov.isa.model.MeasuredPpm;
import gov.isa.model.MeasuredSecs;
import gov.isa.model.Meters;
import gov.isa.model.ModelFactory;
import gov.isa.model.Mutability;
import gov.isa.model.NameValuePair;
import gov.isa.model.Percent;
import gov.isa.model.RangeDeclaration;
import gov.isa.model.ThresholdDeclaration;
import gov.isa.model.TypeName;
import gov.isa.model.UTC;
import gov.isa.util.TimeUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author jmerritt
 */
public class ChemicalObservableHAPSITE {
    private ObservableHandler clientChemical;
    public BehaviorControls chemicalBehaviorControls;
    private final Logger myLogger = Logger.getLogger(ChemicalObservableHAPSITE.class);
    private final ModelFactory modelFactory;
    private ObservableHandler clientBSO;
    private BSOObservable bsoObservable;
    private ObservableHandler clientPosition;
    private ObservableHandler clientIdentity;
    private CustomTypeDeclaration customTypeDeclaration;
    private PropertyHandler customPropertyHandler;
    private Boolean reportBSO = false;
    
    public ChemicalObservableHAPSITE(ModelFactory modelFactory) {
        this.modelFactory = modelFactory;
        if (System.getProperty("reportBSO") != null) {
            reportBSO = System.getProperty("reportBSO").equalsIgnoreCase("true");
        }
    }

    public ComponentBehavior createChemicalEvent(ModelFactory model_factory, String bsoID) {
        customTypeDeclaration = createCustomTypeDeclaration(model_factory);
        customPropertyHandler = createCustomTypePropertyHandler(model_factory, customTypeDeclaration);

        clientChemical = createChemicalObservable(model_factory);
        clientPosition = createPositionObservable(model_factory);
        bsoObservable = new BSOObservable(model_factory, bsoID);
        clientBSO = bsoObservable.createBSOObservable(model_factory);
        clientIdentity = createIdentityObservable(model_factory);

        if (reportBSO) {
            return new AbstractComponentBehavior() {

                @Override
                public ImmutableSet<ObservableHandler> getObservables() {
                    return ImmutableSet.<ObservableHandler>of(
                            clientChemical,
                            clientBSO,
                            clientPosition,
                            clientIdentity
                    );
                }

                @Override
                public ImmutableSet<PropertyHandler> getProperties() {
                    return ImmutableSet.<PropertyHandler>of( // leave this code commented out
                            // otherwise these properties get reported in the Status message
                            //                        customPropertyHandler // declare the property handler
                            );
                }

                @Override
                public void initialize(BehaviorControls controls) {
                    chemicalBehaviorControls = controls;
                }

                @Override
                public ImmutableSet<CustomTypeDeclaration> getCustomTypes() {
                    return ImmutableSet.<CustomTypeDeclaration>of(
                            customTypeDeclaration // declare the POC extended type
                    );
                }
            ;
        }  
            ;} else {
            return new AbstractComponentBehavior() {

                @Override
                public ImmutableSet<ObservableHandler> getObservables() {
                    return ImmutableSet.<ObservableHandler>of(
                            clientChemical,
                            //                        clientBSO,
                            clientPosition//,
                            //                        clientIdentity,
                            //customTypeObservableHandler
                    );
                }

                @Override
                public ImmutableSet<PropertyHandler> getProperties() {
                    return ImmutableSet.<PropertyHandler>of( // leave this code commented out
                            // otherwise these properties get reported in the Status message
                            //                        customPropertyHandler // declare the property handler
                            );
                }

                @Override
                public void initialize(BehaviorControls controls) {
                    chemicalBehaviorControls = controls;
                }

                @Override
                public ImmutableSet<CustomTypeDeclaration> getCustomTypes() {
                    return ImmutableSet.<CustomTypeDeclaration>of(
                            customTypeDeclaration // declare the POC extended type
                    );
                }
            ;
        }

    

    ;
}
    }
        
    private PropertyHandler createCustomTypePropertyHandler(ModelFactory model_factory, CustomTypeDeclaration declaration) {
        PropertyHandler propertyHandler = new CachingPropertyHandler(
                model_factory, // model factory
                model_factory.newPropertyDeclaration( // property declaration
                        "AdditionalMeasurements",// name
                        "Additional Measurements", // description
                        model_factory.newCustomTypeName(declaration.getName()), // type
                        //                                model_factory.newCustomTypeName(declaration.getName()), // type
                        model_factory.newMutability(Mutability.Predefined.READ_WRITE), // mutability
                        model_factory.newSingleStructure(), // structure
                        Optional.<RangeDeclaration>absent(), // range declaration
                        Optional.<ThresholdDeclaration>absent() // threshold declaration
                ),
                createCustomTypeValue(model_factory), // initial value
                ReadyState.READY, // ready state
                ReportingState.REPORTING // reporting state
        );
        return propertyHandler;
    }

    private CustomTypeDeclaration createCustomTypeDeclaration(ModelFactory model_factory) {
        return model_factory.newCustomTypeDeclaration(
                "AdditionalMeasurements",
                "Additional Measurements",
                ImmutableList.<FieldDeclaration>of(
                        model_factory.newFieldDeclaration(
                                "scanType", // name
                                "Scan Type: GCMS or SURVEY", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "scanMode", // name
                                "Scan Mode", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "methodType", // name
                                "Method Type", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "methodName", // name
                                "Method Name", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "startTime", // name
                                "Start Time", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "purity", // name
                                "Purity", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "alarmLevel", // name
                                "Alarm Level", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "fit", // name
                                "Fit", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        ),
                        model_factory.newFieldDeclaration(
                                "resultID", // name
                                "Result ID", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        )
                ));
    }

    private CustomType createCustomTypeValue(ModelFactory model_factory) {
        return model_factory.newCustomType(
                ImmutableList.<NameValuePair>of(
                        model_factory.newNameValuePair("scanType", "0"),
                        model_factory.newNameValuePair("scanMode", "0"),
                        model_factory.newNameValuePair("methodState", "0"),
                        model_factory.newNameValuePair("startTime", "0"),
                        model_factory.newNameValuePair("purity", "0"),
                        model_factory.newNameValuePair("alarmLevel", "0"),
                        model_factory.newNameValuePair("fit", "0")
                ));
    }

    private ObservableHandler createChemicalObservable(ModelFactory model_factory) {
        return new ObservableHandler(
                model_factory,
                StandardCapabilities.chemicalReadingObservable(model_factory),
                ReadyState.READY,
                ReportingState.REPORTING
        );
    }

    private ObservableHandler createIdentityObservable(ModelFactory model_factory) {
        return new ObservableHandler(
                model_factory,
                StandardCapabilities.identityObservable(model_factory),
                ReadyState.READY,
                ReportingState.REPORTING
        );
    }

    private ObservableHandler createPositionObservable(ModelFactory model_factory) {
        return new ObservableHandler(
                model_factory,
                StandardCapabilities.positionObservable(model_factory),
                ReadyState.READY,
                ReportingState.REPORTING
        );
    }

    private void updateAdditionalMeasurements(HAPSITEReadings readings) {
        CustomType newValues = readings.getAdditionalMeasurements(modelFactory);
        customPropertyHandler.setValue(newValues);
    }

    public void publishEvent(String materialName, Boolean harmful,
            String density, Integer hbar, Double latitude,
            Double longitude, Double altitude, String identity, HAPSITEReadings readings) {
        EventType eventType;
        if (harmful) {
            eventType = modelFactory.newEventType(EventType.Predefined.ALARM);
        } else {
            eventType = modelFactory.newEventType(EventType.Predefined.MEASUREMENT);
        }

        // convert mgpm3 to Kgpm3
        Double dose = Double.parseDouble(density)/1000.0;
        UTC start = TimeUtils.currentTimeUTC(modelFactory);
        UTC stale = TimeUtils.after(modelFactory, start, 30.0);
        ChemicalMaterialClass.Predefined materialClass;
        if (readings.getMaterialName().equals("TIC")) {
            materialClass= ChemicalMaterialClass.Predefined.TOXIC_INDUSTRIAL_CHEMICAL;
        } else {
            materialClass= ChemicalMaterialClass.Predefined.NERVE_AGENT;
        }
        
        ChemicalReading reading = modelFactory.newChemicalReading(
                Optional.<ChemicalMaterialClass>of(modelFactory.newChemicalMaterialClass(materialClass)),
                Optional.<String>of(materialName),
                Optional.<String>of(readings.getServiceNumber()), // service number (cas number)
                Optional.<Boolean>absent(),
                Optional.<MeasuredKgpm3>of(modelFactory.newMeasuredKgpm3(
                        modelFactory.newKgpm3(dose), Optional.<Kgpm3>absent())),
                Optional.<MeasuredSecs>absent(), // duration
                Optional.<MeasuredKgsecpm3>absent(), // concentration time
                Optional.<MeasuredKgpm2>absent(), // deposition
                Optional.<MeasuredPpm>absent(), // mass fraction
                Optional.<Count>absent(), // GBar reading
                Optional.<Count>absent());

        updateAdditionalMeasurements(readings);
        if (reportBSO) {
            chemicalBehaviorControls.getComponent().observe(
                    start,
                    Optional.<UTC>of(stale),
                    eventType,
                    clientChemical.observe(reading),
                    clientPosition.observe(createPosition(modelFactory, latitude, longitude, altitude)),
                    clientBSO.observe(bsoObservable.getBSOID()),
                    clientIdentity.observe(modelFactory.newStandardIdentity(modelFactory.newSIDC(identity), Optional.<Percent>absent())),
                    customPropertyHandler.observe());
        } else {
            chemicalBehaviorControls.getComponent().observe(
                    start,
                    Optional.<UTC>of(stale),
                    eventType,
                    clientChemical.observe(reading),
                    // Tom sez' report position on properties only
                    //                clientPosition.observe(createPosition(modelFactory, latitude, longitude, altitude)), 
                    // Uncomment these lines to put BSO and Identity back
                    //                clientBSO.observe(bsoObservable.getBSOID()), 
                    //                clientIdentity.observe(modelFactory.newStandardIdentity(modelFactory.newSIDC(identity), Optional.<Percent>absent())),
                    customPropertyHandler.observe());

        }
    }
 
    private GeographicPosition createPosition(ModelFactory model_factory,
            double lat, double lon, double meters) {
        return model_factory.newGeographicPosition(
                model_factory.newDegrees(lat), // latitude
                model_factory.newDegrees(lon), // longitude
                Optional.<Meters>of(model_factory.newMeters(meters)), // altitude (MSL)
                Optional.<Meters>absent(),
                Optional.<Meters>absent(),
                Optional.<Meters>absent(),
                Optional.<Degrees>absent(),
                Optional.<Degrees>absent()
        );
    }
}
