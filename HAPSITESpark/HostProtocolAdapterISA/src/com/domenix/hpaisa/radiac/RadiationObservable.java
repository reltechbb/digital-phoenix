/*
 * RadiationObserable.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Observable for radiation detection
 */
package com.domenix.hpaisa.radiac;

import com.domenix.hpaisa.BSOObservable;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import gov.isa.behaviors.AbstractComponentBehavior;
import gov.isa.behaviors.BehaviorControls;
import gov.isa.behaviors.ComponentBehavior;
import gov.isa.capabilities.ObservableHandler;
import gov.isa.capabilities.PropertyHandler;
import gov.isa.capabilities.ReadyState;
import gov.isa.capabilities.ReportingState;
import gov.isa.capabilities.StandardCapabilities;
import gov.isa.model.ChemicalElement;
import gov.isa.model.Degrees;
import gov.isa.model.EventType;
import gov.isa.model.GeographicPosition;
import gov.isa.model.Gray;
import gov.isa.model.Graypsec;
import gov.isa.model.MeasuredBq;
import gov.isa.model.MeasuredBqpm2;
import gov.isa.model.MeasuredBqpm3;
import gov.isa.model.MeasuredGray;
import gov.isa.model.MeasuredGraypsec;
import gov.isa.model.Meters;
import gov.isa.model.ModelFactory;
import gov.isa.model.Percent;
import gov.isa.model.RadiationReading;
import gov.isa.model.RadioactiveMaterialTypeCategoryCode;
import gov.isa.model.RadioactiveMaterialTypePrimaryRadiationCode;
import gov.isa.model.UTC;
import gov.isa.util.TimeUtils;
import org.apache.log4j.Logger;

/**
 * Observable for radiation detection
 * @author jmerritt
 */
public class RadiationObservable {
    private ObservableHandler client_radiation;
    public BehaviorControls radiationBehaviorControls;
    private final Logger myLogger = Logger.getLogger(RadiationObservable.class);
    private ObservableHandler clientBSO;
    private BSOObservable bsoObservable;
    private ObservableHandler clientPosition;
    private ObservableHandler clientIdentity;
    private Boolean rateAlarm = false;
    private Boolean doseAlarm = false;
    
    ModelFactory modelFactory;
    public RadiationObservable (ModelFactory modelFactory)
    {
        this.modelFactory = modelFactory;
    }
    
    public ComponentBehavior createRadiationEvent(ModelFactory model_factory, String bsoID)
    {
        client_radiation = createRadiationObservable(model_factory);
        clientPosition = createPositionObservable(model_factory);
        bsoObservable = new BSOObservable (model_factory, bsoID);
        clientBSO = bsoObservable.createBSOObservable (model_factory);
        clientIdentity = createIdentityObservable (model_factory);
        
        return new AbstractComponentBehavior()
        {

            @Override
            public ImmutableSet<ObservableHandler> getObservables()
            {
                return ImmutableSet.<ObservableHandler>of(
                        client_radiation,
                        clientBSO,
                        clientPosition,
                        clientIdentity
                );
            }

            @Override
            public void initialize(BehaviorControls controls)
            {
                radiationBehaviorControls = controls;
            }
        ;
    }

    ;
    }

    /**
     * creates a radiation observable
     * @param model_factory
     * @return 
     */
    private ObservableHandler createRadiationObservable(ModelFactory model_factory)
    {
        return new ObservableHandler(
                model_factory,
                StandardCapabilities.radiationReadingObservable(model_factory),
                ReadyState.READY,
                ReportingState.REPORTING
        );
    }
    
    private ObservableHandler createIdentityObservable(ModelFactory model_factory)
    {
        return new ObservableHandler(
                model_factory,
                StandardCapabilities.identityObservable(model_factory),
                ReadyState.READY,
                ReportingState.REPORTING
        );
    }
    
    private ObservableHandler createPositionObservable (ModelFactory model_factory)
    {
        return new ObservableHandler(
                model_factory,
                StandardCapabilities.positionObservable(model_factory),
                ReadyState.READY,
                ReportingState.REPORTING
        );
    }

    /**
     * publishes a radiation event
     * @param rate
     * @param accumulatedDose 
     * @param latitude 
     * @param longitude 
     * @param altitude 
     * @param identity 
     */
    public void publishRadiationEvent(String rate, String accumulatedDose, Double latitude,
            Double longitude, Double altitude, String identity)
    {
        Boolean harmful = rateAlarm || doseAlarm;
        EventType eventType;
        if (harmful)
        {
            eventType = modelFactory.newEventType(EventType.Predefined.ALARM);
        } else
        {
            eventType = modelFactory.newEventType(EventType.Predefined.MEASUREMENT);
        }
        Double dose = 0.0;
        if (accumulatedDose != null)
        {
            dose = Double.parseDouble(accumulatedDose);
        }
        UTC start = TimeUtils.currentTimeUTC(modelFactory);
        UTC stale = TimeUtils.after(modelFactory, start, 30.0);
        RadiationReading reading = modelFactory.newRadiationReading(
                modelFactory.newRadioactiveMaterialTypePrimaryRadiationCode(
                        RadioactiveMaterialTypePrimaryRadiationCode.Predefined.UNKNOWN),
                Optional.<RadioactiveMaterialTypeCategoryCode>absent(),
                Optional.<ChemicalElement>absent(),
                Optional.<MeasuredBq>absent(),
                Optional.<MeasuredBqpm2>absent(),
                Optional.<MeasuredBqpm3>absent(),
                Optional.<MeasuredGray>of(modelFactory.newMeasuredGray(
                        modelFactory.newGray(dose), Optional.<Gray>absent())),
                Optional.<MeasuredGraypsec>of(modelFactory.newMeasuredGraypsec(
                        modelFactory.newGraypsec(Double.parseDouble(rate)),
                        Optional.<Graypsec>absent())),
                Optional.<Boolean>of(harmful));

        radiationBehaviorControls.getComponent().observe(
                start,
                Optional.<UTC>of(stale),
                eventType,
                client_radiation.observe(reading),
                clientPosition.observe(createPosition(modelFactory, latitude, longitude, altitude)), 
                clientBSO.observe(bsoObservable.getBSOID()), 
                clientIdentity.observe(modelFactory.newStandardIdentity(modelFactory.newSIDC(identity), Optional.<Percent>absent())));

        myLogger.debug("Radiation event dose: " + dose + " rate: " + rate);
    }

    private GeographicPosition createPosition(ModelFactory model_factory,
            double lat, double lon, double meters) {
        return model_factory.newGeographicPosition(
                model_factory.newDegrees(lat), // latitude
                model_factory.newDegrees(lon), // longitude
                Optional.<Meters>of(model_factory.newMeters(meters)), // altitude (MSL)
                Optional.<Meters>absent(),
                Optional.<Meters>absent(),
                Optional.<Meters>absent(),
                Optional.<Degrees>absent(),
                Optional.<Degrees>absent()
        );
    }
    
    /**
     * Updates the alert state
     * @param name
     * @param state
     * @param propertyHandler 
     */
    public void updateAlertState(String name, String state, PropertyHandler propertyHandler)
    {
        if (name.equals("rateAlarm"))
        {
            rateAlarm = state.equals("ALERT");
        } else if (name.equals("doseAlarm"))
        {
            doseAlarm = state.equals("ALERT");
        }
/*        Component component = radiationBehaviorControls.getComponent();
        CustomType type = modelFactory.newCustomType(
                ImmutableList.<NameValuePair>of(
                        modelFactory.newNameValuePair(name, state)));
        Observation observation = Observation.createObservation(propertyHandler, type);
        UTC start = TimeUtils.currentTimeUTC(modelFactory);
        
        component.observe(start, 
                Optional.<UTC>absent(), 
                modelFactory.newEventType(EventType.Predefined.ALARM), 
                observation, clientBSO.observe(bsoObservable.getBSOID()));
        myLogger.info ("Alert name: " + name + " state: " + state);*/
    }
}
