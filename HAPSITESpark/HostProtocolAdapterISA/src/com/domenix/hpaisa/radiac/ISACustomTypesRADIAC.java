/*
 * ISOCustomTypes.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Creates the custom types (properties) for the RADIAC
 *
 */
package com.domenix.hpaisa.radiac;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import gov.isa.capabilities.CommandHandler;
import gov.isa.capabilities.PropertyChangeListener;
import gov.isa.capabilities.PropertyHandler;
import gov.isa.capabilities.ReadyState;
import gov.isa.capabilities.ReportingState;
import gov.isa.capabilities.Responder;
import gov.isa.capabilities.StandardCapabilities;
import gov.isa.model.CommandDeclaration;
import gov.isa.model.CustomType;
import gov.isa.model.CustomTypeDeclaration;
import gov.isa.model.CustomTypeName;
import gov.isa.model.FieldDeclaration;
import gov.isa.model.ModelFactory;
import gov.isa.model.Mutability;
import gov.isa.model.NameValuePair;
import gov.isa.model.NameValuePairs;
import gov.isa.model.ParameterDeclaration;
import gov.isa.model.PropertyDeclaration;
import gov.isa.model.Range;
import gov.isa.model.RangeDeclaration;
import gov.isa.model.ResultDeclaration;
import gov.isa.model.ThresholdDeclaration;
import gov.isa.model.TypeName;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.log4j.Logger;

/**
 * Creates the custom types (properties) for the RADIAC
 *
 * @author jmerritt
 */
public class ISACustomTypesRADIAC
{

    private final Logger myLogger = Logger.getLogger(ISACustomTypesRADIAC.class);

    private final HPAISARadiacInitImpl hpaISAInit;

    /**
     * Constructor
     * @param init 
     */
    public ISACustomTypesRADIAC(HPAISARadiacInitImpl init)
    {
        hpaISAInit = init;
    }

    /**
     * Creates custom types: rateThreshold, accumulatedDoseThreshold, rateAlarm and doseAlarm
     *
     * @param model_factory
     * @return custom type declaration
     */
    public List<CustomTypeDeclaration> createCustomTypeDeclaration(ModelFactory model_factory)
    {
        List<CustomTypeDeclaration> customTypeDeclarationList = new ArrayList<>();
        // Definition of the custom type 
        customTypeDeclarationList.add(model_factory.newCustomTypeDeclaration(
                "accumulatedDoseThreshold",
                "Threshold for radiation dose alarm",
                ImmutableList.<FieldDeclaration>of(
                        model_factory.newFieldDeclaration(
                                "accumulatedDoseThreshold", // name
                                "Dose Alarm Threshold", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        )
                )
        ));
        customTypeDeclarationList.add(model_factory.newCustomTypeDeclaration(
                "rateThreshold",
                "Threshold for radiation rate alarm",
                ImmutableList.<FieldDeclaration>of(
                        model_factory.newFieldDeclaration(
                                "rateThreshold", // name
                                "Rate Alarm Threshold", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        )
                )
        ));

        customTypeDeclarationList.add(model_factory.newCustomTypeDeclaration(
                "rateAlarm",
                "Threshold for radiation rate exceeded",
                ImmutableList.<FieldDeclaration>of(
                        model_factory.newFieldDeclaration(
                                "rateAlarm", // name
                                "Rate Alarm Threshold Exceeded", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        )
                )
        ));
        customTypeDeclarationList.add(model_factory.newCustomTypeDeclaration(
                "doseAlarm",
                "Threshold for radiation accumulated dose exceeded",
                ImmutableList.<FieldDeclaration>of(
                        model_factory.newFieldDeclaration(
                                "doseAlarm", // name
                                "Dose Alarm Threshold Exceeded", // description
                                model_factory.newTypeName(TypeName.Predefined.STRING), // type
                                model_factory.newSingleStructure(), // structure
                                false // optional
                        )
                )
        ));
        return customTypeDeclarationList;
    }

    /**
     * Creates a property handler for the color property using the declared
     * custom type
     *
     * @param model_factory model factory to use to create data structures
     * @param typeDeclarationList the list of type declarations
     * @return list custom property handlers
     */
    public List<PropertyHandler> createCustomTypePropertyHandler(
            ModelFactory model_factory, List<CustomTypeDeclaration> typeDeclarationList)
    {
        List<PropertyHandler> customTypePropertyHandlerList = new ArrayList<>();

        for (CustomTypeDeclaration declaration : typeDeclarationList)
        {
            PropertyHandler propertyHandler = new CustomPropertyHandler(
                    model_factory, // model factory
                    model_factory.newPropertyDeclaration( // property declaration
                            declaration.getName(),// name
                            declaration.getDescription(), // description
                            model_factory.newCustomTypeName(declaration.getName()), // type
                            model_factory.newMutability(Mutability.Predefined.READ_WRITE), // mutability
                            model_factory.newSingleStructure(), // structure
                            Optional.<RangeDeclaration>absent(), // range declaration
                            Optional.<ThresholdDeclaration>absent() // threshold declaration
                    ),
                    //TODO: read defaults from config
                    createCustomTypeValue(model_factory, .03, declaration.getName()), // initial value
                    ReadyState.READY, // ready state
                    ReportingState.REPORTING // reporting state
            );
            propertyHandler.addPropertyChangeListener(createPropertyChangeListener ());
            customTypePropertyHandlerList.add(propertyHandler);
        }
        return customTypePropertyHandlerList;
    }

    /**
     * Creates a property change listener for listening to the changes in the
     * value of a property
     *
     * @return property change listener
     */
    public PropertyChangeListener createPropertyChangeListener()
    {

        return new PropertyChangeListener()
        {
            @Override
            public void onPropertyChanged(PropertyDeclaration declaration, Object newval)
            {

                myLogger.debug("Property changed: " + declaration.getName() + " " + newval.toString());

                if (declaration.getType() instanceof CustomTypeName)
                {

                    CustomTypeName customTypeName = (CustomTypeName) declaration.getType();

                    myLogger.debug("Custom type received: " + customTypeName.value());

                    // handle custom type here, if desired
                }
            }
        };
    }

    /**
     * Creates an instance of the custom type color
     *
     * @param model_factory model factory to use to create data structures
     * @param value the Threshold
     * @param nameof the type
     * @return the Custom Type
     */
    private CustomType createCustomTypeValue(ModelFactory model_factory, double value, String name)
    {
        if ((name.equals("rateAlarm") || (name.equals("doseAlarm"))))
        {
            return model_factory.newCustomType(
                ImmutableList.<NameValuePair>of(
                        model_factory.newNameValuePair(name, "NONE")
                ));
        } else
        return model_factory.newCustomType(
                ImmutableList.<NameValuePair>of(
                        model_factory.newNameValuePair(name, value)
                )
        );
    }
    
  /**
   * Creates the command declaration for the custom command.  This includes name, arguments, and return values
   *
   * @param model_factory model factory to use
   * @return a command declaration for Pong command
   */
  private CommandDeclaration createClearCommandDeclaration( ModelFactory model_factory ) {

    ParameterDeclaration param_declaration = model_factory.newParameterDeclaration(
      "ClearItem",
      "Item to clear: Alarm or Dose",
      model_factory.newTypeName( TypeName.Predefined.STRING ),
      false,
      model_factory.newSingleStructure(),
      Optional.<RangeDeclaration>absent()
    );

    ResultDeclaration result_declaration = model_factory.newResultDeclaration(
      "Clear Result",
      "Item that was cleared",
      model_factory.newTypeName( TypeName.Predefined.STRING ),
      model_factory.newSingleStructure(),
      false,
      Optional.<Range>absent()
    );

    return model_factory.newCommandDeclaration(
      "ClearCommand",
      ImmutableList.of( param_declaration ),
      Optional.of( "Clears Rate or Dose alarm" ),
      ImmutableList.of( result_declaration )
    );
  }
  
  /**
   * Create a custom command handler for a command ClearCommand
   *
   * @param model_factory
   * @return custom command handler
   */
  public List <CommandHandler> createCustomCommandHandler( ModelFactory model_factory ) {

    // Get the command declaration for the custom command.  This includes name, arguments, and return values
    final CommandDeclaration declaration = createClearCommandDeclaration(
      model_factory );
    List <CommandHandler> customCommandHandlerList = new ArrayList <>();

    // The command handler will take the "Response" argument and return it as
    // the "Result" in the response message.
    customCommandHandlerList.add ( new CommandHandler(
      model_factory,
      declaration
    ) {
      @Override
      public void respond( Responder responder ) {

        // If a runtime exception is thrown here, a failure response will be
        // automatically sent to the requester.
        //
        // The API will check the argument names and types against your
        // CommandDeclaration before this callback is invoked. However, checking
        // for valid parameter ranges is left up to the user.
        //
        // Prefer the use of the NameValuePairs.* utilities to extract the
        // parameters you need.
        String response_argument = NameValuePairs.getRequired(
          // retrieve the parameters sent from the requester
          responder.getRequest().getParameters(),
          // the name of the argument we want to extract
          "ClearItem",
          // there is a standard object transformer for all types in the data model
          StandardCapabilities.toString
        );
        myLogger.info("Received ClearCommand: " + responder.getRequest().getParameters());

        switch (response_argument)
        {
              case "Alarm":
                  hpaISAInit.sendClearAlarmsMessage(response_argument);
                  responder.success(
                          ImmutableMap.<String, Object>of("Result", response_argument));
                  break;
            case "Dose":
                hpaISAInit.sendClearAccumulatedDoseMessage(response_argument);
                responder.success(
                        ImmutableMap.<String, Object>of("Result", response_argument));
                break;
            default:
                myLogger.error("Invalid clear command: " + response_argument);
                responder.failed("Invalid clear command: " + response_argument);
                break;
          }
      }
      });
    return customCommandHandlerList;
  }
    /**
     * a class that extends PropertyHandler and implements the getValue() and
     * doSetValue() methods
     */
    private class CustomPropertyHandler extends PropertyHandler
    {

        private final AtomicReference<Object> current;
        private final PropertyDeclaration declaration;

        public CustomPropertyHandler(
                ModelFactory model_factory,
                PropertyDeclaration declaration,
                Object value,
                ReadyState ready,
                ReportingState reporting
        )
        {
            super(model_factory, declaration, ready, reporting);
            this.declaration = declaration;
            this.current = new AtomicReference<>(value);
        }

        @Override
        final public Object getValue()
        {
            myLogger.info ("getValue on: " + declaration.getName() + " value=" + current.toString());
            return current.get();
        }

        @Override
        final protected boolean doSetValue(Object value)
        {
            boolean retVal = false;
            myLogger.info ("doSetValue on: " + declaration.getName() + " value=" + value.toString());
            switch (declaration.getName())
            {
                case "rateThreshold":
                    if (value instanceof String) {
                        hpaISAInit.sendSetRateMessage(value.toString());
                        retVal = true;

                    } else {
                        myLogger.error("Rate Threshold must be of type String");
                    }
                    break;
                case "accumulatedDoseThreshold":
                    if (value instanceof String) {
                        hpaISAInit.sendSetDoseMessage(value.toString());
                        retVal = true;
                    } else
                        myLogger.error("Dose Threshold must be of type String");
                    break;
                case "doseAlarm":
                case "rateAlarm":
                    // nothing to do
                    retVal = true;
                    break;
                default:
                    myLogger.error("Invalid name doSetValue: " + declaration.getName());
                    break;
                    
            }

            current.set(value);

            return retVal;
        }
    }
    
      /**
   * Create a command handler for a command called "Move"
   *
   * @param model_factory
   * @return command handler
   */
  public List<CommandHandler> createStandardCommandHandler( final ModelFactory model_factory ) {
      List <CommandHandler> commandHandlerList = new ArrayList <> ();

    return commandHandlerList;
  }
}
