/*
 * TranslateISAToCCSIRadiac.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Additional Radiac specific translation functions
 */
package com.domenix.hpaisa.radiac;

import com.domenix.common.utils.NumberFormatter;
import com.domenix.hpaisa.TranslateISAToCCSI;

/**
 * Additional Radiac specific translation functions
 *
 * @author jmerritt
 */
public class TranslateISAToCCSIRadiac extends TranslateISAToCCSI {

    private static final String CLEAR_ACCUMULATED_DOSE_MESSAGE = "<CmdChn Cmd=\"ClearAccumulatedDose\"/>";

    private static final String CLEAR_ALERT_MESSAGE
            = "<CmdChn Cmd=\"Clear_Alert\"><Arg><ArgName>Alert_ID</ArgName><ArgValue>ALL</ArgValue>"
            + "</Arg></CmdChn>";

    /**
     * Constructor
     *
     * @param hostID
     */
    public TranslateISAToCCSIRadiac(String hostID) {
        super(hostID);

    }

    public String getClearAccumulatedDoseMessage(String msn) {
        String header = MESSAGE_HEADER;
        header = header.replace("{CHANNEL}", "c");
        header = header.replace("{DTG}", NumberFormatter.getDate(null, null));
        header = header.replace("{MSN}", msn);
        String setConfig = CLEAR_ACCUMULATED_DOSE_MESSAGE;

        StringBuilder stringBuilder = new StringBuilder(header);
        stringBuilder.append(setConfig).append(MESSAGE_TRAILER);

        return stringBuilder.toString();
    }

    public String getClearAlertMessage(String msn) {
        String header = MESSAGE_HEADER;
        header = header.replace("{CHANNEL}", "c");
        header = header.replace("{DTG}", NumberFormatter.getDate(null, null));
        header = header.replace("{MSN}", msn);
        String setConfig = CLEAR_ALERT_MESSAGE;

        StringBuilder stringBuilder = new StringBuilder(header);
        stringBuilder.append(setConfig).append(MESSAGE_TRAILER);

        return stringBuilder.toString();
    }
}
