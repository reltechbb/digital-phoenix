/*
 * HPAISAInitImpl.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Implements the abstract class HPAISAInit.  Provides target device specific 
 * initialization of the ISA interface.
 */
package com.domenix.hpaisa.radiac;

import com.domenix.common.utils.NumberFormatter;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.commonha.enums.NSDSTransferEventEnum;
import com.domenix.commonha.intfc.NSDSConnection;
import com.domenix.commonha.intfc.NSDSConnectionList;
import com.domenix.commonha.intfc.NSDSConnectionTypeEnum;
import com.domenix.commonha.intfc.NSDSTransferEvent;
import com.domenix.commonha.queues.HAQueues;
import com.domenix.gps.GPSInterface;
import com.domenix.hpaisa.HPAISAInit;
import com.domenix.hpaisa.HPAISAStandardProperties;
import com.domenix.common.ccsi.MSN;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import gov.isa.behaviors.AbstractComponentBehavior;
import gov.isa.behaviors.BehaviorControls;
import gov.isa.behaviors.ComponentBehavior;
import gov.isa.behaviors.ExternalReceiver;
import gov.isa.capabilities.CommandHandler;
import gov.isa.capabilities.PropertyHandler;
import gov.isa.capabilities.ReadyState;
import gov.isa.model.CommandDeclaration;
import gov.isa.model.CustomTypeDeclaration;
import gov.isa.model.GeographicPosition;
import gov.isa.model.Header;
import gov.isa.model.Message;
import gov.isa.model.Meters;
import gov.isa.model.ModelFactory;
import gov.isa.model.Status;
import gov.isa.net.LifeCyclePhase;
import gov.isa.net.MetaData;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 * Implements the abstract class HPAISAInit. Provides target device specific
 * initialization of the ISA interface.
 *
 * @author jmerritt
 */
public class HPAISARadiacInitImpl extends HPAISAInit {

    private final HPAISAStandardProperties standardProperties;
    private final ISACustomTypesRADIAC customTypes;
    private final TranslateISAToCCSIRadiac translateISAToCCSI;
    private final HAQueues haQueues;
    private final HostNetworkSettings hostNetworkSettings;
    private final NSDSConnectionList connectionList;

    public HPAISARadiacInitImpl(HAQueues queues, HostNetworkSettings settings, NSDSConnectionList cList) {
        super();
        haQueues = queues;
        hostNetworkSettings = settings;
        customTypes = new ISACustomTypesRADIAC(this);
        standardProperties = new HPAISAStandardProperties(this, hostNetworkSettings);
        translateISAToCCSI = new TranslateISAToCCSIRadiac ("HostID");
        connectionList = cList;
    }

    @Override
    protected void processExternalMessage(Message message, Header header, MetaData metadata) {
        if (message instanceof Status) {
            myLogger.info("Status Message received: ");// + message.toString());
        }
    }

    @Override
    protected void processLifecycleChange(LifeCyclePhase prev, LifeCyclePhase next) {
        NSDSTransferEvent recvCmdEvt;
        CcsiChannelEnum[] eChannels;
        Integer msn;
        String dtg;

        myLogger.info("Lifecycle change: " + prev.getPhaseID() + " => " + next.getPhaseID());
        switch (next.getPhaseID()) {
            case OPERATION:
                Integer linkID = 1;
                // add an entry to the connection list
                NSDSConnection c = new NSDSConnection(linkID);
                c.setConnType(NSDSConnectionTypeEnum.PERSISTENT_CONNECTION);
                c.setHostIpString(hostNetworkSettings.getIP().getHostAddress());
                connectionList.addConnection(c);

                // build and send a registration message to the protocol handler
                recvCmdEvt = new NSDSTransferEvent(NSDSTransferEventEnum.XFER_RCV_CMD);
                eChannels = new CcsiChannelEnum[]{CcsiChannelEnum.CMD};
                recvCmdEvt.setConnectionId(1);
                msn = MSN.getMSN().getNextMSN();
                recvCmdEvt.setRecvdMsn(msn);
                recvCmdEvt.setFlags(null);
                recvCmdEvt.setAckFlag(false);
                recvCmdEvt.setChannels(eChannels);
                recvCmdEvt.setEventStatus(true);
                dtg = NumberFormatter.getDate(null, null);

                recvCmdEvt.setRcvdDtg(Long.parseLong(dtg));
                recvCmdEvt.setRecvdBody(translateISAToCCSI.getRegisterMessage());
                haQueues.getHpaXferEventQueue().insertLast(recvCmdEvt);
                break;

            case DEREGISTRATION:
                recvCmdEvt = new NSDSTransferEvent(NSDSTransferEventEnum.XFER_RCV_CMD);
                eChannels = new CcsiChannelEnum[]{CcsiChannelEnum.CMD};
                recvCmdEvt.setConnectionId(1);
                msn = MSN.getMSN().getNextMSN();
                recvCmdEvt.setRecvdMsn(msn);
                recvCmdEvt.setFlags(null);
                recvCmdEvt.setAckFlag(false);
                recvCmdEvt.setChannels(eChannels);
                recvCmdEvt.setEventStatus(true);
                dtg = NumberFormatter.getDate(null, null);

                recvCmdEvt.setRcvdDtg(Long.parseLong(dtg));
                recvCmdEvt.setRecvdBody(translateISAToCCSI.getDeregisterMessage());
                haQueues.getHpaXferEventQueue().insertLast(recvCmdEvt);

                break;
            default:
                // ignore these transisitions
                break;
        }
    }
    private RadiationObservable radiationObservable;

    @Override
    protected List<ComponentBehavior> createObservables(ModelFactory modelFactory) {
        radiationObservable = new RadiationObservable(modelFactory);
        List<ComponentBehavior> componentBehaviorList = new ArrayList<>();
        componentBehaviorList.add(radiationObservable.createRadiationEvent(model_factory, 
                hostNetworkSettings.getPHHostName()));

        return componentBehaviorList;
    }

    @Override
    protected List<CommandDeclaration> createCustomCommands(ModelFactory modelFactory) {
        List<CommandDeclaration> customCommandList = new ArrayList<>();

        return customCommandList;
    }

    public void sendRcvAckMessage(CcsiChannelEnum[] channel, String msg, long ackMsn, String [] cacheKeys) {
        // build and send an ACK message to the protocol handler
        Integer msn = MSN.getMSN().getNextMSN();
        String body = translateISAToCCSI.getAckMessage(Long.toString(
                Instant.now().toEpochMilli()), ackMsn, channel[0].getChanTag(), msn);
        sendTransferMessage(channel, msg, ackMsn, NSDSTransferEventEnum.XFER_RCV_ACK, body, (int) ackMsn, cacheKeys);
    }

    public void sendSentReportMessage(CcsiChannelEnum[] channel, String msg, long ackMsn, String [] cacheKeys) {
        // build and send a message to the protocol handler
        Integer msn = MSN.getMSN().getNextMSN();
        String body = translateISAToCCSI.getAckMessage(Long.toString(
                Instant.now().toEpochMilli()), ackMsn, channel[0].getChanTag(), msn);
        sendTransferMessage(channel, msg, ackMsn, NSDSTransferEventEnum.XFER_SENT_REPORT, body, (int)ackMsn, cacheKeys);
    }

    public void sendReceiveHeartbeatMessage(CcsiChannelEnum[] channel, String msg, long ackMsn) {
        // build and send a message to the protocol handler
        Integer msn = MSN.getMSN().getNextMSN();
        String body = translateISAToCCSI.getAckMessage(Long.toString(
                Instant.now().toEpochMilli()), ackMsn, channel[0].getChanTag(), msn);
        sendTransferMessage(channel, msg, ackMsn, NSDSTransferEventEnum.XFER_RCV_HRTBT, body, msn, null);
    }
    
   public void sendSetDoseMessage(String value) {
        // build and send a message to the protocol handler
        Integer msn = MSN.getMSN().getNextMSN();
        CcsiChannelEnum [] channel = {CcsiChannelEnum.CONFIG};
        String body = translateISAToCCSI.getSetDoseMessage(value, msn);
        sendTransferMessage(channel, null, msn, NSDSTransferEventEnum.XFER_RCV_CMD, body, msn, null);
    }

   public void sendSetRateMessage(String value) {
        // build and send a message to the protocol handler
        Integer msn = MSN.getMSN().getNextMSN();
        CcsiChannelEnum [] channel = {CcsiChannelEnum.CONFIG};
        String body = translateISAToCCSI.getSetRateMessage(value, msn.toString());
        sendTransferMessage(channel, null, msn, NSDSTransferEventEnum.XFER_RCV_CMD, body, msn, null);
    }
   
   public void sendClearAlarmsMessage(String value) {
        // build and send a message to the protocol handler
        Integer msn = MSN.getMSN().getNextMSN();
        CcsiChannelEnum [] channel = {CcsiChannelEnum.CONFIG};
        String body = translateISAToCCSI.getClearAlertMessage (msn.toString());
        sendTransferMessage(channel, null, msn, NSDSTransferEventEnum.XFER_RCV_CMD, body, msn, null);
    }
   
   public void sendClearAccumulatedDoseMessage(String value) {
        // build and send a message to the protocol handler
        Integer msn = MSN.getMSN().getNextMSN();
        CcsiChannelEnum [] channel = {CcsiChannelEnum.CONFIG};
        String body = translateISAToCCSI.getClearAccumulatedDoseMessage(msn.toString());
        sendTransferMessage(channel, null, msn, NSDSTransferEventEnum.XFER_RCV_CMD, body, msn, null);
    }

    public void sendSetLocationMessage(GeographicPosition value) {
        // build and send a message to the protocol handler
        Integer msn = MSN.getMSN().getNextMSN();
        CcsiChannelEnum[] channel = {CcsiChannelEnum.CONFIG};
        String body = translateISAToCCSI.getSetLocationMessage(msn.toString(), value);
        sendTransferMessage(channel, null, msn, NSDSTransferEventEnum.XFER_RCV_CMD, body, msn, null);
    }
      
    public void sendSentAckMessage (long connectionId, CcsiChannelEnum channel, long sentMsn)
    {
        NSDSTransferEvent sentAck = new NSDSTransferEvent(NSDSTransferEventEnum.XFER_SENT_ACK);
        Integer msn = MSN.getMSN().getNextMSN();
        sentAck.setEventStatus(true);
        sentAck.setAckMsn(msn);
        CcsiChannelEnum [] channels = new CcsiChannelEnum [] {channel}; 
        sentAck.setChannels(channels);
        sentAck.setConnectionId(1);
        sentAck.setSentMsn(sentMsn);
        haQueues.getHpaXferEventQueue().insertLast(sentAck);
    }
    
    public void sendSentNakMessage(long connectionId, CcsiChannelEnum channel, long msn, 
            NakReasonCodeEnum nakCode, NakDetailCodeEnum nakDetailCode, 
            String nakDetailMessage)
    {
        myLogger.error ("Need to implement sendSentNakMessage");
    }
    
    private void sendTransferMessage(CcsiChannelEnum[] channel, String msg, long ackMsn,
            NSDSTransferEventEnum event, String body, Integer msn, String [] cacheKeys) {
        NSDSTransferEvent recvCmdEvt = new NSDSTransferEvent(event);
        recvCmdEvt.setConnectionId(1);
        recvCmdEvt.setRecvdMsn(msn);
        recvCmdEvt.setFlags(null);
        recvCmdEvt.setAckMsn(ackMsn);
        recvCmdEvt.setAckFlag(false);
        recvCmdEvt.setChannels(channel);
        recvCmdEvt.setEventStatus(true);
        recvCmdEvt.setCacheKeys(cacheKeys);
        Long dtg = Instant.now().toEpochMilli();

        recvCmdEvt.setRcvdDtg(dtg);
        recvCmdEvt.setRecvdBody(body);
        haQueues.getHpaXferEventQueue().insertLast(recvCmdEvt);
    }

    @Override
    protected List<CommandHandler> createCustomCommandHandler(ModelFactory modelFactory) {
        return customTypes.createCustomCommandHandler(model_factory);
    }

    @Override
    protected List<CommandHandler> createStandardCommandHandler(ModelFactory modelFactory) {
        return customTypes.createStandardCommandHandler(model_factory);
    }

    @Override
    protected List<CustomTypeDeclaration> createCustomTypeDeclaration(ModelFactory modelFactory) {
        return customTypes.createCustomTypeDeclaration(model_factory);
    }

    @Override
    protected List<PropertyHandler> createCustomTypePropertyHandler(ModelFactory modelFactory,
            List<CustomTypeDeclaration> customTypeDeclarationList) {
        return customTypes.createCustomTypePropertyHandler(model_factory, customTypeDeclarationList);
    }

    @Override
    protected ComponentBehavior createExternalReceiver(ModelFactory modelFactory) {
        ComponentBehavior componentBehavior = new AbstractComponentBehavior() {
            @Override
            public void initialize(final BehaviorControls controls) {
                controls.addExternalReceiver(createExternalReceiverHandler());
            }
        };
        return componentBehavior;
    }

    private ExternalReceiver createExternalReceiverHandler() {
        ExternalReceiver receiver = new ExternalReceiver() {
            @Override
            public void onExternalMessage(Message message, Header header, MetaData metadata) {
                processExternalMessage(message, header, metadata);
            }
        };
        return receiver;
    }

    private PropertyHandler client_identity;

    private PropertyHandler client_position;
    private PropertyHandler manufacturerProperty;
    private PropertyHandler serialNumberProperty;
    private PropertyHandler modelProperty;
    private PropertyHandler nameProperty;
    private Double latitude = 38.75;
    private Double longitude = -77.30;
    private Double altitude = 300.0;
    private GPSInterface gpsInterface = null;
    
    /**
     *
     * @param modelFactory
     * @return
     */
    @Override
    protected ComponentBehavior createStandardProperties(ModelFactory modelFactory) {
        // create the GPS Interface
        gpsInterface = new GPSInterface ();

        client_identity = standardProperties.createIdentityHandler(model_factory, hostNetworkSettings.getSensorName());

        client_position = standardProperties.createPositionPropertyHandler(model_factory, latitude, longitude, altitude,
                gpsInterface);
        manufacturerProperty = standardProperties.createManufacturerProperty(model_factory);
        serialNumberProperty = standardProperties.createSerialNumberProperty(model_factory);
        modelProperty = standardProperties.createModelNumberProperty(model_factory);
        nameProperty = standardProperties.createNameProperty(model_factory);

        client_position.addPropertyChangeListener(standardProperties.createPropertyChangeListener());
        
        return new AbstractComponentBehavior() {

            @Override
            public ImmutableSet<PropertyHandler> getProperties() {
                return ImmutableSet.<PropertyHandler>of(
                        client_identity,
                        client_position,
                        manufacturerProperty,
                        serialNumberProperty,
                        modelProperty,
                        nameProperty
                );
            }

            @Override
            public void initialize(BehaviorControls controls) {
                    controls.getScheduler().scheduleAtFixedRate(
                            new Runnable() {
                        // periodically update the position to simulate motion
                        @Override
                        public void run() {
                        if (gpsInterface.isValid()) {
                            latitude = gpsInterface.getLatitude();
                            longitude = gpsInterface.getLongitude();
                            altitude = gpsInterface.getAltitude();
                            updatePosition(String.valueOf(latitude),
                                    String.valueOf(longitude), String.valueOf(altitude));
                            if (client_position.getReady() != ReadyState.READY) {
                                client_position.setReady(ReadyState.READY);
                            }
                        } else {
                            myLogger.warn("GPS Not Ready, position data not available");
                        }

                        }
                    }, 60L, 60L, TimeUnit.SECONDS
                    );
            }
        };
    }

    public void updatePosition(String latitude, String longitude, String altitude) {
        GeographicPosition old_position = (GeographicPosition) client_position.getValue();

        GeographicPosition new_position = old_position.withLatitude(
                model_factory.newDegrees(
                        Double.parseDouble(latitude))
        ).withLongitude(
                model_factory.newDegrees(
                        Double.parseDouble(longitude)
                ));
        new_position = new_position.withAltitude(Optional.<Meters>of(
                model_factory.newMeters(Double.parseDouble(altitude))));

        client_position.setValue(new_position);
    }

    public void processReadingEvent(String level, String dose) {
        radiationObservable.publishRadiationEvent(level, dose, latitude, longitude, altitude,
                hostNetworkSettings.getSensorName());
    }

    public void processAlertEvent(String name, String type) {
        ImmutableSet<PropertyHandler> propertyHandlerList = client_declaration.getPropertyHandlers();
        // find the PropertyHandler for this property
        Boolean found = false;
        for (PropertyHandler propertyHandler : propertyHandlerList) {
            if (propertyHandler.getName().equals(name)) {
                radiationObservable.updateAlertState(name, type, propertyHandler);
                found = true;
                break;
            }
        }

        if (!found) {
            myLogger.error("Couldn't find property handler for: " + name);
        }
    }
}
