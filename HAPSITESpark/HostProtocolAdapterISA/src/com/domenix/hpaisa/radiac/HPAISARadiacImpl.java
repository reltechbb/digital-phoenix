/*
 * HPAISARadiacImpl.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Implementation of the HPA specific to the Radiac
 */
package com.domenix.hpaisa.radiac;

import com.domenix.commonha.ccsi.CcsiAckNakEnum;
import com.domenix.commonha.ccsi.CcsiChannelEnum;
import com.domenix.commonha.ccsi.NakDetailCodeEnum;
import com.domenix.commonha.ccsi.NakReasonCodeEnum;
import com.domenix.commonha.config.HostNetworkSettings;
import com.domenix.commonha.intfc.NSDSConnectionList;
import com.domenix.commonha.intfc.NSDSInterface;
import com.domenix.commonha.queues.HAQueues;
import com.domenix.hpaisa.HPAISABase;
import gov.isa.capabilities.PropertyHandler;
import java.io.IOException;

import java.io.StringReader;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Implementation of the HPA specific to the Radiac
 * @author jmerritt
 */
public class HPAISARadiacImpl extends HPAISABase implements Runnable {

    private HPAISARadiacInitImpl hpaISAInit;
    private HAQueues haQueues;
    private HostNetworkSettings hostNetworkSettings;
    private NSDSConnectionList connectionList;
    private final Logger myLogger;

    public HPAISARadiacImpl() {
        super();
        myLogger = Logger.getLogger(HPAISARadiacImpl.class);
    }

    @Override
    public void initialize(HAQueues queues, HostNetworkSettings config,
            NSDSInterface nsdsInterface, NSDSConnectionList connectionList) {
        super.initialize(queues, config, nsdsInterface, connectionList);
        myLogger.info("Initializing HPAISARadiacImpl");
        this.connectionList = connectionList;
        haQueues = queues;
        hostNetworkSettings = config;
        hpaISAInit = new HPAISARadiacInitImpl(haQueues, hostNetworkSettings, connectionList);
        hpaISAInit.initialize(hostNetworkSettings);
    }

    /**
     * Constructor for testing purposes - not to be used operationally
     */
    public void initializeHPAISA() {
        myLogger.info("Initializing HPAISARadiacImpl test");
        hpaISAInit = new HPAISARadiacInitImpl(haQueues, hostNetworkSettings, connectionList);
        hpaISAInit.initialize(hostNetworkSettings);
    }

    @Override
    public boolean sendReportISA(long connectionId, CcsiChannelEnum[] channel,
            String[] cacheKeys, String msg, CcsiAckNakEnum ack, long ackMsn,
            NakReasonCodeEnum nakCode, boolean ackRequired) {
        // msg may have more than one item in it so wrap it so it can be parsed
        String xmsg = "<x>" + msg + "</x>";

        // notify the PH that the message was sent
        hpaISAInit.sendSentReportMessage(channel, msg, ackMsn, cacheKeys);

        // ack the message
        hpaISAInit.sendRcvAckMessage(channel, msg, ackMsn, cacheKeys);

        try {
            InputSource source = new InputSource(new StringReader(xmsg));
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(source);

            // Always only 1 item in this list
            NodeList rootList = document.getChildNodes();
            // may be more than 1 item in this list
            NodeList childList = rootList.item(0).getChildNodes();
            Integer length = childList.getLength();
            // process every item in the list
            for (Integer item = 0; item < length; item++) {
                Node type = childList.item(item).getFirstChild();
                String nodeName = type.getNodeName();
                switch (nodeName) {
                    case "ReadingsChn":
                    {
                        String level = null;
                        String dose = null;
                        Node readingReport = type.getFirstChild();
                        Node data = readingReport.getFirstChild();
                        
                        if (data != null) {
                            NamedNodeMap attributeList = data.getAttributes();
                            for (Integer attribute = 0; attribute < attributeList.getLength(); attribute++) {
                                if (attributeList.item(attribute).getNodeName().equals("Level")) {
                                    level = attributeList.item(attribute).getNodeValue();
                                    break;
                                }
                            }

                            Node sud = data.getFirstChild();
                            if (sud != null) {
                                NamedNodeMap sudList = sud.getAttributes();
                                for (Integer attribute = 0; attribute < sudList.getLength(); attribute++) {
                                    if (sudList.item(attribute).getNodeName().equals("Value")) {
                                        dose = sudList.item(attribute).getNodeValue();
                                        break;
                                    }
                                }
                            }
                        }
                        // publish the event
                        if ((level != null) && (dose != null))
                        {
                                hpaISAInit.processReadingEvent(level, dose);
                        }
                    }
                    break;
                    case "ConfigChn":
                        Node block = type.getFirstChild();
                        Node configItem = block.getFirstChild();
                        NamedNodeMap configList = configItem.getAttributes();
                        String name = null;
                        String value = null;
                        for (Integer configIndex = 0; configIndex < configList.getLength(); configIndex++)
                        {
                            String configItemName = configList.item(configIndex).getNodeName();
                            String configItemValue = configList.item(configIndex).getNodeValue();
                            if (configItemName.equals("Name"))
                            {
                                name = configItemValue;
                            } else if (configItemName.equals("Value"))
                            {
                                value = configItemValue;
                            }
                        }
                        List<PropertyHandler> propertyList = hpaISAInit.getCustomTypePropertyHandlerList ();
                        for (PropertyHandler property : propertyList)
                        {
                            if (property.getName().equals(name))
                            {
                                property.setValue(value);
                                break;
                            }
                        }

                        break;
                    case "IdentChn":
                        // TODO: publish this
                        // TODO: do I want to report other things (SW version, 
                        // UUID, HW version, Host ID)?
                        break;
                        
                    case "LocationChn":
                        String latitude = null;
                        String longitude = null;
                        String altitude = null;
                        Node locationReport = type.getFirstChild();
                        NamedNodeMap attributeList = locationReport.getAttributes();
                        for (Integer attributes = 0; attributes < attributeList.getLength(); attributes++)
                        {
                            String locationName = attributeList.item(attributes).getNodeName();
                            switch (locationName)
                            {
                                case "Lat":
                                    latitude = attributeList.item(attributes).getNodeValue();
                                    break;
                                case "Lon":
                                    longitude = attributeList.item(attributes).getNodeValue();
                                    break;
                                case "Alt":
                                    altitude = attributeList.item(attributes).getNodeValue();
                                    break;
                                default:
                                    myLogger.error("Unknown attribute to set: " + locationName);
                                    break;
                            }
                        }
//                        hpaISAInit.updatePosition(latitude, longitude, altitude);
                        break;
                    case "AlertsChn":
                        String alertName = null;
                        String alertType = null;
                        NamedNodeMap alertList = type.getAttributes();
                        for (Integer attributes = 0; attributes < alertList.getLength(); attributes++)
                        {
                            if (alertList.item(attributes).getNodeName().equals("Event"))
                            {
                                // gets ALERT, DEALERT, WARN, DEWARN or (initial case) NONE
                                alertType = alertList.item(attributes).getNodeValue();
                            }
                            
                        }
                        Node readingNode = type.getFirstChild();
                        if (readingNode == null)
                        {
                            // this will happen initially when the alertType == NONE
                            // there are no children and nothing to report
                            myLogger.info ("First Child Null: " + xmsg);
                            break;
                        }
                        Node detectNode = readingNode.getFirstChild();
                        if (detectNode == null)
                        {
                            myLogger.error ("First Child Null: " + xmsg);
                            break;
                        }
                        alertList = detectNode.getAttributes();
                        for (Integer attributes = 0; attributes < alertList.getLength(); attributes++)
                        {
                            if (alertList.item(attributes).getNodeName().equals("Detect"))
                            {
                                switch (alertList.item(attributes).getNodeValue())
                                {
                                    case "RadiationAccumulatedDose":
                                        alertName = "doseAlarm";
                                        break;
                                    case "RadiationRate":
                                        alertName = "rateAlarm";
                                        break;
                                    default:
                                        myLogger.error("Invalid value: " + alertList.item(attributes).getNodeValue());
                                        break;
                                }
                            }
                        }
                        if ((alertType != null) && (alertName != null))
                        {
                            hpaISAInit.processAlertEvent(alertName, alertType);
                        }
                        // TODO: do I need an alert ID?
                        break;
                    default:
                        myLogger.error("Not supported yet: " + nodeName);
                        break;
                }
            }

        } catch (IOException | ParserConfigurationException | DOMException | SAXException e) {
            myLogger.error("Exception: " + e.getMessage());
        }

        return true;
    }

    @Override
    public boolean sendAckISA(long connectionId, CcsiChannelEnum channel, long msn) {
        hpaISAInit.sendSentAckMessage(connectionId, channel, msn);
        return true;
    }

    @Override
    public boolean sendNakISA(long connectionId, CcsiChannelEnum channel, long msn, NakReasonCodeEnum nakCode, NakDetailCodeEnum nakDetailCode, String nakDetailMessag) {
        hpaISAInit.sendSentNakMessage(connectionId, channel, msn, nakCode, nakDetailCode, nakDetailMessag);
        return true;
    }
}
