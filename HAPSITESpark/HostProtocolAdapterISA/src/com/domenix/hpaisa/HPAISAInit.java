/*
 * HPAISAInit.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 * 
 * Initialization of the ISA Interface
 *
 */
package com.domenix.hpaisa;

import com.domenix.commonha.config.HostNetworkSettings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.net.InetAddresses;
import gov.isa.behaviors.ComponentBehavior;
import gov.isa.capabilities.CommandHandler;
import gov.isa.capabilities.PropertyHandler;
import gov.isa.components.PrimaryComponent;
import gov.isa.components.PrimaryComponentDeclaration;
import gov.isa.model.CommandDeclaration;
import gov.isa.model.CustomTypeDeclaration;
import gov.isa.model.Header;
import gov.isa.model.Message;
import gov.isa.model.ModelFactory;
import gov.isa.model.UCI;
import gov.isa.net.ConnectionMonitor;
import gov.isa.net.EndpointDescriptor;
import gov.isa.net.LifeCycleListener;
import gov.isa.net.LifeCyclePhase;
import gov.isa.net.MetaData;
import gov.isa.spi.ComponentFactory;
import gov.isa.spi.ComponentFactoryException;
import gov.isa.spi.ComponentFactoryManager;
import gov.isa.spi.ComponentFactoryOption;
import gov.isa.spi.ComponentFactoryProvider;
import gov.isa.spi.StandardComponentFactoryOptions;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

/**
 * Initialization of the ISA Interface
 *
 * @author jmerritt
 */
public abstract class HPAISAInit 
{
    protected ModelFactory model_factory;

    /**
     * The error/debug logger.
     */
    protected Logger myLogger;

    protected UCI client_uci;
    private String my_uci;
    private String controller_uci;
    private String controller_ip;
    private String controller_port;
    protected PrimaryComponentDeclaration client_declaration;
    
    List<PropertyHandler> customTypePropertyHandlerList = null;
    
    public HPAISAInit() 
    {
        myLogger = Logger.getLogger(HPAISAInit.class);
    }

    public void initialize(HostNetworkSettings settings)
    {
        myLogger.info("Initializing HPAISA Interface");

        my_uci = settings.getPHHostName();
        controller_uci = settings.getHostName();
        controller_ip = settings.getIP ().getHostAddress();
        controller_port = String.valueOf(settings.getPort());
        
        //Get a component factory manager
        ComponentFactoryManager manager = new ComponentFactoryManager();
        try
        {
            // Get the default isa api provider from the component factory manager
            ComponentFactoryProvider provider = manager.getDefaultProvider();

            HPAISASecurity isaSecurity = new HPAISASecurity();
            
            String baseDir = System.getProperty("ccsi.baseDir");
            myLogger.info ("Path for keys: " + baseDir);
            Path path = Paths.get(baseDir + "/keys");

            // create a KeyStoreRetriever
            StandardComponentFactoryOptions.KeyStoreRetriever keystore_retriever = isaSecurity.createKeystoreRetriever(
                    path // current working directory
            );

            // create a KeyPasswordRetriever
            StandardComponentFactoryOptions.KeyPasswordRetriever key_password_retriever = isaSecurity.createPasswordRetriever(
                    path // current working directory
            );

            // set initial min and max status intervals
            // TODO: from config
            long initial_minimum_status_interval = settings.getISAStatusInterval();
            long initial_maximum_status_interval = settings.getISAStatusInterval() * 2;

            // Initialize the provider with custom keystore options
            ComponentFactory component_factory = provider.initialize(
                    ImmutableSet.<ComponentFactoryOption<?>>of(
                            new StandardComponentFactoryOptions.ComponentKeyStoreRetriever(keystore_retriever),
                            new StandardComponentFactoryOptions.ComponentKeyPasswordRetriever(key_password_retriever),
                            new StandardComponentFactoryOptions.ComponentTrustStoreRetriever(keystore_retriever),
                            StandardComponentFactoryOptions.InitialMinimumStatusInterval.create(
                                    initial_minimum_status_interval,
                                    TimeUnit.SECONDS
                            ),
                            StandardComponentFactoryOptions.InitialMaximumStatusInterval.create(
                                    initial_maximum_status_interval,
                                    TimeUnit.SECONDS
                            )
                    )
            );

            // Get a model factory from the component factory.  It is used to construct all data types
            model_factory = component_factory.getModelFactory();

            // Create a uci for this component
            client_uci = model_factory.newUCI(my_uci);

            // Create a uci for the controller you are connecting to.
            UCI server_uci = model_factory.newUCI(controller_uci);

            // Create a component declaration for this component.  It is used to declare capabilities for the component.
            client_declaration
                    = component_factory.createPrimaryComponentDeclaration(client_uci);

            // Add the observable behavior to the declaration
            for (ComponentBehavior observableBehavior : createObservables (model_factory))
            {
                client_declaration.declare(observableBehavior);
            }

            client_declaration.declare(createExternalReceiver(model_factory));

            // create a custom type declaration
            List<CustomTypeDeclaration> customTypeDeclarationList
                    = createCustomTypeDeclaration(model_factory);

            for (CustomTypeDeclaration customTypeDeclaration : customTypeDeclarationList)
            {
                // declare the custom type
                client_declaration.declare(customTypeDeclaration);
            }

            // create property handlers for custom types
            customTypePropertyHandlerList
                    = createCustomTypePropertyHandler(model_factory, customTypeDeclarationList);
            for (PropertyHandler propertyHandler : customTypePropertyHandlerList)
            {
                // declare the custom type property handler
                client_declaration.declare(propertyHandler);
            }

            // Create handlers for the standard properties
            client_declaration.declare(createStandardProperties (model_factory));

            // create a property handler for a new property of custom type
            List<CommandHandler> customTypeCommandHandlerList
                    = createCustomCommandHandler(model_factory);
            for (CommandHandler commandHandler : customTypeCommandHandlerList)
            {
                // declare the custom type property handler
                client_declaration.declare(commandHandler);
            }

            // create a property handler for a new property of standard type
            List<CommandHandler> customCommandHandlerList
                    = createStandardCommandHandler(model_factory);
            for (CommandHandler commandHandler : customCommandHandlerList)
            {
                // declare the custom type property handler
                client_declaration.declare(commandHandler);
            }

            // Create a primary component using the component declaration
            PrimaryComponent client_component = component_factory.createPrimaryComponent(
                    client_declaration);

            // Create an endpoint descriptor for the controller you want to connect to.
            EndpointDescriptor server_endpoint = new EndpointDescriptor(
                    InetAddresses.forString(controller_ip),
                    Integer.parseInt(controller_port),
                    server_uci,
                    ImmutableMap.<String, Object>of (
                            // ipl.encodings is a comma separated list of supported encodings
                            "ipl.encodings","ipl_3v7"
                            // comms should be encrypted
                            ,"stack.secure", String.valueOf(settings.getEncryptComm())
                            )
            );

            // Connect this client to the controller and get a monitor that will watch the connection.
            ConnectionMonitor monitor = client_component.addClientToEndpoint(
                    server_endpoint);

            // Add a connection logger that will connection state changes
            addConnectionLogger(monitor);

        } catch (ComponentFactoryException | IllegalStateException | NumberFormatException e)
        {
            myLogger.error("Problem initializing the component factory: " + e.getMessage());
        }
        myLogger.info ("HPAISA Interface Initialization Thread complete");
    }

    /**
     * Handle life cycle changes
     *
     * @param monitor - ConnectionMonitor
     */
    private void addConnectionLogger(ConnectionMonitor monitor)
    {
        monitor.addLifeCycleListener(
                new LifeCycleListener()
        {
            @Override
            public void onLifeCycleChange(
                    LifeCyclePhase prev,
                    LifeCyclePhase next
            )
            {
                processLifecycleChange(prev, next);
            }
        }
        );
    }

    public List<PropertyHandler> getCustomTypePropertyHandlerList ()
    {
        return customTypePropertyHandlerList;
    }
    
    abstract protected void processExternalMessage(Message message,
            Header header, MetaData metadata);

    abstract protected void processLifecycleChange(LifeCyclePhase prev,
            LifeCyclePhase next);

    abstract protected List<ComponentBehavior> createObservables(ModelFactory modelFactory);

    abstract protected List<CommandDeclaration> createCustomCommands(ModelFactory modelFactory);

    abstract protected ComponentBehavior createStandardProperties(ModelFactory modelFactory);

    abstract protected List<CommandHandler> createCustomCommandHandler(ModelFactory modelFactory);

    abstract protected List<CommandHandler> createStandardCommandHandler(ModelFactory modelFactory);

    abstract protected List<CustomTypeDeclaration> createCustomTypeDeclaration(ModelFactory modelFactory);
    
    abstract protected ComponentBehavior createExternalReceiver(ModelFactory modelFactory);

    abstract protected List<PropertyHandler> createCustomTypePropertyHandler(ModelFactory modelFactory,
            List<CustomTypeDeclaration> customTypeDeclarationList);
}
