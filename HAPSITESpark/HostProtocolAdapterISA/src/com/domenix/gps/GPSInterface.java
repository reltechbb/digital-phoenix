/*
 * GPSInterface.java
 *
 * Copyright (c) 2001-2018 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Interface to a GPS or simulated position
 *
 */
package com.domenix.gps;

//import com.domenix.sensorinterfaceadapter.SensorReceiveCallbackInterface;
//import com.domenix.sensorinterfaceadapter.SerialAdapter;
//import java.io.IOException;
import org.apache.log4j.Logger;

/**
 * Interface to a GPS Operates in one of three modes: static (default),
 * simulated and real Static: reports a static latitude, longitude and altitude
 * Simulated: the latitude, longitude and altitude vary over time Real: actual
 * values are read from the GPS Controlled by: 
 * <br>-Dposition.movement=static (STATIC_MODE), simulated (SIMULATED_MODE), real (REAL_MODE) or no position (NO_POSITION_MODE)
 * <br>-Dposition.latitude=latitude in degrees
 * <br>-Dposition.longitude=longitude in degrees
 * <br>-Dposition.altitude=altitude in degrees
 * <br>-Dposition.device=device // overrides the default device "/dev/ttyUSB1"
 *
 * @author jmerritt
 */
public class GPSInterface implements ReceiveCallbackInterface, Runnable {

    private String device = "/dev/ttyAMA0";
    private String baudRate = "9600";
    private String dataBits = "7";
    private String parity = "none";
    private String stopBits = "1";
    private String flowControl = "none";

    private Double latitude = 38.79;
    private Double longitude = -77.33;
    private Double altitude = 300.0;

    // is the data valid
    private Boolean valid = false;
    
    private enum ModeEnum {
        NO_POSITION_MODE,
        STATIC_MODE,
        SIMULATED_MODE,
        REAL_MODE
    }

    private ModeEnum mode = ModeEnum.SIMULATED_MODE;

//    private SerialAdapter adapter = null;
    
    private Thread simThread;
    
    private final Logger myLogger = Logger.getLogger(GPSInterface.class);
    public GPSInterface() {
        if (System.getProperty("position.movement") != null) {
            switch (System.getProperty("position.movement")) {
                case "NO_POSITION_MODE":
                    mode = ModeEnum.NO_POSITION_MODE;
                    break;
                case "STATIC_MODE":
                    mode = ModeEnum.STATIC_MODE;
                    break;
                case "SIMULATED_MODE":
                    mode = ModeEnum.SIMULATED_MODE;
                    break;
                case "REAL_MODE":
                    mode = ModeEnum.REAL_MODE;
                    break;
                default:
                    // leave at default
                    break;
            }
        }
        myLogger.info("GPS mode is " + mode.name());
        if (System.getProperty("position.latitude") != null)
        {
            latitude = Double.parseDouble(System.getProperty("position.latitude"));
            myLogger.info ("Overriding default latitude = " + latitude);
        }
        if (System.getProperty("position.longitude") != null)
        {
            longitude = Double.parseDouble(System.getProperty("position.longitude"));
            myLogger.info ("Overriding default longitude = " + longitude);
        }
        if (System.getProperty("position.altitude") != null)
        {
            altitude = Double.parseDouble(System.getProperty("position.altitude"));
            myLogger.info ("Overriding default altitude = " + altitude);
        }
        if (System.getProperty("position.device") != null)
        {
            device = System.getProperty("position.device");
            myLogger.info ("Overriding default GPS device = " + device);
        }
        
        switch (mode)
        {
            case NO_POSITION_MODE:
                // always invalid
                valid = false;
                break;
            case STATIC_MODE:
                // always valid in static mode
                valid = true;
                break;
            case SIMULATED_MODE:
                simulatedMode ();
                break;
            case REAL_MODE:
                realMode ();
                break;
            default:
                break;
        }
    }

    @Override
    public void dataReceived(String message) {
        if (message.startsWith("$GPGGA")) {
            String[] reading = message.split(",");
            if (reading.length < 14) {
                myLogger.error("Incorrect GPS field count");
                return;
            }
            valid = (Integer.parseInt(reading[6]) >= 1);
            if (valid) {
                // format DDmm.mmmm
                double degrees = Double.parseDouble(reading[2].substring(0, 2));
                double minutes = Double.parseDouble(reading[2].substring(2)) / 60.0;
                latitude = latitude = degrees + minutes;
                if (reading[3].equals("S")) {
                    latitude = -latitude;
                }
                // format DDDmm.mmmm
                degrees = Double.parseDouble(reading[4].substring(0, 3));
                minutes = Double.parseDouble(reading[4].substring(3)) / 60.0;
                longitude = degrees + minutes;
                if (reading[5].equals("W")) {
                    longitude = -longitude;
                }
                altitude = Double.parseDouble(reading[9]);
            }
        }
    }

    private void simulatedMode ()
    {
        simThread = new Thread (this);
        simThread.start();
    }
    
    private void realMode ()
    {
//KBM:   START temp removal.
//        adapter = new SerialAdapter();
//        try {
//            adapter.initialize(device, baudRate, dataBits, parity, stopBits, flowControl, true);
//            adapter.setListener(this);
//            adapter.start();
//        } catch (IOException ex) {
//            myLogger.error("IOException: " + ex.getMessage());
//        }
//KBM:   END temp removal.
    }

    @Override
    public void dataReceived(byte[] message) {
        throw new UnsupportedOperationException("Binary data not supported.");
    }

    @Override
    public void run() {
        // always valid in simulated mode
        valid = true;
        while (true) {
            try {
                Thread.sleep(30000);
                // At 38 degrees N: 1 degree = 364,000 feet (69 miles)
                latitude = latitude + .004;
                // 1 degree longitude = 288,200 feet (54.6 miles)
                longitude = longitude + .005;
                // altitude is in meters
                altitude = altitude + 1.0;

            } catch (InterruptedException ex) {
                myLogger.error("Interrupted exception");
            }
        }
    }

    public Double getLatitude()
    {
        return latitude;
    }
    public Double getLongitude ()
    {
        return longitude;
    }
    public Double getAltitude ()
    {
        return altitude;
    }
    public Boolean isValid ()
    {
        return valid;
    }
    
    public void setPosition (Double latitude, Double longitude, Double altitude)
    {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        valid = true;
    }
}
